#!/bin/bash

echo "#-----------------------------------#"
find workbench/bct -maxdepth 1 -mindepth 1 -type d -exec sh -c 'cd "{}" && echo extension: "{}" && composer dump-auto && echo "#-----------------------------------#"' \;
#for d in workbench/bct/* ; do (cd "$d" && echo extension: "$d" && composer dump-auto); done