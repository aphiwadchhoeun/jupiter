<?php
return[

	'table'=> 'z_members',
	'fields'=>[
		'First Name'=>'first_name',
		'Last Name'=>'last_name',
		'Social Number'=>'social_number',
		'Member Start Date'=>'member_since',
	],
	'pivot' => [],
];
