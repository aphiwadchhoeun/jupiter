<?php
/**
 * Created by PhpStorm.
 * User: sky
 * Date: 06.04.16
 * Time: 14:56
 */

return [
    'extensions' => [
        'bct/dispatch' => [
            'domain' => env('GETCODE_DOMAIN_DISPATCH', 'union.dp'),
            'enable' => [
                'bct/dispatch'
            ]
        ],

        'bct/dispatch_janitors' => [
            'domain' => env('GETCODE_DOMAIN_DISPATCH_JANITORS', 'union.dpj'),
            'enable' => [
                'bct/dispatch_janitors',
                'bct/member',
                'bct/projects',
            ]
        ],

        'bct/dispatch_theater' => [
            'domain' => env('GETCODE_DOMAIN_DISPATCH_THEATER', 'union.dpth'),
            'enable' => [
                'bct/dispatch_theater',
                'bct/member',
                'bct/projects',
            ]
        ],

        'bct/member' => [
            'domain' => env('GETCODE_DOMAIN_MEMBER', 'union.mp'),
            'enable' => [
                'bct/member'
            ]
        ],

        'bct/mrp' => [
            'domain' => env('GETCODE_DOMAIN_MRP', 'union.mrp'),
            'enable' => [
                'bct/mrp'
            ]
        ],

        'bct/mrp_pnw' => [
            'domain' => env('GETCODE_DOMAIN_MRP_PNW', 'union.mrp-pnw'),
            'enable' => [
                'bct/mrp_pnw'
            ]
        ],

        'bct/projects' => [
            'domain' => env('GETCODE_DOMAIN_PROJECTS', 'union.pp'),
            'enable' => [
                'bct/projects'
            ]
        ],

        'bct/rci' => [
            'domain' => env('GETCODE_DOMAIN_RCI', 'union.rci'),
            'enable' => [
                'bct/rci'
            ]
        ],

        'bct/payment' => [
            'domain' => env('GETCODE_DOMAIN_PAYMENT', 'union.payment'),
            'enable' => [
                'bct/payment'
            ]
        ],

        'bct/payment_seiu87' => [
            'domain' => env('GETCODE_DOMAIN_PAYMENT_SEIU87', 'union.payment-seiu87'),
            'enable' => [
                'bct/payment_seiu87'
            ]
        ],
    ]
];