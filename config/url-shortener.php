<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 4/3/2017
 * Time: 4:01 PM
 */

return [

    'domain'   => env('URL_SHORTENER_DOMAIN', ''),
    'endpoint' => '/api/v2/action/shorten',
    'token'    => env('URL_SHORTENER_TOKEN', '')

];