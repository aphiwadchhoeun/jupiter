<?php
/**
 * Part of the Filesystem package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Filesystem
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Maximum allowed size for uploaded files
    |--------------------------------------------------------------------------
    |
    | Define here the maximum size of an uploaded file in bytes.
    |
    | Default: 10 Mb.
    |
    */

    'max_filesize' => 31457280,

    /*
    |--------------------------------------------------------------------------
    | Allowed types of files
    |--------------------------------------------------------------------------
    |
    | Define here all the allowed mime types that are allowed for upload.
    |
    | Look at http://www.iana.org/assignments/media-types for a
    | complete list of standard MIME types
    |
    */

    'allowed_mimes' => [

        // Audio & Video
        'audio/ogg', 'video/mp4', 'video/ogg', 'video/avi',

        // Application
        'application/zip', 'application/pdf', 'application/x-rar-compressed', 'application/x-7z-compressed',
        'application/7zip', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/octet-stream',
        'application/vnd.oasis.opendocument.text', 'application/vnd.oasis.opendocument.spreadsheet', 'pplication/vnd.oasis.opendocument.presentation',
        'application/vnd.ms-powerpointtd', 'application/vnd.openxmlformats-officedocument.presentationml.presentation',

        // Images
        'image/gif', 'image/jpeg', 'image/png', 'image/jpg', 'image/bmp', 'image/x-windows-bmp', 'image/vnd.adobe.photoshop',

        // Text
        'text/plain', 'text/csv'

    ],

    /*
    |--------------------------------------------------------------------------
    | File dispersion
    |--------------------------------------------------------------------------
    |
    | This feature allows you to have a better and more organized file
    | structure that you dictate using placeholders.
    |
    | To disable this feature just set a boolean of "false" as the value.
    |
    | Reserved placeholders:
    |
    |   Information from uploaded files
    |       :extension -> jpg
    |       :mime      -> image/jpeg
    |
    | Supported placeholders by default:
    |
    |   Current Year
    |       :yyyy  ->  2013
    |       :yy    ->  13
    |
    |   Current Month
    |       :mmmm  ->  November
    |       :mmm   ->  Nov
    |       :mm    ->  11
    |
    |   Current Day
    |       :dddd  ->  Friday
    |       :ddd   ->  Fri
    |       :dd    ->  24
    |
    | Note: You need to add the forward slash (/) at the end.
    |
    | Example:
    |
    |   'dispersion' => ':yyyy/:mm/'
    |
    |   All your files will be stored, by default, on:
    |
    |       app/storage/filesystem/2014/01/foo.jpg
    |       app/storage/filesystem/2014/01/bar.png
    |       app/storage/filesystem/2014/01/test.pdf
    |
    */

    'dispersion' => false,

    /*
    |--------------------------------------------------------------------------
    | Placeholders
    |--------------------------------------------------------------------------
    |
    | Define here all the file dispersion placeholders.
    |
    */

    'placeholders' => [

        ':yyyy' => date('Y'),
        ':yy'   => date('y'),
        ':mmmm' => date('F'),
        ':mmm'  => date('M'),
        ':mm'   => date('m'),
        ':dddd' => date('l'),
        ':ddd'  => date('D'),
        ':dd'   => date('d'),

    ],

    /*
    |--------------------------------------------------------------------------
    | Default Connection
    |--------------------------------------------------------------------------
    |
    | Define here the connection name that you want to use by default.
    |
    */

    'default' => 'awss3',

    /*
    |--------------------------------------------------------------------------
    | Connections
    |--------------------------------------------------------------------------
    |
    | Below are all default connections for the various drivers you can use.
    |
    */

    'connections' => [

        // Local
        'local' => [

            'adapter' => 'local',
            'path'    => storage_path().'/files',

        ],

        // Zip
        'zip' => [

            'adapter' => 'zip',
            'path'    => storage_path().'/files/filesystem.zip',

        ],

        // Amazon S3
        'awss3' => [

            'adapter' => 'awss3',
            'key'    => env('S3_KEY', null),
            'secret' => env('S3_SECRET', null),
            'region' => env('S3_REGION', null),
            'bucket' => env('S3_BUCKET', null),
            'prefix'  => null,
            'version' => 'latest',
            'options' => [

            ],

        ],

        // Dropbox
        'dropbox' => [

            'adapter'  => 'dropbox',
            'token'    => null,
            'app_name' => null,
            'prefix'   => null,

        ],

        // Rackspace
        'rackspace' => [

            'adapter'   => 'rackspace',
            'endpoint'  => null,
            'username'  => null,
            'password'  => null,
            'container' => null,
            'service'   => 'cloudFiles',
            'region'    => 'LON',

        ],

        // WebDAV
        'webdav' => [

            'adapter'  => 'webdav',
            'baseUri'  => 'http://example.com/dav/',
            'username' => null,
            'password' => null,

        ],

        // File Transfer Protocol
        'ftp' => [

            'adapter'  => 'ftp',
            'host'     => 'ftp.example.com',
            'port'     => 21,
            'username' => 'username',
            'password' => 'password',
            'root'     => '/path/to/root',
            'passive'  => true,
            'ssl'      => true,
            'timeout'  => 30,

        ],

        // SSH File Transfer Protocol
        'sftp' => [

            'adapter'    => 'sftp',
            'host'       => 'sftp.example.com',
            'port'       => 22,
            'username'   => 'username',
            'password'   => 'password',
            'root'       => '/path/to/root',
            'privateKey' => 'path/to/or/contents/of/privatekey',
            'timeout'    => 10,

        ],

    ],

];
