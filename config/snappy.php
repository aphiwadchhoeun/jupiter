<?php

return array(


    'pdf'   => array(
        'enabled' => true,
        'binary'  => (env('APP_ENV') !== 'local') ? base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64') : env('WKHTMLTOPDF_PATH','/usr/local/bin/wkhtmltopdf'),
        'timeout' => 300,
        'options' => array(
            'margin-bottom'    => '15mm',
            'footer-spacing'   => 5,
            'footer-font-size' => 9,
            'footer-left'      => '[date] [time]',
            'footer-center'    => 'Page [page] of [toPage]',
        ),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => (env('APP_ENV') !== 'local') ? base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64'): env('WKHTMLTOIMAGE_PATH','/usr/local/bin/wkhtmltoimage'),
        'timeout' => false,
        'options' => array(),
    ),


);
