<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN', ''),
        'secret' => env('MAILGUN_SECRET', ''),
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        //'model'  => App\User::class,
        'key'       => 'pk_test_CeFJcvDmd3ClSG0j72Xtjwmv',
        'secret'    => 'sk_test_dZVIboTk96Pel2cFR7GPs8yb',

        // for one account
        //'account'   => 'acct_19C624HZhbOYeqbQ',
        //'platformSecret' => 'sk_test_r5g2of6Sk2Z6qZ3EEMLHYvLm'

        // for two accounts
        'account'   => 'acct_19Ia0aBYPfcUmszB',
        'platformSecret' => 'sk_test_dEsqJLs6VnYqkIdw0QoQ8bQr'
    ],

];
