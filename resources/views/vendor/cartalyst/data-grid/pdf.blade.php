<!DOCTYPE html>
<html>
<head>
    <title>Data Grid - PDF View</title>
    <meta charset="utf-8">

    {{ Asset::queue('normalize', 'app/css/normalize.css') }}
    {{ Asset::queue('bootstrap-grid', 'bootstrap/css/bootstrap-grid-export.css') }}

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach
</head>
<body>

<div class="row">
    <div class="col-md-1">
        <img src="{{url('/themes/frontend/jupiter/assets/app/img/logo.png')}}" alt="@setting('platform.app.title')" height="40"/>
    </div>
    <div class="col-md-11 text-center">
        <h1>{{ (isset($defaultTitle) && $defaultTitle !== '') ? $defaultTitle : (isset($title) && $title !== '') ? $title : array_get($results[0], 'title', '') }}</h1>
    </div>
</div>

<div class="hr10"></div>


<div class="row ext-profiles">

    <?php $keys = array_filter($keys, function ($var) {
        if (!in_array($var, ['title', 'pagination'])) return $var;
    }); ?>

    <div class="col-md-12">

        <table class="table">
            <thead>
            <tr>
                @foreach ($keys as $column)
                    <th
                            @if (isset($columns_width[$column]))
                            class="{{ $columns_width[$column] }}"
                            @endif
                    >{{ ucwords(str_replace(['-', '_'], ' ', $column)) }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($results as $result)
                <tr>
                    @foreach ($keys as $column)
                        <td>
                            {{ $result[$column] }}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

</div>


<div class="footer-info">
    <div class="footer-info-date">
        {{ Carbon\Carbon::now()->format('n/j/Y H:i:s') }}
    </div>
    <div class="footer-info-pagination">
        {{ (isset($exportPageInfo) && $exportPageInfo !== '') ? $exportPageInfo : array_get($results[0], 'pagination', '') }}
    </div>
</div>

</body>
</html>
