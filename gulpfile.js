var elixir = require('laravel-elixir'),
    gulp = require('gulp'),
    shell = require('gulp-shell'),
    livereload = require('gulp-livereload'),
    dotenv = require('dotenv').config({path: '.env'});


/*
 |--------------------------------------------------------------------------
 | UPDATE THEMES ON FILES UPDATE
 |--------------------------------------------------------------------------
 | 1) Install gulp global
 |    npm install --global gulp
 | 2) Install dependencies
 |    npm install
 | 3) Install livereload plugin for your browser
 |    Chrome  - https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei
 |    Safari  - http://download.livereload.com/2.1.0/LiveReload-2.1.0.safariextz
 |    Firefox - http://download.livereload.com/2.1.0/LiveReload-2.1.0.xpi
 | 4) Make sure LiveReload is running (in browser). Click the LiveReload toolbar button to enable or disable LiveReload.
 | 5) Set var of watch module to .enf file (module_name is one of: dispatch, dispatch_janitors, member, mrp, projects)
 |      GULP_WATCH_MODULE={module_name}
 | 6) Use this =)
 |    gulp
 */

var config = {
    path: {
        folder: 'workbench/bct/'
    }
};

var paths = {
    views: {
        dispatch: {
            folders: [ config.path.folder + 'dispatch/themes/**/*' ],
            extensions: ['dispatch']
        },
        dispatch_janitors: {
            folders: [ config.path.folder + 'dispatch_janitors/themes/**/*' ],
            extensions: ['dispatch_janitors']
        },
        dispatch_theater: {
            folders: [ config.path.folder + 'dispatch_theater/themes/**/*' ],
            extensions: ['dispatch_theater']
        },
        member: {
            folders: [ config.path.folder + 'member/themes/**/*' ],
            extensions: ['member']
        },

        mrp: {
            folders: [ config.path.folder + 'mrp/themes/**/*' ],
            extensions: ['mrp']
        },
        mrp_pnw: {
            folders: [ config.path.folder + 'mrp_pnw/themes/**/*' ],
            extensions: ['mrp_pnw']
        },

        payment: {
            folders: [ config.path.folder + 'payment/themes/**/*' ],
            extensions: ['payment']
        },

        payment_seiu87: {
            folders: [
                config.path.folder + 'payment_seiu87/themes/**/*',
                config.path.folder + 'member/themes/**/*',
            ],
            extensions: ['payment_seiu87', 'member']
        },
        member_seiu87: {
            folders: [
                config.path.folder + 'member_seiu87/themes/**/*',
                config.path.folder + 'member/themes/**/*',
            ],
            extensions: ['member_seiu87', 'member']
        },

        member_proa: {
            folders: [
                config.path.folder + 'member_proa/themes/**/*',
            ],
            extensions: ['member_proa']
        },
        payment_proa: {
            folders: [
                config.path.folder + 'payment_proa/themes/**/*',
            ],
            extensions: ['payment_proa']
        },

        member_uaw2865: {
            folders: [
                config.path.folder + 'member_uaw2865/themes/**/*',
            ],
            extensions: ['member_uaw2865']
        },
        payment_uaw2865: {
            folders: [
                config.path.folder + 'payment_uaw2865/themes/**/*',
            ],
            extensions: ['payment_uaw2865']
        },
        uaw2865: {
            folders: [
                config.path.folder + 'member_uaw2865/themes/**/*',
                config.path.folder + 'payment_uaw2865/themes/**/*'
            ],
            extensions: ['member_uaw2865', 'payment_uaw2865']
        },

        rci: {
            folders: [ config.path.folder + 'rci/themes/**/*' ],
            extensions: ['rci']
        },
        projects: {
            folders: [ config.path.folder + 'projects/themes/**/*' ],
            extensions: ['projects']
        },

        import: {
            folders: [ config.path.folder + 'import/themes/**/*' ],
            extensions: ['import']
        },

        oeiu: {
            folders: [
                config.path.folder + 'jatc_enrollment/themes/**/*',
            ],
            extensions: ['jatc_enrollment']
        },

        reports: {
            folders: [ config.path.folder + 'reports/themes/**/*' ],
            extensions: ['reports']
        },

        unionimpactbase: {
            folders: [ config.path.folder + 'unionimpactbase/themes/**/*' ],
            extensions: ['unionimpactbase']
        }
    }
};

gulp.task('theme:publish:all', function () {
    return gulp.src('')
        .pipe(
            shell('php artisan theme:publish --extensions')
        )
        .pipe(livereload())
});



var defaultTasks = Object.keys(paths.views);

defaultTasks.forEach(function(taskName) {

    gulp.task('theme:publish:' + taskName, function () {

        return paths.views[taskName].extensions.forEach(function(extension) {
            gulp.src('')
                .pipe(
                    shell('php artisan theme:publish --extension=bct/' + extension)
                )
                .pipe(livereload())
        });
    });

});

gulp.task('watch', function () {
    livereload.listen();

    if (process.env.GULP_WATCH_MODULE !== 'undefined') {

        defaultTasks.forEach(function(taskName) {
            if (process.env.GULP_WATCH_MODULE === taskName) {
                gulp.watch(paths.views[taskName].folders, { interval: 2000 }, ['theme:publish:' + taskName])
            }
        });

    }
});

gulp.task('default', ['watch']);