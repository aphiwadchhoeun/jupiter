<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->email,
        'password'       => str_random(10),
        'remember_token' => str_random(10),
    ];
});

/*
 * -----------------------------------
 *         Projects Extension
 * -----------------------------------
 *
 * 1) Create Project with contractors (with scope) and visits
 * factory(Bct\Projects\Models\Project::class, 90000)->create()->each(function($u) {
        $u->visits()->save( factory(Bct\Projects\Models\Visit::class, 1)->create() );
        $u->contractors()->save(
        	Bct\Projects\Models\Contractor::all()->random()
        , ['z_scopes_id' => Bct\Projects\Models\Scope::all()->random()->id]);
    });
 */
$factory->define(Bct\Projects\Models\Contact::class, function ($faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name'  => $faker->lastName,
        'phone'      => $faker->phoneNumber,
        'email'      => $faker->email,
        'created_by' => 1,
        'company'    => $faker->company,
        'address'    => $faker->address,
        'city'       => $faker->city,
        'state'      => $faker->state,
        'zip'        => $faker->postcode,
        'fax'        => $faker->phoneNumber,
    ];
});

$factory->define(Bct\Projects\Models\Visit::class, function ($faker) {
    return [
        'details'          => $faker->sentence,
        'z_projects_id'    => 11,
        'approved'         => 0,
        'approved_user_id' => 1,
    ];
});

$factory->define(Bct\Projects\Models\ProjectVisitUser::class, function ($faker) use ($factory) {
    return [
        'z_project_visit_id' => factory(Bct\Projects\Models\Visit::class)->create()->id,
        'user_id'            => 1,
    ];
});

$factory->define(Bct\Projects\Models\Project::class, function ($faker) {
    $faker->addProvider(new Faker\Provider\pt_BR\Address($faker));

    return [
        'name'        => 'Project ' . $faker->company,
        'address'     => $faker->address,
        'city'        => $faker->city,
        'state'       => $faker->state,
        'zipcode'     => $faker->postcode,
        'region'      => $faker->region,
        'region_abrv' => $faker->regionAbbr,
        'lat'         => $faker->latitude,
        'lng'         => $faker->longitude,
        'union_value' => randomFloat()
    ];
});

$factory->define(Bct\Projects\Models\Contractor::class, function ($faker) {
    $faker->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));

    return [
        'name'              => $faker->company,
        'address'           => $faker->address,
        'city'              => $faker->city,
        'state'             => $faker->state,
        'zipcode'           => $faker->postcode,
        'phone'             => $faker->phone,
        'fax'               => $faker->phone,
        'contact_firstname' => $faker->firstName,
        'contact_lastname'  => $faker->lastName,
        'email'             => $faker->email,
        'license'           => $faker->ean13,
        'ein'               => $faker->ean8,
        'is_union'          => 1,
        'created_by'        => 1
    ];
});

$factory->define(Bct\Projects\Models\Visit::class, function ($faker) {
    return [
        'details'          => $faker->text(50),
        'approved'         => 1,
        'approved_user_id' => 1
    ];
});

/*
 * -----------------------------------
 *          Member Extension
 * -----------------------------------
 */
$factory->define(Bct\Member\Models\Local::class, function ($faker) {
    return [
        'name' => 'Local ' . $faker->company,
    ];
});

$factory->define(Bct\Member\Models\Member::class, function ($faker) {
    return [
        'first_name'    => $faker->firstName,
        'last_name'     => $faker->lastName,
        'date_of_birth' => $faker->date,
        'gender'        => collect([ 'male', 'female' ])->random(),
        'social_number' => $faker->randomNumber(3) . '-' . $faker->randomNumber(2) . '-' . $faker->randomNumber(4),
        'member_number' => $faker->numberBetween(1000000, 1000000000),
        'member_since'  => $faker->date,
        'address'       => $faker->streetAddress,
        'city'          => $faker->city,
        'state'         => $faker->stateAbbr,
        'zipcode'       => $faker->postcode,
        'phone'         => $faker->numerify('(###) ###-#### x###'),
        'email'         => $faker->email,
        'active'        => 1
    ];
});

$factory->define(Bct\Member\Models\Employer::class, function ($faker) {

    $faker->addProvider(new Faker\Provider\pt_BR\Company($faker));

    return [
        'name'     => $faker->company,
        'address'  => $faker->streetAddress,
        'city'     => $faker->city,
        'state'    => $faker->stateAbbr,
        'zipcode'  => $faker->postcode,
        'phone'    => $faker->numerify('(###) ###-#### x###'),
        'license'  => $faker->cnpj(false),
        'is_union' => collect([ 1, 0 ])->random(),
    ];
});

$factory->define(Bct\Payment\Models\Item::class, function ($faker) {

    return [
        'extension_cat'      => collect(['Credit', 'Charge'])->random(),
        'is_checkoutvisible' => collect([1, 0])->random(),
        'is_active'          => collect([1, 0])->random(),
        'is_tax'             => collect([1, 0])->random(),
        'description'        => $faker->text(50),
        'item_type'          => collect(['Event', 'MISC'])->random(),
        'qnt'                => 1,
        'rate'               => 0.00,
        'is_append'          => collect([1, 0])->random(),
        'can_delete'         => 1,
    ];

});

$factory->define(Bct\Payment\Models\Pack::class, function ($faker) {
    return [
        'description' => 'pack for group 2`',
        'z_member_group_id' => 0,
        'active' => 1,
    ];
});

$factory->define(Bct\Payment\Models\Group::class, function ($faker) {

    $faker->addProvider(new Faker\Provider\en_US\Company($faker));

    return [
        'name'        => $faker->company,
        'description' => $faker->catchPhrase,
        'status'      => collect([1, 0])->random(),
    ];

});

$factory->define(Bct\Member\Models\Work::class, function ($faker) {

    return [
        'position' => 'Janitor',
        'start_date' => $faker->date,
        'end_date' => '0000-00-00',
        'memo' => $faker->sentence(40),
        'type' => collect(['P', 'A', 'B', 'C'])->random(),
        'number_of_hours' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 9999),
        'created_by' => 0,
        'employment' => 0,
    ];

});

$factory->define(Bct\Payment\Models\GroupMember::class, function ($faker) {

    return [];

});


function randomFloat($min = 0.7, $max = 1)
{
    return $min + mt_rand() / mt_getrandmax() * ( $max - $min );
}