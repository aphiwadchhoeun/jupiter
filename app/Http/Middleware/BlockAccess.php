<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Sentinel;
use Closure;

class BlockAccess {

    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  \Cartalyst\Sentinel\Sentinel $auth
     * @return void
     */
    public function __construct(Sentinel $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $blacklist = explode("\r\n", @config('bct.unionimpactbase.blacklist.clients', null));

        foreach ($blacklist as $client) {

            if (preg_match("/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/i", $client)) {    // check if match single ip address

                if ($request->ip() == $client) {
                    abort(403);
                }

            } else if (preg_match("/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\-[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/i", $client)) { // check if match ip address range

                list($lower, $upper) = explode('-', str_replace('.', '', $client));

                if (str_replace('.', '', $request->ip()) >= $lower && $request->ip() <= $upper) {
                    abort(403);
                }

            } else if (preg_match("/^[0-9*]+\.[0-9*]+\.[0-9*]+\.[0-9*]+$/i", $client)) {    // check if match wildcard ip address

                $reg = str_replace('*', '[0-9]+', $client);
                $reg = str_replace('.', '\.', $reg);

                if (preg_match("/$reg/i", $request->ip())) {
                    abort(403);
                }

            } else if (preg_match("/^[a-zA-Z0-9.-_]+@[a-zA-Z0-9-.]+\.[a-zA-Z0-9]+$/i", $client)) {   // check if match a user email

                if ($this->auth->check()) {
                    if ($client == $this->auth->getUser()->email) {
                        abort(403);
                    }
                }

            }

        }

        return $next($request);
    }
}
