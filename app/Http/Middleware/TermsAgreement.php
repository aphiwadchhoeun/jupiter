<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 1/26/2017
 * Time: 1:02 PM
 */

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Sentinel;
use Closure;

class TermsAgreement
{

    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $auth;

    /**
     * Exception urls
     */
    protected $excepts = [
        'terms-agreement',
        'policy',
        'logout',
    ];


    /**
     * Create a new filter instance.
     *
     * @param  \Cartalyst\Sentinel\Sentinel $auth
     *
     * @return void
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! env('DISABLED_TOS')) {
            // check if user is currently logged in
            if ($this->auth->check()) {

                // give exception to god user
                if ((int) $this->auth->getUser()->getUserId() !== 1) {
                    // check if current user hasn't agreed to terms and service yet
                    if ( ! in_array($request->decodedPath(), $this->excepts) && data_get($this->auth->getUser(),
                            'terms_agreement') == 0
                    ) {
                        return redirect()->route('bct.terms.agreements');
                    }
                }
            }
        }

        return $next($request);
    }

}