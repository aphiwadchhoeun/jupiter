<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Support\Facades\Route;

class Authenticate {
    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  \Cartalyst\Sentinel\Sentinel $auth
     * @return void
     */
    public function __construct(Sentinel $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {


        if ($this->auth->guest()) {


            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                $map_routes = array(
                    'bct.projects.map',
                    'bct.projects.routes.search',
                    'bct.projects.projects.search',
                    'bct.projects.routes.index',
                    'bct.projects.projects.create',
                    'bct.projects.projects.edit',
                    'bct.dp.jobs.create',
                    'bct.dp.jobs.details',
                    'bct.mrp.makeRequest.index',
                    'bct.mrp.projects.edit',
                    'bct.mrp.makeRequest.search.project.map',
                );

                if (in_array(Route::currentRouteName(), $map_routes)) {
                    return redirect()->route('bct.openroutes');
                }

                app('alerts')->error(trans('message.not_logged_in'));

                return redirect()->guest('login');
            }
        }

        return $next($request);
    }
}
