<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Whoops\Handler\JsonResponseHandler;
use Illuminate\Session\TokenMismatchException as TokenMismatchException;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($this->isMethodNotAllowedHttpException($e)) {
            return abort(404);
        }
        elseif ($this->isHttpException($e))
        {
            return $this->renderHttpExceptionCustom($request, $e);
        }
        elseif ($e instanceof TokenMismatchException) {
            return response()->view('errors.token', [], 500);
        }
        else
        {
            if (config('app.debug'))
            {
                $whoops = new Run;
                $whoops->pushHandler(new PrettyPageHandler);

                return response($whoops->handleException($e),
                    $e->getStatusCode(),
                    $e->getHeaders()
                );
            }
        }


        return parent::render($request, $e);
    }

    protected function renderHttpExceptionCustom($request, HttpException $e)
    {
        if (view()->exists('errors.'.$e->getStatusCode()))
        {
            return response()->view('errors.'.$e->getStatusCode(), [], $e->getStatusCode());
        }
        else
        {
            return parent::render($request, $e);
        }
    }

    protected function isMethodNotAllowedHttpException(Exception $e)
    {
        return $e instanceof MethodNotAllowedHttpException;
    }
}
