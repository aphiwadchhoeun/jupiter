<?php
/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 23.03.17
 * Time: 17:12
 */


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueueErrorLogTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('queue_error_log');

        $util->setOptions([
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name' => 'queue_table',
                'type' => 'string'
            ],
            [
                'name' => 'queue_id',
                'type' => 'integer'
            ],
            [
                'name' => 'message',
                'type' => 'text'
            ],

        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}