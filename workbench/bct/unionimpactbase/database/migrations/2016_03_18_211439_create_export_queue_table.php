<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportQueueTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('export_queue');

        $util->setOptions([
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name' => 'name',
                'type' => 'string',
            ],
            [
                'name' => 'path',
                'type' => 'string',
            ],
            [
                'name' => 'source',
                'type' => 'string',
            ],
            [
                'name' => 'ext',
                'type' => 'string',
            ],
            [
                'name' => 'mime',
                'type' => 'string',
            ],
            [
                'name'    => 'created_by',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name' => 'status',
                'type' => 'boolean',
            ],
            [
                'name' => 'hash',
                'type' => 'string',
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
