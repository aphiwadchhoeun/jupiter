<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigDatagridTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('config_datagrid');

        $util->setOptions([
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name' 		=> 'user_id',
                'type' 		=> 'integer',
            ],
            [
                'name' 		=> 'config',
                'type' 		=> 'text',
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
