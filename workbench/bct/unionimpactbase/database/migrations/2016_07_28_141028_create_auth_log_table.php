<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthLogTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('auth_log');

        $util->setOptions([
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name'    => 'user_email',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'user_name',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name' => 'ip',
                'type' => 'string',
            ],
            [
                'name'    => 'browser',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'browser_version',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'platform',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'action',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name' => 'success',
                'type' => 'boolean',
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
