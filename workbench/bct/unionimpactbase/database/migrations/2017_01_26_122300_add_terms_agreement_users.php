<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTermsAgreementUsers extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('users');

        $util->setOptions([
            [
                'name' => 'terms_agreement',
                'type' => 'boolean',
            ],
            [
                'name'    => 'terms_agreement_sign_date',
                'type'    => 'datetime',
                'options' => [
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
