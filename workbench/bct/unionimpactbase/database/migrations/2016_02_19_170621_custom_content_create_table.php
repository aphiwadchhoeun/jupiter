<?php

/**
 * Part of the Platform Content extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Content extension
 * @version    3.2.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2016, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomContentCreateTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('custom_content');

        $util->setOptions([
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name' => 'name',
                'type' => 'string',
            ],
            [
                'name'   => 'slug',
                'type'   => 'string',
                'unique' => true
            ],
            [
                'name'    => 'value',
                'type'    => 'text',
                'options' => [
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

    }
}
