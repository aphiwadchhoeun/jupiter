<?php namespace Bct\Unionimpactbase\Database\Seeds;

use Illuminate\Database\Seeder;
use DB;

class CustomContentTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('custom_content')->truncate();

        DB::table('custom_content')->insert([
            [
                'name'  => 'Kiosk Terms Page',
                'slug'  => 'kiosk-terms-page',
                'value' => 'Here will be terms...'
            ],

            [
                'name'  => 'Landing Page Contractor',
                'slug'  => 'landing-page-contractor',
                'value' => 'Welcome to UnionImpact!
To start submitting MRP Request, administrator(s) need to review your account profile for approval.
No action is needed, once administrator approved your account profile, you will automatically gain access to Market Recovery section.'
            ],

            [
                'name'  => 'Sign Up Page',
                'slug'  => 'sign-up-page',
                'value' => 'Your account has been created!
The system requires account activation, an activation key has been sent to the e-mail address you provided.
Please check your e-mail for further information.'
            ],

            [
                'name'  => 'Token Error Page',
                'slug'  => 'token-error-page',
                'value' => 'CSRF token mismatch',
            ],

        ]);
    }
}
