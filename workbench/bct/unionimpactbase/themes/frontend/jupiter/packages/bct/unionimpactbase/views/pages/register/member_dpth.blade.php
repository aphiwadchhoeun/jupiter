@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    {{{ trans('bct/unionimpactbase::pages/common.titles.signup') }}} ::
    @parent
@stop

{{-- Inline Scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
    {{ Asset::queue('selectize', 'bower_components/selectize/dist/js/standalone/selectize.min.js', 'jquery') }}

    {{ Asset::queue('register-form', 'bct/unionimpactbase::members/js/register.js', 'jquery') }}

@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('selectize-css', 'bower_components/selectize/dist/css/selectize.css') }}
    {{ Asset::queue('selectize-bootstrap3', 'bower_components/selectize/dist/css/selectize.bootstrap3.css', 'bootstrap') }}

@stop

{{-- Page content --}}
@section('page')

    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            {{-- Form --}}
            <form id="register-form" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate action="{{ $form_action }}">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="panel panel-default">

                    <div class="panel-heading">{{ $form_title }}</div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                {{-- First Name --}}
                                <div class="form-group{{ Alert::onForm('first_name', ' has-error') }}">

                                    <label class="control-label"
                                           for="first_name">{{ trans('bct/unionimpactbase::pages/model.form.signup.first_name') }}</label>

                                    <input class="form-control" name="first_name" id="first_name" value="{{ Input::old('first_name') }}"
                                           placeholder="{{ trans('bct/unionimpactbase::pages/model.form.signup.first_name_placeholder') }}"
                                           autofocus>

                                        <span class="help-block">
                                            {{ Alert::onForm('first_name') ?: trans('bct/unionimpactbase::pages/model.form.signup.first_name_help') }}
                                        </span>

                                </div>

                            </div>
                            <div class="col-md-4">
                                {{-- Last Name --}}
                                <div class="form-group{{ Alert::onForm('last_name', ' has-error') }}">

                                    <label class="control-label"
                                           for="last_name">{{ trans('bct/unionimpactbase::pages/model.form.signup.last_name') }}</label>

                                    <input class="form-control" name="last_name" id="last_name" value="{{ Input::old('last_name') }}"
                                           placeholder="{{ trans('bct/unionimpactbase::pages/model.form.signup.last_name_placeholder') }}"
                                           autofocus>

                                        <span class="help-block">
                                            {{ Alert::onForm('last_name') ?: trans('bct/unionimpactbase::pages/model.form.signup.last_name_help') }}
                                        </span>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                {{-- Email Address --}}
                                <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                                    <label class="control-label" for="email">{{ trans('bct/unionimpactbase::auth/form.email_user') }}</label>

                                    <input class="form-control" type="email" name="email" id="email" value="{{ Input::old('email') }}"
                                           placeholder="{{ trans('platform/users::auth/form.email_placeholder') }}"
                                           required
                                           autofocus
                                           data-parsley-trigger="change"
                                           data-parsley-error-message="{{ trans('platform/users::auth/form.email_error') }}">

                                        <span class="help-block">
                                            {{ Alert::onForm('email') ?: trans('platform/users::auth/form.email_help') }}
                                        </span>

                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- Password --}}
                                <div class="form-group{{ Alert::onForm('password', ' has-error') }}">

                                    <label class="control-label" for="password">{{ trans('platform/users::auth/form.password') }}</label>

                                    <input class="form-control" type="password" name="password" id="password"
                                           value="{{ Input::old('password') }}"
                                           placeholder="{{ trans('platform/users::auth/form.password_placeholder') }}"
                                           required
                                           data-parsley-trigger="change"
                                           data-parsley-minlength="6"
                                           data-parsley-error-message="{{ trans('platform/users::auth/form.password_error') }}">

                                        <span class="help-block">
                                            @if (Alert::onForm('password'))
                                                {{ Alert::onForm('password') }}

                                            @else
                                                {{ trans('platform/users::auth/form.password_help') }}
                                                <span class="text-warning">({{ trans('bct/unionimpactbase::pages/model.form.signup.password_help') }})</span>
                                            @endif
                                        </span>

                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- Confirm Password --}}
                                <div class="form-group{{ Alert::onForm('password_confirmation', ' has-error') }}">

                                    <label class="control-label"
                                           for="password_confirmation">{{ trans('platform/users::auth/form.password_confirmation') }}</label>

                                    <input class="form-control" type="password" name="password_confirmation" id="password_confirmation"
                                           value="{{ Input::old('password_confirmation') }}"
                                           placeholder="{{ trans('platform/users::auth/form.password_placeholder') }}"
                                           required
                                           data-parsley-trigger="change"
                                           data-parsley-equalto="#password"
                                           data-parsley-error-message="{{ trans('platform/users::auth/form.password_confirmation_error') }}">

                                        <span class="help-block">
                                            {{ Alert::onForm('password_confirmation') ?: trans('platform/users::auth/form.password_confirmation_help') }}
                                        </span>

                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">

                            <div class="col-md-4">
                                @include('bct/unionimpactbase::partials.forms.input', [
                                    'title' => trans('bct/unionimpactbase::general/form.address'),
                                    'field' => 'business_address',
                                ])
                            </div>
                            <div class="col-md-4">
                                @include('bct/unionimpactbase::partials.forms.input', [
                                    'title' => trans('bct/unionimpactbase::general/form.city'),
                                    'field' => 'business_city',
                                    'data_model' => 'city'
                                ])
                            </div>
                            <div class="col-md-4">
                                @include('bct/unionimpactbase::partials.forms.select', [
                                    'title' => trans('bct/unionimpactbase::general/form.state'),
                                    'field' => 'business_state',
                                    'select_array' => $states,
                                    'select_value' => 'code',
                                    'select_name' => 'state',
                                    'required' => true
                                ])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                @include('bct/unionimpactbase::partials.forms.input', [
                                    'title' => trans('bct/unionimpactbase::general/form.zipcode'),
                                    'field' => 'business_zipcode',
                                ])
                            </div>
                            <div class="col-md-4">
                                @include('bct/unionimpactbase::partials.forms.input', [
                                    'title' => trans('bct/unionimpactbase::general/form.phone'),
                                    'field' => 'business_phone',
                                ])
                            </div>
                        </div>

                        {{-- Form actions --}}
                        <div class="form-group">

                            <button class="btn btn-primary btn-block"
                                    type="submit">{{ trans('platform/users::auth/form.register.submit') }}
                            </button>

                        </div>

                    </div>

                </div>

            </form>

        </div>

    </div>

@stop
