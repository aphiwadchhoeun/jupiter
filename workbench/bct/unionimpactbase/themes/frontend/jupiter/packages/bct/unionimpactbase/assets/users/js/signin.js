/**
 * Created by Aphiwad on 2/24/2016.
 */
jQuery('document').ready(function ($) {

    function checkBrowser() {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) { // If Internet Explorer, return version number
            $('#browser-wanring-text').html('UnionImpact works great with Google Chrome. If you haven\'t installed it yet, please download <a href="https://www.google.com/chrome/browser/" target="_blank" class="text-danger"><i><ins>here</ins></i>.');
        }

        return false;
    }

    checkBrowser();

});