<div class="form-group{{ Alert::onForm($field, ' has-error') }}">

    <label for="{{ $field }}" class="control-label">
        {{ $title }}
    </label>

    <input type="text" class="form-control" name="{{ $field }}" id="{{ $field }}"
           autocomplete="off"
           placeholder="{{ trans('bct/unionimpactbase::general/common.placeholder.fill_this') }}{{ $title }}"

           @if (isset($data_model))
                data-model="{{ $data_model }}"
           @endif

           @if (isset($disabled) && $disabled === true)
                readonly
           @endif

           @if (isset($max_length))
                maxlength="{{ $max_length }}"
           @endif

           @if (isset($model))

               @if (isset($model_info))
                        value="{{ input()->old($model_info['name'] . '.' .$model_info['column'], $model->$model_info['column']) }}"
                    @else
                        value="{{ input()->old($field, $model->$field) }}"
               @endif

           @else
                value="{{ input()->old($field) }}"
           @endif
    >

    <span class="help-block">{{{ Alert::onForm($field) }}}</span>

</div>