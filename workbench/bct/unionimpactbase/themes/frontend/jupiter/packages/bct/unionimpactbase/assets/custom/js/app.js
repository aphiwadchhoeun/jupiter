/**
 * Created by romannebesnuy on 13.04.17.
 */
String.prototype.capitalize = function () {
    return this.toLowerCase().replace(/\b\w/g, function (m) {
        return m.toUpperCase();
    });
};

function showDirection() {
    $('.ranges').each(function () {
        if ($(this).find('.direction').length == 0) {
            $(this).append('<div class="direction text-bold" style="margin-top:10px;">Please, select the date range</div>');
        }
    });
}

$(function() {

    $.listen('parsley:field:success', function(parsleyField) {

        if (parsleyField.$element.hasClass('selectized')) {
            var check = _.filter(_.pluck(parsleyField.$element[0].attributes, 'name'), function(value) {
                return (value.indexOf('data-parsley') !== -1 && value !== 'data-parsley-id');
            });

            if (check.length > 0) {
                parsleyField.$element.parents('.form-group').removeClass('has-error-important').addClass('has-success-important');
                parsleyField.$element.parents('.form-group').find('.help-block').show();
            }
        }

    });

    $.listen('parsley:field:error', function(parsleyField) {

        if (parsleyField.$element.hasClass('selectized')) {
            var check = _.filter(_.pluck(parsleyField.$element[0].attributes, 'name'), function(value) {
                return (value.indexOf('data-parsley') !== -1 && value !== 'data-parsley-id');
            });

            if (check.length > 0) {
                parsleyField.$element.parents('.form-group').find('.help-block').addClass('hide');
                parsleyField.$element.parents('.form-group').removeClass('has-success-important').addClass('has-error-important');
            }
        }
    });

});

