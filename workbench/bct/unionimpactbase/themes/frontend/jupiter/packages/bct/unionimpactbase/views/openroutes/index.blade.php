@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('common.routes') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('map-css', 'bct/unionimpactbase::openroutes/css/index.css') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('index', 'bct/unionimpactbase::openroutes/js/index.js', 'platform') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
<script type="text/javascript" src="{{ URL::asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyBvReYkUqZuyu4HLSHepBRsGax30FnaWnI&libraries=places&language=en&callback=initialize') }}"></script>
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        <div class="panel-body">

            <div class="row">

                <div class="col-md-4">

                    <fieldset class="hidden-print">

                        <div class="form-group">

                            <label class="control-label">
                                {{{ trans('common.start_location') }}}
                            </label>

                            <input type="text" class="form-control openroute" id="start" value="">
                        </div>


                        <div class="form-group">
                            <label class="control-label">
                                {{{ trans('common.end_location') }}}
                            </label>
                            <input type="text" class="form-control openroute" id="finish" value="">
                        </div>

                        <div class="form-group">

                            <button class="btn btn-primary disabled" id="optimize">{{ trans('common.build_route') }}</button>

                        </div>

                    </fieldset>
                </div>

                <div class="col-md-8">

                    {{-- Form: General --}}
                    <fieldset>
                        <div class="form-group">
                            <div id="map-canvas"></div>
                        </div>
                    </fieldset>

                </div>

            </div>

        </div>

        <div class="panel-body">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>

        </div>

    </section>

@stop
