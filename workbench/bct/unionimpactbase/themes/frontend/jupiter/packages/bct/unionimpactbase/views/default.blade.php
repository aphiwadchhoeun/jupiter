@extends('layouts/default')

@section('scripts')
    @parent

    {{ Asset::queue('bct.uib.custom-js', 'bct/unionimpactbase::custom/js/app.js', ['jquery']) }}

    @include('bct/unionimpactbase::php_vars_to_js')
@endsection

@section('styles')
    @parent

    {{ Asset::queue('bct.uib.custom-css', 'bct/unionimpactbase::custom/css/app.css') }}
@endsection

@section('page')
    @parent

    <div class="ext-unionimpactbase">
        @yield('page')
    </div>

@stop