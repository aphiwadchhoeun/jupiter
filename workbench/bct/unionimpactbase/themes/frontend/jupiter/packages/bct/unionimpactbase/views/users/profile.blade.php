@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/unionimpactbase::user/common.title') }}
@stop
{{-- Queue assets --}}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page')
    @include('bct/unionimpactbase::users/partials/nav')

    <div class="col-md-4">

        <div class="panel panel-primary">
            <div class="panel-heading">{{{ trans("bct/unionimpactbase::user/common.title") }}}</div>

            <div class="panel-body">
                <div class="form-label">{{{ trans('platform/users::auth/form.email') }}}</div>
                <div class="form-value">{{ $currentUser->email }}</div>
                <div class="form-label">{{{ trans('platform/users::auth/form.password') }}}</div>
                <div class="form-value"><a href="{{ route('user.password_reminder') }}">Reset password</a></div>
            </div>
        </div>

    </div>

@stop
