@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    {{{ trans('bct/unionimpactbase::pages/common.titles.signup') }}} ::
    @parent
@stop

{{-- Queue Assets --}}
{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}

{{-- Inline Scripts --}}
@section('scripts')
    @parent
@stop

{{-- Page content --}}
@section('page')

    <div class="row">
        @include('bct/unionimpactbase::pages/forms/signup_form')
    </div>

@stop
