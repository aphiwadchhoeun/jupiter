var Extension;

;(function(window, document, $, undefined)
{

	'use strict';

	Extension = Extension || {
		Index: {},
	};

	// Initialize functions
	Extension.Index.init = function()
	{
		Extension.Index.dataGrid();
		Extension.Index.listeners();
	};

	// Add Listeners
	Extension.Index.listeners = function()
	{
		Platform.Cache.$body
			.on('click', '[data-grid-row] a', Extension.Index.titleClick)
		;
	};

	// Ignore row selection on title click
	Extension.Index.titleClick = function(event)
	{
		event.stopPropagation();
	};

	// Data Grid initialization
	Extension.Index.dataGrid = function()
	{
		var config = {
			//hash: false,
			callback: function()
			{
				$('[data-grid-checkbox-all]').prop('checked', false);
			}
		};

		Extension.Index.Grid = $.datagrid('exportqueue', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
	};

	// Job done, lets run.
	Extension.Index.init();

})(window, document, jQuery);
