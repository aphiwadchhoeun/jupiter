<script type="text/template" data-grid="exportqueue" data-template="filters">

	<% _.each(filters, function(f) { %>

		<button class="btn btn-default">

			<% if (f.col_mask !== undefined && f.val_mask !== undefined) { %>

				<%= f.col_mask %> <em><%= f.val_mask %></em>

			<% } else { %>

				<% if (f.column === 'all') { %>

					<%= f.value %>

				<% } else { %>

					<%= f.value %> in <em><%= f.column %></em>

				<% } %>

			<% } %>

			<span><i class="fa fa-times-circle"></i></span>

		</button>

	<% }); %>

</script>
