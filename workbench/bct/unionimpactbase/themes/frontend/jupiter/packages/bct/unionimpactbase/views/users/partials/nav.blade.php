<?php
    $mrpProfileUri = '#';
    if (app('extensions')->has('bct/mrp') && app('extensions')->get('bct/mrp')->isEnabled()) {
        $mrpProfileUri = '/mrp/profile';
    }
    if (app('extensions')->has('bct/mrp_pnw') && app('extensions')->get('bct/mrp_pnw')->isEnabled()) {
        $mrpPnwProfileUri = '/mrp_pnw/profile';
    }
    $memberProfileUri = '#';
    if (app('extensions')->has('bct/member') && app('extensions')->get('bct/member')->isEnabled()) {
        $memberProfileUri = '/mp/profile'; // uri link to member profile registration
    }
?>
{{ Asset::queue('profile-style', 'bct/unionimpactbase::users/css/profile.css') }}

<div class="row">
    <div class="col-md-12">
        <ul class="nav profile-nav nav-pills">
            <li class="{{ (Request::segment(1)=='ui')?'active':'' }}"><a href="/ui/profile">{{ trans('bct/unionimpactbase::user/common.title') }}</a></li>
            <li class="{{ (Request::segment(1)=='mrp')?'active':'' }} {{ $mrpProfileUri=='#'?'disabled':'' }}"><a href="{{ $mrpProfileUri }}">{{ trans('bct/unionimpactbase::user/common.mrp_user') }}</a></li>
            <li class="{{ (Request::segment(1)=='mrp_pnw')?'active':'' }} {{ $mrpPnwProfileUri=='#'?'disabled':'' }}"><a href="{{ $mrpPnwProfileUri }}">{{ trans('bct/unionimpactbase::user/common.mrp_user') }}</a></li>
            <li class="{{ (Request::segment(1)=='mp')?'active':'' }} {{ $memberProfileUri=='#'?'disabled':'' }}"><a href="{{ $memberProfileUri }}">{{ trans('bct/unionimpactbase::user/common.member') }}</a></li>
        </ul>
    </div>
</div>