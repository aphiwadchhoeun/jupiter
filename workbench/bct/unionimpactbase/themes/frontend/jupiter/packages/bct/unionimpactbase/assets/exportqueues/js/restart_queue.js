/**
 * Created by Aphiwad on 6/6/2016.
 */

jQuery('document').ready(function ($) {

    var restartQueue = $('#queue-restart');

    restartQueue.on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var self = this;

        swal({
            title: 'Are you sure you want to restart the Queue?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, restart it!',
            cancelButtonText: 'Cancel',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    toggleBtnState(self, false);

                    $.ajax({
                        url: url,
                        type: 'GET'
                    }).done(function (response) {
                        resolve(response);

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        swal(jqXHR.responseText, '', 'error');

                    }).always(function () {
                        toggleBtnState(self, true);

                    })
                });
            }
        }).then(function (response) {
            swal(response, '', 'success');
        });
    });

    function toggleBtnState(target, enable) {
        if (enable) {
            $(target).removeClass('disabled').find('i:first-child').removeClass('fa-spin');
        } else {
            $(target).addClass('disabled').find('i:first-child').addClass('fa-spin');
        }
    }

});