@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/unionimpactbase::pages/common.titles.signin') }}
@stop

@section('scripts')
    @parent

    {{ Asset::queue('signin-js', 'bct/unionimpactbase::users/js/signin.js', 'jquery') }}
@endsection


{{-- Page content --}}
@section('page')
    <div class="row">
        {!! $content !!}
    </div>

    <div class="row">
        @include('bct/unionimpactbase::pages/forms/signin_form')
    </div>
@stop