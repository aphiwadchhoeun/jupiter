@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/unionimpactbase::exportqueues/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}

{{ Asset::queue('restart-queue-js', 'bct/unionimpactbase::exportqueues/js/restart_queue.js', 'jquery') }}
{{ Asset::queue('index', 'bct/unionimpactbase::exportqueues/js/index.js', 'platform') }}
{{-- Queue assets --}}


{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page')


    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{ trans('bct/unionimpactbase::exportqueues/common.title') }}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="exportqueue" role="form">

                            <div class="input-group">
                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="name">{{ trans('bct/unionimpactbase::exportqueues/model.general.name') }}
                                        </option>
                                        <option value="source">{{ trans('bct/unionimpactbase::exportqueues/model.general.source') }}
                                        </option>
                                        <option value="ext">{{ trans('bct/unionimpactbase::exportqueues/model.general.ext') }}
                                        </option>
                                    </select>
                                </span>

                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">
                                    <span class="fa fa-search"></span>
                                </button>

								<button class="btn btn-default" data-grid="exportqueue" data-reset>
                                    <i class="fa fa-refresh fa-sm"></i>
                                </button>

							</span>

                            </div>

                        </form>

                        @if($currentUser->hasAnyAccess(['superuser', 'queue.restart']))
                            <div class="pull-right">
                                <a href="{{ route('bct.unionimpactbase.queue.restart') }}"
                                   id="queue-restart"
                                   class="btn btn-danger"
                                   data-toggle="tooltip"
                                   data-original-title="{{ trans('bct/unionimpactbase::exportqueues/common.restart') }}">
                                    <i class="fa fa-exclamation-circle"></i></a>
                            </div>
                        @endif

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="exportqueue"></div>

            </div>

            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover"
                       data-source="{{ route('bct.unionimpactbase.export.queue.grid') }}"
                       data-grid="exportqueue">
                    <thead>
                    <tr>
                        <th class="sortable col-md-4"
                            data-sort="name">{{ trans('bct/unionimpactbase::exportqueues/model.general.name') }}</th>
                        <th class="sortable col-md-3"
                            data-sort="source">{{ trans('bct/unionimpactbase::exportqueues/model.general.source') }}</th>
                        <th class="sortable col-md-1"
                            data-sort="ext">{{ trans('bct/unionimpactbase::exportqueues/model.general.ext') }}</th>
                        <th class="sortable col-md-1"
                            data-sort="status">{{ trans('bct/unionimpactbase::exportqueues/model.general.status') }}</th>
                        <th class="sortable col-md-2"
                            data-sort="created_at">{{ trans('bct/unionimpactbase::exportqueues/model.general.created_at') }}</th>
                        <th class="col-md-1">{{{ trans('bct/unionimpactbase::exportqueues/model.general.download') }}}
                        </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="exportqueue"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/unionimpactbase::exportqueues.grid.results')
        @include('bct/unionimpactbase::exportqueues.grid.pagination')
        @include('bct/unionimpactbase::exportqueues.grid.filters')
        @include('bct/unionimpactbase::exportqueues.grid.no_results')

    </section>

@stop
