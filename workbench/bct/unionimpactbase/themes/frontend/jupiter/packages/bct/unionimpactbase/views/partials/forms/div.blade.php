<div class="form-group{{ Alert::onForm($field, ' has-error') }}">

    <label for="{{ $field }}" class="control-label">
        {{ $title }}
    </label>

    <div>{{{ input()->old($field, $model->$field) }}}</div>

    <span class="help-block">{{{ Alert::onForm($field) }}}</span>

</div>