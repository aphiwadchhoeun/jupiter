<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Dev Alert!</title>
    </head>
    <body>
        <p>Something went wrong at {{ url()->full() }}. Go check it out now!</p>
        <p>Current timestamp: {{ date('M-d-Y H:i:s') }}</p>
        <br/>
        <hr/>
        <p>Source: {{ $source }}</p>
        <p>Log:</p>
        <p>{!! print_r($data) !!}</p>
    </body>
</html>
