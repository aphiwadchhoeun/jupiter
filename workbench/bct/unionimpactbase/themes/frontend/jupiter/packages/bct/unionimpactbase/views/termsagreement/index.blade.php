@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/unionimpactbase::termsagreement/common.title') }}
@stop

{{-- Page content --}}
@section('page')
    <div class="row">

        <div class="row">
            <h1 class="text-center">{{ trans('bct/unionimpactbase::termsagreement/common.title') }}</h1>

            <iframe src="https://docs.google.com/document/d/1Evaxjrockqip4Pr4azE0adflt21Kj95lcoYTFy_KkB8/pub?embedded=true"
                    style="width: 70%; min-height: 800px; display: block; margin: 0 auto;"></iframe>
        </div>

        <div class="row">

            <br/>
            <form action="{{ route('bct.terms.agreements') }}" role="form" method="post" accept-char="UTF-8">
                {{-- CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-md-2 col-md-offset-4">
                    <label for="agree" style="font-size: 24pt">
                        <input type="checkbox" id="agree" name="agree" style="width: 30px; height: 30px;"> I agree.
                    </label>
                </div>

                <div class="col-md-2">
                    <input type="submit" class="btn btn-lg btn-primary" value="Submit">
                </div>
            </form>
        </div>
    </div>
@stop