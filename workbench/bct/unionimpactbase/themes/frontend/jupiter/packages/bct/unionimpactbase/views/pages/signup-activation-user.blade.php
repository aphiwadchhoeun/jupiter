@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    {{{ trans('bct/unionimpactbase::pages/common.titles.activation') }}} ::
    @parent
@stop

{{-- Page content --}}
@section('page')

    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <div class="panel panel-default">

                <div class="panel-body">

                    {!! $content !!}

                </div>

            </div>

        </div>

    </div>

@stop
