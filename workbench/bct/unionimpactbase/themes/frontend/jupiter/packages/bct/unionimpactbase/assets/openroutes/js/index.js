jQuery('document').ready(function ($) {


    var $inputs = $('#start,#finish');
    var $build_button = $('#optimize');

    if ($('#start').val() !== '' && $('#finish').val() !== '') {
        $build_button.removeClass('disabled');
    }

    $inputs.on('change', function () {
        if ($('#start').val() !== '' && $('#finish').val() !== '') {
            $build_button.removeClass('disabled');
        }
        else {
            $build_button.addClass('disabled');
        }
    });

    $('#optimize').on('click', function (e) {
        calcRoute();
    });


});


var geocoder, searchBoxStart, searchBoxFinish, map, markerStart, markerFinish, directionsDisplay, layer, zip_layer, county_layer, limit = 8, markers = {}, windows = {}, sublayers = [];
var directionsService, areaInfoWindow,
    is_optimize = false;
var default_lat = (typeof BctUnionimpactbase.default_lat !== 'undefined') ? BctUnionimpactbase.default_lat : '',
    default_lng = (typeof BctUnionimpactbase.default_lng !== 'undefined') ? BctUnionimpactbase.default_lng : '';

/** for google map */

function initialize() {
    geocoder = new google.maps.Geocoder();

    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer;
    areaInfoWindow = new google.maps.InfoWindow();

    var mapOptions = {
        zoom: 10
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    var markerStartOptions = {
        title: 'Start position',
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
    };

    var markerFinishOptions = {
        title: 'Finish position',
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
    };

    markerStart = new google.maps.Marker(markerStartOptions);
    markerFinish = new google.maps.Marker(markerFinishOptions);

    directionsDisplay.setMap(map);

    // Try HTML5 geolocation
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            markerStartOptions.pos = new google.maps.LatLng(position.coords.latitude,
                position.coords.longitude);

            handleNoGeolocation(false);

        }, function () {
            handleNoGeolocation(true);
        });
    } else {
        handleNoGeolocation(false);
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag) {
            map.setCenter(new google.maps.LatLng(default_lat, default_lng));
        } else {
            map.setCenter(markerStartOptions.pos);
            markerStart.setPosition(markerStartOptions.pos);
            getAddress(markerStart.getPosition(), $('#start'));
        }
    }

    searchBoxStart = new google.maps.places.SearchBox(document.getElementById('start'));
    searchBoxFinish = new google.maps.places.SearchBox(document.getElementById('finish'));

    google.maps.event.addDomListener(searchBoxStart, "places_changed", function () {
        var places = searchBoxStart.getPlaces();
        var i, place;

        for (i = 0; place = places[i]; i++) {
            markerStart.setPosition(place.geometry.location);
        }

    });

    google.maps.event.addDomListener(searchBoxFinish, "places_changed", function () {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var places = searchBoxFinish.getPlaces();

    });

    google.maps.event.addDomListener(markerStart, 'dragend', function () {
        calcRoute();
        getAddress(markerStart.getPosition(), $('#start'));
    });

    google.maps.event.addDomListener(markerFinish, 'dragend', function () {
        calcRoute();
        getAddress(markerFinish.getPosition(), $('#finish'));
    });


    google.maps.event.addDomListener(map, "click", function () {
        areaInfoWindow.close();
    })
}

function calcRoute() {
    directionsService.route({
        origin: document.getElementById('start').value,
        destination: document.getElementById('finish').value,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            initAccordion(response);
        } else {
            swal({
                title: "Some problems with your route! Please, check your waypoints!",
                type: "warning",
                confirmButtonColor: "#019964",
                confirmButtonText: "Ok!"
            });
        }
    });
}

function getAddress(position, input) {
    geocoder.geocode({
        latLng: position
    }, function (responses) {
        if (responses && responses.length > 0) {
            /** set address data to inputs */
            input.val(responses[0].formatted_address);
        } else {
            /** clear inputs */
            input.val('');
        }
    });
}





