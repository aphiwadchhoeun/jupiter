<script type="text/template" data-grid="exportqueue" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>

        <td><%- r.name %></td>
        <td><%- r.source %></td>
        <td><span class="badge"><%- r.ext.toUpperCase() %></span></td>
        <td>
            <% if (r.status > 0) { %>
                <span class="label label-primary">Ready</span>
            <% } else if (r.status < 0) { %>
                <span class="label label-danger">Failed</span>
            <% } else { %>
                <span class="label label-warning">Processing</span>
            <% } %>
        </td>
        <td><%- r.created %></td>
        <td>
            <% if(r.status) { %>
                <a href="<%- r.download_uri %>" class="btn btn-primary btn-circle contractor-details" data-toggle="tooltip"
                   data-placement="right"
                   data-original-title="{{{ trans('bct/unionimpactbase::exportqueues/model.general.download') }}}"><i
                            class="fa fa-download fa-fw"></i></a>
            <% } %>
        </td>
    </tr>

    <% }); %>

</script>
