/**
 * Created by roman on 28.03.16.
 */

$(function () {
    var submit_btn = $('#submit-btn'),
        main_form = $('#content-form'),
        state = $('#business_state');

    $('input[name="business_phone"]').mask('(999) 999-9999? x999');
    $('input[name="business_zipcode"]').mask("99999?-9999");

    state.selectize();

    submit_btn.click(function (e) {
        e.preventDefault();

        main_form.submit();
    });

});