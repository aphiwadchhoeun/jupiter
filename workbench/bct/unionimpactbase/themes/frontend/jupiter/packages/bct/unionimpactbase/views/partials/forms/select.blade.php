<div class="form-group{{ Alert::onForm($field, ' has-error') }}">

    <label for="{{ $field }}" class="control-label">
        {{ $title }}
        <i class="fa fa-info-circle" data-toggle="popover"
           data-content="{{ trans('bct/unionimpactbase::general/common.placeholder.fill_this') }}{{ $title }}"></i>
    </label>

    @if (isset($select_array))
        <select name="{{ $field }}" id="{{ $field }}" class="form-control"
            @if (isset($required))
                required
            @endif
        >
            <option value=""></option>
            @foreach ($select_array as $item)
                <option value="{{ $item[$select_value] }}"
                    @if (isset($model))
                        @if (input()->old($field, $model->$field) == $item[$select_value])
                            selected
                        @endif
                    @else
                        @if (input()->old($field) == $item[$select_value])
                            selected
                        @endif
                    @endif


                >
                    {{ $item[$select_name] }}
                </option>
            @endforeach
        </select>
    @endif

    <span class="help-block">{{{ Alert::onForm($field) }}}</span>

</div>