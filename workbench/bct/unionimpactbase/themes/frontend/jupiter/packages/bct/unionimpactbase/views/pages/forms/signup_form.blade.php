<div class="col-md-6 col-md-offset-3">

        <div class="panel panel-default">

            <div class="panel-heading">{{ trans('platform/users::auth/form.register.legend') }}</div>

            <div class="panel-body">

                {{-- Role --}}
                <div class="form-group">

                    <label class="control-label"
                           for="role">{{ trans('bct/unionimpactbase::pages/model.form.signup.role') }}</label>

                    <div class="row row-register">
                        @if (isExtensionEnabled('bct/mrp') || isExtensionEnabled('bct/mrp_pnw'))
                            <div class="col-md-6">
                                <a href="{{ route('bct.register.contractor.mrp') }}"
                                   class="btn btn-primary btn-block">{{ trans('bct/unionimpactbase::user/common.mrp_contractor') }}</A>
                            </div>
                        @endif


                        @if (isExtensionEnabled('bct/dispatch') && !isExtensionEnabled('bct/dispatch_janitors'))
                            <div class="col-md-6">
                                <a href="{{ route('bct.register.contractor.dp') }}"
                                   class="btn btn-primary btn-block">{{ trans('bct/unionimpactbase::user/common.dispatch_contractor') }}</a>
                            </div>
                        @elseif (!isExtensionEnabled('bct/dispatch') && isExtensionEnabled('bct/dispatch_janitors'))
                            <div class="col-md-6">
                                <a href="{{ route('bct.register.contractor.dpj') }}"
                                   class="btn btn-primary btn-block">{{ trans('bct/unionimpactbase::user/common.dispatch_contractor') }}</a>
                            </div>
                        @endif

                        @if (isExtensionEnabled('bct/member'))
                            <div class="col-md-6">
                                <a href="{{ route('bct.register.member.mp') }}"
                                   class="btn btn-primary btn-block">{{ trans('bct/unionimpactbase::user/common.mp_member') }}</a>
                            </div>
                        @endif

                        @if (isExtensionEnabled('bct/dispatch_theater'))
                            <div class="col-md-6">
                                <a href="{{ route('bct.register.member.dpth') }}"
                                   class="btn btn-primary btn-block">{{ trans('bct/unionimpactbase::general/common.member') }}</a>
                            </div>
                        @endif
                    </div>




                </div>

                <hr>

                {{-- Form actions --}}
                <div class="form-group">

                    <p class="help-block text-center">Already have an Account? <a
                                href="{{ route('bct.signin') }}">{{ trans('platform/users::auth/form.login.legend') }}</a>
                    </p>

                </div>

            </div>

        </div>

</div>