@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/unionimpactbase::pages/common.titles.policies') }}
@stop

{{-- Page content --}}
@section('page')
    <div class="row">
        <div class="row">
            <div class="col-md-12">
                {!! $content !!}
            </div>
        </div>

        <div class="row">
            <iframe src="https://docs.google.com/document/d/1-SSnEhrRRm14ULGfJdhbCJ_oyj7kgsczl8FNOfGT_Es/pub?embedded=true"
                    style="width: 70%; min-height: 800px; display: block; margin: 0 auto;"></iframe>
        </div>
    </div>
@stop