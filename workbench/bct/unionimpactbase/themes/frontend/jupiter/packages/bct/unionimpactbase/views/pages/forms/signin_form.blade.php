{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}

<div class="col-md-6 col-md-offset-3">

    <form action="{{{ route('bct.signin') }}}" id="login-form" role="form" method="post" accept-char="UTF-8"
          autocomplete="off" data-parsley-validate>

        {{-- CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="panel panel-default">

            <div class="panel-body">

                {{-- Email Address --}}
                <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                    <label class="control-label"
                           for="email">{{{ trans('platform/users::auth/form.email') }}}</label>

                    <input class="form-control" type="email" name="email" id="email"
                           value="{{{ Input::old('email') }}}"
                           placeholder="{{{ trans('platform/users::auth/form.email_placeholder') }}}"
                           required
                           autofocus
                           data-parsley-trigger="change"
                           data-parsley-error-message="{{{ trans('platform/users::auth/form.email_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('email') ?: trans('platform/users::auth/form.email_help') }}}
						</span>

                </div>

                {{-- Password --}}
                <div class="form-group{{ Alert::onForm('password', ' has-error') }}">

                    <label class="control-label"
                           for="password">{{{ trans('platform/users::auth/form.password') }}}</label>

                    <input class="form-control" type="password" name="password" id="password"
                           placeholder="{{{ trans('platform/users::auth/form.password_placeholder') }}}"
                           required
                           data-parsley-trigger="change"
                           data-parsley-error-message="{{{ trans('platform/users::auth/form.password_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('password') ?: trans('platform/users::auth/form.password_help') }}}
						</span>

                </div>

                {{-- Remember me --}}
                <div class="form-group">

                    <label for="remember">

                        <input type="checkbox" name="remember" id="remember"
                               value="1"{{ Input::old('remember') ? ' checked="checked"' : null }} />

                        {{{ trans('platform/users::auth/form.login.remember-me') }}}

                    </label>

                    <p class="text-right">Recover your
                        <a href="{{ url('reminder') }}">{{ trans('platform/users::auth/form.login.forgot-password') }}</a></p>

                </div>

                <hr>

                {{-- Form actions --}}
                <div class="form-group">

                    <button class="btn btn-primary btn-block"
                            id="submit-btn"
                            type="submit">{{{ trans('platform/users::auth/form.login.submit') }}}
                    </button>

                </div>

                <hr>

                <div class="form-group">
                    <p class="help-block text-center">Or</p>

                    <a class="btn btn-warning btn-block" id="register" name="register" href="{{ url('register') }}">{{ trans('bct/unionimpactbase::pages/common.signin.btn_signup') }}</a>
                </div>

            </div>

        </div>

        <span id="browser-wanring-text" class="text-center text-danger"></span>

    </form>

</div>