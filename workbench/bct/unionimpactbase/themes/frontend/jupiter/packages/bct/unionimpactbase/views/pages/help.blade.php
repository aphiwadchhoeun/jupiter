@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
    Help
@stop


{{-- Page content --}}
@section('page')
    <div class="row">
        <div class="row">
            <div class="col-md-12">
                {!! $content !!}
            </div>
        </div>
    </div>
@stop