@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
@stop
{{-- Queue assets --}}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
    <style type="text/css">
        .transparent, .transparent:hover, .transparent:active {
            cursor: text;
            color: #333;
            text-decoration: none;
        }
    </style>
@stop

{{-- Page --}}
@section('page')

    <div class="panel panel-default">
        <div class="panel-body">
            <h1><a href="{{ route('bct.unionimpactbase.do.flush.caches') }}" class="transparent">404</a>: Page not
                found!</h1>

            <span class="lead">We're sorry, the page you're trying to access is not found.</span>
        </div>
    </div>

@stop
