/**
 * Created by sky on 28.12.16.
 */
var ExtensionBase;

;
(function(window, document, $, undefined) {

    'use strict';

    ExtensionBase = {
        Index: {

        },
    };

    // Date range picker initialization
    ExtensionBase.Index.datePicker = function(ExtensionParent) {
        var startDate, endDate, config, filter,
            $datagrid_form = $('form[data-grid="' + ExtensionParent.Index.grid + '"]'),
            $datagrid_calendar = $('[data-grid="' + ExtensionParent.Index.grid + '"] [data-grid-calendar]'),
            $datagrid_calendar_column_filter = $datagrid_calendar.data('range-filter');

        var filters = _.compact(
            String(window.location.hash.slice(3)).split('/').splice(2)
        );

        config = {
            opens: 'right',
            showDropdowns: true
        };

        _.each(filters, function (route) {
            filter = route.split(':');

            if (filter[0] === 'created_at' && filter[1] !== undefined && filter[2] !== undefined) {
                startDate = moment(filter[1]);

                endDate = moment(filter[2]);
            }
        });



        if (startDate && endDate) {
            config = {
                startDate: startDate,
                endDate: endDate,
                opens: 'right'
            };
        }

        ExtensionBase.Index.createDateRangeElementIfNotExist(ExtensionParent, $datagrid_form, $datagrid_calendar_column_filter);

        var $daterange_start_date = $('[data-grid="' + ExtensionParent.Index.grid + '"] [data-range-start]'),
            $daterange_end_date = $('[data-grid="' + ExtensionParent.Index.grid + '"] [data-range-end]');

        ExtensionParent.Index.datePicker = $datagrid_calendar.daterangepicker(config, function (start, end, label) {
        });

        ExtensionParent.Index.datePicker.on('apply.daterangepicker', function (ev, picker) {
            $daterange_start_date.val(picker.startDate.format('YYYY-MM-DD'));
            $daterange_end_date.val(picker.endDate.format('YYYY-MM-DD'));
            $daterange_start_date.trigger('change');
        });
    };

    ExtensionBase.Index.createDateRangeElementIfNotExist = function(ExtensionParent, $datagrid_form, $datagrid_calendar_column_filter) {
        if ($('[data-grid="' + ExtensionParent.Index.grid + '"] [data-range-start]').length === 0) {
            $datagrid_form.append('<input type="hidden" data-range-filter="' + $datagrid_calendar_column_filter + '" data-format="YYYY-MM-DD" data-range-start></button>');
        }

        if ($('[data-grid="' + ExtensionParent.Index.grid + '"] [data-range-end]').length === 0) {
            $datagrid_form.append('<input type="hidden" data-range-filter="' + $datagrid_calendar_column_filter + '" data-format="YYYY-MM-DD" data-range-end></button>');
        }
    };

})(window, document, jQuery);
