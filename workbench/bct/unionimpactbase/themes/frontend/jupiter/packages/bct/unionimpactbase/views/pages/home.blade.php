@extends('bct/unionimpactbase::default')

{{-- Page title --}}
@section('title')
    @parent
@stop

{{-- Page content --}}
@section('page')
    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    {!! $content !!}
                </div>
            </div>
        </div>
    </div>
@stop