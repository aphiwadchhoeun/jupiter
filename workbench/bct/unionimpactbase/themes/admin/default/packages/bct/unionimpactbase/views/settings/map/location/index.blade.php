@extends('layouts/default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans('bct/unionimpactbase::admin/whitelist/common.title') }}}
@stop

{{ Asset::queue('custom-css', 'bct/unionimpactbase::admin/settings/css/index.css') }}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{ Asset::queue('admin-ui-whitelist-settings', 'bct/unionimpactbase::admin/settings/whitelist/js/index.js', 'jquery') }}

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop


{{-- Page content --}}
@section('page')
    <section class="panel panel-default panel-tabs">

        {{-- Form --}}
        <form id="whitelist-form" action="{{ request()->fullUrl() }}" role="form" method="post"
              accept-char="UTF-8" autocomplete="off" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#actions">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <span class="navbar-brand">{{ trans('bct/unionimpactbase::admin/map/location/common.title') }}</span>
                        </div>

                        {{-- Form: Actions --}}
                        <div class="collapse navbar-collapse" id="actions">

                            <ul class="nav navbar-nav navbar-right">

                                <li>
                                    <button class="btn btn-primary navbar-btn" data-toggle="tooltip"
                                            data-original-title="{{ trans('action.save') }}">
                                        <i class="fa fa-save"></i> <span
                                                class="visible-xs-inline">{{ trans('action.save') }}</span>
                                    </button>
                                </li>

                            </ul>

                        </div>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <div class="settings-panel">

                    <div class="row">

                        <div class="col-md-12">

                            <label for="map_location_default_lat">{{ trans('bct/unionimpactbase::admin/map/location/model.enter_default_lat') }}</label>

                            <input type="text" name="map_location_default_lat" id="map_location_default_lat" class="form-control" value="{{ config('bct.unionimpactbase.map.location.default.lat') }}">

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <label for="map_location_default_lng">{{ trans('bct/unionimpactbase::admin/map/location/model.enter_default_lng') }}</label>

                            <input type="text" name="map_location_default_lng" id="map_location_default_lng" class="form-control" value="{{ config('bct.unionimpactbase.map.location.default.lng') }}">

                        </div>

                    </div>

                </div>

            </div>

        </form>

    </section>

@stop