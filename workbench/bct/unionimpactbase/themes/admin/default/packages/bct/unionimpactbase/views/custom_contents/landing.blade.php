@extends('layouts/default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans('bct/unionimpactbase::admin/common.title') }}}
@stop

{{ Asset::queue('custom-content-css', 'bct/unionimpactbase::admin/css/admin.css') }}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop


{{-- Page content --}}
@section('page')
    <section class="panel panel-default panel-tabs">

        {{-- Form --}}
        <form id="custom-content-form" action="{{ request()->fullUrl() }}" role="form" method="post"
              accept-char="UTF-8" autocomplete="off" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#actions">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <span class="navbar-brand"><small>{{ trans('bct/unionimpactbase::admin/common.pages.landing_home_page') }}</small></span>
                        </div>

                        {{-- Form: Actions --}}
                        <div class="collapse navbar-collapse" id="actions">

                            <ul class="nav navbar-nav navbar-right">

                                <li>
                                    <button class="btn btn-primary navbar-btn" data-toggle="tooltip"
                                            data-original-title="{{ trans('action.save') }}">
                                        <i class="fa fa-save"></i> <span
                                                class="visible-xs-inline">{{ trans('action.save') }}</span>
                                    </button>
                                </li>

                            </ul>

                        </div>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <fieldset>

                    <div class="col-md-12">

                        {{-- Name --}}
                        <div class="form-group{{ Alert::onForm('value', ' has-error') }}">

                            <label for="name" class="control-label">
                                {{{ trans('bct/unionimpactbase::admin/model.value') }}}
                            </label>

                            <textarea class="form-control h400" name="value" id="value" required autofocus
                                      data-parsley-trigger="change">{!! input()->old('value', $content) !!}</textarea>

                            <span class="help-block">{{{ Alert::onForm('value') }}}</span>

                        </div>

                    </div>

                </fieldset>

            </div>

        </form>

    </section>
@stop