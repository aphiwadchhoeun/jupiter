@extends('layouts/default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans('bct/unionimpactbase::admin/general/common.title') }}}
@stop

{{ Asset::queue('custom-css', 'bct/unionimpactbase::admin/settings/css/index.css') }}

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop


{{-- Page content --}}
@section('page')
    <section class="panel panel-default panel-tabs">

        {{-- Form --}}
        <form id="cartodb-form" action="{{ request()->fullUrl() }}" role="form" method="post"
              accept-char="UTF-8" autocomplete="off" enctype="multipart/form-data" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#actions">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <span class="navbar-brand">{{ trans('bct/unionimpactbase::admin/general/common.title') }}</span>
                        </div>

                        {{-- Form: Actions --}}
                        <div class="collapse navbar-collapse" id="actions">

                            <ul class="nav navbar-nav navbar-right">

                                <li>
                                    <button class="btn btn-primary navbar-btn" data-toggle="tooltip"
                                            data-original-title="{{ trans('action.save') }}">
                                        <i class="fa fa-save"></i> <span
                                                class="visible-xs-inline">{{ trans('action.save') }}</span>
                                    </button>
                                </li>

                            </ul>

                        </div>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <div class="settings-panel">

                    <div class="row">

                        <div class="col-md-12">

                            <table class="table table-striped">

                                <tr>
                                    <td>
                                        <label for="app_logo">{{ trans('bct/unionimpactbase::admin/general/model.app_logo') }}</label>
                                    </td>
                                    <td>
                                        <input type="file" name="app_logo" id="app_logo" class="form-control">
                                        <img src="{{ getLogo() }}"
                                             alt="@setting('platform.app.title')" height="120px">
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>{{ trans('bct/unionimpactbase::admin/general/model.show_policy') }}</label>
                                    </td>
                                    <td>
                                        <label class="radio-inline"><input type="radio" name="show_policy"
                                                                           value="1" {{ @config('bct.unionimpactbase.show_policy', 1)?'checked':'' }}>
                                            Yes</label>
                                        <label class="radio-inline"><input type="radio" name="show_policy"
                                                                           value="0" {{ @config('bct.unionimpactbase.show_policy', 1)?'':'checked' }}>
                                            No</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>{{ trans('bct/unionimpactbase::admin/general/model.show_help') }}</label>
                                    </td>
                                    <td>
                                        <label class="radio-inline"><input type="radio" name="show_help"
                                                                           value="1" {{ @config('bct.unionimpactbase.show_help', 1)?'checked':'' }}>
                                            Yes</label>
                                        <label class="radio-inline"><input type="radio" name="show_help"
                                                                           value="0" {{ @config('bct.unionimpactbase.show_help', 1)?'':'checked' }}>
                                            No</label>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <hr>

                    <div class="lead">API</div>

                    <div class="row">

                        <div class="col-md-12">

                            <table class="table table-striped">

                                <tr>
                                    <td>
                                        <label for="cartodb_base_api_url">{{ trans('bct/unionimpactbase::admin/general/model.cartodb_base_api_url') }}</label>
                                    </td>
                                    <td>
                                        <input type="text" name="cartodb_base_api_url" id="cartodb_base_api_url"
                                               class="form-control"
                                               value="{{ @config('bct.unionimpactbase.cartodb.cartodb_base_api_url', 'https://alexbct.cartodb.com:443/api/v2') }}">
                                        <strong>{{ trans('bct/unionimpactbase::admin/general/model.default_cartodb_base_api_url') }}</strong>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label for="cartodb_table">{{ trans('bct/unionimpactbase::admin/general/model.cartodb_table') }}</label>
                                    </td>
                                    <td>
                                        <input type="text" name="cartodb_table" id="cartodb_table" class="form-control"
                                               value="{{ @config('bct.unionimpactbase.cartodb.regions_table', 'dispatch_areas_pnwrcc') }}">
                                        <strong>{{ trans('bct/unionimpactbase::admin/general/model.default_cartodb_table') }}</strong>
                                    </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </form>

    </section>

@stop