/**
 * Created by Aphiwad on 8/24/2016.
 */
jQuery('document').ready(function ($) {

    var whitelist_role = $('#whitelist_role');
    var whitelist_clients = $('#whitelist_clients');

    whitelist_role.on('change', function (e) {
        e.preventDefault();

        loadWhitelistClients(getApiHref());
    });

    function loadWhitelistClients(href) {
        whitelist_clients.prop('disabled', true);
        $.get(href)
            .done(function (data) {
                whitelist_clients.val(data);
            })
            .always(function () {
                whitelist_clients.prop('disabled', false);
            });
    }

    function getApiHref() {
        return whitelist_role.data('api').replace(encodeURI('{slug}'), whitelist_role.val());
    }

    loadWhitelistClients(getApiHref());

});