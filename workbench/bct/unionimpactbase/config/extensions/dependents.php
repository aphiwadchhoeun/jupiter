<?php
/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 23.03.17
 * Time: 14:24
 */

return [

    'bct/member' => [
        'bct/member_proa',
        'bct/member_uaw2865',
        'bct/member_seiu87'
    ],

    'bct/payment' => [
        'bct/payment_proa',
        'bct/payment_uaw2865',
        'bct/payment_seiu87'
    ]

];