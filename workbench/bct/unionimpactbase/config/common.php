<?php
/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 03.04.17
 * Time: 15:14
 */

return [
    'queue_name' => env('QUEUE_NAME_COMMON', 'queue_jobs')
];