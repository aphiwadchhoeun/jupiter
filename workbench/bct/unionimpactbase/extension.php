<?php

use Cartalyst\Extensions\ExtensionInterface;
use Cartalyst\Permissions\Container as Permissions;
use Cartalyst\Settings\Repository as Settings;
use Illuminate\Foundation\Application;

return [

    /*
    |--------------------------------------------------------------------------
    | Name
    |--------------------------------------------------------------------------
    |
    | This is your extension name and it is only required for
    | presentational purposes.
    |
    */

    'name' => 'Unionimpactbase',

    /*
    |--------------------------------------------------------------------------
    | Slug
    |--------------------------------------------------------------------------
    |
    | This is your extension unique identifier and should not be changed as
    | it will be recognized as a new extension.
    |
    | Ideally, this should match the folder structure within the extensions
    | folder, but this is completely optional.
    |
    */

    'slug' => 'bct/unionimpactbase',

    /*
    |--------------------------------------------------------------------------
    | Author
    |--------------------------------------------------------------------------
    |
    | Because everybody deserves credit for their work, right?
    |
    */

    'author' => 'Aphiwad Chhoeun',

    /*
    |--------------------------------------------------------------------------
    | Description
    |--------------------------------------------------------------------------
    |
    | One or two sentences describing the extension for users to view when
    | they are installing the extension.
    |
    */

    'description' => 'Base models for Union Impact',

    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | Version should be a string that can be used with version_compare().
    | This is how the extensions versions are compared.
    |
    */

    'version' => '0.1.0',

    /*
    |--------------------------------------------------------------------------
    | Requirements
    |--------------------------------------------------------------------------
    |
    | List here all the extensions that this extension requires to work.
    | This is used in conjunction with composer, so you should put the
    | same extension dependencies on your main composer.json require
    | key, so that they get resolved using composer, however you
    | can use without composer, at which point you'll have to
    | ensure that the required extensions are available.
    |
    */

    'require' => [
        'platform/pages',
        'platform/users'
    ],

    /*
    |--------------------------------------------------------------------------
    | Autoload Logic
    |--------------------------------------------------------------------------
    |
    | You can define here your extension autoloading logic, it may either
    | be 'composer', 'platform' or a 'Closure'.
    |
    | If composer is defined, your composer.json file specifies the autoloading
    | logic.
    |
    | If platform is defined, your extension receives convetion autoloading
    | based on the Platform standards.
    |
    | If a Closure is defined, it should take two parameters as defined
    | bellow:
    |
    |	object \Composer\Autoload\ClassLoader      $loader
    |	object \Illuminate\Foundation\Application  $app
    |
    | Supported: "composer", "platform", "Closure"
    |
    */

    'autoload' => 'composer',

    /*
    |--------------------------------------------------------------------------
    | Service Providers
    |--------------------------------------------------------------------------
    |
    | Define your extension service providers here. They will be dynamically
    | registered without having to include them in app/config/app.php.
    |
    */

    'providers' => [
        'Bct\Unionimpactbase\Providers\MigrationUtilServiceProvider',
        'Bct\Unionimpactbase\Providers\FlowServiceProvider',
        'Bct\Unionimpactbase\Providers\ContractorServiceProvider',
        'Bct\Unionimpactbase\Providers\MemberServiceProvider',
        'Bct\Unionimpactbase\Providers\ExportQueueProvider',
        'Bct\Unionimpactbase\Providers\SeedUtilServiceProvider',
        'Bct\Unionimpactbase\Providers\ThrottleServiceProvider',
        'Bct\Unionimpactbase\Providers\JavaScriptServiceProvider',
        'Bct\Unionimpactbase\Providers\SnappyServiceProvider',
        'Bct\Unionimpactbase\Providers\ConfigServiceProvider',
        'Bct\Unionimpactbase\Providers\UrlShortenerServiceProvider'
    ],

    /*
    |--------------------------------------------------------------------------
    | Routes
    |--------------------------------------------------------------------------
    |
    | Closure that is called when the extension is started. You can register
    | any custom routing logic here.
    |
    | The closure parameters are:
    |
    |	object \Cartalyst\Extensions\ExtensionInterface  $extension
    |	object \Illuminate\Foundation\Application        $app
    |
    */

    'routes' => function (ExtensionInterface $extension, Application $app) {

        if (!$app->routesAreCached()) {
            Route::get('terms-agreement', ['as' => 'bct.terms.agreements', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\TermsAgreementController@showTermsAgreement']);
            Route::post('terms-agreement', ['as' => 'bct.terms.agreements', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\TermsAgreementController@processTermsAgreement']);

            /*
            * OPEN ROUTES
            */
            Route::get('/routes', ['as' => 'bct.openroutes', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\OpenRoutesController@index']);
            Route::get('/maps', ['as' => 'bct.openroutes', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\OpenRoutesController@index']);


            /*
             * Admin sections
             */
            Route::group([
                'prefix'    => admin_uri() . '/bct/ccontent',
                'namespace' => 'Bct\Unionimpactbase\Controllers\Admin',
            ], function () {
                Route::get('signin', ['as' => 'admin.bct.ccontent.signin', 'uses' => 'CustomContentController@showSignInPage']);
                Route::post('signin', ['as' => 'admin.bct.ccontent.signin', 'uses' => 'CustomContentController@submitSignInPage']);

                Route::get('confirmemail', ['as' => 'admin.bct.ccontent.confirmemail', 'uses' => 'CustomContentController@showActivationPage']);
                Route::post('confirmemail', ['as' => 'admin.bct.ccontent.confirmemail', 'uses' => 'CustomContentController@submitActivationPage']);

                Route::get('landing', ['as' => 'admin.bct.ccontent.landing', 'uses' => 'CustomContentController@showLandingPage']);
                Route::post('landing', ['as' => 'admin.bct.ccontent.landing', 'uses' => 'CustomContentController@submitLandingPage']);

                Route::get('policy', ['as' => 'admin.bct.ccontent.policy', 'uses' => 'CustomContentController@showPolicyPage']);
                Route::post('policy', ['as' => 'admin.bct.ccontent.policy', 'uses' => 'CustomContentController@submitPolicyPage']);

                Route::get('youtube', ['as' => 'admin.bct.ccontent.youtube', 'uses' => 'CustomContentController@showYoutubePage']);
                Route::post('youtube', ['as' => 'admin.bct.ccontent.youtube', 'uses' => 'CustomContentController@submitYoutubePage']);

                Route::get('landing/contractor', ['as' => 'admin.bct.ccontent.landing.contractor', 'uses' => 'CustomContentController@showLandingPageContractor']);
                Route::post('landing/contractor', ['as' => 'admin.bct.ccontent.landing.contractor', 'uses' => 'CustomContentController@submitLandingPageContractor']);

                Route::get('kiosk/terms', ['as' => 'admin.bct.ccontent.kiosk.terms', 'uses' => 'CustomContentController@showKioskTermsPage']);
                Route::post('kiosk/terms', ['as' => 'admin.bct.ccontent.kiosk.terms', 'uses' => 'CustomContentController@submitKioskTermsPage']);

                Route::get('error/token', ['as' => 'admin.bct.ccontent.error.token', 'uses' => 'CustomContentController@showTokenErrorPage']);
                Route::post('error/token', ['as' => 'admin.bct.ccontent.error.token', 'uses' => 'CustomContentController@submitTokenErrorPage']);
            });

            /*
             * Admin Sections - UnionImpact Settings
             */
            Route::group([
                'prefix'    => admin_uri(),
                'namespace' => 'Bct\Unionimpactbase\Controllers\Admin\Settings',
            ], function () {
                Route::get('whitelist', ['as' => 'admin.whitelist', 'uses' => 'WhitelistSettingsController@showWhitelist']);
                Route::post('whitelist', ['as' => 'admin.whitelist', 'uses' => 'WhitelistSettingsController@processWhitelist']);
                Route::get('whitelist/get/clients/{slug}', ['as' => 'admin.whitelist.get.clients', 'uses' => 'WhitelistSettingsController@getWhitelistClient']);

                Route::get('blacklist', ['as' => 'admin.blacklist', 'uses' => 'BlacklistSettingsController@showBlacklist']);
                Route::post('blacklist', ['as' => 'admin.blacklist', 'uses' => 'BlacklistSettingsController@processBlacklist']);

                Route::get('map-location', ['as' => 'admin.map.location', 'uses' => 'MapSettingsController@showLocation']);
                Route::post('map-location', ['as' => 'admin.map.location', 'uses' => 'MapSettingsController@processLocation']);

                Route::get('general', ['as' => 'admin.general', 'uses' => 'GeneralSettingsController@showSettings']);
                Route::post('general', ['as' => 'admin.general', 'uses' => 'GeneralSettingsController@processSettings']);
            });

            /*
             * Overrides default routes
             */
            Route::get('/', ['as' => 'bct.home', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showLandingPage']);
            Route::get('login', ['as' => 'bct.signin', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showSignInPage']);
            Route::post('login', ['as' => 'bct.signin', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@processLogin']);
            Route::get('policy', ['as' => 'bct.policy', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showPolicyPage']);
            Route::get('help', ['as' => 'bct.help', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showHelpPage']);
            Route::get('{id}/reminder', ['as' => 'bct.password_reminder', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\ReminderController@showFormWithEmail']);

            // Overrides default routes
            // Password reminder
            Route::get('reminder', ['as' => 'user.password_reminder', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\ReminderController@index']);
            Route::post('reminder/{id}/{code}', ['as' => 'user.password_reminder.confirm', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\ReminderController@processConfirmation']);

            /*
             * TODO: handling with custom error message
                        Route::post('reminder', ['as' => 'user.password_reminder', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\ReminderController@process']);
            */

            Route::get('register', ['as' => 'bct.signup', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showSignUpPage']);
            Route::get('register/contractor/mrp', ['as' => 'bct.register.contractor.mrp', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showRegisterMRPContractorPage']);
            Route::post('register/contractor/mrp', ['as' => 'bct.register.contractor.mrp', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@processRegistrationMRPContractor']);

            Route::get('register/contractor/dp', ['as' => 'bct.register.contractor.dp', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showRegisterDPContractorPage']);
            Route::post('register/contractor/dp', ['as' => 'bct.register.contractor.dp', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@processRegistrationDPContractor']);

            Route::get('register/contractor/dpj', ['as' => 'bct.register.contractor.dpj', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showRegisterDPJContractorPage']);
            Route::post('register/contractor/dpj', ['as' => 'bct.register.contractor.dpj', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@processRegistrationDPJContractor']);

            Route::get('register/member/mp', ['as' => 'bct.register.member.mp', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showRegisterMPMemberPage']);
            Route::post('register/member/mp', ['as' => 'bct.register.member.mp', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@processRegistrationMPMember']);

	        Route::get('register/member/dpth', ['as' => 'bct.register.member.dpth', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@showRegisterDPTHMemberPage']);
	        Route::post('register/member/dpth', ['as' => 'bct.register.member.dpth', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@processRegistrationDPTHMember']);


	        // Account activation
            Route::get('activate/{id}/{code}', ['as' => 'user.activate', 'uses' => 'Bct\Unionimpactbase\Controllers\Frontend\PagesController@activate']);

            /*
             * Export Queue
             */
            Route::group([
                'prefix'    => 'ui/queue',
                'namespace' => 'Bct\Unionimpactbase\Controllers\Frontend'
            ], function () {
                Route::get('dashboard', ['as' => 'bct.unionimpactbase.export.queue.dashboard', 'uses' => 'ExportQueuesController@index']);

                Route::get('grid', ['as' => 'bct.unionimpactbase.export.queue.grid', 'uses' => 'ExportQueuesController@grid']);

                Route::get('{id}/download', ['as' => 'bct.unionimpactbase.export.queue.download', 'uses' => 'ExportQueuesController@download']);

                Route::get('update/{hash}', ['as' => 'bct.unionimpactbase.export.queue.update', 'uses' => 'ExportQueuesController@update']);
            });

            /*
             * General Routes
             */
            Route::group([
                'prefix'    => 'ui',
                'namespace' => 'Bct\Unionimpactbase\Controllers\Frontend',
            ], function () {
                Route::get('profile', ['as' => 'bct.unionimpactbase.users.profile', 'uses' => 'UsersController@profile']);
                Route::post('users/activate/{id}', ['as' => 'bct.unionimpactbase.users.activate', 'uses' => 'UsersController@activate']);

                // secret page
                Route::get('flushcaches', ['as' => 'bct.unionimpactbase.flush.caches', 'uses' => 'SecretController@showSecret']);
                Route::get('do/flushcaches', ['as' => 'bct.unionimpactbase.do.flush.caches', 'uses' => 'SecretController@flushCaches']);

                Route::post('filter/get', ['as' => 'bct.unionimpactbase.filter.get', 'uses' => 'DataGridController@filterGet']);
                Route::post('filter/save', ['as' => 'bct.unionimpactbase.filter.save', 'uses' => 'DataGridController@processFilterSave']);

                Route::get('queue/restart', ['as' => 'bct.unionimpactbase.queue.restart', 'uses' => 'QueueController@restartQueue']);

                Route::get('api/icon', ['as' => 'global.api.icon', 'uses' => 'GeneralApiController@getIcon']);
                Route::get('api/cartodb/configs/get', ['as' => 'global.api.cartodb.configs.get', 'uses' => 'GeneralApiController@getCartodbConfigs']);
            });
        }

    },

    /*
    |--------------------------------------------------------------------------
    | Database Seeds
    |--------------------------------------------------------------------------
    |
    | Platform provides a very simple way to seed your database with test
    | data using seed classes. All seed classes should be stored on the
    | `database/seeds` directory within your extension folder.
    |
    | The order you register your seed classes on the array below
    | matters, as they will be ran in the exact same order.
    |
    | The seeds array should follow the following structure:
    |
    |	Vendor\Namespace\Database\Seeds\FooSeeder
    |	Vendor\Namespace\Database\Seeds\BarSeeder
    |
    */

    'seeds' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | Permissions
    |--------------------------------------------------------------------------
    |
    | Register here all the permissions that this extension has. These will
    | be shown in the user management area to build a graphical interface
    | where permissions can be selected to allow or deny user access.
    |
    | For detailed instructions on how to register the permissions, please
    | refer to the following url https://cartalyst.com/manual/permissions
    |
    */

    'permissions' => function (Permissions $permissions) {
        $permissions->group('Queue', function ($g) {
            $g->name = 'Queue Manager';

            /*
             * Can restart queue
             */
            $g->permission('queue.restart', function ($p) {
                $p->label = trans('bct/unionimpactbase::general/permissions.queue.restart');

                $p->controller('Bct\Unionimpactbase\Controllers\Frontend\QueueController', 'restartQueue');
            });

        });
    },

    /*
    |--------------------------------------------------------------------------
    | Widgets
    |--------------------------------------------------------------------------
    |
    | Closure that is called when the extension is started. You can register
    | all your custom widgets here. Of course, Platform will guess the
    | widget class for you, this is just for custom widgets or if you
    | do not wish to make a new class for a very small widget.
    |
    */

    'widgets' => function () {

    },

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    |
    | Register any settings for your extension. You can also configure
    | the namespace and group that a setting belongs to.
    |
    */

    'settings' => function (Settings $settings, Application $app) {
    },

    /*
    |--------------------------------------------------------------------------
    | Menus
    |--------------------------------------------------------------------------
    |
    | You may specify the default various menu hierarchy for your extension.
    | You can provide a recursive array of menu children and their children.
    | These will be created upon installation, synchronized upon upgrading
    | and removed upon uninstallation.
    |
    | Menu children are automatically put at the end of the menu for extensions
    | installed through the Operations extension.
    |
    | The default order (for extensions installed initially) can be
    | found by editing app/config/platform.php.
    |
    */

    'menus' => [
        'admin' => [
            [
                'slug'     => 'admin-bct-ccontent',
                'name'     => 'Custom Content',
                'class'    => 'fa fa-code',
                'uri'      => 'bct/ccontent',
                'regex'    => '/:admin\/bct\/ccontent/i',
                'children' => [

                    [
                        'slug'  => 'admin-bct-ccontent-signin',
                        'name'  => 'Sign In Page',
                        'uri'   => 'bct/ccontent/signin',
                        'regex' => '/:admin\/bct\/ccontent\/signin/i',
                    ],

                    [
                        'slug'  => 'admin-bct-ccontent-confirmemail',
                        'name'  => 'Email Confirm Page',
                        'uri'   => 'bct/ccontent/confirmemail',
                        'regex' => '/:admin\/bct\/ccontent\/confirmemail/i',
                    ],

                    [
                        'slug'  => 'admin-bct-ccontent-landing',
                        'name'  => 'Landing Home Page',
                        'uri'   => 'bct/ccontent/landing',
                        'regex' => '/:admin\/bct\/ccontent\/landing$/i',
                    ],

                    [
                        'slug'  => 'admin-bct-ccontent-policy',
                        'name'  => 'Policies Page',
                        'uri'   => 'bct/ccontent/policy',
                        'regex' => '/:admin\/bct\/ccontent\/policy/i',
                    ],

                    [
                        'slug'  => 'admin-bct-ccontent-youtube',
                        'name'  => 'Youtube Tutorials Page',
                        'uri'   => 'bct/ccontent/youtube',
                        'regex' => '/:admin\/bct\/ccontent\/youtube/i',
                    ],

                    [
                        'slug'  => 'admin-bct-ccontent-landing-contractor',
                        'name'  => 'Landing Contractor Page',
                        'uri'   => 'bct/ccontent/landing/contractor',
                        'regex' => '/:admin\/bct\/ccontent\/landing\/contractor$/i',
                    ],

                    [
                        'slug'  => 'admin-bct-ccontent-kiosk-terms',
                        'name'  => 'Kiosk Terms Page',
                        'uri'   => 'bct/ccontent/kiosk/terms',
                        'regex' => '/:admin\/bct\/ccontent\/kiosk\/terms/i',
                    ],

                    [
                        'slug'  => 'admin-bct-ccontent-token-error',
                        'name'  => 'Token Error Page',
                        'uri'   => 'bct/ccontent/error/token',
                        'regex' => '/:admin\/bct\/ccontent\/error\/token/i',
                    ],

                ],
            ],
            [
                'slug'     => 'admin-unionimpact',
                'name'     => 'Union Impact',
                'class'    => 'fa fa-user-secret',
                'uri'      => 'unionimpact',
                'regex'    => '/:admin\/(whitelist)|(blacklist)|(general)|(map-location)/i',
                'children' => [
                    [
                        'slug'  => 'admin-unionimpact-general',
                        'name'  => 'General',
                        'class' => 'fa fa-gears',
                        'uri'   => 'general',
                        'regex' => '/:admin\/general/i',
                    ],
                    [
                        'slug'  => 'admin-unionimpact-whitelist',
                        'name'  => 'Whitelist',
                        'class' => 'fa fa-bell',
                        'uri'   => 'whitelist',
                        'regex' => '/:admin\/whitelist/i',
                    ],
                    [
                        'slug'  => 'admin-unionimpact-blacklist',
                        'name'  => 'Blacklist',
                        'class' => 'fa fa-ban',
                        'uri'   => 'blacklist',
                        'regex' => '/:admin\/blacklist/i',
                    ],
                    [
                        'slug'  => 'admin-unionimpact-map-location',
                        'name'  => 'Map/Location',
                        'class' => 'fa fa-map-marker',
                        'uri'   => 'map-location',
                        'regex' => '/:admin\/map-location/i',
                    ],
                ]
            ]
        ],

        'uibase-menu' => [
            [
                'slug'       => 'uibase-menu-export-queue',
                'name'       => 'Export',
                'class'      => 'fa fa-download fa-fw',
                'uri'        => 'ui/queue/dashboard',
                'regex'      => '/ui\/queue/i',
                'visibility' => 'logged_in',
            ]
        ],

    ],

];
