<?php namespace Bct\Unionimpactbase\Validator\User;

use Cartalyst\Support\Validator;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/17/2015
 * Time: 3:35 PM
 */
class UserOverrideValidator extends Validator implements UserOverrideValidatorInterface {

    protected $messages = [
        'password.regex' => 'Password must has at least one lowercase letter, one uppercase letter, and one digit.'
    ];

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'email'    => 'required|email|unique:users',
        'password' => 'min:6',
    ];

    /**
     * {@inheritDoc}
     */
    public function onCreate() {
        $this->rules['password'] = 'required|min:6';
    }

    /**
     * {@inheritDoc}
     */
    public function onUpdate() {
        $this->rules['email'] .= ',email,{email},email|sometimes';

        $this->rules['password'] = 'min:6|confirmed';
    }

    /**
     * {@inheritDoc}
     */
    public function onRegister() {
        $this->rules['password'] = 'required|min:6|regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/|confirmed';
    }

    /**
     * {@inheritDoc}
     */
    public function onReminder() {
        $this->rules = [
            'password' => 'required|min:6|regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/|confirmed'
        ];
    }
}