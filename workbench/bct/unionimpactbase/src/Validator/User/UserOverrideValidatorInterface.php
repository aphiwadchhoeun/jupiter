<?php namespace Bct\Unionimpactbase\Validator\User;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/17/2015
 * Time: 3:36 PM
 */
interface UserOverrideValidatorInterface {
    /**
     * Creating a user scenario.
     *
     * @return void
     */
    public function onCreate();

    /**
     * Updating a user scenario.
     *
     * @return void
     */
    public function onUpdate();

    /**
     * Registering a user scenario.
     *
     * @return void
     */
    public function onRegister();

    /**
     * Resetting a user password scenario.
     *
     * @return void
     */
    public function onReminder();
}