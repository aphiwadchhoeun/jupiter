<?php namespace Bct\Unionimpactbase\Validator\ExportQueue;

interface ExportQueueValidatorInterface {

	/**
	 * Updating a pdfqueue scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
