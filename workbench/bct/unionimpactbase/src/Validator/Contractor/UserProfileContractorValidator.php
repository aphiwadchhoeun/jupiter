<?php namespace Bct\Unionimpactbase\Validator\Contractor;

    /**
 * Created by PhpStorm.
 * User: roman
 * Date: 25.12.15
 * Time: 13:30
 */

use Cartalyst\Support\Validator;

class UserProfileContractorValidator extends Validator
{

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'business_name' => 'required',
        'business_address' => 'required',
        'business_city' => 'required',
        'business_state' => 'required',
        'business_zipcode' => 'required',
        'business_phone' => 'required',
        'business_license' => 'required',
        'business_ein' => 'required',
    ];


    /**
     * {@inheritDoc}
     */
    public function onUpdate()
    {

    }


    protected $messages = [
    ];

}
