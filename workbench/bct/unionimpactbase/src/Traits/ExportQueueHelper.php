<?php namespace Bct\Unionimpactbase\Traits;

use Bct\Unionimpactbase\Models\ExportQueue;
use Illuminate\Support\Facades\Log;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/15/2016
 * Time: 1:49 PM
 */
trait ExportQueueHelper {

    protected $export;

    /**
     * Create export entry
     *
     * @param $filename
     * @param $type
     * @param $options
     */
    protected function createExportEntry($filename, $type, $options) {
        switch ($type) {
            case 'pdf':
            case 'pdf-details':
            case 'pdf-field-reports':
                $ext = 'pdf';
                $mime = 'application/pdf';
                break;
            case 'csv':
                $ext = 'csv';
                $mime = 'text/csv';
                break;
            case 'xls':
                $ext = 'xls';
                $mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                break;
            default:
                $ext = 'txt';
                $mime = 'text/plain';
                break;
        };


        // create job entity
        $eq = ExportQueue::create([
            'name'       => $filename,
            'source'     => array_get($options, 'export_source', ''),
            'ext'        => $ext,
            'mime'       => $mime,
            'created_by' => array_get($options, 'user_id', 0),
            'status'     => 0
        ]);

        $hash = hash('sha256', implode('', [$eq->id, $eq->name, $eq->created_at]));
        $eq->hash = $hash;
        $eq->save();
        $this->options['callback_url'] = route('bct.unionimpactbase.export.queue.update', $hash);
    }

}