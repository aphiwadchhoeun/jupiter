<?php namespace Bct\Unionimpactbase\Traits;

/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 23.03.17
 * Time: 17:10
 */

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

trait QueueErrorLog {


    /**
     * Create export entry
     *
     * @param $queue_table
     * @param $queue_id
     * @param $message
     */
    protected function errorLog($queue_table = '', $queue_id = 0, $message = '') {
        DB::table('queue_error_log')->insert([
            'queue_table' => $queue_table,
            'queue_id'    => $queue_id,
            'message'     => $message,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);
    }

}