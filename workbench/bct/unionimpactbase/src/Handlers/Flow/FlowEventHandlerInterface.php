<?php namespace Bct\Unionimpactbase\Handlers\Flow;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/19/2015
 * Time: 11:02 AM
 */

use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface FlowEventHandlerInterface extends BaseEventHandlerInterface {

    /**
     * Event authenticating with specified email and password
     *
     * @param $email
     * @param $password
     * @return mixed
     */
    public function authenticating($email, $password);

    /**
     * Event user logged in
     *
     * @param UserInterface $user
     */
    public function loggedIn(UserInterface $user);

    /**
     * Event user logged out
     *
     * @param UserInterface $user
     */
    public function loggedOut(UserInterface $user);

    /**
     * Event request for password reset
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function reminder(UserInterface $user);


    /**
     * Event user registered
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function registered(UserInterface $user);

}