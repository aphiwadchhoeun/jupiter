<?php namespace Bct\Unionimpactbase\Handlers\Flow;

use Bct\Unionimpactbase\Models\AuthLog;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Request;
use Jenssegers\Agent\Facades\Agent;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/19/2015
 * Time: 11:02 AM
 */
class FlowEventHandler extends BaseEventHandler implements FlowEventHandlerInterface {

    /**
     * FlowEventHandler constructor.
     * @param Container $app
     */
    function __construct(Container $app) {
        parent::__construct($app);
    }

    /**
     * Registers the event listeners using the given dispatcher instance.
     *
     * @param  \Illuminate\Events\Dispatcher $dispatcher
     * @return void
     */
    public function subscribe(Dispatcher $dispatcher) {
        $dispatcher->listen('platform.user.reminder', __CLASS__ . '@reminder');
        $dispatcher->listen('sentinel.authenticating', __CLASS__ . '@authenticating');
        $dispatcher->listen('platform.user.logged_in', __CLASS__ . '@loggedIn');
        $dispatcher->listen('platform.user.logged_out', __CLASS__ . '@loggedOut');
        $dispatcher->listen('platform.user.registered', __CLASS__ . '@registered');
    }

    /**
     * Event authenticating with specified email and password
     *
     * @param $email
     * @param $password
     * @return mixed
     */
    public function authenticating($email, $password) {
        $auth = Sentinel::getUserRepository();
        $credentials = [
            'email'    => $email,
            'password' => $password
        ];

        $user = $auth->findByCredentials($credentials);

        $valid = $user !== null ? $auth->validateCredentials($user, $credentials) : false;

        if ($user === null || $valid === false) {
            AuthLog::create([
                'user_email'      => data_get($credentials, 'email'),
                'user_name'       => trim(implode(' ', [data_get($user, 'first_name'), data_get($user, 'last_name')])),
                'ip'              => Request::ip(),
                'browser'         => Agent::browser(),
                'browser_version' => Agent::version(Agent::browser()),
                'platform'        => Agent::platform(),
                'action'          => 'LOGIN',
                'success'         => false
            ]);
        }
    }

    /**
     * Event user logged in
     *
     * @param UserInterface $user
     */
    public function loggedIn(UserInterface $user) {
        AuthLog::create([
            'user_email'      => data_get($user, 'email'),
            'user_name'       => trim(implode(' ', [data_get($user, 'first_name'), data_get($user, 'last_name')])),
            'ip'              => Request::ip(),
            'browser'         => Agent::browser(),
            'browser_version' => Agent::version(Agent::browser()),
            'platform'        => Agent::platform(),
            'action'          => 'LOGIN',
            'success'         => true
        ]);
    }

    /**
     * Event user logged out
     *
     * @param UserInterface $user
     */
    public function loggedOut(UserInterface $user) {
        // TODO: define what to do when user logged out
    }

    /**
     * Event request for password reset
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function reminder(UserInterface $user) {
        AuthLog::create([
            'user_email'      => data_get($user, 'email'),
            'user_name'       => trim(implode(' ', [data_get($user, 'first_name'), data_get($user, 'last_name')])),
            'ip'              => Request::ip(),
            'browser'         => Agent::browser(),
            'browser_version' => Agent::version(Agent::browser()),
            'platform'        => Agent::platform(),
            'action'          => 'PASSWORD_RESET',
            'success'         => true
        ]);
    }

    /**
     * Event user registered
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function registered(UserInterface $user) {
        AuthLog::create([
            'user_email'      => data_get($user, 'email'),
            'user_name'       => trim(implode(' ', [data_get($user, 'first_name'), data_get($user, 'last_name')])),
            'ip'              => Request::ip(),
            'browser'         => Agent::browser(),
            'browser_version' => Agent::version(Agent::browser()),
            'platform'        => Agent::platform(),
            'action'          => 'REGISTERED',
            'success'         => true
        ]);
    }


}