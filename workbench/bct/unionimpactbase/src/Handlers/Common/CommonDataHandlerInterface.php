<?php namespace Bct\Unionimpactbase\Handlers\Common;

/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 12.04.17
 * Time: 19:07
 */

interface CommonDataHandlerInterface
{

    /**
     * Prepares the given data for being stored.
     *
     * @param  array  $data
     * @return mixed
     */
    public function prepare(array $data);
}