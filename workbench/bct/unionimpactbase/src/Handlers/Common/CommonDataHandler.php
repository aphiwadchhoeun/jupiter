<?php namespace Bct\Unionimpactbase\Handlers\Common;
/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 12.04.17
 * Time: 19:05
 */

class CommonDataHandler
{
    /**
     * {@inheritDoc}
     */
    public function prepare(array $data)
    {
        return $this->cleanXSS($data);
    }

    protected function cleanXSS($array)
    {
        // Recursive cleaning for array [] inputs, not just strings.
        return $this->arrayStripTags($array);
    }

    protected function arrayStripTags($array)
    {
        $result = array();

        foreach ($array as $key => $value) {
            // Don't allow tags on key either, maybe useful for dynamic forms.
            $key = strip_tags($key);

            // If the value is an array, we will just recurse back into the
            // function to keep stripping the tags out of the array,
            // otherwise we will set the stripped value.
            if (is_array($value)) {
                $result[$key] = static::arrayStripTags($value);
            } elseif (!is_object($value)) {
                // I am using strip_tags(), you may use htmlentities(),
                // also I am doing trim() here, you may remove it, if you wish.
                $result[$key] = trim(strip_tags($value));
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Check if input data has checkboxes and return 1 or 0
     *
     * @param array      $data
     * @param bool|false $input_checkboxes
     *
     * @return bool
     */
    protected function checkForCheckbox(array &$data, $input_checkboxes = false)
    {
        if ( ! empty( $data ) && $input_checkboxes !== false && is_array($input_checkboxes)) {
            $keys_data = array_keys($data);
            foreach ($input_checkboxes as $checkbox) {
                if (in_array($checkbox, $keys_data)) {
                    $data [$checkbox] = ( $data [$checkbox] === 'on' ) ? 1 : 0;
                } else {
                    $data [$checkbox] = 0;
                }
            }
        }

        return false;
    }
}