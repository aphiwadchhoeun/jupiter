<?php namespace Bct\Unionimpactbase\Handlers\Common;

/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 23.03.17
 * Time: 15:26
 */

use Illuminate\Events\Dispatcher;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use DB;

class CommonEventHandler extends BaseEventHandler implements CommonEventHandlerInterface {

    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher)
    {
        $dispatcher->listen('cache:cleared', __CLASS__.'@cacheCleared');

    }

    /**
     * {@inheritDoc}
     */
    public function cacheCleared()
    {
        $config_bct_extensions_dependents = collect(config('bct.extensions.dependents'));

        if (!$config_bct_extensions_dependents->isEmpty()) {
            foreach ($config_bct_extensions_dependents as $extension_slug => $extension_dependents) {

                $extension_is_checked_for_route = false;
                $extension_config_route_name = 'bct.route.enabled.' . str_replace('bct/', '', $extension_slug);

                foreach ($extension_dependents as $extension_dependent_slug) {

                    $extension_dependent_db = DB::table('extensions')
                        ->select('enabled')
                        ->where('slug', $extension_dependent_slug)
                        ->first();

                    if ($extension_dependent_db && (int) $extension_dependent_db->enabled === 1) {
                        $extension_is_checked_for_route = true;

                        app('cache')->forever($extension_config_route_name, false);
                        break;
                    }

                }

                if ($extension_is_checked_for_route === false) {
                    app('cache')->forever($extension_config_route_name, true);
                }
            }
        }
    }

}
