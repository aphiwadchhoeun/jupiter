<?php namespace Bct\Unionimpactbase\Handlers\Common;

/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 23.03.17
 * Time: 15:26
 */


use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface CommonEventHandlerInterface extends BaseEventHandlerInterface {

    /**
     * When a cache:clear'ed.
     *
     * @param  array  $data
     * @return mixed
     */
    public function cacheCleared();

}
