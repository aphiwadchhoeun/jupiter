<?php namespace Bct\Unionimpactbase\Permissions;

use Cartalyst\Sentinel\Permissions\PermissionsInterface;
use Cartalyst\Sentinel\Permissions\PermissionsTrait;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 10/21/2015
 * Time: 12:19 PM
 */
class OptimisticPermissions implements PermissionsInterface {

    use PermissionsTrait;

    /**
     * {@inheritDoc}
     */
    protected function createPreparedPermissions() {
        $prepared = [];

        if (!empty($this->secondaryPermissions)) {
            foreach ($this->secondaryPermissions as $permissions) {
                $this->mergePermissions($prepared, $permissions);
            }
        }

        if (!empty($this->permissions)) {
            $permissions = [];

            $this->mergePermissions($permissions, $this->permissions);

            $prepared = array_merge($prepared, $permissions);
        }

        return $prepared;
    }

    private function mergePermissions(array &$prepared, array $permissions) {
        foreach ($permissions as $keys => $value) {
            foreach ($this->extractClassPermissions($keys) as $key) {
                // If the value is not in the array, we're opting in
                if (!array_key_exists($key, $prepared)) {
                    $prepared[$key] = $value;

                    continue;
                }

                // If our value is in the array and equals false, it will override
                if ($value === true) {
                    $prepared[$key] = $value;
                }
            }
        }
    }

}