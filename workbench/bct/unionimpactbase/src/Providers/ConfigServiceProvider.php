<?php namespace Bct\Unionimpactbase\Providers;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 07.12.16
 * Time: 15:46
 */


use Cartalyst\Support\ServiceProvider;
use Request;
use URL;

class ConfigServiceProvider extends ServiceProvider
{
    protected $extension_alias = 'bct.uib';
    protected $extension_slug = 'bct/unionimpactbase';

    public function boot() {
        // Subscribe the registered event handler
        $this->app['events']->subscribe('bct.unionimpactbase.common.handler.event');
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->registerExtensionAlias();
        $this->registerExtensionSlug();

        $this->mergeConfigFrom(base_path().'/workbench/bct/unionimpactbase/config/extensions/dependents.php', 'bct.extensions.dependents');
        $this->mergeConfigFrom(base_path().'/workbench/bct/unionimpactbase/config/common.php', get_extension_alias__uib() . '.common');

        $this->bindIf('bct.unionimpactbase.common.handler.event', 'Bct\Unionimpactbase\Handlers\Common\CommonEventHandler');

    }

    protected function registerExtensionAlias()
    {
        $this->app->singleton('extension.alias.uib', function(){
            return $this->getExtensionAlias();
        });
    }

    protected function registerExtensionSlug()
    {
        $this->app->singleton('extension.slug.uib', function(){
            return $this->getExtensionSlug();
        });
    }

    protected function getExtensionAlias()
    {
        return $this->extension_alias;
    }

    protected function getExtensionSlug()
    {
        return $this->extension_slug;
    }
}
