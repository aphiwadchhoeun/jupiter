<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/19/2015
 * Time: 11:01 AM
 */

namespace Bct\Unionimpactbase\Providers;

use Bct\Unionimpactbase\Console\ClearAssets;
use Bct\Unionimpactbase\Console\ClearThrottle;
use Bct\Unionimpactbase\Console\ExtensionInstall;
use Bct\Unionimpactbase\Console\ExtensionMenuInstall;
use Bct\Unionimpactbase\Console\ExtensionMigrate;
use Bct\Unionimpactbase\Console\ExtensionSeed;
use Bct\Unionimpactbase\Console\ExtensionUninstall;
use Bct\Unionimpactbase\Console\FixAddressMember;
use Bct\Unionimpactbase\Console\UnionImpactInstall;
use Bct\Unionimpactbase\Installer\UIInstaller;
use Cartalyst\Support\ServiceProvider;

class FlowServiceProvider extends ServiceProvider {

    public function boot() {
        // Subscribe the registered event handler
        $this->app['events']->subscribe('bct.unionimpactbase.flow.handler.event');

        $this->app['platform.users.validator'] = $this->app->share(function ($app) {
            return $app['bct.unionimpactbase.user.validator'];
        });
    }

    public function register() {
        // Register the event handler
        $this->bindIf('bct.unionimpactbase.flow.handler.event', 'Bct\Unionimpactbase\Handlers\Flow\FlowEventHandler');

        // Register the validator
        $this->bindIf('bct.unionimpactbase.user.validator', 'Bct\Unionimpactbase\Validator\User\UserOverrideValidator');

        // replace permissions middleware handling request from ajax
        $this->app['router']->middleware('permissions', 'Bct\Unionimpactbase\Middleware\Permissions');

        // Register dev mailer
        $this->bindIf('bct.unionimpactbase.dev.mailer', 'Bct\Unionimpactbase\Mailer\Dev\DevAlert');

        $this->registerInstaller();

        $this->registerCommands();

        $this->commands('command.ui.install');
        $this->commands('command.throttle.clear');
        $this->commands('command.ui.migrate');
        $this->commands('command.assets.clear');
        $this->commands('command.ui.menu.install'); // deprecated
        $this->commands('command.ui.extension.install');
        $this->commands('command.ui.extension.uninstall');
        $this->commands('command.ui.extension.seed');
        $this->commands('command.ui.fix.address.member');
    }

    /**
     * Registers the installer.
     *
     * @return void
     */
    protected function registerInstaller() {
        $this->app['ui.installer'] = $this->app->share(function ($app) {
            return new UIInstaller($app, $app['platform.installer.repository']);
        });

        $this->app->alias('ui.installer', 'Platform\Installer\Installer');
    }

    /**
     * Registers the install command.
     *
     * @return void
     */
    protected function registerCommands() {
        $this->app['command.ui.install'] = $this->app->share(function ($app) {
            return new UnionImpactInstall($app['ui.installer']);
        });

        $this->app['command.throttle.clear'] = $this->app->share(function ($app) {
            return new ClearThrottle();
        });

        $this->app['command.ui.migrate'] = $this->app->share(function ($app) {
            return new ExtensionMigrate($app['migrator']);
        });

        $this->app['command.assets.clear'] = $this->app->share(function ($app) {
            return new ClearAssets($app['theme.assets']);
        });

        // deprecated
        $this->app['command.ui.menu.install'] = $this->app->share(function ($app) {
            return new ExtensionMenuInstall();
        });

        $this->app['command.ui.extension.install'] = $this->app->share(function ($app) {
            return new ExtensionInstall();
        });

        $this->app['command.ui.extension.uninstall'] = $this->app->share(function ($app) {
            return new ExtensionUninstall();
        });

        $this->app['command.ui.extension.seed'] = $this->app->share(function ($app) {
            return new ExtensionSeed();
        });

        $this->app['command.ui.fix.address.member'] = $this->app->share(function ($app) {
            return new FixAddressMember();
        });
    }
}