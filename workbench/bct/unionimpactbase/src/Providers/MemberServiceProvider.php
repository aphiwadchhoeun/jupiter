<?php namespace Bct\Unionimpactbase\Providers;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 23.08.16
 * Time: 16:50
 */

use Bct\Unionimpactbase\Repositories\Member\MemberRepository;
use Bct\Unionimpactbase\Repositories\Member\MemberRepositoryInterface;
use Cartalyst\Support\ServiceProvider;

class MemberServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;


    /**
     * {@inheritDoc}
     */
    public function register()
    {
        // Register the repository
        //$this->bindIf('bct.unionimpactbase.member', 'Bct\Unionimpactbase\Repositories\Member\MemberRepository');
        $this->app->bind(MemberRepositoryInterface::class, MemberRepository::class, true);
        $this->app->alias(MemberRepositoryInterface::class, 'bct.unionimpactbase.member');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            MemberRepository::class,
            MemberRepositoryInterface::class,
            'bct.unionimpactbase.member',
        ];
    }

}