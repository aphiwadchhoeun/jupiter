<?php namespace Bct\Unionimpactbase\Providers;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 12:40
 */

use Bct\Unionimpactbase\Libs\Laracasts\Utilities\LaravelViewBinder;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Laracasts\Utilities\JavaScript\PHPToJavaScriptTransformer;

class JavaScriptServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('JavaScriptUnionimpactbase', function ($app) {
            $view = 'bct/unionimpactbase::php_vars_to_js';
            $namespace = 'BctUnionimpactbase';

            $binder = new LaravelViewBinder($app['events'], $view);

            return new PHPToJavaScriptTransformer($binder, $namespace);
        });
    }

    /**
     * Publish the plugin configuration.
     */
    public function boot()
    {
        AliasLoader::getInstance()->alias(
            'JavaScriptUnionimpactbase',
            'Bct\Unionimpactbase\Libs\Laracasts\Utilities\JavaScriptUnionimpactbaseFacade'
        );
    }

}
