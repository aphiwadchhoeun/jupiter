<?php namespace Bct\Unionimpactbase\Providers;

use Bct\Unionimpactbase\Repositories\MigrationUtil\MigrationUtilRepository;
use Bct\Unionimpactbase\Repositories\MigrationUtil\MigrationUtilRepositoryInterface;
use Cartalyst\Support\ServiceProvider;

class MigrationUtilServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * {@inheritDoc}
     */
    public function register() {
        // Register the repository
        //$this->bindIf('bct.unionimpactbase.migrationutil', 'Bct\Unionimpactbase\Repositories\MigrationUtil\MigrationUtilRepository');
        $this->app->bind(MigrationUtilRepositoryInterface::class, MigrationUtilRepository::class, true);
        $this->app->alias(MigrationUtilRepositoryInterface::class, 'bct.unionimpactbase.migrationutil');

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            MigrationUtilRepository::class,
            MigrationUtilRepositoryInterface::class,
            'bct.unionimpactbase.migrationutil'
        ];
    }

}
