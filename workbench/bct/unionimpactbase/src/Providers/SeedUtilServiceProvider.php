<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 7/25/2016
 * Time: 5:42 PM
 */

namespace Bct\Unionimpactbase\Providers;


use Bct\Unionimpactbase\Repositories\SeedUtil\SeedUtilRepository;
use Bct\Unionimpactbase\Repositories\SeedUtil\SeedUtilRepositoryInterface;
use Cartalyst\Support\ServiceProvider;

class SeedUtilServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        // Register the repository
        //$this->bindIf('bct.unionimpactbase.seedutil', 'Bct\Unionimpactbase\Repositories\SeedUtil\SeedUtilRepository');
        $this->app->bind(SeedUtilRepositoryInterface::class, SeedUtilRepository::class, true);
        $this->app->alias(SeedUtilRepositoryInterface::class, 'bct.unionimpactbase.seedutil');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            SeedUtilRepository::class,
            SeedUtilRepositoryInterface::class,
            'bct.unionimpactbase.seedutil',
        ];
    }

}