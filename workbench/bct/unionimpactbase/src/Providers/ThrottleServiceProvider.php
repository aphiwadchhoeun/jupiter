<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/15/2016
 * Time: 4:20 PM
 */

namespace Bct\Unionimpactbase\Providers;


use Bct\Unionimpactbase\Checkpoints\WhitelistCheckpoint;
use Cartalyst\Support\ServiceProvider;

class ThrottleServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->registerWhitelistCheckpoint();
    }


    /**
     * Register whitelist checkpoint
     */
    protected function registerWhitelistCheckpoint() {
        $this->app->singleton('sentinel.checkpoint.whitelist', function ($app) {
            return new WhitelistCheckpoint($app['sentinel.throttling'], $app['request']->getClientIp());
        });
    }

}