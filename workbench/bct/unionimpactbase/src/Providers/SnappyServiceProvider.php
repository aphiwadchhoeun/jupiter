<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 3/9/2017
 * Time: 12:42 PM
 */

namespace Bct\Unionimpactbase\Providers;

use Bct\Unionimpactbase\Repositories\Snappy\SnappyRepository;
use Bct\Unionimpactbase\Repositories\Snappy\SnappyRepositoryInterface;
use Cartalyst\Support\ServiceProvider;

class SnappyServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //$this->bindIf('bct.unionimpactbase.snappy', 'Bct\Unionimpactbase\Repositories\Snappy\SnappyRepository');
        $this->app->bind(SnappyRepositoryInterface::class, SnappyRepository::class, true);
        $this->app->alias(SnappyRepositoryInterface::class, 'bct.unionimpactbase.snappy');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            SnappyRepository::class,
            SnappyRepositoryInterface::class,
            'bct.unionimpactbase.snappy',
        ];
    }
}