<?php namespace Bct\Unionimpactbase\Providers;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 17.12.15
 * Time: 15:38
 */

use Bct\Unionimpactbase\Mailer\UserMailer;
use Bct\Unionimpactbase\Mailer\UserMailerInterface;
use Bct\Unionimpactbase\Repositories\Contractor\ContractorRepository;
use Bct\Unionimpactbase\Repositories\Contractor\ContractorRepositoryInterface;
use Cartalyst\Support\ServiceProvider;
use Request;
use URL;

class ContractorServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        //$this->bindIf('bct.unionimpactbase.mailer', 'Bct\Unionimpactbase\Mailer\UserMailer');
        $this->app->bind(UserMailerInterface::class, UserMailer::class, true);
        $this->app->alias(UserMailerInterface::class, 'bct.unionimpactbase.mailer');

        // Register the repository
        //$this->bindIf('bct.unionimpactbase.contractor', 'Bct\Unionimpactbase\Repositories\Contractor\ContractorRepository');
        $this->app->bind(ContractorRepositoryInterface::class, ContractorRepository::class, true);
        $this->app->alias(ContractorRepositoryInterface::class, 'bct.unionimpactbase.contractor');

        // Register the validator
        $this->bindIf('bct.unionimpactbase.user.profile.contractor.validator', 'Bct\Unionimpactbase\Validator\Contractor\UserProfileContractorValidator');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            UserMailer::class,
            UserMailerInterface::class,
            'bct.unionimpactbase.mailer',
            ContractorRepository::class,
            ContractorRepositoryInterface::class,
            'bct.unionimpactbase.contractor',
        ];
    }

}