<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 4/3/2017
 * Time: 3:58 PM
 */

namespace Bct\Unionimpactbase\Providers;

use Cartalyst\Support\ServiceProvider;

class UrlShortenerServiceProvider extends ServiceProvider
{

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__.'/../../config/url-shortener.php';
        $this->publishes([$configPath => config_path('url-shortener.php')], 'config');

        $this->app['bct.unionimpactbase.url-shortener'] = $this->app->share(function ($app) {
            return $app['bct.unionimpactbase.lib.url-shortener'];
        });
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__.'/../../config/url-shortener.php';
        $this->mergeConfigFrom($configPath, 'url-shortener');

        $this->bindLibs();
    }


    protected function bindLibs()
    {
        // Register the repository
        $this->bindIf('bct.unionimpactbase.lib.url-shortener', 'Bct\Unionimpactbase\Libs\UrlShortener\UrlShortener');
    }

}