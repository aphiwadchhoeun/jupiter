<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/19/2015
 * Time: 11:01 AM
 */

namespace Bct\Unionimpactbase\Providers;

use Cartalyst\Support\ServiceProvider;

class ExportQueueProvider extends ServiceProvider {

    public function boot() {
        // Register the attributes namespace
        $this->app['platform.attributes.manager']->registerNamespace(
            $this->app['Bct\Unionimpactbase\Models\ExportQueue']
        );
    }

    public function register() {
        // Register the repository
        $this->bindIf('bct.unionimpactbase.export.queue', 'Bct\Unionimpactbase\Repositories\ExportQueue\ExportQueueRepository');

        // Register the event handler
        $this->bindIf('bct.unionimpactbase.export.queue.handler.event', 'Bct\Unionimpactbase\Handlers\ExportQueue\ExportQueueEventHandler');

        // Register Queue Dispatcher
        $this->bindIf('bct.unionimpactbase.export.queue.dispatcher', 'Bct\Unionimpactbase\Dispatchers\ExportQueueDispatcher');
    }

}