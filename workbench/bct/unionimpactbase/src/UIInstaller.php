<?php
namespace Bct\Unionimpactbase\Installer;

use Illuminate\Container\Container;
use Platform\Installer\Installer;
use Platform\Installer\Repository;
use Cartalyst\Extensions\Extension;
use Illuminate\Support\Str;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 1/20/2016
 * Time: 3:40 PM
 */
class UIInstaller extends Installer {

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $laravel
     * @param  \Platform\Installer\Repository $repository
     * @return void
     */
    public function __construct(Container $laravel, Repository $repository) {
        parent::__construct($laravel, $repository);
    }

    /**
     * Installs all the available extensions.
     *
     * @return void
     */
    protected function installExtensions() {
        // Alright, database connection established. Let's now grab all
        // core extensions
        $extensionBag = $this->laravel['platform']->getExtensionBag();

        // Register all local extension
        $extensionBag->findAndRegisterExtensions();

        // Sort the extensions by their dependencies
        $extensionBag->sortExtensions();

        // Set the connection resolver on our extensions
        Extension::setConnectionResolver($this->laravel['db']);

        // Set the laravel migrator on our extensions
        Extension::setMigrator($this->laravel['migrator']);

        // Flush the Cache
        $this->laravel['cache']->flush();

        // Loop through extensions
        // Only install platform extensions
        foreach ($extensionBag->all() as $extension) {
            if (preg_match('/^platform/', $extension->getSlug())) {
                $extension->install();

                $extension->enable();
            }
        }
    }

    public function prepDeployConfigs() {
        $deploy_config = parse_ini_file(base_path('deploy.ini'));

        $db_config = [
            'host'     => $deploy_config['host'],
            'database' => $deploy_config['database'],
            'username' => $deploy_config['username'],
            'password' => $deploy_config['password']
        ];

        $user_config = [
            'email'    => $deploy_config['user_email'],
            'password' => $deploy_config['user_password']
        ];

        $config = array_merge($this->repository->getDatabaseData('mysql'), $db_config);

        $this->repository->setDatabaseData('mysql', $config);
        $this->repository->setUserData($user_config);

        return $deploy_config;
    }
    protected function setupDatabase()
    {
        $driver = $this->repository->getDatabaseDriver();

        $config = $this->repository->getDatabaseData($driver);

        try {
            $this->laravel['db.factory']->make(array_merge(compact('driver'), $config));
        } catch (\PDOException $e) {
            throw $e;
        }

        $configFile = __DIR__."/stubs/database/{$driver}.php";
        $envStub = __DIR__.'/stubs/env.stub';

        // Now, let's update our stub file with
        // our actual database credentials
        $contents = str_replace(
            array_map(function ($key) {
                return '{{'.$key.'}}';
            }, array_keys($config)),
            array_values($config),
            $this->laravel['files']->get($envStub)
        );

        // Generate a key
        $contents = str_replace('{{key}}', Str::random(32), $contents);

        // Just a triple check we can actually
        // write the configuration.
        if ($this->laravel['files']->put(($envFile = $this->laravel['path.base'].'/.env'), $contents) === false) {
            throw new \RuntimeException("Could not write env file to [$envFile].");
        }

        if ($this->laravel['files']->put(($newConfigFile = $this->laravel['path.base'].'/config/database.php'), $this->laravel['files']->get($configFile)) === false) {
            throw new \RuntimeException("Could not write database config file to [$newConfigFile].");
        }

        // Reload env file
        $this->laravel['Illuminate\Foundation\Bootstrap\DetectEnvironment']->bootstrap($this->laravel);

        // Reload config
        $this->laravel['Illuminate\Foundation\Bootstrap\LoadConfiguration']->bootstrap($this->laravel);

        // Purge old connection
        $this->laravel['db']->purge();

        // Set table prefix on the connection
        $this->laravel['db']->setTablePrefix(array_get($config, 'prefix'));

        // Reconnect using the new config
        $this->laravel['db']->reconnect($driver);
    }
    public function install($testing = false)
    {
        // Fire the "installer.before" event
        $this->laravel['events']->fire('installer.before');

        if (! $testing) {
            // Install config
            $this->setupDatabase();
        }

        // Migrate the required packages
        $this->migrateRequiredPackages();

        // Install platform extensions
        $this->installExtensions();

        if (! $testing) {
            // Disable checkpoints
            unset($this->laravel['sentinel.checkpoints']);

            // Create default user
            $this->createDefaultUser();
        }

        // Fire the "installer.after" event
        $this->laravel['events']->fire('installer.after');

        if (! $testing) {
            // Write platform's installed version config
            $this->updatePlatformInstalledVersion($this->laravel['platform']->codebaseVersion());
        }
    }

}