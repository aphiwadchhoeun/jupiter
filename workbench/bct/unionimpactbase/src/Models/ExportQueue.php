<?php namespace Bct\Unionimpactbase\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class ExportQueue extends BaseModel implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait, SoftDeletes;

    static $STATUS_PENDING = 0;
    static $STATUS_COMPLETED = 1;

    /**
     * {@inheritDoc}
     */
    protected $table = 'export_queue';

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    protected $fillable = [
        'name',
        'path',
        'source',
        'created_by',
        'ext',
        'mime',
        'status'
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/unionimpactbase.export_queue';

    public function getFullNameAttribute() {
        return $this->name . '.' . $this->ext;
    }

}
