<?php namespace Bct\Unionimpactbase\Models;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 06.11.15
 * Time: 14:21
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Platform\Attributes\Traits\EntityTrait;

class States extends BaseModel implements EntityInterface
{

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_states';

}
