<?php namespace Bct\Unionimpactbase\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.03.16
 * Time: 11:44
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Sentinel\Users\EloquentUser;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Support\Facades\Hash;
use Platform\Attributes\Traits\EntityTrait;

class User extends EloquentUser implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'users';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'activated',
    ];

    /**
     * {@inheritDoc}
     */
    protected $appends = [
        'activated',
        'full_name'
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
    ];

    protected $fillable = [
        'email',
        'password',
        'last_name',
        'first_name',
        'permissions',
        'z_user_profiles_id',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/unionimpactbase.user';

    protected static $userProfilesModel = 'Bct\Unionimpactbase\Models\UserProfiles';

    protected static $datagridModel = 'Bct\Unionimpactbase\Models\ConfigDatagrid';


    /**
     * Relationship with config datagrid
     */
    public function datagrid() {
        return $this->hasOne(static::$datagridModel, 'user_id');
    }

    public function profile()
    {
        return $this->belongsTo(static::$userProfilesModel, 'z_user_profiles_id');
    }

    public function setPasswordAttribute($pass){

        $this->attributes['password'] = Hash::make($pass);

    }

    /**
     * Get mutator for the "activated" attribute.
     *
     * @return bool
     */
    public function getActivatedAttribute()
    {
        $activation = $this->activations->sortByDesc('created_at')->first();

        return (bool) $activation ? $activation->completed : false;
    }

    public function getFullNameAttribute()
    {
        return array_to_string_projects([ str_limit($this->first_name, 64), str_limit($this->last_name, 64) ], ' ');
    }

    /*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */

    public function scopeEmail($query, $email)
    {
        $query->where('email', $email);
    }

}
