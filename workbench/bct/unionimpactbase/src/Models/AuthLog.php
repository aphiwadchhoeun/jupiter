<?php namespace Bct\Unionimpactbase\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class AuthLog extends BaseModel {

    use SoftDeletes;

    /**
     * {@inheritDoc}
     */
    protected $table = 'auth_log';

}
