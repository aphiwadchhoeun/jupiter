<?php namespace Bct\Unionimpactbase\Models;

/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 31.03.17
 * Time: 12:47
 */


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    protected $guarded = ['*', 'id'];

}
