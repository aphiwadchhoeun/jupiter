<?php namespace Bct\Unionimpactbase\Models;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 23.08.16
 * Time: 18:21
 */


use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class Member extends BaseModel implements EntityInterface
{

    use EntityTrait, NamespacedEntityTrait, SoftDeletes;

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_members';

    /**
     * SoftDeletes
     *
     * @var array
     */
    protected $dates = [ 'deleted_at' ];

    protected $with = [
        //'user',
        'profile'
    ];

    protected $mp_roles = [
        'mp_member',
        'mp_officestaff',
        'mp_admin'
    ];
    protected static $userModel = 'Bct\Unionimpactbase\Models\User';

    protected static $userProfilesModel = 'Bct\Unionimpactbase\Models\UserProfiles';

    protected static $emailModel = 'Bct\Unionimpactbase\Models\MemberEmail';

    protected static $nameModel = 'Bct\Unionimpactbase\Models\MemberName';


    /*
     * -----------------------------------
     *             RELATIONS
     * -----------------------------------
     */
    public function user()
    {
        return $this->hasManyThrough(static::$userModel, static::$userProfilesModel, 'z_members_id', 'z_user_profiles_id');
    }

    public function profile()
    {
        return $this->hasOne(static::$userProfilesModel, 'z_members_id');
    }

    public function members_email(){
        return $this->hasMany(static::$emailModel, 'z_members_id');
    }

	public function members_name(){
		return $this->hasMany(static::$nameModel, 'z_members_id');
	}

    /*
     * -----------------------------------
     *              SCOPES
     * -----------------------------------
     */
    public function getUser()
    {
        return $this->user()->first();
    }

    public function getMembersWithMemberRole()
    {
        return $this->profile()->first()->user()->first()->whereHas('roles', function ($query) {
            $query->where('slug', 'mp_member');
        });
    }


    public function getFirstRole()
    {
        if (!$this->profile()->first() || !$this->profile()->first()->user()->first() || !$this->profile()->first()->user()->first()->roles) {
            return false;
        }

        $roles = $this->profile()->first()->user()->first()->roles;
        foreach ($roles as $role) {
            if (in_array($role->slug, $this->mp_roles)) {
                return $role;
            }
        }

        return false;
    }


    public function getFirstRoleSlug()
    {
        if (!$this->profile()->first() || !$this->profile()->first()->user()->first() || !$this->profile()->first()->user()->first()->roles) {
            return false;
        }

        $roles = $this->profile()->first()->user()->first()->roles;
        foreach ($roles as $role) {
            if (in_array($role->slug, $this->mp_roles)) {
                return $role->slug;
            }
        }

        return false;
    }


    public function getFirstRoleName()
    {
        if (!$this->profile()->first() || !$this->profile()->first()->user()->first() || !$this->profile()->first()->user()->first()->roles) {
            return false;
        }

        $roles = $this->profile()->first()->user()->first()->roles;
        foreach ($roles as $role) {
            if (in_array($role->slug, $this->mp_roles)) {
                return $role->name;
            }
        }

        return false;
    }


    public function isMPMember()
    {
        if ($this->getUser() && $this->getUser()->isMPMember()) {
            return true;
        }
    }


}
