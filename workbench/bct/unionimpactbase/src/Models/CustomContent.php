<?php namespace Bct\Unionimpactbase\Models;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/22/2016
 * Time: 12:24 PM
 */
use Cartalyst\Attributes\EntityInterface;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class CustomContent extends BaseModel implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'custom_content';

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/unionimpactbase.customcontent';

}