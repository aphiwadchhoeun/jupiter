<?php namespace Bct\Unionimpactbase\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.12.15
 * Time: 15:39
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Platform\Attributes\Traits\EntityTrait;

class MemberName extends BaseModel implements EntityInterface
{
	use EntityTrait, NamespacedEntityTrait;

	protected $table = 'z_member_name';

	public $timestamps = true;

	/*
	 * -----------------------------------
	 *             RELATIONS
	 * -----------------------------------
	 */
	protected static $memberModel = 'Bct\Unionimpactbase\Models\Member';


	public function member()
	{
		return $this->belongsTo(static::$memberModel, 'z_members_id');
	}

	public function getFullName(){
		return $this->first_name.' '.$this->middle_name.' '.$this->last_name;
	}


}