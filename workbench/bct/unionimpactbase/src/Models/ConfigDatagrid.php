<?php namespace Bct\Unionimpactbase\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.03.16
 * Time: 11:46
 */
use Cartalyst\Attributes\EntityInterface;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class ConfigDatagrid extends BaseModel implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'config_datagrid';

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/unionimpactbase.datagrid';

    protected static $userModel = 'Bct\Unionimpactbase\Models\User';


    /**
     * Relationship with user profile table
     */
    public function user() {
        return $this->belongsTo(static::$userModel, 'user_id');
    }

}