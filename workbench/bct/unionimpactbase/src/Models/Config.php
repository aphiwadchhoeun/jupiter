<?php namespace Bct\Unionimpactbase\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 25.01.16
 * Time: 12:08
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Platform\Attributes\Traits\EntityTrait;

class Config extends BaseModel implements EntityInterface
{

    use EntityTrait, NamespacedEntityTrait;

    protected $table = 'config';

    public $timestamps = false;

    protected $primaryKey = 'item';

    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [ 'item', 'value' ];


    /**
     * @param $query
     * @param $key
     *
     * @return mixed
     */
    public function scopeFindByItem($query, $key)
    {
        return $query->where('item', $key);
    }
}
