<?php namespace Bct\Unionimpactbase\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 10.08.15
 * Time: 15:34
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Platform\Attributes\Traits\EntityTrait;

class UserProfiles extends BaseModel implements EntityInterface
{

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_user_profiles';

    protected $with = [
        //'member',
        //'user'
    ];

    protected static $userModel = 'Bct\Unionimpactbase\Models\User';
    protected static $memberModel = 'Bct\Unionimpactbase\Models\Member';

    public function user()
    {
        return $this->hasOne(static::$userModel, 'z_user_profiles_id');
    }

    public function member()
    {
        return $this->belongsTo(static::$memberModel, 'z_members_id');
    }

    public function isContractorPending()
    {
        return ($this->contractor_status);
    }

    public function hasContractor()
    {
        return ((int)$this->z_contractors_id !== 0);
    }

	public function hasMember()
	{
        return ((int)$this->z_members_id !== 0);
	}

}
