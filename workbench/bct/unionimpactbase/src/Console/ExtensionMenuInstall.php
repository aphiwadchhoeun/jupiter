<?php namespace Bct\Unionimpactbase\Console;

    /**
 * Created by PhpStorm.
 * User: roman
 * Date: 17.03.16
 * Time: 15:24
 */

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ExtensionMenuInstall extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'ui:menu-install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Union Impact Menu Installation';
    /**
     * ClearThrottle constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $extension = app('extensions')->get($this->input->getOption('extension'));

        $observer = app('platform.menus.observer');

        $observer->afterUninstall($extension);
        $observer->afterInstall($extension);
        $observer->afterEnable($extension);

        $this->comment('Menu Installation complete :)');
    }


    protected function getOptions() {
        return [
            ['extension', null, InputOption::VALUE_REQUIRED, 'Give the extension name to run migration for.'],
        ];
    }

}
