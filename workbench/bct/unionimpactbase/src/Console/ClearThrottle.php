<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/5/2016
 * Time: 4:46 PM
 */

namespace Bct\Unionimpactbase\Console;


use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ClearThrottle extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'throttle:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear throttle';

    /**
     * ClearThrottle constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $content = implode(',', ['id', 'user_id', 'type', 'ip', 'created_at', 'updated_at']) . "\r\n";

        $throttles = DB::table('throttle')->select('*')->get();

        if(count($throttles) > 0) {
            foreach ($throttles as $t) {
                $content .= implode(',', [$t->id, $t->user_id, $t->type, $t->ip, $t->created_at, $t->updated_at]) . "\r\n";
            }

            $filename = 'throttle_' . Carbon::now()->format('Y-m-d-H-i-s') . '.log';
            Storage::put($filename, $content);

            echo "Saved logs!\r\n";

            DB::table('throttle')->truncate();

            echo "Throttle cleared!";
        } else {
            echo "No Throttle to clear.";
        }
    }

}