<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/25/2016
 * Time: 10:11 AM
 */

namespace Bct\Unionimpactbase\Console;


use Cartalyst\Themes\Assets\AssetManager;
use Illuminate\Console\Command;

class ClearAssets extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'assets:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all compiled assets.';

    protected $assetManager;

    /**
     * ClearAssets constructor.
     * @param AssetManager $assetManager
     */
    public function __construct(AssetManager $assetManager) {
        parent::__construct();

        $this->assetManager = $assetManager;
    }

    public function handle() {
        $this->assetManager->clearAssets(true);
        $this->output->writeln("Assets Cleared!");
    }

}