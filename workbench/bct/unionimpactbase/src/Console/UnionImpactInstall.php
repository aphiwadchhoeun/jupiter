<?php

namespace Bct\Unionimpactbase\Console;

use Bct\Unionimpactbase\Installer\UIInstaller;
use Platform\Installer\Console\InstallCommand;
use Symfony\Component\Console\Input\InputOption;

class UnionImpactInstall extends InstallCommand {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'ui:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Union Impact Installation';

    /**
     * The installer assosiated with the command.
     *
     * @var \Platform\Installer\Installer
     */
    protected $installer;

    /**
     * Create a new command instance.
     *
     * @param UIInstaller $installer
     */
    public function __construct(UIInstaller $installer) {
        parent::__construct($installer);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        // Show the welcome message
        $this->showWelcomeMessage();

        if ($this->input->getOption('autoload')) {
            // Load from deploy.ini and start prep
            print_r($this->installer->prepDeployConfigs());
        } else {
            // Ask for the database details
            $this->askDatabaseDetails();

            // Ask for the default user details
            $this->askDefaultUserDetails();
        }

        $this->comment('Installation in progress...');

        // Install Platform :
        $this->installer->install();

        $this->comment('Installation complete :)');
    }

    /**
     * Shows custom welcome message.
     *
     * @return void
     */
    protected function showWelcomeMessage() {
        $this->output->writeln(<<<WELCOME
<fg=white>
*-----------------------------------------------*
|                                               |
|     Welcome to the Union Impact Installer     |
|          Based on Cartalyst Platform.         |
|                                               |
*-----------------------------------------------*
</fg=white>
WELCOME
        );
    }

    protected function getOptions() {
        return [
            ['autoload', null, InputOption::VALUE_OPTIONAL, 'Auto load config from deploy.ini'],
        ];
    }

}
