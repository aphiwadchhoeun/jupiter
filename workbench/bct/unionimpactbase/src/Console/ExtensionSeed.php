<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 7/25/2016
 * Time: 4:04 PM
 */

namespace Bct\Unionimpactbase\Console;


use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ExtensionSeed extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ui:ex-seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run seeds for the specified extension based on its slug.';

    /**
     * Extension Seed constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $extension = app('extensions')->get($this->input->getOption('extension'));

            // check if the extension exists
            if ($extension) {
                $this->output->writeln("Extension \"" . $extension->getNamespace() . "\" found.");

                // verify the extension is currently enabled
                if ($extension->isInstalled() && $extension->isEnabled()) {
                    $this->output->writeln("Extension seeding...");

                    $seederFilePath = "{$extension->getSeedsPath()}/{$extension->seeder_file}.php";
                    if (file_exists($seederFilePath)) {
                        require_once $seederFilePath;

                        $namespace = "{$extension->getNamespace()}\\{$extension->seeds_namespace}\\{$extension->seeder_file}";

                        $class = '\\' . ltrim($namespace, '\\');

                        $seeder = new $class;

                        $seeder->run();
                    }

                    $this->output->writeln("Extension Seeded!");
                } else {
                    $this->output->error("Cannot run seeds for extension {$extension} because it's not enabled.");
                }
            } else {
                $this->output->error("Extension not found.");
            }
        } catch (\RuntimeException $re) {
            $this->output->error($re->getMessage());
        }
    }

    protected function getOptions() {
        return [
            ['extension', null, InputOption::VALUE_REQUIRED, 'Give the extension slug to install.'],
        ];
    }

}