<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/23/2016
 * Time: 12:47 PM
 */

namespace Bct\Unionimpactbase\Console;


use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ExtensionMigrate extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ui:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migration for Union Impact extensions';

    /**
     * ClearThrottle constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $extension = app('extensions')->get($this->input->getOption('extension'));
        if (!$this->input->hasOption('force')) {
            static::runMigrate($extension);
        } else {
            static::runResetMigrate($extension);
            static::runMigrate($extension);
        }

        foreach ($extension->getMigrator()->getNotes() as $note) {
            $this->output->writeln($note);
        }
    }

    public static function runMigrate($extension) {
        $path = $extension->getMigrationsPath();

        $extension->getMigrator()->run($path);
    }

    public function runResetMigrate($extension) {
        $path = $extension->getMigrationsPath();

        $files = $extension->getMigrator()->getMigrationFiles($path);
        $repository = $extension->getMigrator()->getRepository();

        // Get an array of migration names which will be
        // reset
        $migrations = array_intersect(array_reverse($repository->getRan()), $files);

        // Loop through the migrations we have to rollback
        foreach ($migrations as $migration) {
            // Let the migrator resolve the migration instance
            $instance = $extension->getMigrator()->resolve($migration);

            // And we'll call the down method on the migration
            $instance->down();

            // Now we need to manipulate what the migrator does to
            // delete a migration.
            $migrationClass = new \StdClass;
            $migrationClass->migration = $migration;
            $repository->delete($migrationClass);
        }
    }

    protected function getOptions() {
        return [
            ['extension', null, InputOption::VALUE_REQUIRED, 'Give the extension name to run migration for.'],
            ['force', null, InputOption::VALUE_OPTIONAL, 'Force migration to run.'],
        ];
    }


}