<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 6/13/2016
 * Time: 2:23 PM
 */

namespace Bct\Unionimpactbase\Console;


use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ExtensionInstall extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ui:ex-install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install a specified extension based on its slug.';

    /**
     * Extension Installed constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $extension = app('extensions')->get($this->input->getOption('extension'));

            // check if the extension exists
            if ($extension) {
                $this->output->writeln("Extension \"" . $extension->getNamespace() . "\" found.");

                // verify the extension is currently not installed, and can be installed
                if ($extension->canInstall() && $extension->isUninstalled()) {
                    $this->output->writeln("Extension installing...");
                    $extension->install();
                    $extension->enable();
                    $this->output->writeln("Extension Installed!");
                } else {
                    $this->output->error("Cannot be installed because it is already installed  or dependency constraint.");
                }
            } else {
                $this->output->error("Extension not found.");
            }
        } catch (\RuntimeException $re) {
            $this->output->writeln($re->getMessage());
        }
    }

    protected function getOptions() {
        return [
            ['extension', null, InputOption::VALUE_REQUIRED, 'Give the extension slug to install.'],
        ];
    }

}