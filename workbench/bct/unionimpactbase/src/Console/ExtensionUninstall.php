<?php

namespace Bct\Unionimpactbase\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ExtensionUninstall extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ui:ex-uninstall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Uninstall a specified extension based on its slug.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $extension = app('extensions')->get($this->input->getOption('extension'));

            // check if the extension exists
            if ($extension) {
                $this->output->writeln("Extension \"" . $extension->getNamespace() . "\" found.");

                // verify the extension is currently installed, and can be installed
                if ($extension->canUninstall() && $extension->isInstalled()) {
                    $this->output->writeln("Extension uninstalling...");
                    $extension->uninstall();
                    $this->output->writeln("Extension Uninstalled!");
                } else {
                    $this->output->error("Cannot be uninstalled because it is already uninstalled or dependency constraint.");
                }
            } else {
                $this->output->error("Extension not found.");
            }
        } catch (\RuntimeException $re) {
            $this->output->writeln($re->getMessage());
        }
    }

    protected function getOptions() {
        return [
            ['extension', null, InputOption::VALUE_REQUIRED, 'Give the extension slug to install.'],
        ];
    }
}
