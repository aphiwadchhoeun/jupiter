<?php namespace Bct\Unionimpactbase\Console;

/**
 * Created by PhpStorm.
 * User: romannebesnuy
 * Date: 10.04.17
 * Time: 12:18
 */

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class FixAddressMember extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fix:address-member';

    /**
     * @var string
     */
    protected $description = 'Fix address of member. Trim address and remove empty addresses.';

    protected $table = 'z_members';


    /**
     * FixAddressMember constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {

        if ( ! Schema::hasTable($this->getTable())) {
            echo "`" . $this->getTable() . "` table doesn't exists.";
            return;
        }

        $members = DB::table($this->getTable())
            ->select(
                'id',
                'address'
            )
            ->where('address', 'like', '%,%')
            ->get();

        if (count($members) === 0) {
            echo "No members that needed to be fixed.";
            return;
        }

        $count = 0;

        foreach ($members as $member) {
            $address = implode(', ', array_filter(array_map('trim', explode(',', $member->address))));

            if ($member->address !== $address) {
                DB::table($this->getTable())
                    ->where('id', $member->id)
                    ->update(['address' => $address]);


                ++$count;
            }

        }

        if ($count === 0) {
            echo "No members that needed to be fixed.";
        } elseif ($count === 1) {
            echo "1 member was fixed.";
        } elseif ($count > 1) {
            echo "{$count} members were fixed.";
        }

    }

    protected function getTable() {
        return $this->table;
    }

}