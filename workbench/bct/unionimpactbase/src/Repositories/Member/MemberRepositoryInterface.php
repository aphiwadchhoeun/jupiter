<?php namespace Bct\Unionimpactbase\Repositories\Member;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 23.08.16
 * Time: 16:52
 */

interface MemberRepositoryInterface
{

    public function grid();


    public function findAll();


    public function find($id);

}