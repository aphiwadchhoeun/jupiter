<?php namespace Bct\Unionimpactbase\Repositories\Member;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 23.08.16
 * Time: 16:52
 */

use Bct\Unionimpactbase\Models\MemberEmail;
use Bct\Unionimpactbase\Models\MemberName;
use Bct\Unionimpactbase\Models\UserProfiles;
use Bct\Unionimpactbase\Repositories\BaseRepository;
use Carbon\Carbon;
use Cartalyst\Support\Traits;
use Illuminate\Support\Facades\Schema;
use Illuminate\Container\Container;
use Platform\Roles\Models\Role;
use Platform\Users\Models\User;
use Validator;

class MemberRepository extends BaseRepository implements MemberRepositoryInterface
{

    use Traits\RepositoryTrait;
    /**
     * The Data handler.
     *
     * @var \Bct\Unionimpactbase\Handlers\DataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent model.
     *
     * @var string
     */
    protected $model;


    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app)
    {
        parent::__construct();

        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->setModel(get_class($app['Bct\Unionimpactbase\Models\Member']));
    }


    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this->createModel();
    }


    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->createModel()->get();
    }


    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->grid()->find($id);
    }


    public function registerUserForMember(User $user_registered, $role_name, $data)
    {
        $user = \Bct\Unionimpactbase\Models\User::find($user_registered->id);

        try {
            if (!$user->profile) {

                $profile = new UserProfiles();
                $profile->fill($data)->save();

                $user->profile()->associate($profile)->save();
            } else {
                $user->profile()->fill($data)->push();
            }

            if (!$user->inRole($role_name) && $role_user = Role::where('slug', $role_name)->first()) {
                $user->roles()->attach($role_user->id);
            }
        } catch (\Exception $e) {
            return [$e->getMessage(), false, $user];
        }

        return [ "", true, $user ];
    }

    public function registerMember($user, $member_data)
    {
        $member = $this->grid();
        $member_data['status_created'] = 1;
        $member->fill($member_data)->save();

        // Adding Hash to newly created Member
        $member->hash = sha1($member->id . $member->first_name . $member->last_name . $member->created_at . Carbon::now()->toDateTimeString());
        $member->save();
		if(Schema::hasTable('z_member_name')){
			$member->members_name()->create([
				'first_name'=> $member_data['first_name'],
				'last_name'=> $member_data['last_name'],
				'type'  => 'Primary'
			]);
		}
	    if(Schema::hasTable('z_member_email')) {
		    $member->members_email()->create([
			    'email' => $member_data['email'],
			    'type'  => 'Primary'
		    ]);
	    }

        $user->profile->member()->associate($member)->save();
    }

}
