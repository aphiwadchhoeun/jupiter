<?php namespace Bct\Unionimpactbase\Repositories\SeedUtil;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\DB;

/*
 * ------------
 * USAGE SAMPLE
 * ------------
 *
 * // get an instance of SeedUtil
 * $util = app()->make('bct.unionimpactbase.seedutil');
 *
 * // set table to run seed on
 * $util->setTable('table_name');
 *
 * // set column names to check if they are exists before insert a new record
 * $util->setKeyColumns(['col_1', 'col_2', 'col_3]);
 *
 * // set the data tobe seeded
 * $util->setData([
 *      'col_1' => 'val1',
 *      'col_2' => 'val2',
 *      'col_3' => 'val3',
 *      'col_4' => 'val4',
 *      'col_5' => 'val5',
 * ]);
 *
 * // run seed
 * $util->seed();
 *
 */

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 7/25/2016
 * Time: 5:35 PM
 */
class SeedUtilRepository implements SeedUtilRepositoryInterface {

    use Traits\ContainerTrait;

    protected $table, $data, $keys;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app) {
        $this->setContainer($app);
    }

    /**
     * Set target table
     *
     * @param $table
     * @return mixed
     */
    public function setTable($table) {
        $this->table = $table;
    }

    /**
     * Set data to be seeded
     *
     * @param array $data
     * @return mixed
     */
    public function setData(array $data) {
        $this->data = $data;
    }

    /**
     * Set columns to act as unique identifier key
     *
     * @param $keys
     */
    public function setKeyColumns($keys) {
        $this->keys = $keys;
    }

    /**
     * Run the seed
     * @return mixed
     * @throws \Exception
     */
    public function seed() {
        // if there's any data exists in the table, quit immediately
        if (DB::table($this->table)->count() > 0) {
            return true;
        }

        try {
            foreach ($this->data as $row) {
                $key_cols = array_only($row, $this->keys);

                if (is_null(DB::table($this->table)->where($key_cols)->first())) {
                    DB::table($this->table)->insert($row);
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

}