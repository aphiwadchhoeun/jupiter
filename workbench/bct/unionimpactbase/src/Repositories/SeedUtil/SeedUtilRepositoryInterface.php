<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 7/25/2016
 * Time: 5:36 PM
 */

namespace Bct\Unionimpactbase\Repositories\SeedUtil;


interface SeedUtilRepositoryInterface {

    /**
     * Set target table
     *
     * @param $table
     * @return mixed
     */
    public function setTable($table);

    /**
     * Set data to be seeded
     *
     * @param array $data
     * @return mixed
     */
    public function setData(array $data);

    /**
     * Run the seed
     *
     * @return mixed
     */
    public function seed();

}