<?php namespace Bct\Unionimpactbase\Repositories\CustomContent;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/22/2016
 * Time: 12:22 PM
 */
interface CustomContentRepositoryInterface {

    public function findBySlug($slug);

    public function store($name, $data);

}