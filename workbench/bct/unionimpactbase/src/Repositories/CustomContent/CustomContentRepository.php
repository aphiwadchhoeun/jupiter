<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/22/2016
 * Time: 12:27 PM
 */

namespace Bct\Unionimpactbase\Repositories\CustomContent;

use Bct\Unionimpactbase\Models\CustomContent;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;

class CustomContentRepository implements CustomContentRepositoryInterface {

    use Traits\ContainerTrait;

    /**
     * Constructor.
     * @param Container $app
     */
    public function __construct(Container $app) {
        $this->setContainer($app);
    }

    public function findBySlug($slug) {
        return CustomContent::whereSlug($slug)->first();
    }

    public function updateSignInContent($data) {
        return $this->store('Sign In Page', $data);
    }

    public function updateSignUpContent($data) {
        return $this->store('Sign Up Page', $data);
    }

    public function updateLandingContent($data) {
        return $this->store('Landing Page', $data);
    }

    public function updateLandingContentContractor($data) {
        return $this->store('Landing Page Contractor', $data);
    }

    public function updatePolicyContent($data) {
        return $this->store('Policy Page', $data);
    }

    public function updateYoutubeContent($data) {
        return $this->store('Youtube Help Page', $data);
    }

    public function updateKioskTermsPage($data) {
        return $this->store('Kiosk Terms Page', $data);
    }

    public function updateTokenErrorPage($data) {
        return $this->store('Token Error Page', $data);
    }

    public function store($name, $data) {
        $model = CustomContent::whereSlug(str_slug($name, '-'))->first();

        if ($model) {
            return $model->update([
                'name'  => $name,
                'slug'  => str_slug($name, '-'),
                'value' => $data
            ]);
        }

        return CustomContent::create([
            'name'  => $name,
            'slug'  => str_slug($name, '-'),
            'value' => $data
        ]);
    }

}