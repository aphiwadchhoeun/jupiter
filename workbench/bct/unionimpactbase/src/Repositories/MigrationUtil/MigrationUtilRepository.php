<?php namespace Bct\Unionimpactbase\Repositories\MigrationUtil;

use Cartalyst\Support\Traits;
use Closure;
use Doctrine\DBAL\Types\Type;
use Exception;
use Illuminate\Container\Container;
use Illuminate\Database\QueryException;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use ReflectionMethod;

/*
 * ------------
 * USAGE SAMPLE
 * ------------
 *
 * $util = app()->make('bct.unionimpactbase.migrationutil');
 *
 * $util->setTableName('tbl_name');
 *
 * $options = [
 *      [
 *          'name'      => 'id',
 *          'type'      => 'increments',
 *      ],
 *      [
 *          'name'      => 'name',
 *          'type'      => 'string',
 *          'options' => [
 *               'default'  => 'placeholder',
 *               'length'   => 255,
 *               'nullable' => true,
 *          ]
 *      ],
 *      [
 *          'name'      => 'age',
 *          'type'      => 'integer',
 *          'options'   => [
 *               'unsigned'   => true,
 *          ]
 *      ],
 *      [
 *          'name'      => 'salary',
 *          'type'      => 'decimal',
 *          'options'   => [
 *               'total'    => 12,
 *               'places'   =>  2
 *          ]
 *      ],
 *      [
 *          'name'      => 'date_of_birth',
 *          'type'      => 'date',
 *      ]
 * ];
 *
 * $util->setOptions($options);
 *
 * $util->run();
 *
 */

class MigrationUtilRepository implements MigrationUtilRepositoryInterface
{

    use Traits\ContainerTrait;

    protected $tblName;

    protected $tblPrefix = '';

    protected $options = [];

    protected $preMigrate, $postMigrate;

    static protected $supported_types = [ 'increments', 'smallIncrements', 'mediumIncrements', 'bigIncrements' ];

    static protected $with_default_length = [ 'string', 'char' ];

    static protected $int_relatives_signed = [
        'integer',
        'tinyInteger',
        'smallInteger',
        'mediumInteger',
        'bigInteger'
    ];

    static protected $int_relatives_unsigned = [
        'unsignedTinyInteger',
        'unsignedSmallInteger',
        'unsignedMediumInteger',
        'unsignedInteger',
        'unsignedBigInteger'
    ];

    static protected $float_relatives = [ 'float', 'double', 'decimal' ];


    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app)
    {
        $this->setContainer($app);
    }


    /**
     * set table name
     *
     * @param $name
     */
    public function setTableName($name)
    {
        $this->tblName = $name;
    }

    /**
     * set table prefix
     *
     * @param $name
     */
    public function setTablePrefix($name)
    {
        $this->tblPrefix = $name;
    }

    /**
     * get table name
     */
    public function getTableName()
    {
        return $this->tblName;
    }

    /**
     * get table prefix
     */
    public function getTablePrefix()
    {
        return $this->tblPrefix;
    }

    public function getFullTableName()
    {
        return implode('', array_filter(array_map('trim', [
            DB::getTablePrefix(),
            $this->getTablePrefix(),
            $this->getTableName()
        ])));
    }


    /**
     * set options
     *
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }


    /**
     * run migration with current options
     */
    public function run()
    {
        if (empty($this->options)) {
            throw new Exception('UtilError: Options is empty.');
        }

        if (Schema::hasTable($this->getFullTableName())) {

            if($this->preMigrate) {
                Schema::table($this->getFullTableName(), function ($table) {

                    call_user_func_array($this->preMigrate, [$table]);

                });
            }

            foreach ($this->options as $col) {

                $colName = array_get($col, 'name');

                if ($colName != 'id' && Schema::hasColumn($this->getFullTableName(), $colName)) {

                    if ($this->typeChanged(array_get($col, 'name'), array_get($col, 'type'))) {
                        echo 'Column \''.array_get($col, 'name').'\' in Table \''.$this->getFullTableName().'\' is already existed, and have mismatched data type.' . "\r\n";
                    }

                    try {
                        Schema::table($this->getFullTableName(), function ($table) use ($col) {

                            // adjust additional options for current column
                            $this->adjustAdditionalOptions($col, $table);

                        });
                    } catch (QueryException $qe) {
                        if ($qe->getCode() != 42000) {  // if index duplicated, skip and silent
                            throw $qe;
                        }
                    }

                } else {
                    if ($colName != 'id') {

                        Schema::table($this->getFullTableName(), function ($table) use ($col) {

                            $this->createOrUpdateColumns($col, $table);

                        });

                    }
                };

            }

            Schema::table($this->getFullTableName(), function ($table) {

                if (Schema::hasColumn($this->getFullTableName(), 'created_at') && Schema::hasColumn($this->getFullTableName(),
                        'updated_at')
                ) {
                    $table->timestamp('created_at')->nullable()->default(null)->change();
                    $table->timestamp('updated_at')->nullable()->default(null)->change();
                    if (env('REWRITE_ZERO_TIMESTAMPS', false)) {
                        DB::update('update '.$this->getFullTableName().' set created_at = NULL where created_at = ?',
                            ['0000-00-00 00:00:00']);
                        DB::update('update '.$this->getFullTableName().' set updated_at = NULL where updated_at = ?',
                            ['0000-00-00 00:00:00']);
                    }
                }

            });





            if($this->postMigrate) {
                Schema::table($this->getFullTableName(), function ($table) {

                    call_user_func_array($this->postMigrate, [$table]);

                });
            }

        } else {

            $options = $this->options;

            Schema::create($this->getFullTableName(), function (Blueprint $table) use ($options) {

                foreach ($options as $col) {

                    $this->createOrUpdateColumns($col, $table);

                }

                $table->nullableTimestamps();
                $table->softDeletes();
            });

        }

    }


    protected function createOrUpdateColumns($col, $blueprint)
    {
        $name  = array_get($col, 'name');
        $type  = array_get($col, 'type');
        $attrs = array_get($col, 'options', []);

        // prepare attributes
        if (in_array($type, static::$with_default_length)) {
            $length = array_get($col, 'options.length', 255);
            $attrs  = array_merge(compact('length'), $attrs);
        } else {
            if (in_array($type, static::$int_relatives_signed)) {
                $autoIncrement = array_get($col, 'options.autoIncrement', false);
                $unsigned      = array_get($col, 'options.unsigned', false);
                $attrs         = array_merge(compact('autoIncrement', 'unsigned'), $attrs);
            } else {
                if (in_array($type, static::$int_relatives_unsigned)) {
                    $type          = camel_case(substr($type, 8, strlen($type)));
                    $autoIncrement = array_get($col, 'options.autoIncrement', false);
                    $unsigned      = true;
                    $attrs         = array_merge(compact('autoIncrement', 'unsigned'), $attrs);
                } else {
                    if (in_array($type, static::$float_relatives)) {
                        $total  = array_get($col, 'options.total', 8);
                        $places = array_get($col, 'options.places', 2);
                        $attrs  = array_merge(compact('total', 'places'), $attrs);
                    } else {
                        if ($type == 'double') {
                            $total  = array_get($col, 'options.total', 15);
                            $places = array_get($col, 'options.places', 8);
                            $attrs  = array_merge(compact('total', 'places'), $attrs);
                        }
                    }
                }
            }
        }

        // adjust additional options for current column
        $this->adjustAdditionalOptions($col, $blueprint);

        // start invoking add column
        if (in_array($type, static::$supported_types)) {
            $blueprint->$type($name);
        } else {
            $method = new ReflectionMethod('Illuminate\Database\Schema\Blueprint', 'addColumn');
            $method->setAccessible(true);

            if ( ! empty($attrs)) {
                $method->invoke($blueprint, $type, $name, $attrs);
            } else {
                $method->invoke($blueprint, $type, $name);
            }
        }
    }


    protected function adjustAdditionalOptions($col, $blueprint)
    {
        $name    = array_get($col, 'name');
        $unique  = array_get($col, 'unique', false);
        $primary = array_get($col, 'primary', false);
        $index   = array_get($col, 'index', false);
        $foreign = array_get($col, 'foreign', false);

        // additional options
        if ($unique) {
            $blueprint->unique($name);
        } else {
            if ($primary) {
                $blueprint->primary($name);
            } else {
                if ($index) {
                    $blueprint->index($name);
                } else {
                    if ($foreign) {
                        $blueprint->foreign($name);
                    }
                }
            }
        }
    }

    public function typeChanged($colName, $newType) {
        $dbType = DB::connection()->getDoctrineColumn(DB::getTablePrefix().$this->getFullTableName(),
            $colName)->getType()->getName();

        return strtolower(Type::getType($newType)) != strtolower(Type::getType($dbType));
    }

    public function hasColumn($column) {
        return Schema::hasColumn($this->getFullTableName(), $column);
    }

    public function onPreMigrate(Closure $callback) {
        $this->preMigrate = $callback;
    }

    public function onPostMigrate(Closure $callback) {
        $this->postMigrate = $callback;
    }



}
