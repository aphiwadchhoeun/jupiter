<?php namespace Bct\Unionimpactbase\Repositories\MigrationUtil;

use Closure;

interface MigrationUtilRepositoryInterface {

	public function setTableName($name);

	public function setOptions(array $options);

	public function run();

	public function onPreMigrate(Closure $callback);

    public function onPostMigrate(Closure $callback);

    public function hasColumn($column);

    public function typeChanged($colName, $newType);

}
