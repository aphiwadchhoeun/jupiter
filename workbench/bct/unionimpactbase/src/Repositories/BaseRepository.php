<?php namespace Bct\Unionimpactbase\Repositories;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 04.11.15
 * Time: 13:02
 */

use Bct\Unionimpactbase\Models\User;
use Sentinel;
use Cartalyst\Support\Traits;

class BaseRepository  {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\ValidatorTrait;

    protected $currentUser;
    protected $currentUserModule;
    public function __construct()
    {
        $this->currentUser = Sentinel::getUser();
        $this->currentUserModule = (!is_null($this->currentUser)) ? User::find($this->currentUser->id) : null;
    }

}