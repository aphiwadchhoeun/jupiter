<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 3/9/2017
 * Time: 12:20 PM
 */

namespace Bct\Unionimpactbase\Repositories\Snappy;

class SnappyRepository implements SnappyRepositoryInterface
{

    protected $snappy;

    protected $DEFAULT_OPTIONS = [
        'paper'       => 'a4',
        'orientation' => 'landscape',
    ];

    protected $options = [];


    /**
     * SnappyRepository constructor.
     */
    public function __construct()
    {
        $this->snappy = app('snappy.pdf.wrapper');

        $this->DEFAULT_OPTIONS['footer-right'] = config('platform.app.tagline');

        $this->setOptions($this->DEFAULT_OPTIONS);
    }


    /**
     * Set an option
     *
     * @param       $key
     * @param       $value
     */
    public function setOption($key, $value)
    {
        switch ($key) {
            case "title":
                $this->options[$key] = $value;
                break;
            case "filename":
                $this->options[$key] = $value;
                break;
            case "paper":
                $this->options[$key] = $value;
                $this->snappy->setPaper($value);
                break;
            case "orientation":
                $this->options[$key] = $value;
                $this->snappy->setOrientation($value);
                break;
            case "view":
                if (is_array($value)) {
                    list($view, $data) = $value;
                    $this->options[$key] = $view;
                    $data['title']       = array_get($this->options, 'title', '');
                    $this->snappy->loadView($view, $data);
                } else {
                    throw(new \Exception('Snappy - View: Param must be an array.'));
                }
                break;
            default:
                $this->options[$key] = $value;
                $this->snappy->setOption($key, $value);
        }
    }


    /**
     * Set options array
     *
     * @param $options
     *
     */
    public function setOptions($options)
    {
        foreach ($options as $k => $v) {
            $this->setOption($k, $v);
        }
    }


    /**
     * Get value of an option
     *
     * @param $key
     *
     * @return mixed
     */
    public function getOption($key)
    {
        return array_get($this->options, $key, '');
    }


    /**
     * Return pdf download
     *
     * @return mixed
     */
    public function export()
    {
        return response($this->snappy->output(), 200)->header('Content-Type',
            'application/pdf')->header('Content-Disposition',
            'attachment; filename="'.$this->getOption('filename').'.pdf');
    }

}