<?php namespace Bct\Unionimpactbase\Repositories\Snappy;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 3/9/2017
 * Time: 12:17 PM
 */
interface SnappyRepositoryInterface
{

    /**
     * Set an option
     *
     * @param       $key
     * @param       $value
     *
     * @return
     */
    public function setOption($key, $value);


    /**
     * Set options array
     *
     * @param $options
     *
     */
    public function setOptions($options);


    /**
     * Get value of an option
     *
     * @param $key
     *
     * @return mixed
     */
    public function getOption($key);


    /**
     * Return pdf download
     *
     * @return mixed
     */
    public function export();

}