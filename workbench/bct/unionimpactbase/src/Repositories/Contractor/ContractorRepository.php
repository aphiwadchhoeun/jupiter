<?php namespace Bct\Unionimpactbase\Repositories\Contractor;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 17.12.15
 * Time: 15:23
 */

use Bct\Unionimpactbase\Models\UserProfiles;
use Bct\Unionimpactbase\Repositories\BaseRepository;
use Carbon\Carbon;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Platform\Roles\Models\Role;
use Platform\Users\Models\User;
use Validator;

class ContractorRepository extends BaseRepository implements ContractorRepositoryInterface
{

    /**
     * The Data handler.
     *
     * @var \Bct\Unionimpactbase\Handlers\DataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent model.
     *
     * @var string
     */
    protected $model;


    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app)
    {
        parent::__construct();

        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->setValidator($app['bct.unionimpactbase.user.profile.contractor.validator']);
    }


    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this->createModel();
    }


    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->container['cache']->rememberForever('bct.unionimpactbase.contractor.all', function () {
            return $this->createModel()->get();
        });
    }


    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->grid()->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input)
    {
        return $this->validator->on('create')->validate($input);
    }


    /**
     * {@inheritDoc}
     */
    public function validForUpdate($data, array $input)
    {
        return $this->validator->on('update')->setCustomAttributes([ $data ])->validate($input);
    }

    public function registerUserForContractor(User $user_registered, $role_name, $data)
    {
        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            $user = \Bct\Unionimpactbase\Models\User::find($user_registered->id);

            if (!is_null($user) && !is_null($user->profile) && $user->profile->hasContractor()) {
                return ['You are already linked to Contractor', false];
            }

            try {
                if (!$user->profile) {

                    $profile = new UserProfiles();
                    $profile->fill($data)->save();

                    $user->profile()->associate($profile)->save();
                } else {
                    $user->profile()->fill($data)->push();
                }

                if (!$user->inRole($role_name) && $role_mrp_user = Role::where('slug', $role_name)->first()) {
                    $user->roles()->attach($role_mrp_user->id);
                }
            } catch (\Exception $e) {
                return [$e->getMessage(), false];
            }

            return [ "", true ];
        }

        return [ $messages, false ];

    }

}
