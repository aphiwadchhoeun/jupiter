<?php namespace Bct\Unionimpactbase\Repositories\Contractor;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 17.12.15
 * Time: 15:24
 */

interface ContractorRepositoryInterface
{

    public function grid();


    public function findAll();


    public function find($id);

}