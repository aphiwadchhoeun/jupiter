<?php namespace Bct\Unionimpactbase\Repositories\ExportQueue;

use Bct\Unionimpactbase\Models\PdfQueue;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;

class ExportQueueRepository implements ExportQueueRepositoryInterface {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait;

    /**
     * The Eloquent unionimpactbase model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     * @return void
     */
    public function __construct(Container $app) {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->setModel(get_class($app['Bct\Unionimpactbase\Models\ExportQueue']));
    }

    /**
     * {@inheritDoc}
     */
    public function grid() {
        return $this
            ->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll() {
        return $this->container['cache']->rememberForever('bct.unionimpactbase.pdfqueue.all', function () {
            return $this->createModel()->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function find($id) {
        return $this->container['cache']->rememberForever('bct.unionimpactbase.pdfqueue.' . $id, function () use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input) {
        // Create a new pdfqueue
        $pdfqueue = $this->createModel();

        // Fire the 'bct.unionimpactbase.pdfqueue.creating' event
        if ($this->fireEvent('bct.unionimpactbase.pdfqueue.creating', [$input]) === false) {
            return false;
        }

        // Save the pdfqueue
        $pdfqueue->fill($input)->save();

        // Fire the 'bct.unionimpactbase.pdfqueue.created' event
        $this->fireEvent('bct.unionimpactbase.pdfqueue.created', [$pdfqueue]);

        return $pdfqueue;
    }

    /**
     * Updates the specified entity
     *
     * @param $id
     * @param array $input
     * @return mixed
     */
    public function update($id, array $input) {
        // Get the pdfqueue object
        $pdfqueue = $this->find($id);

        // Fire the 'bct.unionimpactbase.pdfqueue.updating' event
        if ($this->fireEvent('bct.unionimpactbase.pdfqueue.updating', [$pdfqueue, $input]) === false) {
            return false;
        }

        // Save the pdfqueue
        $pdfqueue->fill($input)->save();

        // Fire the 'bct.unionimpactbase.pdfqueue.updated' event
        $this->fireEvent('bct.unionimpactbase.pdfqueue.updated', [$pdfqueue]);

        return $pdfqueue;
    }

}
