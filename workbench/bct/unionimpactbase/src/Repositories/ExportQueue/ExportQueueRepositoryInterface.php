<?php namespace Bct\Unionimpactbase\Repositories\ExportQueue;

interface ExportQueueRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Bct\Unionimpactbase\Models\PdfQueue
	 */
	public function grid();

	/**
	 * Returns all the unionimpactbase entries.
	 *
	 * @return \Bct\Unionimpactbase\Models\PdfQueue
	 */
	public function findAll();

	/**
	 * Returns a unionimpactbase entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Bct\Unionimpactbase\Models\PdfQueue
	 */
	public function find($id);

	/**
	 * Creates the given entity.
	 *
	 * @param  array $input
	 * @return array|bool
	 */
	public function create(array $input);

	/**
	 * Updates the specified entity
	 *
	 * @param $id
	 * @param array $input
	 * @return mixed
	 */
	public function update($id, array $input);

}
