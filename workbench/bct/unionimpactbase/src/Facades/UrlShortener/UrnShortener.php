<?php namespace Bct\Unionimpactbase\Facades\UrlShortener;

use Illuminate\Support\Facades\Facade;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 4/3/2017
 * Time: 4:25 PM
 */
class UrlShortener extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'bct.unionimpactbase.url-shortener';
    }

}