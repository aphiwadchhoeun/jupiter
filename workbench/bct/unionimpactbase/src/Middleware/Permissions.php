<?php

/**
 * Part of the Platform Access extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Access extension
 * @version    3.1.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace Bct\Unionimpactbase\Middleware;

use Closure;
use Cartalyst\Alerts\Alerts;
use Cartalyst\Sentinel\Sentinel;

class Permissions
{
    /**
     * The sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $auth;

    /**
     * The alerts instance.
     *
     * @var \Cartalyst\Alerts\Alerts
     */
    protected $alerts;

    /**
     * Create a new middleware instance.
     *
     * @param  \Cartalyst\Sentinel\Sentinel  $auth
     * @return void
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get the prepared permissions
        $permissions = app('Platform\Permissions\Repositories\PermissionsRepositoryInterface')->getPreparedPermissions();

        // Get the permission name based on the current page
        $permission = array_get($permissions, $request->route()->getActionName());

        // Check if the user has access
        if ($this->auth->hasAnyAccess(['superuser', $permission])) {
            return $next($request);
        }

        $message = $permission ? 'no_access_to' : 'no_access';

        app('alerts')->error(trans('platform/access::permissions.'.$message, compact('permission')));

        if ($request->ajax()) {
            return response()->json(['error' => trans('platform/access::permissions.no_access')], 403);
        }

        return redirect('/');
    }
}
