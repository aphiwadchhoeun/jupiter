<?php

/*
|--------------------------------------------------------------------------
| Extension Hooks
|--------------------------------------------------------------------------
|
| Hooks for various stages of an Extension's lifecycle. You can access the
| individual extension properties through $extension->getSlug().
|
*/
use Platform\Menus\Models\Menu;


Extension::registering(function (Extension $extension) {
    // Before an extension is registered (happens for every extension)
});

Extension::registered(function (Extension $extension) {
    // After an extension is registered
});

Extension::booting(function (Extension $extension) {
    // Before an installed and enabled extension boots (after all are registered)
});

Extension::booted(function (Extension $extension) {
    // After an installed and enabled extension boots
});

Extension::installing(function (Extension $extension) {
    // Before an extension is installed
});

Extension::installed(function (Extension $extension) {
    // After an extension is installed

    \Illuminate\Support\Facades\Cache::flush();
});

Extension::uninstalling(function (Extension $extension) {
    // Before an extension is uninstalled
});

Extension::uninstalled(function (Extension $extension) {
    // After an extension is uninstalled
});

Extension::enabling(function (Extension $extension) {
    // Before an extension is enabled
});

Extension::enabled(function (Extension $extension) {
    if ($extension->getSlug() == 'bct/unionimpactbase') {
        // After an extension is enabled
        Menu::whereSlug('account-menu')->update(['regex' => '']);
        Menu::whereSlug('account-profile')->update(['enabled' => false]);
    }

    setRouteEnabledConfigForExtension($extension, 'enabled');
});

Extension::disabling(function (Extension $extension) {
    // Before an extension is disabled
});

Extension::disabled(function (Extension $extension) {
    // After an extension is disabled

    setRouteEnabledConfigForExtension($extension, 'disabled');
});

Extension::upgrading(function (Extension $extension) {
    // Before an extension is upgraded
});

Extension::upgraded(function (Extension $extension) {
    // After an extension is upgraded
});


function setRouteEnabledConfigForExtension($extension, $action)
{
    $extension_slug = $extension->getSlug();
    $extension_config_route_name = 'bct.route.enabled.' . str_replace('bct/', '', $extension_slug);

    $config_bct_extensions_dependents = collect(config('bct.extensions.dependents'));
    $extension_dependents = collect(data_get($config_bct_extensions_dependents, $extension_slug));

    if (!$config_bct_extensions_dependents->isEmpty() && $action === 'enabled' && !$extension_dependents->isEmpty()) {

        app('cache')->forever($extension_config_route_name, true);

    } elseif (!$config_bct_extensions_dependents->isEmpty() && $action === 'disabled' && !$extension_dependents->isEmpty()) {

        app('cache')->forever($extension_config_route_name, false);

    } elseif ($extension_dependents->isEmpty()) {

        foreach ($config_bct_extensions_dependents as $extension_config_slug => $extension_dependents) {
            if (in_array($extension_slug, $extension_dependents)) {

                $extension_config_route_name = 'bct.route.enabled.' . str_replace('bct/', '', $extension_config_slug);

                if ($action === 'enabled') {
                    app('cache')->forever($extension_config_route_name, false);
                } else {
                    app('cache')->forever($extension_config_route_name, true);
                }
            }
        }

    }

}