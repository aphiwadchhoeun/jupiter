<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/2/2016
 * Time: 6:03 PM
 */

namespace Bct\Unionimpactbase\Queues;


use App\Jobs\Job;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BaseJob extends Job {

    use InteractsWithQueue, SerializesModels;

    protected $filename;
    protected $results;
    protected $options;
    protected $export;

    /**
     * BaseJob constructor.
     * @param $filename
     * @param $results
     * @param $options
     */
    public function __construct($filename, $results, $options) {
        $this->filename = $filename;
        $this->results = $results;
        $this->options = $options;
    }

    /**
     * Job fail
     */
    public function failed() {
        // prepare callback url
        $callback_url = array_get($this->options, 'callback_url', '');
        $params = [
            'path'   => '',
            'status' => -1
        ];
        $url = $callback_url . '?' . http_build_query($params);

        // start http request to callback url
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $client->get($url);

        if ((bool)env('APP_DEBUG') == false) {
            app('bct.unionimpactbase.dev.mailer')->devAlert($this, $callback_url);
        }
    }

    /**
     * After queue is done successfully
     */
    protected function postQueue($filepath) {
        // prepare callback url
        $callback_url = array_get($this->options, 'callback_url', '');
        $params = [
            'path'   => $filepath,
            'status' => 1
        ];
        $url = $callback_url . '?' . http_build_query($params);

        // start http request to callback url
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $client->get($url);
    }

    /**
     * Uploading file in directory path /queue/{USER_ID}/
     *
     * @param $content
     * @param $ext
     * @return string
     */
    protected function uploadFile($content, $ext) {
        $filesystem = app()->make('filesystem');

        $filename = $this->getUploadFileName($ext);

        $filepath = $this->getFilePath($filename, $this->options['user_id']);

        $filesystem->disk('s3')->put($filepath, $content);

        return $filepath;
    }

    protected function getFilePath($filename, $userid) {
        return "queue/{$userid}/{$filename}";
    }

    protected function getUploadFileName($ext) {
        return md5(Carbon::now()->timestamp) . '.' . $ext;
    }

}