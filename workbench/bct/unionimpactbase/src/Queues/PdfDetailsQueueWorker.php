<?php namespace Bct\Unionimpactbase\Queues;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 15.04.16
 * Time: 12:44
 */

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 3/22/2016
 * Time: 5:45 PM
 */
class PdfDetailsQueueWorker extends BaseJob implements SelfHandling, ShouldQueue {

    /*
     * THIS WORKER NEED TO BE RUN ON QUEUE "queue_jobs"
     */

    /**
     * PdfQueueWorker constructor.
     * @param $filename
     * @param $results
     * @param $options
     */
    public function __construct($filename, $results, $options) {
        parent::__construct($filename, $results, $options);
    }

    /**
     * Job run
     */
    public function handle() {
        $orientation = array_get($this->options, 'orientation', 'landscape');

        $snappy = app('snappy.pdf.wrapper');

        $snappy->setPaper('a4')
            ->setOrientation($orientation)
            ->setOption('header-line', false)
            ->setOption('header-center', '')
            ->setOption('footer-right', config('platform.app.tagline'));

        if ($option_margin_bottom = array_get($this->options, 'margin-bottom')) {
            $snappy = $snappy->setOption('margin-bottom', $option_margin_bottom);
        }

        if ($option_footer_html = array_get($this->options, 'footer-html')) {
            $snappy = $snappy->setOption('footer-html', $option_footer_html);
        }

        $snappy->loadView($this->options['pdf_view'],
            [
                'results' => isset($this->results) ? $this->results : [],
                'title'   => array_get($this->options, 'title', ''),
            ]);

        // start uploading file
        $filepath = $this->uploadFile($snappy->output(), 'pdf');

        $this->postQueue($filepath);
    }

}