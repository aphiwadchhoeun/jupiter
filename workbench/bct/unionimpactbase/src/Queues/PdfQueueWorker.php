<?php namespace Bct\Unionimpactbase\Queues;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 3/22/2016
 * Time: 5:45 PM
 */
class PdfQueueWorker extends BaseJob implements SelfHandling, ShouldQueue {

    /*
     * THIS WORKER NEED TO BE RUN ON QUEUE "queue_jobs"
     */
    /**
     * PdfQueueWorker constructor.
     * @param $filename
     * @param $results
     * @param $options
     */
    public function __construct($filename, $results, $options) {
        parent::__construct($filename, $results, $options);
    }


    /**
     * Job run
     * @throws \Exception
     */
    public function handle() {
        $orientation = array_get($this->options, 'orientation', 'landscape');

        $snappy = app('snappy.pdf.wrapper');

        $snappy->setPaper('a4')
            ->setOrientation($orientation)
            ->setOption('header-line', false)
            ->setOption('header-center', '')
            ->setOption('footer-right', config('platform.app.tagline'));

        $snappy->loadView($this->options['pdf_view'], [
                'keys'            => array_get($this->options, 'keys', []),
                'results'         => isset($this->results) ? $this->results : [],
                'defaultTitle'    => array_get($this->options, 'default_title', ''),
                'defaultSubTitle' => array_get($this->options, 'default_sub_title', ''),
                'exportPageInfo'  => array_get($this->options, 'export_page_info', ''),
                'params'          => array_get($this->options, 'params', [])
            ]);

        // start uploading file
        $filepath = $this->uploadFile($snappy->output(), 'pdf');

        $this->postQueue($filepath);
    }

}