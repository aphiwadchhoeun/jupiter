<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 4/4/2016
 * Time: 3:12 PM
 */

namespace Bct\Unionimpactbase\Queues;


use Bct\Reports\Models\Config;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Drawing;


class XlsQueueWorker extends BaseJob implements SelfHandling, ShouldQueue {

    /*
     * THIS WORKER NEED TO BE RUN ON QUEUE "queue_jobs"
     */

    /**
     * XlsQueueWorker constructor.
     * @param $filename
     * @param $results
     * @param $options
     */
    public function __construct($filename, $results, $options) {
        parent::__construct($filename, $results, $options);
    }

    /**
     * Job run
     */
    public function handle() {
        $header = $this->escapeHeaderWithID($this->options['keys']);
        $sheet = $this->results;
        $columns_width = array_values($this->options['columns_width']);


        $template_config = Config::findByItem('bct.reports.settings.xls_template')->first();
        if ($template_config !== '' && $template_config !== null && isset($template_config->value) && $template_config->value !== '') {

            $template_path = $template_config->value;
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(false);
            $objPHPExcel = $objReader->load($template_path);
            $sc = $objPHPExcel->getActiveSheet()->copy();
            $clonedSheet = clone $sc;
            $temporarySheet = clone $clonedSheet;
            $temporarySheet->setTitle('temp');
            $objPHPExcel->addSheet($temporarySheet, 0);
            $objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($temporarySheet));

            $start_cell = Config::findByItem('bct.reports.settings.start_cell')->first();
            if ($start_cell !== '' && $start_cell !== null && isset($start_cell->value) && $start_cell->value !== '') {
                $start_pos = $start_cell->value;
            }
        } else {
            $objPHPExcel = new PHPExcel();
            $start_pos = 'A1';
        }

        $column = substr($start_pos, 0, 1);
        $row = (int)substr($start_pos, 1);
        $header_cell = $column . $row;
        $sheet_cell = $column . ++$row;


        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->fromArray($header, null, $header_cell);
        $objPHPExcel->getActiveSheet()->fromArray($sheet, null, $sheet_cell);

        $font = $objPHPExcel->getDefaultStyle()->getFont();
        foreach ($columns_width as $width) {
            $width = str_replace('px', '', $width);
            $width = (int)PHPExcel_Shared_Drawing::pixelsToCellDimension($width, $font);
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setAutoSize(false);
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($width);

            $column++;

        }

        if (isset($temporarySheet)) {
            $sheetCount = $objPHPExcel->getSheetCount();
            $objPHPExcel->removeSheetByIndex($sheetCount - 1);
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        //Temporarly saving file to browser
        ob_start();
        $objWriter->save('php://output');
        $content = ob_get_clean();


        // start uploading file
        $filepath = $this->uploadFile($content, 'xlsx');

        $this->postQueue($filepath);
    }

    protected function escapeHeaderWithID($headers) {
        if (is_array($headers) && !empty($headers)) {
            if (str_limit($headers[0], 2) == 'ID') {
                $headers[0] = "'" . $headers[0];
            }
        }

        return $headers;
    }

}