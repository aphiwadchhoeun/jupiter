<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 4/4/2016
 * Time: 3:12 PM
 */

namespace Bct\Unionimpactbase\Queues;


use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use League\Csv\Writer;

class CsvQueueWorker extends BaseJob implements SelfHandling, ShouldQueue {

    /*
     * THIS WORKER NEED TO BE RUN ON QUEUE "queue_jobs"
     */

    /**
     * CsvQueueWorker constructor.
     * @param $filename
     * @param $results
     * @param $options
     */
    public function __construct($filename, $results, $options) {
        parent::__construct($filename, $results, $options);
    }

    /**
     * Job run
     */
    public function handle() {
        $writer = Writer::createFromFileObject(new \SplTempFileObject());
        $writer->insertOne($this->escapeHeaderWithID($this->options['keys']));
        $writer->insertAll($this->prepareData($this->results));
        $content = $writer->__toString();

        // start uploading file
        $filepath = $this->uploadFile($content, 'csv');

        $this->postQueue($filepath);
    }

    protected function prepareData($data) {
        $data = [];
        foreach ($this->results as $r) {
            $temp = [];
            foreach ($r as $c) {
                if (is_array($c)) {
                    array_push($temp, $c['value']);
                } else {
                    array_push($temp, $c);
                }
            }
            array_push($data, $temp);
        }

        return $data;
    }

    protected function escapeHeaderWithID($headers) {
        if (is_array($headers) && !empty($headers)) {
            if (str_limit($headers[0], 2) == 'ID') {
                $headers[0] = "'" . $headers[0];
            }
        }

        return $headers;
    }

}