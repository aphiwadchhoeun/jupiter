<?php
/**
 * Created by PhpStorm.
 * User: sky
 * Date: 08.04.16
 * Time: 15:09
 */

use Bct\Unionimpactbase\Models\CustomContent;

if ( ! function_exists('ccontent')) {
    /**
     * @param      $key
     * @param null $default
     *
     * @return null
     */
    function ccontent($key, $default = null)
    {
        try {
            $option = CustomContent::where('slug', $key)->first();

            if (empty($option)) {
                return $default;
            }

            if (is_numeric($option->value)) {
                if ((int)$option->value == $option->value) {
                    return intval($option->value);
                }

                return (float)$option->value;
            }

            return $option->value;
        } catch (PDOException $e) {
            return $default;
        }
    }
}

if ( ! function_exists('get_extension_alias__uib')) {
    /**
     *  @return string
     */
    function get_extension_alias__uib()
    {
        return app('extension.alias.uib');
    }
}

if ( ! function_exists('get_extension_slug__uib')) {
    /**
     *  @return string
     */
    function get_extension_slug__uib()
    {
        return app('extension.slug.uib');
    }
}

if ( ! function_exists('getQueueDefaultName')) {
    /**
     *  @return string
     */
    function getQueueDefaultName()
    {
        return config(get_extension_alias__uib() . '.common.queue_name');
    }

}

if ( ! function_exists('blade_helper__uib')) {
    /**
     * Call only from blade!!
     *
     * @param $view
     *
     * @return string
     */
    function blade_helper__uib($view)
    {
        return get_extension_slug__uib() . '::' . $view;
    }
}

if ( ! function_exists('trans_helper__uib')) {
    /**
     * Call only from blade!!
     *
     * @param        $id
     * @param array  $parameters
     * @param string $domain
     * @param null   $locale
     *
     * @return string
     */
    function trans_helper__uib($id, $parameters = [], $domain = 'messages', $locale = null)
    {
        return trans(get_extension_slug__uib() . '::' . $id, $parameters, $domain, $locale);
    }
}

if ( ! function_exists('trans_choice_helper__uib')) {
    /**
     * Call only from blade!!
     *
     * @param        $id
     * @param array  $parameters
     * @param string $domain
     * @param null   $locale
     *
     * @return string
     */
    function trans_choice_helper__uib($id, $parameters = [], $domain = 'messages', $locale = null)
    {
        return trans(get_extension_slug__uib() . '::' . $id, $parameters, $domain, $locale);
    }
}

if ( ! function_exists('route_helper__uib')) {
    /**
     * @param       $name
     * @param array $parameters
     * @param bool  $absolute
     * @param null  $route
     *
     * @return string
     */
    function route_helper__uib($name, $parameters = [], $absolute = true, $route = null)
    {
        return route(get_extension_alias__uib() . '.' . $name, $parameters, $absolute, $route);
    }
}

if ( ! function_exists('getLogo')) {

    /**
     * Get custom logo url
     *
     */
    function getLogo()
    {
        if (\Illuminate\Support\Facades\File::exists(public_path('files/logo.png'))) {
            return asset('files/logo.png');
        }

        return Asset::getUrl('app/img/logo.png');
    }
}

if ( ! function_exists('getQueueDefaultName')) {
    /**
     *  @return string
     */
    function getQueueDefaultName()
    {
        return config('bct.uib.common.queue_name');
    }

}

if ( ! function_exists('view_helper')) {
    /**
     * Call only from PHP code (not blade views)!
     *
     * @param       $view
     * @param array $data
     * @param array $mergeData
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function view_helper($view, $data = [], $mergeData = [])
    {
        $extension = get_extension_from_called_function(debug_backtrace());
        return view($extension . '::' . $view, $data, $mergeData);
    }

}

if ( ! function_exists('trans_helper')) {
    /**
     * Call only from PHP code (not blade views)!
     *
     * @param        $id
     * @param array  $parameters
     * @param string $domain
     * @param null   $locale
     *
     * @return string
     */
    function trans_helper($id, $parameters = [], $domain = 'messages', $locale = null)
    {
        $extension = get_extension_from_called_function(debug_backtrace());
        return trans($extension . '::' . $id, $parameters, $domain, $locale);
    }

}

if ( ! function_exists('trans_choice_helper')) {
    /**
     * Call only from PHP code (not blade views)!
     *
     * @param        $id
     * @param        $number
     * @param array  $parameters
     * @param string $domain
     * @param null   $locale
     *
     * @return string
     */
    function trans_choice_helper($id, $number, array $parameters = [], $domain = 'messages', $locale = null)
    {
        $extension = get_extension_from_called_function(debug_backtrace());
        return trans_choice($extension . '::' . $id, $number, $parameters, $domain, $locale);
    }

}

if ( ! function_exists('get_extension_from_called_function')) {
    /**
     * @param array $debug_backtrace
     *
     * @return string
     * @throws Exception
     */
    function get_extension_from_called_function(array $debug_backtrace)
    {
        if (!preg_match('/.*\/(bct\/[^\/]+)\//', $debug_backtrace[0]['file'], $matches)) {
            throw new \Exception('Call view from undefined extension');
        }

        return $matches[1];
    }

}