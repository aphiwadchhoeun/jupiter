<?php namespace Bct\Unionimpactbase\Mailer\Dev;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/9/2016
 * Time: 3:57 PM
 */

use Cartalyst\Support\Mailer;

class DevAlert extends Mailer {

    public function devAlert($source, $data = null) {
        // Set the email subject
        $this->setSubject(trans('bct/unionimpactbase::dev/common.subjects.dev_alert'));

        // Set the email recipient
        $this->addTo('dev@businesscomputertechnicians.com', 'Dev'); // in beta testing

        // Set the email view
        $this->setView(
            "bct/unionimpactbase::emails/dev/dev_alert",
            [
                'source' => get_class($source),
                'data'   => $data
            ]
        );

        // Send the email
        $this->send();
    }

}