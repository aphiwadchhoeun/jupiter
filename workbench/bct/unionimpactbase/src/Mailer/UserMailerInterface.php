<?php namespace Bct\Unionimpactbase\Mailer;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.10.15
 * Time: 12:09
 */

use Cartalyst\Sentinel\Users\UserInterface;

interface UserMailerInterface {

    /**
     * Send a generic email to the user.
     *
     * @param  \Cartalyst\Sentinel\Users\UserInterface  $user
     * @param  string  $subject
     * @param  string  $view
     * @param  array  $data
     * @return void
     */
    public function genericUserEmail($user, $subject, $view, array $data = []);
}
