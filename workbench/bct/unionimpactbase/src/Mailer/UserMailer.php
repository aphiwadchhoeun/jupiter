<?php namespace Bct\Unionimpactbase\Mailer;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.10.15
 * Time: 12:08
 */

use Cartalyst\Support\Mailer;
use Cartalyst\Sentinel\Users\UserInterface;

class UserMailer extends Mailer implements UserMailerInterface {

    /**
     * {@inheritDoc}
     */
    public function genericUserEmail($user, $subject, $view, array $data = [])
    {
        // Prepare the email recipient
        $recipient = [ $user->email, "{$user->first_name} {$user->last_name}" ];

        // Prepare the email
        $this->sendEmail($subject, $recipient, $view, $data);
    }

    /**
     * Prepares the email by setting the necessary properties
     * and finally sends the email to the given recipient.
     *
     * @param  string  $subject
     * @param  array  $recipient
     * @param  string  $view
     * @param  array  $data
     * @return void
     */
    protected function sendEmail($subject, array $recipient, $view, array $data)
    {
        // Set the email subject
        $this->setSubject($subject);

        // Set the email recipient
        $this->addTo(
            array_get($recipient, 0),
            array_get($recipient, 1)
        );

        // Set the email view
        $this->setView(
            "bct/unionimpactbase::emails/{$view}",
            array_merge(compact('user'), $data)
        );

        // Send the email
        $this->send();
    }

}
