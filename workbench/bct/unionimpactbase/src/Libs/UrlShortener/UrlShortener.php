<?php namespace Bct\Unionimpactbase\Libs\UrlShortener;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 4/3/2017
 * Time: 4:40 PM
 */
class UrlShortener
{

    protected $domain, $endpoint, $token;


    /**
     * UrlShortener constructor.
     */
    public function __construct()
    {
        $this->domain   = config('url-shortener.domain');
        $this->endpoint = config('url-shortener.endpoint');
        $this->token    = config('url-shortener.token');
    }


    /**
     * Shorten the given url
     *
     * @param $url
     */
    public function shorten($url)
    {
        $ch      = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $this->buildApiUrl($url));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    protected function buildApiUrl($long_url)
    {
        return $this->domain.$this->endpoint.'?key='.$this->token.'&url='.$long_url;
    }

}