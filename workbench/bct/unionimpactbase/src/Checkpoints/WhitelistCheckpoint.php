<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/15/2016
 * Time: 4:05 PM
 */

namespace Bct\Unionimpactbase\Checkpoints;


use Cartalyst\Sentinel\Checkpoints\CheckpointInterface;
use Cartalyst\Sentinel\Throttling\ThrottleRepositoryInterface;
use Cartalyst\Sentinel\Users\UserInterface;

class WhitelistCheckpoint implements CheckpointInterface {
    /**
     * @var ThrottleRepositoryInterface
     */
    private $throttle;
    /**
     * @var
     */
    private $ipAddress;


    /**
     * WhitelistCheckpoint constructor.
     * @param ThrottleRepositoryInterface $throttle
     * @param $ipAddress
     */
    public function __construct(ThrottleRepositoryInterface $throttle, $ipAddress) {

        $this->throttle = $throttle;
        $this->ipAddress = $ipAddress;
    }

    /**
     * Checkpoint after a user is logged in. Return false to deny persistence.
     *
     * @param  \Cartalyst\Sentinel\Users\UserInterface $user
     * @return bool
     */
    public function login(UserInterface $user) {
        $user_roles = $user->roles()->get(['slug'])->pluck('slug');

        foreach ($user_roles as $role) {
            if ($config = config('whitelist.client.' . $role, false)) {
                if (!in_array($this->ipAddress, explode("\r\n", $config))) {
                    throw new WhitelistException("Your access privilege is not allowed for this IP address [{$this->ipAddress}]");
                }
            }
        }

    }

    /**
     * Checkpoint for when a user is currently stored in the session.
     *
     * @param  \Cartalyst\Sentinel\Users\UserInterface $user
     * @return bool
     */
    public function check(UserInterface $user) {
    }

    /**
     * Checkpoint for when a failed login attempt is logged. User is not always
     * passed and the result of the method will not affect anything, as the
     * login failed.
     *
     * @param  \Cartalyst\Sentinel\Users\UserInterface $user
     * @return void
     */
    public function fail(UserInterface $user = null) {
    }

}