<?php namespace Bct\Unionimpactbase\Dispatchers;

use Bct\Projects\Queues\FieldReportsQueueWorker;
use Bct\Unionimpactbase\Queues\CsvQueueWorker;
use Bct\Unionimpactbase\Queues\PdfDetailsQueueWorker;
use Bct\Unionimpactbase\Queues\PdfQueueWorker;
use Bct\Unionimpactbase\Queues\XlsQueueWorker;
use Bct\Unionimpactbase\Traits\ExportQueueHelper;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/15/2016
 * Time: 1:37 PM
 */
class ExportQueueDispatcher {

    use DispatchesJobs, ExportQueueHelper;

    protected $filename, $type, $data, $options;

    /**
     * Dispatch job to queue
     *
     * @param string $filename Name of file to be saved
     * @param string $type Can be 'pdf', 'pdf-details', 'csv', 'xls'
     * @param array $data Data to be rendered
     * @param array $options Additional options
     */
    public function dispatchJob($filename, $type, $data = [], $options = []) {
        $this->filename = $filename;
        $this->type = $type;
        $this->data = $data;
        $this->options = $options;

        // before dispatch job
        $this->preDispatch();

        $job = null;
        if ($type == 'pdf') {
            $job = (new PdfQueueWorker($this->filename, $this->data, $this->options))
                ->onQueue(getQueueDefaultName());

        } else if ($type == 'pdf-details') {
            $job = (new PdfDetailsQueueWorker($this->filename, $this->data, $this->options))
                ->onQueue(getQueueDefaultName());

        } else if ($type == 'pdf-field-reports') {
            $job = (new FieldReportsQueueWorker($this->filename, $this->data, $this->options))
                ->onQueue(getQueueDefaultName());

        } else if ($type == 'csv') {
            $job = (new CsvQueueWorker($this->filename, $this->data, $this->options))
                ->onQueue(getQueueDefaultName());

        } else if ($type == 'xls') {
            $job = (new XlsQueueWorker($this->filename, $this->data, $this->options))
                ->onQueue(getQueueDefaultName());

        }

        if ($job != null) {
            $this->dispatch($job);
        }

        // after dispatch job
        $this->postDispatch();
    }

    /**
     * Execute before dispatch job
     */
    protected function preDispatch() {
        $this->createExportEntry($this->filename, $this->type, $this->options);
    }

    /**
     * Execute after dispatch job
     */
    protected function postDispatch() {

    }


}