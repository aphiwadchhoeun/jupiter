<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/19/2016
 * Time: 2:26 PM
 */

namespace Bct\Unionimpactbase\Controllers\Admin\Settings;

use Bct\Unionimpactbase\Models\Config;
use Platform\Roles\Models\Role;


class WhitelistSettingsController extends UnionImpactSettingsController {

    const CONFIG_PREFIX = 'whitelist.client.';

    /**
     * displaying whitelist admin panel
     */
    public function showWhitelist() {
        $roles = Role::orderBy('name')
            ->get(['slug', 'name']);

        return view('bct/unionimpactbase::settings.whitelist.index', compact('roles'));
    }

    /**
     * saving whitelist settings
     */
    public function processWhitelist() {
        try {
            $whitelist_config = Config::whereItem($this->getConfigKey(static::CONFIG_PREFIX . input('whitelist_role')))
                ->first();

            if (!$whitelist_config) {
                $whitelist_config = new Config();
            }

            $whitelist_config->fill([
                'item'  => $this->getConfigKey(static::CONFIG_PREFIX . input('whitelist_role')),
                'value' => input('whitelist_clients')
            ]);
            $whitelist_config->save();

            $this->alerts->success(trans('bct/unionimpactbase::admin/whitelist/message.success.whitelist_saved'));

            return redirect()->back();

        } catch (\Exception $e) {
            $this->alerts->error(trans("bct/unionimpactbase::admin/whitelist/message.error.whitelist_saving"));

            return redirect()->route('admin.whitelist');
        }
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getWhitelistClient($slug) {
        return config($this->getConfigKey(static::CONFIG_PREFIX . $slug), null);
    }

}