<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/24/2016
 * Time: 12:10 PM
 */

namespace Bct\Unionimpactbase\Controllers\Admin\Settings;


use Platform\Access\Controllers\AdminController;


class UnionImpactSettingsController extends AdminController {

    /**
     * @param $key
     * @return string
     */
    protected function getConfigKey($key) {
        return 'bct.unionimpactbase.' . $key;
    }

}