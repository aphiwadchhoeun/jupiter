<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/19/2016
 * Time: 2:26 PM
 */

namespace Bct\Unionimpactbase\Controllers\Admin\Settings;


use Bct\Unionimpactbase\Models\Config;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class GeneralSettingsController extends UnionImpactSettingsController {

    const CARTODB_API_URL = 'cartodb.cartodb_base_api_url';
    const CARTODB_TABLE = 'cartodb.regions_table';
    const SHOW_POLICY = 'show_policy', SHOW_HELP = 'show_help';
    const LOGO_PATH = 'themes/frontend/jupiter/assets/app/img';


    protected $error;

    /**
     * {@inheritDoc}
     */
    public function getError() {
        return $this->error;
    }

    /**
     * {@inheritDoc}
     */
    public function setError($error) {
        $this->error = $error;
    }


    /**
     * displaying cartodb admin panel
     */
    public function showSettings() {
        return view('bct/unionimpactbase::settings.general.index');
    }

    /**
     * saving cartodb settings
     */
    public function processSettings() {
        try {
            // saving app logo
            $this->saveAppLogo();

            // saving cartodb table name into config
            $this->saveCartodbConfigs();

            // saving show policy link config
            $this->saveShowPolicyLink();

            // saving show help link config
            $this->saveShowHelpLink();

            $this->alerts->success(trans('bct/unionimpactbase::admin/general/message.success.save'));

            return redirect()->back();

        } catch (\Exception $e) {
            $error_message = $this->getError() ? $this->getError() : trans("bct/unionimpactbase::admin/general/message.error.save");
            $this->alerts->error($error_message);

            return redirect()->route('admin.general');
        }
    }

    /**
     * Saving custom app logo
     */
    protected function saveAppLogo() {
        if (request()->file('app_logo')) {
            $file = request()->file('app_logo');

            if ($this->validForUpload($file)) {
                $file->move(public_path(static::LOGO_PATH), 'logo.png');
            }
        }
    }

    /**
     * Saving cartodb config
     */
    protected function saveCartodbConfigs() {
        /*
         * Saving Cartodb Base API URL
         */
        $cartodb_api = Config::whereItem($this->getConfigKey(static::CARTODB_API_URL))
            ->first();

        if (!$cartodb_api) {
            $cartodb_api = new Config();
        }

        $cartodb_api->fill([
            'item'  => $this->getConfigKey(static::CARTODB_API_URL),
            'value' => input('cartodb_base_api_url')
        ]);
        $cartodb_api->save();

        /*
         * Saving Cartodb Table Name
         */
        $cartodb_table = Config::whereItem($this->getConfigKey(static::CARTODB_TABLE))
            ->first();

        if (!$cartodb_table) {
            $cartodb_table = new Config();
        }

        $cartodb_table->fill([
            'item'  => $this->getConfigKey(static::CARTODB_TABLE),
            'value' => input('cartodb_table')
        ]);
        $cartodb_table->save();
    }

    /**
     * Saving show policy link
     */
    protected function saveShowPolicyLink() {
        $show_policy = Config::whereItem($this->getConfigKey(static::SHOW_POLICY))
            ->first();

        if (!$show_policy) {
            $show_policy = new Config();
        }

        $show_policy->fill([
            'item'  => $this->getConfigKey(static::SHOW_POLICY),
            'value' => input('show_policy')
        ]);
        $show_policy->save();
    }

    /**
     * Saving show help link
     */
    protected function saveShowHelpLink() {
        $show_help = Config::whereItem($this->getConfigKey(static::SHOW_HELP))
            ->first();

        if (!$show_help) {
            $show_help = new Config();
        }

        $show_help->fill([
            'item'  => $this->getConfigKey(static::SHOW_HELP),
            'value' => input('show_help')
        ]);
        $show_help->save();
    }

    /**
     * Sanitizes the file name.
     *
     * @param  string $fileName
     * @return string
     */
    protected function sanitizeFileName($fileName) {
        $regex = ['#(\.){2,}#', '#[^A-Za-z0-9\.\_\- ]#', '#^\.#', '#[ ]#', '![_]+!u'];

        return preg_replace($regex, '_', strtolower($fileName));
    }

    /**
     * Validating uploaded file
     *
     * @param UploadedFile $file
     * @return bool
     * @throws \Exception
     */
    protected function validForUpload(UploadedFile $file) {
        $allowed_mimetypes = ['image/gif', 'image/jpeg', 'image/png', 'image/jpg', 'image/bmp', 'image/x-windows-bmp', 'image/vnd.adobe.photoshop'];

        if (!$file->isValid()) {
            $this->setError($file->getErrorMessage());
            throw new \Exception();
        } elseif (!in_array($file->getClientMimeType(), $allowed_mimetypes)) {
            $this->setError(trans('bct/unionimpactbase::admin/general/message.error.file.mimetype'));
            throw new \Exception();
        } elseif ($file->getClientSize() > UploadedFile::getMaxFilesize()) {
            $this->setError(trans('bct/unionimpactbase::admin/general/message.error.file.size'));
            throw new \Exception();
        }

        return true;
    }

}