<?php namespace Bct\Unionimpactbase\Controllers\Admin\Settings;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 29.09.16
 * Time: 11:17
 */

use Bct\Unionimpactbase\Models\Config;
use Platform\Roles\Models\Role;


class MapSettingsController extends UnionImpactSettingsController {

    const CONFIG_PREFIX = 'map.location.';

    /**
     * displaying location admin panel
     */
    public function showLocation() {
        return view('bct/unionimpactbase::settings.map.location.index');
    }

    /**
     * saving location settings
     */
    public function processLocation() {
        try {
            $default_lat = Config::whereItem($this->getConfigKey(static::CONFIG_PREFIX . 'default.lat'))
                ->first();
            $default_lng = Config::whereItem($this->getConfigKey(static::CONFIG_PREFIX . 'default.lng'))
                ->first();

            if (!$default_lat) {
                $default_lat = new Config();
            }

            if (!$default_lng) {
                $default_lng = new Config();
            }

            $default_lat->fill([
                'item'  => $this->getConfigKey(static::CONFIG_PREFIX . 'default.lat'),
                'value' => input('map_location_default_lat')
            ]);
            $default_lat->save();

            $default_lng->fill([
                'item'  => $this->getConfigKey(static::CONFIG_PREFIX . 'default.lng'),
                'value' => input('map_location_default_lng')
            ]);
            $default_lng->save();

            $this->alerts->success(trans('bct/unionimpactbase::admin/map/location/message.success.save'));

            return redirect()->back();

        } catch (\Exception $e) {
            $this->alerts->error(trans("bct/unionimpactbase::admin/map/location/message.error.save"));

            return redirect()->route('admin.map.location');
        }
    }
}