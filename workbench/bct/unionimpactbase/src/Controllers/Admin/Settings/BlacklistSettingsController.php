<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 9/19/2016
 * Time: 2:26 PM
 */

namespace Bct\Unionimpactbase\Controllers\Admin\Settings;


use Bct\Unionimpactbase\Models\Config;

class BlacklistSettingsController extends UnionImpactSettingsController {

    const CONFIG_PREFIX = 'blacklist.clients';

    /**
     * displaying blacklist admin panel
     */
    public function showBlacklist() {
        $clients = config($this->getConfigKey(static::CONFIG_PREFIX), null);

        return view('bct/unionimpactbase::settings.blacklist.index', compact('clients'));
    }

    /**
     * saving blacklist settings
     */
    public function processBlacklist() {
        try {
            $blacklist_config = Config::whereItem($this->getConfigKey(static::CONFIG_PREFIX))
                ->first();

            if (!$blacklist_config) {
                $blacklist_config = new Config();
            }

            $blacklist_config->fill([
                'item'  => $this->getConfigKey(static::CONFIG_PREFIX),
                'value' => input('blacklist_clients')
            ]);
            $blacklist_config->save();

            $this->alerts->success(trans('bct/unionimpactbase::admin/blacklist/message.success.blacklist_saved'));

            return redirect()->back();

        } catch (\Exception $e) {
            $this->alerts->error(trans("bct/unionimpactbase::admin/blacklist/message.error.blacklist_saving"));

            return redirect()->route('admin.blacklist');
        }
    }

}