<?php

namespace Bct\Unionimpactbase\Controllers\Admin;

use Bct\Unionimpactbase\Repositories\CustomContent\CustomContentRepository;
use Platform\Access\Controllers\AdminController;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/19/2016
 * Time: 4:53 PM
 */
class CustomContentController extends AdminController {

    protected $content;

    /**
     * CustomContentController constructor.
     * @param CustomContentRepository $content
     */
    public function __construct(CustomContentRepository $content) {
        parent::__construct();

        $this->content = $content;
    }

    public function showSignInPage() {
        $content = data_get($this->content->findBySlug('sign-in-page'), 'value');

        return view('bct/unionimpactbase::custom_contents.signin', compact('content'));
    }

    public function submitSignInPage() {
        if ($this->content->updateSignInContent(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.signin');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

    public function showActivationPage() {
        $content = data_get($this->content->findBySlug('sign-up-page'), 'value');

        return view('bct/unionimpactbase::custom_contents.confirmemail', compact('content'));
    }

    public function submitActivationPage() {
        if ($this->content->updateSignUpContent(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.confirmemail');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

    public function showLandingPage() {
        $content = data_get($this->content->findBySlug('landing-page'), 'value');

        return view('bct/unionimpactbase::custom_contents.landing', compact('content'));
    }

    public function submitLandingPage() {
        if ($this->content->updateLandingContent(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.landing');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

    public function showLandingPageContractor() {
        $content = data_get($this->content->findBySlug('landing-page-contractor'), 'value');

        return view('bct/unionimpactbase::custom_contents.landing_contractor', compact('content'));
    }

    public function submitLandingPageContractor() {
        if ($this->content->updateLandingContentContractor(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.landing.contractor');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

    public function showPolicyPage() {
        $content = data_get($this->content->findBySlug('policy-page'), 'value');

        return view('bct/unionimpactbase::custom_contents.policy', compact('content'));
    }

    public function submitPolicyPage() {
        if ($this->content->updatePolicyContent(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.policy');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

    public function showYoutubePage() {
        $content = data_get($this->content->findBySlug('youtube-help-page'), 'value');

        return view('bct/unionimpactbase::custom_contents.youtube', compact('content'));
    }

    public function submitYoutubePage() {
        if ($this->content->updateYoutubeContent(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.youtube');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

    public function showKioskTermsPage() {
        $content = data_get($this->content->findBySlug('kiosk-terms-page'), 'value');

        return view('bct/unionimpactbase::custom_contents.kiosk_terms', compact('content'));
    }

    public function submitKioskTermsPage() {
        if ($this->content->updateKioskTermsPage(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.kiosk.terms');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

    public function showTokenErrorPage() {
        $content = data_get($this->content->findBySlug('token-error-page'), 'value');

        return view('bct/unionimpactbase::custom_contents.error_token', compact('content'));
    }

    public function submitTokenErrorPage() {
        if ($this->content->updateTokenErrorPage(input('value', ''))) {
            $this->alerts->success(trans("bct/unionimpactbase::admin/message.success.update_custom_content"));

            return redirect()->route('admin.bct.ccontent.error.token');
        }

        $this->alerts->error(trans('bct/unionimpactbase::admin/message.error.update_custom_content'));

        return redirect()->back();
    }

}