<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/19/2016
 * Time: 4:04 PM
 */

namespace Bct\Unionimpactbase\Controllers\Frontend;

use Bct\Unionimpactbase\Checkpoints\WhitelistException;
use Bct\Unionimpactbase\Models\States;
use Bct\Unionimpactbase\Models\User;
use Bct\Unionimpactbase\Repositories\Contractor\ContractorRepositoryInterface;
use Bct\Unionimpactbase\Repositories\CustomContent\CustomContentRepository;
use Bct\Unionimpactbase\Repositories\Member\MemberRepositoryInterface;
use Platform\Foundation\Controllers\Controller;
use Platform\Users\Repositories\UserRepositoryInterface;
use DB;

class PagesController extends Controller {

    protected $content;

    /**
     * The Users repository.
     *
     * @var \Platform\Users\Repositories\UserRepositoryInterface
     */
    protected $users;
    protected $contractors;
    protected $members;


    /**
     * PagesController constructor.
     *
     * @param CustomContentRepository $content
     * @param UserRepositoryInterface $users
     * @param ContractorRepositoryInterface $contractors
     * @param MemberRepositoryInterface     $members
     */
    public function __construct(
        CustomContentRepository $content,
        UserRepositoryInterface $users,
        ContractorRepositoryInterface $contractors,
        MemberRepositoryInterface $members
    ) {
        parent::__construct();

        $this->content = $content;
        $this->users = $users;
        $this->contractors = $contractors;
        $this->members = $members;
    }

    protected function getMailer() {
        return app('bct.unionimpactbase.mailer');
    }

    public function showLandingPage() {
        if (!$this->currentUser) {
            return $this->showSignInPage();
        }

        $user = User::find($this->currentUser->id);

        if (( $user->inRole('mrp_user') || $user->inRole('dp_contractor') ) && ! is_null($user->profile) && ! $user->profile->hasContractor()) {
            $content = data_get($this->content->findBySlug('landing-page-contractor'), 'value');
        } else {
            $content = data_get($this->content->findBySlug('landing-page'), 'value');
        }

        return view('bct/unionimpactbase::pages/home', compact('content'));
    }

    public function showSignInPage() {
        $content = data_get($this->content->findBySlug('sign-in-page'), 'value');

        return view('bct/unionimpactbase::pages/signin', compact('content'));
    }

    public function showSignUpPage() {
        $view = 'signup';
        $content = '';

        if (session('registration-complete')) {
            $content = data_get($this->content->findBySlug('sign-up-page'), 'value');

            $view = 'signup-activation-user';
        }

        return view('bct/unionimpactbase::pages/' . $view, compact('content'));
    }

    public function showPolicyPage() {
        $content = data_get($this->content->findBySlug('policy-page'), 'value');

        return view('bct/unionimpactbase::pages/policy', compact('content'));
    }

    public function showHelpPage() {
        $content = data_get($this->content->findBySlug('youtube-help-page'), 'value');

        return view('bct/unionimpactbase::pages/help', compact('content'));
    }

    /**
     * Handle posting of the form for logging the user in.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processLogin() {
        try {
            list($messages) = $this->users->auth()->login(request()->all());
        } catch (WhitelistException $we) {
            $messages = $we->getMessage();
        }

        // Do we have any errors?
        if (!$messages) {
            $this->alerts->success(
                trans('platform/users::auth/message.success.login')
            );

            return redirect()->intended('/')->with('login_success', true);
        }

        $this->alerts->error($messages);

        return redirect()->back();
    }

    public function showRegisterMRPContractorPage() {
        $states = States::all();
        $form_action = route('bct.register.contractor.mrp');
        $form_title = trans('bct/unionimpactbase::user/common.register_mrp_contractor');

        return view('bct/unionimpactbase::pages/register/contractor', compact('states', 'form_action', 'form_title'));
    }

    public function showRegisterDPContractorPage() {
        $states = States::all();
        $form_action = route('bct.register.contractor.dp');
        $form_title = trans('bct/unionimpactbase::user/common.register_dispatch_contractor');

        return view('bct/unionimpactbase::pages/register/contractor', compact('states', 'form_action', 'form_title'));
    }

    public function showRegisterDPJContractorPage() {
        $states = States::all();
        $form_action = route('bct.register.contractor.dpj');
        $form_title = trans('bct/unionimpactbase::user/common.register_dispatch_contractor');

        return view('bct/unionimpactbase::pages/register/contractor', compact('states', 'form_action', 'form_title'));
    }

    public function showRegisterMPMemberPage()
    {
        $form_action = route('bct.register.member.mp');
        $form_title = trans('bct/unionimpactbase::user/common.register_mp_member');
        return view('bct/unionimpactbase::pages/register/member', compact('form_action', 'form_title'));
    }

	public function showRegisterDPTHMemberPage()
	{
		$states = States::all();
		$form_action = route('bct.register.member.dpth');
		$form_title = trans('bct/unionimpactbase::user/common.register_member');
		return view('bct/unionimpactbase::pages/register/member_dpth', compact('states','form_action', 'form_title'));
	}


    /**
     * Activate the user.
     *
     * @param  int $id
     * @param  string $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($id, $code) {
        try {

            $user = $this->users->find($id);

            if (!is_null($user)) {
                $type = $this->users->activation()->activate($user, $code) ? 'success' : 'error';

                $this->alerts->{$type}(
                    trans("bct/unionimpactbase::auth/message.{$type}.activate")
                );


				if(!$user->inRole('mp_member') && !$user->inRole('dpth_member')){
					// Prepare the email subject
					$subject = trans('bct/unionimpactbase::user/common.emails.contractor_activation');

					$user_module = User::find($user->id);
					if ($user_module->profile && !$user_module->profile->hasContractor()) {
						$business_name = $user_module->profile->business_name;

						$officeadmin_users = User::whereHas('roles', function ($query) {
							$query->where('slug', 'mrp_admin');
						})->get();

						if (!$officeadmin_users->isEmpty()) {
							foreach ($officeadmin_users as $officeadmin_user) {
								$this->getMailer()->genericUserEmail($officeadmin_user, $subject, 'after_activation_to_dpj_officeadmins_notification', compact('business_name'));

							}
						}
					}
				}
				elseif($user->inRole('dpth_member')){
					$subject = trans('bct/unionimpactbase::user/common.emails.member_activation');

					$user_module = User::find($user->id);
					if ($user_module->profile && !$user_module->profile->hasMember()) {

						$subject = 'New Member Registration';

						$user_name = $user_module->first_name.' '.$user_module->last_name;

						//Send Email to Office Staff
						$officeadmin_users = User::whereHas('roles', function ($query) {
							$query->whereIn('slug', ['dpth_officeadmin','dpth_officestaff']);
						})->get();

						if (!$officeadmin_users->isEmpty()) {
							foreach ($officeadmin_users as $officeadmin_user) {
								$this->getMailer()->genericUserEmail($officeadmin_user, $subject, 'dpth_member_register_activation', compact('user_name'));
							}
						}
					}
				}


                return redirect('login');
            }

            $this->alerts->error('Old url');

            return redirect()->back()->withInput();

        } catch (\Exception $e) {
            $this->alerts->error($e->getMessage());

            return redirect()->back()->withInput();
        }

    }

    public function processRegistrationMRPContractor() {
        return $this->processRegistrationContractor('mrp_user');
    }

    public function processRegistrationDPContractor() {
        return $this->processRegistrationContractor('dp_contractor');
    }

    public function processRegistrationDPJContractor() {
        return $this->processRegistrationContractor('dpj_contractor');
    }

    public function processRegistrationMPMember()
    {
        return $this->processRegistrationMember('mp_member');
    }

	public function processRegistrationDPTHMember()
	{
		return $this->processRegistrationMember('dpth_member');
	}

    protected function processRegistrationContractor($role_name)
    {
        $data_user = array_only(request()->all(), ['first_name', 'last_name', 'email', 'password', 'password_confirmation']);
        $data_contractor = array_except(request()->all(), ['first_name', 'last_name', 'email', 'password', 'password_confirmation']);

        //Hack for not showing mailer error on registering new dpj contractor
        $data_user['first_name'] = strip_tags($data_user['first_name']);
        $data_user['last_name'] = strip_tags($data_user['last_name']);


        DB::beginTransaction();

        try {

            // Store the user
            list($messages, $user) = app('platform.users.auth')->register($data_user);


            // Do we have any errors?
            if ($messages->isEmpty()) {

                list($message, $success) = $this->contractors->registerUserForContractor($user, $role_name, $data_contractor);

                if ($success === true) {

                    DB::commit();

                    //$this->alerts->success($message);

                    return redirect('register')->with('registration-complete', $user);
                } else {
                    if (is_string($message)) {
                        $this->alerts->error($message);
                    } else {
                        $this->alerts->error($message, 'form');
                    }

                    DB::rollBack();

                    return redirect()->back()->withInput();
                }
            }

            DB::rollBack();

            $this->alerts->error($messages, 'form');

            return redirect()->back()->withInput();

        } catch (\Exception $e) {
            DB::rollBack();

            $this->alerts->error($e->getMessage());

            return redirect()->back()->withInput();
        }
    }


    protected function processRegistrationMember($role_name)
    {
        $data_user = array_only(request()->all(), ['first_name', 'last_name', 'email', 'password', 'password_confirmation']);
	    $data_member = array_except(request()->all(), ['first_name', 'last_name', 'email', 'password', 'password_confirmation']);

	    $data_user['first_name'] = strip_tags($data_user['first_name']);
        $data_user['last_name']  = strip_tags($data_user['last_name']);

        DB::beginTransaction();

        try {

            // Store the user
            list($messages, $user) = app('platform.users.auth')->register($data_user);

            // Do we have any errors?
            if ($messages->isEmpty()) {

                list( $message, $success, $user ) = $this->members->registerUserForMember($user, $role_name, $data_member);

	            if($role_name!=='dpth_member'){
		            $this->members->registerMember($user, [
			            'first_name' => $data_user['first_name'],
			            'last_name'  => $data_user['last_name'],
			            'email'      => $data_user['email'],
			            'active'     => 1
		            ]);
	            }


                if ($success === true) {

                    DB::commit();

                    return redirect('register')->with('registration-complete', $user);
                } else {
                    if (is_string($message)) {
                        $this->alerts->error($message);
                    } else {
                        $this->alerts->error($message, 'form');
                    }

                    DB::rollBack();

                    return redirect()->back()->withInput();
                }
            }

            DB::rollBack();

            $this->alerts->error($messages, 'form');

            return redirect()->back()->withInput();

        } catch (\Exception $e) {
            DB::rollBack();

            $this->alerts->error($e->getMessage());

            return redirect()->back()->withInput();
        }
    }

}