<?php namespace Bct\Unionimpactbase\Controllers\Frontend;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 09.03.16
 * Time: 17:14
 */

use Bct\Unionimpactbase\Models\ConfigDatagrid;
use Config;

class DataGridController extends BaseController {

    /**
     * DataGridController constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function filterGet()
    {
        try {
            $user_datagrid = $this->currentUserModule->datagrid;

            $db_datagrid_filters = (!is_null($user_datagrid)) ? $user_datagrid->config : null;

            return response()->json([ 'success' => true , 'filters' => $db_datagrid_filters], 200);
        } catch (\Exception $e) {
            return response()->json([ 'success' => false , 'message' => $e->getMessage()], 422);
        }
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function processFilterSave() {
        $filter = input('filter');

        $user_datagrid = $this->currentUserModule->datagrid;

        $db_datagrid_filters = !is_null($user_datagrid) ? json_decode($user_datagrid->config, true) : [];

        if (!empty($db_datagrid_filters)) {
            $is_filter_in_db = collect($db_datagrid_filters)->contains(function ($key, $value) use ($filter) {
                return ($value['pathname'] === $filter['pathname'] && $value['grid'] === $filter['grid']);
            });

            if ($is_filter_in_db) {
                $new_datagrid_filters = collect($db_datagrid_filters)->map(function($item, $key) use ($filter) {
                    if ($item['pathname'] === $filter['pathname'] && $item['grid'] === $filter['grid']) {
                        $item['filter'] = $filter['filter'];
                    }

                    return $item;
                })->all();
            } else {
                $new_datagrid_filters = $db_datagrid_filters;
                $new_datagrid_filters[] = $filter;
            }

            try {
                $user_datagrid->config = collect($new_datagrid_filters)->toJson();
                $user_datagrid->save();

                return response()->json([ 'success' => true , 'filters' => $new_datagrid_filters], 200);
            } catch (\Exception $e) {
                return response()->json([ 'success' => false , 'message' => $e->getMessage()], 422);
            }
        } else {
            try {
                $datagrid = ConfigDatagrid::create([
                    'config' => collect([$filter])->toJson()
                ]);
                $datagrid->save();

                $datagrid->user()->associate($this->currentUserModule);
                $datagrid->save();

                return response()->json([ 'success' => true , 'filters' => [$filter]], 200);
            } catch (\Exception $e) {
                return response()->json([ 'success' => false , 'message' => $e->getMessage()], 422);
            }
        }
    }

}
