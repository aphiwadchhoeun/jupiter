<?php
namespace Bct\Unionimpactbase\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;
use Illuminate\Support\Facades\App;
use JavaScriptUnionimpactbase;

class OpenRoutesController extends Controller {

	/**
	 * The repository.
	 *
	 * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
	 */
	protected $users;

	/**
	 * Constructor.
	 *
	 *
	 */
	public function __construct() {

	}

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		if(
			isExtensionEnabled('bct/projects') || isExtensionEnabled('bct/member') ||
			isExtensionEnabled('bct/dispatch') || isExtensionEnabled('bct/mrp')
			|| isExtensionEnabled('bct/mrp_pnw')  || isExtensionEnabled('bct/dispatch_janitors')
		) {

			JavaScriptUnionimpactbase::put([
				'default_lat'    => config('bct.unionimpactbase.map.location.default.lat'),
				'default_lng'    => config('bct.unionimpactbase.map.location.default.lng'),
			]);

			return view('bct/unionimpactbase::openroutes.index');
		}

		App::abort(404);

	}

}
