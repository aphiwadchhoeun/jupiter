<?php namespace Bct\Unionimpactbase\Controllers\Frontend;

    /**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.03.16
 * Time: 11:59
 */


/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.16
 * Time: 13:30
 */

use Bct\Unionimpactbase\Models\User;
use Platform\Foundation\Controllers\Controller;

class BaseController extends Controller
{


    protected $currentUserModule;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->currentUserModule = (!is_null($this->currentUser)) ? User::find($this->currentUser->id) : null;
    }


}