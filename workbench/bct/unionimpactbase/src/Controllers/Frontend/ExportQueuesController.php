<?php namespace Bct\Unionimpactbase\Controllers\Frontend;


use Bct\Unionimpactbase\Models\ExportQueue;
use Bct\Unionimpactbase\Repositories\ExportQueue\ExportQueueRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Container\Container;
use League\Flysystem\FileNotFoundException;
use Platform\Access\Controllers\AuthorizedController;

class ExportQueuesController extends AuthorizedController {

    /**
     * Array of whitelisted methods which do not get
     * compared to permissions for the user.
     *
     * @var array
     */
    protected $permissionsWhitelist = ['index', 'grid', 'download', 'update'];

    /**
     * Array of whitelisted methods which do not need to be authorized.
     *
     * @var array
     */
    protected $authWhitelist = ['update'];

    protected $queues;
    protected $filesystem;

    /**
     * PdfQueuesController constructor.
     * @param Container $app
     * @param ExportQueueRepositoryInterface $queues
     */
    public function __construct(Container $app, ExportQueueRepositoryInterface $queues) {
        parent::__construct();

        $this->queues = $queues;
        $this->filesystem = $app['filesystem'];
    }

    /**
     * Update queue export record with S3 download url and status
     *
     * @param $hash
     */
    public function update($hash) {
        if ($eq = ExportQueue::where('hash', $hash)->first()) {
            $eq->path = input('path', '');
            $eq->status = input('status', 0);
            $eq->save();
        }
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $currentUser = $this->currentUser;

        return view('bct/unionimpactbase::exportqueues/index', compact('currentUser'));
    }

    public function grid() {
        $data = $this->queues->grid()
            ->where('created_by', '=', $this->currentUser->getUserId())
            ->where('created_at', '>=', Carbon::now()->subMonth())
            ->select([
                'id',
                'name',
                'source',
                'ext',
                'status',
                'created_at'
            ]);

        $columns = [
            'id',
            'name',
            'source',
            'ext',
            'status',
            'created_at',
            'created',
            'download_uri'
        ];

        $settings = [
            'max_results' => 16,
            'threshold'   => 16,
            'throttle'    => 16,
            'sort'        => 'created_at',
            'direction'   => 'desc',
        ];

        $transformer = function ($element) {
            $element['download_uri'] = route('bct.unionimpactbase.export.queue.download', $element['id']);
            $element['created'] = Carbon::createFromFormat('Y-m-d H:i:s', $element['created_at'])->format('M-d-Y H:i:s');

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function download($id) {
        if ($q = $this->queues->find($id)) {
            try {
                $file = $this->filesystem->disk('s3')->read($q->path);
            } catch (FileNotFoundException $e) {
                return response(null, 404);
            }

            $headers = [
                'Connection'          => 'close',
                'Content-Type'        => $q->mime,
                'Content-Length'      => strlen($file),
                'Content-Disposition' => 'attachment; filename="' . $q->full_name . '"',
            ];

            return $this->respond($file, $headers);
        }

        return response(null, 404);
    }

    /**
     * Sends the response with the appropriate headers.
     *
     * @param  string $file
     * @param  array $headers
     * @return \Illuminate\Http\Response
     */
    protected function respond($file, $headers = []) {
        $response = response($file, 200);

        foreach ($headers as $name => $value) {
            $response->header($name, $value);
        }

        return $response;
    }

}
