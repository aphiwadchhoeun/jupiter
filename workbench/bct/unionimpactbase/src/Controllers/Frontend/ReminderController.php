<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 3/10/2016
 * Time: 11:44 AM
 */

namespace Bct\Unionimpactbase\Controllers\Frontend;


use Cartalyst\Support\Traits\EventTrait;
use Illuminate\Container\Container;
use Platform\Foundation\Controllers\Controller;

class ReminderController extends Controller {

    use EventTrait;

    protected $app;

    protected $users;
    protected $sentinel;

    /**
     * ReminderController constructor.
     * @param Container $app
     */
    public function __construct(Container $app) {
        parent::__construct();

        $this->app = $app;
        $this->users = $this->app['platform.users'];
        $this->sentinel = $this->app['sentinel'];
    }


    /**
     * Display reminder form
     *
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFormWithEmail($userId) {

        if ($id = session('password_reminder')) {
            $user = $this->users->find($id);

            return view('platform/users::auth/password_reminder_success', compact('user'));
        }

        $currentUserEmail = $this->users->find($userId)->email;

        return view('bct/unionimpactbase::auth/password_reminder', compact('currentUserEmail'));
    }

    /**
     * Show the form for the forgot password.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        if ($id = session('password_reminder')) {
            $user = $this->users->find($id);

            return view('platform/users::auth/password_reminder_success', compact('user'));
        }

        return view('bct/unionimpactbase::auth/password_reminder');
    }

    /**
     * Handle posting of the form for the password reminder confirmation.
     *
     * @param  int $id
     * @param  string $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processConfirmation($id, $code) {
        if (!$user = $this->users->find($id)) {
            $this->alerts->error(trans('platform/users::auth/message.user_not_found'));

            return redirect('login');
        }

        $messages = $this->users->validForReminder($id, request()->all());

        if (!$messages->isEmpty()) {
            $this->alerts->error($messages, 'form');

            return redirect()->back()->withInput();
        }

        if ($this->sentinel->getReminderRepository()->complete($user, $code, input('password'))) {

            $activation = $user->activations()->orderBy('created_at', 'desc')->first();

            if (!$activation) {
                $activation = $this->users->activation()->create($user);
            }

            if ($activation && $this->users->activation()->activate($user, $activation->code)) {

                $this->alerts->success(trans('bct/unionimpactbase::auth/message.success.reset-and-activate-confirm'));

                return redirect('login');
            }

            $this->alerts->success(trans('platform/users::auth/message.success.reset-password-confirm'));

            return redirect('login');
        }

        return redirect()->back()->withInput()->withEmail(
            trans('platform/users::auth/message.password_reminder_confirm.error')
        );
    }

}