<?php namespace Bct\Unionimpactbase\Controllers\Frontend;
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 6/6/2016
 * Time: 11:00 AM
 */

namespace Bct\Unionimpactbase\Controllers\Frontend;


use Illuminate\Support\Facades\Artisan;
use Platform\Access\Controllers\AuthorizedController;

class QueueController extends AuthorizedController {

    /**
     * QueueController constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function restartQueue() {
        try {
            $code = Artisan::call('queue:restart');

            $code += Artisan::call('queue:retry', [
                'id' => ['all']
            ]);

            if ($code > 0) {
                return response('Command did not run successfully, Please try again.', 500);
            }

            return response('Queue has been restarted!', 200);
        } catch (\Exception $e) {
            return response('Command threw an exception, Please try again.', 500);
        }
    }

}