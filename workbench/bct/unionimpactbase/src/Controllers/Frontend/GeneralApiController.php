<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/17/2016
 * Time: 2:44 PM
 */

namespace Bct\Unionimpactbase\Controllers\Frontend;

use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;
use Platform\Access\Controllers\AuthorizedController;


class GeneralApiController extends AuthorizedController {

    protected $permissionsWhitelist = ['getIcon', 'getCartodbConfigs'];

    // cache life time in minutes
    protected static $cache_life_time = 1440;
    protected static $default_dimension = 64;

    /**
     * Get icon image for map markers
     *
     * @return mixed
     */
    public function getIcon() {
        $icon_w = ceil(input('w', static::$default_dimension) / 2);
        $icon_h = ceil(input('h', static::$default_dimension) / 2);

        $filename = input('icon', 'smile');

        $color = input('c', 'green');

        // setup icon emoji
        $icon = Image::cache(function ($image) use ($icon_w, $icon_h, $filename) {
            $image->make(base_path('resources/assets/img/' . $filename . '.png'))
                ->resize($icon_w, $icon_h);
        }, static::$cache_life_time, true);

        // setup house black outline
        $outline = Image::cache(function ($image) {
            $image->make(base_path('resources/assets/img/house_outline.png'))
                ->resize(input('w', static::$default_dimension), input('h', static::$default_dimension));
        }, static::$cache_life_time, true);

        // putting everything together
        $img = Image::cache(function ($image) use ($icon, $outline, $icon_w, $icon_h, $color) {
            $image = $image->make(base_path('resources/assets/img/house.png'));
            switch ($color) {
                case 'green':
                    $image->colorize(70, 100, 0);
                    break;
                case 'red':
                    $image->colorize(100, 40, 0);
                    break;
                case 'yellow':
                    $image->colorize(100, 90, 0);
                    break;
            }
            $image->resize(input('w', static::$default_dimension), input('h', static::$default_dimension))
                ->insert($outline)
                ->insert($icon, 'top-left', ceil($icon_w / 2), ceil($icon_h / 1.3));
        }, static::$cache_life_time, true);

        return $img->response();
    }

    /**
     * Get table source for regions to use for map from Cartodb service
     */
    public function getCartodbConfigs() {
        $cartodb_api = config('bct.unionimpactbase.cartodb.cartodb_base_api_url', 'https://alexbct.cartodb.com:443/api/v2');
        $cartodb_table = config('bct.unionimpactbase.cartodb.regions_table', 'dispatch_areas_pnwrcc');

        return [
            'api_url'   => $cartodb_api,
            'tablename' => $cartodb_table
        ];
    }

}