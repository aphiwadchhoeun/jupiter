<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 1/26/2017
 * Time: 12:35 PM
 */

namespace Bct\Unionimpactbase\Controllers\Frontend;

use Carbon\Carbon;
use Platform\Access\Controllers\AuthorizedController;

class TermsAgreementController extends AuthorizedController
{

    protected $permissionsWhitelist = ['showTermsAgreement', 'processTermsAgreement'];


    public function showTermsAgreement()
    {
        return view('bct/unionimpactbase::termsagreement/index');
    }


    public function processTermsAgreement()
    {
        if (empty(input('agree'))) {
            $this->alerts->error(trans('bct/unionimpactbase::termsagreement/message.error'));

            return redirect()->back();
        }

        try {
            $user                            = $this->currentUser;
            $user->terms_agreement           = true;
            $user->terms_agreement_sign_date = Carbon::now();
            $user->save();
        } catch (\Exception $e) {
            $this->alerts->error(trans('bct/unionimpactbase::termsagreement/message.500'));

            return redirect()->back();
        }

        return redirect()->to('/');
    }

}