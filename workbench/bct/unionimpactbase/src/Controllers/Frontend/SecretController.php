<?php namespace Bct\Unionimpactbase\Controllers\Frontend;


use Platform\Access\Controllers\AuthorizedController;

class SecretController extends AuthorizedController {

    /**
     * UsersController constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function showSecret() {
        return view('bct/unionimpactbase::secret/404');
    }

    public function flushCaches() {
        app('cache')->flush();

        return 'ok';
    }

}
