<?php namespace Bct\Unionimpactbase\Controllers\Frontend;

use Bct\Unionimpactbase\Repositories\Contractor\ContractorRepositoryInterface;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Support\Traits;
use DB;
use Platform\Access\Controllers\AuthorizedController;
use Platform\Users\Repositories\UserRepositoryInterface;

class UsersController extends AuthorizedController
{

    use Traits\EventTrait;

    /**
     * Array of whitelisted methods which do not get
     * compared to permissions for the user.
     *
     * @var array
     */
    protected $permissionsWhitelist = ['profile', 'activate'];

    protected $users;


    /**
     * UsersController constructor.
     *
     * @param ContractorRepositoryInterface $contractors
     * @param UserRepositoryInterface       $users
     */
    public function __construct(
        UserRepositoryInterface $users
    ) {
        parent::__construct();

        $this->users = $users;
    }


    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function profile()
    {
        return view('bct/unionimpactbase::users/profile');
    }


    /**
     * Manually activate user account
     *
     * @param $id
     */
    public function activate($id)
    {
        if ($user = $this->users->find($id)) {
            Activation::removeExpired();

            $activation = Activation::exists($user) ? Activation::exists($user) : Activation::create($user);

            Activation::complete($user, $activation->code);

            if (Activation::completed($user)) {
                return response()->json(['success' => true], 200);
            }

            return response()->json(['success' => false], 400);
        }

        return response()->json(['success' => false], 400);
    }
}
