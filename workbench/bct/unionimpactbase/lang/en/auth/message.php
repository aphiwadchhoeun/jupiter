<?php

/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    3.2.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2016, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

    // General messages
    'user_not_found'      => 'Incorrect email or password, please register your account.',
    'user_already_exists' => 'User already exists.',
    'user_is_activated'   => 'Your account is already activated.',
    'user_not_activated'  => 'Your account is not activated.',
    'user_is_banned'      => 'Your account is banned.',
    'invalid_email'       => 'Email is either invalid or was not found.',
    'invalid_password'    => 'Password is incorrect.',

    'throttling' => [
        'global' => 'Too many unsuccessful attempts have been made globally, logins are locked for another :delay second(s).',
        'ip'     => 'Suspicious activity has occured on your IP address and you have been denied access for another :delay second(s).',
        'user'   => 'Too many unsuccessful login attempts have been made against your account. Please try again after another :delay second(s).',
    ],

    // Success messages
    'success'    => [
        'login'               => 'Successfully logged in.',
        'register'            => 'Account successfully created.',
        'update'              => 'Your account has been updated.',
        'activate'            => 'Your account has now been activated. Thank you for registering.',
        'register_contractor' => 'Registration is complete and your account is pending administrator\'s review. Once approved you will receive a notification email and your account will be active.',

        'reset-password-confirm' => 'Your password reset has been successfully confirmed. You can now sign in with your new password.',
        'reset-and-activate-confirm' => 'Your account has been activated and password reset successfully. You can now sign in with your new password.'
    ],

    // Error messages
    'error'      => [
        'login'    => 'There was an issue logging you in. Please try again.',
        'register' => 'There was an issue creating your account. Please try again.',
        'activate' => 'Your account is already activated.',

        'reset-password'         => 'Unable to reset your password, please make sure you are using a registered email address.',
        'reset-password-confirm' => 'There was an issue confirming your password reset code. Please try again.',
    ],

];
