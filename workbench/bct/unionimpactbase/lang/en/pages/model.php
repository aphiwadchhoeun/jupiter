<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/22/2016
 * Time: 2:36 PM
 */

return [
    'form' => [
        'signup' => [
            'first_name'       => 'First Name',
            'last_name'        => 'Last Name',
            'email'            => 'Email',
            'password'         => 'Password',
            'password_confirm' => 'Confirm Password',
            'role'             => 'Choose Role',

            'first_name_placeholder' => 'John',
            'last_name_placeholder'  => 'Doe',
            'first_name_help'        => 'Type your first name.',
            'last_name_help'         => 'Type your last name.',
            'password_help'          => 'Must contains at least one lowercase letter, one uppercase letter, and numbers.',
        ]
    ]
];