<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/19/2016
 * Time: 4:13 PM
 */

return [
    'titles' => [
        'home'       => 'Union Impact',
        'signin'     => 'Union Impact - Sign In',
        'signup'     => 'Union Impact - Sign Up',
        'activation' => 'Union Impact - Activation',
        'policies'   => 'UNION IMPACT PRIVACY POLICY',
    ],

    'signin' => [
        'btn_signup' => 'Create an account'
    ]
];