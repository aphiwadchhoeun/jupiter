<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/22/2016
 * Time: 12:46 PM
 */

return [

    // Success messages
    'success' => [
        'update_custom_content' => 'Custom Content was successfully updated.',
    ],

    // Error messages
    'error'   => [
        'update_custom_content' => 'There was an issue updating the Custom Content. Please try again.',
    ],

];