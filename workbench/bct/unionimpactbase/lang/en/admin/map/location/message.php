<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/24/2016
 * Time: 12:28 PM
 */

return [
    'instruction' => '',

    'success' => [
        'save' => 'Map/Location Settings was successfully saved!'
    ],

    'error' => [
        'save' => 'Unable to save Map/Location Settings!'
    ]
];