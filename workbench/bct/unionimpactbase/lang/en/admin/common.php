<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 2/19/2016
 * Time: 4:51 PM
 */

return [
    'title' => 'Custom Content',

    'pages' => [
        'signin_page'                  => 'Sign In Page Content',
        'email_confirm_page'           => 'Email Confirm Page Content',
        'landing_home_page'            => 'Landing Home Page Content',
        'landing_home_page_contractor' => 'Landing Contractor Home Page Content',
        'policy_page'                  => 'Policy Page Content',
        'youtube_tutorials_page'       => 'Youtube Tutorials Page Content',
        'kiosk_terms_page'             => 'Kiosk Terms Page',
        'error_token_page'             => 'Token Error Page',

    ]
];