<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/24/2016
 * Time: 12:28 PM
 */

return [
    'instruction' => 'Enter blacklist IP addresses for client. These clients will be blocked from logging in the system. Multiple entries separated by new line.',

    'success' => [
        'blacklist_saved' => 'Blacklist Settings was successfully saved!'
    ],

    'error' => [
        'blacklist_saving' => 'Unable to save Blacklist Settings!'
    ]
];