<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/24/2016
 * Time: 12:28 PM
 */

return [
    'success' => [
        'save' => 'Settings was successfully saved!'
    ],

    'error' => [
        'save' => 'Unable to save Settings!',
        'file' => [
            'mimetype' => 'File type is not valid.',
            'size'     => 'File size is too large.'
        ]
    ]
];