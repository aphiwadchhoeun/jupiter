<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/24/2016
 * Time: 1:59 PM
 */

return [
    'cartodb_base_api_url' => 'Cartodb API URL',
    'cartodb_table'        => 'Cartodb Table Name for Region',
    'app_logo'             => 'Upload Logo',
    'show_policy'          => 'Show Policy link on footer',
    'show_help'            => 'Show Help videos link on footer',

    'default_cartodb_table'        => 'Default "dispatch_areas_pnwrcc"',
    'default_cartodb_base_api_url' => 'Default "https://alexbct.cartodb.com:443/api/v2"'
];