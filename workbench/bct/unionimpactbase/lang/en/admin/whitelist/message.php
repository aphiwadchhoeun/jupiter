<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 8/24/2016
 * Time: 12:28 PM
 */

return [
    'instruction' => 'Enter whitelist IP addresses for client. These clients will have access only when they logging in with this role from specified IP addresses. Multiple entries separated by new line.',

    'success' => [
        'whitelist_saved' => 'Whitelist Settings was successfully saved!'
    ],

    'error' => [
        'whitelist_saving' => 'Unable to save Whitelist Settings!'
    ]
];