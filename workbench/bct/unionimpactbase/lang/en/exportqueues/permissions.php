<?php

return [

	'index'  => 'List Export',
	'create' => 'Create new Export',
	'edit'   => 'Edit Export',
	'delete' => 'Delete Export',

];
