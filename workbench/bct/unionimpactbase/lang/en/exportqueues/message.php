<?php

return [

	// General messages
	'not_found' => 'Export [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Export was successfully created.',
		'update' => 'Export was successfully updated.',
		'delete' => 'Export was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the Export. Please try again.',
		'update' => 'There was an issue updating the Export. Please try again.',
		'delete' => 'There was an issue deleting the Export. Please try again.',
	],

];
