<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 3/18/2016
 * Time: 3:02 PM
 */

return [

    'general' => [
        'name'       => 'Name',
        'status'     => 'Status',
        'download'   => 'Download',
        'created_at' => 'Created At',
        'source'     => 'Source',
        'ext'        => 'File Type'
    ]

];
