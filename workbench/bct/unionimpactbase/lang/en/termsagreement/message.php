<?php

return [

    // General messages
    'error' => 'You must agree to the Terms of Service in order to gain access to our system.',
    '500'   => 'Something went wrong on our end, please try again later!'
];
