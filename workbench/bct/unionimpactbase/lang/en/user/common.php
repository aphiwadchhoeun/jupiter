<?php

return [

    'title'                        => 'User Profile',
    'mp_member'                    => 'MP Member',
    'dpth_member'                  => 'DPTH Member',
    'mrp_user'                     => 'MRP Profile',
    'member'                       => 'Member Profile',
    'contractor'                   => 'Contractor',
    'register_mrp_contractor'      => 'Register MRP Contractor',
    'register_dispatch_contractor' => 'Register Dispatch Contractor',
    'register_mp_member'           => 'Register MP Member',
    'register_dpth_member'         => 'Register DPTH Member',
    'register_member'              => 'Register Member',
    'mrp_contractor'               => 'MRP Contractor',
    'dispatch_contractor'          => 'Dispatch Contractor',

    'actions' => [
        'create_profile' => 'Create/Edit Profile'
    ],

    'emails' => [
        'contractor_activation' => 'New Contractor is activated'
    ]
];
