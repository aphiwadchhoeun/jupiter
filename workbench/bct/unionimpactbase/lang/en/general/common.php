<?php

return [

    'title'                  => 'Union Impact |',
    'dashboard'              => 'Dashboard',
    'actions'                => 'Actions',
    'create'                 => 'Create',
    'edit'                   => 'Edit',
    'settings'               => 'Settings',
    'details'                => 'Details',
    'cancel'                 => 'Cancel',
    'back'                   => 'Back',
    'next'                   => 'Next',
    'finish'                 => 'Finish',
    'add'                    => 'Add',
    'remove'                 => 'Remove',
    'search'                 => 'Search',
    'commit'                 => 'Commit',
    'undo'                   => 'Undo',
    'response'               => 'Bid Response',
    'hour_details'           => 'Hour Submission Details',
    'submit_hours'           => 'Submit Hours',
    'form_note'              => 'Your information will only be saved when you click "Next"',
    'show'                   => 'Show',
    'print'                  => 'Print',
    'active'                 => 'Active',
    'inactive'               => 'Inactive',
    'not_enough_data'        => 'Not Enough Data',
    'role'                   => 'Role',
    'find_member_press_plus' => 'Please find the member you would like to add and press PLUS icon',
    'select_list_to_edit'    => 'Select the list you’d like to edit',
    'close'                  => 'Close',
    'save_changes'           => 'Save changes',
    'summary_total'          => 'Summary total',
    'all_locals'             => 'All locals',
    'locals'                 => 'Locals',
    'amount'                 => 'Amount',
    'days_past_paid_through' => 'Number of days past paid through date',
    'of'                     => 'of',
    'account'                => 'Account',
    'permissions'            => 'Permissions',
    'amount'                 => 'Amount',
    'date'                   => 'Date',
    'update'                 => 'Update',
    'fullname'               => 'Full Name',
    'created_by'             => 'Created by',
    'updated_at'             => 'Updated At',
    'monthly_dues_total'     => 'Monthly Dues Total',
    'grand_total'            => 'Grand Total',
    'yes'                    => 'Yes',
    'no'                     => 'No',
    'save'                   => 'Save',
    'view'                   => 'View',
    'view_info'              => 'View Info',
    'image'                  => 'Image',
    'other'                  => 'Other',
    'agree_terms_conditions' => 'I agree to Terms & Conditions',
    'closed'                 => 'Closed',
    'pending'                => 'Pending',
    'archived'               => 'Archived',
    'filled'                 => 'Filled',
    'not_filled'             => 'Not Filled',
    'cancelled'              => 'Cancelled',
    'disabled'               => 'Disabled',
    'accepted'               => 'Accepted',
    'waiting'                => 'Waiting',
    'select_name'            => 'Select',
    'dispatch_criteria'      => 'Dispatch Criteria',
    'dispatch_history'       => 'Dispatch History',
    'contact_log'            => 'Contact Log',
    'type'                   => 'Type',
    'contractor_name'        => 'Contractor',
    'result'                 => 'Result',
    'sort'                   => 'Sort',
    'status_name'            => 'Status',
    'maximum_number_files'   => 'maximum :number files',
    'match_found'            => 'Match Found',
    'view_dispatch'          => 'View Dispatch',
    'dispatch'               => 'Dispatch',
    'on_list'                => 'On The List',
    'link'                   => 'Link',
    'register'               => 'Register',
    'contact_firstname'      => 'Contact First Name',
    'contact_lastname'       => 'Contact Last Name',
    'dispatch_from_job'      => 'Dispatch from Job',
    'dispatch_from_member'   => 'Dispatch from Member',
    'configuration'          => 'Configuration',
    'users'                  => 'Users',
    'reports'                => 'Reports',
    'dispatcher'             => 'Dispatcher',
    'job'                    => 'Job',
    'date_slot_filled'       => 'Date slot filled',
    'date_response'          => 'Date Response',
    'response'               => 'Response',
    'start_date'             => 'Start date',
    'address'                => 'Address',
    'submitted_by'           => 'Submitted by',
    'submitted_date'         => 'Submitted Date',
    'blacklist'              => 'Blacklist',
    'buildings_blacklist'    => 'Buildings Blacklist',
    'name'                   => 'Name',
    'member'                 => 'Member',

    'placeholder' => [
        'fill_this' => 'Fill this for ',
        'optional'  => 'optional'
    ],

    'breadcrumbs' => [
        'project_info' => 'Project Information',
        'bidding_info' => 'Bidding Information',
        'scopes_info'  => 'Request',
        'bidders_info' => 'Other Bidders',
        'overview'     => 'Overview'
    ],

    'gender' => [
        'male'   => 'Male',
        'female' => 'Female'
    ],

    'buttons' => [
        'show_all'          => 'Show All',
        'show_active'       => 'Show Active',
        'show_pending'      => 'Show Pending',
        'toggle_navigation' => 'Toggle navigation',
        'toggle_dropdown'   => 'Toggle Dropdown',
        'csv'               => 'CSV',
        'pdf'               => 'PDF'
    ],

    'select' => [
        'role'           => 'Select a role',
        'date_range'     => 'Please select date range',
        'event'          => 'Select Event',
        'locals'         => 'Select Locals',
        'all'            => 'Select All',
        'criteria'       => 'Select Criteria',
        'criteria_value' => 'Select Criteria Value',
        'project_type'   => 'Select Project Type',
        'list'           => 'Select List',
        'dispatcher'     => 'Select Dispatcher',
        'member'         => 'Select Member',
        'contractor'     => 'Select Contractor',
    ],

    'deselect' => [
        'all' => 'Deselect All'
    ],

    'information' => [
        'default' => 'Information',
        'user'    => 'User information',
        'member'  => 'Member information'
    ],

    'accounts' => [
        'office_staff' => 'Office staff accounts',
        'member'       => 'Member accounts'
    ],

    'settings' => [
        'reports' => 'Reports Settings'
    ],

    'status' => [
        'complete'     => 'Complete',
        'pending'      => 'Pending',
        'job_accepted' => 'Job Accepted',
        'job_declined' => 'Job Declined',
        'no_answer'    => 'No Answer',
        'busy_signal'  => 'Busy Signal',
        'wrong_number' => 'Wrong Number',
        'text'         => [
            'job' => [
                'answer' => ':name :answer',
            ]
        ]
    ],

    'new' => [
        'user'         => 'New User',
        'members'      => 'New Members',
        'job_requests' => 'New Job Requests'
    ],

    'dashboard_titles' => [
        'unfilled_requests' => 'Unfilled Requests',
        'request_history'   => 'Request History'
    ],

    'all' => [
        'values' => 'All Values',
    ],

    'add_new' => [
        'request' => 'Add new Request',
        'job'     => 'Add new Job',
        'workers' => 'Add new Workers'
    ],

    'add_to' => [
        'list' => 'Add to List',
    ],

    'searching' => [
        'address' => 'Search Address'
    ],

    'add_more' => [
        'workers' => 'Add more Workers'
    ],

    'texts' => [
        'get_on_oow_list'                       => 'To get on the Out-Of-Work List click here:',
        'you_not_on_oow'                        => 'You are currently not on the Our-Of-Work List.',
        'you_placed_on_oow_list'                => 'You have been Placed on Out-Of-Work List',
        'pending_please_respond'                => 'You have a pending dispatch - Please Respond',
        'you_have_pending_dispatch_will_expire' => 'You have a pending dispatch. It will expire on :datetime.',
        'click_view_dispatch_respond'           => 'Click View Dispatch to respond.',
        'ability_to_job_create'                 => 'Ability to create job on behalf of contractor',
        'oow'                                   => [
            'member' => [
                'scenario2' => [
                    'body' => [
                        'all' => 'As of :last_checkin_date there are :people_ahead person(s) ahead of you.',
                    ]
                ]
            ]
        ]
    ],

    'btn' => [
        'exit'    => [
            'list' => 'Exit List'
        ],
        'checkin' => 'Check-in'
    ],

    'by' => [
        'job_request' => [
            'start_date'   => 'by Job Request Start Date',
            'created_date' => 'by Job Request Created Date'
        ]
    ],

    'color' => [
        'count_members_in_job' => '[0,9] danger|[10,24] warning|[25,49] primary|[50,99] info|[100,Inf] success'
    ],

    'closed_by' => [
        'dispatch' => 'Closed by Dispatch'
    ],

    'cancelled_by' => [
        'contractor' => 'Cancelled by Contractor'
    ]
];
