<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 12/2/2016
 * Time: 1:12 PM
 */

namespace Bct\Projects\Providers;

use Bct\Projects\Console\ArrangeProjectScopes;
use Cartalyst\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
        $this->commands('command.pp.project.scopes.arrange');
    }


    protected function registerCommands()
    {
        $this->app['command.pp.project.scopes.arrange'] = $this->app->share(function ($app) {
            return new ArrangeProjectScopes();
        });
    }

}