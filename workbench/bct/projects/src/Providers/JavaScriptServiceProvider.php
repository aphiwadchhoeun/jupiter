<?php namespace Bct\Projects\Providers;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 12:40
 */

use Bct\Projects\Libs\Laracasts\Utilities\LaravelViewBinder;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Laracasts\Utilities\JavaScript\PHPToJavaScriptTransformer;

class JavaScriptServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('JavaScriptProjects', function ($app) {
            $view = 'bct/projects::php_vars_to_js';
            $namespace = 'BctProjects';

            $binder = new LaravelViewBinder($app['events'], $view);

            return new PHPToJavaScriptTransformer($binder, $namespace);
        });
    }

    /**
     * Publish the plugin configuration.
     */
    public function boot()
    {
        AliasLoader::getInstance()->alias(
            'JavaScriptProjects',
            'Bct\Projects\Libs\Laracasts\Utilities\JavaScriptProjectsFacade'
        );
    }

}
