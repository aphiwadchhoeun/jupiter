<?php namespace Bct\Projects\Providers;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 14:29
 */


use Cartalyst\Support\ServiceProvider;

class MemberServiceProvider extends ServiceProvider {

    /**
     * {@inheritDoc}
     */
    public function boot() {
        // Register the attributes namespace
        $this->app['platform.attributes.manager']->registerNamespace(
            $this->app['Bct\Projects\Models\Member']
        );

        // Subscribe the registered event handler
        //$this->app['events']->subscribe('bct.projects.member.handler.event');
    }

    /**
     * {@inheritDoc}
     */
    public function register() {
        // Register the repository
        $this->bindIf('bct.projects.member', 'Bct\Projects\Repositories\Member\MemberRepository');

        // Register the data handler
        //$this->bindIf('bct.projects.member.handler.data', 'Bct\Projects\Handlers\Member\MemberDataHandler');

        // Register the event handler
        //$this->bindIf('bct.projects.member.handler.event', 'Bct\Projects\Handlers\Member\MemberEventHandler');

        // Register the validator
        //$this->bindIf('bct.projects.member.validator', 'Bct\Projects\Validator\Member\MemberValidator');
    }

}
