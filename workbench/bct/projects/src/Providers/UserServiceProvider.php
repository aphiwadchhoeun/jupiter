<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider {

    /**
     * {@inheritDoc}
     */
    public function boot() {
        // Register the attributes namespace
        $this->app['platform.attributes.manager']->registerNamespace(
            $this->app['Bct\Projects\Models\User']
        );

        // Subscribe the registered event handler
        $this->app['events']->subscribe('bct.projects.user.handler.event');
    }

    /**
     * {@inheritDoc}
     */
    public function register() {
        // Register the repository
        $this->bindIf('bct.projects.user', 'Bct\Projects\Repositories\User\UserRepository');

        // Register the data handler
        $this->bindIf('bct.projects.user.handler.data', 'Bct\Projects\Handlers\User\UserDataHandler');

        // Register the event handler
        $this->bindIf('bct.projects.user.handler.event', 'Bct\Projects\Handlers\User\UserEventHandler');

        // Register the validator
        $this->bindIf('bct.projects.user.validator', 'Bct\Projects\Validator\User\UserValidator');

        // Register the mailer
        $this->bindIf('bct.projects.user.mailer', 'Bct\Projects\Mailer\UserMailer');

		// Register the auth repository
		$this->bindIf('bct.projects.user.auth', 'Bct\Projects\Repositories\User\AuthRepository');
    }

}
