<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Contact'],
			$this->app['Bct\Projects\Models\Views\ContactView'],
			$this->app['Bct\Projects\Models\Views\MarketShare'],
			$this->app['Bct\Projects\Models\Views\SummaryProject']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.contact.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.contact', 'Bct\Projects\Repositories\Contact\ContactRepository');

		// Register the data handler
		$this->bindIf('bct.projects.contact.handler.data', 'Bct\Projects\Handlers\Contact\ContactDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.contact.handler.event', 'Bct\Projects\Handlers\Contact\ContactEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.contact.validator', 'Bct\Projects\Validator\Contact\ContactValidator');
	}

}
