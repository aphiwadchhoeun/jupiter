<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class GroupServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Group']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.group.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.group', 'Bct\Projects\Repositories\Group\GroupRepository');

		// Register the data handler
		$this->bindIf('bct.projects.group.handler.data', 'Bct\Projects\Handlers\Group\GroupDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.group.handler.event', 'Bct\Projects\Handlers\Group\GroupEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.group.validator', 'Bct\Projects\Validator\Group\GroupValidator');
	}

}
