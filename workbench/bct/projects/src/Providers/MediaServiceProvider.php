<?php namespace Bct\Projects\Providers;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.16
 * Time: 12:09
 */

use Cartalyst\Support\ServiceProvider;
use Request;
use URL;

class MediaServiceProvider extends ServiceProvider
{

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        // Register the attributes namespace
        $this->app['platform.attributes.manager']->registerNamespace(
            $this->app['Bct\Projects\Models\Media']
        );
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        // Register the repository
        $this->bindIf('bct.projects.media', 'Bct\Projects\Repositories\Media\MediaRepository');

        // Register the validator
        $this->bindIf('bct.projects.media.validator', 'Bct\Projects\Validator\Media\MediaValidator');
    }

}
