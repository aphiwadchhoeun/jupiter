<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ListsServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Lists']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.lists.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.lists', 'Bct\Projects\Repositories\Lists\ListsRepository');

		// Register the data handler
		$this->bindIf('bct.projects.lists.handler.data', 'Bct\Projects\Handlers\Lists\ListsDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.lists.handler.event', 'Bct\Projects\Handlers\Lists\ListsEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.lists.validator', 'Bct\Projects\Validator\Lists\ListsValidator');
	}

}
