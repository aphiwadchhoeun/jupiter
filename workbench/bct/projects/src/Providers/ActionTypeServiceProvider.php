<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ActionTypeServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\ActionType']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.action_type.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.action_type', 'Bct\Projects\Repositories\ActionType\ActionTypeRepository');

		// Register the data handler
		$this->bindIf('bct.projects.action_type.handler.data', 'Bct\Projects\Handlers\ActionType\ActionTypeDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.action_type.handler.event', 'Bct\Projects\Handlers\ActionType\ActionTypeEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.action_type.validator', 'Bct\Projects\Validator\ActionType\ActionTypeValidator');
	}

}
