<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ScopeServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Scope']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.scope.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.scope', 'Bct\Projects\Repositories\Scope\ScopeRepository');

		// Register the data handler
		$this->bindIf('bct.projects.scope.handler.data', 'Bct\Projects\Handlers\Scope\ScopeDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.scope.handler.event', 'Bct\Projects\Handlers\Scope\ScopeEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.scope.validator', 'Bct\Projects\Validator\Scope\ScopeValidator');
	}

}
