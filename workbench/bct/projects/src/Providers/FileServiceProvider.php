<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\File']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.file.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.file', 'Bct\Projects\Repositories\File\FileRepository');

		// Register the data handler
		$this->bindIf('bct.projects.file.handler.data', 'Bct\Projects\Handlers\File\FileDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.file.handler.event', 'Bct\Projects\Handlers\File\FileEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.file.validator', 'Bct\Projects\Validator\File\FileValidator');
	}

}
