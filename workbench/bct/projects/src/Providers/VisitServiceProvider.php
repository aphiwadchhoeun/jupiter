<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class VisitServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Visit']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.visit.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.visit', 'Bct\Projects\Repositories\Visit\VisitRepository');

		// Register the data handler
		$this->bindIf('bct.projects.visit.handler.data', 'Bct\Projects\Handlers\Visit\VisitDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.visit.handler.event', 'Bct\Projects\Handlers\Visit\VisitEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.visit.validator', 'Bct\Projects\Validator\Visit\VisitValidator');
	}

}
