<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ContractorServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Contractor'],
			$this->app['Bct\Projects\Models\ContractorAddress'],
			$this->app['Bct\Projects\Models\ContractorPhone'],
			$this->app['Bct\Projects\Models\ContractorEmail'],
			$this->app['Bct\Projects\Models\ContractorLicense'],
			$this->app['Bct\Projects\Models\ContractorCompany'],
			$this->app['Bct\Projects\Models\ContractorNotes']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.contractors.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.contractors', 'Bct\Projects\Repositories\Contractor\ContractorRepository');

		$this->bindIf('bct.projects.contractors.address', 'Bct\Projects\Repositories\Contractor\ContractorAddressRepository');
		$this->bindIf('bct.projects.contractors.phone', 'Bct\Projects\Repositories\Contractor\ContractorPhoneRepository');
		$this->bindIf('bct.projects.contractors.email', 'Bct\Projects\Repositories\Contractor\ContractorEmailRepository');
		$this->bindIf('bct.projects.contrcontractorsactor.notes', 'Bct\Projects\Repositories\Contractor\ContractorNotesRepository');
		$this->bindIf('bct.projects.contractors.company', 'Bct\Projects\Repositories\Contractor\ContractorCompanyRepository');
		$this->bindIf('bct.projects.contractors.license', 'Bct\Projects\Repositories\Contractor\ContractorLicenseRepository');


		// Register the data handler
		$this->bindIf('bct.projects.contractors.handler.data', 'Bct\Projects\Handlers\Contractor\ContractorDataHandler');
		$this->bindIf('bct.projects.contractors.address.handler.data', 'Bct\Projects\Handlers\Contractor\ContractorAddressDataHandler');
		$this->bindIf('bct.projects.contractors.phone.handler.data', 'Bct\Projects\Handlers\Contractor\ContractorPhoneDataHandler');
		$this->bindIf('bct.projects.contractors.email.handler.data', 'Bct\Projects\Handlers\Contractor\ContractorEmailDataHandler');
		$this->bindIf('bct.projects.contractors.notes.handler.data', 'Bct\Projects\Handlers\Contractor\ContractorNotesDataHandler');
		$this->bindIf('bct.projects.contractors.company.handler.data', 'Bct\Projects\Handlers\Contractor\ContractorCompanyDataHandler');
		$this->bindIf('bct.projects.contractors.license.handler.data', 'Bct\Projects\Handlers\Contractor\ContractorLicenseDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.contractors.handler.event', 'Bct\Projects\Handlers\Contractor\ContractorEventHandler');

		// Register the validator

		$this->bindIf('bct.projects.contractors.validator', 'Bct\Projects\Validator\Contractor\ContractorValidator');
		$this->bindIf('bct.projects.user.profile.contractors.validator', 'Bct\Projects\Validator\Contractor\UserProfileContractorValidator');
		$this->bindIf('bct.projects.contractors.address.validator', 'Bct\Projects\Validator\Contractor\ContractorAddressValidator');
		$this->bindIf('bct.projects.contractors.phone.validator', 'Bct\Projects\Validator\Contractor\ContractorPhoneValidator');
		$this->bindIf('bct.projects.contractors.email.validator', 'Bct\Projects\Validator\Contractor\ContractorEmailValidator');
		$this->bindIf('bct.projects.contractors.notes.validator', 'Bct\Projects\Validator\Contractor\ContractorNotesValidator');
		$this->bindIf('bct.projects.contractors.company.validator', 'Bct\Projects\Validator\Contractor\ContractorCompanyValidator');
		$this->bindIf('bct.projects.contractors.license.validator', 'Bct\Projects\Validator\Contractor\ContractorLicenseValidator');

	}

}















