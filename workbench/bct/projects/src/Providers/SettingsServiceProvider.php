<?php namespace Bct\Projects\Providers;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 11:05
 */

use Cartalyst\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        // Register the attributes namespace
        $this->app['platform.attributes.manager']->registerNamespace($this->app['Bct\Projects\Models\Config']);
    }


    /**
     * {@inheritDoc}
     */
    public function register()
    {
        // Register the repository
        $this->bindIf('bct.projects.settings', 'Bct\Projects\Repositories\Settings\SettingsRepository');
    }

}
