<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ContactTypeServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\ContactType']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.contact_type.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.contact_type', 'Bct\Projects\Repositories\ContactType\ContactTypeRepository');

		// Register the data handler
		$this->bindIf('bct.projects.contact_type.handler.data', 'Bct\Projects\Handlers\ContactType\ContactTypeDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.contact_type.handler.event', 'Bct\Projects\Handlers\ContactType\ContactTypeEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.contact_type.validator', 'Bct\Projects\Validator\ContactType\ContactTypeValidator');
	}

}
