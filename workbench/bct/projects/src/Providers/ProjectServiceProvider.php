<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ProjectServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Project']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.project.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.project', 'Bct\Projects\Repositories\Project\ProjectRepository');

		// Register the data handler
		$this->bindIf('bct.projects.project.handler.data', 'Bct\Projects\Handlers\Project\ProjectDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.project.handler.event', 'Bct\Projects\Handlers\Project\ProjectEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.project.validator', 'Bct\Projects\Validator\Project\ProjectValidator');
	}

}
