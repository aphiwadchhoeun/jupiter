<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ActionServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Action']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('bct.projects.action.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.action', 'Bct\Projects\Repositories\Action\ActionRepository');

		// Register the data handler
		$this->bindIf('bct.projects.action.handler.data', 'Bct\Projects\Handlers\Action\ActionDataHandler');

		// Register the event handler
		$this->bindIf('bct.projects.action.handler.event', 'Bct\Projects\Handlers\Action\ActionEventHandler');

		// Register the validator
		$this->bindIf('bct.projects.action.validator', 'Bct\Projects\Validator\Action\ActionValidator');
	}

}
