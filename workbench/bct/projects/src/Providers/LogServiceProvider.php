<?php namespace Bct\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class LogServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot() {
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Bct\Projects\Models\Log']
		);
	}

	/**
	 * {@inheritDoc}
	 */
	public function register() {
		// Register the repository
		$this->bindIf('bct.projects.Log', 'Bct\Projects\Repositories\Log\LogRepository');
	}

}
