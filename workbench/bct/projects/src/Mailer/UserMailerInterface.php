<?php namespace Bct\Projects\Mailer;



use Cartalyst\Sentinel\Users\UserInterface;

interface UserMailerInterface {

    /**
     * Send password reminder email
     *
     * @param UserInterface $user
     * @param $subject
     * @param $view
     * @param array $data
     * @return mixed
     */
    public function reminderEmail(UserInterface $user, $subject, $view, array $data = []);

    /**
     * Send notification new role
     *
     * @param UserInterface $user
     * @param $subject
     * @param $view
     * @param array $data
     * @return mixed
     */
    public function notifyNewRole(UserInterface $user, $subject, $view, array $data = []);

}
