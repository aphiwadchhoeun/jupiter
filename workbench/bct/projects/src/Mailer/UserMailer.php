<?php namespace Bct\Projects\Mailer;

use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Support\Mailer;

class UserMailer extends Mailer implements UserMailerInterface {

    /**
     * Send password reminder email
     *
     * @param UserInterface $user
     * @param $subject
     * @param $view
     * @param array $data
     * @return mixed
     */
    public function reminderEmail(UserInterface $user, $subject, $view, array $data = []) {
        try {
            // Prepare the email recipient
            $recipient = [$user->email, "{$user->first_name} {$user->last_name}"];

            // Set the email subject
            $this->setSubject($subject);

            // Set the email recipient
            $this->addTo(
                array_get($recipient, 0),
                array_get($recipient, 1)
            );

            // Set the email view
            $this->setView(
                "bct/projects::emails/{$view}", $data
            );

            // Send the email
            $this->send();
        } catch (\Exception $e) {
            return app('bct.unionimpactbase.dev.mailer')->devAlert($this, $e->getMessage());
        }
    }

    /**
     * Send notification new role
     *
     * @param UserInterface $user
     * @param $subject
     * @param $view
     * @param array $data
     * @return mixed
     */
    public function notifyNewRole(UserInterface $user, $subject, $view, array $data = []) {
        try {
            // Prepare the email recipient
            $recipient = [$user->email, "{$user->first_name} {$user->last_name}"];

            // Set the email subject
            $this->setSubject($subject);

            // Set the email recipient
            $this->addTo(
                array_get($recipient, 0),
                array_get($recipient, 1)
            );

            // Set the email view
            $this->setView(
                "bct/projects::emails/{$view}", $data
            );

            // Send the email
            $this->send();
        } catch (\Exception $e) {
            app('bct.unionimpactbase.dev.mailer')->devAlert($this, $e->getMessage());
        }
    }
}
