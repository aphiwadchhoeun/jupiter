<?php namespace Bct\Projects\Queues;

/**
 * Created by PhpStorm.
 * User: Vic
 * Date: 14.10.16
 * Time: 14:54
 */

use Bct\Unionimpactbase\Queues\BaseJob;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class FieldReportsQueueWorker extends BaseJob implements SelfHandling, ShouldQueue {

    /*
     * THIS WORKER NEED TO BE RUN ON QUEUE "queue_jobs"
     */

    /**
     * PdfQueueWorker constructor.
     * @param $filename
     * @param $results
     * @param $options
     */
    public function __construct($filename, $results, $options) {
        parent::__construct($filename, $results, $options);
    }

    /**
     * Job run
     */
    public function handle() {
        $snappy = app('snappy.pdf.wrapper');

        $snappy->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-left', 2)
            ->setOption('margin-right', 2)
            ->setOption('margin-top', 5)
            ->setOption('margin-bottom', 5)
            ->setOption('footer-left', null)
            ->setOption('footer-center', null)
            ->setOption('header-line', true)
            ->setOption('header-font-size', 14)
            ->setOption('header-center', array_get($this->options, 'title', ''));

        $snappy->loadView($this->options['pdf_view'],
            [
                'results' => isset($this->results) ? $this->results : [],
            ]);

        // start uploading file
        $filepath = $this->uploadFile($snappy->output(), 'pdf');

        $this->postQueue($filepath);
    }

}