<?php namespace Bct\Projects\Traits\Model;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 23.02.16
 * Time: 15:19
 */

trait DefaultTableTrait {
    protected $_table;

    public function setDefaultTable()
    {
        $this->_table = $this->table;
    }

    public function getDefaultTable()
    {
        return $this->_table;
    }

    public function getQualifiedDeletedAtColumn()
    {
        return $this->getDefaultTable().'.'.$this->getDeletedAtColumn();
    }
}