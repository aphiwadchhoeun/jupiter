<?php namespace Bct\Projects\Repositories\File;

/**
 * Part of the Platform Media extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Media extension
 * @version    2.0.2
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Aws\S3\S3Client;
use Carbon\Carbon;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Cartalyst\Filesystem\Exceptions\FileExistsException;
use Cartalyst\Filesystem\Exceptions\InvalidFileException;
use Cartalyst\Filesystem\Exceptions\InvalidMimeTypeException;
use Cartalyst\Filesystem\Exceptions\MaxFileSizeExceededException;

class FileRepository implements FileRepositoryInterface {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Filesystem instance.
     *
     * @var \Cartalyst\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * The Eloquent model name.
     *
     * @var string
     */
    protected $model;

    /**
     * Holds the occurred error.
     *
     * @var string
     */
    protected $error;

    /**
     * The Tags repository instance.
     *
     * @var \Platform\Tags\Repositories\TagsRepositoryInterface
     */
    protected $tags;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app) {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->filesystem = $app['filesystem'];

        $this->setValidator($app['bct.projects.file.validator']);

        $this->setModel(get_class($app['Bct\Projects\Models\File']));
    }

    /**
     * {@inheritDoc}
     */
    public function grid() {
        return $this->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function find($id) {
        return $this->container['cache']->rememberForever('bct.projects.file.' . $id, function () use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function findByPath($path) {
        return $this->container['cache']->rememberForever('bct.projects.file.path.' . $path, function () use ($path) {
            return $this->createModel()->wherePath($path)->first();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getAllowedMimes() {
        return $this->filesystem->getAllowedMimes();
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($id, array $data) {
        return $this->validator->on('update')->validate($data);
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $data) {
        return $this->validator->on('create')->validate($data);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpload(UploadedFile $file) {
        try {
            $this->filesystem->validateFile($file);

            return true;
        } catch (InvalidFileException $e) {
            $this->setError(trans('platform/media::message.invalid_file'));
        } catch (MaxFileSizeExceededException $e) {
            $this->setError(trans('platform/media::message.file_size_exceeded'));
        } catch (InvalidMimeTypeException $e) {
            $this->setError(trans('platform/media::message.invalid_mime'));
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function upload(UploadedFile $uploadedFile, array $input) {
        try {
            $media = $this->createModel();

            $media->save();

            // Sanitize the file name
            $fileName = $this->prepareFileName(
                array_get($input, 'name', $uploadedFile->getClientOriginalName()),
                $media->id,
                array_get($input, 'z_projects_id')
            );

            // Upload the file
            $file = $this->filesystem->disk('s3')->upload($uploadedFile, $fileName);

            // If the file is an image, we get the image size
            $imageSize = $file->getImageSize();

            $input = array_merge([
                'name'      => $uploadedFile->getClientOriginalName(),
                'path'      => $file->getPath(),
                'extension' => $file->getExtension(),
                'mime'      => $file->getMimetype(),
                'size'      => $file->getSize(),
                'is_image'  => $file->isImage(),
                'width'     => $imageSize['width'],
                'height'    => $imageSize['height'],
            ], $input);

            $media->fill($input)->save();

            $this->fireEvent('bct.projects.file.uploaded', [$media, $file, $uploadedFile]);

            return $media;
        } catch (FileExistsException $e) {
            $this->setError(trans('platform/media::message.file_exists'));

            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function create($data) {
        return with($model = $this->createModel())->create($data);
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input, $uploadedFile = null) {
        $media = $this->find($id);

        $this->fireEvent('bct.projects.file.updating', [$media]);

        if ($uploadedFile instanceof UploadedFile) {
            if ($this->validForUpload($uploadedFile)) {
                // Delete the old media file
                $this->filesystem->disk('s3')->delete($media->path);

                // Sanitize the file name
                $fileName = $this->sanitizeFileName(
                    array_get($input, 'name', $uploadedFile->getClientOriginalName())
                );

                // Upload the file
                $file = $this->filesystem->disk('s3')->upload($uploadedFile, $fileName);

                $this->fireEvent('bct.projects.file.uploaded', [$media, $file, $uploadedFile]);

                $imageSize = $file->getImageSize();

                // Update the media entry
                $input = array_merge([
                    'path'      => $file->getPath(),
                    'extension' => $file->getExtension(),
                    'mime'      => $file->getMimetype(),
                    'size'      => $file->getSize(),
                    'is_image'  => $file->isImage(),
                    'width'     => $imageSize['width'],
                    'height'    => $imageSize['height'],
                ], $input);
            } else {
                return [false, trans('bct/projects::files/message.error.update')];
            }
        }

        // Update the media entry
        if ($media->fill($input)->save()) {
            $this->fireEvent('bct.projects.file.updated', [$media]);

            return [true, null];
        }

        return [false, trans('bct/projects::files/message.error.update')];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id) {
        if ($media = $this->find($id)) {
            $file = $this->filesystem->disk('s3')->get($media->path);

            $this->fireEvent('bct.projects.file.deleting', [$media, $file]);

            $this->filesystem->disk('s3')->delete($media->path);

            $this->fireEvent('bct.projects.file.deleted', [$media]);

            $media->delete();

            return true;
        }

        $this->setError(trans('platform/media::message.error.delete'));

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getError() {
        return $this->error;
    }

    /**
     * {@inheritDoc}
     */
    public function setError($error) {
        $this->error = $error;
    }

    /**
     * Sanitizes the file name.
     *
     * @param  string $fileName
     * @return string
     */
    protected function sanitizeFileName($fileName) {
        $regex = ['#(\.){2,}#', '#[^A-Za-z0-9\.\_\- ]#', '#^\.#', '#[ ]#', '![_]+!u'];

        return preg_replace($regex, '_', strtolower($fileName));
    }

    /**
     * Prepares the filename by sanitizing it and
     * appending the media id to the end.
     *
     * @param  string $fileName
     * @param  string $id
     * @return string
     */
    protected function prepareFileName($fileName, $id, $project_id) {
        $fileName = $this->sanitizeFileName($fileName);

        return $project_id . '/' . $id . "." . pathinfo($fileName, PATHINFO_EXTENSION);
    }

    protected function prepareFileNameDefault($id, $extension = 'json')
    {
        return md5(Carbon::now()->timestamp . $id) . '.' . $extension;

    }

    public function isImage(UploadedFile $file)
    {
        return in_array($file->getMimetype(), $this->imagesMimeTypes);
    }


    /**
     * Return the image width and height.
     *
     * @param UploadedFile $file
     * @param              $path
     *
     * @return array
     */
    public function getImageSize(UploadedFile $file, $path)
    {
        if (! $this->isImage($file)) {
            return [ 'width' => 0, 'height' => 0 ];
        }

        $raw = $this->filesystem->disk('s3')->getAdapter()->read($path);

        $image = imagecreatefromstring($raw['contents']);

        $width  = imagesx($image);
        $height = imagesy($image);

        return compact('width', 'height');
    }

    /**
     * Returns the file extension.
     *
     * @return string
     */
    public function getExtension($path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    public function hasFileOnDisk(Media $media)
    {
        return is_http_response_code_success_projects(route('bct.projects.media.view', $media->path));
    }

    public function getSizeOfFile(Media $media)
    {
        return $this->filesystem->disk('s3')->getSize($media->path);
    }

    public function getMimetypeOfFile(Media $media)
    {
        return $this->filesystem->disk('s3')->getMimetype($media->path);
    }

}
