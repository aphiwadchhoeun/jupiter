<?php namespace Bct\Projects\Repositories\Media;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.16
 * Time: 12:11
 */

use Bct\Projects\Models\Media;
use Carbon\Carbon;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Cartalyst\Filesystem\Exceptions\FileExistsException;
use Cartalyst\Filesystem\Exceptions\InvalidFileException;
use Cartalyst\Filesystem\Exceptions\InvalidMimeTypeException;
use Cartalyst\Filesystem\Exceptions\MaxFileSizeExceededException;

class MediaRepository implements MediaRepositoryInterface
{
    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Filesystem instance.
     *
     * @var \Cartalyst\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * The Eloquent model name.
     *
     * @var string
     */
    protected $model;

    /**
     * Holds the occurred error.
     *
     * @var string
     */
    protected $error;

    /**
     * The Tags repository instance.
     *
     * @var \Platform\Tags\Repositories\TagsRepositoryInterface
     */
    protected $tags;

    protected $imagesMimeTypes = [
        'image/bmp',
        'image/gif',
        'image/jpeg',
        'image/png',
    ];

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container  $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->setContainer($app);

        $this->tags = $app['platform.tags'];

        $this->setDispatcher($app['events']);

        $this->filesystem = $app['filesystem'];

        $this->setValidator($app['platform.media.validator']);

        $this->setModel(get_class($app['Bct\Projects\Models\Media']));
    }

    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this->createModel()->with('tags');
    }

    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->container['cache']->rememberForever('bct.projects.media.'.$id, function () use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function findByPath($path)
    {
        return $this->container['cache']->rememberForever('bct.projects.media.path.'.$path, function () use ($path) {
            return $this->createModel()->wherePath($path)->first();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getAllTags()
    {
        return $this->createModel()->allTags()->lists('name');
    }

    /**
     * {@inheritDoc}
     */
    public function getAllowedMimes()
    {
        return $this->filesystem->getAllowedMimes();
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($id, array $data)
    {
        return $this->validator->on('update')->validate($data);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpload(UploadedFile $file)
    {
        try {
            $this->filesystem->validateFile($file);

            return true;
        } catch (InvalidFileException $e) {
            $this->setError(trans('platform/media::message.invalid_file'));
        } catch (MaxFileSizeExceededException $e) {
            $this->setError(trans('platform/media::message.file_size_exceeded'));
        } catch (InvalidMimeTypeException $e) {
            $this->setError(trans('platform/media::message.invalid_mime'));
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function upload(UploadedFile $uploadedFile, array $input)
    {

        try {
            $media = $this->createModel();

            $media->save();

            // Sanitize the file name
            $fileName = $uploadedFile->getClientOriginalName();
            $fileNameFull = $this->prepareFileNameWithExtension($uploadedFile, $media->id);

            // Get the submitted tags
            $tags = array_pull($input, 'tags', []);

            $path = array_get($input, 'folder', '/') . $fileNameFull;

            // Upload the file
            $file = $this->filesystem->disk('s3')->put($path, file_get_contents($uploadedFile));

            // If the file is an image, we get the image size
            $imageSize = $this->getImageSize($uploadedFile, $path);

            $input = [
                'name'          => $fileName,
                'path'          => $path,
                'extension'     => $this->getExtension($path),
                'mime'          => $uploadedFile->getMimetype(),
                'size'          => $uploadedFile->getSize(),
                'is_image'      => $this->isImage($uploadedFile),
                'width'         => $imageSize['width'],
                'height'        => $imageSize['height'],
                'z_projects_id' => array_get($input, 'z_projects_id', 0),
                'z_contractors_id' => array_get($input, 'z_contractors_id', 0),
                'user_id'       => array_get($input, 'user_id', 0),
                'description'   => array_get($input, 'description', ''),
            ];

            $media->fill($input)->save();

            // Set the tags on the media entry
            $this->tags->set($media, $tags);

            return $media;
        } catch (FileExistsException $e) {
            $media->delete();
            $this->setError(trans('platform/media::message.file_exists'));

            return false;
        }
    }

    public function upload_content($file_contents, array $input)
    {

        try {
            $media = $this->createModel();

            $media->save();

            $extension = array_pull($input, 'extension', 'json');

            // Sanitize the file name
            $fileName = $this->prepareFileNameDefault($media->id, $extension);

            // Get the submitted tags
            $tags = array_pull($input, 'tags', []);

            $path = array_get($input, 'folder', '/') . $fileName;

            // Upload the file
            $file = $this->filesystem->disk('s3')->put($path, $file_contents);

            $input = array_merge([
                'name'      => $fileName,
                'path'      => $path,
                'extension' => $this->getExtension($path),
                'mime'      => $this->filesystem->disk('s3')->mimeType($path),
                'size'      => $this->filesystem->disk('s3')->size($path),
            ], $input);

            $media->fill($input)->save();

            // Set the tags on the media entry
            $this->tags->set($media, $tags);

            return $media;
        } catch (FileExistsException $e) {
            $this->setError(trans('platform/media::message.file_exists'));

            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function create($data)
    {
        return with($model = $this->createModel())->create($data);
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input, $uploadedFile = null)
    {
        $media = $this->find($id);

        $this->fireEvent('platform.media.updating', [ $media ]);

        // Get the submitted tags
        $tags = array_pull($input, 'tags', []);

        if ($uploadedFile instanceof UploadedFile) {
            if ($this->validForUpload($uploadedFile)) {

                // Delete the old media file
                if ($this->filesystem->disk('s3')->has($media->path)) {
                    $this->filesystem->disk('s3')->delete($media->path);
                }

                // Sanitize the file name
                $fileName = $this->sanitizeFileName(
                    array_get($input, 'name', $uploadedFile->getClientOriginalName())
                );

                // Upload the file
                $file = $this->filesystem->disk('s3')->upload($uploadedFile, $fileName);

                $this->fireEvent('platform.media.uploaded', [ $media, $file, $uploadedFile ]);

                $imageSize = $this->getImageSize($file);

                // Update the media entry
                $input = array_merge([
                    'path'      => $file->getPath(),
                    'extension' => $this->getExtension($file->getPath()),
                    'mime'      => $file->getMimetype(),
                    'size'      => $file->getSize(),
                    'is_image'  => $this->isImage($file),
                    'width'     => $imageSize['width'],
                    'height'    => $imageSize['height'],
                ], $input);
            } else {
                return false;
            }
        }

        // Set the tags on the media entry
        $this->tags->set($media, $tags);

        // Update the media entry
        $media->fill($input)->save();

        $this->fireEvent('platform.media.updated', [ $media ]);

        return $media;
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id)
    {
        if ($media = $this->find($id)) {
            //$this->fireEvent('platform.media.deleting', [ $media, $file ]);

            if ($this->filesystem->disk('s3')->has($media->path)) {
                $this->filesystem->disk('s3')->delete($media->path);
            }

            //$this->fireEvent('platform.media.deleted', [ $media ]);

            $media->delete();

            return true;
        }

        //$this->setError(trans('platform/media::message.error.delete'));

        return false;
    }

    /**
     * Sets the media private.
     *
     * @param  int  $id
     * @return void
     */
    public function makePrivate($id)
    {
        if ($media = $this->find($id)) {
            $media->private = true;
            $media->save();
        }
    }

    /**
     * Sets the media public.
     *
     * @param  int  $id
     * @return void
     */
    public function makePublic($id)
    {
        if ($media = $this->find($id)) {
            $media->private = false;
            $media->save();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * {@inheritDoc}
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * Sanitizes the file name.
     *
     * @param  string  $fileName
     * @return string
     */
    protected function sanitizeFileName($fileName)
    {
        $regex = [ '#(\.){2,}#', '#[^A-Za-z0-9\.\_\- ]#', '#^\.#', '#[ ]#', '![_]+!u' ];

        return preg_replace($regex, '_', strtolower($fileName));
    }


    /**
     * Prepares the filename by sanitizing it and
     * appending the media id to the end.
     *
     * @param UploadedFile $file
     * @param              $id
     *
     * @return string
     */
    protected function prepareFileName(UploadedFile $file, $id)
    {
        return md5($file->getClientOriginalName() . $file->getSize() . Carbon::now()->timestamp . $id);
    }

    protected function prepareFileNameWithExtension(UploadedFile $file, $id)
    {
        return md5($file->getClientOriginalName() . $file->getSize() . Carbon::now()->timestamp . $id) . '.' . $file->getClientOriginalExtension();
    }

    protected function prepareFileNameDefault($id, $extension = 'json')
    {
        return md5(Carbon::now()->timestamp . $id) . '.' . $extension;

    }

    public function isImage(UploadedFile $file)
    {
        return in_array($file->getMimetype(), $this->imagesMimeTypes);
    }


    /**
     * Return the image width and height.
     *
     * @param UploadedFile $file
     * @param              $path
     *
     * @return array
     */
    public function getImageSize(UploadedFile $file, $path)
    {
        if (! $this->isImage($file)) {
            return [ 'width' => 0, 'height' => 0 ];
        }

        $raw = $this->filesystem->disk('s3')->getAdapter()->read($path);

        $image = imagecreatefromstring($raw['contents']);

        $width  = imagesx($image);
        $height = imagesy($image);

        return compact('width', 'height');
    }

    /**
     * Returns the file extension.
     *
     * @return string
     */
    public function getExtension($path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    public function hasFileOnDisk(Media $media)
    {
        return is_http_response_code_success_projects(route('bct.projects.media.view', $media->path));
    }

    public function getSizeOfFile(Media $media)
    {
        return $this->filesystem->disk('s3')->getSize($media->path);
    }

    public function getMimetypeOfFile(Media $media)
    {
        return $this->filesystem->disk('s3')->getMimetype($media->path);
    }
}
