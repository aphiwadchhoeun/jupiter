<?php namespace Bct\Projects\Repositories;

    /**
 * Created by PhpStorm.
 * User: roman
 * Date: 18.02.16
 * Time: 11:53
 */

use Bct\Projects\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Support\Traits;

class BaseRepository  {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    protected $currentUser;
    protected $currentUserModule;

    public function __construct()
    {
        $this->currentUser = Sentinel::getUser();
        $this->currentUserModule = (!is_null($this->currentUser)) ? User::find($this->currentUser->id) : null;
    }


}