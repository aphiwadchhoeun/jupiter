<?php namespace Bct\Projects\Repositories\Settings;

    /**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 11:02
 */
use Bct\Projects\Models\Role;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;

class SettingsRepository implements SettingsRepositoryInterface
{
    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Eloquent model.
     *
     * @var string
     */
    protected $model;

    protected $members;


    /**
     * SettingsRepository constructor.
     *
     * @param Container $app
     */
    public function __construct(
        Container $app
    ) {
        $this->setContainer($app);

        $this->setModel(get_class($app['Bct\Projects\Models\Config']));

        $this->app = $app;
    }

    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this->createModel();
    }

    public function changeConfiguration($input)
    {
        foreach ($input as $key => $value) {
            if (!is_null($value)) {
                $this->grid()->updateOrCreate([ 'item' => 'bct.projects.settings.' . $key ], [ 'value' => $value ]);
            }
        }
    }

}