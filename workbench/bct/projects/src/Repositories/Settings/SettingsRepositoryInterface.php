<?php namespace Bct\Projects\Repositories\Settings;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 11:04
 */

interface SettingsRepositoryInterface
{

    public function grid();


    public function changeConfiguration($input);

}