<?php namespace Bct\Projects\Repositories\ActionType;

use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ActionTypeRepository implements ActionTypeRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\ActionType\ActionTypeDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent projects model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.action_type.handler.data'];

		$this->setValidator($app['bct.projects.action_type.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ActionType']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel()->whereNull('deleted_at');
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('bct.projects.action_type.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('bct.projects.action_type.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($action_type, array $input)
	{
		return $this->validator->on('update',compact('action_type'))->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new action_type
		$action_type = $this->createModel();

		// Fire the 'bct.projects.action_type.creating' event
		if ($this->fireEvent('bct.projects.action_type.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the action_type
			$action_type->fill($data)->save();

			// Fire the 'bct.projects.action_type.created' event
			$this->fireEvent('bct.projects.action_type.created', [ $action_type ]);
		}

		return [ $messages, $action_type ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the action_type object
		$action_type = $this->find($id);

		// Fire the 'bct.projects.action_type.updating' event
		if ($this->fireEvent('bct.projects.action_type.updating', [ $action_type, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($action_type, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the action_type
			$action_type->fill($data)->save();

			// Fire the 'bct.projects.action_type.updated' event
			$this->fireEvent('bct.projects.action_type.updated', [ $action_type ]);
		}

		return [ $messages, $action_type ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the action_type exists
		if ($action_type = $this->find($id))
		{
			// Fire the 'bct.projects.action_type.deleted' event
			$this->fireEvent('bct.projects.action_type.deleted', [ $action_type ]);

			// Delete the action_type entry
			$action_type->delete();

            DB::table('z_project_action')->where('z_action_type_id', $action_type->id)->delete();

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
