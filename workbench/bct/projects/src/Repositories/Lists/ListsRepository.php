<?php namespace Bct\Projects\Repositories\Lists;

use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;
use Bct\Projects\Models\Lists;
use Bct\Projects\Models\Project;
use Illuminate\Support\Facades\DB;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Carbon\Carbon;

class ListsRepository implements ListsRepositoryInterface {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     * @var \Bct\Projects\Handlers\Lists\ListsDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent projects model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app) {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['bct.projects.lists.handler.data'];

        $this->setValidator($app['bct.projects.lists.validator']);

        $this->setModel(get_class($app['Bct\Projects\Models\Lists']));
    }

    /**
     * {@inheritDoc}
     */
    public function grid() {
        return $this->createModel()->with('group')->whereNull('deleted_at')->get();
    }

    public function gridReal() {
        return $this->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll() {
        return $this->gridReal()->get();
    }

    /**
     * {@inheritDoc}
     */
    public function find($id) {
        return $this->gridReal()->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input) {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($data, array $input) {
        return $this->validator->on('update')->setCustomAttributes([ $data ])->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input) {
        return !$id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input) {
        // Create a new lists
        $lists = $this->createModel();

        // Fire the 'bct.projects.lists.creating' event
        if ($this->fireEvent('bct.projects.lists.creating', [$input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Save the lists
            $lists->created_by = Sentinel::getUser()->id;
            $lists->fill($data)->save();

            // Fire the 'bct.projects.lists.created' event
            $this->fireEvent('bct.projects.lists.created', [$lists]);
        }

        return [$messages, $lists];
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input) {
        // Get the lists object
        $lists = $this->find($id);

        // Fire the 'bct.projects.lists.updating' event
        if ($this->fireEvent('bct.projects.lists.updating', [$lists, $input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);


        // Validate the submitted data
        $messages = $this->validForUpdate($lists->toArray(), $data);


        $data['is_active'] = $data['status'];

        unset($data['status']);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Update the lists
            $lists->fill($data)->save();

            // Fire the 'bct.projects.lists.updated' event
            $this->fireEvent('bct.projects.lists.updated', [$lists]);
        }

        return [$messages, $lists];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id) {
        // Check if the lists exists
        if ($lists = $this->find($id)) {
            // Fire the 'bct.projects.lists.deleted' event
            $this->fireEvent('bct.projects.lists.deleted', [$lists]);

            // Delete the lists entry
            $lists->delete();

            DB::table('z_project_list_project_join')->where('z_project_list_id', $lists->id)->delete();

            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function enable($id) {
        $this->validator->bypass();

        return $this->update($id, ['enabled' => true]);
    }

    /**
     * {@inheritDoc}
     */
    public function disable($id) {
        $this->validator->bypass();

        return $this->update($id, ['enabled' => false]);
    }


    /**
     * {@inheritDoc}
     */
    public function attach($id, $project_id) {
        try {
            // Get the project object
            $project = Project::find($project_id);
            $list = $this->find($id);


            $raw = DB::table('z_project_list_project_join as middle')->where('z_project_list_id', $list->id)->where('z_projects_id', $project->id)->count();

            if ($list) {
                if ($raw == 0) {
                    $project->lists()->attach($list, [
                        'created_by' => Sentinel::getUser()->id,
                        'is_active'  => true
                    ]);
                }
            }

        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function detach($id, $project_id) {
        try {
            // Get the group object
            $project = Project::find($project_id);

            $project->lists()->detach($id);


        } catch (Exception $e) {
            return false;
        }

        return true;
    }


}
