<?php namespace Bct\Projects\Repositories\ContactType;

use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ContactTypeRepository implements ContactTypeRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\ContactType\ContactTypeDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent projects model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contact_type.handler.data'];

		$this->setValidator($app['bct.projects.contact_type.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ContactType']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel()->whereNull('deleted_at');
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('bct.projects.contact_type.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('bct.projects.contact_type.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($contact_type, array $input)
	{
		return $this->validator->on('update',compact('contact_type'))->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new contact_type
		$contact_type = $this->createModel();

		// Fire the 'bct.projects.contact_type.creating' event
		if ($this->fireEvent('bct.projects.contact_type.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the contact_type
			$contact_type->fill($data)->save();

			// Fire the 'bct.projects.contact_type.created' event
			$this->fireEvent('bct.projects.contact_type.created', [ $contact_type ]);
		}

		return [ $messages, $contact_type ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the contact_type object
		$contact_type = $this->find($id);

		// Fire the 'bct.projects.contact_type.updating' event
		if ($this->fireEvent('bct.projects.contact_type.updating', [ $contact_type, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($contact_type, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the contact_type
			$contact_type->fill($data)->save();

			// Fire the 'bct.projects.contact_type.updated' event
			$this->fireEvent('bct.projects.contact_type.updated', [ $contact_type ]);
		}

		return [ $messages, $contact_type ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the contact_type exists
		if ($contact_type = $this->find($id))
		{
			// Fire the 'bct.projects.contact_type.deleted' event
			$this->fireEvent('bct.projects.contact_type.deleted', [ $contact_type ]);

			// Delete the contact_type entry
			$contact_type->delete();

            DB::table('z_project_contact_join')->where('z_contact_type_id', $contact_type->id)->delete();

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
