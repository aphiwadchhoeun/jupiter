<?php namespace Bct\Projects\Repositories\Visit;

use Bct\Projects\Repositories\BaseRepository;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Validator;
use Illuminate\Container\Container;
use DB;

class VisitRepository extends BaseRepository implements VisitRepositoryInterface {

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\Visit\VisitDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent projects model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * The repository.
	 *
	 * @var string
	 */
	protected $projects;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @param  ProjectRepositoryInterface  $projects
	 */
	public function __construct(Container $app, ProjectRepositoryInterface $projects)
	{
		parent::__construct();

		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.visit.handler.data'];

		$this->setValidator($app['bct.projects.visit.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\Visit']));

		$this->projects = $projects;
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('bct.projects.visit.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('bct.projects.visit.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($project_id, $id, array $input)
	{
		$input['z_projects_id'] = $project_id;
        
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new visit
		$visit = $this->createModel();

		// Fire the 'bct.projects.visit.creating' event
		if ($this->fireEvent('bct.projects.visit.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the visit
			$visit->fill($data)->save();

			// Fire the 'bct.projects.visit.created' event
			$this->fireEvent('bct.projects.visit.created', [ $visit ]);

			// Fire the 'bct.projects.project.updated' event
			$this->fireEvent('bct.projects.project.updated', [ $visit->project ]);
		}

		return [ $messages, $visit ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the visit object
		$visit = $this->find($id);

		// Fire the 'bct.projects.visit.updating' event
		if ($this->fireEvent('bct.projects.visit.updating', [ $visit, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($visit, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the visit
			$visit->fill($data)->save();

			// Fire the 'bct.projects.visit.updated' event
			$this->fireEvent('bct.projects.visit.updated', [ $visit ]);

			// Fire the 'bct.projects.project.updated' event
			$this->fireEvent('bct.projects.project.updated', [ $visit->project ]);
		}

		return [ $messages, $visit ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the visit exists
		if ($visit = $this->find($id))
		{
			// Fire the 'bct.projects.visit.deleted' event
			$this->fireEvent('bct.projects.visit.deleted', [ $visit ]);

			// Delete the visit entry
			$visit->delete();

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function attach($id, array $stages)
	{
		try
		{
			// Get the group object
			$visit = $this->find($id);

			$visit->stages()->sync($stages);

		} catch (\Exception $e)
		{
			return false;
		}

		return true;
	}

	public function dataGrid()
	{
		$data = $this->createModel()
				->fromView()
				->withTrashed();

		$columns = [
			'id',
			'project_name',
			'users_names',
			'region',
			'reason',
			'details',
			'notes_short',
			'created_custom',
			'created_at',
            'weather',
            'workers_count',
			'approved',
			'view_uri',
		];

		$title = input('filename', trans('bct/projects::visits/common.title'));
		$filename = input('filename', $title . "-" . date('M-d-Y'));
		$source  = 'Projects / Visits';


		$export_columns = [
            'project_name'  => [ trans('bct/projects::projects/model.general.project'), 'col-md-2' ],
            'reps'          => [ trans('bct/projects::visits/model.general.reps'), 'col-md-2' ],
        ];

        if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1) {
            $export_columns['weather'] = [ trans('bct/projects::visits/model.general.weather'), 'col-md-1' ];
        }

        if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1) {
            $export_columns['workers_count'] = [ trans('bct/projects::visits/model.general.workers_count'), 'col-md-2' ];
        }

        $export_columns['region'] = [ trans('bct/projects::projects/model.general.region'), 'col-md-1' ];
        $export_columns['notes'] = [
            trans('bct/projects::projects/model.general.follow_up_reason').' & '.trans('bct/projects::visits/model.general.notes'),
            'col-xs-3'
        ];
        $export_columns['date'] = [ trans('bct/projects::visits/model.general.date'), 'col-md-2' ];
        $export_columns['status'] = [ trans('bct/projects::projects/model.general.status_id'), 'col-md-1' ];



        $settings = [
			'sort'             => 'pp_reviews.created_at',
			'direction'        => 'desc',
			'pdf_filename'     => $filename,
			'csv_filename'     => $filename,
			'title'            => $title,
			'orientation'      => 'landscape',
			'pdf_view'         => 'cartalyst/data-grid::pdf_html',
			'export_source'    => $source,
            'export_columns' => $export_columns
		];

		$export_columns = $settings['export_columns'];

		$transformer = function ($element) use ($export_columns) {

			$element->created_custom = $element->created_custom;
			$element->notes_short = $element->notes_short;

			if (!input()->get('download', false)) {
				$element['view_uri'] = route('bct.projects.projects.details', $element->project_id);
			} else {

				$element[get_export_column_name($export_columns['project_name'])] = $element->project_name;
				$element[get_export_column_name($export_columns['reps'])]         = $element->users_names;
				$element[get_export_column_name($export_columns['region'])]       = $element->region;

                if (config_projects('bct.projects.settings.enable_project_visit_weather')) {
                    $element[get_export_column_name($export_columns['weather'])]       = $element->weather;
                }

                if (config_projects('bct.projects.settings.enable_project_visit_workers')) {
                    $element[get_export_column_name($export_columns['workers_count'])]       = $element->workers_count;
                }

				if(input()->get('download')==='pdf'){
					$element[get_export_column_name($export_columns['notes'])] = '<div><b>'.trans('bct/projects::projects/model.general.follow_up').':</b> '.$element->reason.'</div><div><b>'.trans('bct/projects::visits/model.general.notes').':</b> '.$element->notes_short.'</div>';
				}
				else{
					$element[get_export_column_name($export_columns['notes'])] = trans('bct/projects::projects/model.general.follow_up').": ".$element->reason.
						' '. trans('bct/projects::visits/model.general.notes').': '.$element->notes_short;
				}
				$element[get_export_column_name($export_columns['date'])]         = $element->created_custom;
				$element[get_export_column_name($export_columns['status'])]       = ( $element->approved) ? trans('bct/projects::reviews/model.general.approved') : trans('bct/projects::reviews/model.general.not_approved');
			}

			return $element;
		};

		return datagrid_init($data, $columns, $settings, $transformer);
	}

	public function approve($input_ids)
	{
		if (is_array($input_ids)) {
			foreach ($input_ids as $id) {
				$visit = $this->find($id);
				$visit->update([
					'approved' => 1,
					'approved_user_id' => $this->currentUser->id
				]);

				$this->fireEvent('bct.projects.visit.cache.clear', [ $visit ]);
			}
		} elseif ($input_ids === 'all') {
			$this->createModel()
				->where('approved', 0)
				->update([
					'approved' => 1,
					'approved_user_id' => $this->currentUser->id
				]);

			$this->fireEvent('bct.projects.visit.cache.clear.all');
		}
	}

	public function unapprove($input_ids)
	{
		if (is_array($input_ids)) {
			foreach ($input_ids as $id) {
				$visit = $this->find($id);
				$visit->update([
					'approved' => 0,
					'approved_user_id' => 0
				]);

				$this->fireEvent('bct.projects.visit.cache.clear', [ $visit ]);
			}
		} elseif ($input_ids === 'all') {
			$this->createModel()
				->where('approved', 1)
				->update([
					'approved' => 0,
					'approved_user_id' => 0
				]);

			$this->fireEvent('bct.projects.visit.cache.clear.all');
		}
	}

}
