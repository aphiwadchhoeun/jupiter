<?php namespace Bct\Projects\Repositories\Contractor;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\ContractorPhone;
use Validator;

class ContractorPhoneRepository implements ContractorPhoneRepositoryInterface
{

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\DataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent Contractor Phone model.
	 *
	 * @var string
	 */
	protected $model;

	protected $contractors;


	/**
	 * Constructor
	 *
	 * @param Container $app
	 * @param ContractorRepositoryInterface $contractors
	 * @param UserRepositoryInterface $users
	 */
	public function __construct(
		Container $app,
		ContractorRepositoryInterface $contractors
	)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contractors.phone.handler.data'];

		$this->setValidator($app['bct.projects.contractors.phone.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ContractorPhone']));

		$this->contractors = $contractors;
	}


	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this->createModel();
	}


	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.phone.all', function () {
			return $this->createModel()->get();
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.phone.' . $id, function () use ($id) {
			return $this->createModel()->find($id);
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create', [
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update', [
            $id,
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new phone
		$contractor_phone = $this->createModel();


		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Save the Phone
			if($data['type']=='Primary'){
				if ($contractor = Contractor::find($data['z_contractors_id'])) {
                    $contractor->phone = array_get($data, 'phone');
                    $contractor->save();
                    $this->contractors->flushCacheContractor($contractor->id);

                    if ($contractor_phone->type !== 'Primary') {

                        $primary_phone = $contractor->contractors_phone()->where('type', 'Primary')->first();
                        if ($primary_phone !== null) {
                            $primary_phone->type = 'Previous';
                            $primary_phone->save();
                        }
                    }
                }
			}
			$contractor_phone->fill($data)->save();
            $this->flushCache($contractor_phone);

		}

		return [ $messages, $contractor_phone ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the Status object
		$contractor_phone = $this->find($id);

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($id, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Update the Phone
			if($contractor = Contractor::find($data['z_contractors_id'])) {

                if ($data['type'] == 'Primary') {
                    $contractor->phone = array_get($data, 'phone');

                    if ($contractor_phone->type !== 'Primary') {
                        $primary_phone = $contractor->contractors_phone()->where('type', 'Primary')->first();
                        if ($primary_phone !== null) {
                            $primary_phone->type = 'Previous';
                            $primary_phone->save();
                        }
                    }

                }
                $contractor->save();
                $contractor_phone->fill($data)->save();

                $this->contractors->flushCacheContractor($contractor->id);
                $this->flushCache($contractor_phone);
            }

		}

		return [ $messages, $contractor_phone ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the Status exists
		if ($contractor_phone = $this->find($id)) {

			if($contractor_phone->type=='Primary'){
                return false;
			}
			$contractor_phone->delete();

			return true;
		}

		return false;
	}

	public function getAllByContractorId($contractor_id){
		$result = $this->grid()->where('z_contractors_id',$contractor_id)
            ->orderByRaw('type=\'Primary\' desc')
            ->orderBy('updated_at', 'desc')
            ->get();
		return $result;
	}

	protected function flushCache(ContractorPhone $contractor_phone) {
		app('cache')->forget('bct.projects.contractors.phone.all');
		app('cache')->forget('bct.projects.contractors.phone.' . $contractor_phone->id);
	}


}
