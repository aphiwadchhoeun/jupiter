<?php namespace Bct\Projects\Repositories\Contractor;

interface ContractorPhoneRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Bct\Projects\Models\ContractorPhone
	 */
	public function grid();

	/**
	 * Returns all the Member entries.
	 *
	 * @return \Bct\Projects\Models\ContractorPhone
	 */
	public function findAll();

	/**
	 * Returns a ContractorPhone entry by its primary key.
	 *
	 * @param  int $id
	 * @return \Bct\Projects\Models\ContractorPhone
	 */
	public function find($id);


	/**
	 * Determines if the given ContractorPhone is valid for creation.
	 *
	 * @param  array $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given ContractorPhone is valid for update.
	 *
	 * @param  int $id
	 * @param  array $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given ContractorPhone.
	 *
	 * @param  int $id
	 * @param  array $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a ContractorPhone entry with the given data.
	 *
	 * @param  array $data
	 * @return \Bct\Projects\Models\ContractorPhone
	 */
	public function create(array $data);

	/**
	 * Updates the ContractorPhone entry with the given data.
	 *
	 * @param  int $id
	 * @param  array $data
	 * @return \Bct\Projects\Models\ContractorPhone
	 */
	public function update($id, array $data);

	/**
	 * Deletes the ContractorPhone entry.
	 *
	 * @param  int $id
	 * @return bool
	 */
	public function delete($id);

}
