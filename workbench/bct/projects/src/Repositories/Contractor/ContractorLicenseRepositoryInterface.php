<?php namespace Bct\Projects\Repositories\Contractor;

interface ContractorLicenseRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Bct\Projects\Models\ContractorLicense
	 */
	public function grid();

	/**
	 * Returns all the Member entries.
	 *
	 * @return \Bct\Projects\Models\ContractorLicense
	 */
	public function findAll();

	/**
	 * Returns a ContractorLicense entry by its primary key.
	 *
	 * @param  int $id
	 * @return \Bct\Projects\Models\ContractorLicense
	 */
	public function find($id);


	/**
	 * Determines if the given ContractorLicense is valid for creation.
	 *
	 * @param  array $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given ContractorLicense is valid for update.
	 *
	 * @param  int $id
	 * @param  array $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given ContractorLicense.
	 *
	 * @param  int $id
	 * @param  array $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a ContractorLicense entry with the given data.
	 *
	 * @param  array $data
	 * @return \Bct\Projects\Models\ContractorLicense
	 */
	public function create(array $data);

	/**
	 * Updates the ContractorLicense entry with the given data.
	 *
	 * @param  int $id
	 * @param  array $data
	 * @return \Bct\Projects\Models\ContractorLicense
	 */
	public function update($id, array $data);

	/**
	 * Deletes the ContractorLicense entry.
	 *
	 * @param  int $id
	 * @return bool
	 */
	public function delete($id);

}
