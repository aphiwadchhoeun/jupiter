<?php namespace Bct\Projects\Repositories\Contractor;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\ContractorAddress;
use Validator;

class ContractorAddressRepository implements ContractorAddressRepositoryInterface
{

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Contractor\Handlers\DataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent Address Phone model.
	 *
	 * @var string
	 */
	protected $model;

	protected $contractors;


	/**
	 * Constructor
	 *
	 * @param Container $app
	 * @param ContractorRepositoryInterface $contractors
	 */
	public function __construct(
		Container $app,
		ContractorRepositoryInterface $contractors
	)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contractors.address.handler.data'];

		$this->setValidator($app['bct.projects.contractors.address.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ContractorAddress']));

		$this->contractors = $contractors;
	}


	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this->createModel();
	}


	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.address.all', function () {
			return $this->createModel()->get();
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.address.' . $id, function () use ($id) {
			return $this->createModel()->find($id);
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new Address
		$contractor_address = $this->createModel();

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Save the Address

			if($data['type']=='Primary'){
				if ($contractor = Contractor::find($data['z_contractors_id'])) {
                    $contractor->address = array_get($data, 'address');
                    $contractor->city    = array_get($data, 'city');
                    $contractor->state   = array_get($data, 'state');
                    $contractor->zipcode = array_get($data, 'zipcode');
                    $contractor->save();
                    $this->contractors->flushCacheContractor($contractor->id);

                    if ($contractor_address->type !== 'Primary') {
                        $primary_address = $contractor->contractors_address()->where('type', 'Primary')->first();
                        if ($primary_address !== null) {
                            $primary_address->type = 'Previous';
                            $primary_address->save();
                        }
                    }
                }

			}
			$contractor_address->fill($data)->save();
            $this->flushCache($contractor_address);
		}

		return [ $messages, $contractor_address ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the Status object
		$contractor_address = $this->find($id);

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($contractor_address, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Update the Address
			if ($contractor = Contractor::find($data['z_contractors_id'])) {
                if ($data['type'] == 'Primary') {

                    $contractor->address = array_get($data, 'address');
                    $contractor->city    = array_get($data, 'city');
                    $contractor->state   = array_get($data, 'state');
                    $contractor->zipcode = array_get($data, 'zipcode');

                    if ($contractor_address->type !== 'Primary') {
                        $primary_address = $contractor->contractors_address()->where('type', 'Primary')->first();
                        if ($primary_address !== null) {
                            $primary_address->type = 'Previous';
                            $primary_address->save();
                        }
                    }

                } else {
                    if ($data['type'] !== 'Primary' && $contractor_address->type == 'Primary') {
                        $contractor->address = '';
                        $contractor->city    = '';
                        $contractor->state   = '';
                        $contractor->zipcode = '';

                    }
                }
                $contractor->save();
                $contractor_address->fill($data)->save();

                $this->contractors->flushCacheContractor($contractor->id);
                $this->flushCache($contractor_address);
            }
		}

		return [ $messages, $contractor_address ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the Status exists
		if ($contractor_address = $this->find($id)) {

			$contractor_id = $contractor_address->z_contractors_id;
			$contractor = $this->contractors->find($contractor_id);

			if($contractor_address->type=='Primary'){
				$other_address = $contractor->contractors_address()->where('id','!=',$id)->first();
				if($other_address!==null){
					$contractor->address = $other_address->address;
					$contractor->city = $other_address->city;
					$contractor->state = $other_address->state;
					$contractor->zipcode = $other_address->zipcode;
					$other_address->type='Primary';
					$other_address->save();

				}
				else{
					$contractor->address = '';
					$contractor->city = '';
					$contractor->state = '';
					$contractor->zipcode = '';
				}
				$contractor->save();
                $this->contractors->flushCacheContractor($contractor->id);

			}
			$contractor_address->delete();
            $this->flushCache($contractor_address);

			return true;
		}

		return false;
	}

	public function getAllByContractorId($contractor_id){
		$result = $this->grid()->where('z_contractors_id',$contractor_id)
            ->orderByRaw('type=\'Primary\' desc')
            ->orderBy('updated_at', 'desc')
            ->get();
		return $result;
	}

	protected function flushCache(ContractorAddress $contractor_address) {
		app('cache')->forget('bct.projects.contractors.address.all');
		app('cache')->forget('bct.projects.contractors.address.' . $contractor_address->id);
	}
}
