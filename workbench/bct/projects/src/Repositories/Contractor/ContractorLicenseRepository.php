<?php namespace Bct\Projects\Repositories\Contractor;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\ContractorLicense;
use Validator;

class ContractorLicenseRepository implements ContractorLicenseRepositoryInterface
{

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\DataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent Status model.
	 *
	 * @var string
	 */
	protected $model;

	protected $contractors;


	/**
	 * Constructor
	 *
	 * @param Container $app
	 * @param ContractorRepositoryInterface $contractors
	 */
	public function __construct(
		Container $app,
		ContractorRepositoryInterface $contractors
	)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contractors.license.handler.data'];

		$this->setValidator($app['bct.projects.contractors.license.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ContractorLicense']));

		$this->contractors = $contractors;
	}


	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this->createModel();
	}


	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.license.all', function () {
			return $this->createModel()->get();
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.license.' . $id, function () use ($id) {
			return $this->createModel()->find($id);
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create', [
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update', [
            $id,
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new license
		$contractor_license = $this->createModel();


		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Save the License
			if($data['type']=='Primary'){
                if ($contractor = Contractor::find($data['z_contractors_id'])) {
                    $contractor->license = array_get($data, 'license');
                    $contractor->save();
                    $this->contractors->flushCacheContractor($contractor->id);

                    if ($contractor_license->type !== 'Primary') {
                        $primary_license = $contractor->contractors_license()->where('type', 'Primary')->first();
                        if ($primary_license !== null) {
                            $primary_license->type = 'Previous';
                            $primary_license->save();
                        }
                    }
                }
			}
			$contractor_license->fill($data)->save();
            $this->flushCache($contractor_license);

		}

		return [ $messages, $contractor_license ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the Status object
		$contractor_license = $this->find($id);

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($id, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Update the License
			if ($contractor = Contractor::find($data['z_contractors_id'])) {

                if ($data['type'] == 'Primary') {
                    $contractor->license = array_get($data, 'license');

                    if ($contractor_license->type !== 'Primary') {

                        $primary_license = $contractor->contractors_license()->where('type', 'Primary')->first();
                        if ($primary_license !== null) {
                            $primary_license->type = 'Previous';
                            $primary_license->save();
                        }
                    }
                }
                $contractor->save();
                $contractor_license->fill($data)->save();

                $this->contractors->flushCacheContractor($contractor->id);
                $this->flushCache($contractor_license);
            }

		}

		return [ $messages, $contractor_license ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the Status exists
		if ($contractor_license = $this->find($id)) {

			if($contractor_license->type=='Primary'){
                return false;
			}
			$contractor_license->delete();

			return true;
		}

		return false;
	}

	public function getAllByContractorId($contractor_id){
		$result = $this->grid()->where('z_contractors_id',$contractor_id)
            ->orderByRaw('type=\'Primary\' desc')
            ->orderBy('updated_at', 'desc')
            ->get();
		return $result;
	}

	protected function flushCache(ContractorLicense $contractor_license) {
		app('cache')->forget('bct.projects.contractors.license.all');
		app('cache')->forget('bct.projects.contractors.license.' . $contractor_license->id);
	}


}
