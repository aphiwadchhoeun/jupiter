<?php namespace Bct\Projects\Repositories\Contractor;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\ContractorEmail;
use Validator;

class ContractorEmailRepository implements ContractorEmailRepositoryInterface
{

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\DataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent Status model.
	 *
	 * @var string
	 */
	protected $model;

	protected $contractors;


	/**
	 * Constructor
	 *
	 * @param Container $app
	 * @param ContractorRepositoryInterface $contractors
	 */
	public function __construct(
		Container $app,
		ContractorRepositoryInterface $contractors
	)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contractors.email.handler.data'];

		$this->setValidator($app['bct.projects.contractors.email.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ContractorEmail']));

		$this->contractors = $contractors;
	}


	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this->createModel();
	}


	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.email.all', function () {
			return $this->createModel()->get();
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.email.' . $id, function () use ($id) {
			return $this->createModel()->find($id);
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
        return $this->validator->on('create', [
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
        return $this->validator->on('update', [
            $id,
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new email
		$contractor_email = $this->createModel();


		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Save the Email
			if($data['type']=='Primary'){
                if ($contractor = Contractor::find(array_get($data, 'z_contractors_id'))) {
                    $contractor->email = array_get($data, 'email');
                    $contractor->save();
                    $this->contractors->flushCacheContractor($contractor->id);

                    if ($contractor_email->type !== 'Primary') {
                        $primary_email = $contractor->contractors_email()->where('type', 'Primary')->first();
                        if ($primary_email !== null) {
                            $primary_email->type = 'Previous';
                            $primary_email->save();
                        }
                    }
                }
			}
			$contractor_email->fill($data)->save();
            $this->flushCache($contractor_email);
		}

		return [ $messages, $contractor_email ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the Status object
		$contractor_email = $this->find($id);

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($id, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Update the Email
            if ($contractor = Contractor::find($data['z_contractors_id'])) {

                if ($data['type'] == 'Primary') {
                    $contractor->email = array_get($data, 'email');
                    $contractor->save();

                    if ($contractor_email->type !== 'Primary') {
                        $primary_email = $contractor->contractors_email()->where('type', 'Primary')->first();
                        if ($primary_email !== null) {
                            $primary_email->type = 'Previous';
                            $primary_email->save();
                        }
                    }
                }
                $contractor_email->fill($data)->save();

                $this->contractors->flushCacheContractor($contractor->id);
                $this->flushCache($contractor_email);
            }

		}

		return [ $messages, $contractor_email ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the Status exists
		if ($contractor_email = $this->find($id)) {

			if($contractor_email->type=='Primary'){
				return false;
			}
			$contractor_email->delete();

			return true;
		}

		return false;
	}

	public function getAllByContractorId($contractor_id){
		$result = $this->grid()->where('z_contractors_id',$contractor_id)
            ->orderByRaw('type=\'Primary\' desc')
            ->orderBy('updated_at', 'desc')
            ->get();
		return $result;
	}

	protected function flushCache(ContractorEmail $contractor_email) {
		app('cache')->forget('bct.projects.contractors.email.all');
		app('cache')->forget('bct.projects.contractors.email.' . $contractor_email->id);
	}


}
