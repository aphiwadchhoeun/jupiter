<?php namespace Bct\Projects\Repositories\Contractor;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\ContractorNotes;
use Validator;

class ContractorNotesRepository implements ContractorNotesRepositoryInterface
{

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\DataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent Status model.
	 *
	 * @var string
	 */
	protected $model;

	protected $contractors;


	/**
	 * Constructor
	 *
	 * @param Container $app
	 * @param ContractorRepositoryInterface $contractors
	 */
	public function __construct(
		Container $app,
		ContractorRepositoryInterface $contractors
	)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contractors.notes.handler.data'];

		$this->setValidator($app['bct.projects.contractors.notes.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ContractorNotes']));

		$this->contractors = $contractors;
	}


	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this->createModel();
	}


	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.notes.all', function () {
			return $this->createModel()->get();
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.notes.' . $id, function () use ($id) {
			return $this->createModel()->find($id);
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new notes
		$contractor_notes = $this->createModel();


		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {


			if ($data['type'] == 'Primary') {
				if ($contractor = Contractor::find($data['z_contractors_id'])) {
                    $contractor->notes = array_get($data, 'notes');
                    $contractor->save();
                    $this->contractors->flushCacheContractor($contractor->id);

                    if ($contractor_notes->type !== 'Primary') {

                        $primary_notes = $contractor->contractors_notes()->where('type', 'Primary')->first();
                        if ($primary_notes !== null) {
                            $primary_notes->type = 'Previous';
                            $primary_notes->save();
                        }
                    }
                }
			}
			$contractor_notes->fill($data)->save();

            $this->flushCache($contractor_notes);

		}

		return [ $messages, $contractor_notes ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the Status object
		$contractor_notes = $this->find($id);



		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($contractor_notes, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Update the Notes
			if($contractor = Contractor::find($data['z_contractors_id'])) {

                if ($data['type'] == 'Primary') {
                    $contractor->notes = $data['notes'];

                    if ($contractor_notes->type !== 'Primary') {
                        $primary_notes = $contractor->contractors_notes()->where('type', 'Primary')->first();
                        if ($primary_notes !== null) {
                            $primary_notes->type = 'Previous';
                            $primary_notes->save();
                        }
                    }
                } else {
                    if ($data['type'] !== 'Primary' && $contractor_notes->type == 'Primary') {
                        $contractor->notes = '';
                    }
                }
                $contractor->save();
                $contractor_notes->fill($data)->save();

                $this->contractors->flushCacheContractor($contractor->id);
                $this->flushCache($contractor_notes);
            }

		}

		return [ $messages, $contractor_notes ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the Status exists
		if ($contractor_notes = $this->find($id)) {

			$contractor_id = $contractor_notes->z_contractors_id;
			$contractor = $this->contractors->find($contractor_id);

			if($contractor_notes->type=='Primary'){
				$other_notes = $contractor->contractors_notes()->where('id','!=',$id)->first();
				if($other_notes!==null){
					$contractor->notes = $other_notes->notes;

					$other_notes->type='Primary';
					$other_notes->save();

				}
				else{
					$contractor->notes = '';

				}
				$contractor->save();
                $this->contractors->flushCacheContractor($contractor->id);

			}
			$contractor_notes->delete();
            $this->flushCache($contractor_notes);

			return true;
		}

		return false;
	}

	public function getAllByContractorId($contractor_id){
		$result = $this->grid()->where('z_contractors_id',$contractor_id)
            ->orderByRaw('type=\'Primary\' desc')
            ->orderBy('updated_at', 'desc')
            ->get();
		return $result;
	}

	protected function flushCache(ContractorNotes $contractor_notes) {
		app('cache')->forget('bct.projects.contractors.notes.all');
		app('cache')->forget('bct.projects.contractors.notes.' . $contractor_notes->id);
	}


}
