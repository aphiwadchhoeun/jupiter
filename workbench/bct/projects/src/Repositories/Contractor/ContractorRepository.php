<?php namespace Bct\Projects\Repositories\Contractor;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Bct\Projects\Models\ContractorAddress;
use Bct\Projects\Models\ContractorCompany;
use Bct\Projects\Models\ContractorEmail;
use Bct\Projects\Models\ContractorLicense;
use Bct\Projects\Models\ContractorNotes;
use Bct\Projects\Models\ContractorPhone;

class ContractorRepository implements ContractorRepositoryInterface {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     * @var \Bct\Projects\Handlers\Contractor\ContractorDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent projects model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app) {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['bct.projects.contractors.handler.data'];

        $this->setValidator($app['bct.projects.contractors.validator']);

        $this->setModel(get_class($app['Bct\Projects\Models\Contractor']));
    }

    /**
     * {@inheritDoc}
     */
    public function grid() {
        return $this->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll() {
        return $this->container['cache']->rememberForever('bct.projects.contractors.all', function () {
            return $this->createModel()->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function find($id) {
        return $this->container['cache']->rememberForever('bct.projects.contractors.' . $id, function () use ($id) {
            return $this->createModel()->find($id);
        });
    }

    public function findWith($id, $with) {
        return $this->createModel()->with($with)->get()->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input) {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($contractor, array $input) {
        return $this->validator->on('update', compact('contractor'))->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input) {
        return !$id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input) {
        // Create a new contractor
        $contractor = $this->createModel();

        // Fire the 'bct.projects.contractors.creating' event
        if ($this->fireEvent('bct.projects.contractors.creating', [$input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Save the contractor
            $contractor->created_by = Sentinel::getUser()->id;
            $contractor->fill($data)->save();

	        if ($data['address'] !== '' && $data['city'] !== '' && $data['state'] !== '' && $data['zipcode'] !== '') {
		        $address_data['address'] = $data['address'];
		        $address_data['city'] = $data['city'];
		        $address_data['state'] = $data['state'];
		        $address_data['zipcode'] = $data['zipcode'];
		        $address_data['type'] = 'Primary';
		        $address_data['z_contractors_id'] = $contractor->id;

		        $contractor_address = new ContractorAddress;
		        $contractor_address->fill($address_data)->save();
	        }
	        if ($data['email'] !== '') {
		        $email_data['email'] = $data['email'];
		        $email_data['type'] = 'Primary';
		        $email_data['z_contractors_id'] = $contractor->id;

		        $contractor_email = new ContractorEmail;
		        $contractor_email->fill($email_data)->save();
	        }
	        if ($data['phone'] !== '') {
		        $phone_data['phone'] = $data['phone'];
		        $phone_data['type'] = 'Primary';
		        $phone_data['z_contractors_id'] = $contractor->id;

		        $contractor_phone = new ContractorPhone;
		        $contractor_phone->fill($phone_data)->save();
	        }
	        if ($data['license'] !== '') {
		        $license_data['license'] = $data['license'];
		        $license_data['type'] = 'Primary';
		        $license_data['z_contractors_id'] = $contractor->id;

		        $contractor_license = new ContractorLicense;
		        $contractor_license->fill($license_data)->save();
	        }
	        if ($data['ein'] !== '') {
		        $license_data['license'] = $data['ein'];
		        $license_data['type'] = 'EIN';
		        $license_data['z_contractors_id'] = $contractor->id;

		        $contractor_license = new ContractorLicense;
		        $contractor_license->fill($license_data)->save();
	        }
	        if ($data['name'] !== '') {
		        $company_data['company'] = $data['name'];
		        $company_data['type'] = 'Primary';
		        $company_data['z_contractors_id'] = $contractor->id;

		        $contractor_company = new ContractorCompany;
		        $contractor_company->fill($company_data)->save();
	        }
	        if ($data['notes'] !== '') {
		        $notes_data['notes'] = $data['notes'];
		        $notes_data['type'] = 'Primary';
		        $notes_data['z_contractors_id'] = $contractor->id;

		        $contractor_notes = new ContractorNotes;
		        $contractor_notes->fill($notes_data)->save();
	        }

            // Fire the 'bct.projects.contractors.created' event
            $this->fireEvent('bct.projects.contractors.created', [$contractor]);
        }

        return [$messages, $contractor];
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input) {
        // Get the contractor object
        $contractor = $this->find($id);

        // Fire the 'bct.projects.contractors.updating' event
        if ($this->fireEvent('bct.projects.contractors.updating', [$contractor, $input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForUpdate($contractor, $data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Update the contractor
            $contractor->fill($data)->save();

            // Fire the 'bct.projects.contractors.updated' event
            $this->fireEvent('bct.projects.contractors.updated', [$contractor]);
        }

        return [$messages, $contractor];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id, $user_id) {
        // Check if the contractor exists
        if ($contractor = $this->find($id)) {
            if (!($contractor->created_by != $user_id && (!Sentinel::inRole('repadmin') && !Sentinel::inRole('admin')))) {
                // Fire the 'bct.projects.contractors.deleted' event
                $this->fireEvent('bct.projects.contractors.deleted', [$contractor]);

                // Delete the contractor entry
                $contractor->delete();

                DB::table('z_project_contractor_join')->where('z_contractors_id', $contractor->id)->delete();

                return true;
            }
        }

        return false;
    }

    public function downloadPdfDetails($contractor_id)
    {
        if ($contractor = $this->find($contractor_id)) {

            // querying related entities
            $projects = $contractor->projects()
                ->groupBy('id')
                ->orderBy('created_at', 'desc')
                ->get();
            $files = $contractor->files;

            $full_name = $contractor->name;
            $title = 'Contractor Details: ' . $contractor->name . ' | ID:' . $contractor_id . ' - ' . $contractor->license;
            $filename = 'Projects-Contractor-Details-' . preg_replace('/[^\w!@]/', '-', $full_name) . '-' . Carbon::now()->format('m.d.Y');
            $footer_title = $full_name . ' / Contractor';
            $pdfView = 'bct/projects::pdf/details/contractor';

	        $addresses = $contractor->contractors_address()->get();
	        $phones = $contractor->contractors_phone()->where('type', '!=', 'Primary')->take(3)->get();
	        $emails = $contractor->contractors_email()->get();
	        $companies = $contractor->contractors_company()->get();
	        $licenses = $contractor->contractors_license()->get();
	        $notes = $contractor->contractors_notes()->get();

            $snappy = app('bct.unionimpactbase.snappy');

            $snappy->setOptions([
                'title'         => $title,
                'footer-center' => $footer_title,
                'filename'      => $filename,
                'view'          => [
                    $pdfView,
                    compact('contractor', 'contractor_id', 'files', 'projects', 'title', 'addresses', 'phones',
                        'emails', 'companies', 'notes', 'licenses')
                ]
            ]);

            return $snappy->export();
        }

        return redirect()->back();

    }

	public function merge($stay_contractor, $merge_contractor){
        try {
            DB::beginTransaction();

            if (Schema::hasTable('z_contractor_address')) {
                DB::update('update z_contractor_address set z_contractors_id = ?,
                    type = concat(type, " Merged-", ?)
                    where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id, $merge_contractor->id]);
            }

            if (Schema::hasTable('z_contractor_company')) {
                DB::update('update z_contractor_company set z_contractors_id = ?,
                    type = concat(type, " Merged-", ?)
                    where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id, $merge_contractor->id]);
            }

            if (Schema::hasTable('z_contractor_phone')) {
                DB::update('update z_contractor_phone set z_contractors_id = ?,
                    type = concat(type, " Merged-", ?)
                    where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id, $merge_contractor->id]);
            }

            if (Schema::hasTable('z_contractor_email')) {
                DB::update('update z_contractor_email set z_contractors_id = ?,
                    type = concat(type, " Merged-", ?)
                    where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id, $merge_contractor->id]);
            }

            if (Schema::hasTable('z_contractor_license')) {
                DB::update('update z_contractor_license set z_contractors_id = ?,
                    type = concat(type, " Merged-", ?)
                    where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id, $merge_contractor->id]);
            }

            if (Schema::hasTable('z_contractor_notes')) {
                DB::update('update z_contractor_notes set z_contractors_id = ?,
                    type = concat(type, " Merged-", ?)
                    where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id, $merge_contractor->id]);
            }

            DB::update('update z_user_profiles set z_contractors_id = ? where z_contractors_id = ?',
                [$stay_contractor->id, $merge_contractor->id]);

            if (Schema::hasTable('z_project_contractor_join')) {
                $stay_entities  = DB::table('z_project_contractor_join')
                    ->select('*')
                    ->where('z_contractors_id', $stay_contractor->id)
                    ->get();
                $merge_entities = DB::table('z_project_contractor_join')
                    ->select('*')
                    ->where('z_contractors_id', $merge_contractor->id)
                    ->get();

                $delete_ids = [];
                //Remove duplicates with same project and scope
                foreach ($stay_entities as $stay_entity) {
                    foreach ($merge_entities as $merge_entity) {
                        if ($stay_entity->z_projects_id == $merge_entity->z_projects_id && $stay_entity->z_scopes_id == $merge_entity->z_scopes_id) {
                            $delete_ids[] = $merge_entity->id;
                        }
                    }
                }

                DB::table('z_project_contractor_join')
                    ->where('z_contractors_id', $merge_contractor->id)
                    ->whereIn('id', $delete_ids)->delete();

                DB::update('update z_project_contractor_join set z_contractors_id = ? where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id]);
            }

            DB::update('update z_project_action set z_contractors_id = ? where z_contractors_id = ?',
                [$stay_contractor->id, $merge_contractor->id]);

            DB::update('update media set z_contractors_id = ? where z_contractors_id = ?',
                [$stay_contractor->id, $merge_contractor->id]);

            //if MRP extension is installed, then change contractor_id from merged to chosen one
            if (Schema::hasTable('z_mrp_requests')) {
                DB::update('update z_mrp_requests set z_contractors_id = ? where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id]);
                DB::update('update z_bidder_mrp_request_join set z_contractors_id = ? where z_contractors_id = ?',
                    [$stay_contractor->id, $merge_contractor->id]);
            }

            $this->fireEvent('bct.projects.contractors.merged', [$stay_contractor]);

            //remove merged contractor
            $this->fireEvent('bct.projects.contractors.deleted', [$merge_contractor]);
            $merge_contractor->delete();

            DB::commit();

            return $stay_contractor;
        } catch (\Exception $e) {
            DB::rollBack();

            return null;
        }

	}

	public function flushCacheContractor($id) {
		$this->container['cache']->forget('bct.projects.contractors.all');
		$this->container['cache']->forget('bct.projects.contractors.' . $id);
	}

}
