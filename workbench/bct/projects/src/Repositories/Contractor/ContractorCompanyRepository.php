<?php namespace Bct\Projects\Repositories\Contractor;

use Bct\Projects\Models\ContractorCompany;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Bct\Projects\Models\Contractor;
use Validator;

class ContractorCompanyRepository implements ContractorCompanyRepositoryInterface
{

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\DataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent Status model.
	 *
	 * @var string
	 */
	protected $model;

	protected $contractors;


	/**
	 * Constructor
	 *
	 * @param Container $app
	 * @param ContractorRepositoryInterface $contractors
	 */
	public function __construct(
		Container $app,
		ContractorRepositoryInterface $contractors
	)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contractors.company.handler.data'];

		$this->setValidator($app['bct.projects.contractors.company.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\ContractorCompany']));

		$this->contractors = $contractors;
	}


	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this->createModel();
	}


	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.company.all', function () {
			return $this->createModel()->get();
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever(
			'bct.projects.contractors.company.' . $id, function () use ($id) {
			return $this->createModel()->find($id);
		}
		);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create', [
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update', [
            $id,
            data_get($input, 'z_contractors_id')
        ])->validate($input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}


	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new company
		$contractor_company = $this->createModel();

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Save the Company
			if( $contractor = Contractor::find($data['z_contractors_id'])) {

                if ($data['type'] == 'Primary') {
                    $contractor->name = array_get($data, 'company');
                    $contractor->save();
                    $this->contractors->flushCacheContractor($contractor->id);

                    if ($contractor_company->type !== 'Primary') {
                        $primary_company = $contractor->contractors_company()->where('type', 'Primary')->first();
                        if ($primary_company !== null) {
                            $primary_company->type = 'Previous';
                            $primary_company->save();
                        }
                    }
                }
                $contractor_company->fill($data)->save();
                $this->flushCache($contractor_company);
            }

		}

		return [ $messages, $contractor_company ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the Status object
		$contractor_company = $this->find($id);

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($id, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty()) {
			// Update the Company
			if ($contractor = Contractor::find($data['z_contractors_id'])) {

                if ($data['type'] == 'Primary') {
                    $contractor->name = array_get($data, 'company');

                    if ($contractor_company->type !== 'Primary') {
                        $primary_company = $contractor->contractors_company()->where('type', 'Primary')->first();
                        if ($primary_company !== null) {
                            $primary_company->type = 'Previous';
                            $primary_company->save();
                        }
                    }
                }
                $contractor->save();
                $contractor_company->fill(array_except($data, ['_token']))->save();

                $this->contractors->flushCacheContractor($contractor->id);
                $this->flushCache($contractor_company);
            }

		}

		return [ $messages, $contractor_company ];
	}


	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the Status exists
		if ($contractor_company = $this->find($id)) {

			if($contractor_company->type=='Primary'){
				return false;
			}
			$contractor_company->delete();

			return true;
		}

		return false;
	}

	public function getAllByContractorId($contractor_id){
		$result = $this->grid()->where('z_contractors_id',$contractor_id)
            ->orderByRaw('type=\'Primary\' desc')
            ->orderBy('updated_at', 'desc')
            ->get();
		return $result;
	}

	protected function flushCache(ContractorCompany $contractor_company) {
		app('cache')->forget('bct.projects.contractors.company.all');
		app('cache')->forget('bct.projects.contractors.company.' . $contractor_company->id);
	}


}
