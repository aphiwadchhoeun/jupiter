<?php namespace Bct\Projects\Repositories\Contact;

use Bct\Projects\Models\Contact;
use Bct\Projects\Models\Views\ContactView;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Carbon\Carbon;


class ContactRepository implements ContactRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 */
	protected $data;

	/**
	 * The Eloquent mrp model.
	 *
	 * @var string
	 */
	protected $model;

	protected $cache;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.contact.handler.data'];

		$this->setValidator($app['bct.projects.contact.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\Contact']));

		$this->cache = $app['cache'];
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return new ContactView();
	}

	public function gridReal()
	{
		return $this->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('bct.projects.contact.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('bct.projects.contact.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAllStewards()
	{
		return $this->container['cache']->rememberForever('bct.projects.contact.stewards', function()
		{
			return $this->createModel()->getStewards()->get();
		});
	}

	protected function flushCache(Contact $contact)
	{
		$this->cache->forget('bct.projects.contact.all');

		$this->cache->forget('bct.projects.contact.stewards');

		$this->cache->forget('bct.projects.contact.'.$contact->id);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	public function validForUpdate($contact, array $data) {
		return $this->validator->on('update', compact('contact'))->validate($data);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new contact
		$contact = $this->createModel();

		// Fire the 'bct.projects.contact.creating' event
		if ($this->fireEvent('bct.projects.contact.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the contact
			$contact->created_by = Sentinel::getUser()->id;
			$contact->fill($data)->save();

			// Fire the 'bct.projects.contact.created' event
			$this->fireEvent('bct.projects.contact.created', [ $contact ]);
		}

		return [ $messages, $contact ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the contact object
		$contact = $this->find($id);

		// Fire the 'bct.projects.contact.updating' event
		if ($this->fireEvent('bct.projects.contact.updating', [ $contact, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($contact, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the contact
			$contact->fill($data)->save();

			// Fire the 'bct.projects.contact.updated' event
			$this->fireEvent('bct.projects.contact.updated', [ $contact ]);
		}

		return [ $messages, $contact ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id, $user_id)
	{
		// Check if the contact exists
		if ($contact = $this->find($id))
		{
            if(!($contact->created_by != $user_id && (!Sentinel::inRole('repadmin') && !Sentinel::inRole('admin') ))) {
                // Fire the 'bct.projects.contact.deleted' event
                $this->fireEvent('bct.projects.contact.deleted', [ $contact ]);

                // Delete the contact entry
                $contact->delete();

                DB::table('z_project_contact_join')->where('z_contact_id', $contact->id)->delete();


                return true;
            }


		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

	public function downloadPdfDetails($contact_id)
	{
		if($contact = $this->find($contact_id)) {

            $take = 16;
            $contact->load(['projects' => function ($query) use ($take) {
                $query
                    ->orderBy('created_at', 'desc')
                    ->with('type', 'status')
                    ->take($take);
            }]);

            $full_name = $contact->full_name;
            $title = 'Contact Details: ' . $contact->full_name . ' | ID:' . $contact->id . ' - ' . $contact->phone;
            $filename = 'Projects-Contact-Details-' . preg_replace('/[^\w!@]/', '-', $full_name) . '-' . Carbon::now()->format('m.d.Y');
            $footer_title = $full_name . ' / Contact';
            $pdfView = 'bct/projects::pdf/details/contact';

            $snappy = app('bct.unionimpactbase.snappy');

            $snappy->setOptions([
                'title'         => $title,
                'footer-center' => $footer_title,
                'filename'      => $filename,
                'view'          => [
                    $pdfView,
                    compact('contact')
                ]
            ]);

            return $snappy->export();
        }

        return redirect()->back();
	}

	public function updateContactByMerge($contact_merge, $contact_delete, $input_data)
	{
		$contact_delete_projects = $contact_delete->projects;

		if (!$contact_delete_projects->isEmpty()) {
			foreach ($contact_delete_projects as $project) {
				$contact_merge->projects()->attach($project->id, ['z_contact_type_id' => $project->pivot->z_contact_type_id]);
			}
		}


		$update_data = [];

        if (intval($input_data['contact-keep-full_name']) !== (int)$contact_merge->id) {
			$update_data['first_name'] = $contact_delete->first_name;
			$update_data['last_name'] = $contact_delete->last_name;
		}

		if (intval($input_data['contact-keep-location']) !== (int)$contact_merge->id) {
			$update_data['address'] = $contact_delete->address;
			$update_data['city'] = $contact_delete->city;
			$update_data['state'] = $contact_delete->state;
			$update_data['zip'] = $contact_delete->zip;
		}

		if (intval($input_data['contact-keep-company']) !== (int)$contact_merge->id) {
			$update_data['company'] = $contact_delete->company;
		}

		if (intval($input_data['contact-keep-title']) !== (int)$contact_merge->id) {
			$update_data['title'] = $contact_delete->title;
		}

		if (intval($input_data['contact-keep-phone']) !== (int)$contact_merge->id) {
			$update_data['phone'] = $contact_delete->phone;
		}

		if (intval($input_data['contact-keep-phone2']) !== (int)$contact_merge->id) {
			$update_data['phone2'] = $contact_delete->phone2;
		}

		if (intval($input_data['contact-keep-fax']) !== (int)$contact_merge->id) {
			$update_data['fax'] = $contact_delete->fax;
		}

		if (intval($input_data['contact-keep-email']) !== (int)$contact_merge->id) {
			$update_data['email'] = $contact_delete->email;
		}

		if (intval($input_data['contact-keep-notes']) !== (int)$contact_merge->id) {
			$update_data['notes'] = $contact_delete->notes;
		}

		if (!empty($update_data)) {
			$contact_merge->fill($update_data)->save();
		}

		$contact_delete->projects()->detach();
		$contact_delete->delete();

		$this->flushCache($contact_merge);

	}

}
