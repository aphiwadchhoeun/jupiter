<?php namespace Bct\Projects\Repositories\Member;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 12:50
 */


interface MemberRepositoryInterface {

    /**
     * Returns a dataset compatible with data grid.
     *
     * @return \Bct\Projects\Models\Contact
     */
    public function grid();

    /**
     * Returns all the mrp entries.
     *
     * @return \Bct\Projects\Models\Contact
     */
    public function findAll();

    /**
     * Returns a mrp entry by its primary key.
     *
     * @param  int  $id
     * @return \Bct\Projects\Models\Contact
     */
    public function find($id);

    /**
     * Determines if the given mrp is valid for creation.
     *
     * @param  array  $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForCreation(array $data);

    /**
     * Determines if the given mrp is valid for update.
     *
     * @param $contact
     * @param  array $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForUpdate($contact, array $data);

    /**
     * Creates or updates the given mrp.
     *
     * @param  int  $id
     * @param  array  $input
     * @return bool|array
     */
    //public function store($id, array $input);

    /**
     * Creates a mrp entry with the given data.
     *
     * @param  array  $data
     * @return \Bct\Projects\Models\Contact
     */
    //public function create(array $data);

    /**
     * Updates the mrp entry with the given data.
     *
     * @param  int  $id
     * @param  array  $data
     * @return \Bct\Projects\Models\Contact
     */
    //public function update($id, array $data);

    /**
     * Deletes the mrp entry.
     *
     * @param  int  $id
     * @param  int  $user_id
     * @return bool
     */
    //public function delete($id, $user_id);

}
