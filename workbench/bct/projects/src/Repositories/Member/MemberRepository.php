<?php namespace Bct\Projects\Repositories\Member;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 12:49
 */


use Bct\Projects\Models\Member;
use Bct\Projects\Repositories\Member\MemberRepositoryInterface;
use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;


class MemberRepository implements MemberRepositoryInterface {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     */
    protected $data;

    /**
     * The Eloquent mrp model.
     *
     * @var string
     */
    protected $model;

    protected $cache;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container  $app
     */
    public function __construct(Container $app)
    {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        //$this->data = $app['bct.projects.member.handler.data'];

        //$this->setValidator($app['bct.projects.member.validator']);

        $this->setModel(get_class($app['Bct\Projects\Models\Member']));

        $this->cache = $app['cache'];
    }

    public function grid()
    {
        return $this->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->container['cache']->rememberForever('bct.projects.member.all', function()
        {
            return $this->createModel()->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->container['cache']->rememberForever('bct.projects.member.'.$id, function() use ($id)
        {
            return $this->createModel()->find($id);
        });
    }

    protected function flushCache(Member $member)
    {
        $this->cache->forget('bct.projects.member.all');

        $this->cache->forget('bct.projects.member.' . $member->id);
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input)
    {
        return $this->validator->on('create')->validate($input);
    }

    public function validForUpdate($member, array $data) {
        return $this->validator->on('update', compact('member'))->validate($data);
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input)
    {
        return ! $id ? $this->create($input) : $this->update($id, $input);
    }

}
