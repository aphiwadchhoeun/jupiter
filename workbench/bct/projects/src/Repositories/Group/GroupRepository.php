<?php namespace Bct\Projects\Repositories\Group;

use Bct\Projects\Models\Group;
use Carbon\Carbon;
use League\Flysystem\Exception;
use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;

class GroupRepository implements GroupRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\Group\GroupDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent mrp model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.group.handler.data'];

		$this->setValidator($app['bct.projects.group.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\Group']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this->createModel()->get();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('bct.projects.group.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->createModel()->find($id);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($group, array $input)
	{
		return $this->validator->on('update', compact('group'))->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new group
		$group = $this->createModel();

		// Fire the 'bct.projects.group.creating' event
		if ($this->fireEvent('bct.projects.group.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the group
			$group->fill($data)->save();

			// Fire the 'bct.projects.group.created' event
			$this->fireEvent('bct.projects.group.created', [ $group ]);
		}

		return [ $messages, $group ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the group object
		$group = $this->find($id);

		// Fire the 'bct.projects.group.updating' event
		if ($this->fireEvent('bct.projects.group.updating', [ $group, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($group, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the group
			$group->fill($data)->save();

			// Fire the 'bct.projects.group.updated' event
			$this->fireEvent('bct.projects.group.updated', [ $group ]);
		}

		return [ $messages, $group ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the group exists
		if ($group = $this->find($id))
		{
			// Fire the 'bct.projects.group.deleted' event
			$this->fireEvent('bct.projects.group.deleted', [ $group ]);

			// Delete the group entry
			$group->delete();

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function attach($id, $user_id)
	{
		try
		{
			// Get the group object
			$group = $this->find($id);
            
            $count = \DB::table('z_user_group_join as middle')
                           ->where('user_id', $user_id)
                           ->where('z_projects_groups_id', $group->id)->count();	
            
            if ($count == 0) {
                $group->users()->attach($user_id);
            }

			// Fire the 'bct.projects.group.updated' event
			$this->fireEvent('bct.projects.group.updated', [ $group ]);

		} catch (Exception $e)
		{
			return false;
		}

		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public function detach($id, $user_id)
	{
		try
		{
			// Get the group object
			$group = $this->find($id);

			$group->users()->detach($user_id);

			// Fire the 'bct.projects.group.updated' event
			$this->fireEvent('bct.projects.group.updated', [ $group ]);

		} catch (Exception $e)
		{
			return false;
		}

		return true;
	}

}
