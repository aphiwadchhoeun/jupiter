<?php namespace Bct\Projects\Repositories\Group;

interface GroupRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Bct\Projects\Models\Group
	 */
	public function grid();

	/**
	 * Returns all the mrp entries.
	 *
	 * @return \Bct\Projects\Models\Group
	 */
	public function findAll();

	/**
	 * Returns a mrp entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Bct\Projects\Models\Group
	 */
	public function find($id);

	/**
	 * Determines if the given mrp is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given mrp is valid for update.
	 *
	 * @param  Group  $group
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($group, array $data);

	/**
	 * Creates or updates the given mrp.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a mrp entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Bct\Projects\Models\Group
	 */
	public function create(array $data);

	/**
	 * Updates the mrp entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Bct\Projects\Models\Group
	 */
	public function update($id, array $data);

	/**
	 * Deletes the mrp entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

	/**
	 * Attach users the mrp entry.
	 *
	 * @param  int  $id
	 * @param  int  $user_id
	 * @return bool
	 */
	public function attach($id, $user_id);

	/**
	 * Attach users the mrp entry.
	 *
	 * @param  int  $id
	 * @param  int  $user_id
	 * @return bool
	 */
	public function detach($id, $user_id);

}
