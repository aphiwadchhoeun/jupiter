<?php namespace Bct\Projects\Repositories\Project;

use Bct\Projects\Models\Classification;
use Bct\Projects\Models\Contact;
use Bct\Projects\Models\ContactType;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\Lists;
use Bct\Projects\Models\Member;
use Bct\Projects\Models\PrevailingWage;
use Bct\Projects\Models\Project;
use Bct\Projects\Models\ReferenceType;
use Bct\Projects\Models\Scope;
use Bct\Projects\Models\Stage;
use Bct\Projects\Models\Status;
use Bct\Projects\Models\Type;
use Bct\Projects\Models\Value;
use Bct\Projects\Models\Views\MapProject;
use Bct\Projects\Models\Views\MarketShare;
use Bct\Projects\Models\Views\ProjectView;
use Bct\Projects\Models\Views\SummaryProject;
use Bct\Projects\Models\Views\TopContractors;
use Bct\Projects\Models\Visibility;
use Bct\Projects\Repositories\BaseRepository;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Platform\Users\Models\User;
use Illuminate\Support\Facades\Schema;

class ProjectRepository extends BaseRepository implements ProjectRepositoryInterface {

    use DispatchesJobs;
    /**
     * The Data handler.
     *
     * @var \Bct\Projects\Handlers\Project\ProjectDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent projects model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app) {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['bct.projects.project.handler.data'];

        $this->setValidator($app['bct.projects.project.validator']);

        $this->setModel(get_class($app['Bct\Projects\Models\Project']));

    }

    /**
     * {@inheritDoc}
     */
    public function grid($without_follow_up_date_null = false) {
        return new ProjectView();
    }

    public function gridStage() {
        return new Stage();
    }

    public function createProjectModel() {
        return $this->createModel();
    }

    public function gridForMap($without_follow_up_date_null = false) {
        if (($lat = input()->get('lat')) && ($lng = input()->get('lng'))) {
            $R = 3959;
            $count_projects = (input('count_projects')) ? input('count_projects') : 250;
            $radius = (input('radius')) ? input('radius') : 0.5;
            $status = (input('status') == 'Active') ? 1 : 3;
            $start_date = (input('start_date') != '') ? Carbon::parse(input('start_date'))->format('Y-m-d') : '';
            $end_date = (input('end_date') != '') ? Carbon::parse(input('end_date'))->format('Y-m-d') : '';
            $tags = (input('tags')) ? input('tags') : '';
            $contractor = (input('contractor')) ? input('contractor') : '';
            $stage = (input('stage')) ? input('stage') : '';
            $region = (input('region')) ? input('region') : '';

            $data = new MapProject();

            if ($without_follow_up_date_null === true) {
                $data = $data->whereNotNull('follow_up_date');
            }

            $minLat = $lat - rad2deg($radius / $R);
            $maxLat = $lat + rad2deg($radius / $R);
            $minLon = $lng - rad2deg($radius / $R / cos(deg2rad($lat)));
            $maxLon = $lng + rad2deg($radius / $R / cos(deg2rad($lat)));

            $data->where('status_id', '=', $status)
                ->whereBetween('lat', [$minLat, $maxLat])
                ->whereBetween('lng', [$maxLon, $minLon]);

            if ($start_date != '' && $end_date != '') {
                $data->whereBetween('follow_up_date', [$start_date, $end_date]);
            }
            if ($contractor != '' && $contractor != "null") {
                $contractor = '%' . $contractor . '%';
                $data->where('contractors', 'LIKE', $contractor);
            }
            if ($stage != '' && $stage != "null") {
                $stage = '%' . $stage . '%';
                $data->where('stages', 'LIKE', $stage);
            }
            if ($region != '' && $region != "null") {
                $data->where('region_abrv', '=', $region);
            }
            if ($tags != '' && $tags != "null") {
                $tags = explode(',', $tags);
                foreach ($tags as $tag) {
                    $tag = '%' . trim($tag) . '%';
                    $data->where('tags', 'LIKE', $tag);
                }
            }

            $data = $data
                ->having('distance', '<', $radius)
                ->orderBy('distance')
                ->take($count_projects)
                ->get(
                    [
                        'id',
                        'name',
                        'city',
                        'region_abrv',
                        'address',
                        'description',
                        'follow_up_date',
                        'contractors',
                        'stages',
                        'tags',
                        'status',
                        'union_value',
                        'lat',
                        'lng',
                        DB::raw('ROUND(DATEDIFF(NOW(), follow_up_date)/7, 0) AS weeks'),
                        DB::raw('( ' . $R . ' * acos(cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians( lat ) ) ) ) AS `distance`')
                    ]);

            return $data;
        } elseif ($hide = input()->get('hide')) {
            return new Collection();
        }
    }

    public function gridWithoutCompleted($list_id) {
        $ids = DB::table('z_project_list_project_join')->where('z_project_list_id', $list_id)->lists('z_projects_id');

        return SummaryProject::where('status', '<>', 'Completed')
            ->whereNotIn('id', $ids);
    }

    /**
     * {@inheritDoc}
     */
    public function findAll() {
        return $this->container['cache']->rememberForever('bct.projects.project.all', function () {
            return $this->createModel();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function find($id) {
        return $this->createModel()->find($id);
    }

    public function findWith($id, $with) {
        return $this->createModel()->with($with)->get()->find($id);
    }

    public function findAllRegionsForProjects() {
        return $this->createProjectModel()
            ->select(
                'region_abrv as name'
            )
            ->groupBy('region_abrv')
            ->get();
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input) {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($project, array $input) {
        //return $this->validator->on('update')->validate($input);
        return $this->validator->on('update', compact('project'))->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input) {
        return !$id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input) {
        // Create a new project
        $project = $this->createModel();

        // Fire the 'bct.projects.project.creating' event
        if ($this->fireEvent('bct.projects.project.creating', [$input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {

            // Save the project
            $project->fill(array_except($data, ['tags', 'followup_reason']))->save();

            if (isset($data['tags'])) {
                $project->tag($data['tags']);
            }

            // Fire the 'bct.projects.project.created' event
            $this->fireEvent('bct.projects.project.created', [$project, $input]);
        }

        return [$messages, $project];
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input) {
        // Get the project object
        $project = $this->find($id);

        // Fire the 'bct.projects.project.updating' event
        if ($this->fireEvent('bct.projects.project.updating', [$project, $input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForUpdate($project, $data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {

            // Update the project
            $project->fill(array_except($data, ['tags']))->save();

            if (isset($data['tags'])) {
                $tags = $project->tags()->get()->toArray();
                if ($tags) {
                    $remove_tags = array();
                    foreach ($tags as $tag) {
                        $remove_tags[] = $tag['name'];
                    }

                    $project->untag($remove_tags);
                }

                $project->tag($data['tags']);
            } else {
                $tags = $project->tags()->get()->toArray();
                if ($tags) {
                    $remove_tags = array();
                    foreach ($tags as $tag) {
                        $remove_tags[] = $tag['name'];
                    }

                    $project->untag($remove_tags);
                }
            }

            $status = Status::find($data['z_project_status_id']);
            if ($status->name == 'Completed') {
                DB::table('z_project_list_project_join')->where('z_projects_id', $id)->delete();
            }

            // Fire the 'bct.projects.project.updated' event
            $this->fireEvent('bct.projects.project.updated', [$project]);
        }

        return [$messages, $project];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id) {
        // Check if the project exists
        if ($project = $this->find($id)) {
            // Fire the 'bct.projects.project.deleted' event
            $this->fireEvent('bct.projects.project.deleted', [$project]);

            // Delete the project entry
            $project->setStatusDisabled();

            return true;
        }

        return false;
    }

    public function storeContacts($id, array $data, $mode) {
        if ($project = $this->find($id)) {
            if ($contact = Contact::find(array_get($data, 'contact_id'))) {
                $contactTypeId = array_get($data, 'contact_type');

                if ($this->checkType($id, array_get($data, 'contact_id'), $contactTypeId, array_get($data, 'pivot_id'))) {
                    if ($mode == 'create') {
                        $project->contacts()->attach($contact,
                            [
                                'z_contact_type_id' => $contactTypeId,
                                'company'           => data_get($contact, 'company', '')
                            ]);

                        return [true, null];
                    } else {
                        DB::table('z_project_contact_join')
                            ->where('id', array_get($data, 'pivot_id'))
                            ->update(
                                [
                                    'z_contact_type_id' => $contactTypeId,
                                    'company'           => data_get($contact, 'company', '')
                                ]);

                        return [true, null];
                    }
                }

                return [false, trans('bct/projects::projects/common.duplicate_contact')];
            }
        }

        return [false, trans('bct/projects::general/message.fail')];
    }

    public function deleteContact($pivot_id) {
        return DB::table('z_project_contact_join')->where(['id' => $pivot_id])->delete();
    }

    public function storeMembers($id, array $data, $mode) {
        if ($project = $this->find($id)) {
            if ($member = Member::find(array_get($data, 'member_id'))) {

                    if ($mode == 'create') {
                        $project->members()->attach($member,
                            [
                                'z_scopes_id'     => input('scope_id'),
                                'number_of_hours' => input('number_hours'),
                                'wage'            => formatMoneyToFloat(input('wage'))
                            ]);

                        return [true, null];
                    } else {
                        DB::table('z_project_member_join')
                            ->where('id', array_get($data, 'pivot_id'))
                            ->update(
                                [
                                    'z_scopes_id'     => input('scope_id'),
                                    'number_of_hours' => input('number_hours'),
                                    'wage'            => formatMoneyToFloat(input('wage'))
                                ]);

                        return [true, null];
                    }

                return [false, trans('bct/projects::projects/common.duplicate_contact')];
            }
        }

        return [false, trans('bct/projects::general/message.fail')];
    }

    public function deleteMember($pivot_id) {
        return DB::table('z_project_member_join')->where(['id' => $pivot_id])->delete();
    }

    public function storeSteward($id, $steward_id) {
        if ($project = $this->find($id)) {
            if ($user = User::find($steward_id)) {
                if ($this->checkSteward($id, $steward_id)) {
                    $project->users()->attach($user);

                    return [true, null];
                }

                return [false, trans('bct/projects::projects/common.duplicate_steward')];
            }
        }

        return [false, trans('bct/projects::general/message.fail')];
    }

    protected function checkSteward($project_id, $steward_id) {
        $result = DB::table('z_project_user_join')->where([
            'z_projects_id' => $project_id,
            'user_id'       => $steward_id,
        ])->first();

        if ($result) return false;

        return true;
    }

    public function deleteSteward($pivot_id) {
        return DB::table('z_project_user_join')->where(['id' => $pivot_id])->delete();
    }

    public function storeContractor($id, array $data, $mode) {
        // Get the project object
        if ($project = $this->find($id)) {
            if ($contractor = Contractor::find(array_get($data, 'contractor_id'))) {
                $scopeIds = array_get($data, 'scope_id', 0);
                if (!is_array($scopeIds)) {
                    $scopeIds = array(0 => $scopeIds);
                }
                if (count($scopeIds) !== 0) {
                    foreach ($scopeIds as $scopeId) {
                        $isAgreement = array_get($data, 'is_agreement', 0);
                        $isUnion = $contractor->is_union;

                        if ($contractor->is_union) {
                            $isUnion = !array_get($data, 'is_non_union', 0);
                        }

                        if ($this->checkScope($id, array_get($data, 'contractor_id'), $scopeId, array_get($data, 'pivot_id'))) {
                            if ($mode == 'create') {
                                $project->contractors()->attach($contractor, [
                                    'z_scopes_id'  => $scopeId,
                                    'is_agreement' => $isAgreement,
                                    'is_union'     => $isUnion,
                                ]);
                            } else {
                                DB::table('z_project_contractor_join')
                                    ->where('id', array_get($data, 'pivot_id'))
                                    ->update([
                                        'z_scopes_id'  => $scopeId,
                                        'is_agreement' => $isAgreement,
                                        'is_union'     => $isUnion,
                                    ]);
                            }

                            $this->updateUnionValue($project);
                        }
                    }

                    return [true, null];
                }

                return [false, trans('bct/projects::projects/common.duplicate_contractor')];
            }
        }

        return [false, trans('bct/projects::general/message.fail')];
    }

    public function storeContractorNa($id, array $data, $mode) {
        // Get the project object
        if ($project = $this->find($id)) {
            $scopeIds = array_get($data, 'scope_id', 0);
            if (!is_array($scopeIds)) {
                $scopeIds = array(0 => $scopeIds);
            }
            if (count($scopeIds) !== 0) {
                foreach ($scopeIds as $scopeId) {

                    if ($this->checkScope($id, array_get($data, 'contractor_id'), $scopeId, array_get($data, 'pivot_id'))) {
                        if ($mode == 'create') {
                            $project->contractors()->attach(0, [
                                'z_scopes_id'  => $scopeId,
                                'is_agreement' => 0,
                                'is_union'     => 0,
                            ]);
                        } else {
                            DB::table('z_project_contractor_join')
                                ->where('id', array_get($data, 'pivot_id'))
                                ->update([
                                    'z_scopes_id'  => $scopeId,
                                    'is_agreement' => 0,
                                    'is_union'     => 0,
                                ]);
                        }

                        $this->updateUnionValue($project);
                    }
                }

                return [true, null];
            }

            return [false, trans('bct/projects::projects/common.duplicate_contractor')];
        }

        return [false, trans('bct/projects::general/message.fail')];
    }

    public function deleteContractor($pivot_id) {
        $project_id = DB::table('z_project_contractor_join')->where('id', $pivot_id)->first()->z_projects_id;

        try {
            DB::table('z_project_contractor_join')->where('id', $pivot_id)->delete();

            if ($project = $this->find($project_id)) {
                $this->updateUnionValue($project);
            }

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Recalculate uion percentage value and update it
     *
     * @param $project
     */
    protected function updateUnionValue($project) {
        // calculate union percentage value and store it in project
        $scope_ids = DB::table('z_project_contractor_join')
            ->where('z_project_contractor_join.z_projects_id', $project->id)
            ->lists('z_scopes_id');

        $total_value = 0;
        foreach($scope_ids as $sid) {
            $total_value += intval(DB::table('z_scopes_classifications_join')
                ->where('z_scopes_id', $sid)
                ->where('z_classifications_id', $project->z_project_classification_id)
                ->first([DB::raw('sum(value) as value')])->value);
        }

        $total_union_value = intval(DB::table('z_scopes_classifications_join')
            ->join('z_project_contractor_join', 'z_project_contractor_join.z_scopes_id', '=', 'z_scopes_classifications_join.z_scopes_id')
            ->where(function ($query) {
                $query->where('z_project_contractor_join.is_union', 1)
                    ->orWhere('z_project_contractor_join.is_agreement', 1);
            })
            ->whereIn('z_scopes_classifications_join.z_scopes_id', $scope_ids)
            ->where('z_classifications_id', $project->z_project_classification_id)
            ->where('z_project_contractor_join.z_projects_id', $project->id)
            ->first([DB::raw('sum(z_scopes_classifications_join.value) as value')])->value);

        $project->update([
            'union_value' => ($total_value > 0) ? ($total_union_value / $total_value) : 0
        ]);
    }

    public function storeLists($id, $pivot_id, array $data) {
        // Get the project object
        $project = $this->find($id);
        $list = Lists::find(array_get($data, 'list_id'));


        $raw = DB::table('z_project_list_project_join as middle')->where('z_project_list_id', $list->id)->where('z_projects_id', $project->id);

        if ($list) {
            if ($raw->count() == 0) {
                $project->lists()->attach($list, [
                    'created_by' => Sentinel::getUser()->id,
                    'is_active'  => true,
                    'created_at' => Carbon::now()
                ]);
            } else {
                $raw->update([
                    'is_active'  => true,
                    'updated_at' => Carbon::now()
                ]);
            }

        }

    }

    public function deleteLists($pivot_id) {
        return DB::table('z_project_list_project_join')->where('id', $pivot_id)->delete();
    }

    public function checkType($project_id, $contact_id, $contact_type_id, $pivot_id) {
        $raw = DB::table('z_project_contact_join as middle')->where([
            'z_contact_id'      => $contact_id,
            'z_projects_id'     => $project_id,
            'z_contact_type_id' => $contact_type_id
        ]);

        if ($pivot_id) {
            $raw->where('middle.id', '<>', $pivot_id);
        }

        $result = $raw->first();

        if ($result) return false;

        return true;
    }

    /**
     * Check if scope for this contractor is already exists
     *
     * @param $project_id
     * @param $contractor_id
     * @param $scope_id
     * @param $pivot_id
     * @return bool
     */
    public function checkScope($project_id, $contractor_id, $scope_id, $pivot_id) {
        $raw = DB::table('z_project_contractor_join as middle')->where([
            'z_contractors_id' => $contractor_id,
            'z_projects_id'    => $project_id,
            'z_scopes_id'      => $scope_id,
        ]);

        if ($pivot_id) {
            $raw->where('id', '<>', $pivot_id);
        }

        $result = $raw->first();

        if ($result) return false;

        return true;
    }

    public function gridContacts($id) {
        // Get the project object
        $project = $this->find($id);

        return $project->contacts()->select([
            'z_contact.first_name as first_name',
            'z_contact.last_name as last_name',
            DB::raw('CONCAT_WS(\' \', z_contact.first_name, z_contact.last_name) as full_name'),
            'z_contact.id as id',
            'z_contact.email as email',
            'z_contact.phone as phone',
            'z_contact.company as company',
            'type.name as type',
            'type.id as type_id',
        ]);
    }

    public function gridStewards($id) {
        // Get the project object
        $project = $this->find($id);

        return $project->users()
            ->join('z_user_profiles as up', 'users.z_user_profiles_id', '=', 'up.id')
            ->select([
                DB::raw('CONCAT(users.first_name, " ", users.last_name) as full_name'),
                'users.id as id',
                'users.email as email',
                'up.business_phone as phone',
                'up.business_local as local',
                'z_project_user_join.id as pivot_id'
            ])->get();
    }

    public function statuses() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.status.all', function () {
                return Status::orderBy('name')->get();
            });
    }

    public function classifications() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.classification.all', function () {
                return Classification::orderBy('name')->get();
            });
    }

    public function stages() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.stage.all', function () {
                return Stage::orderBy('order')->get();
            });
    }

    public function values() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.value.all', function () {
                return Value::all();
            });
    }

    public function types() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.projectType.all', function () {
                return Type::orderBy('type')->get();
            });
    }

    public function prevailingWages() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.prevailingWage.all', function () {
                return PrevailingWage::all();
            });
    }

    public function referenceTypes() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.referenceType.all', function () {
                return ReferenceType::all();
            });
    }

    public function visibilities() {
        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.visibility.all', function () {
                return Visibility::all();
            });
    }

    public function contactTypes($query = null) {
        if ($query) {
            return ContactType::where('name', 'like', '%' . $query . '%')->get();
        }

        return ContactType::whereNull('deleted_at')->get();
    }

    public function scopes($project = null) {
        if ($project && $project->classification) {
            $clName = $project->classification->name;

            return $this->container['cache']
                ->rememberForever('bct.pprofiles.projects.scopes.' . $clName, function () use ($clName) {
                    return Scope::ofClassification($clName)
                        ->orderBy('order', 'asc')
                        ->get()
                        ->map(function ($item) use ($clName) {
                            $item->value = $item->classifications()
                                ->where('name', $clName)->first()->pivot->value;
                            return $item;
                        })
                        ->reject(function ($item) {
                            return $item->value === 0;
                        });
                });
        }

        return $this->container['cache']
            ->rememberForever('bct.pprofiles.projects.scopes.default', function () {
                return Scope::defaultScopes()
                    ->orderBy('id')
                    ->get();
            });
    }

    public function shareByArea($scope, $region) {
        $union = $pa = $nonUnion = 0;
        $marketshare = new MarketShare();

        if ($region == 'OUT_OF_REGION') {
            $marketshare = $marketshare->orWhere(function ($query) {
                $query->where(function ($query) {
                    $query->whereNull('region')
                        ->orWhereIn('region', ['n/a', '']);
                });
            });
        } else if (!empty($region)) {
            $marketshare = $marketshare->where('region', $region);
        }
        if (!empty($scope)) {
            $marketshare = $marketshare->where('scope', '=', $scope);
        }

        $unionQuery = clone $marketshare;

        $paQuery = clone $marketshare;

        $nonUnionQuery = clone $marketshare;

        $union = $unionQuery
            ->where('is_union', '=', 1)
            ->get([DB::raw('SUM(value) as value')])->first()->value;
        $pa = $paQuery
            ->where('is_agreement', '=', 1)
            ->get([DB::raw('SUM(value) as value')])->first()->value;
        $nonUnion = $nonUnionQuery
            ->where('is_union', '=', 0)
            ->where('is_agreement', '=', 0)
            ->get([DB::raw('SUM(value) as value')])->first()->value;

        return [
            ['status' => 'Union', 'count' => $union],
            ['status' => 'PA', 'count' => $pa],
            ['status' => 'Non-Union', 'count' => $nonUnion],
        ];
    }

    public function totals($year) {
        $results = [];

        if (!$year) $year = Carbon::now()->format('Y');

        foreach (range(1, 12) as $month) {
            $results[] = [
                'status' => Carbon::create($year, $month)->format('M'),
                'count'  => $this->createModel()
	                ->where('z_project_status_id','!=',2)
                    ->whereNull('deleted_at')
                    ->whereBetween('created_at', [
                        Carbon::create($year, $month, 1)->format('Y-m-d'),
                        Carbon::create($year, $month, 31)->format('Y-m-d')
                    ])
                    ->count(),
            ];
        }

        return $results;
    }

    public function topContractors($scope, $region, $isUnion = false) {
        $results = [];

        $query = new TopContractors();

        if ($region == 'OUT_OF_REGION') {
            $query = $query->where(function ($query) {
                $query->whereNull('region_abrv')
                    ->orWhereIn('region_abrv', ['n/a', '']);
            });
        } else if (!empty($region)) {
            $query = $query->where('region_abrv', $region);
        }

        if ($scope) {
            $query = $query->where('scope', $scope);
        }

        $rows = $query->where('project_status_id', 1)
            ->where('is_union', $isUnion)
            ->orderBy('value', 'desc')
            ->take(10)
            ->get(['id', 'contractor_name', 'value']);

        foreach ($rows as $item) {
            $results[] = [
                'id'    => $item->id,
                'name'  => $item->contractor_name,
                'value' => $item->value,
            ];
        }

        return $results;
    }

    public function followUp($region) {
        $results = [];

        $projects = $this->createModel()
            ->where('z_project_status_id', 1)
            ->orderBy('follow_up_date', 'asc')
            ->take(10);

        if ($region == 'OUT_OF_REGION') {
            $projects->where(function ($query) {
                $query->whereNull('region_abrv')
                    ->orWhereIn('region_abrv', ['n/a', '']);
            });
        } else if (!empty($region)) {
            $projects->where('region_abrv', $region);
        }

        $projects = $projects->get();

        foreach ($projects as $project) {
            $results[] = [
                'id'             => $project->id,
                'name'           => $project->name,
                'follow_up_date' => $project->follow_up_date,
            ];
        }

        return $results;
    }

    /**
     * check if projects associated with list
     *
     * @param $listId
     * @param $projectIds
     * @return mixed
     */
    public function checkIfExistsInList($listId, $projectIds) {
        foreach ($projectIds as $projectId) {
            $exists = DB::table('z_project_list_project_join as middle')
                ->where('z_project_list_id', $listId)
                ->where('z_projects_id', $projectId)->count();

            if ($exists > 0) {
                return false;
            }
        }

        return true;
    }

    public function downloadPdfDetails($project_id) {
        if($project = $this->find($project_id)) {

            $project->load([
                'visits'   => function ($query) {
                    $query->orderBy('created_at', 'desc');
                },
                'contacts' => function ($query) {
                    $query->orderBy('name', 'asc');
                },
                'actions'  => function ($query) {
                    $query->orderBy('created_at', 'desc');
                },
                'files'    => function ($query) {
                    $query->orderBy('created_at', 'desc');
                },
                'lists'    => function ($query) {
                    $query->orderBy('join_created_at', 'desc');
                },
            ]);

            $visits = $project->visits;
            $contractors = $project->contractorsWithNa;
            $contacts = $project->contacts;
            $actions = $project->actions;
            $files = $project->files;
            $lists = $project->lists;
            $tag_groups = $project->tags()->groupby('group')->get(['group'])->implode('group', ', ');

            $full_name = $project->name;
            $title = 'Project Details: ' . $full_name . " ID: " . $project_id;
            $filename = 'Projects-Project-Details-' . preg_replace('/[^\w!@]/', '-', $full_name) . '-' . Carbon::now()->format('m.d.Y');
            $footer_title = $full_name . ' / Project';
            $pdfView = 'bct/projects::pdf/details/project';

            $snappy = app('bct.unionimpactbase.snappy');

            $snappy->setOptions([
                'title'         => $title,
                'footer-center' => $footer_title,
                'filename'      => $filename,
                'view'          => [
                    $pdfView,
                    compact('project', 'visits', 'contractors', 'contacts', 'actions', 'files', 'lists', 'tag_groups')
                ]
            ]);

            return $snappy->export();
        }

        return redirect()->back();
    }

    public function tags() {
        return Project::allTags()->get();
    }

    public function gridProjects() {
        $model = $this->createProjectModel();

        return $model
            ->leftJoin('z_project_status', 'z_project_status.id', '=', $model->getTable() . '.z_project_status_id')
            ->leftJoin('tagged', function ($leftJoin) {

                $taggable_type = 'Bct\Projects\Models\Project';
                $leftJoin->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->where('tagged.taggable_type', '=', $taggable_type);
            })
            ->leftJoin('tags', 'tagged.tag_id', '=', 'tags.id')
            ->select(
                $model->getTable() . '.id'
            )
            ->groupBy($model->getTable() . '.id')
            ->lists('id');
    }

    public function getExportReport1Project($currentUser) {
        $regions = input('regions');
        $scopes = input('scopes', null);
        $include_non_union_gc = input('inc', false) == 'true';
        $projects_data = [];
        $projects_count = 0;

        if ($regions) {

            if (!empty($scopes)) {
                $scopes = array_map(function ($v) {
                    return '\'' . $v . '\'';
                }, $scopes);
                $scopes = implode(',', $scopes);
            } else {
                $scopes = null;
            }

            sort($regions);
            foreach ($regions as $region) {
                $projects_data[$region] = DB::select('call pp_report_union_jobs(?, ?, ?)',
                    array($include_non_union_gc, $scopes, $region));

                $projects_count += count($projects_data[$region]);
            }

            if ($projects_count > 0) {

                if (input('download') === 'pdf') {
                    $filename = input('filename', 'Projects Report');

                    $queueOptions = [
                        'pdf_view'      => 'bct/projects::pdf/reports/report1',
                        'export_source' => 'Projects / Reports',
                        'title'         => $filename,
                        'orientation'   => 'landscape',
                        'user_id'       => ($currentUser) ? $currentUser->id : 0,
                        'margin-bottom' => '30',
                        'footer-html'   => view('bct/projects::pdf/partials/footer_report1')->render()
                    ];

                    // dispatch pdf render job to a queue worker
                    $dispatcher = app('bct.unionimpactbase.export.queue.dispatcher');
                    $dispatcher->dispatchJob($filename, 'pdf-details', $projects_data, $queueOptions);

                    return response('completed', 200);
                } else {
                    return response()->json(['success' => true, 'results' => json_encode($projects_data)], 200);
                }


            } else {
                return response()->json(['success' => false, 'message' => 'Sorry, no project for selected regions'], 422);
            }

        }

        return response()->json(['success' => false, 'message' => 'Sorry, but you must select some region(s)'], 422);
    }


    public function getReportActiveProjectsWithStewards($currentUser) {
        $regions = input('regions');
        $projects_data = [];
        $projects_count = 0;

        if ($regions) {

            foreach ($regions as $region) {
                $projects_data[$region] = $this
                    ->createProjectModel()
                    ->fromReportActiveProjectWithStewardsView()
                    ->withTrashed()
                    ->where('region_abrv', $region)
                    ->get();

                $projects_count += $projects_data[$region]->count();
            }

            if ($projects_count > 0) {

                if (input('download') === 'pdf') {
                    $filename = input('filename', 'Projects Report');

                    $queueOptions = [
                        'pdf_view'      => 'bct/projects::pdf/reports/report_active_projects_with_stewards',
                        'export_source' => 'Projects / Reports',
                        'title'         => $filename,
                        'orientation'   => 'landscape',
                        'user_id'       => ($currentUser) ? $currentUser->id : 0,
                        'footer-html'   => view('bct/projects::pdf/partials/footer_actiive_projects_with_stewards')->render()
                    ];

                    // dispatch pdf render job to a queue worker
                    $dispatcher = app('bct.unionimpactbase.export.queue.dispatcher');
                    $dispatcher->dispatchJob($filename, 'pdf-details', $projects_data, $queueOptions);

                    return response('completed', 200);
                } else {
                    return response()->json(['success' => true, 'results' => json_encode($projects_data)], 200);
                }


            } else {
                return response()->json(['success' => false, 'message' => 'Sorry, no project for selected regions'], 422);
            }

        }

        return response()->json(['success' => false, 'message' => 'Sorry, but you must select some region(s)'], 422);
    }

    /**
     * Process report Active Service Jobs List
     *
     * @param $currentUser
     * @return Response
     */
    public function getExportActiveServiceJobs($currentUser) {
        $regions = input('regions', []);

        if ($regions) {

            $projects_data = $this
                ->createProjectModel()
                ->fromReportActiveServiceJobsView()
                ->whereIn('region', $regions)
                ->orderBy('region')
                ->orderBy('city')
                ->withTrashed()
                ->get();

            if ($projects_data->count() > 0) {

                if (input('download') === 'pdf') {
                    $filename = input('filename', 'Projects Report');

                    $queueOptions = [
                        'pdf_view'      => 'bct/projects::pdf/reports/report_active_service_jobs',
                        'export_source' => 'Projects / Reports',
                        'title'         => $filename,
                        'orientation'   => 'landscape',
                        'user_id'       => ($currentUser) ? $currentUser->id : 0,
                        'footer-html'   => view('bct/projects::pdf/partials/footer_active_service_job')->render()
                    ];

                    // dispatch pdf render job to a queue worker
                    $dispatcher = app('bct.unionimpactbase.export.queue.dispatcher');
                    $dispatcher->dispatchJob($filename, 'pdf-details', $projects_data, $queueOptions);

                    return response('completed', 200);
                } else {
                    return response()->json(['success' => true, 'results' => json_encode($projects_data)], 200);
                }


            } else {
                return response()->json(['success' => false, 'message' => 'Sorry, no project for selected regions'], 422);
            }

        }

        return response()->json(['success' => false, 'message' => 'Sorry, but you must select some region(s)'], 422);
    }


    public function getFollowUpDate($project_id) {
        $follow_up_date = $this->find($project_id)->follow_up_date;
        $follow_up_date = date_correct_details_projects($follow_up_date);

        return response()->json(['success' => true, 'html' => $follow_up_date]);

    }

    public function getStatus($project_id) {
        $project = $this->find($project_id);

        $html = '<span class="label label-' . $project->status_color . '">' . $project->status->name . '</span>';

        return response()->json(['success' => true, 'html' => $html]);
    }

	public function merge($stay_project,$merge_project){

        try {
            DB::beginTransaction();

            if (Schema::hasTable('z_project_member_join')) {
                $this->mergeEntity('z_project_member_join', $stay_project, $merge_project, true,
                    ['z_members_id', 'z_scopes_id']);
            }
            if (Schema::hasTable('z_project_contractor_join')) {
                $this->mergeEntity('z_project_contractor_join', $stay_project, $merge_project, true,
                    ['z_contractors_id', 'z_scopes_id']);
            }

            if (Schema::hasTable('z_project_contact_join')) {
                $this->mergeEntity('z_project_contact_join', $stay_project, $merge_project, true,
                    ['z_contact_id', 'z_contact_type_id']);
            }

            if (Schema::hasTable('z_project_action')) {
                $this->mergeEntity('z_project_action', $stay_project, $merge_project, false, []);
            }

            if (Schema::hasTable('z_project_visit')) {
                $this->mergeEntity('z_project_visit', $stay_project, $merge_project, true, ['details']);
            }

            if (Schema::hasTable('z_project_list_project_join')) {
                $this->mergeEntity('z_project_list_project_join', $stay_project, $merge_project, true,
                    ['z_project_list_id']);
            }

            if (Schema::hasTable('z_jobs')) {
                $this->mergeEntity('z_jobs', $stay_project, $merge_project, false, []);
            }

            //if MRP extension is installed, then change project_id from merged to chosen one
            if (Schema::hasTable('z_mrp_requests')) {
                DB::table('z_mrp_requests')->where('z_projects_id',
                    $merge_project->id)->update(['z_projects_id' => $stay_project->id]);
                DB::table('z_projects')->where('id', $merge_project->id)->update(['z_project_status_id' => 2]);
            }

            DB::update('update media set z_projects_id = ? where z_projects_id = ?',
                [$stay_project->id, $merge_project->id]);

            $this->fireEvent('bct.projects.project.merged', [$stay_project]);

            DB::commit();

            $this->updateProjectStage($stay_project);

            return $stay_project;

        } catch (\Exception $e) {
            DB::rollBack();

            return null;
        }

	}

    private function updateProjectStage($project)
    {
        $last_visit = $project->visits()->orderBy('created_at', 'desc')->first();
        if ($last_visit) {
            $project->update([
                'z_project_stage_id' => data_get($last_visit, 'stages.0.id', 0)
            ]);
        }
    }

    private function mergeEntity($table, $stay_project, $merge_project, $remove_duplicated, $dup_cols)
    {

        if ($remove_duplicated) {

            $stay_entities  = DB::table($table)->select('*')->where('z_projects_id', $stay_project->id)->get();
            $merge_entities = DB::table($table)->select('*')->where('z_projects_id', $merge_project->id)->get();

            $delete_ids = [];
            foreach ($stay_entities as $stay_entity) {
                foreach ($merge_entities as $merge_entity) {
                    $flag = true;
                    foreach ($dup_cols as $col) {
                        if (data_get($stay_entity, $col) != data_get($merge_entity, $col)) {
                            $flag = false;
                        }
                    }

                    if ($flag) {
                        $delete_ids[] = $merge_entity->id;
                    }
                }
            }

            DB::table($table)->where('z_projects_id', $merge_project->id)->whereIn('id', $delete_ids)->delete();
        }

        DB::table($table)->where('z_projects_id', $merge_project->id)->update(['z_projects_id' => $stay_project->id]);

    }

    public function mergeConfirm($stay_project_id,$merge_project_id)
    {

        $stay_project = $this->find($stay_project_id);
        $merge_project = $this->find($merge_project_id);

        // VISITS
        $merge_visits = $merge_project->visits;
        $stay_visits = $stay_project->visits;
        $temp_visits = $merge_visits->reject(function ($item) {
            return $item->details == 'Project was created by Market Recovery!';
        });
        $stay_visits = $stay_visits->merge($temp_visits);

        // CONTRACTORS
        $merge_contractors = $merge_project->contractors;
        $stay_contractors = $stay_project->contractors;
        $temp_contractors = ($stay_contractors->count() > 0) ? $stay_contractors : $merge_contractors;
        foreach($merge_contractors as $merge_contractor) {
            foreach ($stay_contractors as $stay_contractor) {
                if (($merge_contractor->id != $stay_contractor->id) ||
                    ($merge_contractor->pivot->z_scopes_id != $stay_contractor->pivot->z_scopes_id)) {
                    $temp_contractors->push($merge_contractor);
                }
            }
        }
        $stay_contractors = $stay_contractors->merge($temp_contractors);

        // CONTACTS
        $merge_contacts = $merge_project->contacts;
        $stay_contacts = $stay_project->contacts;
        $temp_contacts = ($stay_contacts->count() > 0) ? $stay_contacts : $merge_contacts;
        foreach($merge_contacts as $merge_contact) {
            foreach($stay_contacts as $stay_contact) {
                if(($merge_contact->id != $stay_contact->id) ||
                    ($merge_contact->pivot->z_contact_type_id != $stay_contact->pivot->z_contact_type_id)) {
                    $temp_contacts->push($merge_contact);
                }
            }
        }
        $stay_contacts = $stay_contacts->merge($temp_contacts);

        // ACTIONS
        $actions = array();
        $added_actions = array();
        if(Schema::hasTable('z_project_action')){
            $actions = DB::table('z_project_action')
                ->select(
                    'z_project_action.details as details',
                    'z_project_action.action_type as action_type',
                    'z_project_action.created_at as created_at',
                    'z_contractors.name as contractor_name'
                )
                ->where('z_projects_id',$merge_project_id)
                ->leftJoin('z_contractors','z_contractors.id','=','z_project_action.z_contractors_id')->get();

            $added_actions = DB::table('z_project_action')
                ->select(
                    'z_project_action.details as details',
                    'z_project_action.action_type as action_type',
                    'z_project_action.created_at as created_at',
                    'z_contractors.name as contractor_name'
                )
                ->where('z_projects_id',$stay_project_id)
                ->leftJoin('z_contractors','z_contractors.id','=','z_project_action.z_contractors_id')->get();

        }

        $merge_members = $merge_project->members;
        $stay_members = $stay_project->members;
        $temp_members = ($stay_members->count() > 0) ? $stay_members : $merge_members;
        foreach($merge_members as $merge_member) {
            foreach($stay_members as $stay_member) {
                if(($merge_member->id != $stay_member->id) ||
                    ($merge_member->pivot->z_scopes_id != $stay_member->pivot->z_scopes_id)) {
                    $temp_contacts->push($merge_member);
                }
            }
        }
        $stay_members = $stay_members->merge($temp_members);

        $lists = array();
        $added_lists = array();
        if(Schema::hasTable('z_project_list_project_join')){
            $lists = collect(
                DB::table('z_project_list_project_join')
                    ->select(
                        'z_project_list.name as name',
                        'z_project_list.category as category',
                        'z_projects_group.name as group_name',
                        'z_project_list.due_date as due_date',
                        'z_project_list.is_active as is_active'
                    )
                    ->where('z_projects_id',$merge_project_id)
                    ->leftJoin('z_project_list','z_project_list.id','=','z_project_list_project_join.z_project_list_id')
                    ->leftJoin('z_projects_group','z_projects_group.id','=','z_project_list.z_projects_group_id')
                    ->get()
            );
            $lists_ids=[];
            if(count($lists)>0){
                $lists_ids = (array)$lists->pluck('z_project_list_id');
            }

            $added_lists =
                DB::table('z_project_list_project_join')
                    ->select(
                        'z_project_list.name as name',
                        'z_project_list.category as category',
                        'z_projects_group.name as group_name',
                        'z_project_list.due_date as due_date',
                        'z_project_list.is_active as is_active'
                    )
                    ->where('z_projects_id',$stay_project_id)
                    ->whereNotIn('z_project_list_id',$lists_ids)
                    ->leftJoin('z_project_list','z_project_list.id','=','z_project_list_project_join.z_project_list_id')
                    ->leftJoin('z_projects_group','z_projects_group.id','=','z_project_list.z_projects_group_id')
                    ->get();
        }

        // FILES
        $merge_files = collect(DB::table('media')->where('z_projects_id', $merge_project_id)->get());
        $stay_files  = collect(DB::table('media')->where('z_projects_id', $stay_project_id)->get());
        $stay_files  = $stay_files->merge($merge_files);

        $jobs=array();
        $added_jobs=array();
        if(Schema::hasTable('z_jobs')){

            $jobs=DB::table('z_jobs')->select('id','name')->where('z_projects_id',$merge_project_id)->get();
            $added_jobs=DB::table('z_jobs')->select('id','name')->where('z_projects_id',$stay_project_id)->get();

        }

        $title = 'Projects Merge Details';

        return view('bct/projects::projects/merge_confirm',
            compact('stay_project', 'merge_project', 'merge_visits', 'stay_visits',
                'merge_contractors', 'stay_contractors', 'merge_contacts', 'stay_contacts',
                'merge_members', 'stay_members', 'merge_files', 'stay_files', 'actions', 'added_actions',
                'lists', 'added_lists', 'title', 'jobs', 'added_jobs')
        );
    }
}
