<?php namespace Bct\Projects\Repositories\Project;

interface ProjectRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Bct\Projects\Models\Project
	 */
	public function grid();

	/**
	 * Returns all the projects entries.
	 *
	 * @return \Bct\Projects\Models\Project
	 */
	public function findAll();

	/**
	 * Returns a projects entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Bct\Projects\Models\Project
	 */
	public function find($id);

	/**
	 * Determines if the given projects is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given projects is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given projects.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a projects entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Bct\Projects\Models\Project
	 */
	public function create(array $data);

	/**
	 * Updates the projects entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Bct\Projects\Models\Project
	 */
	public function update($id, array $data);

	/**
	 * Deletes the projects entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

	/**
	 * check if projects associated with list
	 *
	 * @param $listId
	 * @param $projectIds
	 * @return mixed
	 */
	public function checkIfExistsInList($listId, $projectIds);

	public function statuses();

	public function classifications();

	public function stages();

	public function values();

	public function types();

	public function prevailingWages();

	public function referenceTypes();

	public function visibilities();

	public function contactTypes($query = null);

	public function storeContacts($id, array $data, $mode);

	public function storeSteward($id, $steward_id);

	public function storeContractor($id, array $data, $mode);

	public function deleteContact($pivot_id);

	public function deleteSteward($pivot_id);

	public function deleteContractor($pivot_id);

	public function storeLists($id, $pivot_id, array $data);

	public function scopes($project = null);

	public function shareByArea($scope, $area);

	public function totals($year);

	public function topContractors($scope, $area, $isUnion);

	public function followUp($area);

	public function tags();
	
	public function findAllRegionsForProjects();
	
	public function getExportReport1Project($currentUser);

	public function getReportActiveProjectsWithStewards($currentUser);

    public function getExportActiveServiceJobs($currentUser);
}
