<?php namespace Bct\Projects\Repositories\User;

use Carbon\Carbon;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Cartalyst\Sentinel\Users\UserInterface;
use Illuminate\Support\Facades\DB;
use Bct\Projects\Models\User;
use Bct\Projects\Models\Group;
use Bct\Projects\Models\UserProfile;
use Bct\Projects\Models\ZipCode;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Platform\Roles\Models\Role;

class UserRepository implements UserRepositoryInterface {

    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     * @var \Bct\Projects\Handlers\User\UserDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent mrp model.
     *
     * @var string
     */
    protected $model;

    /**
     * The Users Activation repository.
     *
     * @var \Platform\Users\Repositories\UserActivationRepositoryInterface
     */
    protected $activation;

    /**
     * The Users Auth repository.
     *
     * @var \Platform\Users\Repositories\AuthRepositoryInterface
     */
    protected $auth;


    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container $app
     */
    public function __construct(Container $app) {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['bct.projects.user.handler.data'];

        $this->setValidator($app['bct.projects.user.validator']);

        $this->setModel(get_class($app['Bct\Projects\Models\User']));
    }

    /**
     * {@inheritDoc}
     */
    public function activation() {
        if (!$this->activation) {
            $this->activation = $this->container['platform.users.activation'];
        }

        return $this->activation;
    }

    /**
     * {@inheritDoc}
     */
    public function auth() {
        if (!$this->auth) {
            $this->auth = $this->container['bct.projects.user.auth'];
        }

        return $this->auth;
    }

    /**
     * {@inheritDoc}
     */
    public function getSentinel() {
        if (!$this->sentinel) {
            $this->sentinel = $this->container['sentinel'];
        }

        return $this->sentinel;
    }

    /**
     * {@inheritDoc}
     */
    public function grid() {
        return $this->createModel()
            ->whereHas('roles', function ($query) {
                $query->whereIn('slug', ['repuser', 'repadmin']);
            })
            //->join('activations', 'users.id', '=', 'activations.user_id')
            ->with([
                'roles' => function ($query) {
                    $query->whereIn('slug', ['repuser', 'repadmin']);
                },
                'activations'
            ]);
    }

    public function gridStewards() {
        $results = User::whereHas('roles', function ($q) {
            $q->whereSlug('steward');
        });

        return $results->get()->each(function ($item) {
            $item->status = $item->activated;
            $item->full_name = $item->first_name . ' ' . $item->last_name;

            if ($item->profile) {
                $item->phone = $item->profile->business_phone;
            }

        });
    }


    /**
     * {@inheritDoc}
     */
    public function findAll() {
        return $this->container['cache']->rememberForever('bct.projects.user.all', function () {
            return $this->createModel()->whereHas('roles', function ($query) {
                $query->whereIn('slug', ['repuser', 'repadmin', 'steward']);
            })->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function find($id) {
        return $this->container['cache']->rememberForever('bct.projects.user.' . $id, function () use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input) {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($user, array $input) {
        return $this->validator->on('update', compact('user'))->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForRegistration(array $input) {
        return $this->validator->on('register')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input) {
        return !$id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input) {
        // Create a new user
        $user = $this->createModel();

        // Fire the 'bct.projects.user.creating' event
        if ($this->fireEvent('bct.projects.user.creating', [$input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepareCreate($input);

        // Validate the submitted data
        $messages = $this->validForCreation(array_except($data, 'individual_group'));

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {

            $check_user = $this->createModel()->where('email', $input['email'])->first();

            if ($check_user) {

                if (!$check_user->profile) {
                    /** save the user profile */
                    $profile = new UserProfile();
                    $profile->business_name = $data['first_name'] . " " . $data['last_name'];
                    $profile->business_phone = $data['phone'];
                    $profile->contact_firstname = $data['first_name'];
                    $profile->contact_lastname = $data['last_name'];
                    $profile->contact_phone = $data['phone'];
                    $profile->save();

                    // Save the user
                    $check_user->fill(array_except($data, ['password_confirmation', 'phone']));
                    $check_user->profile()->associate($profile);
                    $check_user->save();
                }

                if (isset($data['individual_group'])) {
                    $group_name = $data['first_name'] . " " . $data['last_name'];
                    $count_groups = Group::where('name', 'LIKE', $group_name . '%')->count();
                    if ($count_groups > 0) {
                        $group_name = $group_name . '-' . $count_groups;
                    }
                    $group = new Group();
                    $group->name = $group_name;
                    $group->individual = 1;
                    $group->status = 1;
                    $group->save();
                    $check_user->groups()->attach($group->id);
                }

                $defaultRole = Role::whereSlug('repuser')->first();
                if (data_get($data, 'is_steward', false)) {
                    $defaultRole = Role::whereSlug('steward')->first();
                }

                if ($activate = array_get($data, 'activated')) {
                    $this->activation()->activate($check_user);
                } else {
                    $this->activation()->deactivate($check_user);
                }

                // Fire the 'platform.user.created' event
                $this->fireEvent('bct.projects.user.created', [$check_user, array_get($data, 'role', $defaultRole->id)]);

            } else {
                /** save the user profile */
                $profile = new UserProfile();
                $profile->business_name = $data['first_name'] . " " . $data['last_name'];
                $profile->business_phone = $data['phone'];
                $profile->contact_firstname = $data['first_name'];
                $profile->contact_lastname = $data['last_name'];
                $profile->contact_phone = $data['phone'];
                $profile->save();

                // Save the user
                $user->fill(array_except($data, ['password_confirmation', 'phone']));
                $user->profile()->associate($profile);
                $user->save();
                if (isset($data['individual_group'])) {
                    $group_name = $data['first_name'] . " " . $data['last_name'];
                    $count_groups = Group::where('name', 'LIKE', $group_name . '%')->count();
                    if ($count_groups > 0) {
                        $group_name = $group_name . '-' . $count_groups;
                    }
                    $group = new Group();
                    $group->name = $group_name;
                    $group->individual = 1;
                    $group->status = 1;
                    $group->save();
                    $user->groups()->attach($group->id);
                }


                // Get the user activation status
                $activate = array_get($data, 'activated');

                // Create the user activation
                $activation = $this->activation()->create($user);

                // Activate the user
                if ($activate === true && !array_get($data, 'is_steward')) {
                    $this->activation()->activate($user, $activation->code);
                }

                $defaultRole = Role::whereSlug('repuser')->first();
                if (data_get($data, 'is_steward', false)) {
                    $defaultRole = Role::whereSlug('steward')->first();
                }

                // Fire the 'platform.user.created' event
                $this->fireEvent('bct.projects.user.created', [$user, array_get($data, 'role', $defaultRole->id)]);
            }


        }

        return [$messages, (isset($check_user)) ? $check_user : $user];
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input) {
        // Get the user object
        $user = $this->find($id);

        // Fire the 'bct.projects.user.updating' event
        if ($this->fireEvent('bct.projects.user.updating', [$user, $input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepareUpdate($input);

        // Validate the submitted data
        $messages = $this->validForUpdate($user, $data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Update the user
            $this->getSentinel()->getUserRepository()->update($user, array_only($data, ['first_name', 'last_name']));

            /** save the user profile */
            if ($profile = $user->profile) {
                $profile->business_name = $data['first_name'] . " " . $data['last_name'];
                $profile->business_phone = $data['phone'];
                $profile->contact_firstname = $data['first_name'];
                $profile->contact_lastname = $data['last_name'];
                $profile->contact_phone = $data['phone'];
                $profile->save();
            }

            $defaultRole = Role::whereSlug('repuser')->first();
            if (data_get($data, 'is_steward', false)) {
                $defaultRole = Role::whereSlug('steward')->first();
            }

            if ($activate = array_get($data, 'activated')) {
                $this->activation()->activate($user);
            } else {
                $this->activation()->deactivate($user);
            }

            // Fire the 'platform.user.updated' event
            $this->fireEvent('bct.projects.user.updated', [$user, array_get($data, 'role', $defaultRole->id)]);
        }

        return [$messages, $user];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id) {
        // Check if the user exists
        if ($user = $this->find($id)) {

            // Fire the 'bct.projects.user.deleted' event
            $this->fireEvent('bct.projects.user.deleted', [$user]);

            // Delete the user entry
            $roles = DB::table('roles')->whereIn('slug',
                ['repuser', 'repadmin', 'super-repadmin', 'pp-guest'])->lists('id');
            DB::table('role_users')->where('user_id', $user->id)->whereIn('role_id', $roles)->delete();
            DB::table('z_user_group_join')->where('user_id', $user->id)->delete();


            return true;
        }

        return false;
    }

    /**
     * Link mrp user to contractor
     *
     * @param $id
     * @param array $input
     * @return \Bct\Projects\Models\User
     */
    public function link($id, array $input) {
        // Get the user object
        $user = $this->find($id);

        // Get the user activation status
        if ($activate = array_get($input, 'activated')) {
            $this->activation()->activate($user);
        } else {
            $this->activation()->deactivate($user);
        }

        // Get the new user roles
        if ($role = array_get($input, 'role')) {
            // Get the user roles
            $userRoles = $user->roles->lists('id')->toArray();

            // Prepare the roles to be added and removed
            $toAdd = array_diff([$role], $userRoles);
            $toDel = array_diff($userRoles, [$role]);

            // Detach the user roles
            if (!empty($toDel)) $user->roles()->detach($toDel);

            // Attach the user roles
            if (!empty($toAdd)) $user->roles()->attach($toAdd);
        }

        // Fire the 'bct.projects.user.updated' event
        $this->fireEvent('bct.projects.user.updated', $user);

        return $user;
    }

    /**
     * Process to MRP user profile
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function profile(UserInterface $user) {
        if (!$profile = $this->find($user->user_id)) {
            $profile = new User();
        }

        $this->fireEvent('bct.projects.user.profile', [$profile]);

        return $profile;
    }

    /**
     * {@inheritDoc}
     */
    public function attach($id, $group_id) {
        try {
            // Get the group object
            $user = $this->find($id);

            $user->groups()->attach($group_id);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function detach($id, $group_id) {
        try {
            // Get the group object
            $user = $this->find($id);

            $user->groups()->detach($group_id);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function repVisits($region) {
        $users = DB::table('users as u')
            ->select(DB::raw('u.id as user_id'),
                DB::raw('CONCAT(u.first_name, " ", u.last_name) as name'),
                DB::raw('p.name as project_name'),
                DB::raw('pv.created_at as visit_date'))
            ->join('z_project_visit_user_id_join as uvj', 'uvj.user_id', '=', 'u.id')
            ->join('z_project_visit as pv', 'uvj.z_project_visit_id', '=', 'pv.id')
            ->join('z_projects as p', 'pv.z_projects_id', '=', 'p.id')
            ->join('role_users as ru', 'ru.user_id', '=', 'u.id')
            ->join('roles', 'ru.role_id', '=', 'roles.id')
            ->whereIn('roles.slug', ['repuser', 'repadmin', 'steward'])
            ->whereNull('p.deleted_at')
            ->orderBy('pv.created_at', 'desc')
            ->take(10);

        if ($region == 'OUT_OF_REGION') {
            $users->where(function ($query) {
                $query->whereNull('p.region_abrv')
                    ->orWhereIn('p.region_abrv', ['n/a', '']);
            });
        } else if (!empty($region)) {
            $users->where('p.region_abrv', $region);
        }

        return $users->get();
    }

    public function repsWithoutCurrent($user_id) {
        return $this->createModel()
            ->select(
                'users.id',
                'users.first_name',
                'users.last_name'
            )
            ->leftJoin('role_users', 'users.id', '=', 'role_users.user_id')
            ->leftJoin('roles', 'roles.id', '=', 'role_users.role_id')
            ->rightJoin('activations', 'activations.user_id','=','users.id')
            ->where('users.id', '!=', $user_id)
            ->where('activations.completed','=','1')
            ->whereIn('roles.slug', ['repuser', 'repadmin'])
            ->orderBy('users.first_name', 'asc')
            ->orderBy('users.last_name', 'asc')
            ->groupBy('users.id')
            ->get();
    }
}
