<?php namespace Bct\Projects\Repositories\User;
/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    2.0.3
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Cartalyst\Support\Traits;
use Cartalyst\Sentinel\Checkpoints;
use Illuminate\Container\Container;
use League\Flysystem\Exception;

class AuthRepository implements AuthRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait;

	/**
	 * The Platform Users repository.
	 *
	 * @var \Platform\Users\Repositories\UserRepositoryInterface
	 */
	protected $users;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->users = $app['bct.projects.user'];
	}

	/**
	 * {@inheritDoc}
	 */
	public function login(array $input)
	{
		try
		{
			// Should the user be remembered?
			$remember = (bool) array_get($input, 'remember', false);

			// Get all the valid credentials columns
			$credentials = array_intersect_key($input, array_flip($this->getValidLoginColumns()));

			// Authenticate the user with the given credentials
			if ($user = $this->users->getSentinel()->authenticate($credentials, $remember))
			{
				// Fire the 'platform.user.logged_in'
				$this->fireEvent('platform.user.logged_in', $user);

				return [ null, $user ];
			}

			$errors = trans('platform/users::auth/message.user_not_found');
		}
		catch (Checkpoints\NotActivatedException $e)
		{
			$errors = trans('platform/users::auth/message.user_not_activated');
		}
		catch (Checkpoints\ThrottlingException $e)
		{
			$type = $e->getType();

			$delay = $e->getDelay();

			$errors = trans("platform/users::auth/message.throttling.{$type}", compact('delay'));
		}

		return [ $errors, null ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function logout()
	{
		// Get the logged in user
		if ($user = $this->users->getSentinel()->getUser())
		{
			// Log the user out
			$this->users->getSentinel()->logout();

			// Fire the 'platform.user.logged_out' event
			$this->fireEvent('platform.user.logged_out', $user);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function getValidLoginColumns()
	{
		return $this->container['config']->get('platform-users.login_columns', ['email', 'password']);
	}

	/**
	 * {@inheritDoc}
	 */
	public function register(array $input, array $roles = [], $activate = false)
	{
		$user = null;

		list($messages, $user) = $this->users->register($input);

		return [ $messages, $user ];
	}

}
