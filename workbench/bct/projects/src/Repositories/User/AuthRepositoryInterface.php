<?php namespace Bct\Projects\Repositories\User;
/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    2.0.3
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

interface AuthRepositoryInterface {

	/**
	 * Logs a user in.
	 *
	 * @param  array  $input
	 * @return array
	 */
	public function login(array $input);

	/**
	 * Logs a user out.
	 *
	 * @return void
	 */
	public function logout();

	/**
	 * Register a new user with the given data.
	 *
	 * @param  array  $data
	 * @param  array  $roles
	 * @param  bool  $activate
	 * @return \Cartalyst\Sentinel\Users\UserInterface
	 */
	public function register(array $data, array $roles = [], $activate = false);

	/**
	 * Returns all the valid login columns.
	 *
	 * @return array
	 */
	public function getValidLoginColumns();

}
