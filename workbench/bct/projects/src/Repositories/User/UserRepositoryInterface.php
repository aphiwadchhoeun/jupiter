<?php namespace Bct\Projects\Repositories\User;

use Cartalyst\Sentinel\Users\UserInterface;

interface UserRepositoryInterface {

    /**
     * Returns a dataset compatible with data grid.
     *
     * @return \Bct\Projects\Models\User
     */
    public function grid();

    /**
     * Returns a dataset for stewards
     *
     * @return mixed
     */
    public function gridStewards();

    /**
     * Returns all the mrp entries.
     *
     * @return \Bct\Projects\Models\User
     */
    public function findAll();

    /**
     * Returns a mrp entry by its primary key.
     *
     * @param  int $id
     * @return \Bct\Projects\Models\User
     */
    public function find($id);

    /**
     * Determines if the given mrp is valid for creation.
     *
     * @param  array $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForCreation(array $data);

    /**
     * Determines if the given mrp is valid for update.
     *
     * @param  int $id
     * @param  array $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForUpdate($id, array $data);

    /**
     * Creates or updates the given mrp.
     *
     * @param  int $id
     * @param  array $input
     * @return bool|array
     */
    public function store($id, array $input);

    /**
     * Creates a mrp entry with the given data.
     *
     * @param  array $data
     * @return \Bct\Projects\Models\User
     */
    public function create(array $data);

    /**
     * Updates the mrp entry with the given data.
     *
     * @param  int $id
     * @param  array $data
     * @return \Bct\Projects\Models\User
     */
    public function update($id, array $data);

    /**
     * Deletes the mrp entry.
     *
     * @param  int $id
     * @return bool
     */
    public function delete($id);

    /**
     * Link mrp user to contractor
     *
     * @param $id
     * @param array $data
     * @return \Bct\Projects\Models\User
     */
    public function link($id, array $data);

    /**
     * Process to MRP user profile
     *
     * @param UserInterface $user
     * @return mixed
     */
    public function profile(UserInterface $user);

	/**
	 * Returns the Platform Users Auth repository instance.
	 *
	 * @return \Bct\Projects\Repositories\User\AuthRepositoryInterface
	 */
	public function auth();

	/**
	 * Attach group the user.
	 *
	 * @param  int  $id
	 * @param  int  $group_id
	 * @return bool
	 */
	public function attach($id, $group_id);

	/**
	 * Detach group the user.
	 *
	 * @param  int  $id
	 * @param  int  $group_id
	 * @return bool
	 */
	public function detach($id, $group_id);

	public function repVisits($area);

	public function repsWithoutCurrent($user_id);

}
