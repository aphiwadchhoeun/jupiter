<?php namespace Bct\Projects\Repositories\Action;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;
use Bct\Projects\Models\Action;

class ActionRepository implements ActionRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\Action\ActionDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent projects model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.action.handler.data'];

		$this->setValidator($app['bct.projects.action.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\Action']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('bct.projects.action.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('bct.projects.action.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($project_id, $id, array $input)
	{
		$input['z_projects_id'] = $project_id;

		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new action
		$action = $this->createModel();

		// Fire the 'bct.projects.action.creating' event
		if ($this->fireEvent('bct.projects.action.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the action
			$action->user_id = Sentinel::getUser()->id;
			$action->fill($data)->save();

			// Fire the 'bct.projects.action.created' event
			$this->fireEvent('bct.projects.action.created', [ $action ]);
		}

		return [ $messages, $action ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the action object
		$action = $this->find($id);

		// Fire the 'bct.projects.action.updating' event
		if ($this->fireEvent('bct.projects.action.updating', [ $action, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($action, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the action
			$action->fill($data)->save();

			// Fire the 'bct.projects.action.updated' event
			$this->fireEvent('bct.projects.action.updated', [ $action ]);
		}

		return [ $messages, $action ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the action exists
		if ($action = $this->find($id))
		{
			// Fire the 'bct.projects.action.deleted' event
			$this->fireEvent('bct.projects.action.deleted', [ $action ]);

			// Delete the action entry
			$action->delete();

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
