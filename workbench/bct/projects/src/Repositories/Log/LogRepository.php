<?php namespace Bct\Projects\Repositories\Log;

use Bct\Projects\Models\ActionType;
use Bct\Projects\Models\Contact;
use Bct\Projects\Models\ContactType;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\Group;
use Bct\Projects\Models\Lists;
use Bct\Projects\Models\Project;
use Bct\Projects\Models\Scope;
use Bct\Projects\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Validator;

class LogRepository implements LogRepositoryInterface {

	use Traits\ContainerTrait;
	use Traits\RepositoryTrait{
		Traits\RepositoryTrait::createModel as parentCreateModel;
	}

	/**
	 * The Data handler.
	 *
	 */
	protected $data;

	/**
	 * The Eloquent mrp model.
	 *
	 * @var string
	 */
	protected $model;

	protected static $newProfile = 'CREATE_NEW_PROFILE';
	protected static $updateProfile = 'UPDATE_PROFILE';
	protected static $deleteProfile = 'DELETE_PROFILE';

	protected static $newUserGroup = 'CREATE_NEW_USER_GROUP';
	protected static $updateUserGroup = 'UPDATE_USER_GROUP';
	protected static $deleteUserGroup = 'DELETE_USER_GROUP';

	protected static $newContractor = 'CREATE_NEW_CONTRACTOR';
	protected static $updateContractor = 'UPDATE_CONTRACTOR';
	protected static $deleteContractor = 'DELETE_CONTRACTOR';

	protected static $newContact = 'CREATE_NEW_CONTACT';
	protected static $updateContact = 'UPDATE_CONTACT';
	protected static $deleteContact = 'DELETE_CONTACT';

	protected static $newProject = 'CREATE_NEW_PROJECT';
	protected static $updateProject = 'UPDATE_PROJECT';
	protected static $deleteProject = 'DELETE_PROJECT';

	protected static $newList = 'CREATE_NEW_LIST';
	protected static $updateList = 'UPDATE_LIST';
	protected static $deleteList = 'DELETE_LIST';

	protected static $newScope = 'CREATE_NEW_SCOPE';
	protected static $updateScope = 'UPDATE_SCOPE';
	protected static $deleteScope = 'DELETE_SCOPE';

	protected static $newContactType = 'CREATE_NEW_CONTACT_TYPE';
	protected static $updateContactType = 'UPDATE_CONTACT_TYPE';
	protected static $deleteContactType = 'DELETE_CONTACT_TYPE';

    protected static $newActionType = 'CREATE_NEW_ACTION_TYPE';
    protected static $updateActionType = 'UPDATE_ACTION_TYPE';
    protected static $deleteActionType = 'DELETE_ACTION_TYPE';

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container $app
	 */
	public function __construct(Container $app) {
		$this->setContainer($app);

		$this->setModel(get_class($app['Bct\Projects\Models\Log']));
	}

	/**
	 * Create a new instance of the model.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function createModel(array $data = [])
	{
		$class = '\\'.ltrim($this->model, '\\');

		$log = new $class($data);
		$currentUser = Sentinel::getUser();
		if ($currentUser) $log->committed_by = $currentUser->id;

		return $log;
	}

	public function logNewProfile(User $user) {
		$log = $this->createModel();
		$log->action = static::$newProfile;
		$log->extra = json_encode($user->getAttributes());
		$log->save();
	}

	public function logUpdateProfile(User $user) {
		$log = $this->createModel();
		$log->action = static::$updateProfile;
		$log->extra = json_encode($user->getAttributes());
		$log->save();
	}

	public function logDeleteProfile(User $user)
	{
		$log = $this->createModel();
		$log->action = static::$deleteProfile;
		$log->extra = json_encode($user->getAttributes());
		$log->save();
	}

	public function logNewUserGroup(Group $group) {
		$log = $this->createModel();
		$log->action = static::$newUserGroup;
		$log->extra = json_encode($group->getAttributes());
		$log->save();
	}

	public function logUpdateUserGroup(Group $group) {
		$log = $this->createModel();
		$log->action = static::$updateUserGroup;
		$log->extra = json_encode($group->getAttributes());
		$log->save();
	}

	public function logDeleteUserGroup(Group $group) {
		$log = $this->createModel();
		$log->action = static::$deleteUserGroup;
		$log->extra = json_encode($group->getAttributes());
		$log->save();
	}

	public function logNewContractor(Contractor $contractor)
	{
		$log = $this->createModel();
		$log->action = static::$newContractor;
		$log->extra = json_encode($contractor->getAttributes());
		$log->save();
	}

	public function logUpdateContractor(Contractor $contractor)
	{
		$log = $this->createModel();
		$log->action = static::$updateContractor;
		$log->extra = json_encode($contractor->getAttributes());
		$log->save();
	}

	public function logDeleteContractor(Contractor $contractor)
	{
		$log = $this->createModel();
		$log->action = static::$deleteContractor;
		$log->extra = json_encode($contractor->getAttributes());
		$log->save();
	}

	public function logNewContact(Contact $item)
	{
		$log = $this->createModel();
		$log->action = static::$newContact;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logUpdateContact(Contact $item)
	{
		$log = $this->createModel();
		$log->action = static::$updateContact;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logDeleteContact(Contact $item)
	{
		$log = $this->createModel();
		$log->action = static::$deleteContact;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logNewProject(Project $item)
	{
		$log = $this->createModel();
		$log->action = static::$newProject;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logUpdateProject(Project $item)
	{
		$log = $this->createModel();
		$log->action = static::$updateProject;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logDeleteProject(Project $item)
	{
		$log = $this->createModel();
		$log->action = static::$deleteProject;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logNewList(Lists $item)
	{
		$log = $this->createModel();
		$log->action = static::$newList;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logUpdateList(Lists $item)
	{
		$log = $this->createModel();
		$log->action = static::$updateList;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logDeleteList(Lists $item)
	{
		$log = $this->createModel();
		$log->action = static::$deleteList;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logNewScope(Scope $item) {
		$log = $this->createModel();
		$log->action = static::$newScope;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logUpdateScope(Scope $item) {
		$log = $this->createModel();
		$log->action = static::$updateScope;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logDeleteScope(Scope $item) {
		$log = $this->createModel();
		$log->action = static::$deleteScope;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logNewContactType(ContactType $item) {
		$log = $this->createModel();
		$log->action = static::$newContactType;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logUpdateContactType(ContactType $item) {
		$log = $this->createModel();
		$log->action = static::$updateContactType;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}

	public function logDeleteContactType(ContactType $item) {
		$log = $this->createModel();
		$log->action = static::$deleteContactType;
		$log->extra = json_encode($item->getAttributes());
		$log->save();
	}


    public function logNewActionType(ActionType $item) {
        $log = $this->createModel();
        $log->action = static::$newActionType;
        $log->extra = json_encode($item->getAttributes());
        $log->save();
    }


    public function logUpdateActionType(ActionType $item) {
        $log = $this->createModel();
        $log->action = static::$updateActionType;
        $log->extra = json_encode($item->getAttributes());
        $log->save();
    }

    public function logDeleteActionType(ActionType $item) {
        $log = $this->createModel();
        $log->action = static::$deleteActionType;
        $log->extra = json_encode($item->getAttributes());
        $log->save();
    }
}
