<?php namespace Bct\Projects\Repositories\Log;

use Bct\Projects\Models\Contact;
use Bct\Projects\Models\ContactType;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\Group;
use Bct\Projects\Models\Lists;
use Bct\Projects\Models\Project;
use Bct\Projects\Models\Scope;
use Bct\Projects\Models\User;

interface LogRepositoryInterface {

	public function logNewProfile(User $user);

	public function logUpdateProfile(User $user);

	public function logDeleteProfile(User $user);

	public function logNewUserGroup(Group $user);

	public function logUpdateUserGroup(Group $user);

	public function logDeleteUserGroup(Group $user);

	public function logNewContractor(Contractor $contractor);

	public function logUpdateContractor(Contractor $contractor);

	public function logDeleteContractor(Contractor $contractor);

	public function logNewContact(Contact $item);

	public function logUpdateContact(Contact $item);

	public function logDeleteContact(Contact $item);

	public function logNewProject(Project $item);

	public function logUpdateProject(Project $item);

	public function logDeleteProject(Project $item);

	public function logNewList(Lists $item);

	public function logUpdateList(Lists $item);

	public function logDeleteList(Lists $item);

	public function logNewScope(Scope $item);

	public function logUpdateScope(Scope $item);

	public function logDeleteScope(Scope $item);

	public function logNewContactType(ContactType $item);

	public function logUpdateContactType(ContactType $item);

	public function logDeleteContactType(ContactType $item);
}
