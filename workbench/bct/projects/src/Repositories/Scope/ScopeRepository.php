<?php namespace Bct\Projects\Repositories\Scope;

use Validator;
use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;
use Bct\Projects\Models\Scope;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ScopeRepository implements ScopeRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Bct\Projects\Handlers\Scope\ScopeDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent projects model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['bct.projects.scope.handler.data'];

		$this->setValidator($app['bct.projects.scope.validator']);

		$this->setModel(get_class($app['Bct\Projects\Models\Scope']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel()->whereNull('z_scopes.deleted_at');
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('bct.projects.scope.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('bct.projects.scope.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($scope, array $input)
	{
		return $this->validator->on('update', compact('scope'))->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new scope
		$scope = $this->createModel();

		// Fire the 'bct.projects.scope.creating' event
		if ($this->fireEvent('bct.projects.scope.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the scope
			$scope->fill($data)->save();

			// Fire the 'bct.projects.scope.created' event
			$this->fireEvent('bct.projects.scope.created', [ $scope ]);
		}

		return [ $messages, $scope ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the scope object
		$scope = $this->find($id);

		// Fire the 'bct.projects.scope.updating' event
		if ($this->fireEvent('bct.projects.scope.updating', [ $scope, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($scope, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the scope
			$scope->fill($data)->save();

			// Fire the 'bct.projects.scope.updated' event
			$this->fireEvent('bct.projects.scope.updated', [ $scope ]);
		}

		return [ $messages, $scope ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the scope exists
		if ($scope = $this->find($id))
		{
			// Fire the 'bct.projects.scope.deleted' event
			$this->fireEvent('bct.projects.scope.deleted', [ $scope ]);

			// Delete the scope entry
			$scope->delete();
            DB::table('z_project_contractor_join')->where('z_scopes_id', $scope->id)->delete();

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
