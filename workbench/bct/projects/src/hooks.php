<?php

use Platform\Menus\Models\Menu;
use Platform\Roles\Models\Role;

/*
|--------------------------------------------------------------------------
| Extension Hooks
|--------------------------------------------------------------------------
|
| Hooks for various stages of an Extension's lifecycle. You can access the
| individual extension properties through $extension->getSlug().
|
*/

Extension::registering(function (Extension $extension) {
    // Before an extension is registered (happens for every extension)
});

Extension::registered(function (Extension $extension) {
    // After an extension is registered
});

Extension::booting(function (Extension $extension) {
    // Before an installed and enabled extension boots (after all are registered)
});

Extension::booted(function (Extension $extension) {
    // After an installed and enabled extension boots
});

Extension::installing(function (Extension $extension) {
    if ($extension->getSlug() == 'bct/projects') {
        // Before an extension is installed
        $repUserRole = Role::firstOrNew([
            'slug' => 'repuser',
            'name' => 'Projects - RepUser',
        ]);
        $repUserRole->save();

        $repUserRole = Role::firstOrNew([
            'slug' => 'repadmin',
            'name' => 'Projects - RepAdmin',
        ]);
        $repUserRole->save();

        $repUserRole = Role::firstOrNew([
            'slug' => 'steward',
            'name' => 'Projects - Steward',
        ]);
        $repUserRole->save();

        $repUserRole = Role::firstOrNew([
            'slug' => 'super-repadmin',
            'name' => 'Projects - SuperRepAdmin',
        ]);
        $repUserRole->save();

        // view only role
        $guest = Role::firstOrNew([
            'slug' => 'pp-guest',
            'name' => 'Projects - Guest',
        ]);
        $guest->save();
    }
});

Extension::installed(function (Extension $extension) {
    // After an extension is installed

});

Extension::uninstalling(function (Extension $extension) {
    // Before an extension is uninstalled
});

Extension::uninstalled(function (Extension $extension) {
    // After an extension is uninstalled
});

Extension::enabling(function (Extension $extension) {
    // Before an extension is enabled
});

Extension::enabled(function (Extension $extension) {
    if ($extension->getSlug() == 'bct/projects') {
        // After an extension is enabled
        $superadmin_roles = Role::whereIn('slug', ['admin', 'super-repadmin'])->lists('id');
        $admin_roles      = Role::whereIn('slug', ['admin', 'repadmin', 'super-repadmin'])->lists('id');
        $user_roles       = Role::whereIn('slug', ['admin', 'repuser', 'repadmin', 'super-repadmin','pp-guest'])->lists('id');
        $user_no_guest_roles= Role::whereIn('slug', ['admin', 'repuser', 'repadmin', 'super-repadmin'])->lists('id');

        // set this menu for everyone
        Menu::whereSlug('projects-admin-projects-main')->update([
            'roles' => Role::whereIn('slug',
                ['admin', 'repuser', 'repadmin', 'super-repadmin', 'pp-guest'])->lists('id')
        ]);

        $menu_items = Menu::where('extension', 'bct/projects')->get();
        foreach ($menu_items as $menu_item) {
            $is_superadmin = strpos($menu_item->slug, 'super');
            $is_admin      = strpos($menu_item->slug, 'admn');
            $is_usr        = strpos($menu_item->slug, 'usr');

            if ($is_usr) {
                if(strpos($menu_item->slug,'visits') || strpos($menu_item->slug,'lists') || strpos($menu_item->slug,'routes') || strpos($menu_item->slug,'reports')){
                    $roles = '['.$user_no_guest_roles->implode(",").']';
                }
                else{
                    $roles = '['.$user_roles->implode(",").']';
                }
            }

            if ($is_admin) {
                $roles = '['.$admin_roles->implode(",").']';
            }

            if ($is_superadmin) {
                $roles = '['.$superadmin_roles->implode(",").']';
            }

            if ( ! empty($roles)) {
                $item        = Menu::findOrFail($menu_item->id);
                $item->roles = $roles;
                $item->save();
            }
        }

        // set this menu for admin types, special case
        Menu::whereSlug('projects-settings-admn')->update([
            'roles' => Role::whereIn('slug', ['admin', 'repadmin', 'super-repadmin'])->lists('id')
        ]);

        Role::where('slug', 'repuser')->update([
            'permissions' => json_encode([
                'superuser'                           => false,
                'projects_users.index'                => false,
                'projects_users.create'               => false,
                'projects_users.edit'                 => false,
                'projects_users.delete'               => false,
                'projects_users.visits'               => false,
                'projects_group.index'                => false,
                'projects_groups.create'              => false,
                'projects_groups.delete'              => false,
                'projects_contact.index'              => true,
                'projects_contact.create'             => true,
                'projects_contact.edit'               => true,
                'projects_contact.delete'             => true,
                'projects_contractors.index'          => true,
                'projects_contractors.create'         => true,
                'projects_contractors.edit'           => true,
                'projects_contractors.delete'         => true,
                'projects_contractors.address'        => true,
                'projects_contractors.phone'          => true,
                'projects_contractors.email'          => true,
                'projects_contractors.notes'          => true,
                'projects_contractors.company'        => true,
                'projects_contractors.license'        => true,
                'projects_projects.index'             => true,
                'projects_projects.create'            => true,
                'projects_projects.edit'              => true,
                'projects_projects.delete'            => true,
                'projects_project_actions.all'        => true,
                'projects_project_contractors.all'    => true,
                'projects_project_contacts.all'       => true,
                'projects_project_visits.all'         => true,
                'projects_project_files.all'          => true,
                'projects_project_lists.all'          => true,
                'projects_project_members.all'        => true,
                'projects_project_stewards.all'       => true,
                'projects_contractor_files.all'       => true,
                'projects_lists.index'                => false,
                'contact_types.index'                 => false,
                'action_types.index'                  => false,
                'scopes.index'                        => false,
                'stewards.index'                      => false,
                'bct.projects.settings.configuration' => false,
                'projects_lists.search'               => true,
                'projects_lists.create'               => true,
                'projects_lists.edit'                 => true,
                'projects_lists.delete'               => true,
                'projects_routes.index'               => true,
                'projects_map.index'                  => true,
                'projects_dashboard.index'            => true,
                'projects_media.index'                => true,
                'projects_visits.index'               => true,
                'bct.pp.reports'                      => true,
            ])
        ]);

        Role::where('slug', 'repadmin')->update([
            'permissions' => json_encode([
                'superuser'                           => false,
                'projects_users.index'                => true,
                'projects_users.create'               => true,
                'projects_users.edit'                 => true,
                'projects_users.delete'               => true,
                'projects_users.visits'               => true,
                'projects_group.index'                => true,
                'projects_groups.create'              => true,
                'projects_groups.edit'                => true,
                'projects_groups.delete'              => true,
                'projects_contact.index'              => true,
                'projects_contact.create'             => true,
                'projects_contact.edit'               => true,
                'projects_contact.delete'             => true,
                'projects_contractors.index'          => true,
                'projects_contractors.create'         => true,
                'projects_contractors.edit'           => true,
                'projects_contractors.delete'         => true,
                'projects_contractors.address'        => true,
                'projects_contractors.phone'          => true,
                'projects_contractors.email'          => true,
                'projects_contractors.notes'          => true,
                'projects_contractors.company'        => true,
                'projects_contractors.license'        => true,
                'projects_projects.index'             => true,
                'projects_projects.create'            => true,
                'projects_projects.edit'              => true,
                'projects_projects.delete'            => true,
                'projects_project_actions.all'        => true,
                'projects_project_contractors.all'    => true,
                'projects_project_contacts.all'       => true,
                'projects_project_visits.all'         => true,
                'projects_project_files.all'          => true,
                'projects_contractor_files.all'       => true,
                'projects_project_lists.all'          => true,
                'projects_project_members.all'        => true,
                'projects_project_stewards.all'       => true,
                'projects_lists.index'                => true,
                'contact_types.index'                 => true,
                'action_types.index'                  => true,
                'scopes.index'                        => true,
                'stewards.index'                      => true,
                'bct.projects.settings.configuration' => true,
                'projects_lists.search'               => true,
                'projects_lists.create'               => true,
                'projects_lists.edit'                 => true,
                'projects_lists.delete'               => true,
                'projects_routes.index'               => true,
                'projects_map.index'                  => true,
                'projects_dashboard.index'            => true,
                'projects_locals.index'               => true,
                'projects_media.index'                => true,
                'projects_visits.index'               => true,
                'bct.pp.reports'                      => true,
            ])
        ]);

        Role::where('slug', 'super-repadmin')->update([
            'permissions' => json_encode([
                'tags.index'                          => true,
                'tags.create'                         => false,
                'tags.update'                         => false,
                'tags.delete'                         => false,
                'projects_users.index'                => true,
                'projects_users.create'               => true,
                'projects_users.edit'                 => true,
                'projects_users.delete'               => true,
                'projects_users.visits'               => true,
                'scopes.index'                        => true,
                'bct.projects.settings.configuration' => true,
                'projects_dashboard.index'            => true,
                'queue.restart'                       => true,

                // plus what are in repadmin
                'superuser'                           => false,
                'projects_group.index'                => true,
                'projects_groups.create'              => true,
                'projects_groups.edit'                => true,
                'projects_groups.delete'              => true,
                'projects_contact.index'              => true,
                'projects_contact.create'             => true,
                'projects_contact.edit'               => true,
                'projects_contact.delete'             => true,
                'projects_contractors.index'          => true,
                'projects_contractors.create'         => true,
                'projects_contractors.edit'           => true,
                'projects_contractors.delete'         => true,
                'projects_contractors.address'        => true,
                'projects_contractors.phone'          => true,
                'projects_contractors.email'          => true,
                'projects_contractors.notes'          => true,
                'projects_contractors.company'        => true,
                'projects_contractors.license'        => true,
                'projects_projects.index'             => true,
                'projects_projects.create'            => true,
                'projects_projects.edit'              => true,
                'projects_projects.delete'            => true,
                'projects_project_actions.all'        => true,
                'projects_project_contractors.all'    => true,
                'projects_project_contacts.all'       => true,
                'projects_project_visits.all'         => true,
                'projects_project_files.all'          => true,
                'projects_contractor_files.all'       => true,
                'projects_project_lists.all'          => true,
                'projects_project_members.all'        => true,
                'projects_project_stewards.all'       => true,
                'projects_lists.index'                => true,
                'contact_types.index'                 => true,
                'action_types.index'                  => true,
                'stewards.index'                      => true,
                'projects_lists.search'               => true,
                'projects_lists.create'               => true,
                'projects_lists.edit'                 => true,
                'projects_lists.delete'               => true,
                'projects_routes.index'               => true,
                'projects_map.index'                  => true,
                'projects_locals.index'               => true,
                'projects_media.index'                => true,
                'projects_visits.index'               => true,
                'bct.pp.reports'                      => true,
            ])
        ]);

        Role::where('slug', 'pp-guest')->update([
            'permissions' => json_encode([
                'superuser'                             => false,
                'pp_guest.dashboard'                    => true,
                'pp_guest.users'                        => true,
                'pp_guest.users.details'                => true,
                'pp_guest.groups'                       => true,
                'pp_guest.groups.details'               => true,
                'pp_guest.contacts'                     => true,
                'pp_guest.contractors'                  => true,
                'pp_guest.contractors.details'          => true,
                'pp_guest.contractors.address'          => true,
                'pp_guest.contractors.phone'            => true,
                'pp_guest.contractors.email'            => true,
                'pp_guest.contractors.company'          => true,
                'pp_guest.contractors.license'          => true,
                'pp_guest.contractors.notes'            => true,
                'pp_guest.projects'                     => true,
                'pp_guest.projects.details'             => true,
                'pp_guest.projects.details.actions'     => true,
                'pp_guest.projects.details.contractors' => true,
                'pp_guest.projects.details.contacts'    => true,
                'pp_guest.projects.details.visits'      => false,
                'pp_guest.projects.details.files'       => true,
                'pp_guest.projects.details.lists'       => false,
                'pp_guest.projects.details.members'     => true,
                'pp_guest.lists'                        => false,
                'pp_guest.lists.details'                => false,
                'pp_guest.contact-types'                => true,
                'pp_guest.scopes'                       => true,
                'pp_guest.stewards'                     => true,
                'pp_guest.tags'                         => true,
                'pp_guest.routes'                       => false,
                'pp_guest.map'                          => true,
                'pp_guest.map.search'                   => true,
                'pp_guest.locals'                       => true,
                'pp_guest.media'                        => true,
                'pp_guest.reviews'                      => false,
                'pp_guest.reports'                      => false,
            ])
        ]);
    }
});

Extension::disabling(function (Extension $extension) {
    // Before an extension is disabled
});

Extension::disabled(function (Extension $extension) {
    // After an extension is disabled
});

Extension::upgrading(function (Extension $extension) {
    // Before an extension is upgraded
});

Extension::upgraded(function (Extension $extension) {
    // After an extension is upgraded
});
