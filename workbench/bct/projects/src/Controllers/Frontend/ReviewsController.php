<?php namespace Bct\Projects\Controllers\Frontend;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 15.02.16
 * Time: 15:59
 */
use Bct\Projects\Models\Role;
use Bct\Projects\Repositories\Visit\VisitRepositoryInterface;

class ReviewsController extends BaseController
{
    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];

    protected $visits;


    /**
     * ReviewsController constructor.
     *
     * @param VisitRepositoryInterface $visits
     */
    public function __construct(
        VisitRepositoryInterface $visits
    ) {
        parent::__construct();

        $this->visits = $visits;
    }

    public function index()
    {
        $currentUser = $this->currentUser;
        return view('bct/projects::reviews/index', compact('currentUser'));
    }

    public function grid()
    {
        return $this->visits->dataGrid();
    }

    public function approve()
    {
        $input_ids = input('rows');
        
        if ((is_array($input_ids) && empty($input_ids)) || ( is_string($input_ids) && $input_ids !=='all' )) {
            return response()->json([ 'success' => false, 'errors' => 'Not correct input data' ], 422);
        }

        try {
            $this->visits->approve($input_ids);
        } catch (\Exception $e) {
            return response()->json([ 'success' => false, 'errors' => $e->getMessage() ], 422);
        }

        $message = trans('bct/projects::reviews/message.success.approve');
        return response()->json([ 'success' => true , 'message' => $message], 200);
    }

    public function unapprove()
    {
        $input_ids = input('rows');

        if ((is_array($input_ids) && empty($input_ids)) || ( is_string($input_ids) && $input_ids !=='all' )) {
            return response()->json([ 'success' => false, 'errors' => 'Not correct input data' ], 422);
        }

        try {
            $this->visits->unapprove($input_ids);
        } catch (\Exception $e) {
            return response()->json([ 'success' => false, 'errors' => $e->getMessage() ], 422);
        }

        $message = trans('bct/projects::reviews/message.success.approve');
        return response()->json([ 'success' => true , 'message' => $message], 200);
    }

}