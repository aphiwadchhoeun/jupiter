<?php namespace Bct\Projects\Controllers\Frontend;

use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Models\TempItems;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use JavaScriptProjects;
use JavaScriptUnionimpactbase;

class RoutesController extends AuthorizedController {


    protected $authWhitelist = [
        'index'
    ];

    protected $permissionsWhitelist = [
        'index'
    ];

    /**
     * The repository.
     *
     * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
     */
    protected $users;

    /**
     * Constructor.
     *
     * @param UserRepositoryInterface $users
     */
    public function __construct(UserRepositoryInterface $users) {
        parent::__construct();

        $this->users = $users;
        if ($this->currentUser) $this->currentUser = $this->users->find($this->currentUser->id);
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        if (!$user = $this->currentUser) {

            JavaScriptUnionimpactbase::put([
                'default_lat'    => config('bct.unionimpactbase.map.location.default.lat'),
                'default_lng'    => config('bct.unionimpactbase.map.location.default.lng'),
            ]);

            return view('bct/unionimpactbase::openroutes.index');
        }

        JavaScriptProjects::put([
            'default_lat'    => config_projects('bct.unionimpactbase.map.location.default.lat'),
            'default_lng'    => config_projects('bct.unionimpactbase.map.location.default.lng'),
        ]);

        return view('bct/projects::routes.index', compact('user'));
    }

    public function submitRoute() {
        if ($projects = request()->get('entries')) {
            $this->currentUser->tempItems()->delete();

            foreach ($projects as $key => $project) {
                $tempItem = new TempItems();
                $tempItem->sortOrder = $key;
                $tempItem->z_projects_id = $project;
                $tempItem->user()->associate($this->currentUser);
                $tempItem->save();
            }
        }

        return route('bct.projects.routes.index');
    }

    /**
     * Create custom entry.
     *
     */
    public function create() {
        $tempItem = new TempItems();
        $tempItem->address = input('address');
        $tempItem->lat = input('lat');
        $tempItem->lng = input('lng');
        $tempItem->sortOrder = input('sort');
        $tempItem->user()->associate($this->currentUser);
        $tempItem->save();

        return $tempItem;
    }


    /**
     * sort entries.
     *
     */
    public function sort() {
        foreach (input('items', []) as $sort => $id) {
            $item = TempItems::find($id);
            $item->sortOrder = $sort;
            $item->save();
        }
    }

    /**
     * Create custom entry.
     *
     */
    public function delete($id) {
        $tempItem = TempItems::find($id);

        if ($tempItem) $tempItem->delete();

        return $tempItem;
    }
}
