<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Option;
use Bct\Projects\Repositories\Contractor\ContractorNotesRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Storage;
use JavaScriptContractor;
class ContractorNotesController extends AuthorizedController
{

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The ContractorNotes repository.
	 *
	 * @var \Bct\Projects\Repositories\Contractor\ContractorNotesRepositoryInterface
	 */
	protected $contractor_notes;

	/**
	 * ContractorNotesController constructor.
	 *
	 * @param ContractorNotesRepositoryInterface  $contractor_notes
	 */
	public function __construct(
		ContractorNotesRepositoryInterface $contractor_notes
	) {
		parent::__construct();

		$this->contractor_notes         = $contractor_notes;
	}

	/**
	 * Show the form for creating new ContractorNotes.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}


	/**
	 * Handle posting of the form for creating new ContractorNotes.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}


	/**
	 * Show the form for updating ContractorNotes.
	 *
	 * @param  int $id
	 *
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}


	/**
	 * Handle posting of the form for updating ContractorNotes.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}


	/**
	 * Remove the specified ContractorNotes.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->contractor_notes->delete($id) ? 'success' : 'error';
		$this->alerts->success(trans('bct/projects::contractors/message.notes.'.$type.'.delete'));

		$result = ($type=='success')? true:false;
		return response()->json(['success'=>$result]);	}



	protected function getNotes($id)
	{

		if ( ! $contractor_notes = $this->contractor_notes->find($id)) {

			return response()->json(['success'=>true,'contractor_notes'=>$contractor_notes]);

		}
		else{
			return response()->json(['success'=>false,'message'=>'Notes not found']);
		}

	}


	/**
	 * Processes the form.
	 *
	 * @param  string $mode
	 * @param  int    $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the contractor
		list( $messages, $contractor_notes ) = $this->contractor_notes->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty()) {

			if (!$id) {
				$this->alerts->success(trans('bct/projects::contractors/message.notes.success.create'));
			} else {
				$this->alerts->success(trans('bct/projects::contractors/message.notes.success.update'));
			}

			return response()->json(['status'=>'true']);
		}
		return response()->json(['status'=>'false','messages'=>$messages]);
	}

	public function getAll($contractor_id){
		return $this->contractor_notes->getAllByContractorId($contractor_id);
	}


}