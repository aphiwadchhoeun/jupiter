<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\State;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Repositories\Contractor\ContractorRepositoryInterface;
use Bct\Projects\Repositories\Contractor\ContractorAddressRepositoryInterface;
use Bct\Projects\Repositories\Contractor\ContractorEmailRepositoryInterface;
use Bct\Projects\Repositories\Contractor\ContractorCompanyRepositoryInterface;
use Bct\Projects\Repositories\Contractor\ContractorPhoneRepositoryInterface;
use Bct\Projects\Repositories\Contractor\ContractorLicenseRepositoryInterface;
use Bct\Projects\Repositories\Contractor\ContractorNotesRepositoryInterface;


class ContractorsController extends AuthorizedController {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Projects repository.
     *
     * @var \Bct\Projects\Repositories\Contractor\ContractorRepositoryInterface
     */
    protected $contractors;

    protected $projects;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];


    /**
     * ContractorsController constructor.
     *
     * @param ContractorRepositoryInterface $contractors
     * @param ProjectRepositoryInterface $projects
     * @param ContractorAddressRepositoryInterface $contractor_addresses
     * @param ContractorEmailRepositoryInterface   $contractor_emails
     * @param ContractorCompanyRepositoryInterface $contractor_companies
     * @param ContractorPhoneRepositoryInterface   $contractor_phones
     * @param ContractorNotesRepositoryInterface   $contractor_notes
     * @param ContractorLicenseRepositoryInterface $contractor_licenses
     */
    public function __construct(
        ContractorRepositoryInterface $contractors,
        ProjectRepositoryInterface $projects,
        ContractorAddressRepositoryInterface $contractor_addresses,
        ContractorEmailRepositoryInterface $contractor_emails,
        ContractorCompanyRepositoryInterface $contractor_companies,
        ContractorPhoneRepositoryInterface $contractor_phones,
        ContractorNotesRepositoryInterface $contractor_notes,
        ContractorLicenseRepositoryInterface $contractor_licenses
    ) {
        parent::__construct();

        $this->contractors = $contractors;
        $this->projects = $projects;
	    $this->contractor_addresses = $contractor_addresses;
	    $this->contractor_emails = $contractor_emails;
	    $this->contractor_companies = $contractor_companies;
	    $this->contractor_phones = $contractor_phones;
	    $this->contractor_licenses = $contractor_licenses;
	    $this->contractor_notes = $contractor_notes;
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $user = $this->currentUser;

        return view('bct/projects::contractors/index', compact('user'));
    }

    /**
     * Show the form for creating new contractor.
     *
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function create(Request $request) {
        if ($request->ajax()) {
            return $this->showAjaxForm();
        }

        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new contractor.
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        return $this->processForm($request, 'create');
    }

    /**
     * Show the form for updating contractor.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating contractor.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) {
        return $this->processForm($request, 'update', $id);
    }

    /**
     * Show the details for contractor.
     *
     * @param  int $id
     * @return mixed
     */
    public function details($id) {
        return $this->showDetails($id);
    }

    /**
     * Remove the specified contractor.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->contractors->delete($id, $this->currentUser->id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("bct/projects::contractors/message.{$type}.delete")
        );

        return redirect()->route('bct.projects.contractors.all');
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction() {
        $action = input('action');

        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                if ($action == 'delete') {
                    $this->contractors->{$action}($row, $this->currentUser->id);
                } else {
                    $this->contractors->{$action}($row);
                }
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Datasource for the contractors Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $data = $this->contractors->grid()
            ->select(
                'id',
                'name',
                'city',
                'state',
                'phone',
                'license',
                'is_union',
                'active'
            );

        $columns = [
            'id',
            'name',
            'city',
            'state',
            'phone',
            'license',
            'is_union',
            'active',
            'is_union_text',
            'active_text',
            'is_union_color',
            'active_color',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::contractors/common.title'));
        $filename = input('filename', trans('bct/projects::contractors/common.title'));
        $source = 'Projects / Contractors';
        $settings = [
            'sort'           => 'name',
            'direction'      => 'asc',
            'title'          => $title,
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'export_source'  => $source,
            'export_columns' => [
                'id'    => trans('bct/projects::general/form.id'),
                'name'    => trans('bct/projects::general/form.name'),
                'city'    => trans('bct/projects::general/form.city'),
                'state'   => trans('bct/projects::general/form.state'),
                'phone'   => [trans('bct/projects::general/form.phone'), 'col-md-3'],
                'license' => trans('bct/projects::general/form.license'),
                'union'   => trans('bct/projects::general/form.union'),
                'active'   => trans('bct/projects::general/common.active'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element['name'] = str_limit($element['name'], 35);
            $element->is_union_text = $element->is_union_text;
            $element->active_text = $element->active_text;
            $element->is_union_color = $element->is_union_color;
            $element->active_color = $element->active_color;

            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.contractors.details', $element->id);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['state']] = $element->state;
                $element[get_export_column_name($export_columns['phone'])] = $element->phone;
                $element[$export_columns['license']] = $element->license;
                $element[$export_columns['union']] = $element->is_union_text;
                $element[$export_columns['active']] = $element->active_text;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }


    /**
     * Datasource for the contractors Data Grid.
     *
     * @param $contractor_id
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridProjects($contractor_id) {
        $model = $this->projects->createProjectModel();
        $data = $model
            ->leftJoin('z_project_contractor_join', 'z_project_contractor_join.z_projects_id', '=', $model->getTable() . '.id')
            ->leftJoin('z_project_status', 'z_project_status.id', '=', $model->getTable() . '.z_project_status_id')

            ->leftJoin('z_project_visit as visit1', 'visit1.z_projects_id', '=', 'z_projects.id')
            ->leftJoin('z_project_visit as visit2', function ($join) {
                $join
                    ->on('visit2.z_projects_id', '=', 'z_projects.id')
                    ->on('visit1.id', '<', 'visit2.id');
            })
            ->leftJoin('z_project_visit_stage_join', 'z_project_visit_stage_join.z_project_visit_id', '=', 'visit1.id')
            ->leftJoin('z_project_stage', 'z_project_stage.id', '=', 'z_project_visit_stage_join.z_project_stage_id')

            ->whereNull('visit2.id')
            ->where('z_project_contractor_join.z_contractors_id', $contractor_id)
            ->select(
                $model->getTable() . '.id',
                $model->getTable() . '.name',
                $model->getTable() . '.city',
                $model->getTable() . '.state',
                $model->getTable() . '.created_at',
                'z_project_contractor_join.is_union',
                'z_project_contractor_join.is_agreement',
                'z_project_contractor_join.z_projects_id',
                'z_project_status.name as status'
            )
            ->groupBy($model->getTable() . '.id');

        $columns = [
            'id',
            'name',
            'city',
            'state',
            'status',
            'is_agreement',
            'is_union',
            'z_projects_id',
            'created_at',
            'created',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::contractors/common.title') . "-" . trans('bct/projects::projects/common.title'));
        $filename = input('filename', trans('bct/projects::contractors/common.title') . "-" . trans('bct/projects::projects/common.title'));
        $source = 'Projects / Contractors';
        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'id'    => trans('bct/projects::projects/model.general.id'),
                'name'    => trans('bct/projects::projects/model.general.name'),
                'city'    => trans('bct/projects::projects/model.general.city'),
                'state'   => trans('bct/projects::projects/model.general.state'),
                'created' => trans('bct/projects::projects/model.general.created_at'),
                'status'  => trans('bct/projects::projects/model.general.status_id'),
                'union'   => trans('bct/projects::projects/model.general.union'),
                'pa'      => trans('bct/projects::projects/model.general.pa'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element['created'] = date('M-d-Y', strtotime($element['created_at']));

            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.projects.details', $element['z_projects_id']);
            } else {
                $pa = ($element['is_agreement'] === 1) ? 'Yes' : 'No';
                $created = date_correct_details_projects($element['created_at']);
                $union = ($element['is_union'] === 1) ? 'Yes' : 'No';

                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['state']] = $element->state;
                $element[$export_columns['created']] = $created;
                $element[$export_columns['status']] = $element->status;
                $element[$export_columns['union']] = $union;
                $element[$export_columns['pa']] = $pa;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {
        // Do we have a contractor identifier?
        if (isset($id)) {
            if (!$contractor = $this->contractors->find($id)) {
                $this->alerts->error(trans('bct/projects::contractors/message.not_found', compact('id')));

                return redirect()->route('bct.projects.contractors.all');
            }

            /** check if user can modify contact */
            if ($contractor->created_by != $this->currentUser->id && Sentinel::inRole('repuser')) {
                $this->alerts->error(trans('bct/projects::contractors/permissions.access_error', compact('id')));

                return redirect()->back();
            }
        } else {
            $contractor = $this->contractors->createModel();
        }

	    $states = State::all()->pluck('code');

	    $vars = compact('mode', 'contractor', 'states');

	    if($mode == 'edit'){
		    $addresses = $this->contractor_addresses->getAllByContractorId($id);
		    $phones = $this->contractor_phones->getAllByContractorId($id);
		    $emails = $this->contractor_emails->getAllByContractorId($id);
		    $companies = $this->contractor_companies->getAllByContractorId($id);
		    $licenses  = $this->contractor_licenses->getAllByContractorId($id);
		    $notes  = $this->contractor_notes->getAllByContractorId($id);

		    $vars = compact(
			    'mode',
			    'contractor',
			    'addresses',
			    'phones',
			    'emails',
			    'notes',
			    'companies',
			    'licenses',
			    'states');
	    }

        // Show the page
        return view('bct/projects::contractors.form', $vars);
    }

    /**
     * Shows the popup form.
     *
     * @return mixed
     */
    protected function showAjaxForm() {
        $contractor = $this->contractors->createModel();
        $mode = 'create';

        // Show the page
        return view('bct/projects::contractors.modal', compact('mode', 'contractor'));
    }

    /**
     * Processes the form.
     *
     * @param  Request $request
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm(Request $request, $mode, $id = null) {
        // Store the contractor
        list($messages, $contractor) = $this->contractors->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            /** if ajax form */
            if ($request->ajax()) {
                return ['success' => true];
            } else {
                $this->alerts->success(trans("bct/projects::contractors/message.success.{$mode}"));

                return redirect()->route('bct.projects.contractors.details', $contractor->id);
            }
        }

        $this->alerts->error($messages, 'form');

        /** if ajax form */
        if ($request->ajax()) {
            $request->flash();

            return $this->showAjaxForm();
        }

        return redirect()->back()->withInput();
    }

    /**
     * Shows the details.
     *
     * @param  int $id
     * @return mixed
     */
    protected function showDetails($id = null) {
        // Do we have a contractor identifier?
        if (isset($id) && $contractor = $this->contractors->find($id)) {

            $files = $contractor->files;
            $stages = $this->projects->stages();

	        $addresses = $this->contractor_addresses->getAllByContractorId($id);
	        $phones = $this->contractor_phones->getAllByContractorId($id);
	        $emails = $this->contractor_emails->getAllByContractorId($id);
	        $companies = $this->contractor_companies->getAllByContractorId($id);
	        $licenses  = $this->contractor_licenses->getAllByContractorId($id);
	        $notes  = $this->contractor_notes->getAllByContractorId($id);
            $currentUser = $this->currentUser;

            $has_access = $contractor->created_by == $this->currentUser->id || !Sentinel::inRole('repuser');

            return view('bct/projects::contractors.details', compact('contractor', 'has_access','files', 'stages','addresses','phones','emails','companies','licenses','notes','currentUser'));
        }


        $this->alerts->error(trans('bct/projects::contractors/message.not_found', compact('id')));

        return redirect()->route('bct.projects.contractors.all');
    }

    public function downloadPdfDetails($contractor_id) {
        return $this->contractors->downloadPdfDetails($contractor_id);
    }

    public function mergeAction()
    {
        $stay_contractor_id = input('stay');
        $merge_contractor_id = input('merge');

        $stay_contractor = $this->contractors->find($stay_contractor_id);
        $merge_contractor = $this->contractors->find($merge_contractor_id);

        if ( ! $stay_contractor || ! $merge_contractor) {
            return response()->json(['success' => false, 'message' => 'Contractors do not exist.'], 422);
        }

	    $merge_contractor = $this->contractors->merge($stay_contractor,$merge_contractor);

        if ( ! $merge_contractor) {
            return response()->json(['success' => false, 'message' => 'Something went wrong, please try again.'], 500);
        }

	    $this->alerts->success(trans("bct/projects::contractors/message.success.merge"));

	    return response()->json([ 'success' => true, 'contractor'=>$merge_contractor->name ]);
    }

    public function getContractorsJsonList() {
        $q = input('q');

        $contractors = $this->contractors->grid()
            ->where('name', 'like', '%' . $q . '%')
            ->where('active','=','1')
            ->orderBy('name', 'asc')
            ->limit(100)
            ->get(['name']);

        return response()->json($contractors);
    }


    public function mergeConfirm($target_contractor_id, $removing_contractor_id)
    {
        $target_contractor   = $this->contractors->find($target_contractor_id);
        $removing_contractor = $this->contractors->find($removing_contractor_id);

        if ( ! $target_contractor || ! $removing_contractor) {
            $this->alerts->error(trans('bct/projects::contractors/message.error.not_found'));

            return redirect()->route('bct.projects.contractors.all');
        }

        return view('bct/projects::contractors/merge_confirm', compact('target_contractor', 'removing_contractor'));
    }
}
