<?php namespace Bct\Projects\Controllers\Frontend;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 06.06.16
 * Time: 11:00
 */

use Bct\Projects\Models\ActionType;
use Bct\Projects\Models\Contact;
use Bct\Projects\Models\Contractor;
use Bct\Projects\Models\PrevailingWage;
use Bct\Projects\Models\Project;
use Bct\Projects\Models\ProjectClassification;
use Bct\Projects\Models\ProjectType;
use Bct\Projects\Models\Scope;
use Bct\Projects\Models\Stage;
use Illuminate\Support\Facades\DB;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Platform\Tags\Models\Tag;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Platform\Tags\Widgets\Tags;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 26.10.15
 * Time: 17:11
 */
class ReportController extends BaseController {

    use DispatchesJobs;

    protected $projects;


    /**
     * ReportController constructor.
     *
     * @param ProjectRepositoryInterface $projects
     */
    public function __construct(
        ProjectRepositoryInterface $projects
    ) {
        parent::__construct();

        $this->projects = $projects;
    }

    public function index() {
        return view('bct/projects::reports/index');
    }

    public function report1Projects() {
        $scopes = Scope::distinct()->get(['name']);

        return view('bct/projects::reports/report1', compact('scopes'));
    }

    public function report1ProjectsExport() {
        return $this->projects->getExportReport1Project($this->currentUser);
    }

    public function reportActiveProjectsWithStewards() {
        return view('bct/projects::reports/report_active_projects_with_stewards');
    }

    public function reportActiveProjectsWithStewardsExport() {
        return $this->projects->getReportActiveProjectsWithStewards($this->currentUser);
    }

    public function reportActiveServiceJobs() {
        return view('bct/projects::reports/active_service_jobs');
    }

    public function reportActiveServiceJobsExport() {
        return $this->projects->getExportActiveServiceJobs($this->currentUser);
    }

    public function reportRepSummary(){
        $today = Carbon::now()->toDateTimeString();
        $month_ago = Carbon::now()->subDays(29)->toDateTimeString();
        return view('bct/projects::reports/rep_summary',compact('today','month_ago'));
    }

    public function reportRepSummaryGrid(){

        $data = $this->projects->reportRepSummary()->groupBy('users.id');

        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $show_filters =[];

        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );


        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('project_visits.created_at',[$filter_created_start,$filter_created_end]);
        }


        $columns = [
            'repname',
            'role',
            'projects_created',
            'projects_visited',
            'total_visits',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.rep_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);
        $filename = input('filename', trans('bct/projects::reports/common.tabs.rep_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'repname',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'repname'          => trans('bct/projects::reports/common.col.repname'),
                'role'             => trans('bct/projects::reports/common.col.role'),
                'projects_created' => trans('bct/projects::reports/common.col.projects_created'),
                'projects_visited' => trans('bct/projects::reports/common.col.projects_visited'),
                'total_visits'     => trans('bct/projects::reports/common.col.total_visits')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->role = str_replace('Projects - ','',$element->role);
            if (!input()->get('download', false)) {

            } else {
                $element[$export_columns['repname']]          = $element->repname;
                $element[$export_columns['role']]             = $element->role;
                $element[$export_columns['projects_created']] = $element->projects_created;
                $element[$export_columns['projects_visited']] = $element->projects_visited;
                $element[$export_columns['total_visits']]     = $element->total_visits;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);

    }

    public function reportContractorSummary(){
        $today = Carbon::now()->toDateTimeString();
        $month_ago = Carbon::now()->subDays(29)->toDateTimeString();
        $regions = $this->projects->select(DB::raw('DISTINCT region_abrv'))->orderBy('region_abrv','ASC')->get()->pluck('region_abrv');
        return view('bct/projects::reports/contractor_summary',compact('regions','today','month_ago'));
    }

    public function reportContractorSummaryGrid(){
        $data = $this->projects->reportContractorSummary()->groupBy('z_contractors.id');

        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $is_union = get_input_filter_value_by_key_pp('filters', 'is_union');
        $region = get_input_filter_value_by_key_pp('filters', 'region');
        $filters=[];
        $show_filters =[];


        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if(!is_null($is_union)){
            $is_union_text = $is_union==1?'Yes':'No';
            $show_filters['is_union']=['title'=>'Union','value'=>$is_union_text];
            $data = $data
                ->where('z_contractors.is_union','=',$is_union);
        }

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $filters['region']=$region;
            $data = $data
                ->where('total_projects.region_abrv','=',$region);
        }

        $columns = [
            'contractor_id',
            'contractor_name',
            'is_union',
            'projects_total',
            'projects_active',
            'projects_completed',
            'projects_visits',
            'projects_actions',
            'is_union_text',
            'is_union_color',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);
        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'contractor_name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'contractor_name'    => trans('bct/projects::reports/common.col.contractor_name'),
                'is_union'           => trans('bct/projects::reports/common.col.union'),
                'projects_total'     => trans('bct/projects::reports/common.col.total_projects'),
                'projects_active'    => trans('bct/projects::reports/common.col.active_projects'),
                'projects_completed' => trans('bct/projects::reports/common.col.completed_projects'),
                'projects_visits'    => trans('bct/projects::reports/common.col.projects_visits'),
                'projects_actions'   => trans('bct/projects::reports/common.col.number_of_actions_taken')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            $element->is_union_text = $element->is_union_text;
            $element->is_union_color = $element->is_union_color;

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.contractor_summary.projects',['contractor_id'=>$element->contractor_id]);
                $active_projects_url= route('bct.projects.reports.contractor_summary.projects',['contractor_id'=>$element->contractor_id,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.contractor_summary.projects',['contractor_id'=>$element->contractor_id,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;

            } else {
                $element[$export_columns['contractor_name']] = $element->contractor_name;
                $element[$export_columns['is_union']] = $element->is_union_text;
                $element[$export_columns['projects_total']] = $element->projects_total;
                $element[$export_columns['projects_active']] = $element->projects_active;
                $element[$export_columns['projects_completed']] = $element->projects_completed;
                $element[$export_columns['projects_visits']] = $element->projects_visits;
                $element[$export_columns['projects_actions']] = $element->projects_actions;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }


    public function сontractorSummaryProjects($contractor_id){

        $contractor = Contractor::find($contractor_id);
        $title = 'Contractor Summary: '.$contractor->name.' Projects';
        $page='contractor_summary';
        $grid_url = route('bct.projects.reports.contractor_summary.projects.grid',['contractor_id'=>$contractor_id]);

        $params = Input::query();

        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function contractorSummaryProjectsGrid($contractor_id){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_contractors','z_project_contractor_join.z_contractors_id','=','z_contractors.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->where('z_project_contractor_join.z_contractors_id','=',$contractor_id)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');



        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $region = request()->get('region');
        $status = request()->get('status');
        $show_filters=[];

        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('visit1.created_at',[$filter_created_start,$filter_created_end]);
        }


        if($status){
            $show_filters['status']=['title'=>'Status','value'=>ucfirst($status)];
            $status_ids=['active'=>1,'completed'=>3];
            $data = $data
                ->where('z_projects.z_project_status_id','=',$status_ids[$status]);
        }

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $data = $data
                ->where('z_projects.region_abrv','=',$region);
        }

        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function reportStageSummary(){
        $regions = $this->projects->select(DB::raw('DISTINCT region_abrv'))->orderBy('region_abrv','ASC')->get()->pluck('region_abrv');
        $tags = Tag::all()->sortBy('name');
        $contractors = Contractor::all()->sortBy('name');
        $project_types = ProjectType::all()->sortBy('type');
        $today = Carbon::now()->toDateTimeString();
        $month_ago = Carbon::now()->subDays(29)->toDateTimeString();
        return view('bct/projects::reports/stage_summary',compact('regions','tags','contractors','project_types','today','month_ago'));

    }

    public function reportStageSummaryGrid(){
        $data = $this->projects->reportStageSummary()->groupBy('z_project_stage.id');

        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $filter_region =  get_input_filter_value_by_key_pp('filters', 'region');
        $filter_contractor =  get_input_filter_value_by_key_pp('filters', 'contractor');
        $filter_tag =  get_input_filter_value_by_key_pp('filters', 'tag');
        $filter_project_type =  get_input_filter_value_by_key_pp('filters', 'project_type');
        $filters=[];
        $show_filters =[];

        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($filter_region){
            $filters['region']=$filter_region;
            $show_filters['region']=['title'=>'Region','value'=>$filter_region];
            $data = $data
                ->where('z_projects.region_abrv','=',$filter_region);
        }

        if($filter_contractor){
            $filters['contractor']=$filter_contractor;
            if($contractor = Contractor::find($filter_contractor)){
                $filters['contractor_name']=$contractor->name;
                $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor->name];
            }
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
        }

        if($filter_tag){
            $filters['tag']=$filter_tag;
            if($tag = Tag::find($filter_tag)){
                $filters['tag_name']=$tag->name;
                $show_filters['tag']=['title'=>'Tag','value'=>$tag->name];
            }
            $data = $data
                ->where('tagged.tag_id','=',$filter_tag);
        }

        if($filter_project_type){
            $filters['project_type']=$filter_project_type;
            if($project_type = ProjectType::find($filter_project_type)){
                $filters['project_type_name']=$project_type->type;
                $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type->type];
            }
            $data = $data
                ->where('z_projects.z_project_types_id','=',$filter_project_type);
        }

        $columns = [
            'stage_id',
            'stage_name',
            'projects_total',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.stage_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.stage_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'stage_name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'stage_name'     => trans('bct/projects::reports/common.col.stage_name'),
                'projects_total' => trans('bct/projects::reports/common.col.project_count')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.stage_summary.projects',['stage_id'=>$element->stage_id]);
                $active_projects_url= route('bct.projects.reports.stage_summary.projects',['stage_id'=>$element->stage_id,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.stage_summary.projects',['stage_id'=>$element->stage_id,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;


            } else {
                $element[$export_columns['stage_name']] = $element->stage_name;
                $element[$export_columns['projects_total']] = $element->projects_total;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }


    public function stageSummaryProjects($stage_id){

        $stage = Stage::find($stage_id);
        $title ='Stage Summary: '.$stage->name.' Projects';
        $page='stage_summary';

        $grid_url = route('bct.projects.reports.stage_summary.projects.grid',['stage_id'=>$stage_id]);

        $params = Input::query();

        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function stageSummaryProjectsGrid($stage_id){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->leftJoin("z_project_visit_stage_join","z_project_visit_stage_join.z_project_visit_id","=","z_project_visit.id")
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->where('z_project_visit_stage_join.z_project_stage_id','=',$stage_id)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');

        $region = request()->get('region');
        $contractor = request()->get('contractor');
        $contractor_name = request()->get('contractor_name');
        $tag = request()->get('tag');
        $tag_name = request()->get('tag_name');
        $project_type = request()->get('project_type');
        $project_type_name = request()->get('project_type_name');
        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $show_filters =[];

        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $data = $data
                ->where('z_projects.region_abrv','=',$region);
        }

        if($contractor){
            $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor_name];
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$contractor);
        }

        if($tag){
            $show_filters['tag']=['title'=>'Tag','value'=>$tag_name];
            $data = $data
                ->where('tagged.tag_id','=',$tag);
        }

        if($project_type){
            $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type_name];
            $data = $data
                ->where('z_projects.z_project_types_id','=',$project_type);
        }


        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function reportWageSummary(){
        $regions = $this->projects->select(DB::raw('DISTINCT region_abrv'))->orderBy('region_abrv','ASC')->get()->pluck('region_abrv');
        $tags = Tag::all()->sortBy('name');
        $contractors = Contractor::all()->sortBy('name');
        $project_types = ProjectType::all()->sortBy('type');
        return view('bct/projects::reports/wage_summary',compact('regions','tags','contractors','project_types'));

    }

    public function reportWageSummaryGrid(){

        $query1= PrevailingWage::select(
            "z_prevailing_wage.id as wage_id",
            "z_prevailing_wage.name as wage_type",
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total")
        )
            ->leftJoin("z_projects","z_prevailing_wage.id","=","z_projects.z_prevailing_wage_id")
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })->groupBy('z_prevailing_wage.id');

        $query2= Project::select(
            DB::raw("0 as wage_id"),
            DB::raw("'Unknown' as wage_type"),
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total")
        )
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->where('z_projects.z_prevailing_wage_id','=',0);


        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $filter_region =  get_input_filter_value_by_key_pp('filters', 'region');
        $filter_contractor =  get_input_filter_value_by_key_pp('filters', 'contractor');
        $filter_tag =  get_input_filter_value_by_key_pp('filters', 'tag');
        $filter_project_type =  get_input_filter_value_by_key_pp('filters', 'project_type');
        $filters=[];
        $show_filters =[];


        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $query1 = $query1
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
            $query2 = $query2
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);

        }

        if($filter_region){
            $filters['region']=$filter_region;
            $show_filters['region']=['title'=>'Region','value'=>$filter_region];
            $query1 = $query1
                ->where('z_projects.region_abrv','=',$filter_region);
            $query2 = $query2
                ->where('z_projects.region_abrv','=',$filter_region);
        }

        if($filter_contractor){
            $filters['contractor']=$filter_contractor;
            if($contractor = Contractor::find($filter_contractor)){
                $filters['contractor_name']=$contractor->name;
                $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor->name];
            }
            $query1 = $query1
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
            $query2 = $query2
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
        }

        if($filter_tag){
            $filters['tag']=$filter_tag;
            if($tag = Tag::find($filter_tag)){
                $filters['tag_name']=$tag->name;
                $show_filters['tag']=['title'=>'Tag','value'=>$tag->name];
            }
            $query1 = $query1
                ->where('tagged.tag_id','=',$filter_tag);
            $query2 = $query2
                ->where('tagged.tag_id','=',$filter_tag);
        }

        if($filter_project_type){
            $filters['project_type']=$filter_project_type;
            if($project_type = ProjectType::find($filter_project_type)){
                $filters['project_type_name']=$project_type->type;
                $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type->type];

            }
            $query1 = $query1
                ->where('z_projects.z_project_types_id','=',$filter_project_type);
            $query2 = $query2
                ->where('z_projects.z_project_types_id','=',$filter_project_type);
        }
        $data = $query1->union($query2);

        $columns = [
            'wage_id',
            'wage_type',
            'projects_total',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.wage_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.wage_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'wage_type',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'wage_type'      => trans('bct/projects::reports/common.col.wage_type'),
                'projects_total' => trans('bct/projects::reports/common.col.project_count')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.wage_summary.projects',['wage_id'=>$element->wage_id]);
                $active_projects_url= route('bct.projects.reports.wage_summary.projects',['wage_id'=>$element->wage_id,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.wage_summary.projects',['wage_id'=>$element->wage_id,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;

            } else {
                $element[$export_columns['wage_type']] = $element->wage_type;
                $element[$export_columns['projects_total']] = $element->projects_total;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function wageSummaryProjects($wage_id){

        if($wage_id==0){
            $wage_name='Unknown';
        }
        else{
            $wage = PrevailingWage::find($wage_id);
            $wage_name = $wage->name;

        }
        $title = 'Wage Summary: '.$wage_name.' Projects';
        $page='wage_summary';
        $grid_url = route('bct.projects.reports.wage_summary.projects.grid',['wage_id'=>$wage_id]);


        $params = Input::query();

        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function wageSummaryProjectsGrid($wage_id){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->where('z_projects.z_prevailing_wage_id','=',$wage_id)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');

        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $region = request()->get('region');
        $contractor = request()->get('contractor');
        $contractor_name = request()->get('contractor_name');
        $tag = request()->get('tag');
        $tag_name = request()->get('tag_name');
        $project_type = request()->get('project_type');
        $project_type_name = request()->get('project_type_name');
        $show_filters =[];

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $data = $data
                ->where('z_projects.region_abrv','=',$region);
        }

        if($contractor){
            $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor_name];
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$contractor);
        }

        if($tag){
            $show_filters['tag']=['title'=>'Tag','value'=>$tag_name];
            $data = $data
                ->where('tagged.tag_id','=',$tag);
        }

        if($project_type){
            $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type_name];
            $data = $data
                ->where('z_projects.z_project_types_id','=',$project_type);
        }
        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }


        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function reportContactsSummary(){
        $regions = $this->projects->select(DB::raw('DISTINCT region_abrv'))->orderBy('region_abrv','ASC')->get()->pluck('region_abrv');

        return view('bct/projects::reports/contacts_summary',compact('regions'));

    }

    public function reportContactsSummaryGrid(){
        $data = $this->projects->reportContactsSummary()->groupBy("z_contact.id");

        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $filter_region =  get_input_filter_value_by_key_pp('filters', 'region');
        $filters=[];
        $show_filters=[];

        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($filter_region){
            $filters['region']=$filter_region;
            $show_filters['region']=['title'=>'Region','value'=>$filter_region];
            $data = $data
                ->where('total_projects.region_abrv','=',$filter_region);
        }

        $columns = [
            'contact_id',
            'contact_name',
            'contact_company',
            'projects_total',
            'projects_active',
            'projects_completed',
            'projects_visits',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contact_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contact_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'contact_name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'contact_name'       => trans('bct/projects::reports/common.col.contact_name'),
                'contact_company'    => trans('bct/projects::reports/common.col.contact_company'),
                'projects_total'     => trans('bct/projects::reports/common.col.total_projects'),
                'projects_active'    => trans('bct/projects::reports/common.col.active_projects'),
                'projects_completed' => trans('bct/projects::reports/common.col.completed_projects'),
                'projects_visits'    => trans('bct/projects::reports/common.col.projects_visits'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.contacts_summary.projects',['contact_id'=>$element->contact_id]);
                $active_projects_url= route('bct.projects.reports.contacts_summary.projects',['contact_id'=>$element->contact_id,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.contacts_summary.projects',['contact_id'=>$element->contact_id,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;

            } else {
                $element[$export_columns['contact_name']] = $element->contact_name;
                $element[$export_columns['contact_company']] = $element->contact_company;
                $element[$export_columns['projects_total']] = $element->projects_total;
                $element[$export_columns['projects_active']] = $element->projects_active;
                $element[$export_columns['projects_completed']] = $element->projects_completed;
                $element[$export_columns['projects_visits']] = $element->projects_visits;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }


    public function contactsSummaryProjects($contact_id){

        $contact = Contact::find($contact_id);
        $page='contacts_summary';
        $title = 'Contacts Summary: '.$contact->first_name.' '.$contact->last_name.' Projects';
        $grid_url = route('bct.projects.reports.contacts_summary.projects.grid',['contact_id'=>$contact_id]);

        $params = Input::query();

        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function contactsSummaryProjectsGrid($contact_id){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contact_join','z_project_contact_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id','=','z_projects.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->where('z_project_contact_join.z_contact_id','=',$contact_id)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');



        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $region = request()->get('region');
        $status = request()->get('status');
        $show_filters =[];

        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($status){
            $show_filters['status']=['title'=>'Status','value'=>ucfirst($status)];
            $status_ids=['active'=>1,'completed'=>3];
            $data = $data
                ->where('z_projects.z_project_status_id','=',$status_ids[$status]);
        }

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $data = $data
                ->where('z_projects.region_abrv','=',$region);
        }


        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function reportProjectClassificationSummary(){
        $regions = $this->projects->select(DB::raw('DISTINCT region_abrv'))->orderBy('region_abrv','ASC')->get()->pluck('region_abrv');
        $tags = Tag::all()->sortBy('name');
        $contractors = Contractor::all()->sortBy('name');
        $project_types = ProjectType::all()->sortBy('type');
        return view('bct/projects::reports/project_classification_summary',compact('regions','tags','contractors','project_types'));

    }

    public function reportProjectClassificationSummaryGrid(){

        $query1 = ProjectClassification::select(
            "z_project_classification.id as classification_id",
            "z_project_classification.name as classification_name",
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits"),
            DB::raw("COUNT( DISTINCT z_project_contractor_join.z_contractors_id ) as projects_contractors"),
            DB::raw("COUNT( DISTINCT z_project_contact_join.z_contact_id ) as projects_contacts"),
            DB::raw("COUNT( DISTINCT z_project_action.id ) as projects_actions")
        )
            ->leftJoin("z_projects","z_project_classification.id","=","z_projects.z_project_classification_id")
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_contact_join','z_project_contact_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_action','z_project_action.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })->groupBy('z_project_classification.id');


        $query2=Project::select(
            DB::raw("0 as classification_id"),
            DB::raw("'Unknown' as classification_name"),
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits"),
            DB::raw("COUNT( DISTINCT z_project_contractor_join.z_contractors_id ) as projects_contractors"),
            DB::raw("COUNT( DISTINCT z_project_contact_join.z_contact_id ) as projects_contacts"),
            DB::raw("COUNT( DISTINCT z_project_action.id ) as projects_actions")
        )
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_contact_join','z_project_contact_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_action','z_project_action.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })->where('z_projects.z_project_classification_id','=',0);
        
        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $filter_region =  get_input_filter_value_by_key_pp('filters', 'region');
        $filter_contractor =  get_input_filter_value_by_key_pp('filters', 'contractor');
        $filter_tag =  get_input_filter_value_by_key_pp('filters', 'tag');
        $filter_project_type =  get_input_filter_value_by_key_pp('filters', 'project_type');
        $filters=[];
        $show_filters=[];

        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $query1 = $query1
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
            $query2 = $query2
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($filter_region){
            $filters['region']=$filter_region;
            $show_filters['region']=['title'=>'Region','value'=>$filter_region];
            $query1 = $query1
                ->where('z_projects.region_abrv','=',$filter_region);
            $query2 = $query2
                ->where('z_projects.region_abrv','=',$filter_region);
        }

        if($filter_contractor){
            $filters['contractor']=$filter_contractor;
            if($contractor = Contractor::find($filter_contractor)){
                $filters['contractor_name']=$contractor->name;
                $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor->name];

            }
            $query1 = $query1
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
            $query2 = $query2
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
        }

        if($filter_tag){
            $filters['tag']=$filter_tag;
            if($tag = Tag::find($filter_tag)){
                $filters['tag_name']=$tag->name;
                $show_filters['tag']=['title'=>'Tag','value'=>$tag->name];
            }
            $query1 = $query1
                ->where('tagged.tag_id','=',$filter_tag);
            $query2 = $query2
                ->where('tagged.tag_id','=',$filter_tag);
        }

        if($filter_project_type){
            $filters['project_type']=$filter_project_type;
            if($project_type = ProjectType::find($filter_project_type)){
                $filters['project_type_name']=$project_type->type;
                $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type->type];
            }
            $query1 = $query1
                ->where('z_projects.z_project_types_id','=',$filter_project_type);
            $query2 = $query2
                ->where('z_projects.z_project_types_id','=',$filter_project_type);
        }

        $data = $query1->union($query2);

        $columns = [
            'classification_id',
            'classification_name',
            'projects_total',
            'projects_active',
            'projects_completed',
            'projects_visits',
            'projects_contacts',
            'projects_contractors',
            'projects_actions',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.project_classification_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.project_classification_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'classification_name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'classification_name'  => trans('bct/projects::reports/common.col.classification_name'),
                'projects_total'       => trans('bct/projects::reports/common.col.project_count'),
                'projects_active'      => trans('bct/projects::reports/common.col.active_count'),
                'projects_completed'   => trans('bct/projects::reports/common.col.completed_count'),
                'projects_visits'      => trans('bct/projects::reports/common.col.number_of_visits'),
                'projects_contractors' => trans('bct/projects::reports/common.col.number_of_contractors'),
                'projects_contacts'    => trans('bct/projects::reports/common.col.number_of_contacts'),
                'projects_actions'     => trans('bct/projects::reports/common.col.number_of_actions'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.project_classification_summary.projects',['classification_id'=>$element->classification_id]);
                $active_projects_url= route('bct.projects.reports.project_classification_summary.projects',['classification_id'=>$element->classification_id,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.project_classification_summary.projects',['classification_id'=>$element->classification_id,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;


            } else {
                $element[$export_columns['classification_name']] = $element->classification_name;
                $element[$export_columns['projects_total']] = $element->projects_total;
                $element[$export_columns['projects_active']] = $element->projects_active;
                $element[$export_columns['projects_completed']] = $element->projects_completed;
                $element[$export_columns['projects_visits']] = $element->projects_visits;
                $element[$export_columns['projects_contractors']] = $element->projects_contractors;
                $element[$export_columns['projects_actions']] = $element->projects_actions;
                $element[$export_columns['projects_contacts']] = $element->projects_contacts;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }



    public function projectClassificationSummaryProjects($classification_id){

        if($classification_id==0){
            $classification_name='Unknown';
        }
        else{
            $classification = ProjectClassification::find($classification_id);
            $classification_name = $classification->name;

        }

        $title = 'Project Classification Summary: '.$classification_name.' Projects';
        $page='project_classification_summary';
        $grid_url = route('bct.projects.reports.project_classification_summary.projects.grid',['classification_id'=>$classification_id]);

        $params = Input::query();


        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function projectClassificationSummaryProjectsGrid($classification_id){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id','=','z_projects.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->leftJoin("z_project_visit_stage_join","z_project_visit_stage_join.z_project_visit_id","=","visit1.id")
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->where('z_projects.z_project_classification_id','=',$classification_id)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');



        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $region = request()->get('region');
        $contractor = request()->get('contractor');
        $contractor_name = request()->get('contractor_name');
        $tag = request()->get('tag');
        $tag_name = request()->get('tag_name');
        $project_type = request()->get('project_type');
        $project_type_name = request()->get('project_type_name');
        $status = request()->get('status');
        $show_filters =[];



        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $data = $data
                ->where('z_projects.region_abrv','=',$region);
        }

        if($contractor){
            $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor_name];
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$contractor);
        }

        if($tag){
            $show_filters['tag']=['title'=>'Tag','value'=>$tag_name];
            $data = $data
                ->where('tagged.tag_id','=',$tag);
        }

        if($project_type){
            $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type_name];
            $data = $data
                ->where('z_projects.z_project_types_id','=',$project_type);
        }
        if($status){
            $show_filters['status']=['title'=>'Status','value'=>ucfirst($status)];
            $status_ids=['active'=>1,'completed'=>3];
            $data = $data
                ->where('z_projects.z_project_status_id','=',$status_ids[$status]);
        }


        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function reportProjectRegionSummary(){
        $tags = Tag::all()->sortBy('name');
        $contractors = Contractor::all()->sortBy('name');
        $project_types = ProjectType::all()->sortBy('type');
        $project_classifications = ProjectClassification::all()->sortBy('name');
        return view('bct/projects::reports/project_region_summary',compact('tags','contractors','project_types','project_classifications'));

    }

    public function reportProjectRegionSummaryGrid(){
        $data = $this->projects->reportProjectRegionSummary()->groupBy("z_projects.region_abrv");

        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $filter_contractor =  get_input_filter_value_by_key_pp('filters', 'contractor');
        $filter_tag =  get_input_filter_value_by_key_pp('filters', 'tag');
        $filter_project_type =  get_input_filter_value_by_key_pp('filters', 'project_type');
        $filter_project_classification =  get_input_filter_value_by_key_pp('filters', 'classification');
        $filters=[];
        $show_filters=[];

        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($filter_contractor){
            $filters['contractor']=$filter_contractor;
            if($contractor = Contractor::find($filter_contractor)){
                $filters['contractor_name']=$contractor->name;
                $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor->name];
            }
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
        }

        if($filter_tag){
            $filters['tag']=$filter_tag;
            if($tag = Tag::find($filter_tag)){
                $filters['tag_name']=$tag->name;
                $show_filters['tag']=['title'=>'Tag','value'=>$tag->name];
            }
            $data = $data
                ->where('tagged.tag_id','=',$filter_tag);
        }

        if($filter_project_type){
            $filters['project_type']=$filter_project_type;
            if($project_type = ProjectType::find($filter_project_type)){
                $filters['project_type_name']=$project_type->type;
                $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type->type];
            }
            $data = $data
                ->where('z_projects.z_project_types_id','=',$filter_project_type);
        }
        if($filter_project_classification){
            $filters['classification']=$filter_project_classification;
            if($classification = ProjectClassification::find($filter_project_classification)){
                $filters['classification_name']=$classification->name;
                $show_filters['classification']=['title'=>'Classification','value'=>$classification->name];

            }
            $data = $data
                ->where('z_projects.z_project_classification_id','=',$filter_project_classification);
        }

        $columns = [
            'region',
            'projects_total',
            'projects_active',
            'projects_completed',
            'projects_visits',
            'projects_contacts',
            'projects_contractors',
            'projects_actions',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.project_region_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.project_region_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'region',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'region'               => trans('bct/projects::reports/common.col.region'),
                'projects_total'       => trans('bct/projects::reports/common.col.project_count'),
                'projects_active'      => trans('bct/projects::reports/common.col.active_count'),
                'projects_completed'   => trans('bct/projects::reports/common.col.completed_count'),
                'projects_visits'      => trans('bct/projects::reports/common.col.number_of_visits'),
                'projects_contacts'    => trans('bct/projects::reports/common.col.number_of_contacts'),
                'projects_contractors' => trans('bct/projects::reports/common.col.number_of_contractors'),
                'projects_actions'     => trans('bct/projects::reports/common.col.number_of_actions'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.project_region_summary.projects',['region'=>$element->region]);
                $active_projects_url= route('bct.projects.reports.project_region_summary.projects',['region'=>$element->region,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.project_region_summary.projects',['region'=>$element->region,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;

            } else {
                $element[$export_columns['region']] = $element->region;
                $element[$export_columns['projects_total']] = $element->projects_total;
                $element[$export_columns['projects_active']] = $element->projects_active;
                $element[$export_columns['projects_completed']] = $element->projects_completed;
                $element[$export_columns['projects_visits']] = $element->projects_visits;
                $element[$export_columns['projects_contractors']] = $element->projects_contractors;
                $element[$export_columns['projects_actions']] = $element->projects_actions;
                $element[$export_columns['projects_contacts']] = $element->projects_contacts;

            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }


    public function projectRegionSummaryProjects($region){

        $title = 'Project Region Summary: '.$region.' Projects';
        $grid_url = route('bct.projects.reports.project_region_summary.projects.grid',['region'=>$region]);
        $page='project_region_summary';
        $params = Input::query();


        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function projectRegionSummaryProjectsGrid($region){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id','=','z_projects.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->leftJoin("z_project_visit_stage_join","z_project_visit_stage_join.z_project_visit_id","=","visit1.id")
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->where('z_projects.region_abrv','=',$region)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');



        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $classification = request()->get('classification');
        $classification_name = request()->get('classification_name');
        $contractor = request()->get('contractor');
        $contractor_name = request()->get('contractor_name');
        $tag = request()->get('tag');
        $tag_name = request()->get('tag_name');
        $project_type = request()->get('project_type');
        $project_type_name = request()->get('project_type_name');
        $status = request()->get('status');
        $show_filters =[];


        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($classification){
            $show_filters['classification']=['title'=>'Classification','value'=>$classification_name];
            $data = $data
                ->where('z_projects.z_project_classification_id','=',$classification);
        }

        if($contractor){
            $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor_name];
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$contractor);
        }

        if($tag){
            $show_filters['tag']=['title'=>'Tag','value'=>$tag_name];
            $data = $data
                ->where('tagged.tag_id','=',$tag);
        }

        if($project_type){
            $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type_name];
            $data = $data
                ->where('z_projects.z_project_types_id','=',$project_type);
        }
        if($status){
            $show_filters['status']=['title'=>'Status','value'=>ucfirst($status)];
            $status_ids=['active'=>1,'completed'=>3];
            $data = $data
                ->where('z_projects.z_project_status_id','=',$status_ids[$status]);
        }


        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);
        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function reportProjectTypeSummary(){
        $tags = Tag::all()->sortBy('name');
        $contractors = Contractor::all()->sortBy('name');
        $regions = $this->projects->select(DB::raw('DISTINCT region_abrv'))->orderBy('region_abrv','ASC')->get()->pluck('region_abrv');
        $project_classifications = ProjectClassification::all()->sortBy('name');
        return view('bct/projects::reports/project_type_summary',compact('tags','contractors','project_types','regions','project_classifications'));


    }

    public function reportProjectTypeSummaryGrid(){

        $query1 = ProjectType::select(
            "z_project_types.id as project_type_id",
            "z_project_types.type as type",
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits"),
            DB::raw("COUNT( DISTINCT z_project_contractor_join.z_contractors_id ) as projects_contractors"),
            DB::raw("COUNT( DISTINCT z_project_contact_join.z_contact_id ) as projects_contacts"),
            DB::raw("COUNT( DISTINCT z_project_action.id ) as projects_actions")
        )
            ->leftJoin("z_projects","z_project_types.id","=","z_projects.z_project_types_id")
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_contact_join','z_project_contact_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_action','z_project_action.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })->groupBy('z_project_types.type');

        $query2 = Project::select(
            DB::raw("0 as project_type_id"),
            DB::raw("'Unknown' as type"),
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits"),
            DB::raw("COUNT( DISTINCT z_project_contractor_join.z_contractors_id ) as projects_contractors"),
            DB::raw("COUNT( DISTINCT z_project_contact_join.z_contact_id ) as projects_contacts"),
            DB::raw("COUNT( DISTINCT z_project_action.id ) as projects_actions")
        )
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_contact_join','z_project_contact_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_action','z_project_action.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })->where('z_projects.z_project_types_id','=',0);

        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $filter_contractor =  get_input_filter_value_by_key_pp('filters', 'contractor');
        $filter_tag =  get_input_filter_value_by_key_pp('filters', 'tag');
        $filter_region =  get_input_filter_value_by_key_pp('filters', 'region');
        $filter_project_classification =  get_input_filter_value_by_key_pp('filters', 'classification');
        $filters=[];
        $show_filters=[];
        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $query1 = $query1
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
            $query2 = $query2
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($filter_contractor){
            $filters['contractor']=$filter_contractor;
            if($contractor = Contractor::find($filter_contractor)){
                $filters['contractor_name']=$contractor->name;
                $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor->name];
            }
            $query1 = $query1
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
            $query2 = $query2
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
        }

        if($filter_tag){
            $filters['tag']=$filter_tag;
            if($tag = Tag::find($filter_tag)){
                $filters['tag_name']=$tag->name;
                $show_filters['tag']=['title'=>'Tag','value'=>$tag->name];
            }
            $query1 = $query1
                ->where('tagged.tag_id','=',$filter_tag);
            $query2 = $query2
                ->where('tagged.tag_id','=',$filter_tag);
        }

        if($filter_region){
            $filters['region']=$filter_region;
            $show_filters['region']=['title'=>'Region','value'=>$filter_region];
            $query1 = $query1
                ->where('z_projects.region_abrv','=',$filter_region);
            $query2 = $query2
                ->where('z_projects.region_abrv','=',$filter_region);
        }

        if($filter_project_classification){
            $filters['classification']=$filter_project_classification;
            if($classification = ProjectClassification::find($filter_project_classification)){
                $filters['classification_name']=$classification->name;
                $show_filters['classification']=['title'=>'Classification','value'=>$classification->name];
            }
            $query1 = $query1
                ->where('z_projects.z_project_classification_id','=',$filter_project_classification);
            $query2 = $query2
                ->where('z_projects.z_project_classification_id','=',$filter_project_classification);
        }

        $data = $query1->union($query2);
        $columns = [
            'project_type_id',
            'type',
            'projects_total',
            'projects_active',
            'projects_completed',
            'projects_visits',
            'projects_contacts',
            'projects_contractors',
            'projects_actions',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.project_type_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.project_type_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'type',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'type'                 => trans('bct/projects::reports/common.col.type_name'),
                'projects_total'       => trans('bct/projects::reports/common.col.project_count'),
                'projects_active'      => trans('bct/projects::reports/common.col.active_count'),
                'projects_completed'   => trans('bct/projects::reports/common.col.completed_count'),
                'projects_visits'      => trans('bct/projects::reports/common.col.number_of_visits'),
                'projects_contacts'    => trans('bct/projects::reports/common.col.number_of_contacts'),
                'projects_contractors' => trans('bct/projects::reports/common.col.number_of_contractors'),
                'projects_actions'     => trans('bct/projects::reports/common.col.number_of_actions'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.project_type_summary.projects',['project_type'=>$element->project_type_id]);
                $active_projects_url= route('bct.projects.reports.project_type_summary.projects',['project_type'=>$element->project_type_id,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.project_type_summary.projects',['project_type'=>$element->project_type_id,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;


            } else {
                $element[$export_columns['type']] = $element->type;
                $element[$export_columns['projects_total']] = $element->projects_total;
                $element[$export_columns['projects_active']] = $element->projects_active;
                $element[$export_columns['projects_completed']] = $element->projects_completed;
                $element[$export_columns['projects_visits']] = $element->projects_visits;
                $element[$export_columns['projects_contractors']] = $element->projects_contractors;
                $element[$export_columns['projects_actions']] = $element->projects_actions;
                $element[$export_columns['projects_contacts']] = $element->projects_contacts;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function projectTypeSummaryProjects($project_type_id){
        if($project_type_id==0){
            $type='Unknown';
        }
        else{
            $project_type = ProjectType::find($project_type_id);
            $type = $project_type->name;

        }
        $title = 'Project Type Summary: '.$type.' Projects';
        $page='project_type_summary';

        $grid_url = route('bct.projects.reports.project_type_summary.projects.grid',['project_type_id'=>$project_type_id]);

        $params = Input::query();

        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function projectTypeSummaryProjectsGrid($project_type_id){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id','=','z_projects.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->leftJoin("z_project_visit_stage_join","z_project_visit_stage_join.z_project_visit_id","=","visit1.id")
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->where('z_projects.z_project_types_id','=',$project_type_id)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');



        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $classification = request()->get('classification');
        $classification_name = request()->get('classification_name');
        $contractor = request()->get('contractor');
        $contractor_name = request()->get('contractor_name');
        $tag = request()->get('tag');
        $tag_name = request()->get('tag_name');
        $region = request()->get('region');
        $status = request()->get('status');
        $show_filters =[];


        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($classification){
            $show_filters['classification']=['title'=>'Classification','value'=>$classification_name];
            $data = $data
                ->where('z_projects.z_project_classification_id','=',$classification);
        }

        if($contractor){
            $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor_name];
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$contractor);
        }

        if($tag){
            $show_filters['tag']=['title'=>'Tag','value'=>$tag_name];
            $data = $data
                ->where('tagged.tag_id','=',$tag);
        }

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $data = $data
                ->where('z_projects.region_abrv','=',$region);
        }
        if($status){
            $show_filters['status']=['title'=>'Status','value'=>ucfirst($status)];
            $status_ids=['active'=>1,'completed'=>3];
            $data = $data
                ->where('z_projects.z_project_status_id','=',$status_ids[$status]);
        }


        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function reportActionsSummary(){
        $regions = $this->projects->select(DB::raw('DISTINCT region_abrv'))->orderBy('region_abrv','ASC')->get()->pluck('region_abrv');
        $tags = Tag::all()->sortBy('name');
        $contractors = Contractor::all()->sortBy('name');
        $project_types = ProjectType::all()->sortBy('type');
        $project_classifications = ProjectClassification::all()->sortBy('name');
        return view('bct/projects::reports/actions_summary',compact('regions','tags','contractors','project_types','project_classifications'));

    }

    public function reportActionsSummaryGrid(){
        $data = $this->projects->reportActionsSummary()->groupBy('z_action_type.name');

        list($filter_created_start, $filter_created_end) = get_input_filter_value_by_key_pp('filters', 'z_project_visit.created_at', true);
        $filter_contractor =  get_input_filter_value_by_key_pp('filters', 'contractor');
        $filter_tag =  get_input_filter_value_by_key_pp('filters', 'tag');
        $filter_region =  get_input_filter_value_by_key_pp('filters', 'region');
        $filter_project_classification =  get_input_filter_value_by_key_pp('filters', 'classification');
        $filter_project_type =  get_input_filter_value_by_key_pp('filters', 'project_type');
        $filters=[];
        $show_filters =[];

        input()->replace(
            collect(input()->all())
                ->except('filters')
                ->toArray()
        );

        if ($filter_created_start && $filter_created_end) {
            $filters['start']=$filter_created_start;
            $filters['end']=$filter_created_end;
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($filter_contractor){
            $filters['contractor']=$filter_contractor;
            if($contractor = Contractor::find($filter_contractor)){
                $filters['contractor_name']=$contractor->name;
                $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor->name];
            }
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$filter_contractor);
        }

        if($filter_tag){
            $filters['tag']=$filter_tag;
            if($tag = Tag::find($filter_tag)){
                $filters['tag_name']=$tag->name;
                $show_filters['tag']=['title'=>'Tag','value'=>$tag->name];
            }
            $data = $data
                ->where('tagged.tag_id','=',$filter_tag);
        }

        if($filter_region){
            $filters['region']=$filter_region;
            $show_filters['region']=['title'=>'Region','value'=>$filter_region];
            $data = $data
                ->where('total_projects.region_abrv','=',$filter_region);
        }

        if($filter_project_classification){
            $filters['classification']=$filter_project_classification;
            if($classification = ProjectClassification::find($filter_project_classification)){
                $filters['classification_name']=$classification->name;
                $show_filters['classification']=['title'=>'Classification','value'=>$classification->name];
            }
            $data = $data
                ->where('total_projects.z_project_classification_id','=',$filter_project_classification);
        }

        if($filter_project_type){
            $filters['project_type']=$filter_project_type;
            if($project_type = ProjectType::find($filter_project_type)){
                $filters['project_type_name']=$project_type->type;
                $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type->type];

            }
            $data = $data
                ->where('total_projects.z_project_types_id','=',$filter_project_type);
        }


        $columns = [
            'action_type_id',
            'action_type',
            'projects_total',
            'projects_active',
            'projects_completed',
            'projects_visits',
            'projects_contractors',
            'all_projects_url',
            'active_projects_url',
            'completed_projects_url'
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.actions_summary'));
        $subtitle = $this->addFiltersToTitle($show_filters);
        $filename = input('filename', trans('bct/projects::reports/common.tabs.actions_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'action_type',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'action_type'          => trans('bct/projects::reports/common.col.action_type'),
                'projects_total'       => trans('bct/projects::reports/common.col.project_count'),
                'projects_active'      => trans('bct/projects::reports/common.col.active_count'),
                'projects_completed'   => trans('bct/projects::reports/common.col.completed_count'),
                'projects_visits'      => trans('bct/projects::reports/common.col.number_of_visits'),
                'projects_contractors' => trans('bct/projects::reports/common.col.number_of_contractors')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns,$filters) {

            if (!input()->get('download', false)) {
                $all_projects_url= route('bct.projects.reports.actions_summary.projects',['action_type'=>$element->action_type_id]);
                $active_projects_url= route('bct.projects.reports.actions_summary.projects',['action_type'=>$element->action_type_id,'status'=>'active']);
                $completed_projects_url= route('bct.projects.reports.actions_summary.projects',['action_type'=>$element->action_type_id,'status'=>'completed']);
                if(count($filters)>0) {
                    $all_projects_url .= '?';
                    $active_projects_url .= '&';
                    $completed_projects_url .= '&';
                    $filters_count = count($filters);
                    $i=1;
                    foreach ($filters as $key => $value) {
                        $value = urlencode($value);
                        $all_projects_url .= $key.'='.$value;
                        $active_projects_url .= $key.'='.$value;
                        $completed_projects_url .= $key.'='.$value;

                        if($filters_count!==$i){
                            $all_projects_url .= '&';
                            $active_projects_url .= '&';
                            $completed_projects_url .= '&';
                        }
                        $i++;
                    }
                }
                $element->all_projects_url = $all_projects_url;
                $element->active_projects_url = $active_projects_url;
                $element->completed_projects_url = $completed_projects_url;


            } else {
                $element[$export_columns['action_type']] = $element->action_type;
                $element[$export_columns['projects_total']] = $element->projects_total;
                $element[$export_columns['projects_active']] = $element->projects_active;
                $element[$export_columns['projects_completed']] = $element->projects_completed;
                $element[$export_columns['projects_visits']] = $element->projects_visits;
                $element[$export_columns['projects_contractors']] = $element->projects_contractors;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function actionsSummaryProjects($action_type_id){
        $action_type = ActionType::find($action_type_id);
        $title = 'Actions Summary: '.$action_type->name.' Projects';
        $page = 'actions_summary';
        $grid_url = route('bct.projects.reports.actions_summary.projects.grid',['action_type_id'=>$action_type_id]);

        $params = Input::query();

        $params_count = count($params);
        $i=0;
        if(count($params)>0) {
            $grid_url .= '?';
            foreach ($params as $key => $value) {
                $grid_url .= $key.'='.urlencode($value);
                if($params_count!==$i){
                    $grid_url .= '&';
                }
                $i++;
            }
        }

        return view('bct/projects::reports/details',compact('title','grid_url','page'));
    }

    public function actionsSummaryProjectsGrid($action_type_id){
        $data = Project::select(
            "z_projects.id as project_id",
            "z_projects.name as project_name",
            "visit1.created_at as last_visit"
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id','=','z_projects.id')
            ->leftJoin('z_project_visit as visit1', function($join){
                $join
                    ->on('visit1.z_projects_id',"=",'z_projects.id')
                    ->on('visit1.details', '!=', DB::raw("'Project was created!'"));
            })
            ->leftJoin('z_project_visit as visit2', function($join){
                $join
                    ->on('visit2.z_projects_id',"=",'z_projects.id')
                    ->on('visit2.details', '!=', DB::raw("'Project was created!'"))
                    ->on('visit2.id', '>', 'visit1.id');
            })
            ->leftJoin("z_project_visit_stage_join","z_project_visit_stage_join.z_project_visit_id","=","visit1.id")
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->leftJoin('z_project_action','z_project_action.z_projects_id','=','z_projects.id')
            ->where('z_project_action.z_action_type_id','=',$action_type_id)
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');


        $filter_created_start = request()->get('start');
        $filter_created_end = request()->get('end');
        $classification = request()->get('classification');
        $classification_name = request()->get('classification_name');
        $contractor = request()->get('contractor');
        $contractor_name = request()->get('contractor_name');
        $tag = request()->get('tag');
        $tag_name = request()->get('tag_name');
        $project_type = request()->get('project_type');
        $project_type_name = request()->get('project_type_name');
        $status = request()->get('status');
        $region = request()->get('region');
        $show_filters =[];


        if ($filter_created_start && $filter_created_end) {
            $show_filters['visit_date']=['title'=>'Visit Date','value'=>Carbon::parse($filter_created_start)->format('M d, Y').' - '.Carbon::parse($filter_created_end)->format('M d, Y')];
            $data = $data
                ->whereBetween('z_project_visit.created_at',[$filter_created_start,$filter_created_end]);
        }

        if($classification){
            $show_filters['classification']=['title'=>'Classification','value'=>$classification_name];
            $data = $data
                ->where('z_projects.z_project_classification_id','=',$classification);
        }

        if($status){
            $show_filters['status']=['title'=>'Status','value'=>ucfirst($status)];
            $status_ids=['active'=>1,'completed'=>3];
            $data = $data
                ->where('z_projects.z_project_status_id','=',$status_ids[$status]);
        }

        if($contractor){
            $show_filters['contractor']=['title'=>'Contractor','value'=>$contractor_name];
            $data = $data
                ->where('z_project_contractor_join.z_contractors_id','=',$contractor);
        }

        if($tag){
            $show_filters['tag']=['title'=>'Tag','value'=>$tag_name];
            $data = $data
                ->where('tagged.tag_id','=',$tag);
        }

        if($region){
            $show_filters['region']=['title'=>'Region','value'=>$region];
            $data = $data
                ->where('z_projects.region_abrv','=',$region);
        }
        if($project_type){
            $show_filters['project_type']=['title'=>'Project Type','value'=>$project_type_name];
            $data = $data
                ->where('z_projects.z_project_types_id','=',$project_type);
        }


        $columns = [
            'project_id',
            'project_name',
            'last_visit',
            'details_url',
        ];

        $title = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));

        $subtitle = $this->addFiltersToTitle($show_filters);

        $filename = input('filename', trans('bct/projects::reports/common.tabs.contractor_summary'));
        $source = 'Projects / Reports ';
        $settings = [
            'sort'           => 'project_id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'subtitle'       => $subtitle,
            'export_source'  => $source,
            'export_columns' => [
                'project_name' => trans('bct/projects::reports/common.col.name_project'),
                'last_visit'   => trans('bct/projects::reports/common.col.last_visit'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element->last_visit = date_time_correct_pp($element->last_visit);
            $element->details_url = route('bct.projects.projects.details',['id'=>$element->project_id]);

            if (!input()->get('download', false)) {
            } else {
                $element[$export_columns['project_name']] = $element->project_name;
                $element[$export_columns['last_visit']] = $element->last_visit;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function addFiltersToTitle($show_filters){
        $filters_count = count($show_filters);
        $i=1;
        if($filters_count>0){
            $filters = '(';
            foreach($show_filters as $key=>$filter){
                $filters.= $filter['title'].': '.$filter['value'];
                if($filters_count !== $i) {
                    $filters.= ', ';
                }
                $i++;
            }
            $filters.= ')';
            return $filters;
        }
        else{
            return '';
        }
    }


}