<?php namespace Bct\Projects\Controllers\Frontend;

use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;

class DashboardController extends AuthorizedController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'visited',
	];

	/**
	 * The Projects repository.
	 *
	 * @var \Bct\Projects\Repositories\Project\ProjectRepositoryInterface
	 */
	protected $projects;

	/**
	 * The Users repository.
	 *
	 * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
	 */
	protected $users;

	/**
	 * Constructor.
	 *
	 * @param ProjectRepositoryInterface $projects
	 * @param UserRepositoryInterface $users
	 */
	public function __construct(
		ProjectRepositoryInterface $projects,
		UserRepositoryInterface $users
	) {
		parent::__construct();

		$this->projects = $projects;
		$this->users = $users;
	}

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		if($this->currentUser->inRole('super-repadmin')) {
			return redirect(route('bct.projects.users.all'), 302);
		}

		$scopes = $this->projects->scopes();

		return view('bct/projects::pages/dashboard', compact('scopes'));
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function shareByArea()
	{
		return $this->projects->shareByArea(input('scope'), input('area'));
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function totals()
	{
		return $this->projects->totals(input('year'));
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function topContractors()
	{
		return $this->projects->topContractors(input('scope'), input('area'), input('isUnion'));
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function followUp()
	{
		return $this->projects->followUp(input('area'));
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function repVisits()
	{
		return $this->users->repVisits(input('area'));
	}
}
