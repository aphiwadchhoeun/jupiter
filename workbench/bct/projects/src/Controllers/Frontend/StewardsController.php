<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Repositories\Contact\ContactRepositoryInterface;
use Illuminate\Http\Request;
use Bct\Projects\Repositories\Group\GroupRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Platform\Roles\Models\Role;

/**
 * Class StewardsController
 * @package Bct\Projects\Controllers\Frontend
 *
 * @deprecated
 */
class StewardsController extends AuthorizedController {
    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
     */
    protected $users;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\User\ContactRepositoryInterface
     */
    protected $contacts;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Group\GroupRepositoryInterface
     */
    protected $groups;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete', 'attach', 'detach'
    ];

    /**
     * Constructor.
     *
     * @param UserRepositoryInterface $users
     * @param GroupRepositoryInterface $groups
     */
    public function __construct(UserRepositoryInterface $users, GroupRepositoryInterface $groups, ContactRepositoryInterface $contacts) {
        parent::__construct();

        $this->users = $users;
        $this->groups = $groups;
        $this->contacts = $contacts;
    }

    /**
     * Display a listing of user.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('bct/projects::settings.stewards.index');
    }

    /**
     * Datasource for the user Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $data = $this->contacts->findAllStewards();

        $columns = [
            'id',
            'first_name',
            'last_name',
            'email',
            'phone',
            'company',
            'created_at' => 'created',
        ];
        $filename = trans('bct/projects::contacts/common.title') . "-" . date('M-d-Y');
        $settings = [
            'sort'             => 'created_at',
            'direction'        => 'desc',
            'pdf_filename'     => $filename,
            'csv_filename'     => $filename,
            'throttle'         => 16,
            'threshold'        => 16,
            'max_results'      => 16,
            'paginate_exports' => true,
        ];


        $transformer = function ($element) use ($data, $settings) {
            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.contacts.details', $element['id']);
            } else if (input()->get('download')) {
                $element['title'] = trans('bct/projects::contacts/common.title');
                $element['pagination'] = 'page ' . request()->input('page') . ' of ' . ceil($data->count() / $settings['max_results']);
            }
            $element['created'] = date('M-d-Y', strtotime($element['created']));

            $element['name'] = str_limit($element['first_name'] . ' ' . $element['last_name'], 35);
            $element['company'] = str_limit($element['company'], 35);

            return $element;
        };

        return datagrid($data, $columns, $settings, $transformer);
    }

    /**
     * Show the form for creating new user.
     *
     * @param  Request $request
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request) {
        if ($request->ajax()) {
            return $this->showAjaxForm();
        }

        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        return $this->processForm($request, 'create');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating user.
     *
     * @param  int $id
     * @param  Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) {
        return $this->processForm($request, 'update', $id);
    }

    /**
     * Remove the specified user.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->users->delete($id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("bct/projects::users/message.{$type}.delete")
        );

        return redirect()->route('bct.projects.stewards.all');
    }

    /**
     * Show the details for user.
     *
     * @param  int $id
     * @return mixed
     */
    public function details($id) {
        return $this->showDetails($id);
    }

    /**
     * Executes the mass action.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function executeAction($id = null) {
        $action = input('action');

        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                if (!is_null($id)) {
                    $this->users->{$action}($id, $row);
                } else {
                    $this->users->{$action}($row);
                }

            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {
        // Do we have a user identifier?
        if (isset($id)) {
            if (!$user = $this->users->find($id)) {
                $this->alerts->error(trans('bct/projects::users/message.not_found', compact('id')));

                return redirect()->route('bct.projects.stewards.all');
            }
        } else {
            $user = $this->users->createModel();
        }

        $role = Role::whereSlug('steward')->first();

        // Show the page
        return view('bct/projects::settings.stewards.form', compact('mode', 'user', 'role'));
    }

    /**
     * Shows the popup form.
     *
     * @return mixed
     */
    protected function showAjaxForm() {
        $user = $this->users->createModel();
        $mode = 'create';
        $role = Role::whereSlug('steward')->first();

        // Show the page
        return view('bct/projects::settings.stewards.modal', compact('mode', 'user', 'role'));
    }

    /**
     * Processes the form.
     *
     * @param  Request $request
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm(Request $request, $mode, $id = null) {
        // Store the user
        list($messages) = $this->users->store($id, request()->all());
        // Do we have any errors?
        if ($messages->isEmpty()) {
            /** if ajax form */
            if ($request->ajax()) {
                return ['success' => true];
            }

            $this->alerts->success(trans("bct/projects::users/message.success.{$mode}"));

            return redirect()->route('bct.projects.stewards.all');
        }

        $this->alerts->error($messages, 'form');

        /** if ajax form */
        if ($request->ajax()) {
            $request->flash();

            return $this->showAjaxForm();
        }

        return redirect()->back()->withInput();
    }

    /**
     * Shows the details.
     *
     * @param  int $id
     * @return mixed
     */
    protected function showDetails($id = null) {
        // Do we have a contact identifier?
        if (isset($id) && $user = $this->users->find($id)) {
            // Show the page
            return view('bct/projects::settings.stewards.details', compact('user'));
        }

        $this->alerts->error(trans('bct/projects::users/message.not_found', compact('id')));

        return redirect()->route('bct.projects.stewards.all');
    }
}
