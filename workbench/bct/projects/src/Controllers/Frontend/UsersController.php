<?php

namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Group;
use Bct\Projects\Models\User;
use Bct\Projects\Repositories\Group\GroupRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Platform\Roles\Models\Role;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Cartalyst\Support\Traits;

class UsersController extends AuthorizedController {

    use Traits\EventTrait;

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction', 'getSummaryGroupsCounter'
    ];

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
     */
    protected $users;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Group\GroupRepositoryInterface
     */
    protected $groups;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete', 'attach', 'detach'
    ];

    private $limit = 5;

    /**
     * Constructor.
     *
     * @param UserRepositoryInterface $users
     * @param GroupRepositoryInterface $groups
     */
    public function __construct(UserRepositoryInterface $users, GroupRepositoryInterface $groups) {
        parent::__construct();

        $this->users = $users;
        $this->groups = $groups;
    }

    /**
     * Display a listing of user.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('bct/projects::users.index');
    }

    /**
     * Datasource for the user Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $model = $this->users->createModel();

        $data = $model->fromView();


        $columns = [
            'name',
            'email',
            'status',
            'role',
            'created',
            'created_at',
            'link_uri',
        ];

        $title = input('filename', trans('bct/projects::users/common.title_reps'));
        $filename = input('filename', trans('bct/projects::users/common.title_reps'));
        $source = 'Projects / Settings / Reps';
        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'    => trans('bct/projects::users/model.general.name'),
                'email'   => trans('bct/projects::users/model.general.email'),
                'created' => trans('bct/projects::users/model.general.created_at'),
                'role'   => trans('bct/projects::users/model.general.role'),
                'status'  => trans('bct/projects::users/model.general.status'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $new_date = date('M-d-Y', strtotime($element['created_at']));
            $element['created'] = $new_date;
            $element->name = str_limit($element['name'], 35);
            $element->status_text = ($element->activated) ? trans('bct/projects::general/common.active') : trans('bct/projects::general/common.disabled');
            $element['link_uri'] = route('bct.projects.users.link', $element['id']);

            if (input()->get('download', false)) {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['email']] = $element->email;
                $element[$export_columns['status']] = $element->status;
                $element[$export_columns['role']] = $element->role;
                $element[$export_columns['created']] = $element->status_text;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Datasource for the groups Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridGroups($id) {
        $reverse = input('reverse');
        $user = $this->users->find($id);

        $model = $this->groups->createModel();
        $data = $model
            ->leftJoin('z_user_group_join', 'z_user_group_join.z_projects_groups_id', '=', $model->getTable() . '.id');

        if ($reverse) {
            $data = $data
                ->whereIn('z_user_group_join.z_projects_groups_id', array_column($user->groups->toArray(), 'id'));
        } else {
            $data = $data
                ->where(function ($query) use ($user) {
                    $query
                        ->whereNotNull('z_user_group_join.z_projects_groups_id')
                        ->whereNotIn('z_user_group_join.z_projects_groups_id', array_column($user->groups->toArray(), 'id'));
                })
                ->orWhereNull('z_user_group_join.z_projects_groups_id');
        }

        $data = $data
            ->whereNull($model->getTable() . '.deleted_at')
            ->groupBy($model->getTable() . '.id')
            ->select(
                $model->getTable() . '.id',
                $model->getTable() . '.name',
                $model->getTable() . '.individual',
                $model->getTable() . '.description',
                $model->getTable() . '.status',
                'z_user_group_join.z_projects_groups_id'
            );

        if ($limit = input('limit')) {
            $data = $data->take($limit);
        }

        $direction = 'desc';

        if (input('reverse')) {
            $direction = 'asc';
        }

        $columns = [
            'id',
            'name',
            'individual',
            'description',
            'status',
            'status_name',
            'z_projects_groups_id'
        ];

        $title = input('filename', (input('reverse')) ? trans("bct/projects::groups/common.user_groups") : trans("bct/projects::groups/common.available_groups"));
        $filename = $title;
        $source = 'Projects / Settings / Reps';
        $settings = [
            'sort'           => 'name',
            'direction'      => $direction,
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'throttle'       => $limit,
            'threshold'      => $limit,
            'max_results'    => $limit,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'        => trans('bct/projects::groups/model.general.name'),
                'individual'  => trans('bct/projects::groups/model.general.type'),
                'description' => trans('bct/projects::groups/model.general.description'),
                'status'      => trans('bct/projects::groups/model.general.status'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            if ($element->status) {
                $element->status_name = trans('bct/projects::general/common.active');
            } else {
                $element->status_name = trans('bct/projects::general/common.disabled');
            }

            if (!input()->get('download', false)) {

            } else {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['individual']] = $element->individual;
                $element[$export_columns['description']] = $element->description;
                $element[$export_columns['status']] = $element->status_name;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Datasource for the rep visits Data Grid.
     *
     * @param $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridVisits($id) {
        $user = $this->users->find($id);

        $data = $user->visits()
            ->join('z_projects', 'z_projects.id', '=', 'z_project_visit.z_projects_id');

        if ($limit = input('limit')) {
            $data = $data->take($limit);
        }

        $columns = [
            'details',
            'project' => [
                'name',
                'deleted_at'
            ],
            'created',
            'created_at',
            'stages_names',
            'view_url'
        ];

        $title = input('filename', trans('bct/projects::users/common.tabs.visits'));
        $filename = $title;
        $source = 'Projects / Settings / Reps ';

        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'project' => trans('bct/projects::visits/model.general.project'),
                'notes'   => trans('bct/projects::visits/model.general.notes'),
                'stage'   => trans('bct/projects::visits/model.general.stage'),
                'created' => trans('bct/projects::visits/model.general.date'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $new_date = date('M-d-Y', strtotime($element['created_at']));
            $element['created'] = $new_date;
            $element['details'] = str_limit($element['details'], 35);
            $element->stages_names = array_to_string_projects($element->stages->pluck('name'));
            $element['view_url'] = ($element->project) ? route('bct.projects.projects.details', $element->project->id) : null;

            if (!input()->get('download', false)) {

            } else {
                $element[$export_columns['project']] = $element->project->name;
                $element[$export_columns['notes']] = $element->details;
                $element[$export_columns['stage']] = $element->stages_names;
                $element[$export_columns['created']] = $element->created;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Show the form for creating new user.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating user.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified user.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->users->delete($id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("bct/projects::users/message.{$type}.delete")
        );

        return redirect()->route('bct.projects.users.all');
    }

    /**
     * Executes the mass action.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function executeAction($id = null) {
        $action = input('action');

        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                if (!is_null($id)) {
                    $this->users->{$action}($id, $row);
                } else {
                    $this->users->{$action}($row);
                }
            }
            if ($action == 'delete') {
                $this->alerts->success(
                    trans("bct/projects::users/message.success.delete")
                );
            }

            return response('Success');
        }


        return response('Failed', 500);
    }

    /**
     * Show edit link to contractor form
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editLink($id) {
        return $this->showEditLinkForm($id);
    }

    /**
     * Update link to contractor
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateLink($id) {
        return $this->processLinkForm($id);
    }

    protected function showEditLinkForm($id = null) {
        $user_roles = Role::whereIn('slug', ['repuser', 'repadmin', 'pp-guest'])->get();
        // Do we have a user identifier?
        if (isset($id)) {
            if (!$user = $this->users->find($id)) {
                $this->alerts->error(trans('bct/projects::users/message.not_found', compact('id')));

                return redirect()->route('bct.projects.users.all');
            }
        } else {
            $this->alerts->error(trans('bct/projects::users/message.fail'));

            return redirect()->route('bct.projects.users.all');
        }

        $groups = $this->groups->findAll()->where('status', Group::STATUS_ENABLED);


        $groups_count = $user->groups->count();
        $visits_count = $user->visits->count();

        if ($groups_count > $this->limit) {
            $grid_groups_count = $this->limit;
        } else {
            $grid_groups_count = $groups_count;
        }

        if ($visits_count > $this->limit) {
            $grid_visits_count = $this->limit;
        } else {
            $grid_visits_count = $visits_count;
        }
        $user_groups = $user->groups()->take(5)->get();
        $user_visits = $user->visits()->with('project')->take(5)->get();

        // Show the page
        return view('bct/projects::users.summary', compact('user', 'groups', 'user_roles', 'groups_count',
            'grid_groups_count', 'visits_count', 'grid_visits_count', 'user_groups', 'user_visits'));

    }

    public function getSummaryGroupsCounter($id) {
        $user = $this->users->find($id);
        $groups_count = $user->groups->count();

        if ($groups_count > $this->limit) {
            $grid_count = $this->limit;
        } else {
            $grid_count = $groups_count;
        }

        return "(" . $grid_count . " of " . $groups_count . ")";
    }

    public function getSummaryVisitsCounter($id) {
        $user = $this->users->find($id);
        $visits_count = $user->visits->count();

        if ($visits_count > $this->limit) {
            $grid_visits_count = $this->limit;
        } else {
            $grid_visits_count = $visits_count;
        }

        return "(" . $grid_visits_count . " of " . $visits_count . ")";
    }


    protected function processLinkForm($id = null) {
        $this->users->link($id, request()->all());

        $this->alerts->success(trans("bct/projects::users/message.success.update"));

        return redirect()->route('bct.projects.users.all');
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        $user_roles = Role::whereIn('slug', ['repuser', 'repadmin', 'super-repadmin', 'pp-guest'])->get();

        // Do we have a user identifier?
        if (isset($id)) {
            if (!$user = $this->users->find($id)) {
                $this->alerts->error(trans('bct/projects::users/message.not_found', compact('id')));

                return redirect()->route('bct.projects.users.all');
            }
        } else {
            $user = $this->users->createModel();
        }

        // Show the page
        if ($mode == "update") {
            return view('bct/projects::users.edit-form', compact('mode', 'user', 'user_roles'));
        } else {
            return view('bct/projects::users.form', compact('mode', 'user', 'user_roles'));
        }
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        // Store the user
        list($messages, $user) = $this->users->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("bct/projects::users/message.success.{$mode}"));

            return redirect()->route('bct.projects.users.link', array('id' => $user->id));
        }

        $this->alerts->error($messages, 'form');

        return redirect()->back()->withInput();
    }

    public function checkUser() {

        if (request()->ajax()) {
            $user_info = request()->all();
            $role = $user_info["role"];
            $email = $user_info["email"];

            $user = User::with('roles')->where('email', $email)->first();
            $user_with_selected_role = User::whereHas('roles', function ($q) use ($email, $role) {
                $q->where('id', '=', $role);
            })->where('email', $email)->first();


            if ( 
                $user_with_selected_role
                    ||
                !$user
            ) {
                return ["status" => "ok"];
            } elseif (
                $user->inRole('repuser')
                    ||
                $user->inRole('repadmin')
            ) {
                if ($user->inRole('repuser')) {
                    $role_name = Role::where('slug', 'repadmin')->first()->name;
                } elseif ($user->inRole('repadmin')) {
                    $role_name = Role::where('slug', 'repuser')->first()->name;
                }

                return ["status" => "new_role",
                        "roles"  => $role_name,
                        "user"   => $user
                ];
            } else {
                return ["status" => "ok"];
            }
        }
    }

    public function addRole() {
        if (request()->ajax()) {
            $user_info = request()->all();
            $roleId = $user_info["role"];
            $email = $user_info["email"];

            $user = User::where('email', $email)->first();
            if ($user) {

                $role = Sentinel::findRoleById($roleId);
                $role_repuser = Role::where('slug', 'repuser')->first();
                $role_repadmin = Role::where('slug', 'repadmin')->first();

                $user->roles()->detach($role_repuser->id);
                $user->roles()->detach($role_repadmin->id);
                $user->roles()->attach($roleId);

                $this->fireEvent('', [$user, $role]);

                return ["status" => "ok"];
            }

            return ["status" => "bad"];
        }
    }

}
    