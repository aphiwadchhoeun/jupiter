<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Group;
use Bct\Projects\Repositories\Group\GroupRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Repositories\Lists\ListsRepositoryInterface;
use DB;

class GroupsController extends AuthorizedController {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'updateView',
    ];

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Group\GroupRepositoryInterface
     */
    protected $users;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
     */
    protected $groups;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\User\ListsRepositoryInterface
     */
    protected $lists;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'attach', 'detach'
    ];

    private $limit = 5;


    /**
     * Constructor.
     *
     * @param GroupRepositoryInterface $groups
     * @param UserRepositoryInterface $users
     */
    public function __construct(GroupRepositoryInterface $groups, UserRepositoryInterface $users, ListsRepositoryInterface $lists) {
        parent::__construct();

        $this->groups = $groups;
        $this->users = $users;
        $this->lists = $lists;
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $groups = $this->groups->findAll()->where('status', Group::STATUS_ENABLED);
        $groups = Group::where('status', Group::STATUS_ENABLED)->get();

        return view('bct/projects::groups.index', compact('groups'));
    }

    /**
     * Show the form for creating new user.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new user.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating user.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating user.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }

    /**
     * Show the form for updating user.
     *
     * @param  int $id
     * @return mixed
     */
    public function view($id) {
        return $this->showView($id);
    }

    /**
     * Handle posting of the form for updating user.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateView($id) {
        return $this->processAttach($id);
    }

    /**
     * Remove the specified user.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteView($id) {
        return $this->showDelete($id);
    }

    /**
     * Remove the specified user.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $group = Group::find($id);

        $type = $group->delete($id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("bct/projects::groups/message.{$type}.delete")
        );

        return redirect()->route('bct.projects.groups.all');
    }

    /**
     * Shows the form.
     *
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        // Do we have a user identifier?
        if (isset($id)) {
            $group = Group::find($id);
            if (!$group) {
                $this->alerts->error(trans('bct/projects::groups/message.not_found', compact('id')));

                return redirect()->route('bct.projects.groups.all');
            }
        } else {
            $group = $this->groups->createModel();
        }

        // Show the page
        return view('bct/projects::groups.form', compact('mode', 'group'));
    }

    /**
     * Shows the view.
     *
     * @param  int $id
     * @return mixed
     */
    protected function showView($id) {

        $group = Group::find($id);
        // Do we have a user identifier?
        if (!isset($id) || !$group) {
            $this->alerts->error(trans('bct/projects::groups/message.not_found', compact('id')));

            return redirect()->route('bct.projects.groups.all');
        }

        $groups = $this->groups->findAll()->where('status', Group::STATUS_ENABLED);
        $groups = Group::where('status', Group::STATUS_ENABLED)->get();

        $reps_count = $group->users->count();

        if ($reps_count > $this->limit) {
            $grid_count = $this->limit;
        } else {
            $grid_count = $reps_count;
        }

        $group_reps = $group->users()->with('roles')->take($this->limit)->get();

        // Show the page
        return view('bct/projects::groups.summary', compact('group', 'groups', 'reps_count', 'grid_count', 'group_reps'));
    }

    /**
     * Shows the form.
     *
     * @param  int $id
     * @return mixed
     */
    protected function showDelete($id) {
        $group = Group::find($id);
        if (!$group) {
            $this->alerts->error(trans('bct/projects::groups/message.not_found', compact('id')));

            return redirect()->route('bct.projects.groups.all');
        }

        // Show the delete popup
        return view('bct/projects::groups.delete', compact('group'));
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {

        // Store the group
        list($messages, $group) = $this->groups->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("bct/projects::groups/message.success.{$mode}"));

            return redirect()->route('bct.projects.groups.view', $group->id);
        }

        if ($mode == 'create') {
            $error_message = '';
            foreach ($messages->all() as $message) {
                $error_message .= $message;
            }
            $this->alerts->error($error_message);
        } else {
            $this->alerts->error($messages, 'form');
        }
        if ($mode == "update") {
            return ["error" => true];
        }

        return redirect()->route('bct.projects.groups.view', $group->id);
    }

    /**
     * Attach user to group
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processAttach($id) {
        // Attach user to group
        $action = input('action');

        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                $this->groups->{$action}($id, $row);
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Datasource for the group Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $model = $this->groups->createModel();
        $data = $model
            ->select(
                $model->getTable() . '.id',
                $model->getTable() . '.name',
                $model->getTable() . '.status',
                $model->getTable() . '.individual',
                $model->getTable() . '.description',
                $model->getTable() . '.created_at'
            );

        $columns = [
            'id',
            'name',
            'individual',
            'description',
            'status',
            'created_at',
            'link_uri',
        ];

        $title = input('filename', trans('bct/projects::groups/common.title'));
        $filename = input('filename', trans('bct/projects::groups/common.title'));
        $source = 'Projects / Settings / Groups';
        $settings = [
            'sort'           => 'name',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'        => trans('bct/projects::groups/model.general.name'),
                'individual'  => trans('bct/projects::groups/model.general.type'),
                'description' => trans('bct/projects::groups/model.general.description'),
                'status'      => trans('bct/projects::groups/model.general.status'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $element['name'] = str_limit($element['name'], 35);
            $element['description'] = str_limit($element['description'], 35);

            if (!input()->get('download', false)) {
                $element['link_uri'] = route('bct.projects.groups.view', $element['id']);
            } else if (input()->get('download')) {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['individual']] = $element->individual;
                $element[$export_columns['description']] = $element->description;
                $element[$export_columns['status']] = $element->status;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }


    /**
     * Datasource for the user Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridUsers($group_id) {
        $reverse = input('reverse');
        $model = $this->users->createModel();

        $group = $this->groups->find($group_id);

        $sub_data = $model->select('*', DB::raw('CONCAT_WS(\' \', first_name, last_name) as full_name'));

        $data = $model
            ->setTable(DB::raw("( {$sub_data->toSql()} ) as " . $model->getDefaultTable()))
            ->leftJoin('z_user_group_join', 'z_user_group_join.user_id', '=', $model->getDefaultTable() . '.id')
            ->leftJoin('role_users', 'role_users.user_id', '=', $model->getDefaultTable() . '.id')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->leftJoin('activations', 'activations.user_id', '=', 'role_users.user_id');

        if ($reverse) {
            $data = $data->where('z_user_group_join.z_projects_groups_id', $group->id);
        } else {
            $data = $data
                ->whereIn('roles.slug', ['repuser', 'repadmin'])
                ->whereNotIn('role_users.user_id', array_column($group->users->toArray(), 'id'));
        }

        $data = $data
            ->groupBy('role_users.user_id')
            ->select(
                $model->getDefaultTable() . '.*',
                'activations.completed as status',
                DB::raw('GROUP_CONCAT(DISTINCT roles.name) as roles'),
                DB::raw('GROUP_CONCAT(role_users.user_id) as users')
            );

        if ($limit = input('limit')) {
            $data = $data->take($limit);
        }

        $columns = [
            'id',
            'email',
            'full_name',
            'status',
            'created_at',
            'roles',
            'users'
        ];

        $title = input('filename', (input('reverse')) ? trans("bct/projects::users/common.group_reps") : trans("bct/projects::users/common.available_reps"));
        $filename = input('filename', $title);
        $source = 'Projects / ' . (input('reverse')) ? 'Settings / Groups' : 'Projects';
        $settings = [
            'sort'           => 'full_name',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'   => trans('bct/projects::users/model.general.name'),
                'email'  => trans('bct/projects::users/model.general.email'),
                'role'   => trans('bct/projects::users/model.general.role'),
                'status' => trans('bct/projects::users/model.general.status'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            if ($element->status) {
                $element->status = trans('bct/projects::general/common.active');
            } else {
                $element->status = trans('bct/projects::general/common.disabled');
            }

            if (!input()->get('download', false)) {
                $element['link_uri'] = route('bct.projects.users.link', $element['id']);
            } else {
                $element[$export_columns['name']] = $element->full_name;
                $element[$export_columns['email']] = $element->email;
                $element[$export_columns['role']] = $element->roles;
                $element[$export_columns['status']] = $element->status;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Checks if group attached to any list.
     *
     * @return Response
     */
    public function checkGroupToListAttachment() {
        if (request()->ajax()) {
            $group_info = request()->all();
            $group_id = $group_info["id"];

            $count = $this->lists->where('z_projects_group_id', $group_id)->count();

            if ($count > 0) {
                $lists = $this->lists->where('z_projects_group_id', $group_id)->lists('name')->implode(", ");

                return ["success" => false, "lists" => $lists];
            } else {
                return ["success" => true];
            }
        }
    }

    /**
     * Updates reps counter on summary section
     *
     * @return string
     */
    public function getSummaryRepsCounter($id) {
        $group = Group::find($id);

        $reps_count = $group->users->count();

        if ($reps_count > $this->limit) {
            $grid_count = $this->limit;
        } else {
            $grid_count = $reps_count;
        }

        return "(" . $grid_count . " of " . $reps_count . ")";
    }
}
