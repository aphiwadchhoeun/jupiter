<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Option;
use Bct\Projects\Repositories\Contractor\ContractorAddressRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Repositories\Contractor\ContractorRepositoryInterface;
use Storage;

use JavaScriptContractor;
class ContractorAddressController extends AuthorizedController
{

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The ContractorAddress repository.
	 *
	 * @var \Bct\Projects\Repositories\Contractor\ContractorAddressRepositoryInterface
	 */
	protected $contractor_address;

	protected $contractor;

	/**
	 * ContractorAddressController constructor.
	 *
	 * @param ContractorAddressRepositoryInterface  $contractor_address
	 * @param ContractorRepositoryInterface $contractor
	 */
	public function __construct(
		ContractorAddressRepositoryInterface $contractor_address,
		ContractorRepositoryInterface $contractor
	) {
		parent::__construct();

		$this->contractor_address         = $contractor_address;
		$this->contractor                 = $contractor;
	}

	/**
	 * Show the form for creating new ContractorAddress.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}


	/**
	 * Handle posting of the form for creating new ContractorAddress.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}


	/**
	 * Show the form for updating ContractorAddress.
	 *
	 * @param  int $id
	 *
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}


	/**
	 * Handle posting of the form for updating ContractorAddress.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}


	/**
	 * Remove the specified ContractorAddress.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->contractor_address->delete($id) ? 'success' : 'error';
		$this->alerts->success(trans('bct/projects::contractors/message.address.'.$type.'.delete'));

		$result = ($type=='success')? true:false;
		return response()->json(['success'=>$result]);
	}


	protected function getAddress($id)
	{

		if ( ! $contractor_address = $this->contractor_address->find($id)) {

			return response()->json(['success'=>true,'contractor_address'=>$contractor_address]);
		}
		else{
			return response()->json(['success'=>false,'message'=>'Address not found']);
		}

	}


	/**
	 * Processes the form.
	 *
	 * @param  string $mode
	 * @param  int    $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the contractor
		list( $messages, $contractor_address ) = $this->contractor_address->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty()) {

			if (!$id) {
				$this->alerts->success(trans('bct/projects::contractors/message.address.success.create'));
			} else {
				$this->alerts->success(trans('bct/projects::contractors/message.address.success.update'));
			}

			return response()->json(['status'=>'true']);
		}

        return response()->json(['status'=>'false','messages'=>$messages]);
	}

	public function getAll($contractor_id){
		return $this->contractor_address->getAllByContractorId($contractor_id);
	}

}