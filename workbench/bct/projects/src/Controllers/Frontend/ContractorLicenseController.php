<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Option;
use Bct\Projects\Repositories\Contractor\ContractorLicenseRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Storage;
use JavaScriptContractor;
class ContractorLicenseController extends AuthorizedController
{

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The ContractorLicense repository.
	 *
	 * @var \Bct\Projects\Repositories\Contractor\ContractorLicenseRepositoryInterface
	 */
	protected $contractor_license;

	/**
	 * ContractorLicenseController constructor.
	 *
	 * @param ContractorLicenseRepositoryInterface  $contractor_license
	 */
	public function __construct(
		ContractorLicenseRepositoryInterface $contractor_license
	) {
		parent::__construct();

		$this->contractor_license         = $contractor_license;
	}

	/**
	 * Show the form for creating new ContractorLicense.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}


	/**
	 * Handle posting of the form for creating new ContractorLicense.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}


	/**
	 * Show the form for updating ContractorLicense.
	 *
	 * @param  int $id
	 *
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}


	/**
	 * Handle posting of the form for updating ContractorLicense.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}


	/**
	 * Remove the specified ContractorLicense.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->contractor_license->delete($id) ? 'success' : 'error';
		$this->alerts->success(trans('bct/projects::contractors/message.license.'.$type.'.delete'));

		$result = ($type=='success')? true:false;
		return response()->json(['success'=>$result]);	}



	protected function getLicense($id)
	{

		if ( ! $contractor_license = $this->contractor_license->find($id)) {

			return response()->json(['success'=>true,'contractor_license'=>$contractor_license]);

		}
		else{
			return response()->json(['success'=>false,'message'=>'License not found']);
		}

	}


	/**
	 * Processes the form.
	 *
	 * @param  string $mode
	 * @param  int    $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the contractor
		list( $messages, $contractor_license ) = $this->contractor_license->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty()) {

			if (!$id) {
				$this->alerts->success(trans('bct/projects::contractors/message.license.success.create'));
			} else {
				$this->alerts->success(trans('bct/projects::contractors/message.license.success.update'));
			}

			return response()->json(['status'=>'true']);
		}
		return response()->json(['status'=>'false','messages'=>$messages]);
	}

	public function getAll($contractor_id){
		return $this->contractor_license->getAllByContractorId($contractor_id);
	}


}