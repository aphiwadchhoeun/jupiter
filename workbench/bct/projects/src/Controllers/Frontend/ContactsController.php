<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Bct\Projects\Repositories\Contact\ContactRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use DB;

class ContactsController extends AuthorizedController {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Contact\ContactRepositoryInterface
     */
    protected $contacts;

    protected $projects;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];


    /**
     * ContactsController constructor.
     *
     * @param ContactRepositoryInterface $contacts
     * @param ProjectRepositoryInterface $projects
     */
    public function __construct(
        ContactRepositoryInterface $contacts,
        ProjectRepositoryInterface $projects
    ) {
        parent::__construct();

        $this->contacts = $contacts;
        $this->projects = $projects;
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $currentUser = $this->currentUser;

        return view('bct/projects::contacts.index', compact('currentUser'));
    }

    /**
     * Show the form for creating new contact.
     *
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function create(Request $request) {
        if ($request->ajax()) {
            return $this->showAjaxForm();
        }

        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new contact.
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        return $this->processForm($request, 'create');
    }

    /**
     * Show the form for updating contact.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating contact.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) {
        return $this->processForm($request, 'update', $id);
    }

    /**
     * Show the details for contact.
     *
     * @param  int $id
     * @return mixed
     */
    public function details($id) {
        return $this->showDetails($id);
    }

    /**
     * Remove the specified contact.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->contacts->delete($id, $this->currentUser->id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("bct/projects::contacts/message.{$type}.delete")
        );

        return redirect()->route('bct.projects.contacts.all');
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction() {
        $action = input('action');
        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                if ($action == 'delete') {
                    $this->contacts->{$action}($row, $this->currentUser->id);
                    $this->alerts->success(
                        trans("bct/projects::contacts/message.success.delete")
                    );
                } else {
                    $this->contacts->{$action}($row);
                }
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Datasource for the contacts Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $data = $this->contacts->gridReal()->fromView()->withTrashed();

        $columns = [
            'id',
            'company',
            'title',
            'phone',
            'phone2',
            'fax',
            'email',
            'name',
            'full_name_short',
            'location',
            'location_short',
            'title_short',
            'notes_short',
            'email_short',
            'active',
            'active_text',
            'active_color',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::contacts/common.title'));
        $filename = input('filename', trans('bct/projects::contacts/common.title'));
        $source = 'Projects / Contacts';
        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'global_columns' => [
                'id',
                'company',
                'phone',
                'email',
                'name',
                'active'
            ],
            'export_columns' => [
                'id'    => trans('bct/projects::contacts/model.general.id'),
                'name'    => trans('bct/projects::contacts/model.general.name'),
                'company' => trans('bct/projects::contacts/model.general.company'),
                'phone'   => trans('bct/projects::contacts/model.general.phone'),
                'email'   => trans('bct/projects::contacts/model.general.email'),
                'active'   => trans('bct/projects::general/common.active'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            $element['company'] = str_limit($element['company'], 35);
            $element->full_name_short = $element->full_name_short;
            $element->location = $element->location;
            $element->location_short = $element->location_short;
            $element->title_short = $element->title_short;
            $element->notes_short = $element->notes_short;
            $element->email_short = $element->email_short;
            $element->active_text = $element->active_text;
            $element->active_color = $element->active_color;

            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.contacts.details', $element['id']);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['company']] = $element->company;
                $element[$export_columns['phone']] = $element->phone;
                $element[$export_columns['email']] = $element->email;
                $element[$export_columns['active']] = $element->active_text;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Datasource for the contacts Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridProjects($contact_id) {
        $model = $this->projects->createProjectModel();
        $data = $model
            ->leftJoin('z_project_contact_join', 'z_project_contact_join.z_projects_id', '=', $model->getTable() . '.id')
            ->leftJoin('z_contact_type', 'z_contact_type.id', '=', 'z_project_contact_join.z_contact_type_id')
            ->leftJoin('z_project_status', 'z_project_status.id', '=', $model->getTable() . '.z_project_status_id')
            ->where('z_project_contact_join.z_contact_id', '=', $contact_id)
            ->select(
                $model->getTable() . '.id',
                $model->getTable() . '.name',
                $model->getTable() . '.city',
                $model->getTable() . '.state',
                $model->getTable() . '.created_at',
                'z_contact_type.name as contact_type',
                'z_project_status.name as status'
            );

        $contact = $this->contacts->find($contact_id);

        $columns = [
            'id',
            'name',
            'city',
            'state',
            'created',
            'created_at',
            'status',
            'contact_type',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::contacts/common.title') . "-" . trans('bct/projects::projects/common.title'));
        $filename = input('filename', trans('bct/projects::contacts/common.title') . "-" . trans('bct/projects::projects/common.title'));
        $source = 'Projects / Contacts';
        $settings = [
            'sort'           => 'z_projects.created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'id'         => trans('bct/projects::projects/model.general.id'),
                'name'         => trans('bct/projects::projects/model.general.name'),
                'city'         => trans('bct/projects::projects/model.general.city'),
                'state'        => trans('bct/projects::projects/model.general.state'),
                'created'      => trans('bct/projects::projects/model.general.created_at'),
                'status'       => trans('bct/projects::projects/model.general.status_id'),
                'contact_type' => trans('bct/projects::projects/model.general.contact_type'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $new_date = date('M-d-Y', strtotime($element['created_at']));
            $element['created'] = $new_date;

            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.projects.details', $element->id);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['state']] = $element->state;
                $element[$export_columns['created']] = $element->created;
                $element[$export_columns['status']] = $element->status;
                $element[$export_columns['contact_type']] = $element->contact_type;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        // Do we have a contact identifier?
        if (isset($id)) {
            if (!$contact = $this->contacts->find($id)) {
                $this->alerts->error(trans('bct/projects::contacts/message.not_found', compact('id')));

                return redirect()->route('bct.projects.contacts.all');
            }
            
            /**TODO: disable this check, remove this code block next phase
             * check if user can modify contact */
            /*if ($contact->created_by != $this->currentUser->id && (!Sentinel::inRole('repadmin') && !Sentinel::inRole('admin'))) {
                $this->alerts->error(trans('bct/projects::contacts/permissions.access_error', compact('id')));

                return redirect()->back();
            }*/
        } else {
            $contact = $this->contacts->createModel();
        }

        // Show the page
        return view('bct/projects::contacts.form', compact('mode', 'contact'));
    }

    /**
     * Shows the popup form.
     *
     * @return mixed
     */
    protected function showAjaxForm() {
        $contact = $this->contacts->createModel();
        $mode = 'create';

        // Show the page
        return view('bct/projects::contacts.modal', compact('mode', 'contact'));
    }

    /**
     * Processes the form.
     *
     * @param  Request $request
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm(Request $request, $mode, $id = null) {
        // Store the contact
        list($messages, $contact) = $this->contacts->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            /** if ajax form */
            if ($request->ajax()) {
                return ['success' => true];
            } else {
                $this->alerts->success(trans("bct/projects::contacts/message.success.{$mode}"));

                return redirect()->route('bct.projects.contacts.details', $contact->id);
            }
        }

        $this->alerts->error($messages, 'form');

        /** if ajax form */
        if ($request->ajax()) {
            $request->flash();

            return $this->showAjaxForm();
        }

        return redirect()->back()->withInput();
    }

    /**
     * Shows the details.
     *
     * @param  int $id
     * @return mixed
     */
    protected function showDetails($id = null) {

        // Do we have a contractor identifier?
        if (isset($id) && $contact = $this->contacts->find($id)) {
            $currentUser = $this->currentUser;

            return view('bct/projects::contacts.details', compact('contact','currentUser'));
        }

        $this->alerts->error(trans('bct/projects::contacts/message.not_found', compact('id')));

        return redirect()->route('bct.projects.contacts.all');
    }

    public function downloadPdfDetails($contact_id) {
        return $this->contacts->downloadPdfDetails($contact_id);
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function merge() {
        try {

            DB::beginTransaction();

            $input_data = input()->all();
            $contact_merge = $this->contacts->find(input('contact-one-id'));
            $contact_delete = $this->contacts->find(input('contact-two-id'));

            $this->contacts->updateContactByMerge($contact_merge, $contact_delete, $input_data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->alerts->error(trans('bct/projects::contacts/message.error.merge'));

            return redirect()->route('bct.projects.contacts.all');
        }

        $this->alerts->success(trans('bct/projects::contacts/message.success.merge'));

        return redirect()->back();
    }
}
