<?php namespace Bct\Projects\Controllers\Frontend;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 14:15
 */


use Bct\Projects\Repositories\Member\MemberRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Bct\Projects\Repositories\Contact\ContactRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use DB;
use Platform\Foundation\Controllers\Controller;


class MembersController extends Controller {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Contact\ContactRepositoryInterface
     */
    protected $members;

    protected $projects;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];


    /**
     * MembersController constructor.
     *
     * @param MemberRepositoryInterface  $members
     * @param ProjectRepositoryInterface $projects
     */
    public function __construct(
        MemberRepositoryInterface $members,
        ProjectRepositoryInterface $projects
    ) {
        parent::__construct();

        $this->members = $members;
        $this->projects = $projects;
    }

    /**
     * Show the form for creating new contact.
     *
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function create(Request $request) {
        if ($request->ajax()) {
            return $this->showAjaxForm();
        }

        return $this->showForm('create');
    }

    /**
     * Show the form for updating contact.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        // Do we have a contact identifier?
        if (isset($id)) {
            if (!$member = $this->members->find($id)) {
                $this->alerts->error(trans('bct/projects::members/message.not_found', compact('id')));

                return redirect()->route('bct.projects.members.all');
            }
        } else {
            $member = $this->members->createModel();
        }

        // Show the page
        return view('bct/projects::members.form', compact('mode', 'member'));
    }

}
