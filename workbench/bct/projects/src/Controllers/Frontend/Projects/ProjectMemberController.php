<?php namespace Bct\Projects\Controllers\Frontend\Projects;
/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 12:46
 */

use Bct\Projects\Models\MemberType;
use Bct\Projects\Repositories\Member\MemberRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Bct\Projects\Repositories\Scope\ScopeRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Platform\Access\Controllers\AuthorizedController;

class ProjectMemberController extends AuthorizedController {

    protected $projects, $members, $scopes;


    /**
     * ProjectMemberController constructor.
     *
     * @param ProjectRepositoryInterface $projects
     * @param MemberRepositoryInterface  $members
     * @param ScopeRepositoryInterface   $scopes
     */
    public function __construct(
        ProjectRepositoryInterface $projects,
        MemberRepositoryInterface $members,
        ScopeRepositoryInterface $scopes
    ) {
        parent::__construct();
        $this->projects = $projects;
        $this->members = $members;
        $this->scopes = $scopes;
    }

    /**
     * Datasource for the project-members Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridMembers($id) {
        $project = $this->projects->find($id);

        $model = $this->members->grid();

        $data = $model
            ->fromViewProject()
            ->withTrashed()
            ->where('z_projects_id', $project->id);

        $columns = [
            'id',
            'member_number',
            'member_name',
            'wage',
            'wage_correct',
            'number_of_hours',
            'scope_name',
            'phone',
            'view_uri',
        ];

        $title = input('filename', $project->name . ' - ' . trans('bct/projects::members/common.title'));
        $filename = input('filename', trans('bct/projects::projects/common.title') . '-' . trans('bct/projects::members/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'member_name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'global_columns' => [
                'member_number',
                'member_name',
                'scope_name',
                'number_of_hours',
                'wage',
            ],
            'export_columns' => [
                'member_number'   => trans('bct/projects::contacts/model.general.id'),
                'member_name'     => trans('bct/projects::members/model.general.name'),
                'scope_name'      => trans('bct/projects::members/model.general.scope'),
                'number_of_hours' => trans('bct/projects::members/model.general.number_hours'),
                'wage'            => trans('bct/projects::members/model.general.wage'),
            ]
        ];

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($id, $export_columns) {

            if (!input()->get('download', false)) {
                $element->wage_correct = show_money_projects($element->wage);
                $element['view_uri'] = route('bct.projects.projects.member.edit', [$id, $element['id'], $element->pivot_id]);
            } else {
                $element[$export_columns['member_number']]   = $element->member_number;
                $element[$export_columns['member_name']]     = $element->member_name;
                $element[$export_columns['scope_name']]      = $element->scope_name;
                $element[$export_columns['number_of_hours']] = $element->number_of_hours;
                $element[$export_columns['wage']]            = $element->wage;
            }


            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Return dataset for all members
     * @param $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridAllMembers($id) {
        $model = $this->members->grid();

        $members_ids_in_project = $this->projects->find($id)->members->lists('id')->toArray();

        $data = $model
            ->fromView()
            ->withTrashed()
            ->whereNotIn('id', $members_ids_in_project);

        $columns = [
            'id',
            'member_number',
            'member_name',
            'member_since',
            'member_since_correct',
            'phone',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::members/common.title'));
        $filename = input('filename', trans('bct/projects::members/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'global_columns' => [
                'member_number',
                'member_name',
                'phone',
                'member_since',
            ],
            'export_columns' => [
                'member_number' => trans('bct/projects::members/model.general.id'),
                'member_name'   => trans('bct/projects::members/model.general.name'),
                'phone'         => trans('bct/projects::members/model.general.phone'),
                'member_since'  => trans('bct/projects::members/model.general.member_since'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($id, $data, $export_columns) {


            if (!input()->get('download', false)) {
                $element->member_since_correct = date_correct_details_projects($element->member_since);
                $element['view_uri'] = route('bct.projects.projects.member.add', [ $id, $element['id'] ]);
            } else {
                $element[$export_columns['member_number']] = $element->member_number;
                $element[$export_columns['member_name']]   = $element->member_name;
                $element[$export_columns['phone']]         = $element->phone;
                $element[$export_columns['member_since']]  = $element->member_since;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Show the add member form.
     *
     * @param  int $id
     * @param $member_id
     * @return mixed
     */
    public function addMember($id, $member_id) {
        return $this->showMemberForm('create', $id, $member_id);
    }

    /**
     * Store member to specified project
     *
     * @param $id
     * @param $member_id
     * @return mixed
     */
    public function storeMember($id, $member_id) {
        return $this->processFormMembers($id, 'create');
    }

    /**
     * Show the add member form.
     *
     * @param  int $id
     * @param $member_id
     * @param $pivot_id
     * @return mixed
     */
    public function editMember($id, $member_id, $pivot_id) {
        return $this->showMemberForm('update', $id, $member_id, $pivot_id);
    }

    /**
     * Handle posting of the form for updating member.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateMember($id, $member_id, $pivot_id) {
        return $this->processFormMembers($id, 'update');
    }

    /**
     * Show the members of project.
     *
     * @param  int $id
     * @param $mode
     * @return mixed
     */
    protected function processFormMembers($id, $mode) {
        list($result, $msg) = $this->projects->storeMembers($id, request()->all(), $mode);

        if ($result) {
            return response('success', 200);
        }

        return response($msg, 200);
    }

    /**
     * Show the add member form.
     *
     * @param  int $id
     * @param $pivot_id
     * @return mixed
     */
    public function deleteMember($id, $pivot_id) {
        if ($this->projects->deleteMember($pivot_id)) {
            return response('success', 200);
        }

        return response('fail', 200);
    }

    /**
     * Shows the member popup.
     *
     * @param  string $mode
     * @param  int $id
     * @param $member_id
     * @param null $pivot_id
     * @return mixed
     */
    protected function showMemberForm($mode, $id, $member_id, $pivot_id = null) {
        if (isset($id) && $project = $this->projects->find($id)) {
            if ($member = $this->members->find($member_id)) {
                $pivot = DB::table('z_project_member_join')->where('id', $pivot_id)->first();

                $scopes = $this->scopes->findAll();

                // Show the page
                return view('bct/projects::projects.tabs.members.add-member',
                    compact('mode', 'member', 'project', 'pivot', 'scopes')
                );
            } else {
                return response(trans('bct/projects::members/message.not_found', ['id' => $member_id]), 204);
            }
        }

        return response(trans('bct/projects::members/message.not_found', ['id' => $member_id]), 204);
    }

    public function getMemberTypesData() {
        if ($id = $query = input('id')) {
            return MemberType::find($id);
        }

        return $this->projects->memberTypes(input('q'));
    }

}