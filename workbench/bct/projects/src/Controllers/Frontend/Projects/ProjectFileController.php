<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 10/28/2015
 * Time: 6:15 PM
 */

namespace Bct\Projects\Controllers\Frontend\Projects;

use Bct\Projects\Repositories\File\FileRepositoryInterface;
use Bct\Projects\Repositories\Media\MediaRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Cartalyst\Filesystem\Laravel\Facades\Filesystem;
use Illuminate\Support\Facades\Response;
use Platform\Access\Controllers\AuthorizedController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProjectFileController extends AuthorizedController {

    protected $projects, $files, $media;

    /**
     * ProjectFileController constructor.
     *
     * @param ProjectRepositoryInterface $projects
     * @param FileRepositoryInterface    $files
     * @param MediaRepositoryInterface   $media
     */
    public function __construct(
        ProjectRepositoryInterface $projects,
        FileRepositoryInterface $files,
        MediaRepositoryInterface $media
    ) {
        parent::__construct();

        $this->projects = $projects;
        $this->files = $files;
        $this->media = $media;
    }


    /**
     * Datasource for the project-actions Data Grid.
     *
     * @param $id
     * @return \Cartalyst\DataGrid\DataGrid
     *
     * @deprecated
     */
    public function gridFiles($id) {
        $project = $this->projects->find($id);

        $data = $project->files()->with(['user']);

        $data = $data->get()->each(function ($item) use ($id) {
            $item->username = $item->user->first_name . ' ' . $item->user->last_name;
        });

        $columns = [
            'id',
            'extension',
            'name',
            'path',
            'size',
            'created_at' => 'created',
            'username',
            'description',
        ];

        $filename = trans('bct/projects::files/common.title') . "-" . date('M-d-Y');
        $settings = [
            'sort'             => 'created_at',
            'direction'        => 'desc',
            'pdf_filename'     => $filename,
            'csv_filename'     => $filename,
            'max_results'      => 16,
            'throttle'         => 16,
            'threshold'        => 16,
            'paginate_exports' => true,
        ];

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $transformer = function ($element) use ($project, $data, $settings) {
            if (!input()->get('download', false)) {
                $element['download_uri'] = route('bct.projects.media.download', $element['path']);
                $element['edit_uri'] = route('bct.projects.projects.editFile', [$project->id, $element['id']]);
                $element['view_uri'] = route('bct.projects.projects.viewFile', [$project->id, $element['path']]);
            } else if ($downloadType = input()->get('download')) {
                if ($downloadType == 'pdf') {
                    $element['title'] = $project->name . ' - ' . trans('bct/projects::files/common.title');
                    $element['pagination'] = 'page ' . request()->input('page') . ' of ' . ceil($data->count() / $settings['max_results']);
                }
                unset($element['id']);
                unset($element['extension']);
                unset($element['path']);
            }

            $element['size'] = number_format($element['size'] / 1024, 2) . 'KB';
            $element['created'] = date('M-d-Y', strtotime($element['created']));

            return $element;
        };

        return datagrid($data, $columns, $settings, $transformer);
    }

    /**
     * Prepares mime types for output.
     *
     * @param  array $mimes
     * @return string
     */
    protected function prepareMimes($mimes) {
        return implode(', ', $mimes);
    }

    public function createFile($id) {
        // Get a list of all the allowed mime types
        $allowedMimes = ['txt', 'csv', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'odt', 'ods', 'odp',
            'gif', 'jpg', 'png', 'bmp', 'psd',
            'ogg', 'mp4', 'avi', 'zip', 'rar', '7zip'];
        // Prepare mimes
        $mimes = $this->prepareMimes($allowedMimes);

        // Show the page
        return view('bct/projects::projects.tabs.files.form', compact('mimes'));
    }

    public function editFile($project_id, $id) {
        $file = $this->files->find($id);

        // Show the page
        return view('bct/projects::projects.tabs.files.edit', compact('file'));
    }

    public function updateFile($project_id, $id) {
        list($result, $msg) = $this->files->update($id, request()->all());

        if ($result) {
            return response('success', 200);
        }

        return response($msg, 200);
    }

    public function storeFile() {
        $file = request()->file('file');


        $messages = $this->files->validForCreation(request()->all());

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {

            $input = [
                'folder' => asset_project_files_path_projects(),
                'tags' => ['project_file'],
                'z_projects_id' => input('z_projects_id'),
                'description' => input('description'),
                'user_id' => $this->currentUser->getUserId()
            ];

            try {
                if ($media = $this->media->upload($file, $input)) {
                    return response($media);
                }

            } catch ( \Exception $e ) {
                return response()->json([ 'success' => false , 'message' => $e->getMessage()], 422);
            }


        }

        $messages = $messages->getMessages();

        return response(array_shift($messages)[0], 400);

        //return response($this->files->getError(), 400);
    }

    public function deleteFiles() {
        $action = request()->input('action');

        if (in_array($action, $this->actions)) {
            foreach (request()->input('rows', []) as $entry) {
                $this->files->{$action}($entry);
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Downloads the given media file.
     *
     * @param  string $path
     * @return \Illuminate\Http\Response
     */
    public function viewFile($id, $path) {
        $media = $this->getMedia($path);

        $file = Filesystem::read($media->path);

        $headers = [
            'Connection'          => 'close',
            'Content-Type'        => $media->mime,
            'Content-Length'      => strlen($file),
            'Content-Disposition' => 'inline; filename="' . $media->name . '"',
        ];

        return $this->respond($file, $headers);
    }

    protected function getMedia($path) {
        if (!$media = $this->files->findByPath($path)) {
            throw new HttpException(404, 'File does not exist.');
        }

        return $media;
    }

    /**
     * Sends the response with the appropriate headers.
     *
     * @param  string $file
     * @param  array $headers
     * @return \Illuminate\Http\Response
     */
    protected function respond($file, $headers = []) {
        $response = Response::make($file, 200);

        foreach ($headers as $name => $value) {
            $response->header($name, $value);
        }

        return $response;
    }


    public function deleteFile($project_id, $file_id)
    {

        if ($this->media->delete($file_id)) {
            $this->alerts->success(trans('bct/projects::projects/message.success.upload'));

            return response()->json([ 'success' => true ]);
        }

        $this->alerts->error(trans('bct/projects::projects/message.error.upload'));

        return redirect()->route('bct.projects.projects.details', $project_id);
    }

    public function filesUpload($project_id)
    {
        $files = input()->file('files');

        $json = array(
            'files' => []
        );

        if (!is_null($files) && is_array($files) && !empty($files)) {
            foreach( $files as $file ) {

                $input = [
                    'folder' => asset_project_files_path_projects()
                ];

                $file_data = [
                    'document_name' => '',
                    'name' => $file->getClientOriginalName(),
                    'description' => '',
                    'size' => $file->getSize(),
                    'type' => $file->getMimeType(),
                    'extension' => $file->getClientOriginalExtension(),
                    'deleteType' => 'DELETE',
                ];

                try {
                    $input['z_projects_id'] = $project_id;
                    $input['user_id'] = $this->currentUser->getUserId();

                    $media = $this->media->upload($file, $input);

                    $file_data['id'] = $media->id;
                    $file_data['file_id'] = $media->id;
                    $file_data['url'] = route('bct.projects.media.download', $media->path);
                    $file_data['deleteUrl'] = route('bct.projects.projects.files.delete', [$media->id ]);
                    $file_data['date_correct'] = ($media->date === '0000-00-00' || is_null($media->date)) ? '' : Carbon::parse($media->date)->format('m/d/Y');


                } catch ( \Exception $e ) {
                    return response()->json([ 'success' => false , 'message' => $e->getMessage()], 422);
                }

                $json['files'][] = $file_data;

            }
        }


        return response()->json($json, 200);
    }

    public function filesGet($project_id = null)
    {
        $projects_files = [];

        if (!is_null($project_id)) {
            $project = $this->projects->findWith($project_id, 'files');
            $files = $project->files;

            if (!$files->isEmpty()) {
                foreach ($files as $file) {


                    if (!is_null($file) && $this->media->hasFileOnDisk($file)) {
                        $projects_files[] = [
                            'id' => $file->id,
                            'name' => $file->name,
                            'document_name' => custom_files_name_projects($file->name),
                            'description' => $file->description,
                            'date_correct' => date_correct_details_projects($file->created_at),
                            'size' => formatBytes($this->media->getSizeOfFile($file)),
                            'type' => $this->media->getMimetypeOfFile($file),
                            'extension' => $this->media->getExtension($file->path),
                            'url' => route('bct.projects.media.download', $file->path),
                            'deleteUrl' => route('bct.projects.projects.files.delete', [$file->id]),
                            'deleteType' => 'DELETE'
                        ];
                    }

                }

                return response()->json($projects_files, 200);
            }
        }


        return response()->json($projects_files, 200);
    }

    public function filesDelete($file_id)
    {
        try {
            if ($this->media->delete($file_id)) {
                $this->alerts->success(trans('bct/projects::projects/message.success.upload'));

                return response()->json([ 'success' => true ]);
            } else {
                return response()->json([ 'success' => false , 'message' => 'No file'], 422);
            }

        } catch (\Exception $e) {
            return response()->json([ 'success' => false , 'message' => $e->getMessage()], 422);
        }

        return response()->json([ 'success' => true], 200);
    }

    public function filesUpdate()
    {
        $data = input('files_data');

        if (is_array($data) && !empty($data)) {
            foreach ($data as $file_data) {
                if ( isset($file_data['id']) && is_numeric($file_data['id']) ) {

                    try {
                        $project_file = $this->media->find($file_data['id']);

                        $project_file->fill([
                            'name' => $file_data['document_name'],
                            'description' => $file_data['document_description'],
                        ])->save();
                    } catch (\Exception $e) {
                        return response()->json([ 'success' => false , 'message' => $e->getMessage()], 422);
                    }

                }

            }

            return response()->json([ 'success' => true], 200);
        }

        return response()->json([ 'success' => false , 'message' => 'No data'], 422);
    }

}