<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 10/28/2015
 * Time: 6:25 PM
 */

namespace Bct\Projects\Controllers\Frontend\Projects;


use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;

/**
 * Class ProjectStewardController
 * @package Bct\Projects\Controllers\Frontend\Projects
 *          
 * @deprecated
 */
class ProjectStewardController extends AuthorizedController {

    protected $projects, $users;

    /**
     * ProjectStewardController constructor.
     * @param ProjectRepositoryInterface $projects
     * @param UserRepositoryInterface $users
     */
    public function __construct(ProjectRepositoryInterface $projects, UserRepositoryInterface $users) {
        parent::__construct();

        $this->projects = $projects;
        $this->users = $users;
    }

    /**
     * Datasource for the project-stewards Data Grid.
     *
     * @param $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridStewards($id) {
        $project = $this->projects->find($id);

        $data = $this->projects->gridStewards($id);

        $columns = [
            'id',
            'email',
            'phone',
            'local',
            'full_name',
            'pivot'
        ];

        $filename = trans('bct/projects::stewards/common.title') . "-" . date('M-d-Y');
        $settings = [
            'sort'             => 'full_name',
            'direction'        => 'asc',
            'pdf_filename'     => $filename,
            'csv_filename'     => $filename,
            'max_results'      => 16,
            'throttle'         => 16,
            'threshold'        => 16,
            'paginate_exports' => true,
        ];

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $transformer = function ($element) use ($id, $data, $settings, $project) {
            if (!input()->get('download', false)) {
                $element['delete_uri'] = route('bct.projects.projects.deleteSteward', [$id, $element['pivot']['id']]);
            } else if ($downloadType = input()->get('download')) {

                if ($downloadType == 'pdf') {
                    $element['title'] = $project->name . ' - ' . trans('bct/projects::stewards/common.title');
                    $element['pagination'] = 'page ' . request()->input('page') . ' of ' . ceil($data->count() / $settings['max_results']);
                }
            }

            $element['full_name'] = str_limit($element['full_name'], 35);

            return $element;
        };

        return datagrid($data, $columns, $settings, $transformer);
    }

    /**
     * Return data source for all Stewards
     *
     * @param $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridAllStewards($id) {
        $data = $this->users->gridStewards();

        $columns = [
            'id',
            'email',
            'full_name',
            'phone',
            'local',
        ];

        $settings = [
            'sort'      => 'full_name',
            'direction' => 'asc',
            'throttle'  => 16,
            'threshold' => 16,
        ];

        $transformer = function ($element) use ($id) {
            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.projects.steward.add', [$id, $element['id']]);
            }

            $element['full_name'] = str_limit($element['full_name'], 35);

            return $element;
        };

        return datagrid($data, $columns, $settings, $transformer);
    }

    /**
     * Handle positing for the form for adding stewards
     *
     * @param $id
     * @param $steward_id
     * @return mixed
     */
    public function storeSteward($id, $steward_id) {
        return $this->processStewards($id, $steward_id);
    }

    /**
     * Show the stewards of project.
     *
     * @param  int $id
     * @param $steward_id
     * @return mixed
     */
    protected function processStewards($id, $steward_id) {
        list($result, $msg) = $this->projects->storeSteward($id, $steward_id);

        if ($result) {
            return response('success', 200);
        }

        return response($msg, 200);
    }

    /**
     * Show the add steward form.
     *
     * @param  int $id
     * @param $steward_id
     * @return mixed
     */
    public function deleteSteward($id, $steward_id) {
        if ($this->projects->deleteSteward($steward_id)) {
            return response('success', 200);
        }

        return response('fail', 200);
    }

}