<?php namespace Bct\Projects\Controllers\Frontend\Projects;

use Bct\Projects\Models\Action;
use Bct\Projects\Models\Contact;
use Bct\Projects\Models\Visit;
use Bct\Projects\Repositories\Group\GroupRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Platform\Access\Controllers\AuthorizedController;
use JavaScriptProjects;
use JavaScriptUnionimpactbase;
use DB;

class ProjectsController extends AuthorizedController {

    use DispatchesJobs;

    protected $authWhitelist = [
        'edit'
    ];

    protected $permissionsWhitelist = [
        'edit'
    ];

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction', 'deleteFile'
    ];

    /**
     * The repository.
     *
     * @var \Bct\Projects\Repositories\Project\ProjectRepositoryInterface
     */
    protected $projects;

    /**
     * The repository.
     *
     * @var \Bct\Projects\Repositories\Group\GroupRepositoryInterface
     */
    protected $groups;

    protected $users;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
        'storeLists'
    ];

    private $limit = 5;

    /**
     * Constructor.
     *
     * @param ProjectRepositoryInterface $projects
     * @param GroupRepositoryInterface $group
     * @param UserRepositoryInterface $users
     */
    public function __construct(
        ProjectRepositoryInterface $projects,
        GroupRepositoryInterface $group,
        UserRepositoryInterface $users
    ) {
        parent::__construct();

        $this->projects = $projects;
        $this->groups = $group;
        $this->users = $users;
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        if (input('action', null) == 'storeLists') {
            // if clicking on ADD_TO_LIST button
            return $this->showFormListsMany();
        } else {
            $projects_all_ids = $this->projects->gridProjects();
            $stages = $this->projects->stages();
	        $user = $this->currentUser;


	        return view('bct/projects::projects.index', compact('projects_all_ids', 'stages', 'user'));
        }
    }

    /**
     * Shows the form for adding many groups to one list.
     *
     * @return mixed
     */
    protected function showFormListsMany() {
        $number = input('number');

        // Show the page
        return view('bct/projects::projects.form', compact('number'));
    }

    /**
     * Show the form for search projects before create.
     *
     * @return \Illuminate\View\View
     */
    public function search() {

        JavaScriptProjects::put([
            'default_lat' => config_projects('bct.unionimpactbase.map.location.default.lat'),
            'default_lng' => config_projects('bct.unionimpactbase.map.location.default.lng'),
        ]);

        return view('bct/projects::projects.search');
    }

    /**
     * Show the form for creating new project.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new project.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating project.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        if (!$this->currentUser) {

            JavaScriptUnionimpactbase::put([
                'default_lat' => config('bct.unionimpactbase.map.location.default.lat'),
                'default_lng' => config('bct.unionimpactbase.map.location.default.lng'),
            ]);

            return view('bct/unionimpactbase::openroutes.index');
        }

        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating project.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }

    /**
     * Show the details for project.
     *
     * @param  int $id
     * @return mixed
     */
    public function details($id) {
        return $this->showDetails($id);
    }

    /**
     * Remove the specified project.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->projects->delete($id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("bct/projects::projects/message.{$type}.delete")
        );

        return redirect()->route('bct.projects.projects.all');
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction() {
        $action = input('action');

        if (in_array($action, $this->actions)) {

            if ($action == 'storeLists' && input('rows') === 'all') {
                $projects_all_ids = $this->projects->gridProjects()->toArray();

                if (!empty($projects_all_ids)) {
                    foreach ($projects_all_ids as $projects_all_id) {
                        $this->projects->{$action}($projects_all_id, null, ['list_id' => input('list_id')]);
                    }
                }
            } else {
                foreach (input('rows', []) as $row) {
                    $this->projects->{$action}($row, null, ['list_id' => input('list_id')]);
                }
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Datasource for the projects Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $model = $this->projects->createProjectModel();

        $data = $model
            ->select(
                'z_projects.id',
                'z_projects.name',
                DB::raw('IF(char_length(`z_projects`.`name`) > 30, CONCAT(SUBSTR(`z_projects`.`name`, 1, 30), "..."), `z_projects`.`name`) as name_short'),
                'z_projects.city',
                'z_projects.region_abrv',
                'z_projects.follow_up_date',
	            DB::raw('z_projects.z_project_classification_id as classification'),
	            DB::raw('DATE_FORMAT(z_projects.follow_up_date, \'%b-%d-%Y\') as follow_up_date_custom'),
                DB::raw('GROUP_CONCAT(DISTINCT tags.name) as tags'),
                DB::raw('z_project_status.name as status'),
                DB::raw('GROUP_CONCAT(DISTINCT `z_project_stage`.`name`) as stages')
            )
            ->leftJoin('z_project_status', 'z_project_status.id', '=', 'z_projects.z_project_status_id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            })
            ->leftJoin('tags', 'tags.id', '=', 'tagged.tag_id')
            ->leftJoin('z_project_visit as visit1', 'visit1.z_projects_id', '=', 'z_projects.id')
            ->leftJoin('z_project_visit as visit2', function ($join) {
                $join
                    ->on('visit2.z_projects_id', '=', 'z_projects.id')
                    ->on('visit1.id', '<', 'visit2.id');
            })
            ->leftJoin('z_project_visit_stage_join', 'z_project_visit_stage_join.z_project_visit_id', '=', 'visit1.id')
            ->leftJoin('z_project_stage', 'z_project_stage.id', '=', 'z_project_visit_stage_join.z_project_stage_id')
            ->whereNull('z_projects.deleted_at')
            ->whereNull('visit2.id')
            ->groupBy('z_projects.id');


        $columns = [
            'id',
            'name',
            'name_short',
            'city',
            'region_abrv',
            'status',
            'tags',
            'stages',
	        'classification',
            'follow_up_date',
            'follow_up_date_custom',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::projects/common.title'));
        $filename = input('filename', trans('bct/projects::projects/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'follow_up_date',
            //'count_group_by_column' => 'z_projects.id',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'id'             => trans('bct/projects::projects/model.general.id'),
                'name'           => trans('bct/projects::projects/model.general.name'),
                'city'           => trans('bct/projects::projects/model.general.city'),
                'region_abrv'    => trans('bct/projects::projects/model.general.region'),
                'tags'           => trans('bct/projects::tags/common.tags'),
                'follow_up_date' => trans('bct/projects::projects/model.general.follow_up_date_grid'),
                'status'         => trans('bct/projects::projects/model.general.status_id')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            if (!input()->get('download', false)) {

                if ($element['tags'] !== '' || $element['tags'] !== null) {
                    $tags = explode(',', $element['tags']);

                    $count_tags = count($tags);
                    foreach ($tags as $index => $tag) {
                        $i = $index + 1;

                        if ((int)$count_tags !== $i) {
                            $tags[$index] .= ',';
                        }

                        if ($i % 3 == 0) {
                            $tags[$index] .= "<br/>";
                        }
                    }

                    $element['tags'] = implode($tags);
                }
                $element['view_uri'] = route('bct.projects.projects.details', $element['id']);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['region_abrv']] = $element->region_abrv;
                $element[$export_columns['tags']] = $element->tags;
                $element[$export_columns['follow_up_date']] = $element->follow_up_date_custom;
                $element[$export_columns['status']] = ($element->status) ? $element->status : '';

            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Datasource for the projects Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridProjectCreate() {
        $data = $this->projects->gridForMap(true);

        $columns = [
            'id',
            'name',
            'city',
            'region_abrv',
            'status',
            'tags',
            'union_percent',
            'union_value',
            'description',
            'follow_up_date',
            'follow_up_date_custom',
            'contractors',
            'stages',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::projects/common.title') . " Search");
        $filename = input('filename', trans('bct/projects::projects/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'             => 'id',
            'direction'        => 'desc',
            'pdf_filename'     => $filename,
            'csv_filename'     => $filename,
            'paginate_exports' => true,
            'title'            => $title,
            'export_source'    => $source,
            'export_columns'   => [
                'id'             => trans('bct/projects::projects/model.general.id'),
                'name'           => trans('bct/projects::projects/model.general.name'),
                'city'           => trans('bct/projects::projects/model.general.city'),
                'region_abrv'    => trans('bct/projects::projects/model.general.region'),
                'tags'           => trans('bct/projects::tags/common.tags'),
                'follow_up_date' => trans('bct/projects::projects/model.general.follow_up_date_grid'),
                'status'         => trans('bct/projects::projects/model.general.status_id')
            ]
        ];

        if (input()->get('download')) {
            $settings['throttle'] = null;
            $settings['threshold'] = null;
            $settings['max_results'] = null;
            $settings['paginate_exports'] = false;
        }

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $element->union_percent = number_format($element->union_value * 100, 2) . '%';
            $element->follow_up_date_custom = $element->follow_up_date_custom;

            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.projects.details', $element['id']);
                $element['description'] = str_limit($element['description'], 120);
                $element['name'] = str_limit($element['name'], 60);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['region_abrv']] = $element->region_abrv;
                $element[$export_columns['tags']] = $element->tags;
                $element[$export_columns['follow_up_date']] = $element->follow_up_date_custom;
                $element[$export_columns['status']] = ($element->status) ? $element->status : '';
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Search project on map
     *
     * Being used: create project, map tab
     *
     * @return mixed
     */
    public function searchOnMap() {
        $data = $this->projects->gridForMap(true);

        foreach ($data as $item) {
            $item->setAppends(['follow_up_date_custom']);
            $item->details_url = route('bct.projects.projects.details', $item->id);
            $item->union_percent = number_format($item->union_value * 100, 2) . '%';
        }

        return $data;
    }


    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        // Do we have a contact identifier?
        if (isset($id)) {
            if (!$project = $this->projects->find($id)) {
                $this->alerts->error(trans('bct/projects::projects/message.not_found', compact('id')));

                return redirect()->route('bct.projects.projects.all');
            }
        } else {
            $project = $this->projects->createModel();

            /** predefined data */
            $project->follow_up_date = Carbon::now()->addWeeks(2)->toDateString();
            $status = $this->projects->statuses()->where('name', 'Active')->first();

            if ($status) $project->z_project_status_id = $status->id;
        }

        $statuses = $this->projects->statuses();
        $classifications = $this->projects->classifications();
        $stages = $this->projects->stages();
        $values = $this->projects->values();
        $types = $this->projects->types();
        $prevailingWages = $this->projects->prevailingWages();
        $referenceTypes = $this->projects->referenceTypes();
        $visibilities = $this->projects->visibilities();
        $all_tags = $this->projects->tags()->pluck('name')->toArray();
        $selected_tags = $project->tags()->get()->pluck('name')->toArray();
        $selected_stages = ($project->getLastVisit()) ? $project->getLastVisit()->stages : collect([]);

        $contractors_count = $project->contractors()->count();
        $disable_classifications = false;
        if ($contractors_count > 0) {
            $disable_classifications = true;
        }

        JavaScriptProjects::put([
            'default_lat' => config_projects('bct.unionimpactbase.map.location.default.lat'),
            'default_lng' => config_projects('bct.unionimpactbase.map.location.default.lng'),
        ]);

        // Show the page
        return view('bct/projects::projects.tabs.details.form', compact(
            'mode', 'project', 'statuses', 'classifications', 'stages', 'values', 'types', 'all_tags', 'selected_tags', 'prevailingWages',
            'referenceTypes', 'visibilities', 'disable_classifications', 'contractors_count', 'selected_stages'
        ));
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        // Store the contact
        list($messages, $project) = $this->projects->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("bct/projects::projects/message.success.{$mode}"));

            if ($project->id) {
                return Redirect::to(URL::route('bct.projects.projects.details', $project->id) . '#details');
            }

            return redirect()->route('bct.projects.projects.all');
        }

        $this->alerts->error($messages, 'form');

        if ($project->id) {
            return redirect()->back();
        }

        return redirect()->route('bct.projects.projects.create')->withInput();
    }

    /**
     * Shows the details tab.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function showDetails($id = null) {
        // Do we have a project identifier?
        if (isset($id) && $project = $this->projects->find($id)) {

            // Show the page
            $mode = 'update';

            $statuses = $this->projects->statuses();

            $classifications = $this->projects->classifications();
            $stages = $this->projects->stages();
            $values = $this->projects->values();
            $types = $this->projects->types();
            $prevailingWages = $this->projects->prevailingWages();
            $referenceTypes = $this->projects->referenceTypes();
            $visibilities = $this->projects->visibilities();

            $project->load([
                'files' => function ($query) {
                    $query->orderBy('created_at', 'desc');
                },
                'lists' => function ($query) {
                    $query->orderBy('join_created_at', 'desc');
                },
            ]);

            $visits = Visit::fromViewProject()
                ->withTrashed()
                ->where('z_projects_id', $id)
                ->orderBy('created_at', 'desc')
                ->get();
            $contractors = $project->contractorsWithNa;
            $contacts = Contact::fromViewProject()
                ->withTrashed()
                ->where('z_projects_id', $id)
                ->orderBy('full_name', 'asc')
                ->get();
            $actions = Action::fromViewProject()
                ->withTrashed()
                ->where('pp_project_actions.z_projects_id', $id)
                ->orderBy('action_date', 'desc')
                ->get();
            $files = $project->files;
            $lists = $project->lists;
            $members = $project->members;

            $contacts_url = route('bct.projects.projects.details.contacts', $id);
            $contractors_url = route('bct.projects.projects.details.contractors', $id);
            $visits_url = route('bct.projects.projects.details.visits', $id);
            $actions_url = route('bct.projects.projects.details.actions', $id);
            $files_url = route('bct.projects.projects.details.files', $id);
            $lists_url = route('bct.projects.projects.details.lists', $id);
            $members_url = route('bct.projects.projects.details.members', $id);


            $is_repuser = Sentinel::inRole('repuser');

            $tag_groups = $project->tags()->groupby('group')->get(['group'])->implode('group', ', ');

            $referer_url = URL::previous();

            if (strpos($referer_url, '/contacts/details') !== false) {
                $back_url = $referer_url;
            } else {
                $back_url = route('bct.projects.projects.all');
            }

            $currentUser = $this->currentUser;

            return view('bct/projects::projects.summary', compact('project', 'mode', 'statuses', 'classifications',
                'stages', 'values', 'types', 'prevailingWages', 'referenceTypes', 'visibilities', 'is_repuser',
                'visits', 'contractors', 'contacts', 'actions', 'files', 'lists', 'contacts_url', 'contractors_url',
                'visits_url', 'actions_url', 'files_url', 'lists_url', 'back_url', 'tag_groups', 'members', 'members_url','currentUser'
            ));
        }

        $this->alerts->error(trans('bct/projects::projects/message.not_found', compact('id')));

        return redirect()->route('bct.projects.projects.all');
    }

    /**
     * Checks if completed project exists in any lists on project update
     *
     * @return Response
     */
    public function checkCompletedProjectExistsInList($id) {
        $project = $this->projects->find($id);
        $lists = $project->all_lists;

        if ($lists->count() > 0) {
            $project_lists = $project->all_lists->lists('name')->implode(', ');

            return $project_lists;
        }


        return response('Failed', 500);
    }

    /**
     * Get project union value
     */
    public function getUnionValue($id) {
        $project = $this->projects->find($id);

        return number_format($project->union_value * 100, 2);
    }

    /**
     * Get project contractors
     */
    public function getContractors($id) {
        $project = $this->projects->find($id);

        $union = number_format($project->union_value * 100, 2) . '%';
        $contractors = $project->contractorsWithNa;
        $html = view('bct/projects::projects.tabs.details.summary-block.contractor-summary', compact('project', 'contractors'))->render();

        return response()->json(['union' => $union, 'html' => $html]);
    }

    /**
     * Get project contacts
     */
    public function getContacts($id) {
        $contacts = Contact::fromViewProject()
            ->withTrashed()
            ->where('z_projects_id', $id)
            ->orderBy('full_name', 'asc')
            ->get();

        $html = view('bct/projects::projects.tabs.details.summary-block.contact-summary', compact('contacts'))->render();

        return response()->json(['html' => $html]);
    }

    /**
     * Get project visits
     */
    public function getVisits($id) {
        $project = $this->projects->find($id);

        $visits = Visit::fromViewProject()
            ->withTrashed()
            ->where('z_projects_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();

        $html = view('bct/projects::projects.tabs.details.summary-block.visit-summary', compact('project', 'visits'))->render();

        return response()->json(['html' => $html]);
    }

    /**
     * Get project actions
     */
    public function getActions($id) {
        $actions = Action::fromViewProject()
            ->withTrashed()
            ->where('pp_project_actions.z_projects_id', $id)
            ->orderBy('action_date', 'desc')
            ->get();

        $html = view('bct/projects::projects.tabs.details.summary-block.actions-history-summary', compact('actions'))->render();

        return response()->json(['html' => $html]);
    }

    /**
     * Get project files
     */
    public function getFiles($id) {
        $project = $this->projects->find($id);

        $project->load([
            'files' => function ($query) {
                $query->orderBy('created_at', 'desc');
            }
        ]);
        $files = $project->files;

        $html = view('bct/projects::projects.tabs.details.summary-block.files-summary', compact('project', 'files'))->render();

        return response()->json(['html' => $html]);
    }

    /**
     * Get project lists
     */
    public function getLists($id) {
        $project = $this->projects->find($id);

        $project->load([
            'lists' => function ($query) {
                $query->orderBy('join_created_at', 'desc');
            },
        ]);

        $lists = $project->lists;

        $html = view('bct/projects::projects.tabs.details.summary-block.list-summary', compact('project', 'lists'))->render();

        return response()->json(['html' => $html]);
    }

    /**
     * Get project members
     */
    public function getMembers($id) {
        $project = $this->projects->find($id);

        $project->load([
            'members' => function ($query) {
                $query->orderByRaw('CONCAT(`z_members`.`first_name`, " ", `z_members`.`last_name`) asc');
            },
        ]);

        $members = $project->members;

        $html = view('bct/projects::projects.tabs.details.summary-block.members-summary', compact('project', 'members'))->render();

        return response()->json(['html' => $html]);
    }

    public function downloadPdfDetails($project_id) {
        return $this->projects->downloadPdfDetails($project_id);
    }

    public function downloadGridPdfDetails() {
        $projects_ids = input('projects_ids');

        if ($projects_ids) {

            if (
                !empty($projects_ids) &&
                (
                    (is_array($projects_ids) && $projects_ids === array_filter($projects_ids, 'is_numeric')) ||
                    $projects_ids === 'all'
                )
            ) {
                if ($projects_ids === 'all') {
                    $results = $this->projects->createModel()->get();
                } else {
                    $results = $this->projects->createModel()
                        ->whereIn('id', $projects_ids)
                        ->get()
                        ->sortBy(function ($model) use ($projects_ids) {
                            return array_search($model->getKey(), $projects_ids);
                        });
                }

                $pdfView = 'bct/projects::pdf/details/projects';
                $title = input('filename', 'Projects Details');
                $filename = input('filename', 'Projects-Details');


                /*
                 * Instead of rendering on main thread, this will push job to PdfDetailsQueueWorker
                 */

                $queueOptions = [
                    'pdf_view'      => $pdfView,
                    'export_source' => 'Projects / Projects Details',
                    'title'         => $title,
                    'user_id'       => Sentinel::getUser()->id,
                ];

                // dispatch pdf render job to a queue worker
                $dispatcher = app('bct.unionimpactbase.export.queue.dispatcher');
                $dispatcher->dispatchJob($filename, 'pdf-details', $results, $queueOptions);

                return response()->json(true);
            }
        }

        return response()->json('error', 500);
    }

    /**
     * Download PDF Project Field Report
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function downloadPdfFieldReport($id) {
        if ($project = $this->projects->find($id)) {
            $tags = ['PLA', 'Representative Job', 'Service Job',
                'Board Check'];
            $classifications = ['Bridge', 'Commercial', 'Heavy & HWY',
                'Industrial', 'Mixed Use', 'Remodel',
                'Residential', 'TI', 'Tradeshow'];
            $job_types = ['Bridge', 'Commercial', 'High Rise',
                'Hospital', 'Mall', 'Marine',
                'Medical', 'Millwright', 'Modular',
                'Parking Structure', 'Renewable Energy', 'Residential',
                'School', 'Tilt', 'Tribal', 'Water Treatment'];
            $prevailing_wages = ['Federal', 'State', 'Private'];
            $visibilities = ['Low', 'Medium', 'High'];
            $statuses = ['Active', 'Completed'];
            $stages = ['Planning', 'Award', 'Abatement', 'Demolition',
                'Site Work', 'Insulation', 'Piling', 'Footing', 'Slab',
                'Concrete Decks', 'Walls', 'Roof', 'Frame Wood',
                'Frame Metal', 'Drywall', 'Taping', 'Acoustical',
                'Flooring', 'Scaffolding', 'Doors/Hardware',
                'Millwork', 'Siding/Paint', 'Cabinets', 'Punch List',
                'General Notes', 'Completed'];
            $scopes = [
                ['id' => 'GC', 'val' => 'GC*'],
                ['id' => 'Concrete - Horizontal Decks', 'val' => 'Concrete - Horizontal'],
                ['id' => 'Concrete - VerticalConcrete', 'val' => 'Concrete - Vertical'],
                ['id' => 'Wood Framing', 'val' => 'Wood Frame'],
                ['id' => 'Metal Framing', 'val' => 'Metal Frame'],
                ['id' => 'Insulation', 'val' => 'Insulation'],
                ['id' => 'Drywall', 'val' => 'Drywall'],
                ['id' => 'Scaffolding', 'val' => 'Scaffolding'],
                ['id' => 'Lath/Plaster/EIFS', 'val' => 'Lathing'],
                ['id' => 'Siding', 'val' => 'Siding'],
                ['id' => 'Acoustical', 'val' => 'Acoustical'],
                ['id' => 'Door & Hardware', 'val' => 'Door Hardware'],
                ['id' => 'Finish Carpentry', 'val' => 'Finish Carpentry'],
                ['id' => 'Cabinet Work', 'val' => 'Cabinet Work'],
                ['id' => 'Roofing', 'val' => 'Roof Structure'],
                ['id' => 'Flooring - Carpet', 'val' => 'Flooring - Carpet'],
                ['id' => 'Flooring - Hardwood', 'val' => 'Flooring - Hardwood'],
                ['id' => 'Flooring - Hard Surface', 'val' => 'Flooring - Hard Surface'],
                ['id' => 'Flooring - Resilient', 'val' => 'Flooring - Resilient'],
                ['id' => 'Millwright', 'val' => 'Millwright'],
                ['id' => 'Pile Driving', 'val' => 'Pile'],
                ['id' => 'Diving/Hyberbaric', 'val' => 'Diving']
            ];
            $contact_types = [
                ['id' => 'Steward', 'val' => 'Steward*'],
                ['id' => 'Architect', 'val' => 'Architect'],
                ['id' => 'Developer', 'val' => 'Developer'],
                ['id' => 'Foreman', 'val' => 'Foreman'],
                ['id' => 'Leasing Agent', 'val' => 'Leasing Agent'],
                ['id' => 'Owner', 'val' => 'Owner'],
                ['id' => 'Project Manager', 'val' => 'Project Manager'],
                ['id' => 'Superintendent', 'val' => 'Superintendent'],
                ['id' => 'Tenant', 'val' => 'Tenant']
            ];

            // Contractors that are linked with this project
            //$contractors = $project->contractors()
            //    ->get(['z_contractors.name', 'z_contractors.license', 'scope.name as scope']);
            $contractors = $project->contractorsWithNa;

            // Contacts that are linked with this project
            $contacts = $project->contacts()
                ->get([DB::raw('CONCAT(first_name, \' \', last_name) as contact'), 'z_contact.company',
                    'type.name as type', 'phone']);

            $title    = trans('bct/projects::projects/common.project_field_report').' as of '.Carbon::today()->format('m-d-Y');
            $filename = snake_case(trans('bct/projects::projects/common.project_field_report').'_'.Carbon::today()->format('m_d_Y'));
            $view     = 'bct/projects::pdf/details/field-report';

            $snappy = app('bct.unionimpactbase.snappy');

            $snappy->setOptions([
                'title'            => $title,
                'filename'         => $filename,
                'orientation'      => 'portrait',
                'margin-left'      => 2,
                'margin-right'     => 2,
                'margin-top'       => 5,
                'margin-bottom'    => 5,
                'footer-left'      => null,
                'footer-center'    => null,
                'header-line'      => true,
                'header-font-size' => 14,
                'header-center'    => $title,
                'view'             => [
                    $view,
                    compact('project', 'tags', 'classifications', 'job_types', 'prevailing_wages', 'visibilities',
                        'statuses', 'stages', 'scopes', 'contact_types', 'contractors', 'contacts')
                ]
            ]);

            return $snappy->export();
        }
    }

    /**
     * Download multiple PDF Project Field Reports by Grid
     */
    public function downloadGridPdfFieldReports() {
        $projects_ids = input('projects_ids');

        if (
            !empty($projects_ids) &&
            (
            (is_array($projects_ids) && $projects_ids === array_filter($projects_ids, 'is_numeric'))
            )
        ) {

            $results = [];

            foreach ($projects_ids as $id) {

                if ($project = $this->projects->find($id)) {
                    $tags = ['PLA', 'Representative Job', 'Service Job',
                        'Board Check'];
                    $classifications = ['Bridge', 'Commercial', 'Heavy & HWY',
                        'Industrial', 'Mixed Use', 'Remodel',
                        'Residential', 'TI', 'Tradeshow'];
                    $job_types = ['Bridge', 'Commercial', 'High Rise',
                        'Hospital', 'Mall', 'Marine',
                        'Medical', 'Millwright', 'Modular',
                        'Parking Structure', 'Renewable Energy', 'Residential',
                        'School', 'Tilt', 'Tribal', 'Water Treatment'];
                    $prevailing_wages = ['Federal', 'State', 'Private'];
                    $visibilities = ['Low', 'Medium', 'High'];
                    $statuses = ['Active', 'Completed'];
                    $stages = ['Planning', 'Award', 'Abatement', 'Demolition',
                        'Site Work', 'Insulation', 'Piling', 'Footing', 'Slab',
                        'Concrete Decks', 'Walls', 'Roof', 'Frame Wood',
                        'Frame Metal', 'Drywall', 'Taping', 'Acoustical',
                        'Flooring', 'Scaffolding', 'Doors/Hardware',
                        'Millwork', 'Siding/Paint', 'Cabinets', 'Punch List',
                        'General Notes', 'Completed'];
                    $scopes = [
                        ['id' => 'GC', 'val' => 'GC*'],
                        ['id' => 'Concrete - Horizontal Decks', 'val' => 'Concrete - Horizontal'],
                        ['id' => 'Concrete - VerticalConcrete', 'val' => 'Concrete - Vertical'],
                        ['id' => 'Wood Framing', 'val' => 'Wood Frame'],
                        ['id' => 'Metal Framing', 'val' => 'Metal Frame'],
                        ['id' => 'Insulation', 'val' => 'Insulation'],
                        ['id' => 'Drywall', 'val' => 'Drywall'],
                        ['id' => 'Scaffolding', 'val' => 'Scaffolding'],
                        ['id' => 'Lath/Plaster/EIFS', 'val' => 'Lathing'],
                        ['id' => 'Siding', 'val' => 'Siding'],
                        ['id' => 'Acoustical', 'val' => 'Acoustical'],
                        ['id' => 'Door & Hardware', 'val' => 'Door Hardware'],
                        ['id' => 'Finish Carpentry', 'val' => 'Finish Carpentry'],
                        ['id' => 'Cabinet Work', 'val' => 'Cabinet Work'],
                        ['id' => 'Roofing', 'val' => 'Roof Structure'],
                        ['id' => 'Flooring - Carpet', 'val' => 'Flooring - Carpet'],
                        ['id' => 'Flooring - Hardwood', 'val' => 'Flooring - Hardwood'],
                        ['id' => 'Flooring - Hard Surface', 'val' => 'Flooring - Hard Surface'],
                        ['id' => 'Flooring - Resilient', 'val' => 'Flooring - Resilient'],
                        ['id' => 'Millwright', 'val' => 'Millwright'],
                        ['id' => 'Pile Driving', 'val' => 'Pile'],
                        ['id' => 'Diving/Hyberbaric', 'val' => 'Diving']
                    ];
                    $contact_types = [
                        ['id' => 'Steward', 'val' => 'Steward*'],
                        ['id' => 'Architect', 'val' => 'Architect'],
                        ['id' => 'Developer', 'val' => 'Developer'],
                        ['id' => 'Foreman', 'val' => 'Foreman'],
                        ['id' => 'Leasing Agent', 'val' => 'Leasing Agent'],
                        ['id' => 'Owner', 'val' => 'Owner'],
                        ['id' => 'Project Manager', 'val' => 'Project Manager'],
                        ['id' => 'Superintendent', 'val' => 'Superintendent'],
                        ['id' => 'Tenant', 'val' => 'Tenant']
                    ];

                    // Contractors that are linked with this project
                    $contractors = $project->contractorsWithNa;

                    // Contacts that are linked with this project
                    $contacts = $project->contacts()
                        ->get([DB::raw('CONCAT(first_name, \' \', last_name) as contact'), 'z_contact.company',
                            'type.name as type', 'phone']);

                    array_push($results, [
                        'project'          => $project,
                        'tags'             => $tags,
                        'classifications'  => $classifications,
                        'job_types'        => $job_types,
                        'prevailing_wages' => $prevailing_wages,
                        'visibilities'     => $visibilities,
                        'statuses'         => $statuses,
                        'stages'           => $stages,
                        'scopes'           => $scopes,
                        'contact_types'    => $contact_types,
                        'contractors'      => $contractors,
                        'contacts'         => $contacts
                    ]);
                }

            }

            $pdfView = 'bct/projects::pdf/details/field-reports';
            $title = input('filename', trans('bct/projects::projects/common.project_field_report') . ' as of ' . Carbon::today()->format('m-d-Y'));
            $filename = $title;

            /*
             * Instead of rendering on main thread, this will push job to PdfDetailsQueueWorker
             */

            $queueOptions = [
                'pdf_view'      => $pdfView,
                'export_source' => 'Projects / Projects Field Reports',
                'title'         => $title,
                'user_id'       => Sentinel::getUser()->id,
            ];

            // dispatch pdf render job to a queue worker
            $dispatcher = app('bct.unionimpactbase.export.queue.dispatcher');
            $dispatcher->dispatchJob($filename, 'pdf-field-reports', $results, $queueOptions);

            return response()->json(true);
        }

        return response()->json('error', 500);
    }

    public function getFollowUpDate($id) {
        return $this->projects->getFollowUpDate($id);
    }

    public function getStatus($id) {
        return $this->projects->getStatus($id);
    }

	public function mergeAction()
	{
		$stay_project_id = input('stay');
		$merge_project_id = input('merge');

		if (!$stay_project_id || !$merge_project_id) {
			return response()->json([ 'success' => false , 'message' => 'Not correct input data'], 422);
		}

		$stay_project = $this->projects->find($stay_project_id);
		$merge_project = $this->projects->find($merge_project_id);

		if (!$stay_project || !$merge_project) {
			return response()->json([ 'success' => false , 'message' => 'Some of the projects does not exist'], 422);
		}
		$merge_project = $this->projects->merge($stay_project,$merge_project);

        if ( ! $merge_project) {
            return response()->json(['success' => false, 'message' => 'Something went wrong, please try again.'], 500);
        }

		$this->alerts->success(trans("bct/projects::projects/message.success.merge"));
		return response()->json([ 'success' => true, 'project'=>$merge_project->name ]);
	}

	public function mergeConfirm($stay_project_id,$merge_project_id){
		return $this->projects->mergeConfirm($stay_project_id,$merge_project_id);
	}
		
}
