<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 10/28/2015
 * Time: 6:04 PM
 */

namespace Bct\Projects\Controllers\Frontend\Projects;


use Bct\Projects\Models\Project;
use Bct\Projects\Models\Stage;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use Bct\Projects\Repositories\Visit\VisitRepositoryInterface;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Platform\Access\Controllers\AuthorizedController;

class ProjectVisitController extends AuthorizedController {

    protected $projects, $visits, $users;

    /**
     * ProjectVisitController constructor.
     * @param ProjectRepositoryInterface $projects
     * @param VisitRepositoryInterface $visits
     * @param UserRepositoryInterface $users
     */
    public function __construct(ProjectRepositoryInterface $projects, VisitRepositoryInterface $visits, UserRepositoryInterface $users) {
        parent::__construct();

        $this->projects = $projects;
        $this->visits = $visits;
        $this->users = $users;
    }

    /**
     * Datasource for the project-contacts Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridVisits($id) {
        $project = $this->projects->find($id);

        $model = $this->visits->createModel();

        $data = $model
            ->fromViewProject()
            ->withTrashed()
            ->where('z_projects_id', $id);

        $columns = [
            'id',
            'users_names',
            'stages_names',
            'details',
            'reason',
            'created_at',
            'created',
            'approved',
            'weather',
            'workers_count',
            'view_uri',
        ];

        $title = input('filename', $project->name . ' - ' . trans('bct/projects::visits/common.title'));
        $filename = input('filename', trans('bct/projects::projects/common.title') . '-' . trans('bct/projects::visits/common.title'));
        $source = 'Projects / Projects';

        $export_columns = [
            'users'         => trans('bct/projects::visits/model.general.reps'),
            'details'       => trans('bct/projects::projects/model.general.follow_up_reason').' & '.trans('bct/projects::visits/model.general.notes'),
            'stages'        => trans('bct/projects::visits/model.general.stage'),
        ];

        if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1) {
            $export_columns['weather'] = trans('bct/projects::visits/model.general.weather');
        }

        if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1) {
            $export_columns['workers_count'] = trans('bct/projects::visits/model.general.workers_count');
        }

        $export_columns['created'] = trans('bct/projects::visits/model.general.date');
        $export_columns['approved'] = trans('bct/projects::projects/model.general.status_id');


        $settings = [
            'sort'           => 'created_at',
            'pdf_view'       => 'cartalyst/data-grid::pdf_html',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => $export_columns
        ];

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $export_columns = $settings['export_columns'];
        $currentUser = $this->currentUser;

        $transformer = function ($element) use ($id, $export_columns, $currentUser) {

            $element['created'] = date('M-d-Y H:i:s', strtotime($element['created_at']));

            if ( ! input()->get('download', false)) {
                if ( ! ( $element->created_by != $currentUser->id && $currentUser->inRole('repuser') )) {
                    $element['view_uri'] = route('bct.projects.projects.editVisit', [ $id, $element['id'] ]);
                }
            } else {
                if (input()->get('download') === 'pdf') {
                    $element[$export_columns['details']] = '<div><b>'.trans('bct/projects::projects/model.general.follow_up').":</b> ".$element->reason.'</div><div><b>'.trans('bct/projects::visits/model.general.notes').':</b> '.$element->details.'</div>';
                } else {
                    $element[$export_columns['details']] = trans('bct/projects::projects/model.general.follow_up').": ".$element->reason.' '.trans('bct/projects::visits/model.general.notes').': '.$element->details;
                }

                $element[$export_columns['users']]         = $element->users_names;
                $element[$export_columns['stages']]        = $element->stages_names;

                if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1) {
                    $element[$export_columns['weather']]       = $element->weather;
                }

                if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1) {
                    $element[$export_columns['workers_count']] = $element->workers_count;
                }

                $element[$export_columns['created']]       = date_correct_details_projects($element->created_at);
                $element[$export_columns['approved']]      = ( $element->approved) ? trans('bct/projects::reviews/model.general.approved') : trans('bct/projects::reviews/model.general.not_approved');
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Show the form for creating new project.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function createVisit($id) {
        return $this->showFormVisit('create', $id);
    }

    /**
     * Handle posting of the form for creating new project.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeVisit($id) {
        return $this->processFormVisit('create', $id);
    }

    /**
     * Show the form for updating project.
     *
     * @param  int $id
     * @param  int $visit_id
     * @return mixed
     */
    public function editVisit($id, $visit_id) {
        return $this->showFormVisit('update', $id, $visit_id);
    }

    /**
     * Handle posting of the form for updating project.
     *
     * @param  int $id
     * @param  int $visit_id
     * @return boolean
     */
    public function updateVisit($id, $visit_id) {
        return $this->processFormVisit('update', $id, $visit_id);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @param null $visit_id
     * @return mixed
     */
    protected function showFormVisit($mode, $id, $visit_id = null) {
        $project = $this->projects->find($id);

        // Do we have a contact identifier?
        if (isset($visit_id)) {
            if (!$visit = $this->visits->find($visit_id)) {
                $this->alerts->error(trans('bct/projects::visits/message.not_found', compact('id')));

                return false;
            }
            $created_user = DB::table('z_project_visit_user_id_join')->where('z_project_visit_id', $visit_id)->first();
            $created_by = ($created_user) ? $created_user->user_id : 0;
            if ($created_by != $this->currentUser->id && $this->currentUser->inRole('repuser')) {
                return response(trans('bct/projects::general/message.fail'), 204);
            }
        } else {
            $visit = $this->visits->createModel();
            $project->follow_up_date = Carbon::now()->addWeeks(2)->toDateString();
        }

        // get all other users regardless of being in the same group
        $users = $this->users->repsWithoutCurrent($this->currentUser->getUserId());

        $lists = $project->lists;
        $stages = $this->projects->stages();
        $is_update = false;
        if (!is_null($visit_id)) {
            $is_update = true;
        }

        $is_repuser = Sentinel::inRole('repuser');

        $weather_options = ['Sunny','Cloudy','Light Rain','Heavy Rain','Snow'];

        // Show the page

        return view('bct/projects::projects.tabs.visits.form', compact('mode', 'visit', 'project', 'stages', 'lists',
            'users', 'is_update', 'is_repuser','weather_options'));
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processFormVisit($mode, $id, $visit_id = null) {

        DB::beginTransaction();
        $project = $this->projects->find($id);
        if (!empty(input('follow_up_date'))) {
            $project->follow_up_date = Carbon::createFromFormat('M-d-Y', input('follow_up_date'));
            $project->save();
        }


        // Store the contact
        $visit_data = array_except(request()->all(), [
            'follow_up_date', 'stages', 'lists', 'users'
        ]);

        if (config_projects('bct.projects.settings.visit_auto_approve')) {
            $visit_data['approved'] = 1;
            $visit_data['approved_user_id'] = Sentinel::getUser()->id;
        }

        list($messages, $visit) = $this->visits->store($id, $visit_id, $visit_data);

        // Do we have any errors?
        if ($messages->isEmpty()) {

            $users = input('users', []);
            $users[] = $this->currentUser->id;

            $visit->stages()->sync(input('stages', []));
            $visit->users()->sync($users);

            foreach ($project->lists as $list) {
                if (in_array($list->id, input('lists', []))) {
                    $list->pivot->is_active = false;
                    $list->pivot->save();
                    $is_active = $this->isNeededToInactivate($list);
                    $list->is_active = $is_active;
                    $list->save();
                }
            }

            if (!$this->adjustProjectStatus($project, input('stages', []))) {
                DB::rollBack();
                if ($mode == 'create') {
                    return response(trans('bct/projects::visits/message.error.create'), 200);
                } else {
                    return response(trans('bct/projects::visits/message.error.update'), 200);
                }
            }

            DB::commit();

            return response('success', 200);
        }

        DB::rollBack();
        if ($mode == 'create') {
            return response(trans('bct/projects::visits/message.error.create'), 200);
        } else {
            return response(trans('bct/projects::visits/message.error.update'), 200);
        }
    }

    /**
     * Adjust project status after a Visit is added
     * If there's a stage of "Completed" in visit, then mark the project status to Completed as well
     *
     * @param $stages
     * @return bool
     */
    protected function adjustProjectStatus($project, $stages) {
        $completedStage = Stage::whereName('Completed')->first();
        $disabledStage = Stage::whereName('Disabled')->first();

        if ($completedStage && in_array($completedStage->id, $stages)) {
            return $project->setStatus(Project::$STATUS_COMPLETED);
        }
        elseif ($disabledStage && in_array($disabledStage->id, $stages)) {
            return $project->setStatus(Project::$STATUS_DISABLED);
        }

        return true;
    }

    private function isNeededToInactivate($list) {
        $projects = DB::table('z_project_list_project_join')->where(['z_project_list_id' => $list->id])->get();
        $is_active = false;
        foreach ($projects as $project) {
            if ($project->is_active == true) {
                $is_active = true;
                break;
            }
        }

        return $is_active;
    }

    /**
     * Handle posting of the form for updating project.
     *
     * @param  int $id
     * @param  int $visit_id
     * @return boolean
     */
    public function deleteVisit($id, $visit_id) {
        $type = $this->visits->delete($visit_id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("bct/projects::projects/message.{$type}.delete")
        );

        return redirect()->route('bct.projects.projects.all');
    }

}