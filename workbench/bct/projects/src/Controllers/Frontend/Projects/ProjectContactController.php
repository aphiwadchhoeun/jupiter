<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 10/28/2015
 * Time: 5:50 PM
 */

namespace Bct\Projects\Controllers\Frontend\Projects;


use Bct\Projects\Models\ContactType;
use Bct\Projects\Repositories\Contact\ContactRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Platform\Access\Controllers\AuthorizedController;

class ProjectContactController extends AuthorizedController {

    protected $projects, $contacts;

    public function __construct(ProjectRepositoryInterface $projects, ContactRepositoryInterface $contacts) {
        parent::__construct();
        $this->projects = $projects;
        $this->contacts = $contacts;
    }

    /**
     * Datasource for the project-contacts Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridContacts($id) {
        $project = $this->projects->find($id);

        $model = $this->contacts->gridReal();

        $data = $model
            ->fromViewProject()
            ->withTrashed()
            ->where('z_projects_id', $project->id)
            ->where('active','=','1');

        $columns = [
            'id',
            'full_name',
            'full_name_short',
            'company',
            'phone',
            'email',
            'type',
            'type_id',
            'edit_uri',
            'view_uri',
        ];

        $title = input('filename', $project->name . ' - ' . trans('bct/projects::contacts/common.title'));
        $filename = input('filename', trans('bct/projects::projects/common.title') . '-' . trans('bct/projects::contacts/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'full_name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'global_columns' => [
                'id',
                'company',
                'phone',
                'email',
                'full_name',
            ],
            'export_columns' => [
                'id'    => trans('bct/projects::contacts/model.general.id'),
                'name'    => trans('bct/projects::contacts/model.general.name'),
                'company' => trans('bct/projects::contacts/model.general.company'),
                'phone'   => trans('bct/projects::contacts/model.general.phone'),
                'email'   => trans('bct/projects::contacts/model.general.email'),
                'type'    => trans('bct/projects::projects/model.general.contact_type')
            ]
        ];

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($id, $export_columns) {

            $element->full_name_short = $element->full_name_short;

            if (!input()->get('download', false)) {
                $element->edit_uri = route('bct.projects.contacts.details', $element->id);
                $element['view_uri'] = route('bct.projects.projects.contact.edit', [$id, $element['id'], $element->pivot_id]);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->full_name;
                $element[$export_columns['company']] = $element->company;
                $element[$export_columns['phone']] = $element->phone;
                $element[$export_columns['email']] = $element->email;
                $element[$export_columns['type']] = $element->type;
            }


            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Return dataset for all contacts
     * @param $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridAllContacts($id) {
        $model = $this->contacts->gridReal();
        $data = $model
            ->fromView()
            ->withTrashed()
            ->where('active','=','1');


        $columns = [
            'id',
            'name',
            'email',
            'phone',
            'company',
            'city',
            'edit_uri',
            'view_uri',
            'created',
        ];

        $title = input('filename', trans('bct/projects::contacts/common.title'));
        $filename = input('filename', trans('bct/projects::contacts/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'throttle'       => 16,
            'threshold'      => 16,
            'max_results'    => 16,
            'title'          => $title,
            'export_source'  => $source,
            'global_columns' => [
                'id',
                'company',
                'phone',
                'email',
                'name',
                'city',
            ],
            'export_columns' => [
                'id'    => trans('bct/projects::contacts/model.general.id'),
                'name'    => trans('bct/projects::contacts/model.general.name'),
                'company' => trans('bct/projects::contacts/model.general.company'),
                'phone'   => trans('bct/projects::contacts/model.general.phone'),
                'email'   => trans('bct/projects::contacts/model.general.email'),
                'city'    => trans('bct/projects::contacts/model.general.city'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($id, $data, $export_columns) {

            $new_date = date('M-d-Y', strtotime($element['created_at']));
            $element['created'] = $new_date;

            if (!input()->get('download', false)) {
                $element->edit_uri = route('bct.projects.contacts.details', $element->id);
                $element['view_uri'] = route('bct.projects.projects.contact.add', [$id, $element['id']]);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['company']] = $element->company;
                $element[$export_columns['phone']] = $element->phone;
                $element[$export_columns['email']] = $element->email;
                $element[$export_columns['city']] = $element->city;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Show the add contact form.
     *
     * @param  int $id
     * @param $contact_id
     * @return mixed
     */
    public function addContact($id, $contact_id) {
        return $this->showContactForm('create', $id, $contact_id);
    }

    /**
     * Store contact to specified project
     *
     * @param $id
     * @param $contact_id
     * @return mixed
     */
    public function storeContact($id, $contact_id) {
        return $this->processFormContacts($id, 'create');
    }

    /**
     * Show the add contact form.
     *
     * @param  int $id
     * @param $contact_id
     * @param $pivot_id
     * @return mixed
     */
    public function editContact($id, $contact_id, $pivot_id) {
        return $this->showContactForm('update', $id, $contact_id, $pivot_id);
    }

    /**
     * Handle posting of the form for updating contact.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContact($id, $contact_id, $pivot_id) {
        return $this->processFormContacts($id, 'update');
    }

    /**
     * Show the contacts of project.
     *
     * @param  int $id
     * @param $mode
     * @return mixed
     */
    protected function processFormContacts($id, $mode) {
        list($result, $msg) = $this->projects->storeContacts($id, request()->all(), $mode);

        if ($result) {
            return response('success', 200);
        }

        return response($msg, 200);
    }

    /**
     * Show the add contact form.
     *
     * @param  int $id
     * @param $pivot_id
     * @return mixed
     */
    public function deleteContact($id, $pivot_id) {
        if ($this->projects->deleteContact($pivot_id)) {
            return response('success', 200);
        }

        return response('fail', 200);
    }

    /**
     * Shows the contact popup.
     *
     * @param  string $mode
     * @param  int $id
     * @param $contact_id
     * @param null $pivot_id
     * @return mixed
     */
    protected function showContactForm($mode, $id, $contact_id, $pivot_id = null) {
        if (isset($id) && $project = $this->projects->find($id)) {
            if ($contact = $this->contacts->find($contact_id)) {
                $pivot = DB::table('z_project_contact_join')->where('id', $pivot_id)->first();
                $contactTypes = $this->projects->contactTypes();

                // Show the page
                return view('bct/projects::projects.tabs.contacts.add-contact',
                    compact('mode', 'contact', 'project', 'pivot', 'contactTypes')
                );
            } else {
                return response(trans('bct/projects::contacts/message.not_found', ['id' => $contact_id]), 204);
            }
        }

        return response(trans('bct/projects::contacts/message.not_found', ['id' => $contact_id]), 204);
    }

    public function getContactTypesData() {
        if ($id = $query = input('id')) {
            return ContactType::find($id);
        }

        return $this->projects->contactTypes(input('q'));
    }

}