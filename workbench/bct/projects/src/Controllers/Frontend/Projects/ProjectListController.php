<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 10/28/2015
 * Time: 6:19 PM
 */

namespace Bct\Projects\Controllers\Frontend\Projects;


use Bct\Projects\Repositories\Group\GroupRepositoryInterface;
use Bct\Projects\Repositories\Lists\ListsRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Platform\Access\Controllers\AuthorizedController;

class ProjectListController extends AuthorizedController {

    protected $projects, $lists, $groups;

    /**
     * ProjectListController constructor.
     * @param ProjectRepositoryInterface $projects
     * @param ListsRepositoryInterface $lists
     * @param GroupRepositoryInterface $groups
     */
    public function __construct(ProjectRepositoryInterface $projects, ListsRepositoryInterface $lists, GroupRepositoryInterface $groups) {
        parent::__construct();

        $this->projects = $projects;
        $this->lists = $lists;
        $this->groups = $groups;
    }

    /**
     * Datasource for the project-lists Data Grid.
     *
     * @param $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridLists($id) {
        $project = $this->projects->find($id);

        $data = $this->lists->gridReal()
            ->fromViewProject()
            ->withTrashed()
            ->where('z_projects_id', $project->id);

        $columns = [
            'id',
            'name',
            'date_added',
            'date_added_custom',
            'username',
            'join_id',
            'join_created_by',
            'delete_uri',
        ];

        $title = input('filename', $project->name . ' - ' . trans('bct/projects::lists/common.title'));
        $filename = input('filename', trans('bct/projects::projects/common.title') . '-' . trans('bct/projects::lists/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'       => trans('bct/projects::lists/model.general.name'),
                'user'       => trans('bct/projects::lists/model.general.username'),
                'date_added' => trans('bct/projects::lists/model.general.date_added'),
            ]
        ];

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($id, $export_columns) {
            $element->date_added_custom = date_correct_details_projects($element['date_added']);

            if (!input()->get('download', false)) {
                $current = $element['join_created_by'];
                if ($current == $this->currentUser->id
                    || $this->currentUser->inRole('admin')
                    || $this->currentUser->inRole('repadmin')
                    || $this->currentUser->inRole('super-repadmin')) {
                    $element['delete_uri'] = route('bct.projects.projects.deleteLists', [$id, $element['join_id']]);
                }
            } else {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['user']] = $element->username;
                $element[$export_columns['date_added']] = $element->date_added_custom;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Show the form for attach new lists.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function createLists($id) {
        return $this->showFormLists('create', $id);
    }

    /**
     * Handle posting of the form for creating new lists.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeLists($id) {
        return $this->processFormLists('create', $id);
    }

    /**
     * Handle posting of the form for updating lists.
     *
     * @param  int $id
     * @param  int $pivot_id
     * @return boolean
     */
    public function deleteLists($id, $pivot_id) {
        if ($this->projects->deleteLists($pivot_id)) {
            return response('success', 200);
        }

        return response('fail', 200);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showFormLists($mode, $id, $pivot_id = null) {
        $project = $this->projects->find($id);

        // Do we have a pivot identifier?
        if (isset($pivot_id)) {
            if (!$pivot = DB::table('z_project_list_project_join')->where('id', $pivot_id)->first()) {
                return response(trans('bct/projects::lists/message.not_found', compact('id')), 204);
            }
        }

        $lists = $this->lists
            ->whereNotIn('id', $project->lists()->lists('id'))
            ->where('is_active', true)
            ->whereNull('deleted_at')
            ->get();

        // Show the page
        return view('bct/projects::projects.tabs.lists.form', compact('mode', 'project', 'lists', 'pivot'));
    }

    protected function getGroupLists($id) {
        if (request()->ajax()) {
            $lists = $this->lists->where('z_projects_group_id', $id)->where('is_active', true)->whereNull('deleted_at')->orderBy('name', 'asc')->get();

            return [
                "success" => true,
                "lists"   => $lists
            ];
        } else {
            return redirect()->back();
        }
    }

    protected function getGroups($group_type) {
        if (request()->ajax()) {
            $groups = $this->groups->where('individual', $group_type)->where('status', true)->whereNull('deleted_at')->orderBy('name', 'asc')->get();

            return [
                "success" => true,
                "lists"   => $groups
            ];
        } else {
            return redirect()->back();
        }
    }

    /**
     * Check if projects already associate with list
     */
    protected function checkProjectsContainInList() {
        if (request()->ajax()) {
            if ($this->projects->checkIfExistsInList(input('list_id'), input('rows'))) {
                return response('success', 200);
            }

            return response('fail', 200);
        } else {
            return redirect()->back();
        }
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @param  int $pivot_id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processFormLists($mode, $id, $pivot_id = null) {
        $project = $this->projects->find($id);

        // Store the contact
        $this->projects->storeLists($project->id, $pivot_id, request()->all());

        return response('success', 200);
    }

}