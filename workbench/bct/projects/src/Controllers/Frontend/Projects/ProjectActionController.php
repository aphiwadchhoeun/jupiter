<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/2/2015
 * Time: 2:40 PM
 */

namespace Bct\Projects\Controllers\Frontend\Projects;


use Bct\Projects\Models\ActionType;
use Bct\Projects\Repositories\Action\ActionRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use DB;
use Platform\Access\Controllers\AuthorizedController;

class ProjectActionController extends AuthorizedController {

    protected $projects, $actions;

    /**
     * ProjectActionController constructor.
     * @param ProjectRepositoryInterface $projects
     * @param ActionRepositoryInterface $action
     */
    public function __construct(ProjectRepositoryInterface $projects, ActionRepositoryInterface $action) {
        parent::__construct();

        $this->projects = $projects;
        $this->action = $action;
    }

    /**
     * Datasource for the project-actions Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridActions($id) {
        $project = $this->projects->find($id);

        $model = $this->action->createModel();
        $data = $model
            ->fromViewProject()
            ->withTrashed()
            ->where('pp_project_actions.z_projects_id', $project->id);

        $columns = [
            'id',
            'details',
            'action_type',
            'username',
            'action_date',
            'created',
            'contractor',
            'view_uri',
        ];

        $title = input('filename', $project->name . ' - ' . trans('bct/projects::visits/common.title'));
        $filename = input('filename', trans('bct/projects::actions/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'action_date',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'max_results'    => 16,
            'throttle'       => 16,
            'threshold'      => 16,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'type'       => trans('bct/projects::actions/model.general.action_type'),
                'details'    => trans('bct/projects::actions/model.general.details'),
                'user'       => trans('bct/projects::actions/model.general.user'),
                'contractor' => trans('bct/projects::actions/model.general.contractor_id'),
                'action_date'    => trans('bct/projects::actions/model.general.date')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $user = $this->currentUser;

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $transformer = function ($element) use ($id, $export_columns, $user) {

            $new_date = date('M-d-Y', strtotime($element['action_date']));
            $element['action_date'] = $new_date;
            $element['details'] = $element['details'];

            if (!input()->get('download', false)) {
                /** check for user */
                if ($element['user_id'] == $user->id
                    || $user->inRole('admin')
                    || $user->inRole('repadmin')
                    || $user->inRole('super-repadmin')) {
                    $element['view_uri'] = route('bct.projects.projects.editAction', [$id, $element['id']]);
                }
            } else {
                $element[$export_columns['type']] = $element->action_type_name;
                $element[$export_columns['details']] = $element->details;
                $element[$export_columns['user']] = $element->username;
                $element[$export_columns['contractor']] = $element->contractor;
                $element[$export_columns['action_date']] = date_correct_details_projects($element->action_date);
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Show the form for creating new action.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function createAction($id) {
        return $this->showFormAction('create', $id);
    }

    /**
     * Handle posting of the form for creating new action.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAction($id) {
        return $this->processFormAction('create', $id);
    }

    /**
     * Show the form for updating project.
     *
     * @param  int $id
     * @param  int $action_id
     * @return mixed
     */
    public function editAction($id, $action_id) {
        return $this->showFormAction('update', $id, $action_id);
    }

    /**
     * Handle posting of the form for updating project.
     *
     * @param  int $id
     * @param  int $action_id
     * @return boolean
     */
    public function updateAction($id, $action_id) {
        return $this->processFormAction('update', $id, $action_id);
    }

    /**
     * Handle posting of the form for updating project.
     *
     * @param  int $id
     * @param  int $action_id
     * @return boolean
     */
    public function deleteAction($id, $action_id) {
        if ($this->action->delete($action_id)) {
            return response('success', 200);
        }

        return response('fail', 200);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @param  int $action_id
     * @return mixed
     */
    protected function showFormAction($mode, $id, $action_id = null) {
        $project = $this->projects->find($id);

        // Do we have a contact identifier?
        if (isset($action_id)) {
            if (!$action = $this->action->find($action_id)) {
                return response(trans('bct/projects::actions/message.not_found', compact('id')), 204);
            }
        } else {
            $action = $this->action->createModel();
        }

        $action_types = ActionType::all();
        $contractors = $project->contractors()->where('z_contractors.active','=','1')->get()->unique();

        // Show the page
        return view('bct/projects::projects.tabs.actions.form', compact('mode', 'action', 'project','action_types','contractors'));
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @param  int $action_id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processFormAction($mode, $id, $action_id = null) {
        // Store the contact
        list($messages) = $this->action->store($id, $action_id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            return response('success', 200);
        }

        if ($mode == 'create') {
            return response('bct/projects::actions/message/error.create');
        } else {
            return response('bct/projects::actions/message/error.update');
        }
    }

}