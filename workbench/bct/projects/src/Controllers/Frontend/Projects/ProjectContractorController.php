<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 10/28/2015
 * Time: 5:25 PM
 */

namespace Bct\Projects\Controllers\Frontend\Projects;


use Bct\Projects\Repositories\Contractor\ContractorRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Platform\Access\Controllers\AuthorizedController;

class ProjectContractorController extends AuthorizedController {

    protected $projects, $contractors;

    /** CONTRACTORS ACTIONS */
    /**
     * ProjectContractorController constructor.
     * @param ProjectRepositoryInterface $projects
     * @param ContractorRepositoryInterface $contractors
     */
    public function __construct(ProjectRepositoryInterface $projects, ContractorRepositoryInterface $contractors) {
        parent::__construct();

        $this->projects = $projects;
        $this->contractors = $contractors;
    }


    /**
     * Datasource for the project-contractors Data Grid.
     *
     * @param  int $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridContractors($id) {
        $project = $this->projects->find($id);

        $data = $this->contractors->createModel()
            ->select(
                'z_contractors.id',
                'z_project_contractor_join.id as pivot_id',
                'z_contractors.name',
                'z_contractors.phone',
                'z_project_contractor_join.is_union',
                'z_project_contractor_join.is_agreement',
                DB::raw('scope.id as scope_id'),
                DB::raw('scope.name as scope'),
                DB::raw('sc.value as value')
            )
            ->rightJoin('z_project_contractor_join', 'z_project_contractor_join.z_contractors_id', '=', 'z_contractors.id')
            ->join('z_scopes as scope', 'z_project_contractor_join.z_scopes_id', '=', 'scope.id')
            ->join('z_scopes_classifications_join as sc', 'sc.z_scopes_id', '=', 'z_project_contractor_join.z_scopes_id')
            ->whereNull('z_contractors.deleted_at')
            ->where('z_contractors.active','=','1')
            ->where('z_project_contractor_join.z_projects_id', $id)
            ->where('sc.z_classifications_id', $project->z_project_classification_id)
            ->groupBy('z_project_contractor_join.z_contractors_id', 'z_project_contractor_join.z_scopes_id')
            ->orderBy('scope.name', 'asc');

        if ((int) input('na_only') === 1) {
            $data = $data
                ->whereNull('z_contractors.id');
        }

        $columns = [
            'id',
            'pivot_id',
            'name',
            'is_union',
            'phone',
            'scope',
            'value',
            'has_agreement',
            'view_uri',
            'edit_uri',
            'is_agreement',
            'union_color',
            'union_text'
        ];

        $title = input('filename', $project->name . ' - ' . trans('bct/projects::contractors/common.title'));
        $filename = input('filename', trans('bct/projects::projects/common.title') . '-' . trans('bct/projects::contractors/common.title'));
        $source = 'Projects / Projects';
        $settings = [
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'id' => trans('bct/projects::projects/model.general.id'),
                'scope' => trans('bct/projects::projects/model.general.scope_id'),
                'name'  => trans('bct/projects::contractors/model.general.name'),
                'phone' => trans('bct/projects::contractors/model.general.phone'),
                'union' => trans('bct/projects::contractors/model.general.union'),
                'pa'    => trans('bct/projects::projects/model.general.pa'),
            ]
        ];

        if ($limit = input('limit')) {
            $settings['threshold'] = $settings['throttle'] = $limit;
        }

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($id, $export_columns) {

            $element['name'] = (is_null($element->id)) ? 'N/A' : str_limit($element['name'], 35);

            $element->scope = $element->scope;
            $element->value = $element->value;
            $element->is_union = (is_null($element->id)) ? null : $element->is_union;
            $element->is_agreement = (is_null($element->id)) ? null : $element->is_agreement;
            $element->has_agreement = (is_null($element->id)) ? null : $element->is_agreement;
            $element->union_text = (is_null($element->id)) ? null : (($element->is_union) ? trans('bct/projects::general/common.yes') : trans('bct/projects::general/common.no'));
            $element->union_color = (is_null($element->id)) ? null : (($element->is_union) ? 'success' : 'danger');

            if (!input()->get('download', false)) {
                
                if (!is_null($element['id'])) {
                    $element->view_uri = route('bct.projects.projects.contractor.edit', [
                        $id, $element['id'], $element->pivot_id
                    ]);
                } else {
                    $element->view_uri = route('bct.projects.projects.contractor.edit.na', [
                        $id, $element->pivot_id
                    ]);
                }
                

                $element->edit_uri = route('bct.projects.contractors.details', $element->id);
            } else {
                $element['is_union'] = ($element['is_union'] == 1) ? 'Yes' : 'No';
                $element['has_agreement'] = ($element['has_agreement'] == 1) ? 'Yes' : 'No';

                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['scope']] = $element->scope;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['phone']] = $element->phone;
                $element[$export_columns['union']] = $element->is_union;
                $element[$export_columns['pa']] = $element->has_agreement;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Datasource for the project-contractors Data Grid.
     *
     * @param  int $id
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridAllContractors($id) {
        $data = $this->contractors->grid()->select(
            'id',
            'name',
            'city',
            'phone',
            'state',
            'license'
        )->where('z_contractors.active','=','1');

        $columns = [
            'id',
            'name',
            'city',
            'phone',
            'state',
            'license',
            'view_uri',
            'edit_uri',
        ];

        $title = input('filename', trans('bct/projects::contractors/common.available_contractors'));
        $filename = input('filename', trans('bct/projects::contractors/common.available_contractors'));
        $source = 'Projects / Projects';
        $settings = [
            'sort'           => 'name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'id'    => trans('bct/projects::contractors/model.general.id'),
                'name'    => trans('bct/projects::contractors/model.general.name'),
                'city'    => trans('bct/projects::contractors/model.general.city'),
                'state'   => trans('bct/projects::contractors/model.general.state'),
                'phone'   => trans('bct/projects::contractors/model.general.phone'),
                'license' => trans('bct/projects::contractors/model.general.license'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($id, $export_columns) {
            if (!input()->get('download', false)) {
                $element->edit_uri = route('bct.projects.contractors.details', $element->id);
                $element->view_uri = route('bct.projects.projects.contractor.add', [$id, $element->id]);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['state']] = $element->state;
                $element[$export_columns['phone']] = $element->phone;
                $element[$export_columns['license']] = $element->license;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /** CONTRACTORS ACTIONS */

    /**
     * Show the form for creating new contractors.
     *
     * @param  int $id
     * @param  int $contractor_id
     * @return \Illuminate\View\View
     */
    public function addContractor($id, $contractor_id) {
        return $this->showFormContractor('create', $id, $contractor_id);
    }

    public function addContractorNa($id) {
        return $this->showFormContractorNa('create', $id);
    }

    /**
     * Handle posting of the form for creating new contractors.
     *
     * @param  int $id
     * @param  int $contractor_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeContractor($id, $contractor_id) {
        return $this->processFormContractor($id, 'create');
    }

    public function storeContractorNa($id) {
        return $this->processFormContractor($id, 'create', true);
    }

    /**
     * Show the form for updating contractors.
     *
     * @param  int $id
     * @param $contractor_id
     * @param  int $pivot_id
     * @return mixed
     */
    public function editContractor($id, $contractor_id, $pivot_id) {
        return $this->showFormContractor('update', $id, $contractor_id, $pivot_id);
    }

    public function editContractorNa($id, $pivot_id) {
        return $this->showFormContractorNa('update', $id, $pivot_id);
    }

    /**
     * Handle posting of the form for updating contractors.
     *
     * @param  int $id
     * @param  int $pivot_id
     * @return boolean
     */
    public function updateContractor($id, $contractor_id, $pivot_id) {
        return $this->processFormContractor($id, 'update');
    }

    public function updateContractorNa($id, $pivot_id) {
        return $this->processFormContractor($id, 'update', true);
    }

    /**
     * Handle posting of the form for updating contractors.
     *
     * @param  int $id
     * @param  int $pivot_id
     * @return boolean
     */
    public function deleteContractor($id, $pivot_id) {
        if ($this->projects->deleteContractor($pivot_id)) {
            return response('success', 200);
        }

        return response('fail', 200);
    }


    /**
     * Shows the form.
     *
     * @param      $mode
     * @param      $id
     * @param      $contractor_id
     * @param null $pivot_id
     * @param bool $is_na
     *
     * @return mixed
     */
    protected function showFormContractor($mode, $id, $contractor_id, $pivot_id = null) {
        if (isset($id) && $project = $this->projects->find($id)) {
            if ($contractor = $this->contractors->find($contractor_id)) {
                if ($mode == 'update') {
                    $pivot = DB::table('z_project_contractor_join')->where('id', $pivot_id)->first();
                }

                $scopes = $this->projects->scopes($project);

                // Show the page
                return view('bct/projects::projects.tabs.contractors.form', compact('mode', 'contractor', 'project', 'scopes', 'pivot', 'is_na'));
            } else {
                return response(trans('bct/projects::contractors/message.not_found', compact('id')), 204);
            }
        }

        return response(trans('bct/projects::contractors/message.not_found', compact('id')), 204);
    }

    protected function showFormContractorNa($mode, $id, $pivot_id = null) {
        if (isset($id) && $project = $this->projects->find($id)) {

            $is_na = true;
            $scopes = $this->projects->scopes($project);

            if ($mode == 'update') {
                $pivot = DB::table('z_project_contractor_join')->where('id', $pivot_id)->first();
            }

            return view('bct/projects::projects.tabs.contractors.form_na', compact('mode', 'contractor', 'project', 'scopes', 'pivot', 'is_na'));
        }

        return response(trans('bct/projects::contractors/message.not_found', compact('id')), 204);
    }

    /**
     * Processes the form.
     *
     * @param  int $id
     * @param $data
     * @param $mode
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processFormContractor($id, $mode, $is_na = false) {
        
        if ($is_na === true) {
            list($result, $msg) = $this->projects->storeContractorNa($id, request()->all(), $mode);
        } else {
            list($result, $msg) = $this->projects->storeContractor($id, request()->all(), $mode);
        }

        if ($result) {
            return response('success', 200);
        }

        return response($msg, 200);
    }

}