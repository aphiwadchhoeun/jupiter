<?php

namespace Bct\Projects\Controllers\Frontend;

use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Models\User;
use Bct\Projects\Repositories\Group\GroupRepositoryInterface;
use Bct\Projects\Repositories\Lists\ListsRepositoryInterface;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;

class ListsController extends BaseController {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Lists\ListsRepositoryInterface
     */
    protected $lists;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Group\GroupRepositoryInterface
     */
    protected $groups;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
     */
    protected $users;

    /**
     * The Mrp repository.
     *
     * @var \Bct\Projects\Repositories\Project\ProjectRepositoryInterface
     */
    protected $projects;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete', 'attach', 'detach', 'update'
    ];
    private $limit = 5;

    /**
     * Constructor.
     *
     * @param ListsRepositoryInterface $lists
     * @param GroupRepositoryInterface $groups
     * @param UserRepositoryInterface $users
     * @param ProjectRepositoryInterface $projects
     */
    public function __construct(ListsRepositoryInterface $lists,
                                GroupRepositoryInterface $groups,
                                UserRepositoryInterface $users,
                                ProjectRepositoryInterface $projects) {
        parent::__construct();

        $this->lists = $lists;
        $this->groups = $groups;
        $this->users = $users;
        $this->projects = $projects;
    }

    /**
     * Return the main search view.
     *
     * @return \Illuminate\View\View
     */
    public function search() {
        $has_access = false;

        if (Sentinel::inRole('admin')
            || Sentinel::inRole('repadmin')
            || Sentinel::inRole('super-repadmin')
            || Sentinel::inRole('steward')) {
            $has_access = true;
        }
        $currentUser = $this->currentUser;

        return view('bct/projects::lists.search', compact('has_access','currentUser'));
    }

    /**
     * Show the form for creating new contact.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new contact.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating contact.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating contact.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified contact.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $this->lists->delete($id);
        $this->alerts->success(
            trans("bct/projects::lists/message.delete")
        );

        return [];
    }

    /**
     * Show the details for list.
     *
     * @param  int $id
     * @return mixed
     */
    public function details($id) {
        return $this->showDetails($id);
    }

    /**
     * Handle posting of the form for updating project.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateView($id) {
        return $this->processAttach($id);
    }

    /**
     * Datasource for the lists Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridForUsers() {
        $model = $this->lists->createModel();
        if ($this->currentUser->inRole('repuser')) {
            $user = $this->currentUserModule;
            $groups = $user->groups->toArray();
            $data = $model
                ->leftJoin('z_projects_group', 'z_projects_group.id', '=', $model->getTable() . '.z_projects_group_id')
                ->whereIn('z_projects_group_id', array_column($groups, 'id'))
                ->where('is_active', 1)
                ->select(
                    $model->getTable() . '.id',
                    $model->getTable() . '.name',
                    $model->getTable() . '.category',
                    $model->getTable() . '.is_active',
                    $model->getTable() . '.due_date',
                    'z_projects_group.name as group'
                );
        } else {
            $data = $model
                ->where('is_active', 1)
                ->leftJoin('z_projects_group', 'z_projects_group.id', '=', $model->getTable() . '.z_projects_group_id')
                ->select(
                    $model->getTable() . '.id',
                    $model->getTable() . '.name',
                    $model->getTable() . '.category',
                    $model->getTable() . '.is_active',
                    $model->getTable() . '.due_date',
                    'z_projects_group.name as group'
                );
        }

        $columns = [
            'id',
            'name',
            'group',
            'category',
            'list_uri',
            'view_uri',
            'selected',
            'due_date',
            'due_date_custom',
            'is_active',
        ];

        $title = input('filename', trans('bct/projects::lists/common.title'));
        $filename = input('filename', trans('bct/projects::lists/common.title'));
        $source = 'Projects / Lists';
        $settings = [
            'sort'           => 'due_date',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'     => trans('bct/projects::lists/model.general.name'),
                'group'    => trans('bct/projects::lists/model.general.group_id'),
                'category' => trans('bct/projects::lists/model.general.category'),
                'due_date' => trans('bct/projects::lists/model.general.due_date'),
                'status'   => trans('bct/projects::lists/model.general.list_status'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            if ($element->is_active) {
                $element->is_active = trans('bct/projects::general/common.active');
            } else {
                $element->is_active = trans('bct/projects::general/common.disabled');
            }

            $element['due_date_custom'] = date('M-d-Y', strtotime($element['due_date']));
            $element['name'] = str_limit($element['name'], 35);

            if (!input()->get('download', false)) {
                $element['list_uri'] = route('bct.projects.lists.details', $element['id']);
                $element['view_uri'] = route('bct.projects.lists.gridProjects', $element['id']);

                if ($this->currentUser->z_project_list_id == $element['id']) {
                    $element['selected'] = true;
                }
            } else {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['group']] = $element->group;
                $element[$export_columns['category']] = $element->category;
                $element[$export_columns['status']] = $element->is_active;
                $element[$export_columns['due_date']] = $element->due_date_custom;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Datasource for the lists Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function gridProjects($id) {
        $list = $this->lists->find($id);

        $data = $list->projects_view();

        $columns = [
            'id',
            'name',
            'city',
            'tags',
            'follow_up_date',
            'follow_up_date_custom',
            'lastVisit_custom',
            'view_uri',
            'list_status',
        ];

        $title = input('filename', trans('bct/projects::lists/common.title') . '-' . trans('bct/projects::projects/common.title'));
        $filename = input('filename', trans('bct/projects::lists/common.title') . ' - ' . trans('bct/projects::projects/common.title'));
        $source = 'Projects / Lists';
        $settings = [
            'sort'           => 'follow_up_date',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'id'           => trans('bct/projects::projects/model.general.id'),
                'name'           => trans('bct/projects::projects/model.general.name'),
                'city'           => trans('bct/projects::projects/model.general.city'),
                'follow_up_date' => trans('bct/projects::projects/model.general.follow_up_date'),
                'lastVisit'      => trans('bct/projects::projects/model.general.last_visit'),
                'tags'           => trans('bct/projects::tags/common.tags'),
                'status'         => trans('bct/projects::projects/model.general.status_id'),
            ]

        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            if ($element->visits()->get()->count() > 0) {
                $element->lastVisit = $element->visits()->get()->last()->created_at->format('Y-m-d');
            }

            $element['follow_up_date_custom'] = !empty($element['follow_up_date']) ? date('M-d-Y', strtotime($element['follow_up_date'])) : 'n/a';
            $element['lastVisit_custom'] = !empty($element['lastVisit']) ? date('M-d-Y', strtotime($element['lastVisit'])) : 'n/a';

            $element->list_status = ($element->pivot->is_active) ? 'Not visited' : 'Visited';

            if (!input()->get('download', false)) {
                //Add line break after every third tag
                if($element['tags'] !== '' || $element['tags'] !== null){
                    $tags = explode(',',$element['tags']);

                    $count_tags = count($tags);
                    foreach($tags as $index=>$tag){
                        $i = $index+1;

                        if ((int)$count_tags !== $i) {
                            $tags[$index] .= ',';
                        }

                        if($i % 3 == 0){
                            $tags[$index].="<br/>";
                        }
                    }

                    $element['tags'] = implode($tags);
                }

                $element['view_uri'] = route('bct.projects.projects.details', $element['id']);
            } else {
                $element[$export_columns['id']] = $element->id;
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['follow_up_date']] = $element->follow_up_date_custom;
                $element[$export_columns['tags']] = $element->tags;
                $element[$export_columns['lastVisit']] = $element->lastVisit_custom;
                $element[$export_columns['status']] = $element->list_status;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        // Do we have a contact identifier?
        if (isset($id)) {
            if (!$list = $this->lists->find($id)) {
                $this->alerts->error(trans('bct/projects::lists/message.not_found', compact('id')));

                return redirect()->route('bct.projects.lists.all');
            }
        } else {
            $list = $this->lists->createModel();
        }

        if (Sentinel::inRole('repuser')) {
            $user_id = $this->currentUser->id;
            $user = $this->users->find($user_id);
            $groups = $user->groups()->where('status', true)->get();
            if (count($groups) == 0) {

                return $this->alerts->error(trans('bct/projects::lists/message.no_groups'));
            }
        } else {
            $groups = $this->groups->where('status', true)->orderBy('name', 'ASC')->get();
        }

        // Show the page
        return view('bct/projects::lists.modal', compact('mode', 'list', 'groups'));
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        // Store the list
        request()->merge(["is_active" => true]);
        if ($this->currentUser->inRole('repuser')) {
            request()->merge(["created_by" => $this->currentUser->id]);
        }
        list($messages, $list) = $this->lists->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("bct/projects::lists/message.success.{$mode}"));

            return route('bct.projects.lists.details', $list->id);
        }

        if ($mode == 'create') {
            $error_message = '';
            foreach ($messages->all() as $message) {
                $error_message .= $message;
            }
            $this->alerts->error($error_message);
        } else {
            $this->alerts->error($messages, 'form');
        }
        if ($mode == "update") {
            return ["error" => true];
        }

        return route('bct.projects.lists.details', $list->id);


        return [];
    }

    /**
     * Attach project to list
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processAttach($id) {
        // Attach user to group
        $action = input('action');
        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                $this->lists->{$action}($id, $row);
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Shows the details tab.
     *
     * @param  int $id
     * @return mixed
     */
    protected function showDetails($id = null) {
        // Do we have a project identifier?
        if (isset($id) && $list = $this->lists->find($id)) {

            // Show the page
            $mode = 'update';
            $change_status = false;

            if (Sentinel::inRole('repuser') && $this->currentUser->id != $list->created_by) {
                $has_access = false;
            } else {
                $has_access = true;
            }

            $reps_count = $list->group->users->count();

            if ($reps_count > $this->limit) {
                $grid_reps_count = $this->limit;
            } else {
                $grid_reps_count = $reps_count;
            }

            $projects_count = $list->projects()->get()->count();

            if ($projects_count > $this->limit) {
                $grid_projects_count = $this->limit;
            } else {
                $grid_projects_count = $projects_count;
            }
            $groups = $this->groups->where('status', true)->get();
            if ($this->currentUser->inRole('repuser') || $this->currentUser->inRole('registered') || $this->currentUser->inRole('steward')) {
                $groups = User::find($this->currentUser->id)->groups()->get();
            }
            if (!$list->is_active && !Sentinel::inRole('repuser')) {
                $change_status = true;
            }
            if ($change_status) {
                $count = DB::table('z_project_list_project_join as middle')
                    ->where('z_project_list_id', $id)->where('is_active', 1)->count();

                if ($count <= 0)
                    $change_status = false;
            }


            return view('bct/projects::lists.summary', compact('list', 'mode', 'has_access', 'groups', 'reps_count', 'grid_reps_count', 'projects_count', 'grid_projects_count', 'change_status'));
        }

        $this->alerts->error(trans('bct/projects::lists/message.not_found', compact('id')));

        return redirect()->route('bct.projects.lists.all');
    }

    public function gridForProjects($id) {
        $list = $this->lists->find($id);
        $limit = input('limit', 5);

        $data = $list->projects_view();

        $columns = [
            'id',
            'name',
            'status',
            'follow_up_date',
        ];

        $settings = [
            'sort'      => 'follow_up_date',
            'direction' => 'asc',
            'throttle'  => $limit,
            'threshold' => $limit
        ];

        $transformer = function ($element) {

            if (!input()->get('download', false)) {
                $element->status = ($element->is_active) ? "Not visited" : "Visited";

                $follow_date = date('M-d-Y', strtotime($element['follow_up_date']));
                $element['follow_up_date'] = $follow_date;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function gridForProjectsTab($id) {
        $list=$this->lists->find($id);
        if (input('reverse')) {
            $ids = DB::table('z_project_list_project_join')->where('z_project_list_id', $id)->lists('z_projects_id');
            $data = $this->projects->fromView()->withTrashed()->where('status', '<>', 'Completed')
                ->whereNotIn('id', $ids);
        } else {
            $projects = $list->projects_view();
            $data = $projects;
        }

        $columns = [
            'id',
            'name',
            'city',
            'zipcode',
            'follow_up_date',
            'follow_up_date_custom',
            'status',
            'tags',
            'is_active',
            'view_uri',
        ];

        if (input('reverse')) {
            $filename = input('filename', trans('bct/projects::lists/common.title') . '-' . trans('bct/projects::projects/common.available_projects'));
            $title = input('filename', trans('bct/projects::lists/common.title') . ' \ ' . trans('bct/projects::projects/common.available_projects'));
        } else {
            $filename = input('filename', trans('bct/projects::lists/common.title') . '-' . trans('bct/projects::projects/common.title'));
            $title = input('filename', trans('bct/projects::lists/common.title') . ' \ ' . trans('bct/projects::projects/common.title'));
        }

        $source = 'Projects / Lists';
        $settings = [
            'sort'           => 'follow_up_date',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'throttle'       => 11,
            'threshold'      => 11,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'           => trans('bct/projects::projects/model.general.name'),
                'city'           => trans('bct/projects::projects/model.general.city'),
                'zipcode'        => trans('bct/projects::projects/model.general.zipcode'),
                'follow_up_date' => trans('bct/projects::projects/model.general.follow_up_date'),
                'tags'           => trans('bct/projects::projects/model.general.tags'),
                'status'         => trans('bct/projects::projects/model.general.status_id'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {

            if ($element->visits()->get()->count() > 0) {
                $element->lastVisit = $element->visits()->get()->last()->created_at->format('Y-m-d');
            }

            $element['follow_up_date_custom'] = !empty($element['follow_up_date']) ? date('M-d-Y', strtotime($element['follow_up_date'])) : 'n/a';

            if (!input('reverse')) {
                $element->status = ($element->is_active) ? 'Not visited' : 'Visited';
            }

            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.projects.details', $element['id']);
            } else {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['city']] = $element->city;
                $element[$export_columns['zipcode']] = $element->zipcode;
                $element[$export_columns['follow_up_date']] = $element->follow_up_date_custom;
                $element[$export_columns['tags']] = $element->tags;
                $element[$export_columns['status']] = $element->status;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    public function gridForReps($id) {
        $list = $this->lists->find($id);

        $data = $list->group->users()
            ->leftJoin('activations', 'activations.user_id', '=', 'users.id')
            ->join('role_users', 'role_users.user_id', '=', 'users.id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->leftJoin('z_user_profiles', 'z_user_profiles.id', '=', 'users.z_user_profiles_id')
            ->select(
                'users.id',
                'users.email',
                'users.first_name',
                'users.last_name',
                'users.created_at',
                'z_user_profiles.business_phone as phone',
                'roles.name as role_name',
                'activations.completed as status'
            )
            ->whereIn('roles.slug', ['repuser', 'repadmin', 'super-repadmin']);

        if ($limit = input('limit')) {
            $data = $data->take($limit)->get();
        }

        $columns = [
            'id',
            'email',
            'name',
            'status',
            'created_at',
            'created_at_custom',
            'role',
            'phone',
            'group',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::lists/common.title') . '-' . trans('bct/projects::users/common.title_reps'));
        $filename = input('filename', trans('bct/projects::lists/common.title') . '-' . trans('bct/projects::users/common.title_reps'));
        $source = 'Projects / Lists';
        $settings = [
            'sort'           => 'created_at',
            'direction'      => 'desc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'throttle'       => 11,
            'threshold'      => 11,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'    => trans('bct/projects::users/model.general.name'),
                'phone'   => [trans('bct/projects::users/model.general.phone'), 'col-md-3'],
                'email'   => trans('bct/projects::users/model.general.email'),
                'status'  => trans('bct/projects::users/model.general.status'),
                'role'    => trans('bct/projects::users/model.general.role'),
                'created' => trans('bct/projects::users/model.general.created_at')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns, $list) {

            $element->name = $element->first_name . ' ' . $element->last_name;

            $element->role = $element->role_name;
            $element->group = $list->group->name;

            $visit_date = date('M-d-Y', strtotime($element['created_at']));
            $element['created_at_custom'] = $visit_date;

            if (!input()->get('download', false)) {
                $element['view_uri'] = route('bct.projects.projects.details', $element['id']);
            } else {
                $element[$export_columns['name']] = $element->name;
                $element[get_export_column_name($export_columns['phone'])] = $element->phone;
                $element[$export_columns['email']] = $element->email;
                $element[$export_columns['status']] = ($element->status) ? 'Active' : 'Disabled';
                $element[$export_columns['role']] = $element->role;
                $element[$export_columns['created']] = $element->created_at_custom;
            }

            return $element;
        };


        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Updates projects counter on summary section
     *
     * @return string
     */
    public function getSummaryProjectsCounter($id) {
        $projects_count = DB::table('z_project_list_project_join as middle')
            ->where('z_project_list_id', $id)->count();

        if ($projects_count > $this->limit) {
            $grid_projects_count = $this->limit;
        } else {
            $grid_projects_count = $projects_count;
        }


        return "(" . $grid_projects_count . " of " . $projects_count . ")";
    }

}
    