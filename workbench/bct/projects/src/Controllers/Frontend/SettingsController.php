<?php namespace Bct\Projects\Controllers\Frontend;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 22.10.15
 * Time: 11:32
 */
use Bct\Projects\Models\Role;
use Bct\Projects\Repositories\Settings\SettingsRepositoryInterface;

class SettingsController extends BaseController
{
    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];

    protected $settings;

    protected $settings_configuration = [
        'map_radius',
        'map_count_projects',
        'map_less_4',
        'map_4_8',
        'map_more_8',
        'map_week_start',
        'map_week_end',
        'map_union_start',
        'map_union_end',
        'map_union_1',
        'map_union_2',
        'map_union_3',
        'visit_auto_approve',
        'project_field_report',
        'enable_project_visit_workers',
        'enable_project_visit_weather',
        'enable_project_visit_date',
    ];


    /**
     * SettingsController constructor.
     *
     * @param SettingsRepositoryInterface $settings
     */
    public function __construct(
        SettingsRepositoryInterface $settings
    ) {
        parent::__construct();

        $this->settings = $settings;
    }

    public function configuration()
    {
        return view('bct/projects::settings/configuration');
    }

    public function changeConfiguration()
    {
        $input = input()->only($this->settings_configuration);

        $this->settings->changeConfiguration($input);

        $message = trans('bct/projects::settings/message.success.updated');
        return response()->json([ 'success' => true , 'message' => $message], 200);
    }

}