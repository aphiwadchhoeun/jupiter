<?php namespace Bct\Projects\Controllers\Frontend;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.16
 * Time: 13:28
 */

use Illuminate\Support\Facades\Response;
use League\Flysystem\FileNotFoundException;
use Platform\Foundation\Controllers\Controller;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Filesystem\Laravel\Facades\Filesystem;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Platform\Media\Repositories\MediaRepositoryInterface;
use Illuminate\Container\Container;

class MediaController extends BaseController
{
    /**
     * The Media repository.
     *
     * @var \Platform\Media\Repositories\MediaRepositoryInterface
     */
    protected $media;

    protected $filesystem;


    /**
     * MediaController constructor.
     *
     * @param MediaRepositoryInterface $media
     * @param Container                $app
     */
    public function __construct(
        MediaRepositoryInterface $media,
        Container $app
    )
    {
        parent::__construct();

        $this->media = $media;

        $this->filesystem = $app['filesystem'];
    }

    /**
     * Returns the given media file.
     *
     * @param  string  $path
     * @return \Illuminate\Http\Response
     */
    public function view($path)
    {
        try {
            $media = $this->getMedia($path);
        } catch (HttpException $e) {
            return response(null, 404);
        }

        try {
            $file = $this->filesystem->disk('s3')->read($media->path);
        } catch (FileNotFoundException $e) {
            return response(null, 404);
        }

        $etag = md5($file);

        $ttl = (int) config('platform/media::ttl');

        $headers = [
            'ETag'           => $etag,
            'Content-Type'   => $media->mime,
            'Content-Length' => strlen($file),
            'Cache-Control'  => "max-age={$ttl}, public",
        ];

        if (request()->server('HTTP_IF_NONE_MATCH') === $etag) {
            return response(null, 304, $headers);
        }

        return $this->respond($file, $headers);
    }

    /**
     * Downloads the given media file.
     *
     * @param  string  $path
     * @return \Illuminate\Http\Response
     */
    public function download($path)
    {
        try {
            $media = $this->getMedia($path);
        } catch (HttpException $e) {
            $this->alerts->error($e->getMessage());
            return redirect('/');
        }

        $file = $this->filesystem->disk('s3')->read($media->path);

        $headers = [
            'Connection'          => 'close',
            'Content-Type'        => $media->mime,
            'Content-Length'      => strlen($file),
            'Content-Disposition' => 'attachment; filename="'.$media->name_full.'"',
        ];

        return $this->respond($file, $headers);
    }

    /**
     * Grabs the media file by its path and determines if the
     * logged in user  has access to the media file.
     *
     * @param  string  $path
     * @return \Platform\Media\Media
     */
    protected function getMedia($path)
    {
        if (! $media = $this->media->findByPath($path)) {
            throw new HttpException(404, 'Media does not exist.');
        }

        if ($media->private) {
            $pass = false;

            if ($user = Sentinel::check()) {
                $pass = true;

                $mediaRoles = $media->roles;

                $userRoles = $user->roles->lists('id');

                if (! empty($mediaRoles) and ! array_intersect($mediaRoles, $userRoles)) {
                    $pass = false;
                }
            }

            if (! $pass) {
                throw new HttpException(403, "You don't have permission to access this file.");
            }
        }

        return $media;
    }

    /**
     * Sends the response with the appropriate headers.
     *
     * @param  string  $file
     * @param  array  $headers
     * @return \Illuminate\Http\Response
     */
    protected function respond($file, $headers = [])
    {
        $response = response($file, 200);

        foreach ($headers as $name => $value) {
            $response->header($name, $value);
        }

        return $response;
    }
}
