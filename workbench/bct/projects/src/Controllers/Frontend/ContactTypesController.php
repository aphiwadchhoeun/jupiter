<?php namespace Bct\Projects\Controllers\Frontend;

use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Repositories\ContactType\ContactTypeRepositoryInterface;

class ContactTypesController extends AuthorizedController {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Projects repository.
     *
     * @var \Bct\Projects\Repositories\Area\AreaRepositoryInterface
     */
    protected $contactTypes;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];

    /**
     * Constructor.
     *
     * @param ContactTypeRepositoryInterface $contactTypes
     */
    public function __construct(ContactTypeRepositoryInterface $contactTypes) {
        parent::__construct();

        $this->contactTypes = $contactTypes;
    }


    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('bct/projects::settings/contact_types/index');
    }

    /**
     * Show the form for creating new contact type.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new contact type.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating contact type.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating contact type.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified contact type.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $this->contactTypes->delete($id);
        $this->alerts->success(trans("bct/projects::contact_types/common.delete"));


        return [];
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction() {
        $action = input('action');

        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                $this->contactTypes->{$action}($row);
            }
            if ($action == 'delete') {
                $this->alerts->success(
                    trans("bct/projects::contact_types/message.success.delete")
                );
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Datasource for the contractors Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $model = $this->contactTypes->grid();
        $data = $model
            ->select(
                'id',
                'name'
            );

        $columns = [
            'name',
            'view_uri',
        ];

        $title = input('filename', trans('bct/projects::contact_types/common.title'));
        $filename = input('filename', trans('bct/projects::contact_types/common.title'));
        $source = 'Projects / Settings / Contact Types';
        $settings = [
            'sort'           => 'name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name' => trans('bct/projects::contact_types/model.general.name'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $element['name'] = str_limit($element['name'], 35);

            if (!input()->get('download', false)) {
                $element->view_uri = route('bct.projects.contact_types.edit', $element->id);
            } else {
                $element[$export_columns['name']] = $element->name;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        // Do we have a contact identifier?
        if (isset($id)) {
            if (!$type = $this->contactTypes->find($id)) {
                $this->alerts->error(trans('bct/projects::contact_types/message.not_found', compact('id')));

                return redirect()->route('bct.projects.contact_types.all');
            }
        } else {
            $type = $this->contactTypes->createModel();
        }

        // Show the page
        return view('bct/projects::settings.contact_types.form', compact('mode', 'type'));
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        // Store the contact

        $response = $this->contactTypes->store($id, request()->all());

        if (count($response[0]) == 0) {
            $this->alerts->success(trans("bct/projects::contact_types/common.".$mode));
        } else {
            $this->alerts->error($response[0]);

        }


        return [];
    }

}
