<?php namespace Bct\Projects\Controllers\Frontend;

    /**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.16
 * Time: 13:30
 */

use Bct\Projects\Models\User;
use Platform\Access\Controllers\AuthorizedController;

class BaseController extends AuthorizedController {

    protected $currentUserModule;

    protected $permissionsWhitelist = ['showHelpPage'];

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->currentUserModule = (!is_null($this->currentUser)) ? User::find($this->currentUser->id) : null;
    }

    public function showHelpPage() {
        return view('bct/projects::help');
    }

}