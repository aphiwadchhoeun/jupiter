<?php

namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Local;
use Platform\Access\Controllers\AuthorizedController;
use Cartalyst\Support\Traits;

class LocalsController extends AuthorizedController {

	use Traits\EventTrait;

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction', 'getSummaryGroupsCounter'
	];



	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete', 'attach', 'detach'
	];


	public function getLocals(){
		return Local::getLocalsWithIdJson();
	}

}
