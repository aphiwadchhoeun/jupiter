<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Option;
use Bct\Projects\Repositories\Contractor\ContractorCompanyRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Storage;
use JavaScriptContractor;
class ContractorCompanyController extends AuthorizedController
{


	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The ContractorCompany repository.
	 *
	 * @var \Bct\Projects\Repositories\Contractor\ContractorCompanyRepositoryInterface
	 */
	protected $contractor_company;

	/**
	 * ContractorCompanyController constructor.
	 *
	 * @param ContractorCompanyRepositoryInterface  $contractor_company
	 */
	public function __construct(
		ContractorCompanyRepositoryInterface $contractor_company
	) {
		parent::__construct();

		$this->contractor_company         = $contractor_company;
	}

	/**
	 * Show the form for creating new ContractorCompany.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}


	/**
	 * Handle posting of the form for creating new ContractorCompany.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}


	/**
	 * Show the form for updating ContractorCompany.
	 *
	 * @param  int $id
	 *
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}


	/**
	 * Handle posting of the form for updating ContractorCompany.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}


	/**
	 * Remove the specified ContractorCompany.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->contractor_company->delete($id) ? 'success' : 'error';
		$this->alerts->success(trans('bct/projects::contractors/message.company.'.$type.'.delete'));

		$result = ($type=='success')? true:false;
		return response()->json(['success'=>$result]);	}



	protected function getCompany($id)
	{

		if ( ! $contractor_company = $this->contractor_company->find($id)) {

			return response()->json(['success'=>true,'contractor_company'=>$contractor_company]);

		}
		else{
			return response()->json(['success'=>false,'message'=>'Company not found']);
		}

	}


	/**
	 * Processes the form.
	 *
	 * @param  string $mode
	 * @param  int    $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the contractor
		list( $messages, $contractor_company ) = $this->contractor_company->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty()) {

			if (!$id) {
				$this->alerts->success(trans('bct/projects::contractors/message.company.success.create'));
			} else {
				$this->alerts->success(trans('bct/projects::contractors/message.company.success.update'));
			}

			return response()->json(['status'=>'true']);
		}
		return response()->json(['status'=>'false','messages'=>$messages]);
	}

	public function getAll($contractor_id){
		return $this->contractor_company->getAllByContractorId($contractor_id);
	}


}