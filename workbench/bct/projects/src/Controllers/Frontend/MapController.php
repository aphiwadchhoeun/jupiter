<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Repositories\Contractor\ContractorRepositoryInterface;
use Bct\Projects\Repositories\Project\ProjectRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Models\TempItems;
use Bct\Projects\Repositories\User\UserRepositoryInterface;
use JavaScriptProjects;
use JavaScriptUnionimpactbase;

class MapController extends AuthorizedController {


    protected $authWhitelist = [
        'index'
    ];

    protected $permissionsWhitelist = [
        'index'
    ];
    
    /**
     * The repository.
     *
     * @var \Bct\Projects\Repositories\User\UserRepositoryInterface
     */
    protected $users;

    protected $projects;
    protected $tags;
    protected $contractors;

    /**
     * MapController constructor.
     *
     * @param UserRepositoryInterface    $users
     * @param ProjectRepositoryInterface $projects
     * @param ContractorRepositoryInterface $contractors
     */
    public function __construct(
        UserRepositoryInterface $users,
        ProjectRepositoryInterface $projects,
        ContractorRepositoryInterface $contractors
    ) {
        parent::__construct();

        $this->users = $users;
        $this->projects = $projects;
        $this->contractors = $contractors;
        if ($this->currentUser) $this->currentUser = $this->users->find($this->currentUser->id);
    }

    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        if (!$user = $this->currentUser) {

            JavaScriptUnionimpactbase::put([
                'default_lat'    => config('bct.unionimpactbase.map.location.default.lat'),
                'default_lng'    => config('bct.unionimpactbase.map.location.default.lng'),
            ]);

            return view('bct/unionimpactbase::openroutes.index');
        }

        $radius = config_projects('bct.projects.settings.map_radius');
        $count_projects = config_projects('bct.projects.settings.map_count_projects');
        $stages = $this->projects->stages();

        JavaScriptProjects::put([
            'radius'         => $radius,
            'count_projects' => $count_projects,
            'color_less_4'   => config_projects('bct.projects.settings.map_less_4'),
            'color_4_8'      => config_projects('bct.projects.settings.map_4_8'),
            'color_more_8'   => config_projects('bct.projects.settings.map_more_8'),
            'week_start'     => config_projects('bct.projects.settings.map_week_start'),
            'week_end'       => config_projects('bct.projects.settings.map_week_end'),
            'union_start'    => config_projects('bct.projects.settings.map_union_start'),
            'union_end'      => config_projects('bct.projects.settings.map_union_end'),
            'union_1'        => config_projects('bct.projects.settings.map_union_1'),
            'union_2'        => config_projects('bct.projects.settings.map_union_2'),
            'union_3'        => config_projects('bct.projects.settings.map_union_3'),
            'default_lat'    => config_projects('bct.unionimpactbase.map.location.default.lat'),
            'default_lng'    => config_projects('bct.unionimpactbase.map.location.default.lng'),
        ]);


        return view('bct/projects::map.index', compact('user', 'radius', 'count_projects', 'tags', 'stages'));
    }



    public function getProjectsByMap()
    {
        try {
            $projects = $this->projects->gridForMap(true);

            if (!$projects->isEmpty()) {
                foreach ($projects as $project) {
                    $project->setAppends(['follow_up_date_custom']);
                    $project->details_url = route('bct.projects.projects.details', $project->id);
                    $project->union_percent = number_format($project->union_value * 100, 2) . '%';
                }
            }

            return response()->json([
                'success'  => true,
                'projects' => $projects->toJSON()
            ], 200);
        } catch (\Esception $e) {
            return response()->json([ 'success' => false, 'errors' => $e->getMessage() ], 422);
        }
    }

}
