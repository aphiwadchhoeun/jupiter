<?php

namespace Bct\Projects\Controllers\Frontend;

use Illuminate\Http\Request;
use Platform\Access\Controllers\AuthorizedController;
use Platform\Tags\Repositories\TagsRepositoryInterface;
use Illuminate\Support\Str;

class TagsController extends AuthorizedController {


    /**
     * Constructor.
     *
     * @param TagsRepositoryInterface $tags
     */
    public function __construct(TagsRepositoryInterface $tags) {
        parent::__construct();

        $this->tags = $tags;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('bct/projects::settings/tags/index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        return $this->processForm('create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return $this->showForm('update', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        $type = $this->tags->delete($id) ? 'success' : 'error';
        if ($type == 'success') {
            $this->alerts->success(trans('bct/projects::tags/message.success.delete'));
        } else {
            $this->alerts->error(trans('bct/projects::tags/message.error.delete'));
        }

        return redirect()->route('bct.projects.tags.all');
    }

    public function grid() {
        $data = $this->tags->grid()->select('id', 'name', 'group')
            ->where('namespace', '=', 'bct/projects.project');

        $columns = [
            'id',
            'name',
            'group',
            'edit_uri'
        ];

        $title = input('filename', trans('bct/projects::tags/common.title'));
        $filename = input('filename', trans('bct/projects::tags/common.title'));
        $source = 'Projects / Settings / Projects Tags';
        $settings = [
            'sort'           => 'name',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name' => trans('bct/projects::general/form.name'),
                'group'=> trans('bct/projects::tags/common.group_description')
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $element['name'] = str_limit($element['name'], 35);

            if (!input()->get('download', false)) {
                $element->edit_uri = route('bct.projects.tags.edit', $element->id);
            } else if (input()->get('download')) {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['group']] = $element->group;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);

    }


    public function showForm($mode, $id = null) {
        if (isset($id)) {
            if (!$tag = $this->tags->find($id)) {
                $this->alerts->error(trans('bct/projects::tags/message.not_found', compact('id')));

                return redirect()->route('bct.projects.tags.all');
            }
        } else {
            $tag = $this->tags->grid();
        }

        // Show the page
        return view('bct/projects::settings/tags.form',
            compact('mode', 'tag'));
    }

    public function processForm($mode, $id = null) {
        // Store the tag
        $inputs = request()->all();
        if ($mode = 'create') {
            $inputs['namespace'] = 'bct/projects.project';
            $inputs['slug'] = Str::slug(array_get($inputs, 'name'));
        }
        list($messages, $tag) = $this->tags->store($id, $inputs);
        // Do we have any errors?
        if ($messages->isEmpty()) {


            $this->alerts->success(trans("bct/projects::tags/message.success.{$mode}"));

            if ($tag->id) {
                $tag->group = $inputs['group'];
                $tag->save();
                return redirect()->route('bct.projects.tags.all');
            }

        } else {
            $this->alerts->error($messages);
        }

        if ($tag->id) {
            return redirect()->back();
        }

        return redirect()->route('bct.projects.tags.create')->withInput();
    }
}
