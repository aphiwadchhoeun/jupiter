<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Option;
use Bct\Projects\Repositories\Contractor\ContractorEmailRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Storage;
use JavaScriptContractor;
class ContractorEmailController extends AuthorizedController
{


	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The ContractorEmail repository.
	 *
	 * @var \Bct\Projects\Repositories\Contractor\ContractorEmailRepositoryInterface
	 */
	protected $contractor_email;

	/**
	 * ContractorEmailController constructor.
	 *
	 * @param ContractorEmailRepositoryInterface  $contractor_email
	 */
	public function __construct(
		ContractorEmailRepositoryInterface $contractor_email
	) {
		parent::__construct();

		$this->contractor_email         = $contractor_email;
	}

	/**
	 * Show the form for creating new ContractorEmail.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}


	/**
	 * Handle posting of the form for creating new ContractorEmail.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}


	/**
	 * Show the form for updating ContractorEmail.
	 *
	 * @param  int $id
	 *
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}


	/**
	 * Handle posting of the form for updating ContractorEmail.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}


	/**
	 * Remove the specified ContractorEmail.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->contractor_email->delete($id) ? 'success' : 'error';
		$this->alerts->success(trans('bct/projects::contractors/message.email.'.$type.'.delete'));

		$result = ($type=='success')? true:false;
		return response()->json(['success'=>$result]);	}



	protected function getEmail($id)
	{

		if ( ! $contractor_email = $this->contractor_email->find($id)) {

			return response()->json(['success'=>true,'contractor_email'=>$contractor_email]);

		}
		else{
			return response()->json(['success'=>false,'message'=>'Email not found']);
		}

	}


	/**
	 * Processes the form.
	 *
	 * @param  string $mode
	 * @param  int    $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the contractor
		list( $messages, $contractor_email ) = $this->contractor_email->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty()) {

			if (!$id) {
				$this->alerts->success(trans('bct/projects::contractors/message.email.success.create'));
			} else {
				$this->alerts->success(trans('bct/projects::contractors/message.email.success.update'));
			}

			return response()->json(['status'=>'true']);
		}
		return response()->json(['status'=>'false','messages'=>$messages]);
	}

	public function getAll($contractor_id){
		return $this->contractor_email->getAllByContractorId($contractor_id);
	}


}