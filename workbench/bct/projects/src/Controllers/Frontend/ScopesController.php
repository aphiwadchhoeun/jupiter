<?php namespace Bct\Projects\Controllers\Frontend;

use Bct\Projects\Models\Classification;
use Illuminate\Support\Facades\DB;
use Platform\Access\Controllers\AuthorizedController;
use Bct\Projects\Repositories\ContactType\ContactTypeRepositoryInterface;
use Bct\Projects\Repositories\Scope\ScopeRepositoryInterface;

class ScopesController extends AuthorizedController {

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Projects repository.
     *
     * @var \Bct\Projects\Repositories\Scope\ScopeRepositoryInterface
     */
    protected $scopes;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];

    /**
     * Constructor.
     *
     * @param ScopeRepositoryInterface $scopes
     */
    public function __construct(ScopeRepositoryInterface $scopes) {
        parent::__construct();

        $this->scopes = $scopes;
    }


    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $classifications = Classification::all();

        return view('bct/projects::settings/scopes/index', compact('classifications'));
    }

    /**
     * Show the form for creating new scope.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new scope.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating scope.
     *
     * @param  int $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('edit', $id);
    }

    /**
     * Handle posting of the form for updating scope.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified scope.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $this->scopes->delete($id);

        return [];
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction() {
        $action = input('action');

        if (in_array($action, $this->actions)) {
            foreach (input('rows', []) as $row) {
                $this->scopes->{$action}($row);
            }
            if ($action == 'delete') {
                $this->alerts->success(
                    trans("bct/projects::scopes/message.success.delete")
                );
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Datasource for the contractors Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid() {
        $data = $this->scopes->grid()
            ->join('z_scopes_classifications_join',
                'z_scopes_classifications_join.z_scopes_id', '=', 'z_scopes.id')
            ->join('z_project_classification',
                'z_scopes_classifications_join.z_classifications_id', '=', 'z_project_classification.id')
            ->get([
                DB::raw('z_scopes.id as id'),
                DB::raw('z_scopes.name as name'),
                DB::raw('z_scopes_classifications_join.value as value'),
                DB::raw('z_project_classification.name as classification'),
                'order'
            ]);

        $columns = [
            'id',
            'name',
            'value',
            'classification',
            'order',
            'view_uri'
        ];

        $title = input('filename', trans('bct/projects::scopes/common.title'));
        $filename = input('filename', trans('bct/projects::scopes/common.title'));
        $source = 'Projects / Settings / Scopes';
        $settings = [
            'sort'           => 'classification',
            'direction'      => 'asc',
            'pdf_filename'   => $filename,
            'csv_filename'   => $filename,
            'title'          => $title,
            'export_source'  => $source,
            'export_columns' => [
                'name'           => trans('bct/projects::scopes/model.general.name'),
                'value'          => trans('bct/projects::scopes/model.general.value'),
                'classification' => trans('bct/projects::scopes/model.general.classification'),
                'order'          => trans('bct/projects::scopes/model.general.order'),
            ]
        ];

        $export_columns = $settings['export_columns'];

        $transformer = function ($element) use ($export_columns) {
            $element['name'] = str_limit($element['name'], 35);

            if (!input()->get('download', false)) {
                $element->view_uri = route('bct.projects.scopes.edit', $element->id);
            } else if (input()->get('download')) {
                $element[$export_columns['name']] = $element->name;
                $element[$export_columns['value']] = $element->value;
                $element[$export_columns['classification']] = ($element->classification) ? $element->classification : 'Default';
                $element[$export_columns['order']] = $element->order;
            }

            return $element;
        };

        return datagrid_init($data, $columns, $settings, $transformer);
    }

    /**
     * Shows the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {

        // Do we have a contact identifier?
        if (isset($id)) {
            if (!$scope = $this->scopes->find($id)) {
                $this->alerts->error(trans('bct/projects::scopes/message.not_found', compact('id')));

                return redirect()->route('bct.projects.scopes.all');
            }
        } else {
            $scope = $this->scopes->createModel();
        }

        // Show the page
        return view('bct/projects::settings.scopes.form', compact('mode', 'scope'));
    }

    /**
     * Processes the form.
     *
     * @param  string $mode
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        // Store the contact

        list($messages, $scope) = $this->scopes->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("bct/projects::scopes/message.success.{$mode}"));
        } else {
            $error_message = '';
            foreach ($messages->all() as $message) {
                $error_message .= $message;
            }
            $this->alerts->error($error_message);
        }


        return [];
    }

}
