<?php namespace Bct\Projects\Controllers\Frontend;


use Bct\Projects\Models\Option;
use Bct\Projects\Repositories\Contractor\ContractorPhoneRepositoryInterface;
use Platform\Access\Controllers\AuthorizedController;
use Storage;
use JavaScriptContractor;

class ContractorPhoneController extends AuthorizedController
{

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The ContractorPhone repository.
	 *
	 * @var \Bct\Projects\Repositories\Contractor\ContractorPhoneRepositoryInterface
	 */
	protected $contractor_phone;

	/**
	 * ContractorPhoneController constructor.
	 *
	 * @param ContractorPhoneRepositoryInterface  $contractor_phone
	 */
	public function __construct(
		ContractorPhoneRepositoryInterface $contractor_phone
	) {
		parent::__construct();

		$this->contractor_phone         = $contractor_phone;
	}

	/**
	 * Show the form for creating new ContractorPhone.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}


	/**
	 * Handle posting of the form for creating new ContractorPhone.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}


	/**
	 * Show the form for updating ContractorPhone.
	 *
	 * @param  int $id
	 *
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}


	/**
	 * Handle posting of the form for updating ContractorPhone.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}


	/**
	 * Remove the specified ContractorPhone.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->contractor_phone->delete($id) ? 'success' : 'error';
		$this->alerts->success(trans('bct/projects::contractors/message.phone.'.$type.'.delete'));

		$result = ($type=='success')? true:false;
		return response()->json(['success'=>$result]);	}



	protected function getPhone($id)
	{

		if ( ! $contractor_phone = $this->contractor_phone->find($id)) {

			return response()->json(['success'=>true,'contractor_phone'=>$contractor_phone]);

		}
		else{
			return response()->json(['success'=>false,'message'=>'Phone not found']);
		}

	}


	/**
	 * Processes the form.
	 *
	 * @param  string $mode
	 * @param  int    $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the contractor
		list( $messages, $contractor_phone ) = $this->contractor_phone->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty()) {

			if (!$id) {
				$this->alerts->success(trans('bct/projects::contractors/message.phone.success.create'));
			} else {
				$this->alerts->success(trans('bct/projects::contractors/message.phone.success.update'));
			}

			return response()->json(['status'=>'true']);
		}

		return response()->json(['status'=>'false','messages'=>$messages]);

	}

	public function getAll($contractor_id){
		return $this->contractor_phone->getAllByContractorId($contractor_id);
	}


}