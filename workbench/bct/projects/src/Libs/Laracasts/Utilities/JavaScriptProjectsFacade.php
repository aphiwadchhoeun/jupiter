<?php namespace Bct\Projects\Libs\Laracasts\Utilities;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 22.02.16
 * Time: 13:49
 */

use Illuminate\Support\Facades\Facade;

class JavaScriptProjectsFacade extends Facade
{
    /**
     * The name of the binding in the IoC container.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'JavaScriptProjects';
    }
}