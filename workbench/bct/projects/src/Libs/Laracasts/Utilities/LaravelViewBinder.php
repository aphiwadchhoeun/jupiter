<?php namespace Bct\Projects\Libs\Laracasts\Utilities;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.12.15
 * Time: 17:38
 */


use Illuminate\Contracts\Events\Dispatcher;
use Laracasts\Utilities\JavaScript\ViewBinder;

class LaravelViewBinder implements ViewBinder
{
    /**
     * The event dispatcher implementation.
     *
     * @var Dispatcher
     */
    private $event;

    /**
     * The name of the view to bind JS variables to.
     *
     * @var string
     */
    private $views;

    /**
     * Create a new Laravel view binder instance.
     *
     * @param Dispatcher   $event
     * @param string|array $views
     */
    function __construct(Dispatcher $event, $views)
    {
        $this->event = $event;
        $this->views = str_replace('/', '.', (array)$views);
    }

    /**
     * Bind the given JavaScript to the view.
     *
     * @param string $js
     */
    public function bind($js)
    {
        foreach ($this->views as $view) {
            /*$this->event->listen("composing: {$view}", function () use ($js) {
                echo "<script>{$js}</script>";
            });*/
            echo "<script>{$js}</script>";
        }
    }
}
