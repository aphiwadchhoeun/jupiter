<?php namespace Bct\Projects\Validator\Contractor;

use Cartalyst\Support\Validator;

class ContractorNotesValidator extends Validator
{

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
		'notes'         => 'required|max:255',
		'z_contractors_id' => 'required|integer',
	];

	protected $messages = [
	];


	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
