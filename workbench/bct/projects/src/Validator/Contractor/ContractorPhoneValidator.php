<?php namespace Bct\Projects\Validator\Contractor;

use Cartalyst\Support\Validator;

class ContractorPhoneValidator extends Validator
{

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
		'phone'         => 'required|max:255|unique:z_contractor_phone,phone',
		'type'        => 'required|max:10',
		'z_contractors_id' => 'required|integer',
	];

	protected $messages = [
		'phone.unique'=>'You have already added this phone number'
	];


	/**
	 * {@inheritDoc}
	 */
	public function onUpdate($id, $contractor_id)
	{
        $this->rules['phone'] .= ','.$id.',id,z_contractors_id,'.$contractor_id;
	}


	public function onCreate($contractor_id)
	{
        $this->rules['phone'] .= ',NULL,id,z_contractors_id,'.$contractor_id;
	}

}
