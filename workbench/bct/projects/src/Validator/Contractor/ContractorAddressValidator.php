<?php namespace Bct\Projects\Validator\Contractor;

use Cartalyst\Support\Validator;

class ContractorAddressValidator extends Validator
{

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
		'address'         => 'required|max:255',
		'city'        => 'required|max:255',
		'state'        => 'required|max:255',
		'zipcode'        => 'required|max:255',
		'type'        => 'required|max:10',
		'z_contractors_id' => 'required|integer',
	];

	protected $messages = [
	];


	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
