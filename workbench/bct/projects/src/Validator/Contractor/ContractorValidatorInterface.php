<?php namespace Bct\Projects\Validator\Contractor;

use Bct\Projects\Models\ContactType;
use Bct\Projects\Models\Contractor;

interface ContractorValidatorInterface {

	/**
	 * Updating a contractor scenario.
	 *
	 * @param Contractor $contractor
	 */
	public function onUpdate(Contractor $contractor);

	/**
	 * Creating a contractor scenario
	 * @return
	 */
	public function onCreate();

}
