<?php namespace Bct\Projects\Validator\Contractor;

use Cartalyst\Support\Validator;

class ContractorCompanyValidator extends Validator
{

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'company'          => 'required|unique:z_contractor_company,company',
        'type'             => 'required|max:10',
        'z_contractors_id' => 'required|integer',
    ];

    protected $messages = [];


    public function onCreate($contractor_id)
    {
        $this->rules['company'] .= ',NULL,id,z_contractors_id,'.$contractor_id;
    }


    /**
     * {@inheritDoc}
     */
    public function onUpdate($id, $contractor_id)
    {
        $this->rules['company'] .= ','.$id.',id,z_contractors_id,'.$contractor_id;
    }

}
