<?php namespace Bct\Projects\Validator\Contractor;

use Cartalyst\Support\Validator;
use Bct\Projects\Models\Contractor;

class ContractorValidator extends Validator implements ContractorValidatorInterface
{

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'name'    => 'required|max:64',
        'address' => 'max:64',
        'license' => 'required|max:15|unique:z_contractors',
        'ubi'     => 'sometimes|unique:z_contractors'
    ];


    /**
     * {@inheritDoc}
     */
    public function onUpdate(Contractor $contractor)
    {
        $this->rules['license'] .= ',license,' . $contractor->id . ',id,deleted_at,NULL';
	    $this->rules['name']    = 'max:64';
	    $this->rules['license']    = 'max:15';
    }


    /**
     * Creating a contractor scenario
     */
    public function onCreate()
    {
        $this->rules['license'] .= ',license,NULL,id,deleted_at,NULL';
        $this->rules['ubi'] .= ',ubi,NULL,id,deleted_at,NULL';
    }
}
