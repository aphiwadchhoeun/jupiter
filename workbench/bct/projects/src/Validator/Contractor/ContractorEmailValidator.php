<?php namespace Bct\Projects\Validator\Contractor;

use Cartalyst\Support\Validator;

class ContractorEmailValidator extends Validator
{

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'email'            => 'required|email|unique:z_contractor_email,email',
        'type'             => 'required|max:10',
        'z_contractors_id' => 'required|integer',
    ];

    protected $messages = [];


    public function onCreate($contractor_id)
    {
        $this->rules['email'] .= ',NULL,id,z_contractors_id,'.$contractor_id;
    }


    /**
     * {@inheritDoc}
     */
    public function onUpdate($id, $contractor_id)
    {
        $this->rules['email'] .= ','.$id.',id,z_contractors_id,'.$contractor_id;
    }

}
