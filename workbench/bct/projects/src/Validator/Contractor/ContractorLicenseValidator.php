<?php namespace Bct\Projects\Validator\Contractor;

use Cartalyst\Support\Validator;

class ContractorLicenseValidator extends Validator
{

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'license'          => 'required|unique:z_contractor_license,license',
        'type'             => 'required|max:10',
        'z_contractors_id' => 'required|integer',
    ];

    protected $messages = [];


    public function onCreate($contractor_id)
    {
        $this->rules['license'] .= ',NULL,id,z_contractors_id,'.$contractor_id;
    }


    /**
     * {@inheritDoc}
     */
    public function onUpdate($id, $contractor_id)
    {
        $this->rules['license'] .= ','.$id.',id,z_contractors_id,'.$contractor_id;
    }

}
