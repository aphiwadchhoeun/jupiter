<?php namespace Bct\Projects\Validator\ActionType;

use Bct\Projects\Models\ActionType;

interface ActionTypeValidatorInterface {

	/**
	 * Updating a actiontype scenario.
	 * @param ActionType $action_type
	 * @return void
	 */
	public function onUpdate(ActionType $action_type);

}
