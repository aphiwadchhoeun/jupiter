<?php namespace Bct\Projects\Validator\ActionType;

use Bct\Projects\Models\ActionType;
use Cartalyst\Support\Validator;

class ActionTypeValidator extends Validator implements ActionTypeValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
        'name' => "required|unique:z_action_type,name",
	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate(ActionType $action_type)
	{
        $this->rules['name'] .= ',' . $action_type->id;

	}

}
