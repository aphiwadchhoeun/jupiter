<?php namespace Bct\Projects\Validator\Action;

interface ActionValidatorInterface {

	/**
	 * Updating a action scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
