<?php namespace Bct\Projects\Validator\Group;

use Cartalyst\Support\Validator;
use Bct\Projects\Models\Group;
class GroupValidator extends Validator implements GroupValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
        'name'      => 'required|unique:z_projects_group',
	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate(Group $group)
	{
        $this->rules['name'] = $this->rules['name']. ',name,' . $group->id;
	}

}
