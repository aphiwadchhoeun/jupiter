<?php namespace Bct\Projects\Validator\Group;
use Bct\Projects\Models\Group;
interface GroupValidatorInterface {

	/**
	 * Updating a group scenario.
	 *
	 * @return void
	 */
	public function onUpdate(Group $group);

}
