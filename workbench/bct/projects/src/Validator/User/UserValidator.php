<?php namespace Bct\Projects\Validator\User;

use Cartalyst\Support\Validator;
use Bct\Projects\Models\User;

class UserValidator extends Validator implements UserValidatorInterface {

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'email'    => 'required|email|unique:users',
        'password' => 'min:6',
    ];

    /**
     * {@inheritDoc}
     */
    public function onRegister() {
        $this->rules['password'] = 'required|min:6|confirmed';
    }

    /**
     * {@inheritDoc}
     */
    public function onCreate() {
        $this->rules += [
            'email'      => $this->rules['email'] . ',email,NULL',
            'password'   => 'required|min:6|confirmed',
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required',
            'role'       => 'required'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function onUpdate(User $user) {
        $this->rules = [
            'email'      => $this->rules['email'] . ',email,' . $user->id,
            'password'   => 'min:6|confirmed',
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required',
            'role'       => 'required'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function onReminder() {
        $this->rules = [
            'password' => 'required|min:6|confirmed'
        ];
    }
}
