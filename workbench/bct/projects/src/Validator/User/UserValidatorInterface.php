<?php namespace Bct\Projects\Validator\User;

use Bct\Projects\Models\User;

interface UserValidatorInterface {

	/**
	 * Creating a user scenario.
	 *
	 * @return void
	 */
	public function onCreate();

	/**
	 * @param User $user
	 * @return mixed
	 */
	public function onUpdate(User $user);

	/**
	 * Registering a user scenario.
	 *
	 * @return void
	 */
	public function onRegister();

	/**
	 * Resetting a user password scenario.
	 *
	 * @return void
	 */
	public function onReminder();

}
