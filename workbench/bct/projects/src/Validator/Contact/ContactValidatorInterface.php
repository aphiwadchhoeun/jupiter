<?php namespace Bct\Projects\Validator\Contact;

use Bct\Projects\Models\Contact;

interface ContactValidatorInterface {

	/**
	 * Updating a contact scenario.
	 *
	 * @param Contact $contact
	 */
	public function onUpdate(Contact $contact);

	public function onCreate();

}
