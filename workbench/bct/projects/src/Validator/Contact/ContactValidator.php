<?php namespace Bct\Projects\Validator\Contact;

use Bct\Projects\Models\Contact;
use Cartalyst\Support\Validator;

class ContactValidator extends Validator implements ContactValidatorInterface
{

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'first_name' => 'required_with_all:last_name,phone|required_without_all:company|max:64',
        'last_name'  => 'required_with_all:first_name,phone|required_without_all:company|max:64',
        'address'    => 'max:64',
        'email'      => 'sometimes|email|unique:z_contact',
        'company'    => 'required_without_all:first_name,last_name'
    ];


    /**
     * {@inheritDoc}
     */
    public function onUpdate(Contact $contact)
    {
        $this->rules['email'] .= ',email,' . $contact->id . ',id,deleted_at,NULL';
    }


    public function onCreate()
    {
        $this->rules['email'] .= ',email,NULL,id,deleted_at,NULL';
    }
}
