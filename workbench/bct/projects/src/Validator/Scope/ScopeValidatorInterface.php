<?php namespace Bct\Projects\Validator\Scope;
use Bct\Projects\Models\Scope;
interface ScopeValidatorInterface {

	/**
	 * Updating a scope scenario.
	 *
	 * @return void
	 */
	public function onUpdate(Scope $scope);

}
