<?php namespace Bct\Projects\Validator\Scope;

use Cartalyst\Support\Validator;
use Bct\Projects\Models\Scope;
class ScopeValidator extends Validator implements ScopeValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate(Scope $scope)
	{
	}

}
