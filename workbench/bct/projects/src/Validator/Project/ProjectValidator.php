<?php namespace Bct\Projects\Validator\Project;

use Cartalyst\Support\Validator;
use Bct\Projects\Models\Project;

class ProjectValidator extends Validator implements ProjectValidatorInterface {

    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'name'                => 'required',
        'z_project_status_id' => 'required',
        'lat'                 => 'required',
        'lng'                 => 'required',
        'follow_up_date'      => 'required',
        'num_floor'           => 'numeric',
    ];

    protected $messages = [
        'z_project_status_id.required' => 'The status field is required.'
    ];

    public function onCreate() {
        $this->rules['followup_reason'] = 'required';
    }

    /**
     * {@inheritDoc}
     */
    public function onUpdate(Project $project) {
        // Need if this field is required
        //$this->rules['name'] = $this->rules['name'] . ',name,' . $project->id;
    }

}
