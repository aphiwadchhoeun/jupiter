<?php namespace Bct\Projects\Validator\Project;
use Bct\Projects\Models\Project;

interface ProjectValidatorInterface {

	/**
	 * Creating a new project scenario
	 *
	 * @return mixed
	 */
	public function onCreate();

	/**
	 * Updating a project scenario.
	 *
	 * @return void
	 */
	public function onUpdate(Project $project);

}
