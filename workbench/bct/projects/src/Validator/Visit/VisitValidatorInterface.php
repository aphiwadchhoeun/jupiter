<?php namespace Bct\Projects\Validator\Visit;

interface VisitValidatorInterface {

	/**
	 * Updating a project scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
