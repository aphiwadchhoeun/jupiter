<?php namespace Bct\Projects\Validator\File;

use Cartalyst\Support\Validator;

class FileValidator extends Validator implements FileValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
		'name' => 'required',
		'description' => 'required',
	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
