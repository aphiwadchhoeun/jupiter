<?php namespace Bct\Projects\Validator\File;

interface FileValidatorInterface {

	/**
	 * Updating a file scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
