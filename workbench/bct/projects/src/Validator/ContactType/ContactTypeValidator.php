<?php namespace Bct\Projects\Validator\ContactType;

use Bct\Projects\Models\ContactType;
use Cartalyst\Support\Validator;

class ContactTypeValidator extends Validator implements ContactTypeValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
        'name' => "required|unique:z_contact_type,name",
	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate(ContactType $contact_type)
	{
        $this->rules['name'] .= ',' . $contact_type->id;

	}

}
