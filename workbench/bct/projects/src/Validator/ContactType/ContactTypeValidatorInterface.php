<?php namespace Bct\Projects\Validator\ContactType;

use Bct\Projects\Models\ContactType;

interface ContactTypeValidatorInterface {

	/**
	 * Updating a contacttype scenario.
	 * @param ContactType $contact_type
	 * @return void
	 */
	public function onUpdate(ContactType $contact_type);

}
