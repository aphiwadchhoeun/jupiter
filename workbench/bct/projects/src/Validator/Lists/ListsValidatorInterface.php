<?php namespace Bct\Projects\Validator\Lists;

interface ListsValidatorInterface {

	/**
	 * Updating a lists scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
