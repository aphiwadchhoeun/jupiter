<?php namespace Bct\Projects\Validator\Lists;

use Cartalyst\Support\Validator;

class ListsValidator extends Validator implements ListsValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [
		'name' 					=> 'required|unique:z_project_list,name,:id,id,deleted_at,NULL',
		'z_projects_group_id' 	=> 'required',
	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{
		$this->forgeUniqueRulesOnUpdate('name', $this->getCustomAttributes());
	}

	/**
	 * Ignore a given parameter to unique field
	 *
	 */
	protected function forgeUniqueRulesOnUpdate($field, array $data)
	{
		if (isset( $this->rules[$field] ) && isset( $data[0]['id'] )) {
			$this->rules[$field] = str_replace([ ':id' ], $data[0]['id'], $this->rules[$field]);
		}
	}

}
