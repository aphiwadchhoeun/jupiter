<?php namespace Bct\Projects\Handlers\File;
/**
 * Part of the Platform Media extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Media extension
 * @version    2.0.2
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Bct\Projects\Models\File;
use Illuminate\Events\Dispatcher;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class FileEventHandler extends BaseEventHandler implements FileEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.file.uploaded', __CLASS__.'@uploaded');

		$dispatcher->listen('bct.projects.file.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.file.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.file.deleting', __CLASS__.'@deleting');
		$dispatcher->listen('bct.projects.file.deleted', __CLASS__.'@deleted');
	}

	/**
	 * On upload event.
	 *
	 * @param  \Bct\Projects\Models\File  $media
	 * @param  \Cartalyst\Filesystem\File  $file
	 * @param  \Symfony\Component\HttpFoundation\File\UploadedFile  $uploadedFile
	 * @return void
	 */
	public function uploaded(File $media, \Cartalyst\Filesystem\File $file, UploadedFile $uploadedFile)
	{
		if ($media->thumbnail)
		{
			\Illuminate\Support\Facades\File::delete($media->thumbnail);
		}

		//$this->app['platform.media.manager']->handleUp($media, $file, $uploadedFile);

		$this->flushCache($media);
	}

	/**
	 * On updating event.
	 *
	 * @param  \Bct\Projects\Models\File $media
	 * @return void
	 */
	public function updating(File $media)
	{

	}

	/**
	 * On updated event.
	 *
	 * @param  \Bct\Projects\Models\File  $media
	 * @return void
	 */
	public function updated(File $media)
	{
		$this->flushCache($media);
	}

	/**
	 * On deleting event.
	 *
	 * @param  \Bct\Projects\Models\File  $media
	 * @param  \Cartalyst\Filesystem\File  $file
	 * @return void
	 */
	public function deleting(File $media, \Cartalyst\Filesystem\File $file)
	{
		//$this->app['platform.media.manager']->handleDown($media, $file);
	}

	/**
	 * On deleted event.
	 *
	 * @param  \Bct\Projects\Models\File  $media
	 * @return void
	 */
	public function deleted(File $media)
	{
		$this->flushCache($media);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\File  $media
	 * @return void
	 */
	protected function flushCache(File $media)
	{
		$this->app['cache']->forget('bct.projects.file.'.$media->id);
		$this->app['cache']->forget('bct.projects.file.path.'.$media->path);
	}

}
