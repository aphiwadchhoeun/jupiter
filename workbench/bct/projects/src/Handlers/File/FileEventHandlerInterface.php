<?php namespace Bct\Projects\Handlers\File;
/**
 * Part of the Platform Media extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Media extension
 * @version    2.0.2
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Bct\Projects\Models\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface FileEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * On upload event.
	 *
	 * @param  \Bct\Projects\Models\File  $media
	 * @param  \Cartalyst\Filesystem\File  $file
	 * @param  \Symfony\Component\HttpFoundation\File\UploadedFile  $uploadedFile
	 * @return void
	 */
	public function uploaded(File $media, \Cartalyst\Filesystem\File $file, UploadedFile $uploadedFile);

	/**
	 * On deleting event.
	 *
	 * @param  \Bct\Projects\Models\File  $media
	 * @param  \Cartalyst\Filesystem\File  $file
	 * @return void
	 */
	public function deleting(File $media, \Cartalyst\Filesystem\File $file);

	/**
	 * On deleted event.
	 *
	 * @param  \Platform\Media\Models\Media  $media
	 * @return void
	 */
	public function deleted(File $media);

}
