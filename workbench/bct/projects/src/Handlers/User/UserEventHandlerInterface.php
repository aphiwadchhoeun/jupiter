<?php namespace Bct\Projects\Handlers\User;

use Bct\Projects\Models\User;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface UserEventHandlerInterface extends BaseEventHandlerInterface {

    /**
     * When a user is being created.
     *
     * @param  array $data
     * @return mixed
     */
    public function creating(array $data);

    /**
     * When a user is created.
     *
     * @param  \Bct\Projects\Models\User $user
     * @param $roleId
     * @return mixed
     */
    public function created(User $user, $roleId);

    /**
     * When a user is being updated.
     *
     * @param  \Bct\Projects\Models\User $user
     * @param  array $data
     * @return mixed
     */
    public function updating(User $user, array $data);

    /**
     * When a user is updated.
     *
     * @param  \Bct\Projects\Models\User $user
     * @param $roleId
     * @return mixed
     */
    public function updated(User $user, $roleId);

    /**
     * When a user is deleted.
     *
     * @param  \Bct\Projects\Models\User $user
     * @return mixed
     */
    public function deleted(User $user);

    public function loggedIn(UserInterface $user);

    public function loggedOut(UserInterface $user);

    public function profile(User $user);

}
