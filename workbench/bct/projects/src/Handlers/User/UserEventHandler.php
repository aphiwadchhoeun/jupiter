<?php namespace Bct\Projects\Handlers\User;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Bct\Projects\Models\User;
use Bct\Projects\Repositories\Log\LogRepositoryInterface;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;

class UserEventHandler extends BaseEventHandler implements UserEventHandlerInterface {

    /**
     * @var RequestRepositoryInterface
     */
    protected $logs;

    function __construct(Container $app, LogRepositoryInterface $logs) {
        parent::__construct($app);
        $this->logs = $logs;
    }

    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher) {
        $dispatcher->listen('bct.projects.user.creating', __CLASS__ . '@creating');
        $dispatcher->listen('bct.projects.user.created', __CLASS__ . '@created');

        $dispatcher->listen('bct.projects.user.updating', __CLASS__ . '@updating');
        $dispatcher->listen('bct.projects.user.updated', __CLASS__ . '@updated');

        $dispatcher->listen('bct.projects.user.deleted', __CLASS__ . '@deleted');

        $dispatcher->listen('platform.user.logged_in', __CLASS__ . '@loggedIn');
        $dispatcher->listen('platform.user.logged_out', __CLASS__ . '@loggedOut');

        $dispatcher->listen('bct.projects.user.profile', __CLASS__ . '@profile');

        $dispatcher->listen('bct.projects.user.newRole', __CLASS__ . '@newRole');
    }

    public function loggedIn(UserInterface $user) {
    }

    public function loggedOut(UserInterface $user) {
        // cleaning up all pre-loaded cache
    }


    /**
     * {@inheritDoc}
     */
    public function creating(array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function created(User $user, $roleId) {
        $user->roles()->attach($roleId);
        $user->save();

        $this->flushCache($user);
        $this->logs->logNewProfile($user);

        $this->reminder($user);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(User $user, array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function updated(User $user, $roleId) {
        if (!$user->roles->contains($roleId)) {
            $user->roles()->attach($roleId);
            $user->save();
        }

        $this->flushCache($user);
        $this->logs->logUpdateProfile($user);
    }

    /**
     * {@inheritDoc}
     */
    public function deleted(User $user) {
        $this->flushCache($user);
        $this->logs->logDeleteProfile($user);
    }

    /**
     * Flush the cache.
     *
     * @param  \Bct\Projects\Models\User $user
     * @return void
     */
    protected function flushCache(User $user) {
        $this->app['cache']->forget('bct.projects.user.all');

        $this->app['cache']->forget('bct.projects.user.' . $user->id);
    }

    /**
     * Returns the mailer instance.
     *
     * @return \Bct\Projects\Mailer\UserMailerInterface
     */
    protected function getMailer() {
        return $this->app['bct.projects.user.mailer'];
    }

    /**
     * When user accessing profile
     *
     * @param User $user
     */
    public function profile(User $user) {
        $this->flushCache($user);
    }

    protected function reminder(UserInterface $user) {
        $subject = trans('platform/users::email.subject.reset-password');

        $reminder = $this->app['platform.users']->getReminderRepository()->create($user);

        $reminderLink = $this->app['url']->route('user.password_reminder.confirm', [$user->id, $reminder->code]);

        $this->getMailer()->reminderEmail($user, $subject, 'reminder', compact('user', 'reminderLink'));
    }

    public function newRole(UserInterface $user, $role) {
        $subject = trans('platform/users::email.subject.new_role');

        $this->getMailer()->notifyNewRole($user, $subject, 'new_role', compact('user', 'role'));
    }

}



