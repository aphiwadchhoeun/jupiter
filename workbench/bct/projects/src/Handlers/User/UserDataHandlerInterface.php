<?php namespace Bct\Projects\Handlers\User;

interface UserDataHandlerInterface {

	/**
	 * Prepares the given data for being stored.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function prepareCreate(array $data);

	/**
	 * Prepare the given data for being updated
	 *
	 * @param array $data
	 * @return mixed
	 */
	public function prepareUpdate(array $data);

}
