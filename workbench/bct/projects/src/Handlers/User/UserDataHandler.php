<?php namespace Bct\Projects\Handlers\User;

class UserDataHandler implements UserDataHandlerInterface {


    /**
     * Prepares the given data for being stored.
     *
     * @param  array $data
     * @return mixed
     */
    public function prepareCreate(array $data) {
        $password = str_random(10);
        $data['password'] = $data['password_confirmation'] = $password;
        $data['activated'] = array_get($data, 'activated', 0);
        if($data['activated']==='on'){
            $data['activated']=1;
        }

        return $data;
    }

    /**
     * Prepare the given data for being updated
     *
     * @param array $data
     * @return mixed
     */
    public function prepareUpdate(array $data) {
        $data['activated'] = array_get($data, 'activated', 0);
        if($data['activated']==='on'){
            $data['activated']=1;
        }
        return $data;
    }
}
