<?php namespace Bct\Projects\Handlers\Contractor;

use Bct\Projects\Models\Contractor;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ContractorEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a contractor is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a contractor is created.
	 *
	 * @param  \Bct\Projects\Models\Contractor  $contractor
	 * @return mixed
	 */
	public function created(Contractor $contractor);

	/**
	 * When a contractor is being updated.
	 *
	 * @param  \Bct\Projects\Models\Contractor  $contractor
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Contractor $contractor, array $data);

	/**
	 * When a contractor is updated.
	 *
	 * @param  \Bct\Projects\Models\Contractor  $contractor
	 * @return mixed
	 */
	public function updated(Contractor $contractor);

	/**
	 * When a contractor is deleted.
	 *
	 * @param  \Bct\Projects\Models\Contractor  $contractor
	 * @return mixed
	 */
	public function deleted(Contractor $contractor);

}
