<?php namespace Bct\Projects\Handlers\Contractor;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Contractor;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use Bct\Projects\Repositories\Log\LogRepositoryInterface;

class ContractorEventHandler extends BaseEventHandler implements ContractorEventHandlerInterface {

	/**
	 * @var LogRepositoryInterface
	 */
	protected $logs;

	function __construct(Container $app, LogRepositoryInterface $logs) {
		parent::__construct($app);
		$this->logs = $logs;
	}

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.contractors.creating', __CLASS__.'@creating');
		$dispatcher->listen('bct.projects.contractors.created', __CLASS__.'@created');

		$dispatcher->listen('bct.projects.contractors.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.contractors.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.contractors.deleted', __CLASS__.'@deleted');
		$dispatcher->listen('bct.projects.contractors.merged', __CLASS__.'@merged');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Contractor $contractor)
	{
		$this->flushCache($contractor);
		$this->logs->logNewContractor($contractor);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Contractor $contractor, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Contractor $contractor)
	{
		$this->flushCache($contractor);
		$this->logs->logUpdateContractor($contractor);
	}



	/**
	 * {@inheritDoc}
	 */
	public function deleted(Contractor $contractor)
	{
		$this->flushCache($contractor);
		$this->logs->logDeleteContractor($contractor);
	}

	/**
	 * {@inheritDoc}
	 */
	public function merged(Contractor $contractor)
	{
		$this->flushCache($contractor);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\Contractor  $contractor
	 * @return void
	 */
	protected function flushCache(Contractor $contractor)
	{
		$this->app['cache']->forget('bct.projects.contractors.all');

		$this->app['cache']->forget('bct.projects.contractors.'.$contractor->id);
	}

}
