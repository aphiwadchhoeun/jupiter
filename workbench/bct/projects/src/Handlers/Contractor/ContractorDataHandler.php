<?php namespace Bct\Projects\Handlers\Contractor;

class ContractorDataHandler implements ContractorDataHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data)
	{
		$data['is_union'] = array_get($data, 'is_union', 0);
		$data['active'] = array_get($data, 'active', 0);
		if($data['is_union']==='on'){
			$data['is_union']=1;
		}
        if($data['active']==='on'){
            $data['active']=1;
        }

		return $data;
	}

}
