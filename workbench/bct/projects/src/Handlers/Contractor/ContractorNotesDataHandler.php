<?php namespace Bct\Projects\Handlers\Contractor;

use Bct\Projects\Handlers\BaseDataHandler;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class ContractorNotesDataHandler extends BaseDataHandler {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data) {

		$data['created_by'] = Sentinel::getUser()->getUserId();

		if($data['type']=='Other'){
			$data['type'] = $data['other-type-input'];

		}
		if(isset($data['other-type-input'])){
			unset($data['other-type-input']);
		}

		return $this->cleanXSS($data);

	}


}
