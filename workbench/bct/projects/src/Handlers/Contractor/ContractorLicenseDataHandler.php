<?php namespace Bct\Projects\Handlers\Contractor;

use Bct\Projects\Handlers\BaseDataHandler;

class ContractorLicenseDataHandler extends BaseDataHandler {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data) {

		if($data['type']=='Other'){
			$data['type'] = $data['other-type-input'];

		}
		if(isset($data['other-type-input'])){
			unset($data['other-type-input']);
		}
		return $this->cleanXSS($data);

	}


}
