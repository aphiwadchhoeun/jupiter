<?php namespace Bct\Projects\Handlers\Contact;

class ContactDataHandler implements ContactDataHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data)
	{
        $data['active'] = array_get($data, 'active', 0);
        if($data['active']==='on'){
            $data['active']=1;
        }
		return $data;
	}

}
