<?php namespace Bct\Projects\Handlers\Contact;

use Illuminate\Contracts\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Contact;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use Bct\Projects\Repositories\Log\LogRepositoryInterface;

class ContactEventHandler extends BaseEventHandler implements ContactEventHandlerInterface {

	/**
	 * @var LogRepositoryInterface
	 */
	protected $logs;

	function __construct(Container $app, LogRepositoryInterface $logs) {
		parent::__construct($app);
		$this->logs = $logs;
	}

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.contact.creating', __CLASS__.'@creating');
		$dispatcher->listen('bct.projects.contact.created', __CLASS__.'@created');

		$dispatcher->listen('bct.projects.contact.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.contact.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.contact.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Contact $contact)
	{
		$this->flushCache($contact);
		$this->logs->logNewContact($contact);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Contact $contact, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Contact $contact)
	{
		$this->flushCache($contact);
		$this->logs->logUpdateContact($contact);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Contact $contact)
	{
		$this->flushCache($contact);
		$this->logs->logDeleteContact($contact);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\Contact  $contact
	 * @return void
	 */
	protected function flushCache(Contact $contact)
	{
		$this->app['cache']->forget('bct.projects.contact.all');

		$this->app['cache']->forget('bct.projects.contact.stewards');

		$this->app['cache']->forget('bct.projects.contact.'.$contact->id);
	}

}
