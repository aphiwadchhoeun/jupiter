<?php namespace Bct\Projects\Handlers\Contact;

use Bct\Projects\Models\Contact;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ContactEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a contact is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a contact is created.
	 *
	 * @param  \Bct\Projects\Models\Contact  $contact
	 * @return mixed
	 */
	public function created(Contact $contact);

	/**
	 * When a contact is being updated.
	 *
	 * @param  \Bct\Projects\Models\Contact  $contact
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Contact $contact, array $data);

	/**
	 * When a contact is updated.
	 *
	 * @param  \Bct\Projects\Models\Contact  $contact
	 * @return mixed
	 */
	public function updated(Contact $contact);

	/**
	 * When a contact is deleted.
	 *
	 * @param  \Bct\Projects\Models\Contact  $contact
	 * @return mixed
	 */
	public function deleted(Contact $contact);

}
