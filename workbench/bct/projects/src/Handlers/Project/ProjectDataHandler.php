<?php namespace Bct\Projects\Handlers\Project;

use Carbon\Carbon;

class ProjectDataHandler implements ProjectDataHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data)
	{

		if (!$data['follow_up_date']) {
			$data['follow_up_date'] = Carbon::now()->addWeeks(2)->toDateString();
		} else {
			$data['follow_up_date'] = Carbon::createFromFormat('M-d-Y', $data['follow_up_date'])->toDateString();
		}


		return $data;
	}

}
