<?php namespace Bct\Projects\Handlers\Project;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Project;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use Bct\Projects\Models\Visit;
use Bct\Projects\Repositories\Log\LogRepositoryInterface;

class ProjectEventHandler extends BaseEventHandler implements ProjectEventHandlerInterface {

	/**
	 * @var LogRepositoryInterface
	 */
	protected $logs;

	function __construct(Container $app, LogRepositoryInterface $logs) {
		parent::__construct($app);
		$this->logs = $logs;
	}


	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.project.creating', __CLASS__.'@creating');
		$dispatcher->listen('bct.projects.project.created', __CLASS__.'@created');

		$dispatcher->listen('bct.projects.project.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.project.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.project.deleted', __CLASS__.'@deleted');
		$dispatcher->listen('bct.projects.project.merged', __CLASS__.'@merged');

	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Project $project, array $data)
	{
		$this->flushCache($project);

		$visit = new Visit();
		$visit->reason = data_get($data, 'followup_reason', '');
		$visit->details = 'Project was created!';

        if (config_projects('bct.projects.settings.visit_auto_approve')) {
            $visit->approved = 1;
            $visit->approved_user_id = Sentinel::getUser()->id;

        }

		$visit->project()->associate($project);
		$visit->save();
		$visit->users()->attach(Sentinel::getUser());
		$visit->stages()->sync(data_get($data, 'stages', []));

		$this->logs->logNewProject($project);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Project $project, array $data)
	{
        if ($visit = $project->getLastVisit()) {
            $visit->stages()->sync(data_get($data, 'stages', []));
        } else {
			$visit = new Visit();
			$visit->details = 'Project was updated!';
			$visit->project()->associate($project);
			$visit->save();
			$visit->users()->attach(Sentinel::getUser());
			$visit->stages()->sync(data_get($data, 'stages', []));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Project $project)
	{
		$this->flushCache($project);
		$this->logs->logUpdateProject($project);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Project $project)
	{
		$this->flushCache($project);
		$this->logs->logDeleteProject($project);
	}

	/**
	 * {@inheritDoc}
	 */
	public function merged(Project $project)
	{
		$this->flushCache($project);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\Project  $project
	 * @return void
	 */
	protected function flushCache(Project $project)
	{
		$this->app['cache']->forget('bct.projects.project.all');

		$this->app['cache']->forget('bct.projects.project.'.$project->id);
	}

}
