<?php namespace Bct\Projects\Handlers\Scope;

use Bct\Projects\Models\Scope;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ScopeEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a scope is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a scope is created.
	 *
	 * @param  \Bct\Projects\Models\Scope  $scope
	 * @return mixed
	 */
	public function created(Scope $scope);

	/**
	 * When a scope is being updated.
	 *
	 * @param  \Bct\Projects\Models\Scope  $scope
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Scope $scope, array $data);

	/**
	 * When a scope is updated.
	 *
	 * @param  \Bct\Projects\Models\Scope  $scope
	 * @return mixed
	 */
	public function updated(Scope $scope);

	/**
	 * When a scope is deleted.
	 *
	 * @param  \Bct\Projects\Models\Scope  $scope
	 * @return mixed
	 */
	public function deleted(Scope $scope);

}
