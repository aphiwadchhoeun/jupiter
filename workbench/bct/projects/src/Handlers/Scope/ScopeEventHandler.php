<?php namespace Bct\Projects\Handlers\Scope;

use Bct\Projects\Repositories\Log\LogRepositoryInterface;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Scope;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class ScopeEventHandler extends BaseEventHandler implements ScopeEventHandlerInterface {

    protected $logs;

    /**
     * ScopeEventHandler constructor.
     * @param Container $app
     * @param LogRepositoryInterface $logs
     */
    public function __construct(Container $app, LogRepositoryInterface $logs) {
        parent::__construct($app);
        $this->logs = $logs;
    }


    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher) {
        $dispatcher->listen('bct.projects.scope.creating', __CLASS__ . '@creating');
        $dispatcher->listen('bct.projects.scope.created', __CLASS__ . '@created');

        $dispatcher->listen('bct.projects.scope.updating', __CLASS__ . '@updating');
        $dispatcher->listen('bct.projects.scope.updated', __CLASS__ . '@updated');

        $dispatcher->listen('bct.projects.scope.deleted', __CLASS__ . '@deleted');
    }

    /**
     * {@inheritDoc}
     */
    public function creating(array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function created(Scope $scope) {
        $this->flushCache($scope);
        $this->logs->logNewScope($scope);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(Scope $scope, array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function updated(Scope $scope) {
        $this->flushCache($scope);
        $this->logs->logUpdateScope($scope);
    }

    /**
     * {@inheritDoc}
     */
    public function deleted(Scope $scope) {
        $this->flushCache($scope);
        $this->logs->logDeleteScope($scope);
    }

    /**
     * Flush the cache.
     *
     * @param  \Bct\Projects\Models\Scope $scope
     * @return void
     */
    protected function flushCache(Scope $scope) {
        $this->app['cache']->forget('bct.projects.scope.all');

        $this->app['cache']->forget('bct.projects.scope.' . $scope->id);

        if ($scope->classification == null) {
            $this->app['cache']->forget('bbct.pprofiles.projects.scopes.default');
        } else {
            $this->app['cache']->forget('bbct.pprofiles.projects.scopes.' . $scope->classification);
        }
    }

}
