<?php namespace Bct\Projects\Handlers\Group;

use Bct\Projects\Models\Group;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface GroupEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a group is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a group is created.
	 *
	 * @param  \Bct\Projects\Models\Group  $group
	 * @return mixed
	 */
	public function created(Group $group);

	/**
	 * When a group is being updated.
	 *
	 * @param  \Bct\Projects\Models\Group  $group
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Group $group, array $data);

	/**
	 * When a group is updated.
	 *
	 * @param  \Bct\Projects\Models\Group  $group
	 * @return mixed
	 */
	public function updated(Group $group);

	/**
	 * When a group is deleted.
	 *
	 * @param  \Bct\Projects\Models\Group  $group
	 * @return mixed
	 */
	public function deleted(Group $group);

}
