<?php namespace Bct\Projects\Handlers\Group;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Group;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use Bct\Projects\Repositories\Log\LogRepositoryInterface;

class GroupEventHandler extends BaseEventHandler implements GroupEventHandlerInterface {

	/**
	 * @var LogRepositoryInterface
	 */
	protected $logs;

	function __construct(Container $app, LogRepositoryInterface $logs) {
		parent::__construct($app);
		$this->logs = $logs;
	}

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.group.creating', __CLASS__.'@creating');
		$dispatcher->listen('bct.projects.group.created', __CLASS__.'@created');

		$dispatcher->listen('bct.projects.group.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.group.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.group.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Group $group)
	{
		$this->flushCache($group);
		$this->logs->logNewUserGroup($group);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Group $group, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Group $group)
	{
		$this->flushCache($group);
		$this->logs->logUpdateUserGroup($group);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Group $group)
	{
		$this->flushCache($group);
		$this->logs->logDeleteUserGroup($group);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\Group  $group
	 * @return void
	 */
	protected function flushCache(Group $group)
	{
		$this->app['cache']->forget('bct.projects.group.all');

		$this->app['cache']->forget('bct.projects.group.' . $group->id);
	}

}
