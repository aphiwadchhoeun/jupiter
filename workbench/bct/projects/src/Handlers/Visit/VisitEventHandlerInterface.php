<?php namespace Bct\Projects\Handlers\Visit;

use Bct\Projects\Models\Visit;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface VisitEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a visit is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a visit is created.
	 *
	 * @param  \Bct\Projects\Models\Visit  $visit
	 * @return mixed
	 */
	public function created(Visit $visit);

	/**
	 * When a visit is being updated.
	 *
	 * @param  \Bct\Projects\Models\Visit  $visit
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Visit $visit, array $data);

	/**
	 * When a visit is updated.
	 *
	 * @param  \Bct\Projects\Models\Visit  $visit
	 * @return mixed
	 */
	public function updated(Visit $visit);

	/**
	 * When a visit is deleted.
	 *
	 * @param  \Bct\Projects\Models\Visit  $visit
	 * @return mixed
	 */
	public function deleted(Visit $visit);

}
