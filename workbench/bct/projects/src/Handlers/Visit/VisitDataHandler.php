<?php namespace Bct\Projects\Handlers\Visit;

use Carbon\Carbon;

class VisitDataHandler implements VisitDataHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data)
	{
        if (!empty($data['visit_date']) && !empty($data['visit_time'])) {
            $data['created_at'] = Carbon::createFromFormat('M-d-Y g:i A',$data['visit_date'].' '.$data['visit_time'])->toDateTimeString();
            $data['updated_at'] = Carbon::createFromFormat('M-d-Y g:i A',$data['visit_date'].' '.$data['visit_time'])->toDateTimeString();
            unset($data['visit_date']);
            unset($data['visit_time']);
        }
		return $data;
	}

}
