<?php namespace Bct\Projects\Handlers\Visit;

use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Visit;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class VisitEventHandler extends BaseEventHandler implements VisitEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.visit.creating', __CLASS__.'@creating');
		$dispatcher->listen('bct.projects.visit.created', __CLASS__.'@created');

		$dispatcher->listen('bct.projects.visit.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.visit.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.visit.deleted', __CLASS__.'@deleted');

		$dispatcher->listen('bct.projects.visit.cache.clear', __CLASS__.'@cacheClear');

		$dispatcher->listen('bct.projects.visit.cache.clear.all', __CLASS__.'@cacheClearAll');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Visit $visit)
	{
		$this->flushCache($visit);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Visit $visit, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Visit $visit)
	{
		$this->flushCache($visit);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Visit $visit)
	{
		$this->flushCache($visit);
	}

	public function cacheClear(Visit $visit) {
		return $this->flushCache($visit);
	}

	public function cacheClearAll() {
		return $this->flushCacheAll();
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\Visit  $visit
	 * @return void
	 */
	protected function flushCache(Visit $visit)
	{
		$this->flushCacheAll();

		$this->app['cache']->forget('bct.projects.visit.'.$visit->id);
	}

	protected function flushCacheAll()
	{
		$this->app['cache']->forget('bct.projects.visit.all');
	}

}
