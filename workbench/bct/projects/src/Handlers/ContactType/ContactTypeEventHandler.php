<?php namespace Bct\Projects\Handlers\ContactType;

use Bct\Projects\Repositories\Log\LogRepositoryInterface;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\ContactType;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class ContactTypeEventHandler extends BaseEventHandler implements ContactTypeEventHandlerInterface {

    protected $logs;

    /**
     * ContactTypeEventHandler constructor.
     * @param Container $app
     * @param LogRepositoryInterface $logs
     */
    public function __construct(Container $app, LogRepositoryInterface $logs) {
        parent::__construct($app);
        $this->logs = $logs;
    }


    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher) {
        $dispatcher->listen('bct.projects.contact_type.creating', __CLASS__ . '@creating');
        $dispatcher->listen('bct.projects.contact_type.created', __CLASS__ . '@created');

        $dispatcher->listen('bct.projects.contact_type.updating', __CLASS__ . '@updating');
        $dispatcher->listen('bct.projects.contact_type.updated', __CLASS__ . '@updated');

        $dispatcher->listen('bct.projects.contact_type.deleted', __CLASS__ . '@deleted');
    }

    /**
     * {@inheritDoc}
     */
    public function creating(array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function created(ContactType $contacttype) {
        $this->flushCache($contacttype);
        $this->logs->logNewContactType($contacttype);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(ContactType $contacttype, array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function updated(ContactType $contacttype) {
        $this->flushCache($contacttype);
        $this->logs->logUpdateContactType($contacttype);
    }

    /**
     * {@inheritDoc}
     */
    public function deleted(ContactType $contacttype) {
        $this->flushCache($contacttype);
        $this->logs->logDeleteContactType($contacttype);
    }

    /**
     * Flush the cache.
     *
     * @param  \Bct\Projects\Models\ContactType $contacttype
     * @return void
     */
    protected function flushCache(ContactType $contacttype) {
        $this->app['cache']->forget('bct.projects.contact_type.all');

        $this->app['cache']->forget('bct.projects.contact_type.' . $contacttype->id);
    }

}
