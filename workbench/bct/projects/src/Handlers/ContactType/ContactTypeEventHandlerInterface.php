<?php namespace Bct\Projects\Handlers\ContactType;

use Bct\Projects\Models\ContactType;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ContactTypeEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a contacttype is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a contacttype is created.
	 *
	 * @param  \Bct\Projects\Models\ContactType  $contacttype
	 * @return mixed
	 */
	public function created(ContactType $contacttype);

	/**
	 * When a contacttype is being updated.
	 *
	 * @param  \Bct\Projects\Models\ContactType  $contacttype
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(ContactType $contacttype, array $data);

	/**
	 * When a contacttype is updated.
	 *
	 * @param  \Bct\Projects\Models\ContactType  $contacttype
	 * @return mixed
	 */
	public function updated(ContactType $contacttype);

	/**
	 * When a contacttype is deleted.
	 *
	 * @param  \Bct\Projects\Models\ContactType  $contacttype
	 * @return mixed
	 */
	public function deleted(ContactType $contacttype);

}
