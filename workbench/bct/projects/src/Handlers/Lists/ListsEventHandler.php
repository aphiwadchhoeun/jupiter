<?php namespace Bct\Projects\Handlers\Lists;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Lists;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;
use Bct\Projects\Repositories\Log\LogRepositoryInterface;

class ListsEventHandler extends BaseEventHandler implements ListsEventHandlerInterface {

	/**
	 * @var LogRepositoryInterface
	 */
	protected $logs;

	function __construct(Container $app, LogRepositoryInterface $logs) {
		parent::__construct($app);
		$this->logs = $logs;
	}


	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.lists.creating', __CLASS__.'@creating');
		$dispatcher->listen('bct.projects.lists.created', __CLASS__.'@created');

		$dispatcher->listen('bct.projects.lists.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.lists.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.lists.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Lists $lists)
	{
		$this->flushCache($lists);
		$this->logs->logNewList($lists);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Lists $lists, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Lists $lists)
	{
		$this->flushCache($lists);
		$this->logs->logUpdateList($lists);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Lists $lists)
	{
		$this->flushCache($lists);
		$this->logs->logDeleteList($lists);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\Lists  $lists
	 * @return void
	 */
	protected function flushCache(Lists $lists)
	{
		$this->app['cache']->forget('bct.projects.lists.all');

		$this->app['cache']->forget('bct.projects.lists.'.$lists->id);
	}

}
