<?php namespace Bct\Projects\Handlers\Lists;

use Carbon\Carbon;

class ListsDataHandler implements ListsDataHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data)
	{
		$data['due_date'] = Carbon::parse($data['due_date'])->toDateString();
		return $data;
	}

}
