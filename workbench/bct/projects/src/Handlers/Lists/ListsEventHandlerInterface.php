<?php namespace Bct\Projects\Handlers\Lists;

use Bct\Projects\Models\Lists;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ListsEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a lists is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a lists is created.
	 *
	 * @param  \Bct\Projects\Models\Lists  $lists
	 * @return mixed
	 */
	public function created(Lists $lists);

	/**
	 * When a lists is being updated.
	 *
	 * @param  \Bct\Projects\Models\Lists  $lists
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Lists $lists, array $data);

	/**
	 * When a lists is updated.
	 *
	 * @param  \Bct\Projects\Models\Lists  $lists
	 * @return mixed
	 */
	public function updated(Lists $lists);

	/**
	 * When a lists is deleted.
	 *
	 * @param  \Bct\Projects\Models\Lists  $lists
	 * @return mixed
	 */
	public function deleted(Lists $lists);

}
