<?php namespace Bct\Projects\Handlers\ActionType;

use Bct\Projects\Models\ActionType;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ActionTypeEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a actiontype is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a actiontype is created.
	 *
	 * @param  \Bct\Projects\Models\ActionType  $actiontype
	 * @return mixed
	 */
	public function created(ActionType $actiontype);

	/**
	 * When a actiontype is being updated.
	 *
	 * @param  \Bct\Projects\Models\ActionType  $actiontype
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(ActionType $actiontype, array $data);

	/**
	 * When a actiontype is updated.
	 *
	 * @param  \Bct\Projects\Models\ActionType  $actiontype
	 * @return mixed
	 */
	public function updated(ActionType $actiontype);

	/**
	 * When a actiontype is deleted.
	 *
	 * @param  \Bct\Projects\Models\ActionType  $actiontype
	 * @return mixed
	 */
	public function deleted(ActionType $actiontype);

}
