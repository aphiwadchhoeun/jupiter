<?php namespace Bct\Projects\Handlers\ActionType;

use Bct\Projects\Repositories\Log\LogRepositoryInterface;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\ActionType;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class ActionTypeEventHandler extends BaseEventHandler implements ActionTypeEventHandlerInterface {

    protected $logs;

    /**
     * ActionTypeEventHandler constructor.
     * @param Container $app
     * @param LogRepositoryInterface $logs
     */
    public function __construct(Container $app, LogRepositoryInterface $logs) {
        parent::__construct($app);
        $this->logs = $logs;
    }


    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher) {
        $dispatcher->listen('bct.projects.action_type.creating', __CLASS__ . '@creating');
        $dispatcher->listen('bct.projects.action_type.created', __CLASS__ . '@created');

        $dispatcher->listen('bct.projects.action_type.updating', __CLASS__ . '@updating');
        $dispatcher->listen('bct.projects.action_type.updated', __CLASS__ . '@updated');

        $dispatcher->listen('bct.projects.action_type.deleted', __CLASS__ . '@deleted');
    }

    /**
     * {@inheritDoc}
     */
    public function creating(array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function created(ActionType $actiontype) {
        $this->flushCache($actiontype);
        $this->logs->logNewActionType($actiontype);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(ActionType $actiontype, array $data) {

    }

    /**
     * {@inheritDoc}
     */
    public function updated(ActionType $actiontype) {
        $this->flushCache($actiontype);
        $this->logs->logUpdateActionType($actiontype);
    }

    /**
     * {@inheritDoc}
     */
    public function deleted(ActionType $actiontype) {
        $this->flushCache($actiontype);
        $this->logs->logDeleteActionType($actiontype);
    }

    /**
     * Flush the cache.
     *
     * @param  \Bct\Projects\Models\ActionType $actiontype
     * @return void
     */
    protected function flushCache(ActionType $actiontype) {
        $this->app['cache']->forget('bct.projects.action_type.all');

        $this->app['cache']->forget('bct.projects.action_type.' . $actiontype->id);
    }

}
