<?php namespace Bct\Projects\Handlers;

use Bct\Projects\Models\User;
use Sentinel;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 15.01.16
 * Time: 16:38
 */

class BaseDataHandler
{
    protected $currentUser;
    protected $currentUserModule;


    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->currentUser = Sentinel::getUser();
        $this->currentUserModule = (!is_null($this->currentUser)) ? User::find($this->currentUser->id) : null;
    }


    protected function cleanXSS($array)
    {
        // Recursive cleaning for array [] inputs, not just strings.
        return $this->arrayStripTags($array);
    }

    protected function arrayStripTags($array)
    {
        $result = array();

        foreach ($array as $key => $value) {
            // Don't allow tags on key either, maybe useful for dynamic forms.
            $key = strip_tags($key);

            // If the value is an array, we will just recurse back into the
            // function to keep stripping the tags out of the array,
            // otherwise we will set the stripped value.
            if (is_array($value)) {
                $result[$key] = static::arrayStripTags($value);
            } elseif (!is_object($value)) {
                // I am using strip_tags(), you may use htmlentities(),
                // also I am doing trim() here, you may remove it, if you wish.
                $result[$key] = trim(strip_tags($value));
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    public function getUserId()
    {
        $user_id = Sentinel::getUser()->id;

        return $user_id;
    }
}