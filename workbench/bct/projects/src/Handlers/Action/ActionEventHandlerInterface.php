<?php namespace Bct\Projects\Handlers\Action;

use Bct\Projects\Models\Action;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ActionEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a action is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a action is created.
	 *
	 * @param  \Bct\Projects\Models\Action  $action
	 * @return mixed
	 */
	public function created(Action $action);

	/**
	 * When a action is being updated.
	 *
	 * @param  \Bct\Projects\Models\Action  $action
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Action $action, array $data);

	/**
	 * When a action is updated.
	 *
	 * @param  \Bct\Projects\Models\Action  $action
	 * @return mixed
	 */
	public function updated(Action $action);

	/**
	 * When a action is deleted.
	 *
	 * @param  \Bct\Projects\Models\Action  $action
	 * @return mixed
	 */
	public function deleted(Action $action);

}
