<?php namespace Bct\Projects\Handlers\Action;

use Illuminate\Events\Dispatcher;
use Bct\Projects\Models\Action;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class ActionEventHandler extends BaseEventHandler implements ActionEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('bct.projects.action.creating', __CLASS__.'@creating');
		$dispatcher->listen('bct.projects.action.created', __CLASS__.'@created');

		$dispatcher->listen('bct.projects.action.updating', __CLASS__.'@updating');
		$dispatcher->listen('bct.projects.action.updated', __CLASS__.'@updated');

		$dispatcher->listen('bct.projects.action.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Action $action)
	{
		$this->flushCache($action);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Action $action, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Action $action)
	{
		$this->flushCache($action);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Action $action)
	{
		$this->flushCache($action);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Bct\Projects\Models\Action  $action
	 * @return void
	 */
	protected function flushCache(Action $action)
	{
		$this->app['cache']->forget('bct.projects.action.all');

		$this->app['cache']->forget('bct.projects.action.'.$action->id);
	}

}
