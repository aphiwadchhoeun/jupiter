<?php namespace Bct\Projects\Handlers\Action;

use Carbon\Carbon;

class ActionDataHandler implements ActionDataHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function prepare(array $data)
	{
	    $data['action_date'] = Carbon::createFromFormat('M-d-Y', $data['action_date']);
		return $data;
	}

}
