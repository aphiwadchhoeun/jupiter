<?php namespace Bct\Projects\Models\Views;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:52 PM
 */
use Carbon\Carbon;
use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;

class ContactView extends Model implements EntityInterface {
    use EntityTrait, NamespacedEntityTrait, SoftDeletes;

    /**
     * {@inheritDoc}
     */
    protected $table = 'pp_contacts';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    protected $dates = ['deleted_at'];

}