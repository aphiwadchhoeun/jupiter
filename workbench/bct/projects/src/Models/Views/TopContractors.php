<?php namespace Bct\Projects\Models\Views;


use Illuminate\Database\Eloquent\Model;

class TopContractors extends Model {

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'pp_top_contractors';

}