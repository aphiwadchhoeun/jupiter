<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 5/17/2016
 * Time: 3:48 PM
 */

namespace Bct\Projects\Models\Views;


use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Attributes\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;

class ProjectView extends Model implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'pp_projects';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    protected static $visitModel = 'Bct\Projects\Models\Visit';

    /**
     * Relationship with visit table
     */
    public function visits() {
        return $this->hasMany(static::$visitModel, 'z_projects_id');
    }


    /*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */
    public function getLastVisit() {
        return $this->visits()
            ->orderBy('created_at', 'desc')
            ->first();
    }

}