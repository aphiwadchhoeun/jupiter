<?php namespace Bct\Projects\Models\Views;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:52 PM
 */
use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;

class MarketShare extends Model implements EntityInterface {
    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'pp_market_share';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

}