<?php namespace Bct\Projects\Models\Views;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:52 PM
 */
use Carbon\Carbon;
use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;

class SummaryProject extends Model implements EntityInterface {
    use EntityTrait, NamespacedEntityTrait, SoftDeletes;

    /**
     * {@inheritDoc}
     */
    protected $table = 'pp_summary_profile';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    protected $dates = ['deleted_at', 'created_at'];

    protected static $visitModel = 'Bct\Projects\Models\Visit';

    /**
     * Relationship with visit table
     */
    public function visits() {
        return $this->hasMany(static::$visitModel, 'z_projects_id');
    }

    public function getCreatedAtAttribute()
    {
        return $this->getAttributes()['created_at'];
    }
}