<?php namespace Bct\Projects\Models\Views;


use Carbon\Carbon;
use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;

class MapProject extends Model implements EntityInterface {
	use EntityTrait, NamespacedEntityTrait, SoftDeletes;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'pp_map_project';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	protected $dates = ['deleted_at', 'created_at'];



	public function getCreatedAtAttribute()
	{
		return $this->getAttributes()['created_at'];
	}

	public function getFollowUpDateCustomAttribute() {
		return date_correct_details_projects($this->follow_up_date);
	}
}