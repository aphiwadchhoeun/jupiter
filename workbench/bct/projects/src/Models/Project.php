<?php namespace Bct\Projects\Models;

use Bct\Projects\Models\Contractor;
use Carbon\Carbon;
use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Cartalyst\Tags\TaggableTrait;
use Cartalyst\Tags\TaggableInterface;

class Project extends Model implements EntityInterface, TaggableInterface {

    use EntityTrait, NamespacedEntityTrait, SoftDeletes, TaggableTrait;

    static $STATUS_ACTIVE = 1, $STATUS_DISABLED = 2, $STATUS_COMPLETED = 3;

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_projects';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
        'stages'
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    protected $appends = [
        //'union_value',
        //'full_address',
        //'status_color',
        //'visibility_color',
        //'has_access_add_contractor',
        //'follow_up_date_custom',
    ];

    protected $dates = ['deleted_at'];

    /*
     * -----------------------------------
     *             RELATIONS
     * -----------------------------------
     */
    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/projects.project';

    protected static $prevailingWageModel = 'Bct\Projects\Models\PrevailingWage';

    protected static $stageModel = 'Bct\Projects\Models\Stage';

    protected static $referenceTypeModel = 'Bct\Projects\Models\ReferenceType';

    protected static $visibilityModel = 'Bct\Projects\Models\Visibility';

    protected static $statusModel = 'Bct\Projects\Models\Status';

    protected static $classificationModel = 'Bct\Projects\Models\Classification';

    protected static $valueModel = 'Bct\Projects\Models\Value';

    protected static $typeModel = 'Bct\Projects\Models\Type';

    protected static $contactModel = 'Bct\Projects\Models\Contact';

    protected static $visitModel = 'Bct\Projects\Models\Visit';

    protected static $contractorModel = 'Bct\Projects\Models\Contractor';

    protected static $actionModel = 'Bct\Projects\Models\Action';

    protected static $fileModel = 'Bct\Projects\Models\Media';

    protected static $listsModel = 'Bct\Projects\Models\Lists';

    protected static $usersModel = 'Bct\Projects\Models\User';

    protected static $memberModel = 'Bct\Projects\Models\Member';

    /**
     * Relationship with prevailing wage table
     */
    public function prevailingWage() {
        return $this->belongsTo(static::$prevailingWageModel, 'z_prevailing_wage_id');
    }

    /**
     * Relationship with stage table
     */
    public function stage() {
        return $this->belongsTo(static::$stageModel, 'z_project_stage_id');
    }

    /**
     * Relationship with reference type table
     */
    public function referenceType() {
        return $this->belongsTo(static::$referenceTypeModel, 'z_project_reference_type_id');
    }

    /**
     * Relationship with visibility table
     */
    public function visibility() {
        return $this->belongsTo(static::$visibilityModel, 'z_project_visibility_id');
    }

    /**
     * Relationship with status table
     */
    public function status() {
        return $this->belongsTo(static::$statusModel, 'z_project_status_id');
    }

    /**
     * Relationship with classification table
     */
    public function classification() {
        return $this->belongsTo(static::$classificationModel, 'z_project_classification_id');
    }

    /**
     * Relationship with value table
     */
    public function value() {
        return $this->belongsTo(static::$valueModel, 'z_project_values_id');
    }

    /**
     * Relationship with type table
     */
    public function type() {
        return $this->belongsTo(static::$typeModel, 'z_project_types_id');
    }

    /**
     * Relationship with contact table
     */
    public function contacts() {
        return $this->belongsToMany(static::$contactModel, 'z_project_contact_join', 'z_projects_id', 'z_contact_id')
            ->withPivot(['id', 'z_contact_type_id'])
            ->join('z_contact_type as type', 'z_project_contact_join.z_contact_type_id', '=', 'type.id');
    }

    /**
     * Relationship with users table
     */
    public function users() {
        return $this->belongsToMany(static::$usersModel, 'z_project_user_join', 'z_projects_id', 'user_id');
    }

    /**
     * Relationship with visit table
     */
    public function visits() {
        return $this->hasMany(static::$visitModel, 'z_projects_id');
    }

    /**
     * Relationship with action table
     */
    public function actions() {
        return $this->hasMany(static::$actionModel, 'z_projects_id');
    }

    /**
     * Relationship with files table
     */
    public function files() {
        return $this->hasMany(static::$fileModel, 'z_projects_id');
    }


    /**
     * Relationship with Contractor table
     */
    public function contractors() {
        return $this->belongsToMany(static::$contractorModel, 'z_project_contractor_join', 'z_projects_id', 'z_contractors_id')
            ->withPivot(['id', 'is_union', 'z_scopes_id', 'is_agreement'])
            ->join('z_scopes as scope', 'z_project_contractor_join.z_scopes_id', '=', 'scope.id');
    }

    /**
     * Relationship with Member table
     */
    public function members() {
        return $this->belongsToMany(static::$memberModel, 'z_project_member_join', 'z_projects_id', 'z_members_id')
            ->withPivot(['id', 'number_of_hours', 'z_scopes_id', 'wage'])
            ->join('z_scopes as scope', 'z_project_member_join.z_scopes_id', '=', 'scope.id');
    }

    /**
     * Relationship with lists table
     */
    public function lists() {
        return $this->belongsToMany(static::$listsModel, 'z_project_list_project_join', 'z_projects_id', 'z_project_list_id')
            ->withPivot([DB::raw('id as join_id'), DB::raw('created_by as join_created_by'), 'is_active', DB::raw('created_at as join_created_at')])
            ->join('users as user', 'z_project_list_project_join.created_by', '=', 'user.id')
            ->where('z_project_list_project_join.is_active', 1)
            ->select([
                DB::raw('CONCAT(user.first_name, " ", user.last_name) as username'),
                'z_project_list.id',
                'z_project_list.category',
                'z_project_list.z_projects_group_id',
                'z_project_list.name',
                'z_project_list.id as pid',
                'z_project_list.due_date',
                'z_project_list.is_active',
            ]);
    }


    /**
     * Relationship with lists table
     */
    public function all_lists() {
        return $this->belongsToMany(static::$listsModel, 'z_project_list_project_join', 'z_projects_id', 'z_project_list_id')
            ->withTimestamps()
            ->withPivot('created_by', 'is_active')
            ->join('users as user', 'z_project_list_project_join.created_by', '=', 'user.id')
            ->select([
                DB::raw('CONCAT(user.first_name, " ", user.last_name) as username'),
                'z_project_list.id',
                'z_project_list.name',
                'z_project_list.created_at',
                'z_project_list.id as pid',
            ]);
    }


    /*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */
    public function getFullAddressAttribute() {
        $address = [];

        if ($this->address) $address[] = $this->address;
        if ($this->city) $address[] = $this->city;
        if ($this->zipcode || $this->state) $address[] = $this->state . ' ' . $this->zipcode;

        return implode(', ', $address);
    }

    public function getStatusColorAttribute() {
        if (!array_key_exists('status', $this->getRelations())) {
            $this->load('status');
        }

        if ($this->status) {
            if ($this->status->name === 'Active') {
                return 'success';
            } elseif ($this->status->name === 'Disabled') {
                return 'danger';
            } elseif ($this->status->name === 'Completed') {
                return 'default';
            }
        }
    }

    public function getVisibilityColorAttribute() {
        if (!array_key_exists('visibility', $this->getRelations())) {
            $this->load('visibility');
        }

        if ($this->visibility) {
            if ($this->visibility->name === 'High') {
                return 'danger';
            } elseif ($this->visibility->name === 'Medium') {
                return 'warning';
            } else {
                return 'success';
            }
        }
    }

    public function getHasAccessAddContractorAttribute() {
        return ($this->classification) ? true : false;
    }

    public function getFollowUpDateCustomAttribute() {
        return date_correct_details_projects($this->follow_up_date);
    }

    public function getContractorsWithNaAttribute() {
        $data = $this
            ->select(
                'sc.value as value',
                'scope.name as scope',
                'z_contractors.id',
                'z_contractors.name',
                'z_contractors.license',
                'z_project_contractor_join.is_union',
                'z_project_contractor_join.is_agreement'
            )
            ->rightJoin('z_project_contractor_join', 'z_project_contractor_join.z_projects_id', '=', 'z_projects.id')
            ->leftJoin('z_contractors', 'z_contractors.id', '=', 'z_project_contractor_join.z_contractors_id')
            ->leftJoin('z_scopes as scope', 'z_project_contractor_join.z_scopes_id', '=', 'scope.id')
            ->join('z_scopes_classifications_join as sc', 'sc.z_scopes_id', '=', 'z_project_contractor_join.z_scopes_id')
            ->where('z_project_contractor_join.z_projects_id', $this->id)
            ->where('sc.z_classifications_id', $this->z_project_classification_id)
            ->groupBy('z_project_contractor_join.z_contractors_id', 'z_project_contractor_join.z_scopes_id')
            ->orderBy('scope.name', 'asc');

        return $data
            ->get();
    }

    public function getLastVisit() {
        return $this->visits()
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function newPivot(Model $parent, array $attributes, $table, $exists) {
        if ($parent instanceof Contact) {
            return new ProjectContact($parent, $attributes, $table, $exists);
        }
        if ($parent instanceof Contractor) {
            return new ProjectContractor($parent, $attributes, $table, $exists);
        }

        return parent::newPivot($parent, $attributes, $table, $exists);
    }

    public function setStatusDisabled() {
        $status_disabled = Status::name('Disabled')->first();

        return $this->update([
            'z_project_status_id' => $status_disabled->id
        ]);
    }

    public function setStatus($status) {
        return $this->update([
            'z_project_status_id' => $status
        ]);
    }

    public function getGC() {
        return $this->contractors()
            ->join('z_scopes', 'z_scopes.id', '=', 'z_scopes_id')
            ->where('z_scopes.name', 'GC')
            ->first();
    }

    /*
     * -----------------------------------
     *              SCOPES
     * -----------------------------------
     */
    public function scopeFromView($query) {
        return $query->from('pp_projects');
    }

    public function scopeFromReportView($query) {
        return $query->from('pp_report_projects');
    }

    public function scopeFromReportActiveProjectWithStewardsView($query) {
        return $query->from('pp_report_active_projects_with_stewards');
    }

    public function scopeFromReportActiveServiceJobsView($query) {
        return $query->from('pp_report_active_service_jobs');
    }

    public function reportRepSummary() {
        return User::select(
            DB::raw("CONCAT(`users`.`first_name`,' ',`users`.`last_name`) as repname"),
            "roles.name as role",
            DB::raw("COUNT(DISTINCT project_creations.id) as projects_created"),
            DB::raw("COUNT(DISTINCT project_visits.z_projects_id) as projects_visited"),
            DB::raw("COUNT(DISTINCT z_project_visit_user_id_join.z_project_visit_id) as total_visits")
        )
            ->leftJoin('role_users','role_users.user_id',"=",'users.id')
            ->leftJoin('roles','role_users.role_id',"=",'roles.id')
            ->rightJoin('z_project_visit_user_id_join','z_project_visit_user_id_join.user_id',"=",'users.id')
            ->leftJoin("z_project_visit as project_visits","z_project_visit_user_id_join.z_project_visit_id","=","project_visits.id")
            ->leftJoin('z_project_visit as project_creations', function($join){
                $join
                    ->on('z_project_visit_user_id_join.z_project_visit_id',"=",'project_creations.id')
                    ->on('project_creations.details', '=', DB::raw("'Project was created!'"));
            })
            ->where(function($query){
                $query
                    ->where('roles.slug','=',DB::raw("'repuser'"))
                    ->orWhere('roles.slug','=',DB::raw("'repadmin'"))
                    ->orWhere('roles.slug','=',DB::raw("'super-repadmin'"));
            });
    }
    public function reportContractorSummary() {
        return Contractor::select(
            "z_contractors.id as contractor_id",
            "z_contractors.name as contractor_name",
            "z_contractors.is_union as is_union",
            DB::raw("COUNT( DISTINCT total_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits"),
            DB::raw("COUNT( DISTINCT z_project_action.id ) as projects_actions")
        )
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_contractors_id',"=",'z_contractors.id')
            ->leftJoin("z_projects as total_projects","z_project_contractor_join.z_projects_id","=","total_projects.id")
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('total_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('total_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_action', function($join){
                $join
                    ->on('z_project_action.z_projects_id',"=",'total_projects.id')
                    ->on('z_contractors.id',"=",'z_project_action.z_contractors_id');
            })
            ->leftJoin('z_project_visit', function($join){
                $join
                    ->on('z_project_visit.z_projects_id',"=",'total_projects.id')
                    ->on('z_project_visit.details', '!=', DB::raw("'Project was created!'"));
            });
    }
    public function reportStageSummary() {
        return Stage::select(
            "z_project_stage.id as stage_id",
            "z_project_stage.name as stage_name",
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total")
        )
            ->leftJoin("z_project_visit_stage_join","z_project_visit_stage_join.z_project_stage_id","=","z_project_stage.id")
            ->leftJoin('z_project_visit','z_project_visit_stage_join.z_project_visit_id',"=",'z_project_visit.id')
            ->leftJoin("z_projects","z_project_visit.z_projects_id","=","z_projects.id")
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            });
    }
    public function reportContactsSummary() {
        return Contact::select(
            "z_contact.id as contact_id",
            DB::raw("CONCAT(z_contact.first_name,\" \",z_contact.last_name) as contact_name"),
            "z_contact.company as contact_company",
            DB::raw("COUNT( DISTINCT total_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits")
        )
            ->rightJoin('z_project_contact_join','z_project_contact_join.z_contact_id',"=",'z_contact.id')
            ->leftJoin("z_projects as total_projects","z_project_contact_join.z_projects_id","=","total_projects.id")
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('total_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('total_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'total_projects.id');
    }

    public function reportProjectRegionSummary() {
        return $this->select(
            "z_projects.region_abrv as region",
            DB::raw("COUNT( DISTINCT z_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits"),
            DB::raw("COUNT( DISTINCT z_project_contractor_join.z_contractors_id ) as projects_contractors"),
            DB::raw("COUNT( DISTINCT z_project_contact_join.z_contact_id ) as projects_contacts"),
            DB::raw("COUNT( DISTINCT z_project_action.id ) as projects_actions")
        )
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_contact_join','z_project_contact_join.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'z_projects.id')
            ->leftJoin('z_project_action','z_project_action.z_projects_id',"=",'z_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('z_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            });

    }

    public function reportActionsSummary() {
        return ActionType::select(
            "z_action_type.id as action_type_id",
            "z_action_type.name as action_type",
            DB::raw("COUNT( DISTINCT total_projects.id ) as projects_total"),
            DB::raw("COUNT( DISTINCT active_projects.id ) as projects_active"),
            DB::raw("COUNT( DISTINCT completed_projects.id ) as projects_completed"),
            DB::raw("COUNT( DISTINCT z_project_visit.id ) as projects_visits"),
            DB::raw("COUNT( DISTINCT z_project_contractor_join.z_contractors_id ) as projects_contractors")
        )
            ->leftJoin("z_project_action","z_project_action.z_action_type_id","=","z_action_type.id")
            ->leftJoin("z_projects as total_projects","z_project_action.z_projects_id","=","total_projects.id")
            ->leftJoin('z_projects as active_projects', function ($join) {
                $join
                    ->on('total_projects.id', '=', 'active_projects.id')
                    ->on('active_projects.z_project_status_id', '=', DB::raw("1"));
            })
            ->leftJoin('z_projects as completed_projects', function ($join) {
                $join
                    ->on('total_projects.id', '=', 'completed_projects.id')
                    ->on('completed_projects.z_project_status_id', '=', DB::raw("3"));
            })
            ->leftJoin('z_project_contractor_join','z_project_contractor_join.z_projects_id',"=",'total_projects.id')
            ->leftJoin('z_project_visit','z_project_visit.z_projects_id',"=",'total_projects.id')
            ->leftJoin('tagged', function ($join) {
                $join
                    ->on('total_projects.id', '=', 'tagged.taggable_id')
                    ->on('tagged.taggable_type', '=', DB::raw("'Bct\\\Projects\\\\Models\\\\Project'"));
            });

    }
}
