<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Group extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait, SoftDeletes;

	const STATUS_ENABLED = '1';
	const STATUS_DISABLED = '0';

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'z_projects_group';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	protected $dates = ['deleted_at'];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.group';

	protected static $userModel = 'Bct\Projects\Models\User';

	/**
	 * Relationship with user model
	 */
	public function users() {
		return $this->belongsToMany(static::$userModel, 'z_user_group_join', 'z_projects_groups_id', 'user_id')
			->withTimestamps();
	}

	public function scopeActive($query) {
		return $query->where('status', 1);
	}

}
