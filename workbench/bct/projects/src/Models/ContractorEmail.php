<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;

class ContractorEmail extends Model implements EntityInterface
{
	use EntityTrait, NamespacedEntityTrait;

	protected $table = 'z_contractor_email';

	public $timestamps = true;

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];


	/*
	 * -----------------------------------
	 *             RELATIONS
	 * -----------------------------------
	 */
	protected static $contractorModel = 'Bct\Projects\Models\Contractor';


	public function contractor()
	{
		return $this->belongsTo(static::$contractorModel, 'z_contractors_id');
	}

}