<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;

class ContractorNotes extends Model implements EntityInterface
{
	use EntityTrait, NamespacedEntityTrait;

	protected $table = 'z_contractor_notes';

	public $timestamps = true;

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];


	/*
	 * -----------------------------------
	 *             RELATIONS
	 * -----------------------------------
	 */
	protected static $contractorModel = 'Bct\Projects\Models\Contractor';
	protected static $userModel = 'Bct\Projects\Models\User';


	public function contractor()
	{
		return $this->belongsTo(static::$contractorModel, 'z_contractors_id');
	}

	public function creator()
	{
		return $this->belongsTo(static::$userModel, 'created_by');
	}

}