<?php namespace Bct\Projects\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 17.03.16
 * Time: 12:06
 */

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class ProjectVisitUser extends Model implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_project_visit_user_id_join';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/projects.visit.user';

}
