<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Scope extends Model implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait, SoftDeletes;

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_scopes';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    protected $dates = ['deleted_at'];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/projects.scope';
    protected static $classificationModel = 'Bct\Projects\Models\Classification';
    protected static $classificationPivotTable = 'z_scopes_classifications_join';

    public function classifications() {
        return $this->belongsToMany(static::$classificationModel, static::$classificationPivotTable,
            'z_scopes_id', 'z_classifications_id')
            ->withPivot('value')->withTimestamps();
    }

    public function scopeDefaultScopes($query) {
        return $query;
    }

    /**
     * @param $query
     * @param $classification
     */
    public function scopeOfClassification($query, $classification) {
        return $query->whereHas('classifications', function ($query) use ($classification) {
            $query->where('name', $classification);
        });
    }

}
