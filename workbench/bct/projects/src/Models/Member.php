<?php namespace Bct\Projects\Models;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 12:53
 */


use Bct\Projects\Traits\Model\DefaultTableTrait;
use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Member extends Model implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait, SoftDeletes, DefaultTableTrait {
        DefaultTableTrait::getQualifiedDeletedAtColumn insteadof SoftDeletes;
    }

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_members';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    protected $appends = [
        //'full_name'
    ];

    protected $dates = ['deleted_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setDefaultTable();
    }

    /*
     * -----------------------------------
     *             RELATIONS
     * -----------------------------------
     */

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/projects.member';

    protected static $projectModel = 'Bct\Projects\Models\Project';

    /**
     * Relationship with contact table
     */
    public function projects() {
        return $this->belongsToMany(static::$projectModel, 'z_project_member_join', 'z_members_id', 'z_projects_id');
    }

    /*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */
    public function getFullNameAttribute()
    {
        return array_to_string_projects([ str_limit($this->first_name, 64), str_limit($this->last_name, 64) ], ' ');
    }

    /*
     * -----------------------------------
     *              FUNCTIONS
     * -----------------------------------
     */

    public function scopeFromView($query)
    {
        return $query->from('pp_members');
    }

    public function scopeFromViewProject($query)
    {
        return $query->from('pp_project_members');
    }

    public function newPivot(Model $parent, array $attributes, $table, $exists) {
        if ($parent instanceof Project) {
            return new ProjectMember($parent, $attributes, $table, $exists);
        }
        return parent::newPivot($parent, $attributes, $table, $exists);
    }

}
