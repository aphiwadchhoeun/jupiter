<?php namespace Bct\Projects\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 25.01.16
 * Time: 14:10
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Attributes\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectContact extends Pivot
{


    protected $table = 'z_project_contact_join';

    public $timestamps = true;

    protected $guarded = [
        'id',
    ];

    public function __construct(Model $parent, $attributes, $table, $exists = false)
    {
        parent::__construct($parent, $attributes, $table, $exists);

        if (!array_key_exists('contact_type', $this->getRelations())) {
            $this->load('contact_type');
        }
    }

    protected static $contactTypeModel = 'Bct\Projects\Models\ContactType';


    public function contact_type() {
        return $this->belongsTo(static::$contactTypeModel, 'z_contact_type_id');
    }

}