<?php namespace Bct\Projects\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.12.15
 * Time: 15:39
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;

class ContractorAddress extends Model implements EntityInterface
{
	use EntityTrait, NamespacedEntityTrait;

	protected $table = 'z_contractor_address';

	public $timestamps = true;

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];


	/*
	 * -----------------------------------
	 *             RELATIONS
	 * -----------------------------------
	 */
	protected static $contractorModel = 'Bct\Projects\Models\Contractor';


	public function contractor()
	{
		return $this->belongsTo(static::$contractorModel, 'z_contractors_id');
	}

	public function getFullAddress(){
        $address = [];

        if ($this->address) $address[] = $this->address;
        if ($this->city) $address[] = $this->city;
        if ($this->zipcode || $this->state) $address[] = $this->state . ' ' . $this->zipcode;

        return implode(', ', $address);
	}


}