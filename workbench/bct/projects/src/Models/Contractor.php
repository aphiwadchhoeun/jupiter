<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Contractor extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait, SoftDeletes;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'z_contractors';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	protected $dates = ['deleted_at'];

	protected $appends = [
		//'is_union_text',
		//'is_union_color',
		//'location',
		//'notes_short',
	];

	/*
     * -----------------------------------
     *             RELATIONS
     * -----------------------------------
     */

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.contractor';

	protected static $projectModel = 'Bct\Projects\Models\Project';
	protected static $fileModel = 'Bct\Projects\Models\Media';
	protected static $summaryProjectModel = 'Bct\Projects\Models\Views\SummaryProject';
	protected static $userProfileModel = 'Bct\Projects\Models\UserProfile';
	protected static $addressModel = 'Bct\Projects\Models\ContractorAddress';
	protected static $phoneModel = 'Bct\Projects\Models\ContractorPhone';
	protected static $emailModel = 'Bct\Projects\Models\ContractorEmail';
	protected static $notesModel = 'Bct\Projects\Models\ContractorNotes';
	protected static $companyModel = 'Bct\Projects\Models\ContractorCompany';
	protected static $licenseModel = 'Bct\Projects\Models\ContractorLicense';

	/**
	 * Relationship with projects table
	 */
	public function projects() {
		return $this->belongsToMany(static::$projectModel, 'z_project_contractor_join', 'z_contractors_id', 'z_projects_id')
			->withPivot('is_union', 'z_scopes_id', 'is_agreement')
			->join('z_scopes as scope', 'z_project_contractor_join.z_scopes_id', '=', 'scope.id');
	}

	public function summaryProjects() {
		return $this->belongsToMany(static::$summaryProjectModel, 'z_project_contractor_join', 'z_contractors_id', 'z_projects_id')
			->withPivot('is_union', 'is_agreement');
	}

	/**
	 * Relationship with action table
	 */
	public function files() {
		return $this->hasMany(static::$fileModel, 'z_contractors_id');
	}

	public function contractors_address(){
		return $this->hasMany(static::$addressModel, 'z_contractors_id');
	}

	public function contractors_phone(){
		return $this->hasMany(static::$phoneModel, 'z_contractors_id');
	}

	public function contractors_email(){
		return $this->hasMany(static::$emailModel, 'z_contractors_id');
	}

	public function contractors_notes(){
		return $this->hasMany(static::$notesModel, 'z_contractors_id');
	}

	public function contractors_company(){
		return $this->hasMany(static::$companyModel, 'z_contractors_id');
	}

	public function contractors_license(){
		return $this->hasMany(static::$licenseModel, 'z_contractors_id');
	}

	/*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */
	public function getIsUnionTextAttribute()
	{
		return ($this->is_union) ? trans('bct/projects::general/common.yes') : trans('bct/projects::general/common.no');
	}

	public function getIsUnionColorAttribute()
	{
		return ($this->is_union) ? 'success' : 'danger';
	}

    public function getActiveTextAttribute()
    {
        return ($this->active) ? trans('bct/projects::general/common.yes') : trans('bct/projects::general/common.no');
    }

    public function getActiveColorAttribute()
    {
        return ($this->active) ? 'success' : 'danger';
    }

	public function getLocationAttribute()
	{
		return array_to_string_projects([ str_limit($this->address, 140), str_limit($this->city, 140), str_limit($this->state, 140), str_limit($this->zipcode, 140) ]);
	}

	public function getNotesShortAttribute()
	{
		return  str_limit($this->notes, 140);
	}


	public function getFullAddress() {
        $address = [];

        if ($this->address) $address[] = $this->address;
        if ($this->city) $address[] = $this->city;
        if ($this->zipcode || $this->state) $address[] = $this->state . ' ' . $this->zipcode;

        return implode(', ', $address);
	}


	public function newPivot(Model $parent, array $attributes, $table, $exists) {
		if ($parent instanceof Project) {
			return new ProjectContractor($parent, $attributes, $table, $exists);
		}
		return parent::newPivot($parent, $attributes, $table, $exists);
	}

	/**
	 * Relationship with user_profiles table
	 */
	public function user_profiles() {
		return $this->hasMany(static::$userProfileModel, 'z_contractors_id');
	}





}
