<?php namespace Bct\Projects\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.01.16
 * Time: 12:31
 */



use InvalidArgumentException;
use Cartalyst\Tags\TaggableTrait;
use Cartalyst\Tags\TaggableInterface;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Cartalyst\Attributes\EntityInterface;
use Platform\Attributes\Traits\EntityTrait;

class Media extends Model implements TaggableInterface, EntityInterface
{
    use EntityTrait, NamespacedEntityTrait, TaggableTrait;

    /**
     * {@inheritDoc}
     */
    public $table = 'media';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    protected $appends = [
        //'download_url',
        //'name_full',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/projects.media';

    protected static $userModel = 'Platform\Users\Models\User';

    protected static $projectModel = 'Bct\Projects\Models\Project';

    /**
     * Relationship with platform user
     */
    public function user() {
        return $this->belongsTo(static::$userModel, 'user_id');
    }

    /**
     * Relationship with project table
     */
    public function project() {
        return $this->belongsTo(static::$projectModel, 'z_projects_id');
    }

    /**
     * Get mutator for the "roles" attribute.
     *
     * @param  mixed  $roles
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getRolesAttribute($roles)
    {
        if (! $roles) {
            return [];
        }

        if (is_array($roles)) {
            return $roles;
        }

        if (! $_roles = json_decode($roles, true)) {
            throw new InvalidArgumentException("Cannot JSON decode roles [{$roles}].");
        }

        return $_roles;
    }

    /**
     * Set mutator for the "roles" attribute.
     *
     * @param  array  $roles
     * @return void
     */
    public function setRolesAttribute($roles)
    {
        // If we get a string, let's just ensure it's a proper JSON string
        if (! is_array($roles)) {
            $roles = $this->getRolesAttribute($roles);
        }

        if (! empty($roles)) {
            $roles = array_values(array_map('intval', $roles));
            $this->attributes['roles'] = json_encode($roles);
        } else {
            $this->attributes['roles'] = '';
        }
    }

    public function getDownloadUrlAttribute()
    {
        return route('bct.projects.media.download', $this->path);
    }

    public function getNameFullAttribute()
    {
        return $this->name . '.' . $this->extension;
    }
}
