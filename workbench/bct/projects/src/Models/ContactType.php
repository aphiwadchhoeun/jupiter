<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class ContactType extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait, SoftDeletes;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'z_contact_type';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	protected $dates = ['deleted_at'];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.contact_type';

}
