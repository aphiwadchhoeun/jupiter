<?php namespace Bct\Projects\Models;

/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 12:13
 */
use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Attributes\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectMember extends Pivot
{


    protected $table = 'z_project_member_join';

    public $timestamps = true;

    protected $guarded = [
        'id',
    ];

    protected $with = [
        'scope'
    ];

    public function __construct(Model $parent, $attributes, $table, $exists = false)
    {
        parent::__construct($parent, $attributes, $table, $exists);

        if (!array_key_exists('scope', $this->getRelations())) {
            $this->load('scope');
        }
    }



    protected static $scopeModel = 'Bct\Projects\Models\Scope';


    public function scope() {
        return $this->belongsTo(static::$scopeModel, 'z_scopes_id');
    }

    public function __get($key)
    {
        if ($key == 'scope')
        {
            return $this->scope()->first();
        }

        return parent::__get($key);
    }


}