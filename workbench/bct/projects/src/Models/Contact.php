<?php namespace Bct\Projects\Models;

use Bct\Projects\Traits\Model\DefaultTableTrait;
use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Contact extends Model implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait, SoftDeletes, DefaultTableTrait {
        DefaultTableTrait::getQualifiedDeletedAtColumn insteadof SoftDeletes;
    }

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_contact';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    protected $appends = [
        //'full_name',
        //'full_name_short',
        //'location',
        //'location_short',
        //'notes_short',
        //'title_short',
        //'email_short',
    ];

    protected $dates = ['deleted_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setDefaultTable();
    }

    /*
     * -----------------------------------
     *             RELATIONS
     * -----------------------------------
     */

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/projects.contact';

    protected static $userModel = 'Platform\Users\Models\User';

    protected static $projectModel = 'Bct\Projects\Models\Project';

    /**
     * Relationship with platform user
     */
    public function user() {
        return $this->belongsTo(static::$userModel, 'created_by');
    }

    /**
     * Relationship with contact table
     */
    public function projects() {
        return $this->belongsToMany(static::$projectModel, 'z_project_contact_join', 'z_contact_id', 'z_projects_id')
            ->withPivot('z_contact_type_id')
            ->join('z_contact_type as type', 'z_project_contact_join.z_contact_type_id', '=', 'type.id')
            ->leftJoin('z_project_status as status', function ($join) {
                $join->on('z_projects.z_project_status_id', '=', 'status.id');
            });
    }



    public function setStateAttribute($state) {

        $this->attributes['state'] = strtoupper($state);
    }

    /*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */
    public function getFullNameAttribute()
    {
        return array_to_string_projects([ str_limit($this->first_name, 64), str_limit($this->last_name, 64) ], ' ');
    }

    public function getFullNameShortAttribute()
    {
        return str_limit($this->full_name, 50);
    }

    public function getLocationAttribute()
    {
        return array_to_string_projects([ str_limit($this->address, 64), str_limit($this->city, 64), str_limit($this->state, 64), str_limit($this->zip, 64) ]);
    }

    public function getLocationShortAttribute()
    {
        return str_limit($this->location, 50);
    }

    public function getTitleShortAttribute()
    {
        return  str_limit($this->title, 50);
    }

    public function getNotesShortAttribute()
    {
        return  str_limit($this->notes, 50);
    }

    public function getEmailShortAttribute()
    {
        return  str_limit($this->email, 50);
    }


    /*
     * -----------------------------------
     *              FUNCTIONS
     * -----------------------------------
     */

    /**
     * Get contacts with type "steward"
     */
    public function getStewards(){

        return $this->select('z_contact.*')->leftJoin('z_project_contact_join as project_contact', function ($join) {
            $join->on('z_contact.id', '=', 'project_contact.z_contact_id');
        })->where('project_contact.z_contact_type_id',"=",'7');
    }

    public function scopeFromView($query)
    {
        return $query->from('pp_contacts');
    }

    public function scopeFromViewProject($query)
    {
        return $query->from('pp_project_contacts');
    }

    public function newPivot(Model $parent, array $attributes, $table, $exists) {
        if ($parent instanceof Project) {
            return new ProjectContact($parent, $attributes, $table, $exists);
        }
        return parent::newPivot($parent, $attributes, $table, $exists);
    }

    public function getActiveTextAttribute()
    {
        return ($this->active) ? trans('bct/projects::general/common.yes') : trans('bct/projects::general/common.no');
    }

    public function getActiveColorAttribute()
    {
        return ($this->active) ? 'success' : 'danger';
    }

}
