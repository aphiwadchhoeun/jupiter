<?php

namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;

class Local extends Model implements EntityInterface
{

    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'z_locals';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    static function getLocalsWithIdJson()
    {
        $items = Local::select(['id', 'name'])->get();
        return response()->json($items);
    }

}
