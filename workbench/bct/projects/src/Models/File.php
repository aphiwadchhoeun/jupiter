<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class File extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'media';

	/**
	 * {@inheritDoc}
	 */
	protected $fillable = [
		'mime',
		'name',
		'path',
		'size',
		'private',
		'is_image',
		'extension',
		'thumbnail',
		'width',
		'height',
		'roles',
		'user_id',
		'z_projects_id',
		'description',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.file';

	protected static $userModel = 'Platform\Users\Models\User';

	protected static $projectModel = 'Bct\Projects\Models\Project';

	protected static $contractorModel = 'Bct\Projects\Models\Contractor';

	/**
	 * Relationship with platform user
	 */
	public function user() {
		return $this->belongsTo(static::$userModel, 'user_id');
	}

	/**
	 * Relationship with project table
	 */
	public function project() {
		return $this->belongsTo(static::$projectModel, 'z_projects_id');
	}

	/**
	 * Relationship with contractor table
	 */
	public function contractor() {
		return $this->belongsTo(static::$contractorModel, 'z_contractors_id');
	}

}
