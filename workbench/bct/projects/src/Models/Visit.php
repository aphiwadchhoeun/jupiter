<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Visit extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait, SoftDeletes;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'z_project_visit';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	protected $appends = [
		//'users_names',
		//'stages_names',
	];

	protected $dates = ['deleted_at'];

	/*
     * -----------------------------------
     *             RELATIONS
     * -----------------------------------
     */
	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.visit';

	protected static $userModel = 'Bct\Projects\Models\User';

	protected static $projectModel = 'Bct\Projects\Models\Project';

	protected static $stageModel = 'Bct\Projects\Models\Stage';

	/**
	 * Relationship with platform user
	 */
	public function users() {
		return $this->belongsToMany(static::$userModel, 'z_project_visit_user_id_join',  'z_project_visit_id', 'user_id')
			->withTimestamps();
	}

	public function approved_user() {
		return $this->belongsTo(static::$userModel, 'approved_user_id');
	}

	/**
	 * Relationship with project table
	 */
	public function project() {
		return $this->belongsTo(static::$projectModel, 'z_projects_id');
	}

	/**
	 * Relationship with stage model
	 */
	public function stages() {
		return $this->belongsToMany(static::$stageModel, 'z_project_visit_stage_join',  'z_project_visit_id', 'z_project_stage_id')
			->withTimestamps();
	}

	/*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */

	public function getRepsNamesAttribute() {
		return collect($this->users)->map(function ($item, $key) {
			return empty($item['first_name']) ? $item['email'] : $item['first_name'] . ' ' . $item['last_name'];
		})->implode(', ');
	}

	public function getStagesNamesAttribute()
	{
		return collect($this->stages)->map(function ($item, $key) {
			return $item['name'];
		})->implode(', ');
	}

	public function getCreatedCustomAttribute()
	{
		return date_correct_details_projects($this->created_at);
	}

	public function getNotesShortAttribute()
	{
		return str_limit($this->details, 40);
	}

	/*
     * -----------------------------------
     *              SCOPES
     * -----------------------------------
     */

	public function scopeNotApproved($query)
	{
		return $query->where('approved', 0);
	}

	public function scopeFromView($query)
	{
		return $query->from('pp_reviews');
	}

	public function scopeFromViewProject($query)
	{
		return $query->from('pp_project_visits');
	}

}
