<?php namespace Bct\Projects\Models;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.12.15
 * Time: 15:39
 */

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;

class ContractorPhone extends Model implements EntityInterface
{
	use EntityTrait, NamespacedEntityTrait;

	protected $table = 'z_contractor_phone';

	public $timestamps = true;

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];


	/*
	 * -----------------------------------
	 *             RELATIONS
	 * -----------------------------------
	 */
	protected static $contractorModel = 'Bct\Projects\Models\Contractor';


	public function contractor()
	{
		return $this->belongsTo(static::$contractorModel, 'z_contractors_id');
	}

}