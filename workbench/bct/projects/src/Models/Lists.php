<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Lists extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait, SoftDeletes;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'z_project_list';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	protected $dates = ['deleted_at', 'due_date'];

	protected $appends = [
		'status_name'
	];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.lists';

	protected static $groupModel = 'Bct\Projects\Models\Group';

	protected static $projectModel = 'Bct\Projects\Models\Views\SummaryProject';

	protected static $projectViewModel = 'Bct\Projects\Models\Views\ProjectView';

	/**
	 * Relationship with group model
	 */
	public function group() {
		return $this->belongsTo(static::$groupModel, 'z_projects_group_id');
	}

	/**
	 * Relationship with project
	 */
	public function projects() {
		return $this->belongsToMany(static::$projectModel, 'z_project_list_project_join', 'z_project_list_id', 'z_projects_id');
	}

	/**
	 * Relationship with project view
	 */
	public function projects_view() {
		return $this->belongsToMany(static::$projectViewModel, 'z_project_list_project_join', 'z_project_list_id', 'z_projects_id')
			->withPivot('created_by', 'is_active');
	}
    
    
	/**
	 * Relationship with lists table
	 */
	public function projects_tab() {
		return $this->belongsToMany(static::$projectModel, 'z_project_list_project_join', 'z_project_list_id', 'z_projects_id')
			->withPivot('created_by', 'is_active')
			->with('status')
			->where('z_project_status_id', '<>', 3);
	}

	public function getStatusNameAttribute()
	{
		return ($this->is_active) ? trans('bct/projects::general/common.active') : trans('bct/projects::general/common.disabled');
	}



	public function scopeFromViewProject($query)
	{
		return $query->from('pp_project_lists');
	}

}
