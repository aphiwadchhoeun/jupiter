<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class TempItems extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'z_temp_user_project';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.tempitems';

	protected static $projectModel = 'Bct\Projects\Models\Project';

	protected static $userModel = 'Bct\Projects\Models\User';

	/**
	 * Relationship with temp model
	 */
	public function project() {
		return $this->belongsTo(static::$projectModel, 'z_projects_id');
	}

	/**
	 * Relationship with temp model
	 */
	public function user() {
		return $this->belongsTo(static::$userModel, 'user_id');
	}
}
