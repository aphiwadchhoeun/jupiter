<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Sentinel\Users\EloquentUser;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Support\Facades\Hash;
use Platform\Attributes\Traits\EntityTrait;
use Bct\Projects\Traits\Model\DefaultTableTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends EloquentUser implements EntityInterface {

    use EntityTrait, NamespacedEntityTrait, DefaultTableTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'users';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
		'created_at',
		'updated_at',
		'activated',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $appends = [
		'activated',
		'full_name'
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'activations',
		'profile'
	];

	protected $fillable = [
		'email',
		'password',
		'last_name',
		'first_name',
		'permissions',
		'z_user_profiles_id',
	];

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		$this->setDefaultTable();
	}

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'bct/projects.user';

    protected static $groupModel = 'Bct\Projects\Models\Group';

    protected static $tempModel = 'Bct\Projects\Models\TempItems';

    protected static $profileModel = 'Bct\Projects\Models\UserProfile';

	protected static $visitModel = 'Bct\Projects\Models\Visit';

	/**
	 * Relationship with project visits
	 */
	public function visits() {
		return $this->belongsToMany(static::$visitModel, 'z_project_visit_user_id_join',  'user_id', 'z_project_visit_id')
			->withTimestamps();
	}

	/**
	 * Relationship with group model
	 */
	public function groups() {
		return $this->belongsToMany(static::$groupModel, 'z_user_group_join', 'user_id', 'z_projects_groups_id')
			->withTimestamps();
	}

	/**
	 * Relationship with temp model
	 */
	public function tempItems() {
		return $this->hasMany(static::$tempModel, 'user_id')->orderBy('sortOrder');
	}

	/**
	 * Relationship with user profile table
	 */
	public function profile() {
		return $this->belongsTo(static::$profileModel, 'z_user_profiles_id');
	}

	public function setPasswordAttribute($pass){

		$this->attributes['password'] = Hash::make($pass);

	}

	/**
	 * Get mutator for the "activated" attribute.
	 *
	 * @return bool
	 */
	public function getActivatedAttribute()
	{
		$activation = $this->activations->sortByDesc('created_at')->first();

		return (bool) $activation ? $activation->completed : false;
	}

	public function getFullNameAttribute()
	{
		return array_to_string_projects([ str_limit($this->first_name, 64), str_limit($this->last_name, 64) ], ' ');
	}

	/*
     * -----------------------------------
     *              ACCESSORS
     * -----------------------------------
     */

	public function scopeEmail($query, $email)
	{
		$query->where('email', $email);
	}

	public function scopeFromView($query)
	{
		return $query->from('pp_users');
	}

}
