<?php namespace Bct\Projects\Models;

use Cartalyst\Attributes\EntityInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Action extends Model implements EntityInterface {

	use EntityTrait, NamespacedEntityTrait, SoftDeletes;

	/**
	 * {@inheritDoc}
	 */
	protected $table = 'z_project_action';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		'values.attribute',
	];

	protected $dates = ['deleted_at'];

	/**
	 * {@inheritDoc}
	 */
	protected static $entityNamespace = 'bct/projects.action';

	protected static $userModel = 'Bct\Projects\Models\User';

	protected static $projectModel = 'Bct\Projects\Models\Project';

	protected static $contractorModel = 'Bct\Projects\Models\Contractor';

	protected static $actionTypeModel = 'Bct\Projects\Models\ActionType';

	/**
	 * Relationship with platform user
	 */
	public function user() {
		return $this->belongsTo(static::$userModel, 'user_id');
	}

	/**
	 * Relationship with project table
	 */
	public function project() {
		return $this->belongsTo(static::$projectModel, 'z_projects_id');
	}

	/**
	 * Relationship with contractor table
	 */
	public function contractor() {
		return $this->belongsTo(static::$contractorModel, 'z_contractors_id');
	}


    /**
     * Custom Attributes
     *
     */

    public function getActionDateCustomAttribute()
    {
        return date_correct_details_projects($this->action_date);
    }

    public function getActionTypeNameAttribute()
    {
        if(is_object($this->belongsTo(static::$actionTypeModel, 'z_action_type_id')->first())){
            return $this->belongsTo(static::$actionTypeModel, 'z_action_type_id')->first()->name;
        }
    }


    /**
     * Scopes
     *
     */

	public function scopeFromViewProject($query)
	{
		return $query->from('pp_project_actions');
	}
}
