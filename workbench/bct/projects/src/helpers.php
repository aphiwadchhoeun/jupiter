<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 4/27/2015
 * Time: 11:36 AM
 */

use Carbon\Carbon;
use \Bct\Projects\Models\Config;

/**
 * displaying currency
 *
 * @param $money
 * @param int $decimal_point
 * @return string
 */
if (!function_exists('show_money_projects')) {
    function show_money_projects($money, $decimal_point = 2) {
        return '$' . number_format($money, $decimal_point);
    }
}

if ( ! function_exists('array_to_string_projects')) {
    function array_to_string_projects($data, $separator = ', ')
    {
        if (is_array($data)) {
            return collect($data)
                ->reject(function ($name) {
                    return empty($name);
                })
                ->implode($separator);
        } elseif ($data instanceof \Illuminate\Support\Collection || $data instanceof \Bct\Projects\CustomCollection) {
            return $data
                ->reject(function ($name) {
                    return empty($name);
                })
                ->implode($separator);
        }
    }
}

if ( ! function_exists('date_correct_projects')) {
    function date_correct_projects($date)
    {

        if ($date === '0000-00-00' || ! $date) {
            return '';
        } else {
            $dt = Carbon::parse($date);

            return $dt->format('m/d/Y');
        }
    }
}

if ( ! function_exists('date_correct_details_projects')) {
    function date_correct_details_projects($date)
    {
        if ($date === '0000-00-00') {
            return '';
        } else {
            $dt = Carbon::parse($date);

            return $dt->format('M-d-Y');
        }
    }
}

if ( ! function_exists('time_correct_details_projects')) {
    function time_correct_details_projects($datetime)
    {
        if ($datetime === '0000-00-00 00:00:00') {
            return '';
        } else {
            $dt = Carbon::parse($datetime);

            return $dt->format('g:i A');
        }
    }
}

if ( ! function_exists('datetime_correct_projects')) {
    function datetime_correct_projects($datetime)
    {

        if ($datetime === '0000-00-00 00:00:00') {
            return '';
        } else {
            $dt = Carbon::parse($datetime);

            return $dt->format('m/d/Y');
        }
    }
}

if ( ! function_exists('date_time_correct_pp')) {
    function date_time_correct_pp($datetime)
    {

        if ($datetime === '0000-00-00 00:00:00' || empty($datetime)) {
            return '';
        } else {
            $dt = Carbon::parse($datetime);

            return $dt->format('M-d-Y g:i A');
        }
    }
}

if (! function_exists('get_http_response_code_projects')) {
    function get_http_response_code_projects($theURL)
    {
        stream_context_set_default( [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);

        $headers = get_headers($theURL);

        return substr($headers[0], 9, 3);
    }
}

if (! function_exists('is_http_response_code_success_projects')) {
    function is_http_response_code_success_projects($theURL)
    {
        if (get_http_response_code_projects($theURL) === '200' || get_http_response_code_projects($theURL) === '302') {
            return true;
        }

        return false;
    }
}

if ( ! function_exists('asset_project_files_path_projects')) {
    function asset_project_files_path_projects($filename = '')
    {
        $folder = 'projects/files/';
        $folder_s3 = 'bct/projects/' . $folder;
        $path = ($filename) ? $folder_s3 . $filename : $folder_s3;

        return $path;
    }
}

if ( ! function_exists('custom_files_name_projects')) {
    function custom_files_name_projects($filename = '')
    {
        $pos = strrpos($filename, '.');
        if (!$pos) {
            return $filename;
        }
        return substr($filename, 0, $pos);
    }
}

if ( ! function_exists('asset_contractor_files_path_projects')) {
    function asset_contractor_files_path_projects($filename = '')
    {
        $folder = 'contractors/files/';
        $folder_s3 = 'bct/projects/' . $folder;
        $path = ($filename) ? $folder_s3 . $filename : $folder_s3;

        return $path;
    }
}

if ( ! function_exists('config_projects')) {
    /**
     * @param      $key
     * @param null $default
     *
     * @return null
     */
    function config_projects($key, $default = null)
    {
        try {
            $option = Config::findByItem($key)->first();

            if (empty( $option )) {
                return $default;
            }

            if (is_numeric($option->value)) {
                if ((int) $option->value == $option->value) {
                    return intval($option->value);
                }

                return (float)$option->value;
            }

            return $option->value;
        } catch (PDOException $e) {
            return $default;
        }
    }
}

if ( ! function_exists('get_export_column_name')) {
    function get_export_column_name($column)
    {
        if (is_array($column) && !empty($column)) {
            return $column[0];
        }

        return $column;
    }
}

if ( ! function_exists('formatBytes')) {
    function formatBytes($size, $precision = 2)
    {
        $base     = log($size, 1024);
        $suffixes = [ '', 'K', 'M', 'G', 'T' ];

        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
    }
}

if (! function_exists('formatMoneyToFloat')) {
    function formatMoneyToFloat($money) {
        return trim(str_replace('$', '', $money));
    }
}

if ( ! function_exists('get_input_filter_value_by_key_pp')) {
    /**
     * @param $filter_name
     * @param $key_name
     *
     * @return mixed
     */
    function get_input_filter_value_by_key_pp($filter_name, $key_name, $daterange = false)
    {
        if ($daterange === true) {
            $filter = collect(input($filter_name))->first(function ($value, $key) use ($key_name) {
                return key($key) === $key_name;
            });

            $value = $filter[$key_name];

            if ($value) {
                preg_match('/\|>=([^\|]*)\|<=([^\|]*)\|?/i', $value, $matches);

                return [$matches[1], $matches[2]];
            }
        } else {
            return data_get(
                collect(input($filter_name))->first(function ($value, $key) use ($key_name) {
                    return key($key) === $key_name;
                }),
                $key_name
            );
        }

    }
}