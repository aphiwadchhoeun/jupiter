<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 10:57
 */

return [

    'configuration'        => 'Settings \ Configuration',
	'groups'               => 'Settings \ Groups',
	'users'                => 'Settings \ Users',

];
