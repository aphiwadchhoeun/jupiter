<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 10:59
 */

return [

    'not_found' => 'Setting #:id not found',
    'no_access' => 'No access to Setting #:id',

    'success' => [
        'created' => 'Setting Record successfully created',
        'updated' => 'Setting Record successfully updated',
        'deleted' => 'Setting Record successfully deleted',
    ],
];
