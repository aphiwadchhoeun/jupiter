<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.02.16
 * Time: 11:21
 */

return [
    'map'                          => [
        'radius'         => 'Map \ Radius (miles)',
        'count_projects' => 'Map \ Count Projects',
        'less_4'         => 'Map \ Project Last follow up date less than :start weeks',
        '4_8'            => 'Map \ Project Last follow up date :start-:end weeks',
        'more_8'         => 'Map \ Project Last follow up date more than :end weeks',
        'week'           => [
            'start' => 'Week Start',
            'end'   => 'Week End'
        ]
    ],
    'interval_number'              => 'Interval :num',
    'interval_week'                => 'Week Interval :num',
    'interval_union'               => 'Union Percentage Interval :num',
    'visit_auto_approve'           => 'Auto Approval',
    'project_field_report'         => 'Enable Project Field Report',
    'enable_project_visit_workers' => 'Enable Project Visit Workers',
    'enable_project_visit_weather' => 'Enable Project Visit Weather',
    'enable_project_visit_date'    => 'Enable Project Visit Date',

];