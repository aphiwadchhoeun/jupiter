<?php

return [

    'title'                 => 'Lists',
    'settings'              => 'Settings',
	'find_projects'         => 'Create a site visit in each project to complete the project off of the list.',
	'map_selected'          => 'Map Selected',
    'find_projects_tab'     => 'Please find the Project you would like to add and click the PLUS icon.',
    'add_to_list'           => 'Add To List',
    'remove_from_list'      => 'Remove From List',
    'routes_step_1'         => 'Step 1: Select the list you would like to use',
    'routes_step_2'         => 'Step 2: Select up to 8 projects and click "Map Selected"',
    'edit_projects'         => 'Edit projects',
    
    'tabs'	=> [
		'details'		=>	'Details',
		'projects'      =>	'Projects',
		'reps'          =>	'Reps',
	]
];
