<?php

return [

    'general' => [

        'id'               => 'Id',
        'name'             => 'Name',
        'username'         => 'User',
        'group_id'         => 'Group',
        'due_date'         => 'Due Date',
        'date_added'       => 'Date Added',
        'created_at'       => 'Created',
        'status'           => 'Status',
        'list_status'      => 'List Status',
        'active'           => 'Active',
        'inactive'         => 'Disabled',
        'category'         => 'Category',
        'description'      => 'Description',
        'name_help'        => 'Enter The Name here',
        'group_id_help'    => 'Select Group',
        'due_date_help'    => 'Enter Due Date here',
        'description_help' => 'Enter Description here',
        'category_help'    => 'Enter Category here',
        'date'             => 'Date',
        'group'            => 'Group',

    ],

];
