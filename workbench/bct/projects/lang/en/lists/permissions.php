<?php

return [

    'index'   		=> 'Lists Management',
    'search'   		=> 'Routes Tab',
    'create'  		=> 'Create new List',
    'edit'    		=> 'Edit List',
    'delete'  		=> 'Delete List',
	'access_error' 	=> 'You don\'t have access to this list.',
	'search_l'      => 'Lists Tab'
];
