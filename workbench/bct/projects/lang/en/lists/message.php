<?php

return [

	// General messages
	'not_found'        => 'List [:id] does not exist.',
	'delete'        => 'List has been successfully deleted.',
	'no_groups'     => 'You need to be a part of some group to be able to create a list.',

	// Success messages
	'success'          => [
		'create'  => 'List was successfully created.',
		'update'  => 'List was successfully updated.',
		'delete'  => 'List was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the list. Please try again.',
		'update' => 'There was an issue updating the list. Please try again.',
		'delete' => 'There was an issue deleting the list. Please try again.',
		'attach' => 'There was an issue attaching the users. Please try again.',
	],

];