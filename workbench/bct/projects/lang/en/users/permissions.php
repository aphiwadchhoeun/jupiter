<?php

return [

    'index'   => 'List Users',
    'create'  => 'Create new User',
    'edit'    => 'Edit User',
    'delete'  => 'Delete User',
    'profile' => 'MRP User Profile',
    'visits'  => 'User Visits',
];
