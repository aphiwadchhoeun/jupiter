<?php

return [

    'general' => [

        'id'                     => 'Id',
        'name'                   => 'Name',
        'first_name'             => 'First Name',
        'last_name'              => 'Last Name',
        'email'                  => 'Email',
        'phone'                  => 'Phone',
        'group'                  => 'Group',
        'license'                => 'License',
        'ein'                    => 'Ein',
        'status'                 => 'Status',
        'created_at'             => 'Created',
        'role'             	 	 => 'Role',
        'password'             	 => 'Password',
        'local'             	 => 'Local',
        'groups'             	 => 'Groups',
        'name_help'              => 'Enter the Name here',
        'first_name_help'        => 'Enter the First Name here',
        'last_name_help'         => 'Enter the Last Name here',
        'phone_help'             => 'Enter the Phone here',
        'status_help'            => 'Tick the Status here',
        'email_help'             => 'Enter the Email here',
        'password_help'          => 'Enter the Password here',
        'password_confirm_help'  => 'Enter the Password Confirmation here',
        'local_help'  			 => 'Enter the Local here',
        'role_help'  			 => 'Select User Role',
        'create_group'           => 'Create Individual Group'
    ],

];
