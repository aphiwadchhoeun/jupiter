<?php

return [

    // General messages
    'not_found'        => 'User [:id] does not exist.',
    'fail'             => 'Could not access the form you\'re looking for ',
    'profile_instruct' => 'Profile information is required before all features in the system becomes activated.',
    'status_instruct'  => 'Profile status need to be reviewed by Administrators.',

    // Success messages
    'success'          => [
        'create'     => 'User was successfully created.',
        'update'     => 'User was successfully updated.',
        'delete'     => 'User was successfully deleted.',
        'link'       => 'User was successfully linked.',
        'profile'    => 'Profile was successfully updated.',
        'reset'      => 'Reset password succesful! New password has been sent by email!',
        'new_role'   => 'New role attached succesfully! Notification has been sent!',
        'add_user'   => 'Notification has been sent by email!'
        
    ],

    // Error messages
    'error'            => [
        'create' => 'There was an issue creating the user. Please try again.',
        'update' => 'There was an issue updating the user. Please try again.',
        'delete' => 'There was an issue deleting the user. Please try again.',
        'send'  => 'There was an issue sending the email. Please try again or check email settings.',
    ],

];
