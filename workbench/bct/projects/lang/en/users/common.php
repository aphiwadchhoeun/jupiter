<?php

return [

    'title'                 => 'Users',
    'settings'              => 'Settings',
    'profile'               => 'Profile',
    'profile_details'       => 'Profile details',
    'assigned_groups'       => 'Assigned Groups',
    'not_assigned_groups'   => 'Not Assigned Groups',
    'add_to_all_group'      => 'Add To Selected Groups',
    'remove_from_all_group' => 'Remove From Selected Groups',
    'title_reps'            => 'Reps',
    'find_groups_tab'       => 'Please find the Group you would like to add and click the PLUS icon.',
    'edit_groups'           => 'Edit groups',
    'available_reps'        => 'Available Reps',
    'group_reps'            => 'Group Reps',
	'user_profiles'         => 'User Profiles',

    'tabs' => [
        'details' => 'Details',
        'groups'  => 'Groups',
        'visits'  => 'Visits'
    ]
];
