<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.01.16
 * Time: 14:12
 */

return [
    'title' => 'Reports',

    'tabs' => [
        'report1_projects'               => 'Union Job List per Area',
        'report2_projects'               => 'Active Projects with Stewards',
        'report3_projects'               => 'Active Service Jobs List',
        'rep_summary'                    => 'Rep Summary',
        'contractor_summary'             => 'Contractor Summary',
        'stage_summary'                  => 'Stage Summary',
        'wage_summary'                   => 'Prevailing Wage Summary',
        'contacts_summary'               => 'Contacts Summary',
        'project_classification_summary' => 'Project Classification Summary',
        'project_region_summary'         => 'Project Region Summary',
        'project_type_summary'           => 'Project Type Summary',
        'actions_summary'                => 'Actions Summary',
        'summary_projects'               => 'Projects'
    ],

    'blocks' => [
        'project'    => 'Project',
        'contact'    => 'Contacts',
        'contractor' => 'Contractor',
        'custom'     => 'Custom'
    ],

    'text' => [
        'report1' => [
            'select_regions'       => 'Please select Regions',
            'select_scopes'        => 'Please select Scopes',
            'include_non_union_gc' => 'Inclucde non-union GC'
        ],

        'report2' => [
            'select_regions' => 'Please select Regions'
        ],

        'report3' => [
            'select_regions' => 'Please select Regions'
        ]
    ],
    'placeholder' => [
        'region'                 => 'Region',
        'contractor'             => 'Contractor',
        'project_type'           => 'Project Type',
        'tag'                    => 'Tag',
        'project_classification' => 'Classification',
    ],
    'col' => [
        'project_id'             => 'Job#',
        'project_name'           => 'Job Name',
        'project_address'        => 'Address',
        'project_city'           => 'City',
        'stewards'               => 'Stewards',
        'contractor_name'        => 'Contractor Name',
        'region'                 => 'Region',
        'repname'                => 'Repname',
        'projects_created'       => 'Projects Created',
        'active_projects'        => 'Active Projects',
        'completed_projects'     => 'Completed Projects',
        'completed_count'        => 'Completed Count',
        'total_projects'         => '# of Projects',
        'project_count'          => 'Project Count',
        'name_project'           => 'Project Name',
        'active_count'           => 'Active Count',
        'projects_visited'       => 'Projects Visited',
        'projects_visits'        => 'Project Visits',
        'total_visits'           => 'Total Visit Entries',
        'number_of_visits'       => '# of Visit Entries',
        'total_actions'          => 'Total Actions',
        'number_of_actions_taken'=> '# of Actions Taken',
        'number_of_actions'      => '# of Actions',
        'stage_name'             => 'Stage Name',
        'wage_type'              => 'Prevailing Wage Type',
        'contact_name'           => 'Contact Name',
        'number_of_contacts'     => '# of Contacts',
        'contact_company'        => 'Contact Company',
        'classification_name'    => 'Classification Name',
        'total_contacts'         => 'Total Contacts',
        'number_of_contacts'     => '# of Contacts',
        'total_contractors'      => 'Total Contractors',
        'number_of_contractors'  => '# of Contractors',
        'type_name'              => 'Type Name',
        'action_type'            => 'Action Type',
        'union'                  => 'Union/Non Union',
        'contractor'             => 'Contractor',
        'tag'                    => 'Tag',
        'project_type'           => 'Project Type',
        'project_classification' => 'Project Classification',
        'summary_projects'       => 'Projects',
        'role'                   => 'Role',
        'last_visit'             => 'Last Visit Date'
    ],

    'last_30_days' => 'Last 30 Days',
    'last_90_days' => 'Last 90 Days',
    'visit_date' => 'Visit Date'
];