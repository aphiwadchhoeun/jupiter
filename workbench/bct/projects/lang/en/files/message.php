<?php

return [

	// General messages
	'not_found'        => 'File [:id] does not exist.',

	// Success messages
	'success'          => [
		'create'  => 'File was successfully created.',
		'update'  => 'File was successfully updated.',
		'delete'  => 'File was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the File. Please try again.',
		'update' => 'There was an issue updating the File. Please try again.',
		'delete' => 'There was an issue deleting the File. Please try again.',
	],

];