<?php

return [

    'general' => [
        'id'          => 'Id',
        'created_at'  => 'Created',
        'user'        => 'User',
        'type'        => 'Type',
        'name'        => 'Name',
        'description' => 'Description',
        'date'        => 'Date',

        'name_help'        => 'Enter The Name',
        'description_help' => 'Enter The Description',
    ],

];
