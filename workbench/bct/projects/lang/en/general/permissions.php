<?php

return [

    'dashboard'        => 'MRP Dashboard',
    'make_request'     => 'Make MRP Request',
    'make_response'    => 'Make Bid Response',
    'admin_dashboard'  => 'Admin Dashboard',
    'hours_submission' => 'Hours Submission',
    'media'            => 'Download / View Files',
    'locals'           => 'Locals',
    'reviews'          => 'Reviews',
    'visits'           => 'Visits',
    'reports'          => 'Reports',
];
