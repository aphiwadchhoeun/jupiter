<?php

return [

    'actions'      => 'Actions',
    'edit'         => 'Edit',
    'settings'     => 'Settings',
    'details'      => 'Details',
    'cancel'       => 'Cancel',
    'back'         => 'Back',
    'next'         => 'Next',
    'finish'       => 'Finish',
    'remove'       => 'Remove',
    'search'       => 'Search',
    'commit'       => 'Commit',
    'undo'         => 'Undo',
    'response'     => 'Bid Response',
    'hour_details' => 'Hour Submission Details',
    'submit_hours' => 'Submit Hours',
    'form_note'    => 'Your information will only be saved when you click "Next"',
    'unlock'       => 'Unlock',
    'reset'        => 'Reset password',
    'active'       => 'Active',
    'disabled'     => 'Disabled',
    'disable'     => 'Disable',
    'select'       => 'Select',
    'updated'      => 'Updated',
    'updated_at'   => 'Updated At',
    'print'        => 'Print',
    'is_union'     => 'Is Union',
    'is_not_union' => 'Is Not Union',
    'yes'          => 'Yes',
    'no'           => 'No',
    'address'      => 'Address',
    'of'           => 'of',
    'merge'        => 'Merge',
    'visited'      => 'Visited',
    'not_visited'  => 'Not visited',
    'update'       => 'Update',
    'approve'      => 'Approve',
    'unapprove'    => 'Unapprove',
    'id'           => 'ID',
    'tags'         => 'Tags',
    'create'       => 'Create',
    'select_all'   => 'Select All',
    'deselect_all' => 'Deselect All',
    'na'           => 'N/A',
    'na_only'      => 'N/A Only',
	'status'       => 'Status',
	'jobs'         => 'Jobs',
	'merge_confirmation'    => 'Merge Confirmation',
	'confirm'    => 'Confirm',

    'breadcrumbs' => [
        'project_info' => 'Project Information',
        'bidding_info' => 'Bidding Information',
        'scopes_info'  => 'Request',
        'bidders_info' => 'Other Bidders',
        'overview'     => 'Overview'
    ],

    'buttons' => [
        'show_all'          => 'Show All',
        'show_active'       => 'Show Active',
        'show_union'        => 'Show "Is Union"',
        'show_not_union'    => 'Show "Is Not Union"',
        'show_active'       => 'Show Active',
        'show_pending'      => 'Show Pending',
        'toggle_navigation' => 'Toggle navigation',
        'toggle_dropdown'   => 'Toggle Dropdown',
        'csv'               => 'CSV',
        'pdf'               => 'PDF'
    ],

	'placeholder' => [
		'fill_this' => 'Fill this for ',
		'optional'  => 'optional'
	],



];
