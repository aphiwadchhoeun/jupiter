<?php

return [

    // General messages
    'not_found'         => 'The current User Profile is not found. Please goto Account > Profile.',
    'status_off'        => 'Your current Profile status is Inactive. Please wait for administrator to review your profile.',
    'fail'              => 'Could not access the form you\'re looking for.',
    'back_button_error' => 'Sorry, something went wrong. Please try submitting request again.',
];
