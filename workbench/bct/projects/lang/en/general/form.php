<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 25.01.16
 * Time: 16:58
 */

return [
    'created_at'       => 'Created at',
    'start_date'       => 'Start date',
    'start_time'       => 'Start time',
    'start_date_time'  => 'Start Date & Time',
    'date_time'        => 'Date & Time',
    'end_date'         => 'End date',
    'date'             => 'Date',
    'memo'             => 'Memo',
    'active'           => 'Active',
    'status'           => 'Status',
    'email'            => 'Email',
    'local'            => 'Local',
    'attended'         => 'Attended',
    'notes'            => 'Notes',
    'id'               => 'ID',
    'phone'            => 'Phone',
    'alt_phone'        => 'Alt Phone',
    'state'            => 'State',
    'country'          => 'Country',
    'county'           => 'County',
    'city'             => 'City',
    'zipcode'          => 'Zipcode',
    'address'          => 'Address',
    'area'             => 'Area',
    'region'           => 'Region',
    'first_name'       => 'First Name',
    'last_name'        => 'Last Name',
    'fax'              => 'Fax',
    'value'            => 'Value',
    'lat'              => 'Latitude',
    'lng'              => 'Longitude',
    'name'             => 'Name',
    'license'          => 'License',
    'website'          => 'Website',
    'ein_tax_id'       => 'EIN (Tax ID No)',
    'is_union'         => 'Is Union',
    'union'            => 'Union',
    'state_for_export' => 'State&nbsp;',
	'slug'             => 'Slug',
];