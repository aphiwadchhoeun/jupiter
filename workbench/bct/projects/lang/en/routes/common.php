<?php

return [

	'start'             => 'Start Location',
	'end'               => 'End Location',
	'finish'            => 'Add Location',
	'optimizing'        => 'Optimize Route',
	'build_route'       => 'Build Route',
	'showZip'        	=> 'Zip Codes',
	'showCounty'        => 'County',
	'showRegion'        => 'Regions',
	'details'        	=> 'Route Details',
	'default'        	=> 'None',
];
