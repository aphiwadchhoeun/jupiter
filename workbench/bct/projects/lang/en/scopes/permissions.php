<?php

return [

    'index'   		=> 'List Scopes',
    'create'  		=> 'Create new Scope',
    'edit'    		=> 'Edit Scope',
    'delete'  		=> 'Delete Scope',
	'access_error' 	=> 'You don\'t have access to this scope.'
];
