<?php

return [

	// Success messages
	'success'          => [
		'create'  => 'Scope was successfully created.',
		'update'  => 'Scope was successfully updated.',
		'delete'  => 'Scope was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the scope. Please try again.',
		'update' => 'There was an issue updating the scope. Please try again.',
		'delete' => 'There was an issue deleting the scope. Please try again.',
	],

];