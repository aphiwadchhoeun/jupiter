<?php

return [

    'general' => [

        'id'             => 'Id',
        'name'           => 'Name',
        'value'          => 'Value',
        'classification' => 'Classification',
        'created_at'     => 'Created',
        'name_help'      => 'Enter The Name',
        'value_help'     => 'Enter The Value',
        'order'          => 'Order',
        'order_help'     => 'Enter the Order'
    ],

];
