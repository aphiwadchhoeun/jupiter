<?php
/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 1/30/2017
 * Time: 11:08 AM
 */

return [

    'dashboard'                    => 'Read-only Dashboard Section',
    'users'                        => 'Read-only Users Section',
    'users.details'                => 'Read-only Users / Details Section',
    'groups'                       => 'Read-only Groups Section',
    'groups.details'               => 'Read-only Groups / Details Section',
    'contacts'                     => 'Read-only Contacts Section',
    'contractors'                  => 'Read-only Contractors Section',
    'contractors.details'          => 'Read-only Contractors / Details Section',
    'contractors.address'          => 'Read-only Contractors / Address Section',
    'contractors.phone'            => 'Read-only Contractors / Phone Section',
    'contractors.email'            => 'Read-only Contractors / Email Section',
    'contractors.company'          => 'Read-only Contractors / Company Section',
    'contractors.license'          => 'Read-only Contractors / License Section',
    'contractors.notes'            => 'Read-only Contractors / Notes Section',
    'projects'                     => 'Read-only Projects Section',
    'projects.details'             => 'Read-only Projects / Details Section',
    'projects.details.actions'     => 'Read-only Projects / Details / Actions Section',
    'projects.details.contacts'    => 'Read-only Projects / Details / Contacts Section',
    'projects.details.contractors' => 'Read-only Projects / Details / Contractors Section',
    'projects.details.visits'      => 'Read-only Projects / Details / Visits Section',
    'projects.details.files'       => 'Read-only Projects / Details / Files Section',
    'projects.details.lists'       => 'Read-only Projects / Details / Lists Section',
    'projects.details.members'     => 'Read-only Projects / Details / Members Section',
    'lists'                        => 'Read-only Lists Section',
    'lists.details'                => 'Read-only Lists / Details Section',
    'contact-types'                => 'Read-only Contact Types Section',
    'scopes'                       => 'Read-only Scopes Section',
    'stewards'                     => 'Read-only Stewards Section',
    'tags'                         => 'Read-only Tags Section',
    'routes'                       => 'Read-only Routes Section',
    'map'                          => 'Read-only Map Section',
    'locals'                       => 'Read-only Locals Section',
    'reviews'                      => 'Read-only Reviews Section',
    'reports'                      => 'Read-only Reports Section',
    'media'                        => 'Read-only Media',

];