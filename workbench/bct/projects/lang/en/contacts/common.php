<?php

return [

    'title'       => 'Contacts',
    'settings'    => 'Settings',
    'merge_title' => 'Merge Contacts',
    'create'      => 'Create Contact',
    'merge_text'  => 'These 2 contacts will be merged. Select the information you want to keep',
    'available'   => 'Available Contacts',

];
