<?php

return [

    // General messages
    'not_found' => 'Contact [:id] does not exist.',

    // Success messages
    'success'   => [
        'create' => 'Contact was successfully created.',
        'update' => 'Contact was successfully updated.',
        'delete' => 'Contact was successfully deleted.',
        'merge'  => 'Contact was successfully merged.'
    ],

    // Error messages
    'error'     => [
        'create' => 'There was an issue creating the contact. Please try again.',
        'update' => 'There was an issue updating the contact. Please try again.',
        'delete' => 'There was an issue deleting the contact. Please try again.',
        'merge'  => 'There was an issue merging the contact. Please try again.'
    ],

];