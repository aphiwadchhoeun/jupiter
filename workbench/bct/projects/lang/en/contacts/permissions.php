<?php

return [

    'index'   		=> 'List Contacts',
    'create'  		=> 'Create new Contact',
    'edit'    		=> 'Edit Contact',
    'delete'  		=> 'Delete Contact',
	'access_error' 	=> 'You don\'t have access to this area.'
];
