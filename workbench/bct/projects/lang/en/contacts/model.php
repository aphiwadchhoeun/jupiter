<?php

return [

    'general' => [

        'id'              => 'Id',
        'created_at'      => 'Created',
        'first_name'      => 'First Name',
        'name'            => 'Name',
        'last_name'       => 'Last Name',
        'contacts'        => 'Contacts',
        'company'         => 'Company',
        'company_info'    => 'Company Info',
        'location'        => 'Location',
        'title'           => 'Title',
        'address'         => 'Address',
        'city'            => 'City',
        'state'           => 'State',
        'zip'             => 'Zip',
        'phone'           => 'Phone',
        'fax'             => 'Fax',
        'email'           => 'Email',
        'phone2'          => 'Alt Phone',
        'notes'           => 'Notes',
        'first_name_help' => 'Enter the First Name here',
        'last_name_help'  => 'Enter the Last Name here',
        'company_help'    => 'Enter the Company here',
        'title_help'      => 'Enter the Title here',
        'address_help'    => 'Enter the Address here',
        'city_help'       => 'Enter the City here',
        'state_help'      => 'Enter the State here',
        'zip_help'        => 'Enter the Zip here',
        'phone_help'      => 'Enter the Phone here',
        'fax_help'        => 'Enter the Fax here',
        'email_help'      => 'Enter the Email here',
        'phone2_help'     => 'Enter the Additional Phone here',
        'notes_help'      => 'Enter the Notes here',

        'projects_belong' => 'Projects that associated with this contact',

    ],

];
