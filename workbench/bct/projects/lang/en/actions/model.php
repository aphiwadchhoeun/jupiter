<?php

return [

    'general' => [

        'id'            => 'Id',
        'created_at'    => 'Created',
        'user'          => 'User',
        'details'       => 'Details',
        'action_type'   => 'Type',
        'contractor_id' => 'Contractor',
        'date'          => 'Date',

        'details_help'       => 'Enter The Details',
        'contractor_id_help' => 'Select Contractor',
        'action_type_help'   => 'Enter The Type',
        'date_help'          => 'Enter Action Date,'
    ],

];
