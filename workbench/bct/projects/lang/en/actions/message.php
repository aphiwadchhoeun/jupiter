<?php

return [

	// General messages
	'not_found'        => 'Action [:id] does not exist.',

	// Success messages
	'success'          => [
		'create'  => 'Action was successfully created.',
		'update'  => 'Action was successfully updated.',
		'delete'  => 'Action was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the Action. Please try again.',
		'update' => 'There was an issue updating the Action. Please try again.',
		'delete' => 'There was an issue deleting the Action. Please try again.',
	],

];