<?php

return [

    'title'  => 'Groups',
    'prompt' => 'Select Group',

    'users_in_group'        => 'Users In Selected Group',
    'users_not_in_group'    => 'Users Not In Selected Group',
    'add_all_to_group'      => 'Add Selected To Group',
    'remove_all_from_group' => 'Remove Selected From Group',
    'add_to_group'          => 'Add To Group',
    'remove_from_group'     => 'Remove From Group',
    'find_reps_tab'         => 'Please find the Rep you would like to add and click the PLUS icon.',
    'edit_reps'             => 'Edit reps',
    'user_groups'           => 'User Groups',
    'available_groups'      => 'Available Groups',

    'tabs' => [
        'details' => 'Details',
        'reps'    => 'Reps',
    ],
];
