<?php

return [

    'general' => [

        'id'                     => 'Id',
        'name'                   => 'Name',
        'status'                 => 'Status',
        'description'            => 'Description',
        'type'                   => 'Type',
        'name_help'              => 'Enter the Name here',
        'status_help'            => 'Select the Status',
        'description_help'       => 'Enter the Description here',
        'group_type_help'        => 'Select the Group type',
        'active'                 => 'Active',
		'inactive'               => 'Disabled',
	    'individual'             => 'Individual',
	    'group'                  => 'Group'
        
    ],

];
