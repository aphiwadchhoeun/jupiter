<?php

return [

	// Success messages
	'success'          => [
		'create'  => 'Group was successfully created.',
		'update'  => 'Group was successfully updated.',
		'delete'  => 'Group was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the group. Please try again.',
		'update' => 'There was an issue updating the group. Please try again.',
		'delete' => 'There was an issue deleting the group. Please try again.',
		'attach' => 'There was an issue attaching the users. Please try again.',
	],

];