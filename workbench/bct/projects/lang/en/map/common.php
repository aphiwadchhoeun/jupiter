<?php

return [

    'start'                 => 'Start Location',
    'finish'                => 'Add Location',
    'optimizing'            => 'Optimize Route',
    'showZip'               => 'Zip Codes',
    'showCounty'            => 'County',
    'showRegion'            => 'Regions',
    'details'               => 'Route Details',
    'default'               => 'None',
    'select_radius'         => 'Select radius (miles)',
    'select_count_projects' => 'Select count Projects',
    'radius'                => 'Radius',
    'projects_number'       => 'Projects Number',
];
