<?php

return [

    'index'       => 'List Projects',
    'create'      => 'Create new Project',
    'edit'        => 'Edit Project',
    'delete'      => 'Delete Project',
    'actions'     => 'Manage Project Actions',
    'contacts'    => 'Manage Project Contacts',
    'contractors' => 'Manage Project Contractors',
    'visits'      => 'Manage Project Visits',
    'files'       => 'Manage Project Files',
    'lists'       => 'Manage Project Lists',
    'members'     => 'Manage Project Members',
    'stewards'    => 'Manage Project Stewards',

];
