<?php

return [

    'create_instruction_step1' => 'Step1 - Specify project address or drop a pin on the map and click search icon',
    'create_instruction_step2' => 'Step2 - Select an existing project or click the plus icon to create a new project using the specified address above',

    // General messages
    'not_found'          => 'Project [:id] does not exist.',
    'classification_change'=> 'Before change the classification you must remove contractors first.',

    // Success messages
    'success'            => [
        'create' => 'Project was successfully created.',
        'update' => 'Project was successfully updated.',
        'delete' => 'Project was successfully deleted.',
        'upload' => 'Project File was successfully uploaded.',
	    'merge'  => 'Projects were merged successfully.'
    ],

    // Error messages
    'error'              => [
        'create' => 'There was an issue creating the project. Please try again.',
        'update' => 'There was an issue updating the project. Please try again.',
        'delete' => 'There was an issue deleting the project. Please try again.',
        'upload' => 'There was an issue uploading the project file. Please try again.',
    ],

];