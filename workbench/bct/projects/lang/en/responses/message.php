<?php

return [

    'success' => [
        'submit' => 'Your bid response was successfully submitted.',
    ],

    'error'   => [
        'invalid_status' => 'Sorry, at the current time, the request you\'re trying to process has invalid status',
        'invalid_option' => 'Sorry, we detected an invalid bid response option during the process. Please try again.',
    ]

];
