<?php

return [

    'title'     => 'Bid Response',
    'lost_to'   => 'Who did you lost to?',
    'extension' => 'Why do you need an extension (30 days)?',
    'status'    => 'Response status',
    'hasSubs'   => 'Will you use sub-contractors?',
    'subs'      => 'Sub Contractors',

    'options'   => [
        'lost_bid'  => 'Lost bid',
        'extension' => 'Request 30 days extension',
        'awarded'   => 'Awarded',
    ]

];
