<?php

return [

    'index'   		=> 'List Contact Types',
    'create'  		=> 'Create new Contact Type',
    'edit'    		=> 'Edit Contact Type',
    'delete'  		=> 'Delete Contact Type',
	'access_error' 	=> 'You don\'t have access to this contact type.'
];
