<?php

return [

	'general' => [

		'id' 			=> 'Id',
		'name' 			=> 'Name',
		'created_at' 	=> 'Created',
		'name_help' 	=> 'Enter The Name',
	],

];
