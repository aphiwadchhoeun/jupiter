<?php

return [

	// Success messages
	'success'          => [
		'create'  => 'Contact Type was successfully created.',
		'update'  => 'Contact Type was successfully updated.',
		'delete'  => 'Contact Type was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the contact type. Please try again.',
		'update' => 'There was an issue updating the contact type. Please try again.',
		'delete' => 'There was an issue deleting the contact type. Please try again.',
	],

];