<?php

return [

    'title'             => 'Contact Types',
    'create'            => 'Contact Type was successfully created!',
    'update'            => 'Contact Type was successfully updated!',
    'delete'            => 'Contact Type was successfully deleted!',

];
