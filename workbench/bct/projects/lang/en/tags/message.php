<?php
return [

'not_found'  => 'Tag #:id not found',
'no_access'  => 'No access to Tag #:id',

'success' => [
	'create' => 'Tag Record successfully created',
	'update' => 'Tag Record successfully updated',
	'delete' => 'Tag Record successfully deleted'
],
'error' =>[
	'delete' => 'Something went wrong while deleting Tag record'
]
];