<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 16.02.16
 * Time: 14:23
 */

return [
    'general' => [
        'approved'     => 'Approved',
        'not_approved' => 'Not Approved',
    ]
];