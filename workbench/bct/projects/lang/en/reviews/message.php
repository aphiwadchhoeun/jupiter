<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 16.02.16
 * Time: 15:18
 */


return [


    // General messages
    'not_found'          => 'Review [:id] does not exist.',
    'classification_change'=> 'Before change the classification you must remove contractors first.',

    // Success messages
    'success'            => [
        'create' => 'Review was successfully created.',
        'update' => 'Review was successfully updated.',
        'delete' => 'Review was successfully deleted.',
        'approve' => 'Review was successfully approved.',
    ],

    // Error messages
    'error'              => [
        'create' => 'There was an issue creating review. Please try again.',
        'update' => 'There was an issue updating review. Please try again.',
        'delete' => 'There was an issue deleting review. Please try again.',
        'approve' => 'There was an issue approving review. Please try again.',
    ],

];