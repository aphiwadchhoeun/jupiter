<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 16.02.16
 * Time: 14:45
 */

return [
    'title'              => 'Reviews',
    'approve_selected'   => 'Approve selected items',
    'unapprove_selected' => 'Unapprove selected items'
];