<?php

return [

    'title'             => 'Action Types',
    'create'            => 'Action Type was successfully created!',
    'update'            => 'Action Type was successfully updated!',
    'delete'            => 'Action Type was successfully deleted!',

];
