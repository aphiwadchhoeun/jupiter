<?php

return [

	// Success messages
	'success'          => [
		'create'  => 'Action Type was successfully created.',
		'update'  => 'Action Type was successfully updated.',
		'delete'  => 'Action Type was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the action type. Please try again.',
		'update' => 'There was an issue updating the action type. Please try again.',
		'delete' => 'There was an issue deleting the action type. Please try again.',
	],

];