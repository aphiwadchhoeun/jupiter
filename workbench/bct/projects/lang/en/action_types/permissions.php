<?php

return [

    'index'   		=> 'List Action Types',
    'create'  		=> 'Create new Action Type',
    'edit'    		=> 'Edit Action Type',
    'delete'  		=> 'Delete Action Type',
	'access_error' 	=> 'You don\'t have access to this action type.'
];
