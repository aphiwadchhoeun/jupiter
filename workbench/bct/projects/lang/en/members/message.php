<?php
/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 14:18
 */

return [

    // General messages
    'not_found' => 'Member [:id] does not exist.',

    // Success messages
    'success'   => [
        'create' => 'Member was successfully created.',
        'update' => 'Member was successfully updated.',
        'delete' => 'Member was successfully deleted.',
    ],

    // Error messages
    'error'     => [
        'create' => 'There was an issue creating the Member. Please try again.',
        'update' => 'There was an issue updating the Member. Please try again.',
        'delete' => 'There was an issue deleting the Member. Please try again.',
    ],

];