<?php
/**
 * Created by PhpStorm.
 * User: sky
 * Date: 30.11.16
 * Time: 14:46
 */

return [

    'general' => [

        'id'           => 'Id',
        'name'         => 'Name',
        'phone'        => 'Phone',
        'scope'        => 'Scope',
        'number_hours' => 'Number of Hours',
        'wage'         => 'Wage',
        'member_since' => 'Member Since',
        'email'        => 'Email',

    ],

];
