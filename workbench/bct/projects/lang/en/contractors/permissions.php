<?php

return [

    'index'        => 'List Contractors',
    'create'       => 'Create new Contractor',
    'edit'         => 'Edit Contractor',
    'delete'       => 'Delete Contractor',
    'access_error' => 'You don\'t have access to this area.',
    'files'        => 'Manage Contractor Filess',
    'address'      => 'Manage Contractor Addresses',
    'phone'        => 'Manage Contractor Phones',
    'email'        => 'Manage Contractor Emails',
    'company'      => 'Manage Contractor Companies',
    'license'      => 'Manage Contractor Licenses',
];
