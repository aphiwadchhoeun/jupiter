<?php

return [
    'not_found' => 'Contractor [:id] does not exist.',

    // Success messages
    'success'   => [
        'create' => 'Contractor was successfully created.',
        'update' => 'Contractor was successfully updated.',
        'delete' => 'Contractor was successfully deleted.',
        'merge'  => 'Contractors were merged successfully.'
    ],

    // Error messages
    'error'     => [
        'create'    => 'There was an issue creating the contractor. Please try again.',
        'update'    => 'There was an issue updating the contractor. Please try again.',
        'delete'    => 'There was an issue deleting the contractor. Please try again.',
        'not_found' => 'Contractors do not exist.'
    ],

    'address' => [
        'success' => [
            'create' => 'Contractor address successfully created',
            'update' => 'Contractor address successfully updated',
            'delete' => 'Contractor address successfully deleted',
        ]
    ],
    'phone'   => [
        'success' => [
            'create' => 'Contractor phone successfully created',
            'update' => 'Contractor phone successfully updated',
            'delete' => 'Contractor phone successfully deleted',
        ]
    ],
    'email'   => [
        'success' => [
            'create' => 'Contractor email successfully created',
            'update' => 'Contractor email successfully updated',
            'delete' => 'Contractor email successfully deleted',
        ]
    ],
    'license' => [
        'success' => [
            'create' => 'Contractor license successfully created',
            'update' => 'Contractor license successfully updated',
            'delete' => 'Contractor license successfully deleted',
        ]
    ],
    'company' => [
        'success' => [
            'create' => 'Contractor company successfully created',
            'update' => 'Contractor company successfully updated',
            'delete' => 'Contractor company successfully deleted',
        ]
    ],
    'notes'   => [
        'success' => [
            'create' => 'Employer notes successfully created',
            'update' => 'Employer notes successfully updated',
            'delete' => 'Employer notes successfully deleted',
        ]
    ],

];