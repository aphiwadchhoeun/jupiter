<?php

return [

    'title'                 => 'Contractors',
    'available_contractors' => 'Available Contractors',
    'settings'              => 'Settings',
    'union'                 => 'Union',
    'pa'                    => 'Project Agreement',
    'nonunion'              => 'Non-Union',
    'scope'                 => 'Scope',
    'all_scopes'            => 'All Scopes',
    'create_contractor'     => 'Create Contractor',
    'create_na'             => 'Create N/A',
    'add_na'                => 'Add N/A',
    'merged_contractor'     => 'Merged contractor:',
    'merge_result'          => 'Result after merge:',
    'removing_contractor'   => 'Removing Contractor',
    'target_contractor'     => 'Result Contractor'

];
