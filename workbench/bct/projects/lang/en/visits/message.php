<?php

return [

	// General messages
	'not_found'        => 'Visit [:id] does not exist.',

	// Success messages
	'success'          => [
		'create'  => 'Visit was successfully created.',
		'update'  => 'Visit was successfully updated.',
		'delete'  => 'Visit was successfully deleted.',
	],

	// Error messages
	'error'            => [
		'create' => 'There was an issue creating the Visit. Please try again.',
		'update' => 'There was an issue updating the Visit. Please try again.',
		'delete' => 'There was an issue deleting the Visit. Please try again.',
	],

];