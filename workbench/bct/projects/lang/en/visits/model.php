<?php

return [

    'general' => [

        'id'            => 'Id',
        'created_at'    => 'Created',
        'users'         => 'Users',
        'users_new'     => 'Additional reps visiting',
        'project'       => 'Project',
        'details'       => 'Details',
        'stage'         => 'Stage',
        'stages'        => 'Stages',
        'lists'         => 'Lists',
        'lists_new'     => 'Mark project visit complete for the folowing list(s)',
        'rep'           => 'Rep',
        'notes'         => 'Notes',
        'date'          => 'Date',
        'reps'          => 'Reps',
        'approved_user' => 'User that approve',
        'approved_by'   => 'Approved by',
        'weather'       => 'Weather',
        'workers_count' => 'Workers',

        'details_help'     => 'Enter The Details',
        'stages_help'      => 'Select Stages',
        'users_help'       => 'Select Users',
        'lists_help'       => 'Select Lists',
        'description_help' => 'Enter The Description',
        'visit_date_time'  => 'Visit Date & Time',
        'visit_date'       => 'Visit Date',
        'visit_time'       => 'Visit Time',

    ],

];
