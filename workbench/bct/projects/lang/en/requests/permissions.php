<?php

return [

    'index'  => 'List Requests',
    'create' => 'Create new Request',
    'edit'   => 'Edit Request',
    'delete' => 'Delete Request',
    'manage' => 'Manage Request',

];
