<?php

return [

    'general' => [

        'id'                        => 'Id',
        'party_name'                => 'Project Owner',
        'party_address'             => 'Owner Address',
        'party_city'                => 'Owner City',
        'party_state'               => 'Owner State',
        'party_zipcode'             => 'Owner Zipcode',
        'party_phone'               => 'Owner Phone',
        'estimated_start_date'      => 'Estimated Start Date',
        'duration_month'            => 'Project Duration in months',
        'bid_date'                  => 'Bid Date',
        'bid_time'                  => 'Bid Time',
        'estimate_id'               => 'Project Estimate',
        'award_basis'               => 'Basis Award',
        'contractor_type'           => 'Contractor Type',
        'requested'                 => 'Request',
        'status'                    => 'Status',
        'committed_note'            => 'Notes',
        'committed_craft'           => 'Committed Craft',
        'committed_hours'           => 'Committed Hours',
        'committed_rate'            => 'Committed Rate',
        'committed_by'              => 'Committed By',
        'committed_notes'           => 'Committed Notes',
        'committed_date'            => 'Commit Date',
        'awarded_date'              => 'Award Date',
        'loss_date'                 => 'Lose Date',
        'completed_date'            => 'Complete Date',
        'denied_date'               => 'Deny Date',
        'created_at'                => 'Entry Date',

        'party_name_help'           => 'Enter Project Owner here',
        'party_address_help'        => 'Enter Owner Address here',
        'party_city_help'           => 'Enter Owner City here',
        'party_state_help'          => 'Select Owner State here',
        'party_zipcode_help'        => 'Enter Owner Zipcode here',
        'party_phone_help'          => 'Enter Owner Phone here',
        'estimated_start_date_help' => 'Enter Estimated Start Date here',
        'bid_date_help'             => 'Enter Bid Date here',
        'bid_time_help'             => 'Enter Bid Time here',
        'estimate_id_help'          => 'Select Project Estimate here',
        'award_basis_help'          => 'Select Basis Award here',
        'contractor_type_help'      => 'Select Contractor Type here',
        'requested_help'            => 'Enter Project Request here',

    ],

    'other'   => [
        'mrid'            => 'MRID',
        'project_name'    => 'Project Name',
        'contractor_name' => 'Contractor Name',
        'response'        => 'Bid Response',
        'value'           => 'Value',
        'sub_total'       => 'Sub Total',
    ]

];
