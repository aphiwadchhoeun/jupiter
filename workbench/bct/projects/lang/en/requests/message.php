<?php

return [

    // General messages
    'fail'      => 'Could not access the form you\'re looking for ',
    'not_found' => 'Request [:id] does not exist.',

    // Success messages
    'success'   => [
        'create' => 'Request was successfully created.',
        'update' => 'Request was successfully updated.',
        'delete' => 'Request was successfully deleted.',
        'commit' => 'Request was successfully committed.',
        'submit' => 'Market Recovery Request was successfully submitted.',
    ],

    // Error messages
    'error'     => [
        'create' => 'There was an issue creating the request. Please try again.',
        'update' => 'There was an issue updating the request. Please try again.',
        'delete' => 'There was an issue deleting the request. Please try again.',
        'commit' => 'There was an issue committing the request. Please double check the form, and try again.',
    ],

];
