<?php

return [

    'title'            => 'Requests',
    'commit'           => 'Commit',
    'deny'             => 'Deny',
    'commit_info'      => 'Commit Information',
    'commit_history'   => 'Commitment History',
    'commitment'       => 'Commitment',
    'deny_modal_title' => 'Are you sure you want to deny the request?',
    'deny_notes'       => 'Please explain why the request is denied:',

];
