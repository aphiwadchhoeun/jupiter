<?php

return [

    'title'             => 'Dashboard',
    'not_visited'       => 'Active Projects Not Visited',
    'share_by_area'     => 'Market Share by Region',
    'select_scope'      => 'All Scopes',
    'select_area'       => 'All Regions',
    'select_days'       => 'Days',
    'select_all'        => 'All',
    'totals'            => 'Project Totals',
    'select_current'    => 'Current Year',
    'top_non_union'     => 'Top Non-Union Contractors',
    'top_union'         => 'Top Union Contractors',
    'contractor_name'   => 'Contractor Name',
    'contractor_value'  => 'Value',
    'days'              => 'days',
    'follow_up'         => 'Follow Up',
    'project_name'      => 'Project Name',
    'project_follow_up' => 'Follow Up Date',
    'rep_visits'        => 'Rep Visits (Latest 10)',
    'user_name'         => 'User Name',
    'visit_date'        => 'Visit Date',
    'out_of_region'     => 'OUT OF REGION',
    'year'              => 'Current Year',

];
