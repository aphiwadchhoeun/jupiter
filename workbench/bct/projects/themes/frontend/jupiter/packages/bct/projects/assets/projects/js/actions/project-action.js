var ProjectActionExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectActionExtension = Object.create(ExtensionBase);
    ProjectActionExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-action'
    });

    // Initialize functions
    ProjectActionExtension.Index.init = function () {
        ProjectActionExtension.Index.datePicker(ProjectActionExtension);
        ProjectActionExtension.Index.dataGrid();
        ProjectActionExtension.Index.listeners();
        ProjectActionExtension.Index.datePicker.on('show.daterangepicker',function(){
            showDirectionPP('Created');
        });
    };

    // Add Listeners
    ProjectActionExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', ProjectActionExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    ProjectActionExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    ProjectActionExtension.Index.exporterStatus = function (grid) {
        $('#project-actions-exporter').toggleClass('hide', grid.pagination.filtered == 0);
        $('#project-actions-exporter').parent().toggleClass('hide', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    ProjectActionExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            callback: function (data) {
                ProjectActionExtension.Index
                    .exporterStatus(data);
            }
        };

        ProjectActionExtension.Index.Grid = $.datagrid('project-action', '#project-action-data-grid', '.project-action-data-grid_pagination', '#project-action-data-grid_applied', config);
    };

    // Job done, lets run.
    ProjectActionExtension.Index.init();

})(window, document, jQuery);

$(document.body).on('click', '.actions-add-btn', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                var actionDate = form_modal.find("#action_date");

                if (actionDate) {
                    actionDate.parents('.date').datepicker({
                        format: 'M-dd-yyyy',
                        assumeNearbyYear: true,
                        showOnFocus: false
                    }).on('changeDate', function() {
                        if (actionDate.val()) {
                            submit_btn_modal.prop('disabled',false);
                            $(this).parent().removeClass('has-error');
                            $(this).parent().find('.help-block').text('');
                        }
                    }).on('focusout', function () {
                        var date = actionDate.val();
                        if (date !== '') {
                            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
                            if(valid_date===false){
                                submit_btn_modal.prop('disabled',true);
                                $(this).parent().addClass('has-error');
                                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
                            }
                            else{
                                submit_btn_modal.prop('disabled',false);
                                $(this).parent().removeClass('has-error');
                                $(this).parent().find('.help-block').text('');
                            }
                        }
                    });
                }

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectActionExtension.Index.Grid.refresh();
                                    actionsSummaryUpdate()
                                } else {
                                    swal(data);
                                }
                            })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });
            }

        }
        else {
            location.reload();
        }
    });
});

// Project Action Data Grid
$('#project-action-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-gear');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-gear');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var delete_btn_modal = $('#delete-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                var actionDate = form_modal.find("#action_date");

                if (actionDate) {
                    actionDate.parents('.date').datepicker({
                        format: 'M-dd-yyyy',
                        assumeNearbyYear: true,
                        showOnFocus: false
                    }).on('changeDate', function() {
                        if (actionDate.val()) {
                            submit_btn_modal.prop('disabled',false);
                            $(this).parent().removeClass('has-error');
                            $(this).parent().find('.help-block').text('');
                        }
                    }).on('focusout', function () {
                        var date = actionDate.val();
                        if (date !== '') {
                            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
                            if(valid_date===false){
                                submit_btn_modal.prop('disabled',true);
                                $(this).parent().addClass('has-error');
                                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
                            }
                            else{
                                submit_btn_modal.prop('disabled',false);
                                $(this).parent().removeClass('has-error');
                                $(this).parent().find('.help-block').text('');
                            }
                        }
                    });
                }

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectActionExtension.Index.Grid.refresh();
                                    actionsSummaryUpdate()
                                } else {
                                    swal(data);
                                }
                            })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });

                if (delete_btn_modal) {
                    delete_btn_modal.on('click', function () {
                            delete_btn_modal.prop('disabled', true);
                            var url = $(this).data('url');
                            $.post(url, {})
                                .done(function (data) {
                                    if (data == 'success') {
                                        $modal.modal('hide');
                                        ProjectActionExtension.Index.Grid.refresh();
                                        actionsSummaryUpdate();
                                    } else {
                                        swal('Deleting failed.');
                                    }
                                })
                                .always(function () {
                                    delete_btn_modal.removeAttr('disabled');
                                });
                        }
                    );
                }
            }

        } else {
            location.reload();
        }
    });
});