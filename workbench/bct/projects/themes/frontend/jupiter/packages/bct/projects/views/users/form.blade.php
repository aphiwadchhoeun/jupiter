@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans("action.{$mode}") }}} {{ trans('bct/projects::users/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}
    {{ Asset::queue('user-form', 'bct/projects::users/js/form.js', 'jquery') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default">

        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip"
                                       @if ($mode === 'create')
                                            href="{{ route('bct.projects.users.all') }}"
                                       @else
                                            href="{{ route('bct.projects.users.link', $user->id) }}"
                                       @endif

                                       data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{{ trans("bct/projects::users/common.title") }}} {{{ trans("action.{$mode}") }}}</span>
                        </div>

                        <div class="pull-right">
                                <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                        data-original-title="{{ trans('action.save') }}">
                                    <i class="fa fa-save fa-fw"></i></button>
                        </div>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <div class="col-md-12">

                    {{-- Form: General --}}
                    <fieldset>
                        <div class="row">

                            {{--add more form-group for other fields, just keep the same uniform--}}
                            <div class="form-group{{ Alert::onForm('first_name', ' has-error') }}">

                                <label for="first_name" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.first_name') }}}
                                </label>

                                <input type="text" class="form-control" name="first_name" id="first_name"
                                       placeholder="{{{ trans('bct/projects::users/model.general.first_name_help') }}}"
                                       value="{{{ input()->old('first_name', $user->first_name) }}}">

                                <span class="help-block">{{{ Alert::onForm('first_name') }}}</span>

                            </div>

                            <div class="form-group{{ Alert::onForm('last_name', ' has-error') }}">

                                <label for="last_name" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.last_name') }}}
                                </label>

                                <input type="text" class="form-control" name="last_name" id="last_name"
                                       placeholder="{{{ trans('bct/projects::users/model.general.last_name_help') }}}"
                                       value="{{{ input()->old('last_name', $user->last_name) }}}">

                                <span class="help-block">{{{ Alert::onForm('last_name') }}}</span>

                            </div>

                            <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                                <label for="email" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.email') }}}
                                </label>

                                <input type="text" class="form-control" name="email" id="email"
                                       placeholder="{{{ trans('bct/projects::users/model.general.email_help') }}}"
                                       value="{{{ input()->old('email', $user->email) }}}">

                                <span class="help-block">{{{ Alert::onForm('email') }}}</span>

                            </div>

                            <div class="form-group{{ Alert::onForm('role', ' has-error') }}">

                                <label for="role" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.role') }}}
                                </label>
                                <select class="form-control" name="role" id="role">
                                    <option value="">{{{ trans('bct/projects::users/model.general.role_help') }}}</option>
                                    @foreach($user_roles as $role)
                                        <option value="{{ $role->id }}"
                                                @if(input()->old('role') === $role->id
                                                ||
                                                ($mode === 'edit' && $user->roles->contains($role->id)))
                                                selected
                                                @endif

                                        >{{ $role->name }}</option>
                                    @endforeach
                                </select>

                                <span class="help-block">{{{ Alert::onForm('role') }}}</span>

                            </div>


                            <div class="form-group{{ Alert::onForm('phone', ' has-error') }}">

                                <label for="phone" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.phone') }}}
                                </label>

                                <input type="text" class="form-control" name="phone" id="phone"
                                       placeholder="{{{ trans('bct/projects::users/model.general.phone_help') }}}"
                                       value="@if ($user->profile){{{ $user->profile->business_phone }}} @else {{ input()->old('phone') }} @endif">

                                <span class="help-block">{{{ Alert::onForm('phone') }}}</span>

                            </div>
                            <div class="row col-md-3">
                                <div class="form-group">

                                    <label for="activated" class="row col-md-12 control-label">
                                        {{{ trans('bct/projects::general/common.status') }}}
                                    </label>

                                    <input type="checkbox" name="activated" id="status"
                                           @if ($user->activated || input()->old('activated'))
                                           checked
                                            @endif
                                    />

                                    <span class="help-block">{{{ Alert::onForm('activated') }}}</span>

                                </div>
                            </div>
                            @if($mode == 'create')
                                <div class="row col-md-3">
                                    <div class="form-group">

                                        <label for="individual_group" class="control-label w100">
                                            {{{ trans('bct/projects::users/model.general.create_group') }}}
                                        </label>

                                        <input type="checkbox" name="individual_group" id="individual_group" value="0">

                                    </div>
                                </div>
                            @endif

                        </div>

                    </fieldset>

                </div>

            </div>

        </form>

    </section>
@stop
