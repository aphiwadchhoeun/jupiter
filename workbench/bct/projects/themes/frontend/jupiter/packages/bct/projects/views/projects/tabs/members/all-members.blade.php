{{ Asset::queue('all-members-grid', 'bct/projects::projects/js/members/members.js', ['extension-base', 'bootstrap-typeahead', 'select2-jquery']) }}

<section class="panel panel-default panel-grid">
    {{-- Grid --}}

    {{-- Grid: Header --}}
    <header class="panel-heading">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-members-all-action">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{{ trans('bct/projects::members/common.available') }}}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-members-all-action">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-members-all-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="members" role="form">

                        <div class="input-group">

                            <input class="form-control" name="filter" type="text" id="project-available-members-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="project-available-members-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="members" data-reset id="project-available-members-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                        </div>

                    </form>

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="members-data-grid-applied-filters">

            <div id="members-data-grid_applied" class="btn-group" data-grid="members"></div>

        </div>


        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="members-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridAllMembers', $project->id) }}"
                   data-filename="{{ $project->name }} {{ trans('bct/projects::members/common.available') }}"
                   data-grid="members">
                <thead>
                <tr>
                    <th class="sortable col-md-1"
                        data-sort="member_number">{{ trans('bct/projects::members/model.general.id') }}</th>
                    <th class="sortable col-md-3"
                        data-sort="member_name">{{ trans('bct/projects::members/model.general.name') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="phone">{{ trans('bct/projects::members/model.general.phone') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="member_since">{{ trans('bct/projects::members/model.general.member_since') }}</th>
                    <th class="col-md-1">{{{ trans('bct/projects::general/common.actions') }}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>


    <footer class="panel-footer clearfix">

        {{-- Grid: Pagination --}}
        <div class="members-data-grid_pagination" data-grid="members"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.members.grid.members.results')
    @include('bct/projects::projects.tabs.members.grid.members.pagination')
    @include('bct/projects::projects.tabs.members.grid.members.filters')
    @include('bct/projects::projects.tabs.members.grid.members.no_results')

</section>