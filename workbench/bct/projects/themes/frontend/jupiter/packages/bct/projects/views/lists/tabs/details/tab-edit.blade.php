<header class="panel-heading">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#actions">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip go-to-details" data-toggle="tooltip"
                           data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $list->name }} {{ trans("bct/projects::general/common.details") }}</span>
            </div>

            <div class="pull-right">
                @if ($has_access)
                    <button id="delete-btn" class="btn btn-danger" data-toggle="tooltip"
                            name="delete-btn"
                            data-original-title="{{{ trans('action.delete') }}}">
                        <i class="fa fa-trash-o fa-fw"></i>
                    </button>
                @endif

                <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                        data-original-title="{{{ trans('action.save') }}}">
                    <i class="fa fa-save fa-fw"></i>
                </button>
            </div>
        </div>

    </nav>

</header>
<div class="panel-body">

    <section class="panel panel-default panel-tabs">

        <form id="content-form" action="
              @if ($list->exists)
        {{ route('bct.projects.lists.edit', $list->id) }}
        @else
        {{ route('bct.projects.lists.create') }}
        @endif
                " method="post" data-refresh="lists" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="panel-body">

                <div class="col-md-12">

                    {{-- Form: General --}}
                    <fieldset>
                        <div class="row">

                            {{-- Form:name --}}
                            <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                                <label for="name" class="control-label">
                                    {{{ trans('bct/projects::lists/model.general.name') }}}
                                </label>

                                <input type="text" class="form-control" name="name" id="name" required
                                       placeholder="{{{ trans('bct/projects::lists/model.general.name_help') }}}"
                                       value="{{{ input()->old('name', $list->name) }}}">

                                <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                            </div>

                            {{-- Form:group_id --}}
                            <div class="form-group{{ Alert::onForm('z_projects_group_id', ' has-error') }}">

                                <label for="z_projects_group_id" class="control-label">
                                    {{{ trans('bct/projects::lists/model.general.group_id')}}}
                                </label>

                                <select class="form-control" name="z_projects_group_id" id="group_id" required>
                                    <option value="">{{{ trans('bct/projects::lists/model.general.group_id_help') }}}
                                    </option>
                                    @foreach($groups as $group)
                                        <option value="{{ $group->id }}"
                                                @if(input()->old('z_projects_group_id', $list->z_projects_group_id) == $group->id) selected @endif
                                                >{{ $group->name }}</option>
                                    @endforeach
                                </select>

                                <span class="help-block">{{{ Alert::onForm('z_projects_group_id') }}}</span>

                            </div>


                            @if($change_status)
                                <div class="form-group">

                                    <label for="status" class="control-label">
                                        {{{ trans('bct/projects::lists/model.general.status')}}}
                                    </label>

                                    <select class="form-control" name="status" id="status" required>
                                        <option value="0" selected>{{ trans('bct/projects::general/common.disabled') }}
                                        </option>
                                        <option value="1">{{{ trans('bct/projects::general/common.active') }}}</option>
                                    </select>


                                </div>
                            @else
                                <input type="hidden" name="status" id="status" value="1">
                            @endif


                            <div class="form-group">

                                <label for="due_date" class="control-label">
                                    {{{ trans('bct/projects::lists/model.general.due_date')}}}
                                </label>

                                <div class='input-group date'>
                                    <input type="text" class="form-control" name="due_date" id="due_date"
                                           placeholder="{{{ trans('bct/projects::lists/model.general.due_date_help') }}}"
                                           required data-parsley-date
                                           value="{{ $list->due_date->format('M-d-Y') }}">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>

                                <span class="help-block">{{{ Alert::onForm('due_date') }}}</span>

                            </div>


                            {{-- Form:category --}}
                            <div class="form-group{{ Alert::onForm('category', ' has-error') }}">

                                <label for="category" class="control-label">
                                    {{{ trans('bct/projects::lists/model.general.category') }}}
                                </label>

                                <input type="text" class="form-control" name="category" id="category"
                                       placeholder="{{{ trans('bct/projects::lists/model.general.category_help') }}}"
                                       value="{{{ input()->old('category', $list->category) }}}">

                                <span class="help-block">{{{ Alert::onForm('category') }}}</span>

                            </div>


                            {{-- Form:description --}}
                            <div class="form-group{{ Alert::onForm('description', ' has-error') }}">

                                <label for="description" class="control-label">
                                    {{{ trans('bct/projects::lists/model.general.description') }}}
                                </label>

                                <textarea class="form-control" name="description" rows="4"
                                          placeholder="{{{ trans('bct/projects::lists/model.general.description_help') }}}"
                                        >{{{ input()->old('description', $list->description) }}}</textarea>

                                <span class="help-block">{{{ Alert::onForm('description') }}}</span>

                            </div>


                        </div>

                    </fieldset>

                </div>

            </div>

        </form>

    </section>

</div>