/**
 * Created by Aphiwad on 4/17/2015.
 */
jQuery('document').ready(function ($) {
    var submit_btn = $('#submit-btn');
    var main_form = $('#content-form');
    var activeControl = $('#active');


    $('input[name="phone"]').mask('(999) 999-9999? x999');
    $('input[name="phone2"]').mask('(999) 999-9999? x999');
    $('input[name="fax"]').mask('(999) 999-9999');
    $('input[name="zip"]').mask('99999?-9999');
    submit_btn.click(function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            main_form.submit();
        });
    });

    activeControl.bootstrapToggle({
        onstyle: 'success',
        offstyle: 'danger',
        on: 'Yes',
        off: 'No'
    });

    $('#state').on('keypress', function (event) {
        if (null !== String.fromCharCode(event.which).match(/[a-z]/g)) {
            event.preventDefault();
            if ($(this).val().length < 2) {
                $(this).val($(this).val() + String.fromCharCode(event.which).toUpperCase());
            }
        }
    });
});