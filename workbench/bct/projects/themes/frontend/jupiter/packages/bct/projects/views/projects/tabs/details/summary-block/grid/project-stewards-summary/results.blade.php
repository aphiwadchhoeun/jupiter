<script type="text/template" data-grid="project-stewards-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row class="redirect-to" data-url="<%- r.view_uri%>">
        <td><%- r.full_name %></td>
        <td><%- r.local %></td>
        <td><%- r.phone %></td>
    </tr>

    <% }); %>

</script>
