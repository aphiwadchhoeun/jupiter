<div id="member_file_upload_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >{{{ trans('bct/member::member/common.add_file') }}}</h4>
            </div>

            <div class="modal-body">

                <form id="fileupload" class="fileupload-processing" action="{{ route('bct.projects.projects.files.upload', $project->id) }}" data-action-get="{{ route('bct.projects.projects.files.get', $project->id) }}" method="POST" enctype="multipart/form-data" data-file_id="">
                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                    <div class="row fileupload-buttonbar">
                        <!-- The global progress state -->
                        <div class="col-md-2">
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <div class="btn btn-primary fileinput-button">
                                <input type="file" title="Add file" name="files[]">
                                {{ trans('bct/projects::projects/common.upload') }}
                            </div>

                        </div>
                        <div class="col-lg-10 fileupload-progress fade">
                            <!-- The global progress bar -->
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                <div class="progress-bar progress-bar-success w0"></div>
                            </div>
                            <!-- The extended global progress state -->
                            <div class="progress-extended">&nbsp;</div>
                        </div>

                    </div>
                    <div class="row hide filename-edit">
                        <div class="form-group col-md-12">
                            <label>Filename</label>
                            <input id="filename" placeholder="Enter filename" class="form-control" name="document_name" value="" class="form-control w100" data-parsley-required="true" type="text">
                        </div>
                    </div>
                    <div class="row hide filename-edit">
                        <div class="form-group col-md-12">
                            <label>Description</label>
                            <input id="fileDescription" placeholder="Enter file description" class="form-control" name="document_description" value="" class="form-control w100" data-parsley-required="true" type="text">
                        </div>
                    </div>

                </form>

            </div>

            <div class="modal-footer">
                <button id="project-file-submit-btn-modal" class="btn btn-primary ml10 btn-prevent-double-click">
                    {{{ trans('action.save') }}}</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
            </div>

        </div>

    </div>

</div>
