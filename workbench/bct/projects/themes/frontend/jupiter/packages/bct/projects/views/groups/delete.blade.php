<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="gridSystemModalLabel">{{ $group->name }}</h4>
</div>

<div class="modal-body text-center">

    <i class="fa fa-warning fa-5x"></i>

    <h4>{{{ trans('common.danger') }}}</h4>

    <p class="lead">
        {{{ trans('message.delete_record') }}}
        @if($group->users)
            <br>Users:<br>
            @forelse($group->users as $user)
                {{ $user->first_name }} {{ $user->last_name }} <br>
            @empty
                (No users)
            @endforelse
        @endif
    </p>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
    <a href="{{ route('bct.projects.groups.delete', $group->id) }}" class="btn btn-danger confirm" id="delete-button" name="delete-button">{{{ trans('action.delete') }}}</a>
</div>
