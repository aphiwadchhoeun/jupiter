<div class="modal-dialog ext-profiles">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">{{{ trans('bct/projects::projects/common.contact_' . $mode) }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="
                    @if(isset($pivot))
                        {{ route('bct.projects.projects.contact.edit', [$project->id, $contact->id, $pivot->id]) }}
                    @else
                        {{ route('bct.projects.projects.contact.add', [$project->id, $contact->id]) }}
                    @endif" data-refresh="contacts">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input type="hidden" name="mode" value="{{ $mode }}">
                <input type="hidden" name="contact_id" value="{{ $contact->id }}">
                @if(isset($pivot))
                    <input type="hidden" name="pivot_id" value="{{ $pivot->id }}">
                @endif

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>
                            <div class="row">
                                <table class="table table-bordered table-details">
                                    <tbody>
                                    <tr>
                                        <td class="td-value">
                                            <div>


                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b>{{ trans('bct/projects::contacts/model.general.name') }}:</b>
                                                            <span class="pre-spaces">{{ $contact->full_name }}</span>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <b>{{ trans('bct/projects::general/common.updated') }}:</b>
                                                            <span class="pre-spaces">{{ date_correct_details_projects($contact->updated_at) }}</span>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <b>{{ trans('bct/projects::contacts/model.general.address') }}:</b>
                                                            <span class="pre-spaces">{{ $contact->location }}</span>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <b>{{ trans('bct/projects::contacts/model.general.phone') }}:</b>
                                                            <span class="pre-spaces"><a class="under-none" href="tel:{{ $contact->phone }}">{{ $contact->phone }}</a></span>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <b>{{ trans('bct/projects::contacts/model.general.phone2') }}:</b>
                                                            <span class="pre-spaces"><a class="under-none" href="tel:{{ $contact->phone2 }}">{{ $contact->phone2 }}</a></span>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <b>{{ trans('bct/projects::contacts/model.general.company') }}:</b>
                                                            <span class="pre-spaces">{{ $contact->company }}</span>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <b>{{ trans('bct/projects::contacts/model.general.title') }}:</b>
                                                            <span class="pre-spaces">{{ $contact->title }}</span>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <b>{{ trans('bct/projects::contacts/model.general.email') }}:</b>
                                                            <span class="pre-spaces"><a class="under-none" href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></span>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <b>{{ trans('bct/projects::contacts/model.general.notes') }}:</b>
                                                            <span class="pre-spaces">{{ $contact->notes_short }}</span>
                                                        </div>
                                                    </div>

                                            </div>

                                        </td>

                                </table>
                            </div>

                            <div class="row">

                                {{--add more form-group for other fields, just keep the same uniform--}}
                                <div class="form-group{{ Alert::onForm('contact_type', ' has-error') }}">

                                    <label for="name" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.contact_type') }}}
                                    </label>

                                    <select name="contact_type" id="contact_type" class="form-control" required autofocus>
                                        <option value="">{{ trans('bct/projects::projects/model.general.contact_type_help') }}</option>
                                        @foreach($contactTypes as $type)
                                            <option value="{{ $type->id }}"
                                                    {{ (isset($pivot) && $pivot->z_contact_type_id == $type->id)?'selected':'' }}>
                                                {{ $type->name }}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('contact_type') }}}</span>

                                </div>

                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="submit-btn-modal" name="submit-btn-modal" class="btn btn-primary">
                {{{ trans('action.save') }}}</button>
            @if(isset($pivot))
                <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-action="delete"
                        data-url="{{ route('bct.projects.projects.deleteContact', [
                            'id' => $project->id,
                            'pivot_id' => $pivot->id,
                        ]) }}">
                    {{{ trans('action.delete') }}}</button>
            @endif
            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
