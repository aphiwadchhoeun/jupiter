<!-- The template to display files available for download -->
<script id="template-files" type="text/x-tmpl">
<% for (var i=0, file; file=files[i]; i++) {  %>
    <tr class="template-download" data-id="<%=file.id%>">
        <td>
              <a href="<%= file.url%>" download="<%=file.name%>"><%=file.document_name%></a>
        </td>
        <td>
               <div><%=file.description%></div>
        </td>
        <td class="text-center">
               <div><%=file.size%></div>
        </td>
        <td class="text-center">
               <div><%=file.date_correct%></div>
        </td>

        <td class="text-center">
            <% if (file.deleteUrl) { %>
                <button class="btn btn-circle btn-primary btn-file-update" data-toggle="tooltip" data-placement="top" data-original-title="Edit" data-id="<%=file.id%>" data-name="<%=file.document_name%>" data-description="<%=file.description%>">
                    <i class="fa fa-book fa-fw"></i>
                </button>
                <button class="btn btn-circle btn-danger btn-file-delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" data-type="<%=file.deleteType%>" data-url="<%=file.deleteUrl%>"<% if (file.deleteWithCredentials) { %> data-xhr-fields='{"withCredentials":true}'<% } %>>
                    <i class="fa fa-trash fa-fw"></i>
                </button>

            <% } %>
        </td>
    </tr>
<% } %>
</script>