/**
 * Part of the Platform Media extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Media extension
 * @version    2.0.1
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */
var gridcontractorFilesSummary = $.datagrid('contractor-files-summary', '#contractor-files-summary', '.data-grid_pagination', '#data-grid_applied', {
	preventHashChange: true,
	hash: false
});
var Extension;

;(function(window, document, $, undefined)
{

	'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'contractor-files'
    });

	// Initialize functions
	Extension.Index.init = function()
	{
		Extension.Index
			.listeners()
			.datePicker(Extension)
			.dataGrid()
			.mediaManager()
		;
	};

	// Add Listeners
	Extension.Index.listeners = function()
	{
		Platform.Cache.$body
			.on('click', '[data-grid-row]', Extension.Index.checkRow)
			.on('click', '[data-grid-row] a', Extension.Index.titleClick)
			.on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
			.on('click', '#modal-confirm a.confirm', Extension.Index.bulkActions)
			.on('click', '[data-grid-calendar-preset]', Extension.Index.calendarPresets)
			.on('click', '[data-grid-bulk-action]:not([data-grid-bulk-action="delete"])', Extension.Index.bulkActions)
		;
		return this;
	};

	// Data Grid initialization
	Extension.Index.dataGrid = function()
	{
		var config = {
			preventHashChange: true,
			hash: false,
			scroll: '#data-grid',
			events: {
				removing: function(dg)
				{
					_.each(dg.applied_filters, function(filter)
					{
						if (filter.column === 'created_at' && filter.from !== undefined && filter.to !== undefined)
						{
							$('[data-grid-calendar]').val('');
						}
					});
				},
				sorted: function(response) {
					if (response.pagination.total== 0) {
						$('.download-container').addClass('hide');
					}
				},
				fetched: function(response) {
					if (response.pagination.filtered == 0) {
						$('.download-container').addClass('hide');
					} else {
						$('.download-container' ).removeClass('hide');
					}
				},
			},
			callback: function(data)
			{
				$('[data-grid-checkbox-all]').prop('checked', false);

				$('[data-action]').prop('disabled', true);

				Extension.Index
					.bulkStatus()
					.exporterStatus(data)
				;
			}
		};

		Extension.Index.Grid = $.datagrid('contractor-files', '#contractor-files-summary', '.data-grid_pagination', '#data-grid_applied', config);

		return this;
	};

	// Handle Data Grid checkboxes
	Extension.Index.checkboxes = function(event)
	{
		event.stopPropagation();

		var type = $(this).attr('data-grid-checkbox');

		if (type === 'all')
		{
			$('[data-grid-checkbox]').not(this).not('[data-grid-checkbox][disabled]').prop('checked', this.checked);

			$('[data-grid-row]').not('[data-grid-row][disabled]').not(this).toggleClass('active', this.checked);
		}

		$(this).parents('[data-grid-row]').not('[data-grid-row][disabled]').toggleClass('active');

		Extension.Index.bulkStatus();
	};

	// Handle Data Grid row checking
	Extension.Index.checkRow = function()
	{
		if ($(this).find('[data-grid-checkbox]').prop('disabled')) return false;

		$(this).toggleClass('active');

		var checkbox = $(this).find('[data-grid-checkbox]');

		checkbox.prop('checked', ! checkbox.prop('checked'));

		Extension.Index.bulkStatus();
	};

	Extension.Index.bulkStatus = function()
	{
		var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').not('[data-grid-checkbox][disabled]').length;

		var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').not('[data-grid-checkbox][disabled]').length;

		$('[data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);
		$('[data-grid-bulk-action]').toggleClass('disabled', ! checked);

		if (checked > 0)
		{
			$('[data-grid-bulk-action="delete"]').attr('data-modal', true);
		}
		else
		{
			$('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
		}

		$('[data-grid-checkbox="all"]')
			.prop('disabled', rows < 1)
			.prop('checked', rows < 1 ? false : rows === checked)
		;

		return this;
	};

	Extension.Index.exporterStatus = function(grid)
	{
		$('[files-export]').toggleClass('disabled', grid.pagination.filtered == 0);

		return this;
	};

	// Handle Data Grid bulk actions
	Extension.Index.bulkActions = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

		var action = $(this).data('grid-bulk-action') ? $(this).data('grid-bulk-action') : 'delete';

		if (action =='delete') {
			url = $('[data-grid-bulk-action="delete"]').data('grid-url') ?$('[data-grid-bulk-action="delete"]').data('grid-url') : url;
		}

		var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
		{
			return +event.value;
		});

		if (rows.length > 0)
		{
			if (action == 'email')
			{
				window.location = Extension.Config.emailRoute.replace('rows-ids', rows.join(','));
			}
			else
			{
				$.ajax({
					type: 'POST',
					url: url,
					data: {
						action : action,
						rows   : rows
					},
					success: function(response)
					{
						Extension.Index.Grid.refresh();
						gridcontractorFilesSummary.refresh();
						filesSummaryUpdate();
					}
				});
			}
		}
	};

	// Handle Data Grid calendar
	Extension.Index.calendarPresets = function(event)
	{
		event.preventDefault();

		var start, end;

		switch ($(this).data('grid-calendar-preset'))
		{
			case 'day':
				start = end = moment().subtract(1, 'day').startOf('day').format('MM/DD/YYYY');
				break;

			case 'week':
				start = moment().startOf('week').format('MM/DD/YYYY');
				end   = moment().endOf('week').format('MM/DD/YYYY');
				break;

			case 'month':
				start = moment().startOf('month').format('MM/DD/YYYY');
				end   = moment().endOf('month').format('MM/DD/YYYY');
				break;

			default:
		}

		$('input[name="daterangepicker_start"]').val(start);

		$('input[name="daterangepicker_end"]').val(end);

		$('.range_inputs .applyBtn').trigger('click');
	};

	// Ignore row selection on title click
	Extension.Index.titleClick = function(event)
	{
		event.stopPropagation();
	};

	Extension.Index.mediaManager = function()
	{

		Extension.Index.MediaManager = $.mediamanager({
			onFileQueued : function(file)
			{
				$('input.file-tags').not('.selectize-control').selectize({
					delimiter: ',',
					persist: false,
					maxItems: 3,
					create: function(input) {
						return {
							value: input,
							text: input
						}
					}
				});

				$('.upload__instructions').hide();
			},
            onProgress: function (evt) {
                $('.file-error-help').text('');
                $('.saving').html('Upload in progress<span>.</span><span>.</span><span>.</span>');
                $('.saving').show();
                $('#start_upload').prop('disabled', true);
            },
			onComplete : function()
			{
                $('.file-error-help').text('');
                $('.saving').hide();
				$modal.modal('hide');

				$('.upload__instructions').show();
				Extension.Index.Grid.refresh();
				gridcontractorFilesSummary.refresh();
				
                $('#start_upload').prop('disabled', false);
			},
			onFail : function(e)
			{$('.file-error-help').hide();
                $('.saving').html(e.responseText);
                $('.saving').show();
                $('#start_upload').prop('disabled', false);
			},
			onRemove : function(manager, file)
			{
				if (manager.totalFiles == 0)
				{
					$('.upload__instructions').show();
				}
			}
		});

		return this;
	};

	Extension.Index.setEmailRoute = function(url)
	{
		Extension.Config.emailRoute = url;

		return this;
	};

	// Job done, lets run.
	Extension.Index.init();

})(window, document, jQuery);
