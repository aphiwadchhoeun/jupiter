@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans("action.{$mode}") }}} {{ trans('bct/projects::stewards/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
{{ Asset::queue('user-form', 'bct/projects::settings/stewards/js/form.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Form --}}
    <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate
          data-refresh="stewards">

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <section class="panel panel-default">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip" href="{{ route('bct.projects.stewards.all') }}" data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{ trans("bct/projects::stewards/common.title") }} {{ trans("action.{$mode}") }}</span>
                        </div>

                        <div class="pull-right">
                            @if ($user->exists)
                                <a href="{{ route('bct.projects.stewards.delete', $user->id) }}"
                                   id="steward_delete"
                                   name="steward_delete"
                                   class="btn btn-danger"
                                   data-action-delete data-toggle="tooltip"
                                   data-original-title="{{{ trans('action.delete') }}}" type="delete">
                                    <i class="fa fa-trash-o fa-fw"></i></a>
                            @endif

                            <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                    data-original-title="{{{ trans('action.save') }}}">
                                <i class="fa fa-save fa-fw"></i>
                            </button>
                        </div>

                    </div>

                </nav>

            </header>

            @include('bct/projects::settings.stewards._form')

        </section>

    </form>
@stop
