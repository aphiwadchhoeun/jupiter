/**
 * Created by roman on 28.09.15.
 */
$.encodeURL = function (object) {

    // recursive function to construct the result string
    function createString(element, nest) {
        if (element === null) return '';
        if ($.isArray(element)) {
            var count = 0,
                url = '';
            for (var t = 0; t < element.length; t++) {
                if (count > 0) url += '&';
                url += nest + '[]=' + element[t];
                count++;
            }
            return url;
        } else if (typeof element === 'object') {
            var count = 0,
                url = '';
            for (var name in element) {
                if (element.hasOwnProperty(name)) {
                    if (count > 0) url += '&';
                    url += createString(element[name], nest + '.' + name);
                    count++;
                }
            }
            return url;
        } else {
            return nest + '=' + element;
        }
    }

    var url = '?',
        count = 0;

    // execute a createString on every property of object
    for (var name in object) {
        if (object.hasOwnProperty(name)) {
            if (count > 0) url += '&';
            url += createString(object[name], name);
            count++;
        }
    }

    return url;
};