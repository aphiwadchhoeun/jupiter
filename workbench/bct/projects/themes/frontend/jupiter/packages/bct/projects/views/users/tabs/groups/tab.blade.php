<header class="panel-heading">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip go-to-details" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{{ $user->first_name }}} {{{ $user->last_name }}} {{{ trans("bct/projects::users/common.tabs.groups") }}}</span>
            

            </div>
            
            @if ($user->exists)
                <button id="unlock-groups" class="btn btn-success pull-right margin-right5">
                                {{{ trans("bct/projects::users/common.edit_groups") }}}</button>
            @endif
        </div>

    </nav>

</header>

<div class="panel-body">

    <div class="row">

        <div class="col-md-12">

            <section class="panel panel-default panel-grid">

                {{-- Grid: Header --}}
                <div class="panel-heading hidden-print">

                    <nav class="navbar navbar-default navbar-actions">

                        <div class="container-fluid">

                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#project-contractors-actions">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <span class="navbar-brand">{{ trans("bct/projects::groups/common.user_groups") }}</span>
                            </div>

                            {{-- Grid: Actions --}}
                            <div class="collapse navbar-collapse" id="project-contractors-actions">

                                <ul class="nav navbar-nav navbar-left download-container" data-grid="groups-added">

                                    <li class="dropdown">
                                        <a href="#"
                                           class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                           aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                            <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                            <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                        </ul>
                                    </li>

                                </ul>

                                {{-- Grid: Filters --}}
                                <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                                      data-grid="groups-added" role="form">

                                    <div class="input-group">

                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" disabled>
                                                {{{ trans('common.status') }}}
                                            </button>

                                            <button class="btn btn-default dropdown-toggle" type="button"
                                                    data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>

                                            <ul class="dropdown-menu" role="menu" data-filter-reset>
                                                <li>
                                                    <a href="#" data-filter="status:1" id="rep-group-filter-status-active">
                                                        <i class="fa fa-check-circle"></i> {{trans('bct/projects::general/common.active')}}
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#" data-filter="status:0" id="rep-group-filter-status-disabled">
                                                        <i class="fa fa-times-circle"></i> {{trans('bct/projects::general/common.disabled')}}
                                                    </a>
                                                </li>
                                            </ul>
                                        </span>

                                        <span class="input-group-btn">
                                            <select name="column" class="form-control" id="rep-group-filter-select">
                                                <option value="name">{{ trans('bct/projects::groups/model.general.name') }}
                                                </option>
                                                <option value="description">{{ trans('bct/projects::groups/model.general.description') }}
                                                </option>
                                            </select>
                                        </span>

                                        <input class="form-control" name="filter" type="text" id="rep-group-filter-input-search"
                                               placeholder="{{{ trans('common.search') }}}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit" id="rep-group-filter-btn-search">
                                                <span class="fa fa-search"></span>
                                            </button>

                                            <button class="btn btn-default" data-grid="groups-added" data-reset id="rep-group-filter-btn-reset">
                                                <i class="fa fa-refresh fa-sm"></i>
                                            </button>
                                        </span>
                                    </div>

                                </form>

                            </div>

                        </div>

                    </nav>

                </div>

                <div class="panel-body">

                    {{-- Grid: Applied Filters --}}
                    <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                        <div id="data-grid_groups_added_applied" class="btn-group" data-grid="groups-added"></div>

                    </div>


                    {{-- Grid: Table --}}
                    <div class="table-responsive">

                        <table id="data-grid_groups_added" class="table table-hover"
                               data-source="{{ route('bct.projects.users.gridGroups', [$user->id, 'reverse' => true]) }}"
                               data-filename="{{ $user->first_name }} {{ $user->last_name }} {{ trans("bct/projects::users/common.tabs.groups") }}"
                               data-grid="groups-added">
                            <thead>
                                <tr>
                                    <th class="sortable" data-sort="name">{{{ trans('bct/projects::groups/model.general.name') }}}</th>
                                    <th class="sortable" data-sort="individual">{{{ trans('bct/projects::groups/model.general.type') }}}</th>
                                    <th class="sortable" data-sort="description">{{{ trans('bct/projects::groups/model.general.description') }}}</th>
                                    <th class="sortable" data-sort="status">{{{ trans('bct/projects::groups/model.general.status') }}}</th>
                                    <th>{{{ trans('bct/projects::general/common.actions') }}}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>

                    </div>

                </div>


                <footer class="panel-footer clearfix">

                    {{-- Grid: Pagination --}}
                    <div class="data-grid_groups_added_pagination" data-grid="groups-added"></div>

                </footer>

                {{-- Grid: templates --}}

                @include('bct/projects::users.grid.groups-added.results')
                @include('bct/projects::users.grid.groups-added.pagination')
                @include('bct/projects::users.grid.groups-added.filters')
                @include('bct/projects::users.grid.groups-added.no_results')

            </section>

        </div>

    </div>

</div>

<div id="groups-to-add" class="hidden">
    <div class="navbar-default">
        <span class="navbar-brand please-find-text"id="please-find-text">{{ trans('bct/projects::users/common.find_groups_tab') }}</span>
    </div>

    <div class="panel-body">

        <div class="row">

            <div class="col-md-12">

                <section class="panel panel-default panel-grid">

                    {{-- Grid: Header --}}
                    <div class="panel-heading hidden-print">

                        <nav class="navbar navbar-default navbar-actions">

                            <div class="container-fluid">

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#project-contractors-actions">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>

                                    <span class="navbar-brand">{{ trans("bct/projects::groups/common.available_groups") }}</span>
                                </div>

                                {{-- Grid: Actions --}}
                                <div class="collapse navbar-collapse" id="project-contractors-actions">

                                    <ul class="nav navbar-nav navbar-left download-container" data-grid="groups">

                                        <li class="dropdown">
                                            <a href="#"
                                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                            </ul>
                                        </li>

                                    </ul>

                                    {{-- Grid: Filters --}}
                                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                                          data-grid="groups" role="form">

                                        <div class="input-group">

                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" disabled>
                                                    {{{ trans('common.status') }}}
                                                </button>

                                                <button class="btn btn-default dropdown-toggle" type="button"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                                <ul class="dropdown-menu" role="menu" data-filter-reset>
                                                    <li>
                                                        <a href="#" data-filter="status:1" id="rep-available-group-filter-status-active">
                                                            <i class="fa fa-check-circle"></i> {{ trans('bct/projects::general/common.active') }}
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#" data-filter="status:0" id="rep-available-group-filter-status-disabled">
                                                            <i class="fa fa-times-circle"></i> {{ trans('bct/projects::general/common.disabled') }}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </span>

                                            <span class="input-group-btn">
                                                <select name="column" class="form-control" id="rep-available-group-filter-select">
                                                    <option value="name">{{ trans('bct/projects::groups/model.general.name') }}
                                                    </option>
                                                    <option value="description">{{ trans('bct/projects::groups/model.general.description') }}
                                                    </option>
                                                </select>
                                            </span>

                                            <input class="form-control" name="filter" type="text" id="rep-available-group-filter-input-search"
                                                   placeholder="{{{ trans('common.search') }}}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit" id="rep-available-group-filter-btn-search">
                                                    <span class="fa fa-search"></span>
                                                </button>

                                                <button class="btn btn-default" data-grid="groups" data-reset id="rep-available-group-filter-btn-reset">
                                                    <i class="fa fa-refresh fa-sm"></i>
                                                </button>
                                            </span>
                                        </div>

                                    </form>

                                </div>

                            </div>

                        </nav>

                    </div>

                    <div class="panel-body">

                        {{-- Grid: Applied Filters --}}
                        <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                            <div id="data-grid_groups_applied" class="btn-group" data-grid="groups"></div>

                        </div>


                        {{-- Grid: Table --}}
                        <div class="table-responsive">

                            <table id="data-grid_groups" class="table table-hover"
                                   data-source="{{ route('bct.projects.users.gridGroups', $user->id) }}"
                                   data-filename="{{ trans("bct/projects::groups/common.available_groups") }}"
                                   data-grid="groups">
                                <thead>
                                    <tr>
                                        <th class="sortable" data-sort="name">{{{ trans('bct/projects::groups/model.general.name') }}}</th>
                                        <th class="sortable" data-sort="individual">{{{ trans('bct/projects::groups/model.general.type') }}}</th>
                                        <th class="sortable" data-sort="description">{{{ trans('bct/projects::groups/model.general.description') }}}</th>
                                        <th class="sortable" data-sort="status">{{{ trans('bct/projects::groups/model.general.status') }}}</th>
                                        <th>{{{ trans('bct/projects::general/common.actions') }}}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>

                    </div>


                    <footer class="panel-footer clearfix">

                        {{-- Grid: Pagination --}}
                        <div class="data-grid_groups_pagination" data-grid="groups"></div>

                    </footer>

                    {{-- Grid: templates --}}
                    @include('bct/projects::users.grid.groups.results')
                    @include('bct/projects::users.grid.groups.pagination')
                    @include('bct/projects::users.grid.groups.filters')
                    @include('bct/projects::users.grid.groups.no_results')

                </section>

            </div>

        </div>

    </div>
</div>