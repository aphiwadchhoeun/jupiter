jQuery('document').ready(function ($) {
    $('#merge').click(function(e){
        e.preventDefault();
        var stay_id = $(this).data('stay');
        var merge_id = $(this).data('merge');
        var redirect_url = $(this).data('redirect');

        swal({
            title: "Are you sure you want merge?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            $.ajax({
                type: 'POST',
                url: '/pprofiles/projects/merge',
                async: false,
                data: {
                    stay: stay_id,
                    merge: merge_id
                },
                success: function (response) {
                    window.location=redirect_url;
                },
                error: function (response) {
                    var message = JSON.parse(response.responseText).message;
                    swal(message, null, 'error');
                }
            });
        });
    });
});