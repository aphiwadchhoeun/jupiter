<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    {{ Asset::queue('normalize', 'bct/projects::custom/css/normalize.css') }}
    {{ Asset::queue('custom-pdf-css', 'bct/projects::custom/css/bootstrap-grid-export.css') }}

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach
    <style>
        .text16{
            font-size:16px;
            font-weight:bold;
        }
        .col-md-6 {
            width: 49%;
        }
        table
        {
            border-collapse:unset!important;
        }
        .pre-spaces {
            padding-left: 1em;
        }
    </style>

</head>
<body>

{{--Headers with logo--}}
@include('bct/projects::pdf/partials/header')

<div class="hr10"></div>

<div class="ext-profiles">

    <div class="panel-body">
        <div class="row">

            <div class="col-md-12">
                <table class="table table-bordered table-details">
                    <tbody>
                    <tr>
                        <td class="td-value">
                            <div>

                                <div class="col-md-6 col-sm-12 border-right col-panel">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.name') }}:</b>
                                        <span class="pre-spaces">
                                            {{ $project->name }}
                                            @if($project->status) -
                                            {{ $project->status->name }}
                                            @endif
                                        </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.address') }}:</b>
                                            <span class="pre-spaces">{{ $project->full_address }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.percent_union') }}:</b>
                                            <span class="pre-spaces"><span id="union-value">{{ number_format($project->union_value * 100, 2) }}%</span></span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}:</b>
                                            <span class="pre-spaces">{{ date_correct_details_projects($project->follow_up_date) }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.description') }}:</b>
                                            <span class="pre-spaces">{{ $project->description }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::tags/common.tags') }}:</b>
                                            <span class="pre-spaces">{{ implode(',', $project->tags->pluck('name')->all()) }}</span>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-md-6 col-sm-12 col-panel">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.classification_id') }}:</b>
                                            <span class="pre-spaces">{{ ($project->classification) ? $project->classification->name : '' }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.wage') }}:</b>
                                            <span class="pre-spaces">{{ ($project->prevailingWage) ? $project->prevailingWage->name : '' }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.region') }}:</b>
                                            <span class="pre-spaces">{{ $project->region_abrv }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.visibility_id') }}:</b>
                                        <span class="pre-spaces">
                                            @if ($project->visibility)
                                                {{ $project->visibility->name }}
                                            @endif
                                        </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.type_id') }}:</b>
                                            <span class="pre-spaces">{{ ($project->type) ? $project->type->type : '' }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.value_id') }}:</b>
                                            <span class="pre-spaces">{{ ($project->value) ? $project->value->value : '' }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.reference_details') }}:</b>
                                            <span class="pre-spaces">{{ $project->reference_details }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::tags/common.tag_groups') }}:</b>
                                        <span class="pre-spaces">
                                            {{ $tag_groups }}
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

        @if (!$visits->isEmpty())

            <div><span class="text16">{{ trans('bct/projects::visits/common.title') }}</span> <span class="text14">( {{ count($visits->take(5)->all()) }} {{ trans('bct/projects::general/common.of') }} {{ $visits->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::visits/model.general.reps') }}</th>
                            <td class="td-label col-md-5 text-center"><b>{{trans('bct/projects::projects/model.general.follow_up_reason')}} & {{ trans('bct/projects::visits/model.general.notes') }}</b></td>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.stage') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.visit_date_time') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($visits->take(5)->all() as $visit)
                            <tr>
                                <td class="td-value">{{ $visit->reps_names }}</td>
                                <td class="td-value">
                                    <div><b>{{trans('bct/projects::projects/model.general.follow_up')}}:</b> {{ $visit->reason }}</div>
                                    <div><b>{{ trans('bct/projects::visits/model.general.notes') }}:</b> {{ $visit->details }}</div>
                                    @if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1)
                                        <div><span class="text-bold">{{ trans('bct/projects::visits/model.general.weather') }}:</span> {{ $visit->weather }}</div>
                                    @endif

                                    @if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1)
                                        <div><span class="text-bold">{{ trans('bct/projects::visits/model.general.workers_count') }}:</span> {{ $visit->workers_count }}</div>
                                    @endif
                                </td>
                                <td class="td-value text-center">{{ $visit->stages_names }}</td>
                                <td class="td-value text-center">{{ date_correct_details_projects( $visit->created_at ) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div><span class="text16">{{ trans('bct/projects::visits/common.title') }}</span> <span class="text14">( 0 {{ trans('bct/projects::general/common.of') }} 0 )</span></div>

        @endif


        @if (!$contractors->isEmpty())

            <div><span class="text16">{{ trans('bct/projects::contractors/common.title') }}</span> <span class="text14">( {{ count($contractors->take(5)->all()) }} {{ trans('bct/projects::general/common.of') }} {{ $contractors->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::projects/model.general.scope_id') }}</th>
                            <th class="td-label col-md-5 text-center">{{ trans('bct/projects::contractors/model.general.contractor') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.union') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.pa') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contractors->take(5)->all() as $contractor)
                            <tr>
                                <td class="td-value">{{ $contractor->scope }} ({{ $contractor->value }})</td>
                                <td class="td-value text-center">
                                    @if (!is_null($contractor->name))
                                        {{ $contractor->name }}
                                    @else
                                        {{ trans('bct/projects::general/common.na') }}
                                    @endif
                                </td>
                                <td class="td-value text-center">
                                    @if (!is_null($contractor->name))
                                        @if($contractor->is_union)
                                            Yes
                                        @else
                                            No
                                        @endif
                                    @endif
                                </td>
                                <td class="td-value text-center">
                                    @if (!is_null($contractor->name))
                                        @if ($contractor->is_agreement)
                                            Yes
                                        @else
                                            No
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div><span class="text16">{{ trans('bct/projects::contractors/common.title') }}</span> <span class="text14">( 0 {{ trans('bct/projects::general/common.of') }} 0 )</span></div>

        @endif

        @if (!$contacts->isEmpty())

            <div><span class="text16">{{ trans('bct/projects::contacts/common.title') }}</span><span class="text14">( {{ count($contacts->take(5)->all()) }} {{ trans('bct/projects::general/common.of') }} {{ $contacts->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::contacts/model.general.company') }}</th>
                            <th class="td-label col-md-5 text-center">{{ trans('bct/projects::contacts/model.general.name') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::projects/model.general.type') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::contacts/model.general.phone') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contacts->take(5)->all() as $contact)
                            <tr>
                                <td class="td-value">{{ $contact->company }}</td>
                                <td class="td-value text-center">{{ $contact->full_name }}</td>
                                <td class="td-value text-center">{{ $contact->pivot->contact_type->name }}</td>
                                <td class="td-value text-center">{{ $contact->phone }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div><span class="text16">{{ trans('bct/projects::contacts/common.title') }}</span> <span class="text14">( 0 {{ trans('bct/projects::general/common.of') }} 0 )</span></div>

        @endif

        @if (!$actions->isEmpty())

            <div><span class="text16">{{ trans('bct/projects::actions/common.title') }}</span> <span class="text14">( {{ count($actions->take(5)->all()) }} {{ trans('bct/projects::general/common.of') }} {{ $actions->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::actions/model.general.contractor_id') }}</th>
                            <th class="td-label col-md-5 text-center">{{ trans('bct/projects::actions/model.general.details') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.action_type') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.date') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($actions->take(5)->all() as $action)
                            <tr>
                                <td class="td-value">{{ ($action->contractor) ? $action->contractor->name : '' }}</td>
                                <td class="td-value text-center">{{ $action->details }}</td>
                                <td class="td-value text-center">{{ $action->action_type_name }}</td>
                                <td class="td-value text-center">{{ date_correct_details_projects( $action->action_date ) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div><span class="text16">{{ trans('bct/projects::actions/common.title') }}</span> <span class="text14">( 0 {{ trans('bct/projects::general/common.of') }} 0 )</span></div>

        @endif

        @if (!$files->isEmpty())

            <div><span class="text16">{{ trans('bct/projects::files/common.title') }}</span> <span class="text14">( {{ count($files->take(5)->all()) }} {{ trans('bct/projects::general/common.of') }} {{ $files->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::files/model.general.name') }}</th>
                            <th class="td-label col-md-5 text-center">{{ trans('bct/projects::files/model.general.description') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.type') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.date') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($files->take(5)->all() as $file)
                            <tr>
                                <td class="td-value"><a href="{{ $file->download_url }}" download="{{ $file->name_full }}">{{ $file->name }}</a></td>
                                <td class="td-value text-center">{{ $file->description }}</td>
                                <td class="td-value text-center">{{ $file->extension }}</td>
                                <td class="td-value text-center">{{ date_correct_details_projects( $file->created_at ) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div><span class="text16">{{ trans('bct/projects::files/common.title') }}</span> <span class="text14">( 0 {{ trans('bct/projects::general/common.of') }} 0 )</span></div>

        @endif

        @if (!$lists->isEmpty())

            <div><span class="text16">{{ trans('bct/projects::lists/common.title') }}</span> <span class="text14">( {{ count($lists->take(5)->all()) }} {{ trans('bct/projects::general/common.of') }} {{ $lists->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.name') }}</th>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.category') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.group') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.status') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.date_added') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lists->take(5)->all() as $list)
                            <tr>
                                <td class="td-value">{{ $list->name }}</td>
                                <td class="td-value text-center">{{ $list->category }}</td>
                                <td class="td-value text-center">{{ $list->group->name }}</td>
                                <td class="td-value text-center"><span class="label @if($list->status_name=="Active") label-success @else label-danger @endif">{{ $list->status_name }}</span></td>
                                <td class="td-value text-center">{{ date_correct_details_projects( $list->due_date ) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div><span class="text16">{{ trans('bct/projects::lists/common.title') }}</span> <span class="text14">( 0 {{ trans('bct/projects::general/common.of') }} 0 )</span></div>

        @endif


        @if (!$project->members->isEmpty())

            <div><span class="text16">{{ trans('bct/projects::members/common.title') }}</span> <span class="text14">( {{ count($project->members->take(5)->all()) }} {{ trans('bct/projects::general/common.of') }} {{ $project->members->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td class="td-label col-md-3 text-center">{{ trans('bct/projects::contacts/model.general.id') }}</td>
                            <td class="td-label col-md-3 text-center">{{ trans('bct/projects::members/model.general.name') }}</td>
                            <td class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.scope') }}</td>
                            <td class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.number_hours') }}</td>
                            <td class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.wage') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project->members()->orderBy(DB::raw('CONCAT(`z_members`.`first_name`, " ", `z_members`.`last_name`)'), 'asc')->take(5)->get() as $member)
                            <tr>
                                <td class="td-value">{{ $member->member_number }}</td>
                                <td class="td-value text-center">{{ $member->full_name }}</td>
                                <td class="td-value text-center">{{ $member->pivot->scope->name }}</td>
                                <td class="td-value text-center">{{ $member->pivot->number_of_hours }}</td>
                                <td class="td-value text-center">{{ show_money_projects($member->pivot->wage) }}</td></tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div><span class="text16">{{ trans('bct/projects::members/common.title') }} <span class="text14">( 0 {{ trans('bct/projects::general/common.of') }} 0 )</span></span></div>

        @endif

    </div>

</div>

</body>
</html>
