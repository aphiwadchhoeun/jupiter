@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::action_types/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('index', 'bct/projects::settings/action_types/js/index.js', 'extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::action_types/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="action-types" role="form">

                            <div class="input-group">

                                <span class="input-group-btn none">
                                    <select name="column" class="form-control">
                                        <option value="name" selected>{{ trans('bct/projects::action_types/model.general.name') }}
                                        </option>
                                    </select>
                                </span>

                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">

                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="action-types" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>


                            </div>

                        </form>

                        <div class="pull-right">
                            <a href="{{ route('bct.projects.action_types.create') }}"
                                id="action_type_new"
                                name="action_type_new"
                               class="btn btn-success"
                               data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}" data-modal>
                                <i class="fa fa-plus fa-fw"></i>
                            </a>

                        </div>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="action-types"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.action_types.grid') }}"
                       data-filename="{{ trans('bct/projects::action_types/common.title') }}"
                       data-grid="action-types">
                    <thead>
                    <tr>
                        <th class="sortable col-md-11" data-sort="name">{{ trans('bct/projects::action_types/model.general.name') }}
                        </th>
                        <th class="col-md-1">{{{ trans('bct/projects::general/common.details') }}}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="action-types"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::settings.action_types.grid.index.results')
        @include('bct/projects::settings.action_types.grid.index.pagination')
        @include('bct/projects::settings.action_types.grid.index.filters')
        @include('bct/projects::settings.action_types.grid.index.no_results')

    </section>

@stop
