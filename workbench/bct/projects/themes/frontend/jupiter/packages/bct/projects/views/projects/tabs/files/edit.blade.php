<div class="modal-dialog">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{{ trans('bct/projects::projects/common.file_update') }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="{{{ route('bct.projects.projects.editFile', [
                $file->project->id,
                $file->id
                ]) }}}" data-refresh="files">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>
                            <div class="row">

                                <div class="form-group">

                                    <label for="follow_up_date" class="control-label">
                                        {{{ trans('bct/projects::files/model.general.name') }}}
                                    </label>

                                    <input type="text" class="form-control" name="name" id="name" required autofocus
                                           placeholder="{{{ trans('bct/projects::files/model.general.name_help') }}}"
                                           value="{{ $file->name }}">

                                    <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                                </div>

                                <div class="form-group">

                                    <label for="description" class="control-label">
                                        {{{ trans('bct/projects::files/model.general.description') }}}
                                    </label>

                                    <textarea class="form-control" name="description"
                                              placeholder="{{{ trans('bct/projects::visits/model.general.description_help') }}}"
                                              required rows="5">{{ $file->description }}</textarea>

                                    <span class="help-block">{{{ Alert::onForm('description') }}}</span>

                                </div>

                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="delete-btn-modal" data-href="{{ route('bct.projects.projects.deleteFile', [ $file->project->id, $file->id ]) }}"
               data-method="delete" data-token="{{ csrf_token() }}"
               class="btn btn-danger"
               data-toggle="tooltip" data-original-title="Delete">
                <i class="fa fa-trash-o fa-fw"></i></button>

            <button id="submit-btn-modal" class="btn btn-primary" data-toggle="tooltip"
                    data-original-title="{{{ trans('action.save') }}}">
                {{{ trans('action.save') }}}</button>

            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
