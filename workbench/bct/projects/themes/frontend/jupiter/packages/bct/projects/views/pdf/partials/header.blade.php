<div class="row">
    <div class="col-md-1">
        <img src="{{ getLogo() }}" alt="@setting('platform.app.title')" height="40"/>
    </div>
    @if ($title)
        <div class="col-md-10 text-center">
            <h1>{{ $title }}</h1>
        </div>
    @endif
</div>