<script type="text/template" data-grid="project-contact" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.id %></td>
        <td><a href="<%- r.edit_uri %>"><%- r.full_name_short %></a></td>
        <td><a href="<%- r.edit_uri %>"><%- r.company %></a></td>
        <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
        <td>
            <a href="mailto:<%- r.email %>" class="hidden-print"><%- r.email %></a>
            <span class="visible-print"><%- r.email %></span>
        </td>
        <td><%- r.type %></td>
        <td class="hidden-print"><a href="<%- r.view_uri %>" data-modal
               data-toggle="tooltip" id="contact_edit" name="contact_edit" class="btn btn-primary btn-circle" data-placement="right"
               data-original-title="{{{ trans('bct/projects::general/common.edit') }}}"><i
                        class="fa fa-book fa-fw"></i></a></td>
    </tr>

    <% }); %>

</script>
