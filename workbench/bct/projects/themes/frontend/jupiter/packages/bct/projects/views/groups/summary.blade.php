@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::groups/common.title') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}

{{ Asset::queue('group-details', 'bct/projects::groups/js/details.js', 'jquery') }}
{{ Asset::queue('add-delete', 'bct/projects::groups/js/add-delete.js', 'extension-base') }}
{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-grid">
        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">

                    <ul class="nav nav-tabs">
                        <li class="dropdown">
                            <a href="#details" data-toggle="tab">
                                {{{ trans("bct/projects::groups/common.tabs.details") }}}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                <li class=""><a href="#details" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">@fat</a></li>
                                <li class=""><a href="#details-edit" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">@mdo</a></li>
                            </ul>
					    </li>
                        <li>
                            <a href="#reps" data-toggle="tab">
                                {{{ trans("bct/projects::groups/common.tabs.reps") }}}
                            </a>
                        </li>
                       
                    </ul>

                    <div class="tab-content">
                         
                        <div role="tabpanel" class="tab-pane fade" id="details">
                            @include('bct/projects::groups.tabs.details.tab')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="details-edit">
                            @include('bct/projects::groups.tabs.details.tab-edit')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="reps">
                            @include('bct/projects::groups.tabs.reps.tab')
                        </div>
                        
                    </div>

                </div>

            </div>
        </div>

    </section>

@stop
