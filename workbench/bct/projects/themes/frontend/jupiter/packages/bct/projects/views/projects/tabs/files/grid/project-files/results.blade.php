<script type="text/template" data-grid="project-file" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>

        <td><%- r.name %></td>
        <td><%- r.extension %></td>

        <td><%- r.description%></td>
        <td><%- r.username%></td>

        <td><small><%- r.size %></small></td>

        <td><%- r.created %></td>
        @if (!$currentUser->inRole('pp-guest'))

        <td class="text-center hidden-print">

            <a class="btn btn-default btn-circle" href="<%- r.download_uri %>" data-toggle="tooltip"
               data-original-title="{{{ trans('platform/media::action.download') }}}">
                <i class="fa fa-download fa-fw"></i>
            </a>
            <a href="<%- r.edit_uri %>" id="file_edit" name="file_edit" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
               data-modal data-original-title="{{{ trans('bct/projects::general/common.edit') }}}"><i
                        class="fa fa-book fa-fw"></i></a>

        </td>
        @endif
    </tr>

    <% }); %>

</script>
