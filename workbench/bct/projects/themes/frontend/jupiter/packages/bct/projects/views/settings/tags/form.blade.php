@extends('bct/projects::default')
{{-- Page title --}}

@section('title')
    @parent
    {{ trans('bct/projects::tags/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

    {{ Asset::queue('typeahead', 'bower_components/typeahead.js/dist/typeahead.jquery.js', 'jquery') }}
    {{ Asset::queue('bloodhound', 'bower_components/typeahead.js/dist/bloodhound.js', 'typeahead') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('typeahead', 'bower_components/typeahead.js-bootstrap-css/typeaheadjs.css') }}
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Form --}}
    <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate
          onkeypress="return event.keyCode != 13;">

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <section class="panel panel-default">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div>

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip" href="{{ route('bct.projects.tags.all') }}" data-toggle="tooltip"
                                       data-original-title="{{{ trans('bct/projects::general/common.back') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('bct/projects::general/common.back') }}}</span>
                                    </a>
                                </li>
                            </ul>
                            <span class="navbar-brand">{{ trans('bct/projects::tags/common.details.title') }}
                                @if ($mode==='update')
                                    {{ trans('bct/projects::general/common.edit') }}
                                @else
                                    {{ trans('bct/projects::general/common.create') }}
                                @endif
                            </span>
                        </div>

                        <div class="pull-right ml10">
                            <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                    data-original-title="{{{ trans('action.save') }}}">
                                <i class="fa fa-save fa-fw"></i></button>
                        </div>

                        @if ($mode === 'update')
                            <div class="pull-right">
                                <a href="{{ route( 'bct.projects.tags.delete', [ $tag->id ] ) }}"
                                   data-method="delete" data-token="{{ csrf_token() }}" data-swal="true"
                                   class="btn btn-danger pull-right"
                                   data-toggle="tooltip" data-original-title="Delete">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                        @endif

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <div class="col-md-12">

                    <fieldset>

                        <div class="row">

                            <div class="form-group">
                                    <label class="control-label" for="name">{{trans('bct/projects::general/form.name')}}</label>
                                    <input class="form-control" placeholder="Enter the Name here" type="text" name="name" id="name" value="{{ data_get($tag,'name','') }}"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="group">{{trans('bct/projects::tags/common.group_description')}}</label>
                                <input class="form-control" placeholder="Enter the Group here" type="text" name="group" id="group" value="{{ data_get($tag,'group','') }}"/>
                            </div>

                            </div>


                    </fieldset>

                </div>

            </div>

        </section>

    </form>
@stop
