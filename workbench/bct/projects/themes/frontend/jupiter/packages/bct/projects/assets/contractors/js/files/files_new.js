/**
 * Created by roman on 08.02.16.
 */
/**
 * Created by roman on 14.12.15.
 */

$(function() {
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        //url: 'job/document/upload',
        autoUpload: true
    }).bind('fileuploaddone', function (e, data) {
        $(document).on('keydown', '.template-download input.form-control', function(event) {
            if(event.which === 13) return false;
        });

    }).bind('fileuploadcompleted', function (e, data) {

    });



    // Load existing files:
    $(self).find('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $('#fileupload').data('action-get'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {

        $(self).find('#fileupload').removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: {files: result}});

        $(document).on('keydown', '.template-download input.form-control', function(event) {
            if(event.which === 13) return false;
        });
        $('#fileupload.repuser-file .files input[type="text"]').prop('disabled',true);
        $('#fileupload.repuser-file .files .delete').prop('disabled',true);
        $('input[type="file"]').prop('disabled',false);

    });

    $(document.body).on('click', '.btn-files-update', function(e) {
        e.preventDefault();

        if ($('.template-download').length > 0) {
            var data = [];

            $('.template-download').each(function(index) {
                data.push({
                    id: $(this).data('id'),
                    name: $(this).find('input[name="name"]').val(),
                    description: $(this).find('input[name="description"]').val(),
                });

                var url = window.location.origin + '/pprofiles/contractors/files/update';

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        files_data: data
                    },
                    success: function(response) {
                        if($('#fileupload').hasClass('repuser-file')){
                            location.reload();
                        }
                        else {
                            swal({
                                title: "Success!",
                                text: "Files were updated",
                                type: "success",
                                timer: 1500,
                                showConfirmButton: false
                            });
                        }

                        
                    },
                    error: function(response) {
                        var errors = JSON.parse(response.responseText).errors;

                        swal({
                            title: "Error!",
                            text: "=(",
                            type: "error",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    }
                });
            });

        }

    });
});