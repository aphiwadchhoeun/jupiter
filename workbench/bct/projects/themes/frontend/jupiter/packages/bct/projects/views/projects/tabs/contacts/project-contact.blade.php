{{ Asset::queue('project-contact-grid', 'bct/projects::projects/js/contacts/project-contact.js', ['extension-base', 'bootstrap-typeahead', 'select2-jquery']) }}

<div class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <header class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-contacts-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::projects/common.title') }} {{ trans('bct/projects::contacts/common.title') }}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-contacts-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-contact-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="project-contact" role="form">

                        <div class="input-group">

                            <span class="input-group-btn">

                                <button class="btn btn-default" type="button" disabled>
                                    {{ trans('bct/projects::projects/model.general.contact_type') }}
                                </button>

                                <button class="btn btn-default dropdown-toggle" type="button"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

                                <ul class="dropdown-menu" role="menu">

                                    @foreach(\Bct\Projects\Models\ContactType::all() as $type)
                                    <li>
                                        <a href="#" data-filter="type:{{ $type->name }}" id="project-contact-filter-type-{{ $type->name }}">
                                            <i class="fa fa-user"></i> {{ $type->name }}
                                        </a>
                                    </li>
                                    @endforeach

                                </ul>

                            </span>

                            <input class="form-control" name="filter" type="text" id="project-contact-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="project-contact-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="project-contact" data-reset id="project-contact-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                        </div>

                    </form>

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="project-contact-data-grid-applied-filters">

            <div id="project-contact-data-grid_applied" class="btn-group" data-grid="project-contact"></div>

        </div>

        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="project-contact-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridContacts', ['id' => $project->id]) }}"
                   data-filename="{{ $project->name }} {{ trans("bct/projects::projects/common.tabs.contacts") }}"
                   data-grid="project-contact">
                <thead>
                <tr>
                    <th class="sortable col-md-1"
                        data-sort="id">{{ trans('bct/projects::contacts/model.general.id') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="full_name">{{ trans('bct/projects::contacts/model.general.name') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="company">{{ trans('bct/projects::contacts/model.general.company') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="phone">{{ trans('bct/projects::contacts/model.general.phone') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="email">{{ trans('bct/projects::contacts/model.general.email') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="type">{{ trans('bct/projects::projects/model.general.contact_type') }}</th>
                    <th class="hidden-print col-md-1">{{ trans('bct/projects::general/common.actions') }}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>

    <footer class="panel-footer clearfix hidden-print">

        {{-- Grid: Pagination --}}
        <div class="project-contact-data-grid_pagination" data-grid="project-contact"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.contacts.grid.project-contacts.results')
    @include('bct/projects::projects.tabs.contacts.grid.project-contacts.pagination')
    @include('bct/projects::projects.tabs.contacts.grid.project-contacts.filters')
    @include('bct/projects::projects.tabs.contacts.grid.project-contacts.no_results')

</div>