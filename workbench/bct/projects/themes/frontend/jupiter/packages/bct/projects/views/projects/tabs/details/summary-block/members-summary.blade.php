<span class="navbar-brand">{{ trans("bct/projects::members/common.title") }} <span id="members-counter" class="text14">( {{ $members->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $members->count() }} )</span></span>
@if (!$currentUser->inRole('pp-guest'))

<a data-id="#members" id="members_tab" name="members_tab" class="btn btn-primary go-to-tab pull-right"
   data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
            class="fa fa-edit fa-fw"></i></a>
@endif
<table class="table table-striped table-bordered">
    <tbody>
    <tr>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::contacts/model.general.id') }}</td>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::members/model.general.name') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.scope') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.number_hours') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.wage') }}</td>
    </tr>
    @foreach($members->take(5) as $member)
        <tr>
            <td class="td-value">{{ $member->member_number }}</td>
            <td class="td-value text-center">{{ $member->full_name }}</td>
            <td class="td-value text-center">{{ $member->pivot->scope->name }}</td>
            <td class="td-value text-center">{{ $member->pivot->number_of_hours }}</td>
            <td class="td-value text-center">{{ show_money_projects($member->pivot->wage) }}</td>
        </tr>
    @endforeach
</table>