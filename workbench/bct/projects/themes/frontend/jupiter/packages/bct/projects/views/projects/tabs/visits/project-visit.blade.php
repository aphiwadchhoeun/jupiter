{{ Asset::queue('project-visit-grid', 'bct/projects::projects/js/visits/project-visit.js', ['extension-base', 'bootstrap-datetimepicker', 'select2-jquery', 'bootstrap-datetimepicker']) }}

<section class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <header class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-visits-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::projects/common.project_visits') }}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-visits-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-visit-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span
                                        class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="project-visit" role="form">

                        <div class="input-group">

                            <span class="input-group-btn">

                                <button class="btn btn-default" type="button" data-grid-calendar
                                        id="project-visit-filter-calendar"
                                        data-range-filter="created_at">
                                    <i class="fa fa-calendar"></i>
                                </button>


                            </span>
                            <span class="input-group-btn">
                                <select name="column" class="form-control" id="project-visit-filter-select">
                                    <option value="users_names">{{ trans('bct/projects::visits/model.general.reps') }}
                                    </option>
                                    <option value="details">{{ trans('bct/projects::visits/model.general.notes') }}
                                    </option>
                                    <option value="stages_names">{{ trans('bct/projects::visits/model.general.stage') }}
                                    </option>
                                </select>
                            </span>
                            <input class="form-control" name="filter" type="text"
                                   id="project-visit-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="project-visit-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="project-visits" data-reset id="project-visit-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>


                        </div>

                        <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-start>
                        <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-end>


                    </form>
                    @if (!$currentUser->inRole('pp-guest'))
                    <div class="pull-right">

                        <a href="{{ route('bct.projects.projects.createVisit', $project->id) }}"
                            id="visit_new"
                            name="visit_new"
                           data-modal
                           class="btn btn-success pull-right margin-right5 visits-add-btn ml10" data-toggle="tooltip"
                           id="project-visit-btn-create"
                           data-original-title="{{{ trans('action.create') }}}">
                            <i class="fa fa-plus fa-fw"></i>
                        </a>

                    </div>
                    @endif

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="project-visit-data-grid-applied-filters">

            <div id="project-visit-data-grid_applied" class="btn-group" data-grid="project-visit"></div>

        </div>

        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="project-visit-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridVisits', $project->id) }}"
                   data-filename="{{ $project->name }} {{ trans("bct/projects::projects/common.tabs.visits") }}"
                   data-grid="project-visit">
                <thead>
                <tr>
                    <th class="col-md-2">{{ trans('bct/projects::visits/model.general.reps') }}</th>
                    <th class="sortable col-md-3"
                        data-sort="details">{{ trans('bct/projects::projects/model.general.follow_up_reason') }} & {{ trans('bct/projects::visits/model.general.notes') }}</th>
                    <th class="col-md-2">{{ trans('bct/projects::visits/model.general.stage') }}</th>

                    @if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1)
                        <th class="col-md-1">{{ trans('bct/projects::visits/model.general.weather') }}</th>
                    @endif

                    @if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1)
                        <th class="col-md-2 text-center">{{ trans('bct/projects::visits/model.general.workers_count') }}</th>
                    @endif

                    <th class="sortable col-md-2"
                        data-sort="created_at">{{ trans('bct/projects::visits/model.general.visit_date_time') }}</th>
                    <th class="sortable col-md-1"
                        data-sort="approved">{{ trans('bct/projects::projects/model.general.status_id') }}
                    </th>
                    @if (!$currentUser->inRole('pp-guest'))
                    <th class="hidden-print col-md-1">{{{ trans('bct/projects::general/common.actions') }}}</th>
                    @endif
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>

    <footer class="panel-footer clearfix hidden-print">

        {{-- Grid: Pagination --}}
        <div class="project-visit-data-grid_pagination" data-grid="project-visit"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.visits.grid.project-visits.results')
    @include('bct/projects::projects.tabs.visits.grid.project-visits.pagination')
    @include('bct/projects::projects.tabs.visits.grid.project-visits.filters')
    @include('bct/projects::projects.tabs.visits.grid.project-visits.no_results')

</section>