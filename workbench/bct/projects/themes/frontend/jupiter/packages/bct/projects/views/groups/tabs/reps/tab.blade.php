<header class="panel-heading hidden-print">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip go-to-details" data-toggle="tooltip"
                           data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{{ $group->name }}} {{{ trans("bct/projects::groups/common.tabs.reps") }}}</span>

            </div>

            @if ($group->exists)
                <button id="unlock-reps" class="btn btn-success pull-right margin-right5">
                    {{{ trans("bct/projects::groups/common.edit_reps") }}}</button>
            @endif

        </div>

    </nav>

</header>

<div class="panel-body">
    <div class="row visible-print">
        <h4 class="overview-heading text-center visible-print">
            {{ trans("bct/projects::groups/common.title") }} {{ trans("bct/projects::groups/common.tabs.reps") }}
        </h4>
    </div>

    <div class="row">

        <div class="col-md-12">
            @include('bct/projects::groups.tabs.reps.group-reps')
        </div>

    </div>

</div>

<div class="hidden" id="reps-to-add">

    <div class="navbar-default">
        <span class="navbar-brand please-find-text"id="please-find-text">{{ trans('bct/projects::groups/common.find_reps_tab') }}</span>
    </div>

    <div class="panel-body hidden-print">

        <div class="row">

            <div class="col-md-12">
                @include('bct/projects::groups.tabs.reps.all-reps')
            </div>

        </div>

    </div>

</div>