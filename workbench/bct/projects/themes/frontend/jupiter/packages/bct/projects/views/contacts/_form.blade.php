<div class="panel-body">

    <div class="col-md-12">

        {{-- Form: General --}}
        <fieldset>

            <div class="row">

                <div class="col-md-6">

                    {{--add more form-group for other fields, just keep the same uniform--}}
                    {{-- Form:name --}}
                    <div class="form-group{{ Alert::onForm('first_name', ' has-error') }}">

                        <label for="first_name" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.first_name') }}}
                        </label>

                        <input type="text" class="form-control" name="first_name" id="first_name"
                               maxlength="64"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.first_name_help') }}}"
                               value="{{{ input()->old('first_name', $contact->first_name) }}}">

                        <span class="help-block">{{{ Alert::onForm('first_name') }}}</span>

                    </div>

                </div>

                <div class="col-md-6">

                    {{-- Form:last_name --}}
                    <div class="form-group{{ Alert::onForm('last_name', ' has-error') }}">

                        <label for="last_name" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.last_name') }}}
                        </label>

                        <input type="text" class="form-control" name="last_name" id="last_name"
                               maxlength="64"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.last_name_help') }}}"
                               value="{{{ input()->old('last_name', $contact->last_name) }}}">

                        <span class="help-block">{{{ Alert::onForm('last_name') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">

                    {{-- Form:company --}}
                    <div class="form-group{{ Alert::onForm('company', ' has-error') }}">

                        <label for="company" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.company') }}}
                        </label>

                        <input type="text" class="form-control" name="company" id="company"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.company_help') }}}"
                               value="{{{ input()->old('company', $contact->company) }}}">

                        <span class="help-block">{{{ Alert::onForm('company') }}}</span>

                    </div>

                </div>

                <div class="col-md-6">

                    {{-- Form:title --}}
                    <div class="form-group{{ Alert::onForm('title', ' has-error') }}">

                        <label for="title" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.title') }}}
                        </label>

                        <input type="text" class="form-control" name="title" id="title"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.title_help') }}}"
                               value="{{{ input()->old('title', $contact->title) }}}">

                        <span class="help-block">{{{ Alert::onForm('title') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">

                    {{-- Form:address --}}
                    <div class="form-group{{ Alert::onForm('address', ' has-error') }}">

                        <label for="address" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.address') }}}
                        </label>

                        <input type="text" class="form-control" name="address" id="address"
                               maxlength="64"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.address_help') }}}"
                               value="{{{ input()->old('address', $contact->address) }}}">

                        <span class="help-block">{{{ Alert::onForm('address') }}}</span>

                    </div>

                </div>

                <div class="col-md-6">

                    {{-- Form:city --}}
                    <div class="form-group{{ Alert::onForm('city', ' has-error') }}">

                        <label for="city" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.city') }}}
                        </label>

                        <input type="text" class="form-control" name="city" id="city"
                               maxlength="25"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.city_help') }}}"
                               value="{{{ input()->old('city', $contact->city) }}}">

                        <span class="help-block">{{{ Alert::onForm('city') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">

                    {{-- Form:state --}}
                    <div class="form-group{{ Alert::onForm('state', ' has-error') }}">

                        <label for="state" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.state') }}}
                        </label>

                        <input type="text" class="form-control" name="state" id="state" maxlength="2"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.state_help') }}}"
                               value="{{{ input()->old('state', $contact->state) }}}">

                        <span class="help-block">{{{ Alert::onForm('state') }}}</span>

                    </div>

                </div>

                <div class="col-md-6">

                    {{-- Form:zip --}}
                    <div class="form-group{{ Alert::onForm('zip', ' has-error') }}">

                        <label for="zip" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.zip') }}}
                        </label>

                        <input type="text" class="form-control" name="zip" id="zip" data-mask-zip
                               placeholder="{{{ trans('bct/projects::contacts/model.general.zip_help') }}}"
                               value="{{{ input()->old('zip', $contact->zip) }}}">

                        <span class="help-block">{{{ Alert::onForm('zip') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">

                    {{-- Form:phone --}}
                    <div class="form-group{{ Alert::onForm('phone', ' has-error') }}">

                        <label for="phone" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.phone') }}}
                        </label>

                        <input type="text" class="form-control" name="phone" id="phone" data-mask
                               placeholder="{{{ trans('bct/projects::contacts/model.general.phone_help') }}}"
                               value="{{{ input()->old('phone', $contact->phone) }}}">

                        <span class="help-block">{{{ Alert::onForm('phone') }}}</span>

                    </div>

                </div>

                <div class="col-md-6">

                    {{-- Form:phone2 --}}
                    <div class="form-group{{ Alert::onForm('phone2', ' has-error') }}">

                        <label for="phone2" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.phone2') }}}
                        </label>

                        <input type="text" class="form-control" name="phone2" id="phone2" data-mask
                               placeholder="{{{ trans('bct/projects::contacts/model.general.phone2_help') }}}"
                               value="{{{ input()->old('phone2', $contact->phone2) }}}">

                        <span class="help-block">{{{ Alert::onForm('phone2') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">

                    {{-- Form:fax --}}
                    <div class="form-group{{ Alert::onForm('fax', ' has-error') }}">

                        <label for="fax" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.fax') }}}
                        </label>

                        <input type="text" class="form-control" name="fax" id="fax" data-mask
                               placeholder="{{{ trans('bct/projects::contacts/model.general.fax_help') }}}"
                               value="{{{ input()->old('fax', $contact->fax) }}}">

                        <span class="help-block">{{{ Alert::onForm('fax') }}}</span>

                    </div>

                </div>

                <div class="col-md-6">

                    {{-- Form:email --}}
                    <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                        <label for="email" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.email') }}}
                        </label>

                        <input type="text" class="form-control" name="email" id="email"
                               placeholder="{{{ trans('bct/projects::contacts/model.general.email_help') }}}"
                               value="{{{ input()->old('email', $contact->email) }}}">

                        <span class="help-block">{{{ Alert::onForm('email') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    {{-- Form:notes --}}
                    <div class="form-group{{ Alert::onForm('notes', ' has-error') }}">

                        <label for="notes" class="control-label">
                            {{{ trans('bct/projects::contacts/model.general.notes') }}}
                        </label>

                        <textarea class="form-control" name="notes" rows="3"
                                  placeholder="{{{ trans('bct/projects::contacts/model.general.notes_help') }}}"
                                >{{{ input()->old('notes', $contact->notes) }}}</textarea>

                        <span class="help-block">{{{ Alert::onForm('notes') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-3">

                    {{-- Form:notes --}}
                    <div class="form-group{{ Alert::onForm('active', ' has-error') }}">

                        <label for="active" class="control-label">
                            {{{ trans('bct/projects::general/common.active') }}}
                        </label>

                        <input type="checkbox" name="active" id="active"
                               @if ( $contact->active)
                               checked
                                @endif
                        />
                        <span class="help-block">{{{ Alert::onForm('active') }}}</span>

                    </div>

                </div>

            </div>

        </fieldset>

    </div>

</div>

<div class="panel-footer">
    @if (request()->ajax())
        <button id="submit-btn" class="btn btn-primary">
            <i class="fa fa-save fa-fw"></i>{{{ trans('action.save') }}}
        </button>

        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ trans('action.cancel') }}
        </button>
    @endif

</div>