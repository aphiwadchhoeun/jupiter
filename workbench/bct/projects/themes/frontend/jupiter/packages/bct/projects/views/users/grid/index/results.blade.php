<script type="text/template" data-grid="user" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.name %></td>
        <td><a class="under-none" href="mailto:<%- r.email %>"><%- r.email %></a></td>
        <td><%- r.created %></td>
        <td><%- r.role %></td>
        <td>
            <% if(r.status == 1) { %>
            <span class="label label-success">{{ trans('bct/projects::general/common.active') }}</span>
            <% } else { %>
            <span class="label label-danger">{{ trans('bct/projects::general/common.disabled') }}</span>
            <% } %>
        </td>
        <td class="hidden-print">
            <a href="<%- r.link_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
               data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                        class="fa fa-book fa-fw"></i></a>
        </td>
    </tr>

    <% }); %>

</script>
