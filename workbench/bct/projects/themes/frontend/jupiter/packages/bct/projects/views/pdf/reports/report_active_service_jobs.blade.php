<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        {{ Asset::queue('normalize', 'bct/projects::custom/css/normalize.css') }}
        {{ Asset::queue('bootstrap-grid', 'bootstrap/css/bootstrap-grid-export.css') }}

        {{-- Compiled styles --}}
        @foreach (Asset::getCompiledStyles() as $style)
            <link href="{{ $style }}" rel="stylesheet">
        @endforeach

        <style>
            table,
            table tr th,
            table tr td {
                border-color: #282828 !important;
            }
        </style>

    </head>
    <body>

        @include('bct/projects::pdf/partials/header')

        @if ($results)

            <div class="hr10"></div>

            <div class="ext-profiles">

                <div class="row">
                    <div class="col-lg-12">

                        <table class="table table-bordered table-details">

                            <thead>
                            <tr>
                                <th>{{ trans('bct/projects::reports/common.col.region') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.project_name') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.contractor_name') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.project_address') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.project_city') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.stewards') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results as $item)

                                <tr>
                                    <td>{{ $item->region }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->contractors }}</td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->city }}</td>
                                    <td style="white-space: nowrap;">{{ $item->stewards }}</td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>

                    </div>
                </div>

            </div>

        @endif

    </body>
</html>
