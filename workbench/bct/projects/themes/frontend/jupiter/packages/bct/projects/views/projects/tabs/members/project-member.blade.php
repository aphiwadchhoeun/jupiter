{{ Asset::queue('project-member-grid', 'bct/projects::projects/js/members/project-member.js', ['extension-base', 'bootstrap-typeahead', 'select2-jquery']) }}

<div class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <header class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-members-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::projects/common.title') }} {{ trans('bct/projects::members/common.title') }}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-members-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-member-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="project-member" role="form">

                        <div class="input-group">

                            <input class="form-control" name="filter" type="text" id="project-member-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="project-member-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="project-member" data-reset id="project-member-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                        </div>

                    </form>

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="project-member-data-grid-applied-filters">

            <div id="project-member-data-grid_applied" class="btn-group" data-grid="project-member"></div>

        </div>

        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="project-member-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridMembers', ['id' => $project->id]) }}"
                   data-filename="{{ $project->name }} {{ trans("bct/projects::projects/common.tabs.members") }}"
                   data-grid="project-member">
                <thead>
                <tr>
                    <th class="sortable col-md-1"
                        data-sort="member_number">{{ trans('bct/projects::contacts/model.general.id') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="member_name">{{ trans('bct/projects::members/model.general.name') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="scope_name">{{ trans('bct/projects::members/model.general.scope') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="number_of_hours">{{ trans('bct/projects::members/model.general.number_hours') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="wage">{{ trans('bct/projects::members/model.general.wage') }}</th>
                    @if (!$currentUser->inRole('pp-guest'))
                    <th class="hidden-print col-md-1">{{ trans('bct/projects::general/common.actions') }}</th>
                    @endif
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>

    <footer class="panel-footer clearfix hidden-print">

        {{-- Grid: Pagination --}}
        <div class="project-member-data-grid_pagination" data-grid="project-member"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.members.grid.project-members.results')
    @include('bct/projects::projects.tabs.members.grid.project-members.pagination')
    @include('bct/projects::projects.tabs.members.grid.project-members.filters')
    @include('bct/projects::projects.tabs.members.grid.project-members.no_results')

</div>