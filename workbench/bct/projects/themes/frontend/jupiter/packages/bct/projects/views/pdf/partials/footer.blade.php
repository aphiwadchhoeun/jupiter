<div class="pdf-footer">
    <div class="footer-timestamp">
        {{ Carbon\Carbon::now()->format('n/j/Y H:i:s') }}
    </div>
    <div class="footer-title text-center">{{ $footer_title or '' }}</div>
    <div class="footer-info">
        <span>Page <span class="pagenum"></span>
        <span>@setting('platform.app.tagline')</span>
    </div>
</div>