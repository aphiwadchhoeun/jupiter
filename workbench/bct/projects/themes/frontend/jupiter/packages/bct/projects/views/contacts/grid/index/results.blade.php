<script type="text/template" data-grid="contact" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
            @if (!$currentUser->inRole('repuser'))
                <td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%- r.id %>"></td>
            @endif
            <td><%- r.id %></td>
            <td><%- r.name %></td>
            <td><%- r.company %></td>
            <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
            <td><a class="under-none" href="mailto:<%- r.email %>"><%- r.email %></a></td>
            <td><span class="label label-<%- r.active_color %>"><%- r.active_text %></span></td>

                <td>
                <a href="<%- r.view_uri %>" class="btn btn-primary btn-circle btn-details" data-toggle="tooltip" data-placement="right"
                    data-original-title="{{{ trans('bct/projects::general/common.details') }}}"
                   data-id="<%- r.id %>"
                   data-full_name="<%- r.full_name_short %>"
                   data-location="<%- r.location_short %>"
                   data-company="<%- r.company %>"
                   data-title="<%- r.title_short %>"
                   data-phone="<%- r.phone %>"
                   data-phone2="<%- r.phone2 %>"
                   data-fax="<%- r.fax %>"
                   data-email="<%- r.email_short %>"
                   data-notes="<%- r.notes_short %>"

                >
                    <i class="fa fa-book fa-fw"></i>
                </a>
            </td>
		</tr>

	<% }); %>

</script>
