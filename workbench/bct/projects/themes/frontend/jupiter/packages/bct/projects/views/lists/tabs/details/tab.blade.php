<header class="panel-heading hidden-print">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#actions">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip" href="{{ route('bct.projects.lists.search') }}"
                           data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $list->name }} {{ trans("bct/projects::general/common.details") }}</span>
            </div>

            <div class="pull-right">
            @if($has_access && !$currentUser->inRole('pp-guest'))
                <a href="#details-edit" id="show-details-edit" name="show-details-edit" data-toggle="tooltip"
                   data-original-title="{{{ trans('action.edit') }}}" class="btn btn-primary"><i
                            class="fa fa-edit fa-fw"></i></a>
            @endif
            </div>

        </div>

    </nav>

</header>

<div class="panel-body">

    <div class="row visible-print">
        <h4 class="overview-heading text-center visible-print">
            {{ data_get($list, 'name', '') }} -
            {{ trans("bct/projects::lists/common.title") }} {{ trans("bct/projects::general/common.details") }}
        </h4>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-details">
                <tbody>
                <tr>
                <td class="td-value">
                    <div>

                     <div class="col-md-6 col-sm-12 col-panel">
                        <div class="row">
                            <div class="col-md-12">
                                <b>{{ trans('bct/projects::lists/model.general.name') }}:</b>
                                <span class="pre-spaces">
                                    {{ data_get($list, 'name', '') }}  -
                                    @if(data_get($list, 'is_active', false))
                                        <span class="label label-success">{{ trans('bct/projects::lists/model.general.active') }}</span>
                                    @else
                                        <span class="label label-danger">{{ trans('bct/projects::lists/model.general.inactive') }}</span>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b>{{ trans('bct/projects::lists/model.general.description') }}:</b>
                                <span class="pre-spaces">{{ data_get($list, 'description', '') }}</span>
                            </div>
                        </div>
                     </div>

                     <div class="col-md-6 col-sm-12 border-left col-panel">
                        <div class="row">
                            <div class="col-md-6">
                                <b>{{{ trans('bct/projects::lists/model.general.due_date') }}}:</b>
                                <span class="pre-spaces">{{ date('M-d-Y', strtotime(data_get($list, 'due_date', ''))) }}</span>
                            </div>

                            <div class="col-md-6">
                                <b>{{ trans('bct/projects::lists/model.general.created_at') }}:</b>
                                <span class="pre-spaces">{{ date('M-d-Y', strtotime(data_get($list, 'created_at', ''))) }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b>{{ trans('bct/projects::lists/model.general.category') }}:</b>
                                <span class="pre-spaces">{{ data_get($list, 'category', '') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <b>{{ trans('bct/projects::lists/model.general.group_id') }}:</b>
                                <span class="pre-spaces">{{ data_get($list->group, 'name', '') }}</span>
                            </div>
                        </div>
                     </div>
                    </div>

                </td>

                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">

            <span class="navbar-brand">{{{ trans("bct/projects::projects/common.title") }}} <span
                        id="projects-counter" class="text14">({{ $grid_projects_count }} of {{ $projects_count }})</span></span>
            @if (!$currentUser->inRole('pp-guest'))

            <a data-id="#projects" id="projects_tab" name="projects_tab" class="btn btn-primary pull-right go-to-tab hidden-print"
               data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
                        class="fa fa-edit fa-fw"></i></a>
            @endif
                <table id="data-grid-project-summary" class="table table-striped table-bordered"
                       data-source="{{ route('bct.projects.lists.gridForProjects', [$list->id, 'limit' => 5]) }}"
                       data-grid="project-summary">
                    <thead>
                    <tr>
                        <td class="td-label col-md-6 text-center">{{ trans('bct/projects::projects/model.general.name') }}</td>
                        <td class="td-label col-md-6 text-center">{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}</td>
                        <td class="td-label col-md-6 text-center">{{ trans('bct/projects::projects/model.general.status_id') }}</td>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                @include('bct/projects::lists.tabs.projects.grid.project-summary.results')
                @include('bct/projects::lists.tabs.projects.grid.project-summary.pagination')
                @include('bct/projects::lists.tabs.projects.grid.project-summary.filters')
                @include('bct/projects::lists.tabs.projects.grid.project-summary.no_results')

        </div>

        <div class="col-md-6">
            <span class="navbar-brand">{{ trans("bct/projects::users/common.title_reps") }}
                <span class="text14">({{ $grid_reps_count }} of {{ $reps_count }})</span></span>
            @if (!$currentUser->inRole('pp-guest'))
            <a data-id="#reps" id="reps_tab" name="reps_tab" class="btn btn-primary pull-right go-to-tab hidden-print"
               data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
                        class="fa fa-edit fa-fw"></i></a>
            @endif
            <table id="data-grid-reps-summary" class="table table-striped table-bordered"
                       data-source="{{ route('bct.projects.lists.gridForReps', [$list->id, 'limit' => 5]) }}"
                       data-grid="reps-summary">
                <thead>

                <tr>
                    <td class="td-label col-md-6 text-center">{{ trans('bct/projects::users/model.general.name') }}</td>
                    <td class="td-label col-md-6 text-center">{{ trans('bct/projects::users/model.general.group') }}</td>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

            @include('bct/projects::lists.tabs.reps.grid.reps-summary.results')
            @include('bct/projects::lists.tabs.reps.grid.reps-summary.pagination')
            @include('bct/projects::lists.tabs.reps.grid.reps-summary.filters')
            @include('bct/projects::lists.tabs.reps.grid.reps-summary.no_results')

        </div>
    </div>

</div>