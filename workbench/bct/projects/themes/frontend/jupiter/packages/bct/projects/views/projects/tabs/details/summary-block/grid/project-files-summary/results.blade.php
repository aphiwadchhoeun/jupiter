<script type="text/template" data-grid="project-files-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td>
            <a href="<%- r.view_uri %>" target="_blank" class="hidden-print"><%- r.name %></a>
            <span class="visible-print"><%- r.name %></span>
        </td>
        <td><%- r['created'] %></td>
    </tr>

    <% }); %>

</script>
