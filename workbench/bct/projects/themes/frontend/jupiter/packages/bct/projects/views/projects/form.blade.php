

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">
                {{ trans_choice('bct/projects::projects/common.add_to_list_number', $number, [ 'number' => ($number ? $number : 'selected') ]) }}
            </h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" data-refresh="lists">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>

                            <div class="row">

                                {{--add more form-group for other fields, just keep the same uniform--}}
                                <div class="form-group">

                                    <label for="group_type" class="control-label">
                                        {{{ trans('bct/projects::groups/model.general.type') }}}
                                    </label>

                                    <select class="form-control" name="group_type" id="group_type">
                                        <option selected >{{trans('bct/projects::groups/model.general.group_type_help')}}</option>
                                        <option value="0">{{trans('bct/projects::groups/model.general.group')}}</option>
                                        <option value="1">{{trans('bct/projects::groups/model.general.individual')}}</option>
                                    </select>

                                    <label for="group_id" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.group_id') }}}
                                    </label>

                                    <select disabled class="form-control" name="group_id" id="group_id" required>
                                        <option value selected="true">{{{ trans('bct/projects::projects/model.general.group_id_help') }}}</option>
                                    </select>
                                    
                                    
                                    <label  for="list_id" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.list_id') }}}
                                    </label>

                                    <select disabled class="form-control" name="list_id" id="list_id" required>
                                        <option value="">{{{ trans('bct/projects::projects/model.general.list_id_help') }}}</option>
                                    </select>

                                </div>

                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="add-to-list" class="btn btn-success" data-toggle="tooltip"
                    data-original-title="{{ trans('bct/projects::projects/common.add_to_list') }}">
                {{{ trans('bct/projects::projects/common.add_to_list') }}}
            </button>
            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>
