<script type="text/template" data-grid="stewards" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.full_name %></td>
        <td><%- r.phone %></td>
        <td><a href="mailto:<%- r.email %>"><%- r.email %></a></td>
        <td><%- r.local %></td>
        <td><a href="<%- r.view_uri %>"
               data-toggle="tooltip" class="btn btn-success btn-circle add-confirm" data-placement="right"
               data-original-title="{{{ trans('bct/projects::projects/common.connect') }}}"><i
                        class="fa fa-plus fa-fw"></i></a></td>
    </tr>

    <% }); %>

</script>
