/**
 * Created by Aphiwad on 10/28/2015.
 */
var projectActionsHistorySummary;

if (!projectActionsHistorySummary) {
    var options = {
        events: {
            'fetching': function () {
                $('#actions-counter').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            'fetched': function (result) {
                var num = (result.pagination.filtered > 5) ? 5 : result.pagination.filtered;
                $('#actions-counter').text('(' + num + ' of ' + result.pagination.total + ')');
            }
        }
    };

    projectActionsHistorySummary = $.datagrid(
        'project-actions-summary',
        '#project-actions-summary',
        'null',
        'null',
        options);
}
