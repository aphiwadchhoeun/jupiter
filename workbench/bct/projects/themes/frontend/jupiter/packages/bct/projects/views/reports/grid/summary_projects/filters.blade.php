<script type="text/template" data-grid="summary_projects" data-template="filters">

	<% _.each(filters, function(f) { %>

		<button class="btn btn-default">

			<% if (f.from !== undefined && f.to !== undefined) { %>

				<% if (/[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.from) && /[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.to)) { %>

					<%- f.label %> <em><%- moment(f.from).format('MMM DD, YYYY') %> - <%- moment(f.to).format('MMM DD, YYYY') %></em>

				<% } else { %>

					<%- f.label %> <em><%- f.from %> - <%- f.to %></em>

				<% } %>

			<% } else if (f.col_mask !== undefined && f.val_mask !== undefined) { %>

				<%- f.col_mask %> <em><%- f.val_mask %></em>

			<% } else { %>

				<% if (f.column === 'all') { %>

					<%- f.value %>

				<% } else if (f.column == 'z_projects.name') { %>

					<%- f.value %> in <em>Project Name</em>

				<% } else { %>

					<%- f.value %> in <em><%- f.column.capitalize() %></em>

				<% } %>

			<% } %>

			&nbsp;<span><i class="fa fa-times-circle"></i></span>

		</button>

	<% }); %>

</script>
<script type="text/template" data-grid="summary_projects" data-template="filters_custom">

	<% _.each(filters, function(f) { %>

	<button class="btn btn-default" data-column="<%- f.column %>">

		<% if (f.from !== undefined && f.to !== undefined) {  %>

		<% if (/[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.from) && /[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.to)) { %>

		{{ trans('bct/projects::reports/common.visit_date') }}: <%- f.label %> <em><%- moment(f.from).format('MMM DD, YYYY') %> - <%- moment(f.to).format('MMM DD, YYYY') %></em>

		<% } else if (f.column == 'active') { %>

		All

		<% } else { %>

		<%- f.label %> <em><%- f.from %> - <%- f.to %></em>

		<% } %>

		<% } else if (f.col_mask !== undefined && f.val_mask !== undefined) { %>

		<%- f.col_mask %> <em><%- f.val_mask %></em>

		<% } else { %>

		<% if (f.column === 'all') { %>

		<% } else if (f.column == 'status') { %>

		<%- f.value.capitalize() %>

		<% } else { %>

		<%- f.value %> in <em><%- f.field_text %></em>

		<% } %>

		<% } %>

		<span><i class="fa fa-times-circle"></i></span>

	</button>

	<% }); %>

</script>

