var checkedRows = [], limit = 8;

jQuery('document').ready(function ($) {

    $('#submit-btn').on('click', function (e) {
        e.preventDefault();
        var url = $(this).data('submit-href');
        var source = $(this);

        source.prop('disabled', true);
        source.find('i.fa').removeClass('fa-map-marker')
            .addClass('fa-spinner fa-spin');

        $.post(url, {
            'entries': checkedRows
        }).done(function (data) {
            window.location = data;
        })
        .fail(function () {
            source.removeAttr('disabled');
            source.find('i.fa').removeClass('fa-spinner fa-spin')
                .addClass('fa-map-marker');
        });
    });

    $('[data-modal]').click(function (e) {
        e.preventDefault();
        var source = $(this);
        var $modal = $('#modal-form');

        source.prop('disabled', true);
        source.find('i.fa').removeClass('fa-plus')
            .addClass('fa-spinner fa-spin');
        $modal.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
            source.removeAttr('disabled');
            source.find('i.fa').removeClass('fa-spinner fa-spin')
                .addClass('fa-plus');

            if (response) {
                var dueDate = $('#due_date');
                /** datepicker */
                dueDate.parents('.date').datepicker({
                    format: 'M-dd-yyyy',
                    assumeNearbyYear: true,
                    showOnFocus: false
                });
                $modal.modal('show');

                var submit_btn_modal = $('#submit-btn-modal');
                var delete_btn_modal = $('#delete-btn-modal');
                var form_modal = $('#content-form-modal');

                if (form_modal) {
                    form_modal.on('submit', function (e) {
                        e.preventDefault();
                    });

                    form_modal.on('keypress', function (e) {

                        var keycode = e.which || e.keyCode;

                        if (keycode == 13) {
                            e.preventDefault();
                        }
                    });

                    if (form_modal.parsley()) {
                        form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
                            if (formInstance.isValid() === true) {
                                $.post(
                                    form_modal.attr('action'),
                                    form_modal.serialize(),
                                    function (data) {
                                        $modal.modal('hide');
                                        location.reload();
                                    }
                                );
                            }
                        });

                        submit_btn_modal.on('click', function () {
                            form_modal.parsley().validate();
                        });
                    }

                    if (delete_btn_modal) {
                        delete_btn_modal.on('click', function () {
                            var url = $(this).data('url');
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {},
                                success: function (response) {
                                    $modal.modal('hide');
                                    Extension.Index.Grid.refresh();
                                }
                            });
                        });
                    }
                }
            } else {
                location.reload();
            }

        });
    });

});

function initSearchGrid(checkbox) {
    var url = checkbox.parents('[data-grid-row]').data('url');

    if (checkbox.prop('checked')) {
        $('.projects-grid').fadeIn();

        if (typeof projectsGrid == 'undefined') {
            projectsGrid = $.datagrid('projects', '#project-data-grid', '.data-grid-projects_pagination', '#data-grid-projects_applied', {
                source: url,
                hash: false,
                callback: function () {
                    for (var i in checkedRows) {
                        $('[data-grid="projects"] [data-checkbox-id="' + checkedRows[i] + '"]').prop('checked', true);
                    }
                    checkProjectsAvailable();
                }
            });
        } else {
            projectsGrid.source = url;
            projectsGrid.refresh();
        }
    } else {
        $('.projects-grid').fadeOut();
    }
}

function checkProjectsAvailable() {
    /** show button for map */
    if (checkedRows.length > 0) {
        $('#map-selected').fadeIn();
    } else {
        $('#map-selected').fadeOut();
    }

    /** disable/enable for limit projects */
    if (checkedRows.length >= limit) {
        $('[data-grid="projects"] [data-grid-checkbox]:not(:checked)').attr('disabled', true);
    } else {
        $('[data-grid="projects"] [data-grid-checkbox]:not(:checked)').attr('disabled', false);
    }
}

var Extension, projectsGrid;

;
(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'lists-search'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.datePicker(Extension);
        Extension.Index.datePickerProjects();
        Extension.Index.dataGrid();
        Extension.Index.listeners();
        Extension.Index.datePickerProjects.on('show.daterangepicker',function(){
            showDirectionPP('Due');
        });
        Extension.Index.datePicker.on('show.daterangepicker',function(){
            showDirectionPP('Due');
        });
        initSearchGrid($('[data-grid="lists-search"] [data-grid-checkbox]:checked'));

    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid="lists-search"] [data-grid-row]', Extension.Index.checkRow)
            .on('click', '[data-grid="lists-search"] [data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid="lists-search"] [data-grid-checkbox]', Extension.Index.checkboxes)
            .on('click', '[data-grid="projects"] [data-grid-checkbox]', Extension.Index.checkboxesProjects)
            .on('click', '[data-grid="projects"] [data-grid-row]', Extension.Index.checkRowProjects)
        ;
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxes = function (event) {
        event.stopPropagation();

        $('[data-grid="lists-search"] [data-grid-checkbox]').not(this).prop('checked', false);

        $('[data-grid="lists-search"] [data-grid-row]').not(this).toggleClass('active', false);

        $(this).parents('[data-grid-row]').toggleClass('active');

        initSearchGrid($(this));
    };

    // Handle Data Grid row checking
    Extension.Index.checkRow = function () {
        var checkbox = $(this).find('[data-grid-checkbox]');

        $('[data-grid="lists-search"] [data-grid-checkbox]').not(checkbox).prop('checked', false);

        $('[data-grid="lists-search"] [data-grid-row]').not(checkbox).toggleClass('active', false);

        $(this).toggleClass('active');

        checkbox.prop('checked', !checkbox.prop('checked'));

        initSearchGrid(checkbox);
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxesProjects = function (event) {
        event.stopPropagation();

        if ($(this).is(':disabled')) return false;

        $(this).parents('[data-grid-row]').toggleClass('active');

        rememberChecked($(this));

        checkProjectsAvailable();
    };

    // Handle Data Grid row checking
    Extension.Index.checkRowProjects = function () {
        var checkbox = $(this).find('[data-grid-checkbox]');

        if (checkbox.is(':disabled')) return false;

        $(this).toggleClass('active');

        checkbox.prop('checked', !checkbox.prop('checked'));

        rememberChecked(checkbox);

        checkProjectsAvailable();
    };

    function rememberChecked(checkbox) {
        if (checkbox.prop('checked')) {
            if (checkedRows.indexOf(checkbox.data('checkbox-id')) == -1) {
                checkedRows.push(checkbox.data('checkbox-id'));
            }
        } else {
            var ind = checkedRows.indexOf(checkbox.data('checkbox-id'));
            if (ind != -1) {
                checkedRows.splice(ind, 1);
            }
        }
    }

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    // Date range picker initialization
    Extension.Index.datePickerProjects = function() {
        var startDate, endDate, config, filter;

        var filters = _.compact(
            String(window.location.hash.slice(3)).split('/').splice(2)
        );

        config = {
            opens: 'right'
        };

        _.each(filters, function(route) {
            filter = route.split(':');

            if (filter[0] === 'created_at' && filter[1] !== undefined && filter[2] !== undefined) {
                startDate = moment(filter[1]);

                endDate = moment(filter[2]);
            }
        });

        if (startDate && endDate) {
            config = {
                startDate: startDate,
                endDate: endDate,
                opens: 'right'
            };
        }

        Extension.Index.datePickerProjects = $('[data-grid="projects"] [data-grid-calendar]').daterangepicker(config, function(start, end, label) {
            $('[data-grid="projects"] [data-range-start]').trigger('change');
        });

        Extension.Index.datePickerProjects.on('apply.daterangepicker', function(ev, picker) {
            $('[data-grid="projects"] [data-range-start]').val(picker.startDate.format('YYYY-MM-DD'));
            $('[data-grid="projects"] [data-range-end]').val(picker.endDate.format('YYYY-MM-DD'));
            $('[data-grid="projects"] [data-range-start]').trigger('change');
        });
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            events: {
                sorted: function(response) {
                    if (response.pagination.total== 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function(response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container' ).removeClass('hide');
                    }
                },
            },
            callback: function () {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

                initSearchGrid($('[data-grid="lists-search"] [data-grid-checkbox]:checked'));
            }
        };

        Extension.Index.Grid = $.datagrid('lists-search', '#list-data-grid', '.data-grid-lists_pagination', '#data-grid-lists_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
