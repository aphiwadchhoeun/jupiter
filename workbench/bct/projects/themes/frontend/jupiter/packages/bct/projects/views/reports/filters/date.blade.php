<div class="input-group select-width">

    <span class="input-group-btn">

        <button class="btn btn-default" type="button" disabled>
            {{ trans('bct/projects::reports/common.visit_date') }}
        </button>

        <button class="btn btn-default hidden-xs" type="button" data-grid-calendar
                data-range-filter="z_project_visit.created_at">
            <i class="fa fa-calendar"></i>
        </button>

        <button class="btn btn-default" type="button"
                data-filter-not-refresh
                data-grid-calendar-preset="last30days">
            {{ trans('bct/projects::reports/common.last_30_days') }}
        </button>

         <button class="btn btn-default" type="button"
                 data-grid-calendar-preset="last90days">
            {{ trans('bct/projects::reports/common.last_90_days') }}
        </button>

    </span>

</div>