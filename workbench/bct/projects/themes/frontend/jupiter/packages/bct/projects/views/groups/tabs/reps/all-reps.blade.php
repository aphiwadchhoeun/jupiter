<section class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <div class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-contractors-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans("bct/projects::users/common.available_reps") }}</span>
                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-contractors-actions">

                    <ul class="nav navbar-nav navbar-left download-container" data-grid="users">

                        <li class="dropdown">
                            <a href="#"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="users" role="form">

                        <div class="input-group">

                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" disabled>
                                    {{{ trans('common.status') }}}
                                </button>

                                <button class="btn btn-default dropdown-toggle" type="button"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

                                <ul class="dropdown-menu" role="menu" data-filter-reset>
                                    <li>
                                        <a href="#" data-filter="z_user_profiles.status:1">
                                            <i class="fa fa-check-circle"></i> {{trans('bct/projects::general/common.active')}}
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" data-filter="z_user_profiles.status:0">
                                            <i class="fa fa-times-circle"></i> {{trans('bct/projects::general/common.disabled')}}
                                        </a>
                                    </li>
                                </ul>
                            </span>

                            <span class="input-group-btn">
                                <select name="column" class="form-control">
                                    <option value="users.full_name">{{ trans('bct/projects::users/model.general.name') }}
                                    </option>
                                    <option value="users.email">{{ trans('bct/projects::users/model.general.email') }}
                                    </option>
                                </select>
                            </span>

                            <input class="form-control" name="filter" type="text"
                                   placeholder="{{{ trans('common.search') }}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="users" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>
                                </span>
                        </div>

                    </form>

                </div>

            </div>

        </nav>

    </div>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

            <div id="data-grid_users_all_applied" class="btn-group" data-grid="users"></div>

        </div>


        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="data-grid_users_all" class="table table-hover"
                   data-source="{{ route('bct.projects.groups.gridUsers', $group->id) }}"
                   data-filename="{{ trans("bct/projects::users/common.available_reps") }}"
                   data-grid="users">
                <thead>
                <tr>
                    <th class="sortable" data-sort="full_name">{{ trans('bct/projects::users/model.general.name') }}</th>
                    <th class="sortable" data-sort="email">{{ trans('bct/projects::users/model.general.email') }}</th>
                    <th>{{ trans('bct/projects::users/model.general.role') }}</th>
                    <th class="sortable" data-sort="status">{{ trans('bct/projects::users/model.general.status') }}</th>
                    <th class="hidden-print">{{{ trans('bct/projects::general/common.actions') }}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>


    <footer class="panel-footer clearfix">

        {{-- Grid: Pagination --}}
        <div class="data-grid_users_all_pagination" data-grid="users"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::groups.grid.users.results')
    @include('bct/projects::groups.grid.users.pagination')
    @include('bct/projects::groups.grid.users.filters')
    @include('bct/projects::groups.grid.users.no_results')

</section>