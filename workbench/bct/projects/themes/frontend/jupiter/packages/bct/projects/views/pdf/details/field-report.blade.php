<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        {{ Asset::queue('normalize', 'bct/projects::custom/css/normalize.css') }}
        {{ Asset::queue('bootstrap', 'bootstrap/css/bootstrap.min.css') }}

        {{-- Compiled styles --}}
        @foreach (Asset::getCompiledStyles() as $style)
            <link href="{{ $style }}" rel="stylesheet">
        @endforeach

        <style>
            * {
                font-size: 12pt;
                line-height: 16pt !important;
                letter-spacing: -1pt !important;
                font-size-adjust: 0.58 !important;
            }

            .margin-minus-25 {
                margin-left: -25px;
            }

            .td-label {
                text-transform: uppercase;
                font-weight: bold;
                white-space: nowrap;
            }

            .td-value {
                width: 100%;
                border-bottom: 0.5px solid #000;
                padding-left: 5px;
            }

            .legend-text {
                font-style: italic;
                color: #767676;
            }

            .text-uppercase {
                text-transform: uppercase;
            }

            .pad-10 {
                padding-left: 10px;
                padding-right: 10px;
            }

            .panel-default > .panel-heading {
                background-color: #f5f5f5;
            }

            .panel {
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
                border: 1px solid #000;
            }

            .panel-body {
                padding: 4px;
                border-top: 1px solid #000;
            }

            .panel-heading {
                text-transform: uppercase;
                font-weight: bold;
            }

            .page-break {
                page-break-before: always;
            }

            .table-bordered,
            .table-bordered tr th,
            .table-bordered tr td {
                border-color: #000 !important;
            }

            .table-bordered tr th {
                border-bottom: none !important;
            }

            .table > tbody > tr > td,
            .table > thead > tr > th {
                padding: 0px;
            }

            div.row {
                margin: 0px;
            }

            .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
                padding: 0px;
            }

            .square-box {
                display: inline-block;
                width: 12pt;
                height: 12pt;
                border: 1px solid #000;
                color: #000;
            }

            .square-box-check:after {
                content: "";
                display: block;
                margin-left: 4px;
                margin-top: 2px;
                width: 5pt;
                height: 7pt;
                border-bottom: 3px solid #000;
                border-right: 3px solid #000;
                transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                -webkit-transform: rotate(45deg);
            }

        </style>

    </head>
    <body>

        {{--Body--}}
        <div class="ext-profiles page-break">

            <br>

            <div class="row">

                <div class="col-xs-12">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.project') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'name', ''), 75) }} |
                                ID: {{ data_get($project, 'id', '') }}</td>
                        </tr>
                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.address') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'address', ''), 85) }}</td>
                        </tr>
                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-4">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.city') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'city', ''), 18) }}</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-4">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.state') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'state', ''), 18) }}</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-4">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.zipcode') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'zipcode', ''), 18) }}</td>
                        </tr>
                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-4">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.county') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'county', ''), 18) }}</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-4">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.region') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'region_abrv', ''), 18) }}</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-4">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.created') }}:</td>
                            <td class="td-value">{{ \Carbon\Carbon::parse(data_get($project, 'created_at', null))->format('M-d-Y') }}</td>
                        </tr>
                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.tags') }}:</td>
                            <td class="td-value">{{ str_limit($project->tags->implode('name', ', '), 80) }}</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-12">
                    <span class="legend-text">{{ implode($tags, ' - ') }}</span>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.description') }}:
                            </td>
                            <td class="td-value">{{ str_limit(data_get($project, 'description', ''), 80) }}</td>
                        </tr>
                    </table>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="col-xs-6">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/common.general_contractor') }}:
                            </td>
                            <td class="td-value">{{ str_limit(data_get($project->getGC(), 'name', ''), 40) }}</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-2">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.is_class_a') }}
                                :
                            </td>
                            <td>&nbsp;
                                @if($project->is_class_a)
                                    <div class="square-box square-box-check"></div>
                                @else
                                    <div class="square-box"></div>
                                @endif
                            </td>
                        </tr>
                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-6">

                    <div class="row">

                        <div class="col-xs-12">

                            <table>
                                <tr>
                                    <td class="td-label">{{ trans('bct/projects::projects/model.general.classification_id') }}
                                        :
                                    </td>
                                    <td class="td-value">{{ str_limit(data_get($project, 'classification.name', ''), 18) }}</td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <span class="legend-text">{{ implode($classifications, ' - ') }}</span>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">

                            <table>
                                <tr>
                                    <td class="td-label">{{ trans('bct/projects::projects/model.general.job_type') }}
                                        :
                                    </td>
                                    <td class="td-value">{{ str_limit(data_get($project, 'type.type', ''), 35) }}</td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <span class="legend-text">{{ implode($job_types, ' - ') }}</span>
                        </div>

                    </div>

                </div>

                <div class="col-xs-6">

                    <div class="row">

                        <div class="col-xs-12">

                            <table>
                                <tr>
                                    <td class="td-label">{{ trans('bct/projects::projects/model.general.prevailing_wage_id') }}
                                        :
                                    </td>
                                    <td class="td-value">{{ str_limit(data_get($project, 'prevailingWage.name', ''), 18) }}</td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <span class="legend-text">{{ implode($prevailing_wages, ' - ') }}</span>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">

                            <table>
                                <tr>
                                    <td class="td-label">{{ trans('bct/projects::projects/model.general.visibility_id') }}
                                        :
                                    </td>
                                    <td class="td-value">{{ str_limit(data_get($project, 'visibility.name', ''), 18) }}</td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <span class="legend-text">{{ implode($visibilities, ' - ') }}</span>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">

                            <table>
                                <tr>
                                    <td class="td-label">{{ trans('bct/projects::projects/model.general.reference') }}:
                                    </td>
                                    <td class="td-value">{{ str_limit(data_get($project, 'reference_details', ''), 35) }}</td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">

                            <table>
                                <tr>
                                    <td class="td-label">{{ trans('bct/projects::projects/model.general.value_id') }}:
                                    </td>
                                    <td class="td-value">{{ str_limit(data_get($project->value, 'value', ''), 35) }}</td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">

                            <table>
                                <tr>
                                    <td class="td-label">{{ trans('bct/projects::projects/model.general.status_id') }}:
                                    </td>
                                    <td class="td-value">{{ str_limit(data_get($project->status, 'name', ''), 35) }}</td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <span class="legend-text">{{ implode($statuses, ' - ') }}</span>
                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div id="visit-panel" class="panel panel-default">

                        <div class="panel-heading text-center text-bold">{{ trans('bct/projects::projects/common.visit_notes') }}</div>

                        <div class="panel-body">

                            <div class="row">

                                <div class="col-xs-4">
                                    <strong class="text-uppercase">{{ trans('bct/projects::projects/common.last_visit_date') }}
                                        :</strong>
                                    <span>{{ ($project->getLastVisit())?\Carbon\Carbon::parse(data_get($project->getLastVisit(), 'created_at', null))->format('M-d-Y'):'' }}</span>
                                </div>

                                <div class="col-xs-3">
                                    <strong class="text-uppercase">{{ trans('bct/projects::projects/common.rep') }}
                                        :</strong>
                                    <span>{{ str_limit(($project->getLastVisit() && !$project->getLastVisit()->users->isEmpty())?$project->getLastVisit()->users()->first()->fullName:'', 13) }}</span>
                                </div>

                                <div class="col-xs-5">
                                    <strong class="text-uppercase">{{ trans('bct/projects::projects/common.add_reps') }}
                                        :</strong>
                                    <span>{{ str_limit( ($project->getLastVisit() && !$project->getLastVisit()->users->isEmpty()) ? $project->getLastVisit()->users->implode('fullName', ' - ') : '', 25) }}</span>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12">
                                    <strong class="text-uppercase">{{ trans('bct/projects::projects/common.followup_reason') }}
                                        :</strong>
                                    <span>{{ str_limit(data_get($project->getLastVisit(), 'reason', ''), 75) }}</span>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12" style="min-height: 180px; overflow: hidden; text-overflow: ellipsis;">
                                    <strong class="text-uppercase">{{ trans('bct/projects::visits/model.general.notes') }}
                                        :</strong>
                                    <span>{{ str_limit(data_get($project->getLastVisit(), 'details', ''), 930) }}</span>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12" style="min-height: 50px">
                                    <strong class="text-uppercase">{{ trans('bct/projects::projects/common.stages') }}
                                        :</strong>
                                    <span>{{ str_limit(($project->getLastVisit())?$project->getLastVisit()->stages->implode('name', ', '):'', 190) }}</span>
                                </div>

                            </div>

                        </div>

                        <div class="panel-body">

                            <div class="row">

                                <div class="col-xs-12">
                                    <strong class="text-uppercase">{{ trans('bct/projects::projects/common.followup_date') }}
                                        :</strong>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12">
                                    <strong class="text-uppercase">{{ trans('bct/projects::visits/model.general.notes') }}
                                        :</strong>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12" style="height: 150px;">&nbsp;</div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12">
                                    <strong class="text-uppercase">{{ trans('bct/projects::projects/common.current_stages') }}
                                        : ({{ trans('bct/projects::projects/common.circle_stages') }})</strong>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-12">
                                    <span class="legend-text">{{ implode($stages, ' - ') }}</span>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>


            <div class="row">

                <div class="col-xs-3">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/common.visit_date') }}:
                            </td>
                            <td class="td-value">&nbsp;</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-3">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/common.rep') }}:
                            </td>
                            <td class="td-value">&nbsp;</td>
                        </tr>
                    </table>

                </div>

                <div class="col-xs-6">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/common.add_reps') }}:
                            </td>
                            <td class="td-value">&nbsp;</td>
                        </tr>
                    </table>

                </div>

            </div>

            {{-- New Page Break --}}
            <div class="row page-break">

                <div class="col-xs-12">

                    <table>
                        <tr>
                            <td class="td-label">{{ trans('bct/projects::projects/model.general.project') }}:</td>
                            <td class="td-value">{{ str_limit(data_get($project, 'name', ''), 75) }} |
                                ID: {{ data_get($project, 'id', '') }}</td>
                        </tr>
                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <table id="contractor-info-table" class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th colspan="4"
                                class="text-center text-uppercase">{{ trans('bct/projects::projects/common.contractor_info') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="col-xs-3 td-label">{{ trans('bct/projects::projects/common.scope') }}</td>
                            <td class="td-label">{{ trans('bct/projects::projects/common.contractor') }}</td>
                            <td class="td-label">{{ trans('bct/projects::projects/common.license') }}</td>
                            <td class="col-xs-2 td-label">{{ trans('bct/projects::projects/common.status') }}</td>
                        </tr>
                        @foreach($scopes as $scope)
                            <?php $c = $contractors->where('scope', $scope['id'])->first(); ?>
                            <tr>
                                <td>{{ str_limit(array_get($scope, 'val', ''), 23) }}</td>
                                <td>
                                    @if (!is_null($c) && !is_null(data_get($c, 'name')))
                                        {{ str_limit(data_get($c, 'name', ''), 33) }}
                                    @elseif (!is_null($c) && is_null(data_get($c, 'name')))
                                        {{ trans('bct/projects::general/common.na') }}
                                    @endif
                                </td>
                                <td>
                                    @if (!is_null($c))
                                        {{ str_limit(data_get($c, 'license', ''), 18) }}
                                    @endif
                                </td>
                                <td>
                                    <span class="pad-10">
                                        @if($c && $c->is_union)
                                            <div class="square-box square-box-check"></div>
                                        @else
                                            <div class="square-box"></div>
                                        @endif
                                        U
                                    </span>
                                    <span class="pad-10">
                                        @if($c && $c->is_agreement)
                                            <div class="square-box square-box-check"></div>
                                        @else
                                            <div class="square-box"></div>
                                        @endif
                                        PA
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                        @for($i = 0; $i<4;$i++)
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>
                                    <span class="pad-10">
                                        <div class="square-box"></div>
                                        U
                                    </span>
                                    <span class="pad-10">
                                        <div class="square-box"></div>
                                        PA
                                    </span>
                                </td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <table id="contact-info-table" class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th colspan="4"
                                class="text-center text-uppercase">{{ trans('bct/projects::projects/common.contact_info') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="col-xs-3 td-label">{{ trans('bct/projects::projects/common.type') }}</td>
                            <td class="td-label">{{ trans('bct/projects::projects/common.company') }}</td>
                            <td class="td-label">{{ trans('bct/projects::projects/common.individual') }}</td>
                            <td class="td-label">{{ trans('bct/projects::projects/common.phone') }}</td>
                        </tr>
                        @foreach($contact_types as $type)
                            <?php $ct = $contacts->where('type', $type['id'])->first() ?>
                            <tr>
                                <td>{{ str_limit(array_get($type, 'val', ''), 23) }}</td>
                                <td>{{ str_limit(data_get($ct, 'company', ''), 33) }}</td>
                                <td>{{ str_limit(data_get($ct, 'contact', ''), 18) }}</td>
                                <td>{{ str_limit(data_get($ct, 'phone', ''), 12) }}</td>
                            </tr>
                        @endforeach
                        @for($i = 0; $i<4;$i++)
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        @endfor
                        </tbody>
                    </table>

                </div>

            </div>

        </div>

    </body>
</html>
