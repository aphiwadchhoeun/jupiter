<script type="text/template" data-grid="project-actions-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.type %></td>
        <td><%- r.contractor %></td>
        <td><%- r.details %></td>
        <td><%- r['created'] %></td>
    </tr>

    <% }); %>

</script>
