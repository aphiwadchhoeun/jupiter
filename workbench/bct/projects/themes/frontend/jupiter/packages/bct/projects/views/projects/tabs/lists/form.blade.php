<div class="modal-dialog">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">{{{ trans('bct/projects::projects/common.lists_' . $mode) }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="
                @if(isset($pivot))
                    {{ route('bct.projects.projects.editLists', [$project->id, $pivot->id]) }}
                @else
                    {{ route('bct.projects.projects.createLists', [$project->id]) }}
                @endif
                " data-refresh="lists">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(isset($pivot))
                    <input type="hidden" name="pivot_id" value="{{ $pivot->id }}">
                @endif

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>

                            <div class="row">

                                {{--add more form-group for other fields, just keep the same uniform--}}
                                <div class="form-group">

                                    <label for="list_id" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.list_id') }}}
                                    </label>

                                    <select class="form-control" name="list_id" id="list_id" required>
                                        <option value="">{{{ trans('bct/projects::projects/model.general.list_id_help') }}}</option>
                                        @foreach($lists as $list)
                                            <option value="{{ $list->id }}" @if(isset($pivot) && $pivot->list_id == $list->id) selected @endif
                                                    >{{ $list->name }}</option>
                                        @endforeach
                                    </select>

                                </div>

                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="submit-btn-modal" name="submit-btn-modal"class="btn btn-primary">
                {{{ trans('action.save') }}}</button>

            @if(isset($pivot))
                <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-action="delete"
                        data-url="{{ route('bct.projects.projects.deleteContractor', [
                                'id' => $project->id,
                                'pivot_id' => $pivot->id,
                            ]) }}">
                    {{{ trans('action.delete') }}}</button>
            @endif

            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
