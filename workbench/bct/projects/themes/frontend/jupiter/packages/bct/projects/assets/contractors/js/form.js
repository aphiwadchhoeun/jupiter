/**
 * Created by Aphiwad on 4/17/2015.
 */
$('input').on('keypress', function () {
    $(this).parents('.form-group').removeClass('has-error');
    $(this).parent().find('.help-block').text('');

});

$('body #primary-type').on('click', function () {
    var parent = $(this).parent().parent();
    parent.find('#other-type-input').prop('disabled', true);
    parent.find('#other-type-input').prop("required", false);
    parent.find('#other-type-input').parents('.form-group').removeClass('has-error');

});
$('body #other-type').on('click', function () {
    var parent = $(this).parent().parent();
    parent.find('#other-type-input').prop("disabled", false);
    parent.find('#other-type-input').prop("required", true);
});

jQuery('document').ready(function ($) {
    var statusControl = $('#is_union');
    var activeControl = $('#active');

    // init status switch control
    statusControl.bootstrapToggle({
        onstyle: 'success',
        offstyle: 'danger',
        on: 'Union',
        off: 'NonUnion'
    });

    activeControl.bootstrapToggle({
        onstyle: 'success',
        offstyle: 'danger',
        on: 'Yes',
        off: 'No'
    });

    function toggleOtherType(form, enable) {
        form.find('#other-type').prop('disabled', !enable);
    }

    function toggleDelete(footer, enable) {
        if(enable) {
            footer.find('button[name="delete-btn"]').removeClass('hide');
        } else {
            footer.find('button[name="delete-btn"]').addClass('hide');
        }
    }

    function resetFormValidation(form) {
        form.find('.form-group').removeClass('has-error');
        form.find('.help-block').html('');
    }

    $('input[name="phone"]').mask('(999) 999-9999? x999');
    $('input[name="fax"]').mask('(999) 999-9999');
    $('input[name="ein"]').mask('99-9999999');
    $('input[name="zipcode"]').mask('99999?-9999');
    $('input[name="license"]').mask('?************');

    var contact_type_typeahead = $('#other-type-input');
    var $address_modal = $('#address-form');
    var $phone_modal = $('#phone-form');
    var $email_modal = $('#email-form');
    var $notes_modal = $('#notes-form');
    var $company_modal = $('#company-form');
    var $license_modal = $('#license-form');

    var baseurl = window.location.protocol + '//' + window.location.hostname + '/';

    $('#state').on('keypress', function (event) {
        if (null !== String.fromCharCode(event.which).match(/[a-z]/g)) {
            event.preventDefault();
            if ($(this).val().length < 2) {
                $(this).val($(this).val() + String.fromCharCode(event.which).toUpperCase());
            }
        }
    });


    $('#address-new').click(function () {
        $address_modal.removeClass('update').addClass('create');
        $address_modal.modal('show');
        var form = $address_modal.find('form');
        resetFormValidation(form);
        toggleOtherType(form, true);
    });

    $('#phone-new').click(function () {
        $phone_modal.removeClass('update').addClass('create');
        $phone_modal.modal('show');
        var form = $phone_modal.find('form');
        resetFormValidation(form);
        toggleOtherType(form, true);
    });

    $('#email-new').click(function () {
        $email_modal.removeClass('update').addClass('create');
        $email_modal.modal('show');
        var form = $email_modal.find('form');
        resetFormValidation(form);
        toggleOtherType(form, true);
    });

    $('#notes-new').click(function () {
        $notes_modal.removeClass('update').addClass('create');
        $notes_modal.modal('show');
        var form = $notes_modal.find('form');
        resetFormValidation(form);
        toggleOtherType(form, true);
    });

    $('#license-new').click(function () {
        $license_modal.removeClass('update').addClass('create');
        $license_modal.modal('show');
        var form = $license_modal.find('form');
        resetFormValidation(form);
        toggleOtherType(form, true);
    });

    $('#company-new').click(function () {
        $company_modal.removeClass('update').addClass('create');
        $company_modal.modal('show');
        var form = $company_modal.find('form');
        resetFormValidation(form);
        toggleOtherType(form, true);
    });

    $('.address-update').click(function (e) {
        e.preventDefault();
        $address_modal.removeClass('create').addClass('update');
        $address_modal.modal('show');
        var form = $address_modal.find('form');
        resetFormValidation(form);
        $address_modal.find('#address-submit-btn').attr('address_id', $(this).data('address_id'))
        $address_modal.find('#address-delete-btn').attr('address_id', $(this).data('address_id'))
        $address_modal.find('#address').val($(this).data('address'));
        $address_modal.find('#city').val($(this).data('city'));
        $address_modal.find('#state').val($(this).data('state'));
        $address_modal.find('#zipcode').val($(this).data('zipcode'));
        var type = $(this).data('type');
        if (type == 'Primary') {
            form.find('#primary-type').trigger('click');
        }
        else {
            form.find('#other-type').trigger('click');

            form.find('#other-type-input').val(type);
        }


    });

    $('.phone-update').click(function (e) {
        e.preventDefault();
        $('#phone-submit-btn').attr('phone_id', $(this).data('phone_id'))
        $('#phone-delete-btn').attr('phone_id', $(this).data('phone_id'))
        $phone_modal.removeClass('create').addClass('update')
        $phone_modal.modal('show');
        $phone_modal.find('#phone').val($(this).data('phone'));
        var form = $phone_modal.find('form');
        resetFormValidation(form);
        var type = $(this).data('type');
        if (type == 'Primary') {
            toggleOtherType(form, false);
            toggleDelete($phone_modal, false);
            form.find('#primary-type').trigger('click');
        }
        else {
            toggleOtherType(form, true);
            toggleDelete($phone_modal.find('.modal-footer'), true);
            form.find('#other-type').trigger('click');
            form.find('#other-type-input').val(type);
        }

    });

    $('.email-update').click(function (e) {
        e.preventDefault();
        $('#email-submit-btn').attr('email_id', $(this).data('email_id'))
        $('#email-delete-btn').attr('email_id', $(this).data('email_id'))
        $email_modal.find('#email').val($(this).data('email'));
        $email_modal.removeClass('create').addClass('update')
        $email_modal.modal('show');
        var form = $email_modal.find('form');
        resetFormValidation(form);
        var type = $(this).data('type');
        if (type == 'Primary') {
            toggleOtherType(form, false);
            toggleDelete($email_modal, false);
            form.find('#primary-type').trigger('click');
        }
        else {
            toggleOtherType(form, true);
            toggleDelete($email_modal.find('.modal-footer'), true);
            form.find('#other-type').trigger('click');

            form.find('#other-type-input').val(type);
        }

    });

    $('.notes-update').click(function (e) {
        e.preventDefault();
        $('#notes-submit-btn').attr('notes_id', $(this).data('notes_id'))
        $('#notes-delete-btn').attr('notes_id', $(this).data('notes_id'))
        $notes_modal.find('#notes').val($(this).data('notes'));
        $notes_modal.removeClass('create').addClass('update')
        $notes_modal.modal('show');
        var form = $notes_modal.find('form');
        resetFormValidation(form);
        var type = $(this).data('type');
        if (type == 'Primary') {
            form.find('#primary-type').trigger('click');
        }
        else {
            form.find('#other-type').trigger('click');

            form.find('#other-type-input').val(type);
        }

    });

    $('.license-update').click(function (e) {
        e.preventDefault();
        $('#license-submit-btn').attr('license_id', $(this).data('license_id'))
        $('#license-delete-btn').attr('license_id', $(this).data('license_id'))
        $license_modal.find('#license').val($(this).data('license'));
        $license_modal.removeClass('create').addClass('update')
        $license_modal.modal('show');
        var form = $license_modal.find('form');
        resetFormValidation(form);
        var type = $(this).data('type');
        if (type == 'Primary') {
            toggleOtherType(form, false);
            toggleDelete($license_modal, false);
            form.find('#primary-type').trigger('click');
        }
        else {
            toggleOtherType(form, true);
            toggleDelete($license_modal.find('.modal-footer'), true);
            form.find('#other-type').trigger('click');

            form.find('#other-type-input').val(type);
        }

    });


    $('.company-update').click(function (e) {
        e.preventDefault();
        $('#company-submit-btn').attr('company_id', $(this).data('company_id'))
        $('#company-delete-btn').attr('company_id', $(this).data('company_id'))
        $company_modal.find('#company').val($(this).data('company'));
        $company_modal.removeClass('create').addClass('update')
        $company_modal.modal('show');
        var form = $company_modal.find('form');
        resetFormValidation(form);
        var type = $(this).data('type');
        if (type == 'Primary') {
            toggleOtherType(form, false);
            toggleDelete($company_modal, false);
            form.find('#primary-type').trigger('click');
        }
        else {
            toggleOtherType(form, true);
            toggleDelete($company_modal.find('.modal-footer'), true);
            form.find('#other-type').trigger('click');

            form.find('#other-type-input').val(type);
        }

    });


    $('#address-submit-btn').click(function (e) {
        e.preventDefault();
        var clicked = $(this);
        var form = $address_modal.find('form');
        var formValidator = form.parsley();
        if (formValidator.isValid()) {
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var data = form.serialize();
                        if ($address_modal.hasClass('update')) {
                            var address_id = clicked.attr('address_id');
                            var url = 'pprofiles/contractors/address/update/' + address_id;
                        }
                        else {
                            var url = 'pprofiles/contractors/address/create';
                        }
                        $.ajax({
                            url: baseurl + url,
                            method: 'POST',
                            data: data,
                            success: function (response) {
                                if (response.status == 'true') {
                                    resolve();
                                } else {
                                    reject('Please verify the form, and try again!');
                                    $.each(response.messages, function (index, value) {
                                        var input = form.find('input[name="' + index + '"]');
                                        input.siblings('.help-block').text(value);
                                        input.parents('.form-group').addClass('has-error');
                                    });
                                }
                            }
                        });
                    });
                }
            }).then(function () {
                window.location.reload();
            });
        }
        else {
            var inputs = form.find('input');
            inputs.each(function () {
                if ($(this).val() == '') {
                    $(this).parents('.form-group').addClass('has-error');
                }
            });
        }
    });

    $('#phone-submit-btn').click(function () {
        var clicked = $(this);
        var form = $phone_modal.find('form');
        var formValidator = form.parsley();
        if (formValidator.isValid()) {
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var data = form.serialize();
                        if ($phone_modal.hasClass('update')) {
                            var phone_id = clicked.attr('phone_id');
                            var url = 'pprofiles/contractors/phone/update/' + phone_id;
                        }
                        else {
                            var url = 'pprofiles/contractors/phone/create';
                        }
                        $.ajax({
                            url: baseurl + url,
                            method: 'POST',
                            data: data,
                            success: function (response) {
                                if (response.status == 'true') {
                                    resolve();
                                } else {
                                    reject('Please verify the form, and try again!');
                                    $.each(response.messages, function (index, value) {
                                        var input = form.find('input[name="' + index + '"]');
                                        input.siblings('.help-block').text(value);
                                        input.parents('.form-group').addClass('has-error');
                                    });
                                }
                            }
                        });
                    });
                }
            }).then(function () {
                window.location.reload();
            });
        }
        else {
            var inputs = form.find('input');
            inputs.each(function () {
                if ($(this).val() == '') {
                    $(this).parents('.form-group').addClass('has-error');
                }
            });
        }

    });
    $('#email-submit-btn').click(function () {
        var clicked = $(this);
        var form = $email_modal.find('form');
        var formValidator = form.parsley();
        if (formValidator.isValid()) {
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var data = form.serialize();
                        if ($email_modal.hasClass('update')) {
                            var email_id = clicked.attr('email_id');
                            var url = 'pprofiles/contractors/email/update/' + email_id;
                        }
                        else {
                            var url = 'pprofiles/contractors/email/create';
                        }
                        $.ajax({
                            url: baseurl + url,
                            method: 'POST',
                            data: data,
                            success: function (response) {
                                if (response.status == 'true') {
                                    resolve();
                                } else {
                                    reject('Please verify the form, and try again!');
                                    $.each(response.messages, function (index, value) {
                                        var input = form.find('input[name="' + index + '"]');
                                        input.siblings('.help-block').text(value);
                                        input.parents('.form-group').addClass('has-error');
                                    });
                                }
                            }
                        });
                    });
                }
            }).then(function () {
                window.location.reload();
            });
        }
        else {
            var inputs = form.find('input');
            var emailValidator = $('#email').parsley();
            if (!emailValidator.isValid()) {
                $('#email').parent('.form-group').addClass('has-error');
                $('#email').parent().find('.help-block').text('Please type email correctly');
            }
            inputs.each(function () {
                if ($(this).val() == '') {
                    $(this).parents('.form-group').addClass('has-error');
                }
            });
        }

    });

    $('#notes-submit-btn').click(function () {
        var clicked = $(this);
        var form = $notes_modal.find('form');
        var formValidator = form.parsley();
        if (formValidator.isValid()) {
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var data = form.serialize();
                        if ($notes_modal.hasClass('update')) {
                            var notes_id = clicked.attr('notes_id');
                            var url = 'pprofiles/contractors/notes/update/' + notes_id;
                        }
                        else {
                            var url = 'pprofiles/contractors/notes/create';
                        }
                        $.ajax({
                            url: baseurl + url,
                            method: 'POST',
                            data: data,
                            success: function (response) {
                                if (response.status == 'true') {
                                    resolve();
                                } else {
                                    reject('Please verify the form, and try again!');
                                    $.each(response.messages, function (index, value) {
                                        var input = form.find('input[name="' + index + '"]');
                                        input.siblings('.help-block').text(value);
                                        input.parents('.form-group').addClass('has-error');
                                    });
                                }
                            }
                        });
                    });
                }
            }).then(function () {
                window.location.reload();
            });
        }
        else {
            var inputs = form.find('input');
            var notesValidator = $('#notes').parsley();
            if (!notesValidator.isValid()) {
                $('#notes').parent('.form-group').addClass('has-error');
                $('#notes').parent().find('.help-block').text('This value is to long. It should be 256 characters or fewer');
            }
            inputs.each(function () {
                if ($(this).val() == '') {
                    $(this).parents('.form-group').addClass('has-error');
                }
            });
        }

    });

    $('#license-submit-btn').click(function () {
        var clicked = $(this);
        var form = $license_modal.find('form');
        var formValidator = form.parsley();
        if (formValidator.isValid()) {
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var data = form.serialize();
                        if ($license_modal.hasClass('update')) {
                            var license_id = clicked.attr('license_id');
                            var url = 'pprofiles/contractors/license/update/' + license_id;
                        }
                        else {
                            var url = 'pprofiles/contractors/license/create';
                        }
                        $.ajax({
                            url: baseurl + url,
                            method: 'POST',
                            data: data,
                            success: function (response) {
                                if (response.status == 'true') {
                                    resolve();
                                } else {
                                    reject('Please verify the form, and try again!');
                                    $.each(response.messages, function (index, value) {
                                        var input = form.find('input[name="' + index + '"]');
                                        input.siblings('.help-block').text(value);
                                        input.parents('.form-group').addClass('has-error');
                                    });
                                }
                            }
                        });
                    });
                }
            }).then(function () {
                window.location.reload();
            });
        }
        else {
            var inputs = form.find('input');
            var licenseValidator = $('#license').parsley();
            if (!licenseValidator.isValid()) {
                $('#license').parent('.form-group').addClass('has-error');
                $('#license').parent().find('.help-block').text('Please type license correctly');
            }
            inputs.each(function () {
                if ($(this).val() == '') {
                    $(this).parents('.form-group').addClass('has-error');
                }
            });
        }

    });

    $('#company-submit-btn').click(function () {
        var clicked = $(this);
        var form = $company_modal.find('form');
        var formValidator = form.parsley();
        if (formValidator.isValid()) {
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        var data = form.serialize();
                        if ($company_modal.hasClass('update')) {
                            var company_id = clicked.attr('company_id');
                            var url = 'pprofiles/contractors/company/update/' + company_id;
                        }
                        else {
                            var url = 'pprofiles/contractors/company/create';
                        }
                        $.ajax({
                            url: baseurl + url,
                            method: 'POST',
                            data: data,
                            success: function (response) {
                                if (response.status == 'true') {
                                    resolve();
                                } else {
                                    reject('Please verify the form, and try again!');
                                    $.each(response.messages, function (index, value) {
                                        var input = form.find('input[name="' + index + '"]');
                                        input.siblings('.help-block').text(value);
                                        input.parents('.form-group').addClass('has-error');
                                    });
                                }
                            }
                        });
                    });
                }
            }).then(function () {
                window.location.reload();
            });
        }
        else {
            var inputs = form.find('input');
            var companyValidator = $('#company').parsley();
            if (!companyValidator.isValid()) {
                $('#company').parent('.form-group').addClass('has-error');
                $('#company').parent().find('.help-block').text('Please type company correctly');
            }
            inputs.each(function () {
                if ($(this).val() == '') {
                    $(this).parents('.form-group').addClass('has-error');
                }
            });
        }

    });

    $('#address-delete-btn').click(function () {
        var clicked = $(this);
        swal({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var address_id = clicked.attr('address_id');
                    $.ajax({
                        url: baseurl + 'pprofiles/contractors/address/delete/' + address_id,
                        method: 'DELETE'
                    }).done(function () {
                        resolve();
                    }).fail(function () {
                        reject("Error!");
                    });
                });
            }
        }).then(function () {
            window.location.reload();
        });
    });
    $('#phone-delete-btn').click(function () {
        var clicked = $(this);
        swal({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var phone_id = clicked.attr('phone_id');
                    $.ajax({
                        url: baseurl + 'pprofiles/contractors/phone/delete/' + phone_id,
                        method: 'DELETE'
                    }).done(function () {
                        resolve();
                    }).fail(function () {
                        reject("Error!");
                    });
                });
            }
        }).then(function () {
            window.location.reload();
        });
    });
    $('#email-delete-btn').click(function () {
        var clicked = $(this);
        swal({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var email_id = clicked.attr('email_id');
                    $.ajax({
                        url: baseurl + 'pprofiles/contractors/email/delete/' + email_id,
                        method: 'DELETE'
                    }).done(function () {
                        resolve();
                    }).fail(function () {
                        reject("Error!");
                    });
                });
            }
        }).then(function () {
            window.location.reload();
        });
    });

    $('#notes-delete-btn').click(function () {
        var clicked = $(this);
        swal({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var notes_id = clicked.attr('notes_id');
                    $.ajax({
                        url: baseurl + 'pprofiles/contractors/notes/delete/' + notes_id,
                        method: 'DELETE'
                    }).done(function () {
                        resolve();
                    }).fail(function () {
                        reject("Error!");
                    });
                });
            }
        }).then(function () {
            window.location.reload();
        });
    });

    $('#license-delete-btn').click(function () {
        var clicked = $(this);
        swal({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var license_id = clicked.attr('license_id');
                    $.ajax({
                        url: baseurl + 'pprofiles/contractors/license/delete/' + license_id,
                        method: 'DELETE'
                    }).done(function () {
                        resolve();
                    }).fail(function () {
                        reject("Error!");
                    });
                });
            }
        }).then(function () {
            window.location.reload();
        });
    });

    $('#company-delete-btn').click(function () {
        var clicked = $(this);
        swal({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var company_id = clicked.attr('company_id');
                    $.ajax({
                        url: baseurl + 'pprofiles/contractors/company/delete/' + company_id,
                        method: 'DELETE'
                    }).done(function () {
                        resolve();
                    }).fail(function () {
                        reject("Error!");
                    });
                });
            }
        }).then(function () {
            window.location.reload();
        });
    });

    $('#contractor-delete').click(function (e) {
        e.preventDefault();
        var clicked = $(this);
        swal({
            title: "Are you sure you want to disable?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes,disable it!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            var url = clicked.attr('href');
            window.location.href = url;
        });
    });

    $('#contractor-update').click(function (e) {
        e.preventDefault();
        var clicked = $(this);
        var is_union = 0;
        var status = $('#status-switch').find('div.bootstrap-switch-id-is_union').hasClass('bootstrap-switch-on');
        if (status) {
            is_union = 1;
        }
        var url = clicked.data('url') + '?is_union=' + is_union;
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            window.location.href = url;
        });
    });


});