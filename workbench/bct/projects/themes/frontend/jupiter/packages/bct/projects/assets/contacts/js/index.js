var Extension;
var checked_rows = [];

;(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'contact'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row]', Extension.Index.checkRow)
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '#merge-btn', Extension.Index.mergeAction)
            .on('click', '.btn-replace', Extension.Index.replaceAction)
            .on('click', '#merge-submit-btn', Extension.Index.mergeBtnAction)
            .on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
            .on('hidden.bs.modal', '#merge-modal', Extension.Index.onCloseMergeBtn)
        ;
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxes = function (event) {
        event.stopPropagation();
        var checkbox = $(this);
        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all') {
            $('[data-grid-checkbox]').not(this).prop('checked', this.checked);

            $('[data-grid-row]').not(this).toggleClass('active', this.checked);
        }
        else{
            Extension.Index.saveChecked(checkbox);
        }

        $(this).parents('[data-grid-row]').toggleClass('active');

        Extension.Index.mergeStatus();

    };


    // Handle Data Grid row checking
    Extension.Index.checkRow = function () {
        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', !checkbox.prop('checked'));
        Extension.Index.saveChecked(checkbox);

        Extension.Index.mergeStatus();
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    Extension.Index.mergeStatus = function () {
        var count_rows = checked_rows.length;
        if (count_rows == 2) {
            $('#merge-btn').removeClass('disabled');
        } else {
            $('#merge-btn').addClass('disabled');
        }
    };


    Extension.Index.saveChecked = function(checkbox){
        var parent = $(checkbox).closest('tr'),
            $btn_details = $(parent).find('.btn-details');
        var r = {
            id: $btn_details.data('id'),
            full_name: $btn_details.data('full_name'),
            location: $btn_details.data('location'),
            company: $btn_details.data('company'),
            title: $btn_details.data('title'),
            phone: $btn_details.data('phone'),
            phone2: $btn_details.data('phone2'),
            fax: $btn_details.data('fax'),
            email: $btn_details.data('email'),
            notes: $btn_details.data('notes'),
        };
        if ($(checkbox).prop('checked')) {
            checked_rows.push(r);
        }
        else{
            checked_rows = checked_rows.filter(function(el) {
                return el.id !== r.id;
            });
        }
    }

    Extension.Index.mergeAction = function (event) {
        event.preventDefault();

        if (checked_rows.length == 2) {
            var one_id = checked_rows[0].id,
                two_id = checked_rows[1].id;

            $('#merge-modal').modal('show');
            $('#contact-one-id').val(one_id);
            $('#contact-one-full_name').text(checked_rows[0].full_name).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one-location').text(checked_rows[0].location).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one-company').text(checked_rows[0].company).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one-title').text(checked_rows[0].title).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one-phone').text(checked_rows[0].phone).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one-phone2').text(checked_rows[0].phone2).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one-fax').text(checked_rows[0].fax).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one-email').text(checked_rows[0].email).parents('tr').find('input[type=radio]').val(one_id)
            $('#contact-one-notes').text(checked_rows[0].notes).parents('tr').find('input[type=radio]').val(one_id);
            $('#contact-one').val(checked_rows[0].id);

            $('#contact-two-id').val(two_id);
            $('#contact-two-full_name').text(checked_rows[1].full_name).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-location').text(checked_rows[1].location).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-company').text(checked_rows[1].company).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-title').text(checked_rows[1].title).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-phone').text(checked_rows[1].phone).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-phone2').text(checked_rows[1].phone2).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-fax').text(checked_rows[1].fax).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-email').text(checked_rows[1].email).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two-notes').text(checked_rows[1].notes).parents('tr').find('input[type=radio]').val(two_id);
            $('#contact-two').val(checked_rows[1].id);
        }

    };

    Extension.Index.mergeBtnAction = function (event) {
        event.preventDefault();

        var submit_action = true;

        if ($('input[name="contact-keep-full_name"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-location"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-company"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-title"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-phone"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-phone2"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-fax"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-email"]').is(':checked') === false) {
            submit_action = false;
        }

        if ($('input[name="contact-keep-notes"]').is(':checked') === false) {
            submit_action = false;
        }


        if (submit_action) {
            swal({
                title: "Are you sure you want to merge?",
                text: "Only the selected contact will be kept, the other will be deleted after merge.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
            }).then(function () {
                $('#merge-form').submit();
            });
        } else {
            swal({
                title: "You must select all fields you want to merge",
                type: "error",
                showConfirmButton: true,
            });
        }


    };

    Extension.Index.phoneFormat = function (phone) {
        phone = phone.replace(/[^0-9]/g, '');
        phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        return phone;
    };

    Extension.Index.replaceAction = function () {
        var $contact_one_html = $('.contact-one').html(),
            $contact_two_html = $('.contact-two').html();

        $('.contact-one').html($contact_two_html.replace(/-two/ig, '-one'));
        $('.contact-two').html($contact_one_html.replace(/-one/ig, '-two'))
    };

    Extension.Index.onCloseMergeBtn = function () {
        $('#merge-modal input[type=radio]').prop('checked', false);
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            //hash: false,
            events: {
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
            callback: function () {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

                Extension.Index.mergeStatus();
            }
        };

        Extension.Index.Grid = $.datagrid('contact', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
