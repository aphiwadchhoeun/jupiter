<script type="text/template" data-grid="groups" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td>
            <input input="" type="hidden" value="<%- r.id %>">
            <%- r.name %>
        </td>
        <td>
            <% if(r.individual) { %>
               {{ trans('bct/projects::groups/model.general.individual') }}
            <% } else { %>
                {{ trans('bct/projects::groups/model.general.group') }}
            <% } %>
        </td>
        <td><%- r.description %></td>
        <td>
            <% if(r.status) { %>
                <span class="label label-success">{{ trans('bct/projects::groups/model.general.active') }}</span>
            <% } else { %>
                <span class="label label-danger">{{ trans('bct/projects::groups/model.general.inactive') }}</span>
            <% } %>
        </td>
        <td class="hidden-print">
            <a href="<%- r.link_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
               data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                        class="fa fa-book fa-fw"></i></a>
        </td>
    </tr>

    <% }); %>

</script>
