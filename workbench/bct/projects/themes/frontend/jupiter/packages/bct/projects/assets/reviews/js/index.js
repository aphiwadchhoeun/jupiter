var Extension;

;(function(window, document, $, undefined)
{

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'reviews'
    });

    // Initialize functions
    Extension.Index.init = function()
    {
        Extension.Index.datePicker(Extension);
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function()
    {
        Platform.Cache.$body
            .on('click', '[data-grid-row]', Extension.Index.checkRow)
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
            .on('click', '#modal-confirm a.confirm', Extension.Index.bulkActions)
            .on('click', '[data-grid-calendar-preset]', Extension.Index.calendarPresets)
            .on('click', '[data-grid-bulk-action]:not([data-grid-bulk-action="delete"])', Extension.Index.bulkActions)
            .on('click', '.btn-approve', Extension.Index.approveAction)
            .on('click', '.btn-unapprove', Extension.Index.unapproveAction)

        ;
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxes = function(event)
    {
        event.stopPropagation();

        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all')
        {
            $('[data-grid-checkbox]').not(this).prop('checked', this.checked);

            $('[data-grid-row]').not(this).toggleClass('active', this.checked);
        }

        $(this).parents('[data-grid-row]').toggleClass('active');

        Extension.Index.bulkStatus();
    };

    // Handle Data Grid row checking
    Extension.Index.checkRow = function()
    {
        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', ! checkbox.prop('checked'));

        Extension.Index.bulkStatus();
    };

    Extension.Index.bulkStatus = function()
    {
        var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

        var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;
        var checked_and_not_approve = $('[data-not_approved][data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;
        var checked_and_approve = $('[data-grid-checkbox]:not([data-not_approved]):checked').not('[data-grid-checkbox="all"]').length;


        $('[data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);
        $('[data-grid-bulk-action]').toggleClass('disabled', ! checked);

        if (checked > 0)
        {
            $('[data-grid-bulk-action="delete"]').attr('data-modal', true);
        }
        else
        {
            $('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
        }

        if (checked_and_not_approve > 0) {
            $('.btn-approve').removeClass('disabled');
        } else {
            $('.btn-approve').addClass('disabled');
        }

        if (checked_and_approve > 0) {
            $('.btn-unapprove').removeClass('disabled');
        } else {
            $('.btn-unapprove').addClass('disabled');
        }

        $('[data-grid-checkbox="all"]')
            .prop('disabled', rows < 1)
        ;
    };

    // Handle Data Grid bulk actions
    Extension.Index.bulkActions = function(event)
    {
        event.preventDefault();
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
        var url = window.location.origin + window.location.pathname;

        var action = $(this).data('grid-bulk-action') ? $(this).data('grid-bulk-action') : 'delete';

        var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
        {
            return +event.value;
        });

        if (rows.length > 0)
        {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    action : action,
                    rows   : rows
                },
                success: function(response)
                {
                    Extension.Index.Grid.refresh();
                }
            });
        }
    };

    // Handle Data Grid calendar
    Extension.Index.calendarPresets = function(event)
    {
        event.preventDefault();

        var start, end;

        switch ($(this).data('grid-calendar-preset'))
        {
            case 'day':
                start = end = moment().subtract(1, 'day').startOf('day').format('MM/DD/YYYY');
                break;

            case 'week':
                start = moment().startOf('week').format('MM/DD/YYYY');
                end   = moment().endOf('week').format('MM/DD/YYYY');
                break;

            case 'month':
                start = moment().startOf('month').format('MM/DD/YYYY');
                end   = moment().endOf('month').format('MM/DD/YYYY');
                break;

            default:
        }

        $('input[name="daterangepicker_start"]').val(start);

        $('input[name="daterangepicker_end"]').val(end);

        $('.range_inputs .applyBtn').trigger('click');
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function(event)
    {
        event.stopPropagation();
    };

    Extension.Index.approveAction = function(e) {
        e.preventDefault();

        var url = $(this).data('url');

        if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    rows   : 'all'
                },
                success: function(response)
                {
                    resetLoadingBtn('.btn-approve');
                    Extension.Index.Grid.refresh();
                }
            });
        } else {
            var checked_and_not_approve = $('[data-not_approved][data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;

            if (checked_and_not_approve > 0) {

                var rows = $.map($('[data-not_approved][data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
                {
                    return +event.value;
                });


                if (rows.length > 0)
                {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {
                            rows   : rows
                        },
                        success: function(response)
                        {
                            resetLoadingBtn('.btn-approve');
                            Extension.Index.Grid.refresh();
                        }
                    });
                }

            }
        }
    };

    Extension.Index.unapproveAction = function(e) {
        e.preventDefault();

        var url = $(this).data('url');

        if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    rows   : 'all'
                },
                success: function(response)
                {
                    resetLoadingBtn('.btn-unapprove');
                    Extension.Index.Grid.refresh();
                }
            });
        } else {
            var checked_and_approve = $('[data-grid-checkbox]:not([data-not_approved]):checked').not('[data-grid-checkbox="all"]').length;

            if (checked_and_approve > 0) {

                var rows = $.map($('[data-grid-checkbox]:not([data-not_approved]):checked').not('[data-grid-checkbox="all"]'), function(event)
                {
                    return +event.value;
                });


                if (rows.length > 0)
                {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {
                            rows   : rows
                        },
                        success: function(response)
                        {
                            resetLoadingBtn('.btn-unapprove');
                            Extension.Index.Grid.refresh();
                        }
                    });
                }

            }
        }
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function()
    {
        var config = {
            hash: false,
            source_default: $('#data-grid').data('source-default'),
            callback_local: function() {
                $('[data-filter="pp_reviews.approved:0"]').click();
            },
            events: {
                removing: function(dg)
                {
                    _.each(dg.applied_filters, function(filter)
                    {
                        if (filter.column === 'mrp_user.created_at' && filter.from !== undefined && filter.to !== undefined)
                        {
                            $('[data-grid-calendar]').val('');
                        }
                    });
                },
                sorted: function(response) {
                    if (response.pagination.total== 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function(response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container' ).removeClass('hide');
                    }
                },
            },
            callback: function()
            {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

                Extension.Index.bulkStatus();
            }
        };

        Extension.Index.Grid = $.datagrid('reviews', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);