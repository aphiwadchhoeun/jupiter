/**
 * Updated by Aphiwad 08/05/2016
 */
jQuery('document').ready(function ($) {

    var regions = [];
    var cartodb_api = null, cartodb_table = null;


    function fetchCartodbConfigs(callback) {
        if (cartodb_api == null || cartodb_table == null) {
            $.get('/ui/api/cartodb/configs/get')
                .done(function (data) {
                    cartodb_api = data.api_url;
                    cartodb_table = data.tablename;
                    callback();
                });
        } else {
            callback();
        }
    }

    fetchCartodbConfigs(function () {
        var regionService = cartodb_api + '/sql?q=select areaabrv, areaname from ' + cartodb_table;

        // make a call to cartodb service and retrieve regions data, then populate them into select boxes
        $.get(regionService, function (data) {
            $('#regions-report').prop('disabled', true);
            if (data.rows.length > 0) {

                $.each(data.rows, function (i, row) {

                    $('#regions-report').append($('<option>', {
                        value: row.areaabrv,
                        text: row.areaabrv
                    }));

                });
                $('#regions-report').prop('disabled', false);

            }

        }).done(function () {
            var $regions = $('#regions-report').selectize({
                onChange: function (value) {
                    regions = value;

                    if (value !== null && value.length > 0) {
                        ajaxResults(regions);
                        $(".btn-download-pdf").removeClass('hide');
                        $('.regions-report1-deselect-all').removeClass('hide');
                    } else {
                        $(".btn-download-pdf").addClass('hide');
                        $('.report-results').html('');
                    }
                }
            });
            var selectize_report = $regions[0].selectize;

            $('.regions-report-select-all').on('click', function (e) {
                e.preventDefault();

                $(this).addClass('hide');
                $('.regions-report-deselect-all').removeClass('hide');

                selectize_report.setValue(_.keys(selectize_report.options));
            });

            $('.regions-report-deselect-all').on('click', function (e) {
                e.preventDefault();

                $(this).addClass('hide');
                $('.regions-report-select-all').removeClass('hide');
                selectize_report.clear();
            });
        });
    });

    $('.btn-download-pdf').click(function (e) {
        e.preventDefault();

        askFileName(regions);
    });

});

function askFileName(regions) {
    var defaultFilename = 'Active Service Jobs';
    var title = 'Enter filename';
    var text = '';

    swal({
        title: title,
        text: text,
        input: 'text',
        showCancelButton: true,
        inputValue: defaultFilename,
        showLoaderOnConfirm: true,
        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    if (value.length > 80) {
                        reject("Filename length should not be longer than 80 characters!");
                    }

                    resolve();
                } else {
                    reject("You need to write filename!");
                }
            });
        },
        preConfirm: function (input) {
            return new Promise(function (resolve, reject) {
                var url = '/pprofiles/reports/active-service-jobs/export';

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        regions: regions,
                        filename: input,
                        download: 'pdf'
                    }
                })
                    .done(function () {
                        resolve();
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        var error = (jqXHR.status === 422) ? JSON.parse(jqXHR.responseText) : '';
                        var title = errorThrown;
                        var text = (typeof error.message !== 'undefined') ? error.message : '';
                        swal(title, text, "error")
                    })
                    .always(function () {
                    });
            });
        }
    }).then(function (input) {
        swal('PDF document has been requested!', 'Your document will be ready very soon.', 'success');

    });
}

function ajaxResults(regions) {
    var url = '/pprofiles/reports/active-service-jobs/export';

    showLoader();

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            regions: regions
        }
    })
        .done(function (response) {
            var results = JSON.parse(response.results);
            var report_results_template = _.template($('#report_results_template').html())({results: results});
            $('.report-results').html(report_results_template);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            var error = (jqXHR.status === 422) ? JSON.parse(jqXHR.responseText) : '';
            var title = errorThrown;
            var text = (typeof error.message !== 'undefined') ? error.message : '';
            swal(title, text, "error");
            $('.report-results').html('');
        })
        .always(function () {
            hideLoader()
        });
}

function showLoader() {
    $('.loader').addClass('loader-active');
}

function hideLoader() {
    $('.loader').removeClass('loader-active');
}