/**
 * Created by user on 30/06/15.
 */
var AllMembersExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    AllMembersExtension = Object.create(ExtensionBase);
    AllMembersExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'members'
    });

    // Initialize functions
    AllMembersExtension.Index.init = function () {
        AllMembersExtension.Index.dataGrid();
        AllMembersExtension.Index.listeners();
    };

    // Add Listeners
    AllMembersExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', AllMembersExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    AllMembersExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    AllMembersExtension.Index.exporterStatus = function (grid) {
        $('#project-members-all-exporter').toggleClass('disabled', grid.pagination.filtered == 0);
        $('#project-members-all-exporter').parent().toggleClass('disabled', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    AllMembersExtension.Index.dataGrid = function () {
        var config = {
            //scroll: '#data-grid',
            events: {
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
            callback: function () {
            }
        };

        AllMembersExtension.Index.Grid = $.datagrid('members', '#members-data-grid', '.members-data-grid_pagination', '#members-data-grid_applied', config);
    };

    // Job done, lets run.
    AllMembersExtension.Index.init();

})(window, document, jQuery);

$('#members-add-btn').on('click', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modalAjax = $('#modal-form-ajax');

    $modalAjax.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (response) {
            $modalAjax.modal('show');

            initAjaxModal();

            $('#modal-form-ajax').on('click', '#submit-btn', function (e) {
                e.preventDefault();

                var form_modal = $modalAjax.find('#content-form');

                swal({
                    title: "Are you sure you want to save?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#019964",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    showLoaderOnConfirm: true,
                }).then(function () {
                    $.post(
                        form_modal.attr('action'),
                        form_modal.serialize(),
                        function (data) {
                            if (typeof data.success == 'undefined') {
                                $modalAjax.find('.modal-content').html(data);
                                initAjaxModal();
                            } else {
                                $modalAjax.modal('hide');
                                AllMembersExtension.Index.Grid.refresh();
                            }
                        }
                    );
                });
            });
        } else {
            location.reload();
        }
    });

    function initAjaxModal() {
        $modalAjax.find('[data-mask]').mask('(999) 999-9999');
        $modalAjax.find('[data-mask-zip]').mask('99999?-9999');
    }
});

$('#members-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            $modal.find('select[name="scope_id"]').selectize();
            $modal.find('input[name="number_hours"]').inputmask("decimal", { rightAlign: false, allowMinus: false, digits: 2, placeholder: '0.00' });
            $modal.find('input[name="wage"]').inputmask("currency", { rightAlign: false, allowMinus: false });

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().on('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                            if (data == 'success') {
                                $modal.modal('hide');
                                AllMembersExtension.Index.Grid.refresh();
                                ProjectMemberExtension.Index.Grid.refresh();
                                membersSummaryUpdate();
                            } else {
                                swal(data);
                            }
                        })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });
            }

        } else {
            location.reload();
        }
    });
});
