@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::users/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
{{ Asset::queue('user-link', 'bct/projects::users/css/link.css') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}
{{ Asset::queue('mrpuser-link', 'bct/projects::users/js/link.js', 'bootstrap-toggle') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-tabs">

        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip" href="{{ route('bct.projects.stewards.all') }}" data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{ trans("bct/projects::stewards/common.title") }} {{ trans('bct/projects::users/common.settings') }}</span>
                        </div>

                        <div class="pull-right">
                            <a href="{{ route('bct.projects.stewards.edit', $user->id) }}"
                                id="steward_edit"
                                name="steward_edit"
                               class="btn btn-primary"
                               data-toggle="tooltip"
                               data-original-title="{{{ trans('bct/projects::general/common.edit') }}}"><i
                                        class="fa fa-edit fa-fw"></i></a>
                        </div>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                {{-- Form: General --}}
                <fieldset>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-condensed">
                                <tbody>
                                <tr>
                                    <td class="col-md-2 td-label">{{ trans('bct/projects::users/model.general.first_name') }}</td>
                                    <td class="col-md-4">{{ data_get($user, 'first_name', '') }}</td>
                                    <td class="col-md-2 td-label">{{ trans('bct/projects::users/model.general.last_name') }}</td>
                                    <td class="col-md-4">{{ data_get($user, 'last_name', '') }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 td-label">{{ trans('bct/projects::users/model.general.email') }}</td>
                                    <td class="col-md-4">{{ data_get($user, 'email', '') }}</td>
                                    <td class="col-md-2 td-label">{{ trans('bct/projects::users/model.general.phone') }}</td>
                                    <td class="col-md-4">{{ data_get($user->profile, 'business_phone', '') }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 td-label">{{ trans('bct/projects::users/model.general.local') }}</td>
                                    <td class="col-md-4">{{ data_get($user->profile, 'business_local', '') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </fieldset>
            </div>
        </form>

    </section>
@stop
