@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::contractors/common.title') }}
@stop
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}

{{ Asset::queue('index', 'bct/projects::projects/js/index.js', 'extension-base') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section id="information" class="panel panel-default panel-tabs">

        <header class="panel-heading hidden-print">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{ route('bct.projects.contractors.all') }}"
                                   data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                                    <i class="fa fa-reply"></i> <span
                                            class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                </a>
                            </li>
                        </ul>

                        <span class="navbar-brand">{{ $contractor->name }} {{ trans("bct/projects::general/common.details") }} | ID: {{ data_get($contractor, 'id', '') }}</span>
                    </div>

                    <div class="pull-right hidden-print">
                    @if ($has_access && !$currentUser->inRole('pp-guest'))
                            <a href="{{ route('bct.projects.contractors.edit', $contractor->id) }}"
                               class="btn btn-primary"
                               data-toggle="tooltip"
                               id="contractor_edit"
                               name="contractor_edit"
                               data-original-title="{{{ trans('bct/projects::general/common.edit') }}}"><i
                                        class="fa fa-edit fa-fw"></i></a>
                        @endif
                    </div>

                    <div class="pull-right" style="margin: 1px 10px 0 0">
                        <a href="{{ route('bct.projects.contractors.pdf.details', $contractor->id) }}"
                                class="btn btn-link pull-right"
                                data-toggle="tooltip"
                                data-original-title="{{{ trans('bct/projects::general/common.print') }}}">
                            <i class="fa fa-print fa-fw"></i>
                        </a>
                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">
            <div class="row visible-print">
                <h4 class="overview-heading text-center visible-print">
                    {{ data_get($contractor, 'name', '') }} -
                    {{ trans("bct/projects::contractors/common.title") }} {{ trans("bct/projects::general/common.details") }}
                </h4>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" id="id" name="id" value="{{ $contractor->id }}"/>

                    <table class="table table-bordered table-details">
                        <tbody>
                        <tr>
                            <td class="td-value">
                                <div class="flex">
                                    <div class="col-md-3 border-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="w80px"><b>Name:</b></div> <div>{{ data_get($contractor,'name','') }}</div>
                                            </div>
                                        </div>
                                        @if(isset($companies) && $companies->count()>0)
                                            @foreach($companies as $item)
                                                @if($item->type!=='Primary')
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="w80px">{{$item->type}}:</div> <div>{{$item->company}}</div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="w80px"><b>Address:</b></div> <div class="details-value">{{$contractor->getFullAddress()}}</div>
                                            </div>
                                        </div>
                                        @if(isset($addresses) && $addresses->count()>0)
                                            @foreach($addresses as $item)
                                                @if($item->type!=='Primary')
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="w80px">{{$item->type}}:</div> <div class="details-value">{{$item->getFullAddress()}}</div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-md-3 border-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="w80px"><b>Phone:</b></div> <a href="tel:{{ data_get($contractor,'phone','') }}">{{ data_get($contractor,'phone','') }}</a>
                                            </div>
                                        </div>
                                        @if(isset($phones) && $phones->count()>0)
                                            <?php
                                            $phones = $phones->filter(function ($item) {
                                                return $item['type'] != 'Primary';
                                            })->take(3);
                                            ?>
                                            @foreach($phones as $item)
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="w80px">{{$item->type}}:</div> <a href="tel:{{$item->phone}}">{{$item->phone}}</a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="w80px"><b>Email:</b></div> <a href="mailto:{{ data_get($contractor,'email','') }}">{{ data_get($contractor,'email','') }}</a>
                                            </div>
                                        </div>
                                        @if(isset($emails) && $emails->count()>0)
                                            @foreach($emails as $item)
                                                @if($item->type!=='Primary')
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="w80px">{{$item->type}}:</div> <a href="mailto:{{$item->email}}">{{$item->email}}</a>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-md-3 border-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="w80px"><b>License:</b></div> <div>{{ data_get($contractor,'license','') }}</div>
                                            </div>
                                        </div>
                                        @if(isset($licenses) && $licenses->count()>0)
                                            @foreach($licenses as $item)
                                                @if($item->type!=='Primary')
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="w80px">{{$item->type}}:</div> <div>{{$item->license}}</div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="w80px"><b>Union:</b></div>
                                                @if($contractor->is_union)
                                                    <span class="label label-success">Yes</span>
                                                @else
                                                    <span class="label label-danger">No</span>
                                                @endif

                                                @if($contractor->active == 1)
                                                    <span class="label label-success">{{ trans('bct/projects::general/common.active') }}</span>
                                                @else
                                                    <span class="label label-danger">{{ trans('bct/projects::general/common.disabled') }}</span>

                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="w80px"><b>Notes:</b></div> <div>{{ data_get($contractor,'notes','') }}</div>
                                                @if(isset($notes) && $notes->count()>0)
                                                    @foreach($notes as $item)
                                                        @if($item->type!=='Primary')
                                                            <br/>
                                                            <div>{{$item->notes}}</div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <span class="navbar-brand"><b>{{ trans("bct/projects::files/common.title") }}</b> <span id="files-counter" class="text14">( {{ $files->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $files->count() }} )</span></span>

                    @if (!$currentUser->inRole('pp-guest'))

                    <a href="{{ route('bct.projects.contractors.files',['id'=>$contractor->id])}}" class="btn btn-primary pull-right"
                       data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
                                class="fa fa-edit fa-fw"></i></a>

                    <a href="{{ route('bct.projects.contractors.files',['id'=>$contractor->id])}}" class="btn btn-success pull-right"
                       data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}"><i
                                class="fa fa-plus fa-fw"></i></a>
                    @endif

                    <table class="table table-striped table-bordered">
                        <tbody>
                        <tr>
                            <td class="td-label col-md-3 text-center">{{ trans('bct/projects::files/model.general.name') }}</td>
                            <td class="td-label col-md-5 text-center">{{ trans('bct/projects::files/model.general.description') }}</td>
                            <td class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.type') }}</td>
                            <td class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.date') }}</td>
                        </tr>
                        @foreach($files->take(5) as $file)
                            <tr>
                                <td class="td-value"><a href="{{ $file->download_url }}" download="{{ $file->name_full }}">{{ $file->name }}</a></td>
                                <td class="td-value text-center">{{ $file->description }}</td>
                                <td class="td-value text-center">{{ $file->extension }}</td>
                                <td class="td-value text-center">{{ date_correct_details_projects( $file->created_at ) }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">

                    <section class="panel panel-default panel-grid">

                        {{-- Grid: Header --}}
                        <header class="panel-heading hidden-print">

                            <nav class="navbar navbar-default navbar-actions">

                                <div class="container-fluid">

                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                data-target="#actions">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>

                                        <span class="navbar-brand">{{ trans('bct/projects::projects/common.title') }}</span>

                                    </div>

                                    {{-- Grid: Actions --}}
                                    <div class="collapse navbar-collapse" id="actions">

                                        <ul class="nav navbar-nav navbar-left download-container">

                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown"
                                                   role="button" aria-expanded="false"
                                                   data-original-title="{{{ trans('action.export') }}}">
                                                    <i class="fa fa-download"></i> <span
                                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i>
                                                            CSV</a></li>
                                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>

                                        {{-- Grid: Filters --}}
                                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8"
                                              data-search data-grid="projects" role="form">

                                            <div class="input-group">

                                                <span class="input-group-btn">

                                                    <button class="btn btn-default" type="button" disabled>
                                                        {{{ trans('common.status') }}}
                                                    </button>

                                                    <button class="btn btn-default dropdown-toggle" type="button"
                                                            data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                                        @foreach(\Bct\Projects\Models\Status::all() as $status)
                                                            <li>
                                                                <a href="#" data-filter="z_project_status.name:{{$status->name}}">
                                                                    <i class="fa @if($status->name == 'Completed') fa-times-circle @else fa-check-circle @endif"></i> {{$status->name}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>

                                                    <button class="btn btn-default hidden-xs" type="button"
                                                            data-grid-calendar data-range-filter="z_projects.created_at">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>

                                                </span>

                                                <span class="input-group-btn">

                                                    <button class="btn btn-default" type="button" disabled>
                                                        {{ trans('bct/projects::projects/common.stage') }}
                                                    </button>

                                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                                            aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>

                                                    <ul id="stage-dropdown" class="dropdown-menu" role="menu" data-filter-reset>
                                                        @foreach($stages as $stage)
                                                            <li>
                                                                <a href="#" data-filter="z_project_stage.name:{{$stage->name}}">
                                                                    <i class="fa fa-check-circle "></i> {{$stage->name}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </span>

                                                <span class="input-group">
                                                    <select name="column" class="form-control">
                                                        <option value="z_projects.name">{{ trans('bct/projects::projects/model.general.name') }}
                                                        </option>
                                                        <option value="z_projects.city">{{ trans('bct/projects::projects/model.general.city') }}
                                                        </option>
                                                        <option value="z_projects.state">{{ trans('bct/projects::projects/model.general.state') }}
                                                        </option>
                                                    </select>
                                                </span>



                                                <span class="input-group-btn">

                                                    <input class="form-control" name="filter" type="text"
                                                           placeholder="{{{ trans('common.search') }}}">

                                                    <button class="btn btn-default" type="submit">
                                                        <span class="fa fa-search"></span>
                                                    </button>

                                                    <button class="btn btn-default" data-grid="projects" data-reset>
                                                        <i class="fa fa-refresh fa-sm"></i>
                                                    </button>

                                                </span>

                                            </div>

                                            <input type="hidden" data-range-filter="z_projects.created_at" data-format="YYYY-MM-DD" data-range-start></button>
                                            <input type="hidden" data-range-filter="z_projects.created_at" data-format="YYYY-MM-DD" data-range-end></button>


                                        </form>

                                    </div>

                                </div>

                            </nav>

                        </header>

                        <div class="panel-body">

                            <div class="row visible-print">
                                <div class="col-md-12">
                                    <span class="lead">{{ trans('bct/projects::contractors/model.general.projects_belong') }}
                                        :</span>
                                </div>
                            </div>

                            {{-- Grid: Applied Filters --}}
                            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                                <div id="data-grid_applied" class="btn-group" data-grid="projects"></div>

                            </div>

                            {{-- Grid: Table --}}
                            <div class="table-responsive">

                                <table id="data-grid" class="table table-hover"
                                       data-source="{{ route('bct.projects.contractors.gridProjects', [$contractor->id, 'filters[0][z_project_status.name]=Active']) }}"
                                       data-source-default="{{ route('bct.projects.contractors.gridProjects', $contractor->id) }}"
                                       data-filename="Projects for Contractor - {{ $contractor->name }}"
                                       data-grid="projects">
                                    <thead>
                                    <tr>
                                        <th class="sortable"
                                            data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}
                                        </th>
                                        <th class="sortable"
                                            data-sort="city">{{ trans('bct/projects::projects/model.general.city') }}
                                        </th>
                                        <th class="sortable"
                                            data-sort="state">{{ trans('bct/projects::projects/model.general.state') }}
                                        </th>
                                        <th class="sortable"
                                            data-sort="created_at">{{ trans('bct/projects::projects/model.general.created_at') }}
                                        </th>
                                        <th class="sortable"
                                            data-sort="status">{{ trans('bct/projects::projects/model.general.status_id') }}
                                        </th>
                                        <th class="sortable">{{ trans('bct/projects::projects/model.general.union') }}
                                        </th>
                                        <th class="sortable">{{ trans('bct/projects::projects/model.general.pa') }}
                                        </th>

                                        <th class="hidden-print">{{ trans('bct/projects::general/common.details') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>

                        </div>

                        <footer class="panel-footer clearfix hidden-print">

                            {{-- Grid: Pagination --}}
                            <div class="data-grid_pagination" data-grid="projects"></div>

                        </footer>

                        {{-- Grid: templates --}}
                        @include('bct/projects::contractors.grid.projects.results')
                        @include('bct/projects::contractors.grid.projects.pagination')
                        @include('bct/projects::contractors.grid.projects.filters')
                        @include('bct/projects::contractors.grid.projects.no_results')

                    </section>

                </div>

            </div>

        </div>

    </section>
@stop
