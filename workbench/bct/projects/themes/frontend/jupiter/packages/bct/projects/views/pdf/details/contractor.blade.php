<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    {{ Asset::queue('normalize', 'bct/projects::custom/css/normalize.css') }}
    {{ Asset::queue('custom-pdf-css', 'bct/projects::custom/css/bootstrap-grid-export.css') }}

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach
    <style>
        .text16{
            font-size:16px;
            font-weight:bold;
        }
        table
        {
            border-collapse:unset!important;
        }
        .pre-spaces {
            padding-left: 1em;
        }
        .table td:not(.td-label),
        div{
            overflow-wrap: break-word;
            word-wrap: break-word;

            -ms-word-break: break-all;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            box-sizing: border-box;
        }

        .col-padding {
            padding-left: -10px;
            padding-right: 10px;
        }
        .col-padding-right {
            padding-right: 10px;
        }
        .pull-left{
            float:left;
        }
        .border-right{
            height:150px;
        }
        .col-md-8{
            display:inline;
        }
        .col-md-8{
            width:65%;

            overflow-wrap: break-word;
            word-wrap: break-word;
            -ms-word-break: break-all;
            word-break: break-word;
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
    </style>
</head>
<body>

{{--Headers with logo--}}
@include('bct/projects::pdf/partials/header')

<div class="hr10"></div>

<div class="ext-profiles">

    <div class="panel-body">
        <div class="row">

            <div class="col-md-12">

                <table class="table table-bordered table-details">
                    <tbody>
                    <tr>
                        <td class="td-value">
                            <div class="row-table">
                                <div class="col-md-3 border-right pull-left">
                                    <div class="row">
                                        <div class="col-md-4"><b>Name:</b></div>
                                        <div class="col-md-8">{{ data_get($contractor,'name','') }}</div>
                                    </div>
                                    @if(isset($companies) && $companies->count()>0)
                                        @foreach($companies as $item)
                                            @if($item->type!=='Primary')
                                                <div class="row">
                                                    <div class="col-md-4">{{$item->type}}:</div>
                                                    <div class="col-md-8">{{$item->company}}</div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col-md-4"><b>Address:</b></div>
                                        <div class="col-md-8">{{ $contractor->getFullAddress() }}</div>
                                    </div>
                                </div>
                                <div class="col-md-3 border-right pull-left">
                                    <div class="row">
                                        <div class="col-md-4"><b>Phone:</b></div>
                                        <div class="col-md-8"><a href="tel:{{ data_get($contractor,'phone','') }}">{{ data_get($contractor,'phone','') }}</a></div>
                                    </div>
                                    @if(isset($phones) && $phones->count()>0)
                                        <?php
                                        $phones = $phones->filter(function ($item) {
                                            return $item['type'] != 'Primary';
                                        })->take(3);
                                        ?>
                                        @foreach($phones as $item)
                                            <div class="row">
                                                <div class="col-md-4">{{$item->type}}:</div>
                                                <div class="col-md-8"><a href="tel:{{$item->phone}}">{{$item->phone}}</a></div>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col-md-4"><b>Email:</b></div>
                                        <div class="col-md-8"><a href="mailto:{{ data_get($contractor,'email','') }}">{{ data_get($contractor,'email','') }}</a></div>
                                    </div>
                                    @if(isset($emails) && $emails->count()>0)
                                        @foreach($emails as $item)
                                            @if($item->type!=='Primary')
                                                <div class="row">
                                                    <div class="col-md-4">{{$item->type}}:</div>
                                                    <div class="col-md-8"><a href="mailto:{{$item->email}}">{{$item->email}}</a></div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-md-3 border-right pull-left">
                                    <div class="row">
                                        <div class="col-md-4"><b>License:</b></div>
                                        <div class="col-md-8">{{ data_get($contractor,'license','') }}</div>
                                    </div>
                                    @if(isset($licenses) && $licenses->count()>0)
                                        @foreach($licenses as $item)
                                            @if($item->type!=='Primary')
                                                <div class="row">
                                                    <div class="col-md-4">{{$item->type}}:</div>
                                                    <div class="col-md-8">{{$item->license}}</div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Union:</b>
                                        </div>
                                        <div class="col-md-8">
                                            @if($contractor->is_union)
                                                <span class="label label-success">Yes</span>
                                            @else
                                                <span class="label label-danger">No</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 pull-left">
                                    <div class="row">
                                        <div class="col-md-4"><b>Notes:</b></div>
                                        <div class="col-md-8">

                                            <div>{{ data_get($contractor,'notes','') }}</div>

                                            @if(isset($notes) && $notes->count()>0)
                                                @foreach($notes as $item)
                                                    @if($item->type!=='Primary')
                                                        <br/>
                                                        <div>{{$item->notes}}</div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>

                    </tr>
                    </tbody>
                </table>


            </div>

        </div>

        @if (!$files->isEmpty())
            <div><span class="text16">{{ trans("bct/projects::files/common.title") }}</span> <span class="text14">( {{ $files->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $files->count() }} )</span></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th class="td-label col-md-3 text-center">{{ trans('bct/projects::files/model.general.name') }}</th>
                            <th class="td-label col-md-5 text-center">{{ trans('bct/projects::files/model.general.description') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.type') }}</th>
                            <th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.date') }}</th>
                        </tr>
                        @foreach($files->take(5) as $file)
                            <tr>
                                <td class="td-value"><a href="{{ $file->download_url }}" download="{{ $file->name_full }}">{{ $file->name }}</a></td>
                                <td class="td-value text-center">{{ $file->description }}</td>
                                <td class="td-value text-center">{{ $file->extension }}</td>
                                <td class="td-value text-center">{{ date_correct_details_projects( $file->created_at ) }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        @endif

        @if (!$projects->isEmpty())

            <div><h1>{{ trans('bct/projects::projects/common.title') }}</h1></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ trans('bct/projects::projects/model.general.name') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.city') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.state') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.created_at') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.status_id') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.union') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.pa') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{ $project->name }}</td>
                                <td>{{ $project->city }}</td>
                                <td>{{ $project->state }}</td>
                                <td>{{ date_correct_details_projects($project->created_at) }}</td>
                                <td>{{ ($project->status) ? $project->status->name : '' }}</td>
                                <td>
                                    @if ($project->pivot->is_union)
                                        <span class="label label-success">Yes</span>
                                    @else
                                        <span class="label label-danger">No</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($project->pivot->is_agreement)
                                        <span class="label label-success">Yes</span>
                                    @else
                                        <span class="label label-danger">No</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @endif

</body>
</html>
