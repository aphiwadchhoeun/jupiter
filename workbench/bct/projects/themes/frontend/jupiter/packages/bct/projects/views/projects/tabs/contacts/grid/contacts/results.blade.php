<script type="text/template" data-grid="contacts" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.id %></td>
        <td><a href="<%- r.edit_uri %>"><%- r.name %></a></td>
        <td><a href="<%- r.edit_uri %>"><%- r.company %></a></td>
        <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
        <td><a href="mailto:<%- r.email %>"><%- r.email %></a></td>
        <td><%- r.city %></td>
        <td><a href="<%- r.view_uri %>" data-modal
               data-toggle="tooltip" class="btn btn-success btn-circle" data-placement="right"
               data-original-title="{{{ trans('bct/projects::projects/common.connect') }}}"><i
                        class="fa fa-plus fa-fw"></i></a></td>
    </tr>

    <% }); %>

</script>
