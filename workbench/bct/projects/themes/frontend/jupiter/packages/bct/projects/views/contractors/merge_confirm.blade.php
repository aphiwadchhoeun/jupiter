@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::contractors/common.title') }}
@stop
{{ Asset::queue('contractors-merge-form', 'bct/projects::contractors/css/merge_form.css', 'style') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}

{{ Asset::queue('index', 'bct/projects::contractors/js/merge.js', 'extension-base') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section id="information" class="panel panel-default panel-tabs">

        <header class="panel-heading hidden-print">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{ route('bct.projects.contractors.all') }}"
                                   data-toggle="tooltip" data-original-title="{{ trans('action.cancel') }}">
                                    <i class="fa fa-reply"></i> <span
                                            class="visible-xs-inline">{{ trans('action.cancel') }}</span>
                                </a>
                            </li>
                        </ul>

                        <span class="navbar-brand">{{ trans("bct/projects::general/common.merge_confirmation") }}</span>
                    </div>

                    <div class="pull-right hidden-print">

                        <a href="{{route('bct.projects.contractors.all')}}"
                           class="btn btn-danger">
                            {{ trans('bct/projects::general/common.cancel') }}
                        </a>

                        <a href="{{route('bct.projects.contractors.merge')}}"
                           class="btn btn-primary"
                           id="merge"
                           name="merge"
                           data-redirect="{{route('bct.projects.contractors.all')}}"
                           data-stay="{{$target_contractor->id}}"
                           data-merge="{{$removing_contractor->id}}">
                            {{ trans('bct/projects::general/common.confirm') }}
                        </a>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">
            <table class="table table-responsive table-bordered contractor-merge-table bg-gray">
                <tr>
                    <th colspan="4">{{ trans('bct/projects::contractors/common.removing_contractor') }}</th>
                </tr>
                <tr>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>Name:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($removing_contractor, 'name', '') }}
                        </div>
                        <div class="col-md-3">
                            <b>Address:</b>
                        </div>
                        <div class="col-md-9">
                            {{ $removing_contractor->getFullAddress() }}
                        </div>
                    </td>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>Phone:</b>
                        </div>
                        <div class="col-md-9">
                            <a href="tel:{{ data_get($removing_contractor,'phone','') }}">{{ data_get($removing_contractor,'phone','') }}</a>
                        </div>
                        <div class="col-md-3">
                            <b>Email:</b>
                        </div>
                        <div class="col-md-9">
                            <a href="mailto:{{ data_get($removing_contractor,'email','') }}">{{ data_get($removing_contractor,'email','') }}</a>
                        </div>
                    </td>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>License:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($removing_contractor, 'license', '') }}
                        </div>
                        <div class="col-md-3">
                            <b>EIN:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($removing_contractor, 'ein', '') }}
                        </div>
                        <div class="col-md-3">
                            <b>Union:</b>
                        </div>
                        <div class="col-md-9">
                            @if((bool)$removing_contractor->is_union)
                                <span class="label label-success">Yes</span>
                            @else
                                <span class="label label-danger">No</span>
                            @endif
                        </div>
                    </td>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>Notes:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($removing_contractor, 'notes', '') }}
                        </div>
                    </td>
                </tr>
            </table>

            <div class="col-md-12 text-center"><span class="lead"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></span></div>

            <table class="table table-responsive table-bordered contractor-merge-table">
                <tr>
                    <th colspan="4">{{ trans('bct/projects::contractors/common.target_contractor') }}</th>
                </tr>
                <tr>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>Name:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($target_contractor, 'name', '') }}
                        </div>
                        <div class="col-md-3">
                            <b>Address:</b>
                        </div>
                        <div class="col-md-9">
                            {{ $target_contractor->getFullAddress() }}
                        </div>
                    </td>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>Phone:</b>
                        </div>
                        <div class="col-md-9">
                            <a href="tel:{{ data_get($target_contractor,'phone','') }}">{{ data_get($target_contractor,'phone','') }}</a>
                        </div>
                        <div class="col-md-3">
                            <b>Email:</b>
                        </div>
                        <div class="col-md-9">
                            <a href="mailto:{{ data_get($target_contractor,'email','') }}">{{ data_get($target_contractor,'email','') }}</a>
                        </div>
                    </td>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>License:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($target_contractor, 'license', '') }}
                        </div>
                        <div class="col-md-3">
                            <b>EIN:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($target_contractor, 'ein', '') }}
                        </div>
                        <div class="col-md-3">
                            <b>Union:</b>
                        </div>
                        <div class="col-md-9">
                            @if((bool)$target_contractor->is_union)
                                <span class="label label-success">Yes</span>
                            @else
                                <span class="label label-danger">No</span>
                            @endif
                        </div>
                    </td>
                    <td class="col-md-3">
                        <div class="col-md-3">
                            <b>Notes:</b>
                        </div>
                        <div class="col-md-9">
                            {{ data_get($target_contractor, 'notes', '') }}
                        </div>
                    </td>
                </tr>
            </table>
        </div>

    </section>
@stop
