var Extension;

;(function(window, document, $, undefined)
{
	'use strict';
    var gridProjectAdd, gridProjectDel;
    var isBlocked = true;
    	var config = {
		preventHashChange: true,
		hash: false,
		events: {
			applying_dev: function(dg) {

				var tags = '';
				var many_tags = false;

				_.each(dg.applied_filters, function (filter) {

					var index = dg.applied_filters.indexOf(filter);

					if (filter.column === 'tags' && typeof(filter.value) !== 'undefined' && filter.value !== '') {

						var searchValue = filter.value;

						tags = searchValue.split(',');
						if(tags === searchValue){
							tags = searchValue.split(', ');
							if(tags !== searchValue){
								many_tags = true;
								dg.applied_filters.splice(index,1);
							}
						}
						else{
							many_tags = true;
							dg.applied_filters.splice(index,1);
						}

					}
				});
				if((tags !== '' || tags !== null) && many_tags){
					$.each(tags,function(index,tag){
						var obj = {column:'tags',value:tag,operator:''}
						dg.applied_filters.push(obj)
					});
				}

			}
		}
	};
	Extension = Extension || {
		Index: {},
	};

	// Initialize functions
	Extension.Index.init = function()
	{
        if (!gridProjectDel) {
            gridProjectDel = $.datagrid('project-del', '#data-grid-project-del', '.data-grid-project-del_pagination', '#data-grid-project-del_applied', $.extend(true,config,{
                events: {
                    fetched: function(){
                        if (!gridProjectAdd) {
                            gridProjectAdd = $.datagrid('project-add', '#data-grid-project-add', '.data-grid-project-add_pagination', '#data-grid-project-add_applied', config);
                        }
                    }
                }
            }));
        }

        
        
        
		Extension.Index.listeners();
	};

	// Add Listeners
	Extension.Index.listeners = function()
	{
		Platform.Cache.$body
			.on('click', '.add-to-list', Extension.Index.addProjectToList)
			.on('click', '.remove-from-list', Extension.Index.deleteProjectFromList)
			.on('click', '#unlock-projects', Extension.Index.unlockActions)
		;
	};

    
    
    Extension.Index.addProjectToList = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

        if(!isBlocked) {
            var id = $(this).parents('tr').find('input').val();
        }
        else {
            swal("Tabs blocked!", "Please unlock tabs first before!", "warning");
        }
        
        
		if (id)
		{
			$.ajax({
				type: 'POST',
				url: url,
				data: {
					action : 'attach',
					rows   : [id]
				},
				success: function(response)
				{
					gridProjectAdd.refresh();
                    gridProjectDel.refresh();
                   
				}
			});
		}
	};
    
    
    Extension.Index.deleteProjectFromList = function(event)
	{
		event.preventDefault();

		var url = window.location.origin + window.location.pathname;
        
        if(!isBlocked) {
            var id = $(this).parents('tr').find('input').val();
        }
        else {
            swal("Tabs blocked!", "Please unlock tabs first before!", "warning");
        }
		if (id)
		{
			$.ajax({
				type: 'POST',
				url: url,
				data: {
					action : 'detach',
					rows   : [id]
				},
				success: function(response)
				{
					gridProjectAdd.refresh();
                    gridProjectDel.refresh();
				}
			});
		}
        
        
	};
    
    Extension.Index.unlockActions = function(event)
	{
        if (isBlocked)  {
            $('#unlock-projects').hide();
            swal("You can now edit projects!", "", "success");
           
            $('#projects-to-add').removeClass('hidden');
            $('html, body').animate({
                        scrollTop: $("#please-find-text").offset().top
            }, 1500);
            isBlocked = false;
        }
	};

	// Job done, lets run.
	Extension.Index.init();

})(window, document, jQuery);
