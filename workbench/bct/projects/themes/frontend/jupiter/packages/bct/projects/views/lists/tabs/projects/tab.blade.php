<header class="panel-heading hidden-print">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#actions">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip go-to-details" data-toggle="tooltip"
                           data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $list->name}} {{ trans("bct/projects::lists/common.tabs.projects") }}</span>

            </div>

            @if ($has_access && !$currentUser->inRole('pp-guest'))
                <button id="unlock-projects" class="btn btn-success pull-right margin-right5">
                    {{{ trans("bct/projects::lists/common.edit_projects") }}}
                </button>

            @endif
        </div>

    </nav>

</header>

<div class="panel-body">
    <div class="row visible-print">
        <h4 class="overview-heading text-center visible-print">
            {{ data_get($list, 'name', '') }} -
            {{ trans("bct/projects::lists/common.title") }} {{ trans("bct/projects::lists/common.tabs.projects") }}
        </h4>
    </div>

    <div class="row">

        <div class="col-md-12">

            <section class="panel panel-default panel-grid">

                {{-- Grid: Header --}}
                <header class="panel-heading hidden-print">

                    <nav class="navbar navbar-default navbar-actions">

                        <div class="container-fluid">

                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#actions">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <span class="navbar-brand">{{ trans('bct/projects::projects/common.list_projects') }}</span>

                            </div>

                            {{-- Grid: Actions --}}
                            <div class="collapse navbar-collapse" id="actions">
                                <ul class="nav navbar-nav navbar-left download-container" data-grid="project-del">

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                           aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                            <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                            <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                        </ul>
                                    </li>

                                </ul>

                                {{-- Grid: Filters --}}
                                <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                                      data-grid="project-del" role="form">

                                    <div class="input-group">

                                        <span class="input-group-btn">

                                            <button class="btn btn-default" type="button" data-grid-calendar
                                                    id="list-project-filter-calendar"
                                                    data-range-filter="follow_up_date">
                                                <i class="fa fa-calendar"></i>
                                            </button>

                                        </span>

                                        <span class="input-group-btn">
                                            <select name="column" class="form-control" id="list-project-filter-select">
                                                <option value="name">{{ trans('bct/projects::projects/model.general.name') }}
                                                </option>
                                                <option value="city">{{ trans('bct/projects::projects/model.general.city') }}
                                                </option>
                                                <option value="zipcode">{{ trans('bct/projects::projects/model.general.zip_grid') }}
                                                </option>
                                                <option value="tags">{{ trans('bct/projects::projects/model.general.tags') }}
                                                </option>
                                            </select>
                                        </span>

                                        <input class="form-control" name="filter" type="text" id="list-project-filter-input-search"
                                               placeholder="{{{ trans('common.search') }}}">

                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit" id="list-project-filter-btn-search">
                                                <span class="fa fa-search"></span>
                                            </button>

                                            <button class="btn btn-default" data-grid="project-del" data-reset id="list-project-filter-btn-reset">
                                                <i class="fa fa-refresh fa-sm"></i>
                                            </button>

                                        </span>

                                    </div>

                                    <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-start>
                                    <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-end>

                                </form>

                            </div>

                        </div>

                    </nav>

                </header>

                <div class="panel-body">

                    {{-- Grid: Applied Filters --}}
                    <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                        <div id="data-grid-project-del_applied" class="btn-group" data-grid="project-del"></div>

                    </div>

                    {{-- Grid: Table --}}
                    <div class="table-responsive">

                        <table id="data-grid-project-del" class="table table-hover"
                               data-source="{{ route('bct.projects.lists.gridForProjectsTab', $list->id) }}"
                               data-filename="{{ $list->name}} {{ trans("bct/projects::lists/common.tabs.projects") }}"
                               data-grid="project-del">
                            <thead>
                            <tr>
                                <th class="sortable col-md-5"
                                    data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}
                                </th>
                                <th class="sortable col-md-2"
                                    data-sort="city">{{ trans('bct/projects::projects/model.general.city') }}
                                </th>
                                <th class="sortable col-md-1"
                                    data-sort="zipcode">{{ trans('bct/projects::projects/model.general.zip') }}
                                </th>
                                <th class="sortable col-md-2"
                                    data-sort="follow_up_date">{{ trans('bct/projects::projects/model.general.follow_up_date') }}
                                </th>
                                <th class="sortable col-md-2"
                                    data-sort="tags">{{ trans('bct/projects::projects/model.general.tags') }}
                                </th>
                                <th class="sortable col-md-1"
                                    data-sort="status">{{ trans('bct/projects::projects/model.general.status_id') }}
                                </th>
                                @if (!$currentUser->inRole('pp-guest'))
                                <th class="hidden-print col-md-1">{{ trans('bct/projects::general/common.actions') }}</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>

                    </div>

                </div>

                <footer class="panel-footer clearfix hidden-print">

                    {{-- Grid: Pagination --}}
                    <div class="data-grid-project-del_pagination" data-grid="project-del"></div>

                </footer>

                {{-- Grid: templates --}}
                @include('bct/projects::lists.tabs.projects.grid.project-del.results')
                @include('bct/projects::lists.tabs.projects.grid.project-del.pagination')
                @include('bct/projects::lists.tabs.projects.grid.project-del.filters')
                @include('bct/projects::lists.tabs.projects.grid.project-del.no_results')

            </section>

        </div>

    </div>

</div>
@if (!$has_access)
    <div class="hidden">
        @endif
        <div id="projects-to-add" class="hidden hidden-print">

            <div class="navbar-default">
                <span class="navbar-brand please-find-text"
                      id="please-find-text">{{ trans('bct/projects::lists/common.find_projects_tab') }}</span>
            </div>

            <div class="panel-body">

                <div class="row">

                    <div class="col-md-12">

                        <section class="panel panel-default panel-grid">

                            {{-- Grid --}}

                            {{-- Grid: Header --}}
                            <header class="panel-heading">

                                <nav class="navbar navbar-default navbar-actions">

                                    <div class="container-fluid">

                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                    data-target="#actions">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>

                                            <span class="navbar-brand">{{ trans('bct/projects::projects/common.available_projects') }}</span>

                                        </div>

                                        {{-- Grid: Actions --}}
                                        <div class="collapse navbar-collapse" id="actions">
                                            <ul class="nav navbar-nav navbar-left download-container" data-grid="project-add">

                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                                       aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                                        <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                                    </a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                                        <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                                    </ul>
                                                </li>

                                            </ul>


                                            {{-- Grid: Filters --}}
                                            <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                                                  data-grid="project-add" role="form">

                                                <div class="input-group">

                                                    <span class="input-group-btn">

                                                        <button class="btn btn-default" type="button" disabled>
                                                            {{{ trans('common.filters') }}}
                                                        </button>

                                                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                                                            aria-expanded="false">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>

                                                        <ul class="dropdown-menu" role="menu" data-filter-reset>
                                                            <li>
                                                                <a href="#" data-filter="status:{{{trans('bct/projects::general/common.active')}}}" id="list-available-project-filter-status-{{ trans('bct/projects::general/common.active') }}">
                                                                    <i class="fa fa-check-circle"></i> {{trans('bct/projects::general/common.active')}}
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href="#" data-filter="status:{{{trans('bct/projects::general/common.disabled')}}}" id="list-available-project-filter-status-{{ trans('bct/projects::general/common.disabled') }}">
                                                                    <i class="fa fa-times-circle"></i> {{trans('bct/projects::general/common.disabled')}}
                                                                </a>
                                                            </li>
                                                        </ul>

                                                        <button class="btn btn-default" type="button" data-grid-calendar
                                                                id="list-available-project-filter-calendar"
                                                                data-range-filter="follow_up_date">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>

                                                    </span>

                                                    <span class="input-group-btn">
                                                        <select name="column" class="form-control" id="list-available-project-filter-select">
                                                            <option value="name">{{ trans('bct/projects::projects/model.general.name') }}
                                                            </option>
                                                            <option value="city">{{ trans('bct/projects::projects/model.general.city') }}
                                                            </option>
                                                            <option value="zipcode">{{ trans('bct/projects::projects/model.general.zip_grid') }}
                                                            </option>
                                                            <option value="tags">{{ trans('bct/projects::projects/model.general.tags') }}
                                                            </option>
                                                        </select>
                                                    </span>

                                                    <input class="form-control" name="filter" type="text" id="list-available-project-filter-input-search"
                                                           placeholder="{{{ trans('common.search') }}}">

                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="submit" id="list-available-project-filter-btn-search">
                                                            <span class="fa fa-search"></span>
                                                        </button>

                                                        <button class="btn btn-default" data-grid="project-add" data-reset id="list-available-project-filter-btn-reset">
                                                            <i class="fa fa-refresh fa-sm"></i>
                                                        </button>

                                                    </span>

                                                </div>

                                                <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-start>
                                                <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-end>


                                            </form>


                                        </div>

                                    </div>

                                </nav>

                            </header>

                            <div class="panel-body">

                                {{-- Grid: Applied Filters --}}
                                <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                                    <div id="data-grid-project-add_applied" class="btn-group" data-grid="project-add"></div>

                                </div>


                                {{-- Grid: Table --}}
                                <div class="table-responsive">

                                    <table id="data-grid-project-add" class="table table-hover"
                                           data-source="{{ route('bct.projects.lists.gridForProjectsTab', [$list->id, 'reverse' => true]) }}"
                                           data-filename="{{ trans('bct/projects::projects/common.title') }}"
                                           data-grid="project-add">
                                        <thead>
                                        <tr>
                                            <th class="sortable col-md-5"
                                                data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}
                                            </th>
                                            <th class="sortable col-md-2"
                                                data-sort="city">{{ trans('bct/projects::projects/model.general.city') }}
                                            </th>
                                            <th class="sortable col-md-1"
                                                data-sort="zipcode">{{ trans('bct/projects::projects/model.general.zip') }}
                                            </th>
                                            <th class="sortable col-md-2"
                                                data-sort="follow_up_date">{{ trans('bct/projects::projects/model.general.follow_up_date') }}
                                            </th>
                                            <th class="sortable col-md-2"
                                                data-sort="tags">{{ trans('bct/projects::projects/model.general.tags') }}
                                            </th>
                                            <th class="sortable col-md-1"
                                                data-sort="status">{{ trans('bct/projects::projects/model.general.status_id') }}
                                            </th>
                                            <th class="col-md-1">{{ trans('bct/projects::general/common.actions') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>

                                </div>

                            </div>


                            <footer class="panel-footer clearfix">

                                {{-- Grid: Pagination --}}
                                <div class="data-grid-project-add_pagination" data-grid="project-add"></div>

                            </footer>

                            {{-- Grid: templates --}}
                            @include('bct/projects::lists.tabs.projects.grid.project-add.results')
                            @include('bct/projects::lists.tabs.projects.grid.project-add.pagination')
                            @include('bct/projects::lists.tabs.projects.grid.project-add.filters')
                            @include('bct/projects::lists.tabs.projects.grid.project-add.no_results')

                        </section>

                    </div>

                </div>

            </div>
        </div>
        @if (!$has_access)
    </div>
@endif