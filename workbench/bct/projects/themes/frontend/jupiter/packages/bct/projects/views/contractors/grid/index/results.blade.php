<script type="text/template" data-grid="contractors" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        @if ($currentUser->inRole('admin') || $currentUser->inRole('repadmin') || $currentUser->inRole('super-repadmin'))
            <td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%- r.id %>"></td>
        @endif
        <td><%- r.id %></td>
        <td><%- r.name %></td>
        <td><%- r.city %></td>
        <td><%- r.state %></td>
        <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
        <td><%- r.license %></td>
        <td><span class="label label-<%- r.is_union_color %>"><%- r.is_union_text %></span></td>
        <td><span class="label label-<%- r.active_color %>"><%- r.active_text %></span></td>
        <td><a href="<%- r.view_uri %>" class="btn btn-primary btn-circle btn-details"
               data-id="<%- r.id %>"
               data-name="<%- r.name %>"
               data-toggle="tooltip" data-placement="right"
               data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                        class="fa fa-book fa-fw"></i></a></td>
    </tr>

    <% }); %>

</script>
