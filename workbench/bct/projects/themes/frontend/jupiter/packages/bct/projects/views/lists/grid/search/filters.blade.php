<script type="text/template" data-grid="lists-search" data-template="filters">

	<% _.each(filters, function(f) { %>

		<button class="btn btn-default">

			<% if (f.from !== undefined && f.to !== undefined) { %>

				<%if (f.column === 'z_project_list.due_date') { %>

					{{ trans('bct/projects::lists/model.general.due_date') }}: <em><%- f.from %> - <%- f.to %></em>

				<% } else if (/[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.from) && /[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.to)) { %>

					<%- f.column.replace(/_/ig, ' ').capitalize() %>: <em><%- moment(f.from).format('MMM DD, YYYY') %> - <%- moment(f.to).format('MMM DD, YYYY') %></em>

				<% } else { %>

					<%- f.label %> <em><%- f.from %> - <%- f.to %></em>

				<% } %>

			<% } else if (f.col_mask !== undefined && f.val_mask !== undefined) { %>

				<%- f.col_mask %> <em><%- f.val_mask %></em>

			<% } else { %>

				<% if (f.column === 'all') { %>

					<%- f.value %>

				<% } else if (f.column === 'z_project_list.is_active' && f.value === '1') { %>

					{{ trans('bct/projects::general/common.active') }}

				<% } else if (f.column === 'z_project_list.is_active' && f.value === '0') { %>

					{{ trans('bct/projects::general/common.disabled') }}

				<% } else if (f.column === 'z_project_list.name') { %>

					<%- f.value %> in <em>{{ trans('bct/projects::lists/model.general.name') }}</em>

				<% } else if (f.column === 'z_projects_group.name') { %>

					<%- f.value %> in <em>{{ trans('bct/projects::lists/model.general.group_id') }}</em>

				<% } else if (f.column === 'z_project_list.category') { %>

					<%- f.value %> in <em>{{ trans('bct/projects::lists/model.general.category') }}</em>

				<% } else { %>

					<%- f.value %> in <em><%- f.column.capitalize() %></em>

				<% } %>

			<% } %>

			&nbsp;<span><i class="fa fa-times-circle"></i></span>

		</button>

	<% }); %>

</script>
