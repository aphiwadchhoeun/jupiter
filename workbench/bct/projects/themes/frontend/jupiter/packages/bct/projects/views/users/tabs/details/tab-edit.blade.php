
{{ Asset::queue('typeahead', 'bower_components/typeahead.js-bootstrap-css/typeaheadjs.css') }}
{{ Asset::queue('typeahead', 'bower_components/typeahead.js/dist/typeahead.jquery.js', 'jquery') }}
{{ Asset::queue('bloodhound', 'bower_components/typeahead.js/dist/bloodhound.js', 'typeahead') }}

<header class="panel-heading">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip" href="{{ route('bct.projects.users.link',array('id'=>$user->id)) }}"
                           data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $user->first_name }} {{ $user->last_name }} {{ trans("bct/projects::general/common.details") }}</span>
            </div>

            <div class="pull-right">
                @if ($user->exists)
                    <a href="{{ route('bct.projects.users.delete', $user->id) }}"
                       id="user_delete"
                       name="user_delete"
                       data-method="delete" data-token="{{ csrf_token() }}" data-swal="true"
                       class="btn btn-danger"
                       data-original-title="{{{ trans('action.delete') }}}" type="delete">
                        <i class="fa fa-trash-o fa-fw"></i>{{{ trans('action.delete') }}}</a>
                @endif

                <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                        data-original-title="{{{ trans('action.save') }}}">
                    <i class="fa fa-save fa-fw"></i>{{{ trans('action.save') }}}
                </button>
            </div>

        </div>

    </nav>

</header>
<div class="panel-body">
    <section class="panel panel-default panel-tabs">

        {{-- Form --}}
        <form id="content-form" action="{{ route("bct.projects.users.edit", $user->id) }}" role="form" method="post"
              data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="panel-body">

                <div class="col-md-12">

                    {{-- Form: General --}}
                    <fieldset>
                        <div class="row">

                            {{--add more form-group for other fields, just keep the same uniform--}}
                            <div class="form-group{{ Alert::onForm('first_name', ' has-error') }}">

                                <label for="first_name" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.first_name') }}}
                                </label>

                                <input type="text" class="form-control" name="first_name" id="first_name"
                                       placeholder="{{{ trans('bct/projects::users/model.general.first_name_help') }}}"
                                       value="{{{ input()->old('first_name', $user->first_name) }}}">

                                <span class="help-block">{{{ Alert::onForm('first_name') }}}</span>

                            </div>

                            <div class="form-group{{ Alert::onForm('last_name', ' has-error') }}">

                                <label for="last_name" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.last_name') }}}
                                </label>

                                <input type="text" class="form-control" name="last_name" id="last_name"
                                       placeholder="{{{ trans('bct/projects::users/model.general.last_name_help') }}}"
                                       value="{{{ input()->old('last_name', $user->last_name) }}}">

                                <span class="help-block">{{{ Alert::onForm('last_name') }}}</span>

                            </div>

                            <div class="form-group">
                                <input type="hidden" class="form-control" name="email" id="email"
                                       value="{{{ input()->old('email', $user->email) }}}">
                            </div>

                            <div class="form-group{{ Alert::onForm('role', ' has-error') }}">

                                <label for="role" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.role') }}}
                                </label>

                                <select class="form-control" name="role" id="role">
                                    <option value="">{{{ trans('bct/projects::users/model.general.role_help') }}}
                                    </option>
                                    <?php
                                    $selected = $user->roles()->first();
                                    ?>

                                    @foreach($user_roles as $role)
                                        <option value="{{ $role->id }}"
                                                {{ ($selected->id == $role->id)?'selected':'' }}
                                                >{{ $role->name }}</option>
                                    @endforeach
                                </select>

                                <span class="help-block">{{{ Alert::onForm('role') }}}</span>

                            </div>

                            <div class="form-group{{ Alert::onForm('phone', ' has-error') }}">

                                <label for="phone" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.phone') }}}
                                </label>

                                <input type="text" class="form-control" name="phone" id="phone"
                                       placeholder="{{{ trans('bct/projects::users/model.general.phone_help') }}}"
                                       value="@if ($user->profile){{{ $user->profile->business_phone }}} @else {{ input()->old('phone') }} @endif">

                                <span class="help-block">{{{ Alert::onForm('phone') }}}</span>

                            </div>

                            <div class="form-group{{ Alert::onForm('local', ' has-error') }}">

                                <label for="phone" class="control-label">
                                    {{{ trans('bct/projects::users/model.general.local') }}}
                                </label>

                                <input type="text" class="form-control" name="local" id="local"
                                       placeholder="{{{ trans('bct/projects::users/model.general.local_help') }}}"
                                       maxlength="30"
                                       value="@if ($user->profile){{{ $user->profile->business_local }}}@endif">

                                <span class="help-block">{{{ Alert::onForm('local') }}}</span>

                            </div>


                        </div>

                    </fieldset>

                </div>

            </div>

        </form>

    </section>
</div>