/**
 * Created by Aphiwad on 10/28/2015.
 */
var projectContactSummary;

if (!projectContactSummary) {
    var options = {
        events: {
            'fetching': function () {
                projectContactSummary.refresh();
                $('#contacts-counter').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            'fetched': function (result) {
                var num = (result.pagination.filtered > 5) ? 5 : result.pagination.filtered;
                $('#contacts-counter').text('(' + num + ' of ' + result.pagination.total + ')');
            }
        }
    };

    projectContactSummary = $.datagrid(
        'project-contacts-summary',
        '#project-contacts-summary',
        'null',
        'null',
        options);
}