@extends('layouts/default')

@section('scripts')
    @parent

    @include('bct/projects::php_vars_to_js')
    {{ Asset::queue('profiles-custom-js', 'bct/projects::custom/js/app.js', 'jquery') }}

@endsection

@section('styles')
    @parent

    {{ Asset::queue('profiles-custom-css', 'bct/projects::custom/css/app.css') }}
@endsection

@section('page')
    @parent

    <div class="ext-profiles">
        @yield('page-extension')
    </div>

@stop

@section('footer-youtube-link')
    <a href="{{ route('bct.projects.help') }}">
        <img src="{{ Asset::getUrl('app/img/youtube_icon.png') }}" alt="Help" height="15px">
        <small><i>Youtube Tutorial Videos</i></small>
    </a>
@endsection

{{ Asset::queue('extension-base', 'bct/projects::ExtensionBase.js', 'platform') }}
