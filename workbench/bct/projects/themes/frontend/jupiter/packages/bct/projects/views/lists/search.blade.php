@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
{{ Asset::queue('search', 'bct/projects::lists/css/search.css', 'style') }}

{{ Asset::queue('validate', 'platform/js/parsley.min.js', 'jquery') }}
{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
{{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['jquery', 'moment']) }}
{{ Asset::queue('index', 'bct/projects::lists/js/search.js', 'extension-base') }}


{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-grid">

        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">

                    {{-- Grid --}}
                    <section class="panel panel-default panel-grid">

                        {{-- Grid: Header --}}
                        <header class="panel-heading">

                            <nav class="navbar navbar-default navbar-actions">

                                <div class="container-fluid">

                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                data-target="#step1-actions">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        @if (!$currentUser->inRole('pp-guest'))
                                        <span class="navbar-brand">{{ trans('bct/projects::lists/common.routes_step_1') }}</span>
                                        @else
                                        <span class="navbar-brand">{{ trans('bct/projects::lists/common.title') }}</span>
                                        @endif
                                    </div>

                                    {{-- Grid: Actions --}}
                                    <div class="collapse navbar-collapse" id="step1-actions">

                                        <ul class="nav navbar-nav navbar-left download-container">

                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown"
                                                   role="button" aria-expanded="false"
                                                   data-original-title="{{{ trans('action.export') }}}">
                                                    <i class="fa fa-download"></i> <span
                                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i>
                                                            CSV</a></li>
                                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>

                                        {{-- Grid: Filters --}}
                                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                                              data-grid="lists-search" role="form">


                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" disabled>
                                                        {{{ trans('common.status') }}}
                                                    </button>
                                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                                            aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                                        <li>
                                                            <a href="#" data-filter="z_project_list.is_active:1">
                                                                <i class="fa fa-check-circle"></i> {{ trans('bct/projects::general/common.active') }}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#" data-filter="z_project_list.is_active:0">
                                                                <i class="fa fa-times-circle"></i> {{ trans('bct/projects::general/common.disabled') }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </span>
                                                <span class="input-group-btn">
                    
                                                    <button class="btn btn-default" type="button" data-grid-calendar
                                                            data-range-filter="z_project_list.due_date">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                    

                                                </span>
                    
                                                <span class="input-group-btn">
                                                    <select name="column" class="form-control">
                                                        <option value="z_project_list.name">{{ trans('bct/projects::lists/model.general.name') }}
                                                        </option>
                                                        <option value="z_projects_group.name">{{ trans('bct/projects::lists/model.general.group_id') }}
                                                        </option>
                                                        <option value="z_project_list.category">{{ trans('bct/projects::lists/model.general.category') }}
                                                        </option>
                                                    </select>
                                                </span>

                                                <input class="form-control" name="filter" type="text"
                                                       placeholder="{{{ trans('common.search') }}}">
                    
                                                <span class="input-group-btn">
                    
                                                    <button class="btn btn-default" type="submit">
                                                        <span class="fa fa-search"></span>
                                                    </button>
                    
                                                    <button class="btn btn-default" data-grid="lists-search" data-reset>
                                                        <i class="fa fa-refresh fa-sm"></i>
                                                    </button>
                    
                                                </span>

                                            </div>

                                            <input type="hidden" data-range-filter="z_project_list.due_date" data-format="MMM-DD-YYYY" data-range-start></input>
                                            <input type="hidden" data-range-filter="z_project_list.due_date" data-format="MMM-DD-YYYY" data-range-end></input>


                                        </form>


                                        <div class="nav navbar-nav navbar-right">
                                            @if($has_access)
                                                <a href="{{ route('bct.projects.lists.create') }}"
                                                   id="list_new"
                                                   name="list_new"
                                                   class="btn btn-success pull-right margin-right5"
                                                   data-toggle="tooltip"
                                                   data-original-title="{{{ trans('action.create') }}}" data-modal>
                                                    <i class="fa fa-plus fa-fw"></i>
                                                </a>
                                            @endif
                                        </div>

                                    </div>

                                </div>

                            </nav>

                        </header>

                        <div class="panel-body">

                            {{-- Grid: Applied Filters --}}
                            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                                <div id="data-grid-lists_applied" class="btn-group" data-grid="lists-search"></div>

                            </div>


                            {{-- Grid: Table --}}
                            <div class="table-responsive">

                                {{-- Grid: Table --}}
                                <div class="table-responsive">

                                    <table id="list-data-grid" class="table table-hover"
                                           data-source="{{ route('bct.projects.lists.gridForUsers') }}"
                                           data-filename="{{ trans('bct/projects::lists/common.title') }}"
                                           data-grid="lists-search">
                                        <thead>
                                        <tr>
                                            @if (!$currentUser->inRole('pp-guest'))
                                            <th></th>
                                            @endif
                                            <th class="sortable col-md-4"
                                                data-sort="name">{{ trans('bct/projects::lists/model.general.name') }}
                                            </th>
                                            <th class="sortable"
                                                data-sort="group">{{ trans('bct/projects::lists/model.general.group_id') }}
                                            </th>
                                            <th class="sortable"
                                                data-sort="category">{{ trans('bct/projects::lists/model.general.category') }}
                                            </th>
                                            <th class="sortable"
                                                data-sort="due_date">{{ trans('bct/projects::lists/model.general.due_date') }}
                                            </th>
                                            <th class="sortable"
                                                data-sort="is_active">{{ trans('bct/projects::lists/model.general.list_status') }}
                                            </th>
                                            <th>{{ trans('bct/projects::general/common.details') }}</th>

                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>

                                </div>

                            </div>

                        </div>


                        <footer class="panel-footer clearfix">

                            {{-- Grid: Pagination --}}
                            <div class="data-grid-lists_pagination" data-grid="lists-search"></div>

                        </footer>

                        {{-- Grid: templates --}}
                        @include('bct/projects::lists.grid.search.results')
                        @include('bct/projects::lists.grid.search.pagination')
                        @include('bct/projects::lists.grid.search.filters')
                        @include('bct/projects::lists.grid.search.no_results')

                    </section>

                </div>

            </div>

        </div>
        @if (!$currentUser->inRole('pp-guest'))
        <div class="panel-body projects-grid">

            <div class="row">

                <div class="col-md-12">

                    {{-- Grid --}}
                    <section class="panel panel-default panel-grid">

                        {{-- Grid: Header --}}
                        <header class="panel-heading">

                            <nav class="navbar navbar-default navbar-actions">

                                <div class="container-fluid">

                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                data-target="#step2-actions">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>

                                        <span class="navbar-brand">{{ trans('bct/projects::lists/common.routes_step_2')}}</span>

                                    </div>

                                    {{-- Grid: Actions --}}
                                    <div class="collapse navbar-collapse" id="step2-actions">

                                        <ul class="nav navbar-nav navbar-left download-container">

                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown"
                                                   role="button" aria-expanded="false"
                                                   data-original-title="{{{ trans('action.export') }}}">
                                                    <i class="fa fa-download"></i> <span
                                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i>
                                                            CSV</a></li>
                                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>

                                        {{-- Grid: Filters --}}
                                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8"
                                              data-search data-grid="projects" role="form">

                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" disabled>
                                                        {{{ trans('common.status') }}}
                                                    </button>

                                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                                            aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                                        <li>
                                                            <a href="#" data-filter="z_project_list_project_join.is_active:0">
                                                                <i class="fa fa-check-circle"></i> {{ trans('bct/projects::general/common.visited') }}
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#" data-filter="z_project_list_project_join.is_active:1">
                                                                <i class="fa fa-times-circle"></i> {{ trans('bct/projects::general/common.not_visited') }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </span>
                                                <span class="input-group-btn">

                                                    <button class="btn btn-default" type="button" data-grid-calendar
                                                            data-range-filter="follow_up_date">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>


                                                </span>

                                                <span class="input-group-btn">
                                                    <select name="column" class="form-control">
                                                        <option value="name">{{ trans('bct/projects::projects/model.general.name') }}
                                                        </option>
                                                        <option value="city">{{ trans('bct/projects::projects/model.general.city') }}
                                                        </option>
                                                        <option value="tags">{{ trans('bct/projects::general/common.tags') }}
                                                    </select>
                                                </span>

                                                <input class="form-control" name="filter" type="text"
                                                       placeholder="{{{ trans('common.search') }}}">

                                                <span class="input-group-btn">

                                                    <button class="btn btn-default" type="submit">
                                                        <span class="fa fa-search"></span>
                                                    </button>

                                                    <button class="btn btn-default" data-grid="projects" data-reset>
                                                        <i class="fa fa-refresh fa-sm"></i>
                                                    </button>

                                                </span>

                                            </div>

                                            <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-start>
                                            <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-end>



                                        </form>

                                        <div id="map-selected">
                                            <button id="submit-btn" class="btn btn-success pull-right"
                                                    style="margin-top:4px" data-toggle="tooltip"
                                                    data-submit-href="{{ route('bct.projects.routes.submit.route') }}"
                                                    data-original-title="{{{ trans('bct/projects::lists/common.map_selected') }}}">
                                                <i class="fa fa-map-marker fa-fw"></i>{{ trans('bct/projects::lists/common.map_selected') }}
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </nav>

                        </header>

                        <div class="panel-body">

                            {{-- Grid: Applied Filters --}}
                            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                                <div id="data-grid-projects_applied" class="btn-group" data-grid="projects"></div>

                            </div>


                            {{-- Grid: Table --}}
                            <div class="table-responsive">

                                {{-- Grid: Table --}}
                                <div class="table-responsive">
                                    {{-- Form: CSRF Token --}}
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <table id="project-data-grid" class="table table-hover" data-grid="projects" data-filename="{{ trans('bct/projects::projects/common.title') }}">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th class="sortable col-md-4"
                                                data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}
                                            </th>
                                            <th class="sortable col-md-2"
                                                data-sort="city">{{ trans('bct/projects::projects/model.general.city') }}
                                            </th>
                                            <th class="sortable col-md-1-5"
                                                data-sort="follow Up Date">{{ trans('bct/projects::projects/model.general.follow_up_date') }}
                                            </th>
                                            <th class="sortable col-md-1-5"
                                                data-sort="last Visit">{{ trans('bct/projects::projects/model.general.last_visit') }}
                                            </th>
                                            <th class="sortable col-md-2"
                                                data-sort="tags">{{ trans('bct/projects::general/common.tags') }}
                                            </th>
                                            <th class="sortable col-md-1"
                                                data-sort="status">{{ trans('bct/projects::projects/model.general.status_id') }}
                                            </th>
                                            <th>{{ trans('bct/projects::general/common.details') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                            </div>

                        </div>


                        <footer class="panel-footer clearfix">

                            {{-- Grid: Pagination --}}
                            <div class="data-grid-projects_pagination" data-grid="projects"></div>

                        </footer>

                        {{-- Grid: templates --}}
                        @include('bct/projects::lists.grid.projects.results')
                        @include('bct/projects::lists.grid.projects.pagination')
                        @include('bct/projects::lists.grid.projects.filters')
                        @include('bct/projects::lists.grid.projects.no_results')

                    </section>

                </div>

            </div>

        </div>
        @endif
    </section>
@stop
