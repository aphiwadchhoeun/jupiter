/**
 * Created by Aphiwad on 10/28/2015.
 */
var projectContractorSummary;
var unionUrl = $('#contractor-summary').data('union-value-url');


if (!projectContractorSummary) {
    var options = {
        events: {
            'fetching': function () {
                getUnionValue();
                $('#contractors-counter').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            'fetched': function (result) {
                var num = (result.pagination.filtered > 5) ? 5 : result.pagination.filtered;
                $('#contractors-counter').text('(' + num + ' of ' + result.pagination.total + ')');
            }
        }
    };

    projectContractorSummary = $.datagrid(
        'project-contractors-summary',
        '#project-contractors-summary',
        'null',
        'null',
        options);
}

function getUnionValue() {
    $('#union-value').html('<i class="fa fa-spinner fa-spin"></i>');
    $.ajax({
        url: unionUrl,
        success: function (result) {
            $('#union-value').text(result);
        }
    });
    projectContractorSummary.refresh();
}
