@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
{{ Asset::queue('search-css', 'bct/projects::projects/css/search.css') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('index', 'bct/projects::projects/js/search.js', ['extension-base', 'markercluster-js']) }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
    <script type="text/javascript"
            src="{{ URL::asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyBvReYkUqZuyu4HLSHepBRsGax30FnaWnI&libraries=places&language=en') }}"></script>
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section  class="panel panel-default projects-search-section">

        <div class="panel-heading">
            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{ route('bct.projects.projects.all') }}"
                                   data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                                    <i class="fa fa-reply"></i> <span
                                            class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                </a>
                            </li>
                        </ul>
                        <div class="navbar-brand over-lead">{{ trans('bct/projects::projects/common.title') }} {{ trans("action.create") }}</div><br/>
                        <div class="lead text-bold under-navbar">{{ trans('bct/projects::projects/message.create_instruction_step1') }}</div>



                    </div>

                </div>

            </nav>
        </div>

        <div class="panel-body">

            <div class="col-md-12">

                {{-- Form: General --}}
                <fieldset>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" id="searchmap" value="" class="form-control"
                                       placeholder="{{ trans('bct/projects::projects/model.general.map') }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <a href="#" id="search-btn"
                               class="btn btn-primary" data-toggle="tooltip" disabled
                               data-original-title="{{{ trans('bct/projects::projects/model.general.search') }}}">
                                <i class="fa fa-search fa-fw"></i></a>
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group">
                            <input type="hidden" id="lat" value="">
                            <input type="hidden" id="lng" value="">
                            <input type="hidden" id="radius" value="0.5">

                            <div id="map-canvas"></div>
                        </div>

                    </div>

                </fieldset>

            </div>

        </div>

    </section>

    <section class="panel panel-default projects-search-section">
        {{-- Grid: Header --}}
        <header class="panel-heading">

            <span class="lead text-bold">{{ trans('bct/projects::projects/message.create_instruction_step2') }}</span>

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::projects/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{ trans('action.export') }}">
                                    <i class="fa fa-download"></i> <span
                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="projects" role="form">

                            <div class="input-group">

							<span class="input-group-btn">

								<button class="btn btn-default" type="button" disabled>
                                    {{{ trans('common.filters') }}}
                                </button>

								<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

								<ul class="dropdown-menu" role="menu">
                                    <?php
                                    foreach (\Bct\Projects\Models\Status::all() as $status) {
                                        if ($status->name == 'Completed') continue;
                                        echo '<li>
                                                <a href="#" data-filter="status..id:' . $status->id . '">
                                                    <i class="fa fa-check-circle"></i> ' . $status->name . '
                                                </a>
                                            </li>';
                                    }
                                    ?>
                                </ul>

							</span>

                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit">
                                    <span class="fa fa-search"></span>
                                </button>

								<button class="btn btn-default" data-grid="projects" data-reset>
                                    <i class="fa fa-refresh fa-sm"></i>
                                </button>

							</span>

                            </div>

                        </form>


                        <div class="pull-right">

                            <a href="{{ route('bct.projects.projects.create') }}" id="create-new-project"
                               class="btn btn-success" data-toggle="tooltip" disabled
                               data-original-title="{{{ trans('bct/projects::projects/common.not_found') }}}">
                                <i class="fa fa-plus fa-fw"></i></a>

                        </div>
                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="projects"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover"
                       data-source="{{ route('bct.projects.projects.newproject') }}" data-grid="projects">
                    <thead>
                    <tr>
                        <th class="sortable" data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}
                        </th>
                        <th class="sortable"
                            data-sort="description">{{ trans('bct/projects::projects/model.general.description') }}
                        </th>
                        <th class="sortable"
                            data-sort="follow Up Date">{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}
                        </th>
                        <th>{{ trans('bct/projects::general/common.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div id="data-grid_pagination" data-grid="projects"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::projects.grid.search.results')
        @include('bct/projects::projects.grid.search.pagination')
        @include('bct/projects::projects.grid.search.filters')
        @include('bct/projects::projects.grid.search.no_results')

    </section>

@stop