var ProjectVisitExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectVisitExtension = Object.create(ExtensionBase);
    ProjectVisitExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-visit'
    });

    // Initialize functions
    ProjectVisitExtension.Index.init = function () {
        ProjectVisitExtension.Index.datePicker(ProjectVisitExtension);
        ProjectVisitExtension.Index.dataGrid();
        ProjectVisitExtension.Index.listeners();
        ProjectVisitExtension.Index.datePicker.on('show.daterangepicker',function(){
            showDirectionPP('Created');
        });
    };

    // Add Listeners
    ProjectVisitExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '#visits [data-grid-row]', ProjectVisitExtension.Index.checkRow)
            .on('click', '#visits [data-grid-row] a', ProjectVisitExtension.Index.titleClick)
            .on('click', '#visits [data-grid-checkbox]', ProjectVisitExtension.Index.checkboxes)
            .on('click', '.btn-approve', ProjectVisitExtension.Index.approveOrunapproveBtn)
            .on('click', '.btn-unapprove', ProjectVisitExtension.Index.approveOrunapproveBtn)
        ;
    };

    // Ignore row selection on title click
    ProjectVisitExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    // Handle Data Grid checkboxes
    ProjectVisitExtension.Index.checkboxes = function(event)
    {
        event.stopPropagation();

        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all')
        {
            $('#visits [data-grid-checkbox]').not(this).prop('checked', this.checked);

            $('#visits [data-grid-row]').not(this).toggleClass('active', this.checked);
        }

        $(this).parents('[data-grid-row]').toggleClass('active');

        ProjectVisitExtension.Index.bulkStatus();
    };

    ProjectVisitExtension.Index.exporterStatus = function (grid) {
        $('#project-visit-exporter').toggleClass('hide', grid.pagination.filtered == 0);
        $('#project-visit-exporter').parent().toggleClass('hide', grid.pagination.filtered == 0);

        return this;
    };

    ProjectVisitExtension.Index.bulkStatus = function()
    {
        var rows = $('#visits [data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

        var checked = $('#visits [data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;
        var checked_and_not_approve = $('#visits [data-not_approved][data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;
        var checked_and_approve = $('[data-grid-checkbox]:not([data-not_approved]):checked').not('[data-grid-checkbox="all"]').length;

        $('#visits [data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);
        $('#visits [data-grid-bulk-action]').toggleClass('disabled', ! checked);

        if (checked > 0)
        {
            $('#visits [data-grid-bulk-action="delete"]').attr('data-modal', true);
        }
        else
        {
            $('#visits [data-grid-bulk-action="delete"]').removeAttr('data-modal');
        }

        if (checked_and_not_approve > 0) {
            $('#visits .btn-approve').removeClass('disabled');
        } else {
            $('#visits .btn-approve').addClass('disabled');
        }

        if (checked_and_approve > 0) {
            $('.btn-unapprove').removeClass('disabled');
        } else {
            $('.btn-unapprove').addClass('disabled');
        }

        $('#visits [data-grid-checkbox="all"]')
            .prop('disabled', rows < 1)
            .prop('checked', rows < 1 ? false : rows === checked)
        ;
    };

    // Handle Data Grid row checking
    ProjectVisitExtension.Index.checkRow = function()
    {
        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', ! checkbox.prop('checked'));

        ProjectVisitExtension.Index.bulkStatus();
    };

    ProjectVisitExtension.Index.approveOrunapproveBtn = function(e) {
        e.preventDefault();

        var url = $(this).data('url');

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                rows   : [ $(this).data('visit-id') ]
            },
            success: function(response)
            {
                $('#modal-form').modal('hide');
                ProjectVisitExtension.Index.Grid.refresh();
            }
        });

    };

    ProjectVisitExtension.Index.approveRowsAction = function(e) {
        e.preventDefault();

        var checked_and_not_approve = $('#visits [data-not_approved][data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;

        if (checked_and_not_approve > 0) {

            var rows = $.map($('#visits [data-not_approved][data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
            {
                return +event.value;
            });
            var url = $(this).data('url');

            if (rows.length > 0)
            {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        rows   : rows
                    },
                    success: function(response)
                    {
                        ProjectVisitExtension.Index.Grid.refresh();
                    }
                });
            }

        }
    };

    // Data Grid initialization
    ProjectVisitExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            callback: function (data) {
                ProjectVisitExtension.Index
                    .exporterStatus(data);
            }
        };

        ProjectVisitExtension.Index.Grid = $.datagrid('project-visit', '#project-visit-data-grid', '.project-visit-data-grid_pagination', '#project-visit-data-grid_applied', config);
    };

    // Job done, lets run.
    ProjectVisitExtension.Index.init();

})(window, document, jQuery);

$(document.body).on('click', '.visits-add-btn', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                var stages = $("#stages");
                var users = $("#users");
                var weather = $("#weather");
                var workers_count = $("#workers_count");
                var followupDate = form_modal.find("#follow_up_date");
                var lists = $("#project_lists");
                var reason = $("#reason");
                var visit_date = $("#visit_date");
                var visit_time = $("#visit_time");
                var submit_btn = $('#submit-btn-modal');

                workers_count.numeric({
                    negative: false
                }).keypress(function(e){
                    if (this.value.length == 0 && e.which == 48 ){
                        return false;
                    }
                });

                if (stages) stages.select2();
                if (users) users.select2();
                if (lists) lists.select2();
                if (followupDate) {
                    followupDate.parents('.date').datepicker({
                        format: 'M-dd-yyyy',
                        assumeNearbyYear: true,
                        showOnFocus: false
                    }).on('changeDate', function() {
                        if (followupDate.val()) {
                            submit_btn.prop('disabled',false);
                            $(this).parent().removeClass('has-error');
                            $(this).parent().find('.help-block').text('');
                        }
                    }).on('focusout', function () {
                        var date = followupDate.val();
                        if (date !== '') {
                            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
                            if(valid_date===false){
                                submit_btn.prop('disabled',true);
                                $(this).parent().addClass('has-error');
                                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
                            }
                            else{
                                submit_btn.prop('disabled',false);
                                $(this).parent().removeClass('has-error');
                                $(this).parent().find('.help-block').text('');
                            }
                        }
                    });
                }
                if (visit_date) {
                    visit_date.parents('.date').datepicker({
                        format: 'M-dd-yyyy',
                        assumeNearbyYear: true,
                        showOnFocus: false
                    }).on('changeDate', function() {
                        if (visit_date.val()) {
                            submit_btn.prop('disabled',false);
                            $(this).parent().removeClass('has-error');
                            $(this).parent().find('.help-block').text('');
                        }
                    }).on('focusout', function () {
                        var date = visit_date.val();
                        if (date !== '') {
                            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
                            if(valid_date===false){
                                submit_btn.prop('disabled',true);
                                $(this).parent().addClass('has-error');
                                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
                            }
                            else{
                                submit_btn.prop('disabled',false);
                                $(this).parent().removeClass('has-error');
                                $(this).parent().find('.help-block').text('');
                            }
                        }
                    });
                }
                if(visit_time){
                    visit_time.timepicker();
                }

                // 25 = Completed, 28 = Disabled
                if ($.inArray('25', stages.val()) > -1 || $.inArray('28', stages.val()) > -1) {
                    reason.removeAttr('required');
                    followupDate.removeAttr('required');
                }
                stages.on('change',function(){
                    // 25 = Completed, 28 = Disabled
                    if ($.inArray('25', stages.val()) > -1 || $.inArray('28', stages.val()) > -1) {
                        reason.removeAttr('required');
                        followupDate.removeAttr('required');
                    }
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectVisitExtension.Index.Grid.refresh();
                                    visitsSummaryUpdate();
                                    projectFollowUpUpdate();
                                    projectStatusUpdate();
                                } else {
                                    swal(data);
                                }
                            })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });
            }

        }
        else {
            location.reload();
        }
    });
});

// Project Visit Data Grid
$('#project-visit-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-gear');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-gear');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                var stages = $("#stages");
                var users = $("#users");
                var followupDate = form_modal.find("#follow_up_date");
                var reason = $('#reason');
                var weather = $("#weather");
                var workers_count = $("#workers_count");
                var visit_date = $("#visit_date");
                var visit_time = $("#visit_time");

                workers_count.numeric({
                    negative: false
                }).keypress(function(e){
                    if (this.value.length == 0 && e.which == 48 ){
                        return false;
                    }
                });

                if (stages) stages.select2();
                if (users) users.select2();
                if (followupDate) {
                    followupDate.parents('.date').datepicker({
                        format: 'M-dd-yyyy',
                        assumeNearbyYear: true,
                        showOnFocus: false
                    }).on('changeDate', function() {
                        if (followupDate.val()) {
                            submit_btn_modal.prop('disabled',false);
                            $(this).parent().removeClass('has-error');
                            $(this).parent().find('.help-block').text('');
                        }
                    }).on('focusout', function () {
                        var date = followupDate.val();
                        if (date !== '') {
                            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
                            if(valid_date===false){
                                submit_btn_modal.prop('disabled',true);
                                $(this).parent().addClass('has-error');
                                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
                            }
                            else{
                                submit_btn_modal.prop('disabled',false);
                                $(this).parent().removeClass('has-error');
                                $(this).parent().find('.help-block').text('');
                            }
                        }
                    });
                }
                if (visit_date) {
                    visit_date.parents('.date').datepicker({
                        format: 'M-dd-yyyy',
                        assumeNearbyYear: true,
                        showOnFocus: false
                    }).on('changeDate', function() {
                        if (visit_date.val()) {
                            submit_btn.prop('disabled',false);
                            $(this).parent().removeClass('has-error');
                            $(this).parent().find('.help-block').text('');
                        }
                    }).on('focusout', function () {
                        var date = visit_date.val();
                        if (date !== '') {
                            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
                            if(valid_date===false){
                                submit_btn.prop('disabled',true);
                                $(this).parent().addClass('has-error');
                                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
                            }
                            else{
                                submit_btn.prop('disabled',false);
                                $(this).parent().removeClass('has-error');
                                $(this).parent().find('.help-block').text('');
                            }
                        }
                    });
                }
                if(visit_time){
                    visit_time.timepicker();
                }

                // 25 = Completed, 28 = Disabled
                if ($.inArray('25', stages.val()) > -1 || $.inArray('28', stages.val()) > -1) {
                    reason.removeAttr('required');
                    followupDate.removeAttr('required');
                }
                stages.on('change',function(){
                    // 25 = Completed, 28 = Disabled
                    if ($.inArray('25', stages.val()) > -1 || $.inArray('28', stages.val()) > -1) {
                        reason.removeAttr('required');
                        followupDate.removeAttr('required');
                    }
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectVisitExtension.Index.Grid.refresh();
                                    visitsSummaryUpdate();
                                    projectFollowUpUpdate();
                                    projectStatusUpdate();
                                } else {
                                    swal(data);
                                }
                            });
                    }
                });

                submit_btn_modal.on('click', function () {
                    form_modal.parsley().validate();
                });
            }

        } else {
            location.reload();
        }
    });
});