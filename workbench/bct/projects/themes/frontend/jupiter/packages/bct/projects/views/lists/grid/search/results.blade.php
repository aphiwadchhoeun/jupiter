<script type="text/template" data-grid="lists-search" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row data-url="<%- r.view_uri %>">
            @if (!$currentUser->inRole('pp-guest'))
            <td><input content="id" data-grid-checkbox="" name="entries[]" type="checkbox" value="<%- r.id %>"
                        <% if(r.selected){ %>checked<% } %>>
            </td>
            @endif
            <td><%- r.name %></td>
            <td><%- r.group %></td>
            <td><%- r.category %></td>
            <td><%- r.due_date_custom %></td>
            <td>
                <% if(r.is_active == 'Active') { %>
                <span class="label label-success"><%- r.is_active %></span>
                <% } else { %>
                <span class="label label-danger"><%- r.is_active %></span>
                <% } %>
            </td>
            <td>
                <a href="<%- r.list_uri %>#details" class="btn btn-primary btn-circle"
                   data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}">
                    <i class="fa fa-book fa-fw"></i></a>
            </td>
		</tr>

	<% }); %>

</script>
