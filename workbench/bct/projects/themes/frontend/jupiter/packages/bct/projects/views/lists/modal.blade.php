<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{{ trans("bct/projects::lists/common.title") }}} {{{ trans("action.{$mode}") }}}</h4>
</div>
<div class="modal-body">
    {{-- Form --}}
    <form id="content-form-modal" action="
          @if ($list->exists)
          {{ route('bct.projects.lists.edit', $list->id) }}
          @else
          {{ route('bct.projects.lists.create') }}
          @endif
          " method="post" data-refresh="lists">

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="panel-body">

            <div class="col-md-12">

                {{-- Form: General --}}
                <fieldset>
                    <div class="row">

                        {{-- Form:name --}}
                        <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                            <label for="name" class="control-label">
                                {{{ trans('bct/projects::lists/model.general.name') }}}
                            </label>

                            <input type="text" class="form-control" name="name" id="name" required autofocus
                                   placeholder="{{{ trans('bct/projects::lists/model.general.name_help') }}}"
                                   value="{{{ input()->old('name', $list->name) }}}">

                            <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                        </div>

                        {{-- Form:group_id --}}
                        <div class="form-group{{ Alert::onForm('z_projects_group_id', ' has-error') }}">

                            <label for="z_projects_group_id" class="control-label">
                                {{{ trans('bct/projects::lists/model.general.group_id')}}}
                            </label>

                            <select class="form-control" name="z_projects_group_id" id="group_id" required>
                                <option value="">{{{ trans('bct/projects::lists/model.general.group_id_help') }}}</option>
                                @foreach($groups as $group)
                                <option value="{{ $group->id }}"
                                        @if(input()->old('z_projects_group_id', $list->z_projects_group_id) == $group->id) selected @endif
                                        >{{ $group->name }}</option>
                                @endforeach
                            </select>

                            <span class="help-block">{{{ Alert::onForm('z_projects_group_id') }}}</span>

                        </div>


                        <div class="form-group">

                            <label for="due_date" class="control-label">
                                {{{ trans('bct/projects::lists/model.general.due_date')}}}
                            </label>
                            <div class='input-group date'>
                                <input type="text" class="form-control" name="due_date" id="due_date"
                                       placeholder="{{{ trans('bct/projects::lists/model.general.due_date_help') }}}"
                                       required data-parsley-date value="<?php echo date('M-d-Y', strtotime('+1 months'));  ?>">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                            <span class="help-block">{{{ Alert::onForm('due_date') }}}</span>

                        </div>


                        {{-- Form:category --}}
                        <div class="form-group{{ Alert::onForm('category', ' has-error') }}">

                            <label for="category" class="control-label">
                                {{{ trans('bct/projects::lists/model.general.category') }}}
                            </label>

                            <input type="text" class="form-control" name="category" id="category"                                    placeholder="{{{ trans('bct/projects::lists/model.general.category_help') }}}"
                                   value="{{{ input()->old('category', $list->category) }}}">

                            <span class="help-block">{{{ Alert::onForm('category') }}}</span>

                        </div>


                        {{-- Form:description --}}
                        <div class="form-group{{ Alert::onForm('description', ' has-error') }}">

                            <label for="description" class="control-label">
                                {{{ trans('bct/projects::lists/model.general.description') }}}
                            </label>

                            <textarea class="form-control" name="description" rows="4"
                                      placeholder="{{{ trans('bct/projects::lists/model.general.description_help') }}}"
                                      >{{{ input()->old('description', $list->description) }}}</textarea>

                            <span class="help-block">{{{ Alert::onForm('description') }}}</span>

                        </div>


                    </div>

                </fieldset>

            </div>

        </div>

    </form>
</div>

<div class="modal-footer">
    <button id="submit-btn-modal" name="submit-btn-modal" class="btn btn-primary" data-toggle="tooltip"
            data-original-title="{{{ trans('action.save') }}}">
        <i class="fa fa-save fa-fw"></i>{{{ trans('action.save') }}}</button>

    @if ($list->exists)
    <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-toggle="tooltip" data-action="delete"
            data-original-title="{{{ trans('action.delete') }}}"
            data-url="{{ route('bct.projects.lists.delete', $list->id) }}">
        {{{ trans('action.delete') }}}</button>
    @endif

    <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>

</div>