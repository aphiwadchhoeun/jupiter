var default_lat = (typeof BctProjects.default_lat !== 'undefined') ? BctProjects.default_lat : '',
    default_lng = (typeof BctProjects.default_lng !== 'undefined') ? BctProjects.default_lng : '';

jQuery('document').ready(function ($) {
    initMarkers();
    $('body').on('click', '.delete-confirm', function (e) {
        e.preventDefault();

        var $this = $(this);

        swal({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post($this.attr('href'))
                        .done(function (data) {
                            resolve();
                        })
                        .fail(function () {
                            reject('Error!');
                        });
                });
            }
        }).then(function () {
            $this.parents('tr').remove();

            var marker = markers[$this.parents('tr').data('id')];
            if (marker)
                marker.setMap(null);

            calcRoute();

        });
    });
    $('body').on('click', '#project_details', function (e) {
        e.preventDefault();
        window.open($(this).attr('href'), '_blank');
    });

    $(document).on('click', '#sortable tr', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        checkMarker($(this).data('id'), $(this).hasClass('active'));
    });

    $('#optimize').on('click', function (e) {
        is_optimize = true;
        calcRoute();
        $(this).addClass('disabled').unbind('click');

    });

    $('input[name=options]').on('change', function (e) {
        showDefaultMap();
        switch ($(this).val()) {
            case 'show-zip':
                var type = "zip";
                //showPolygons(type, styles);
                showZipPolygons(true);
                break;
            case 'show-county':
                var type = "county";
                showCountyPolygons(true)
                //showPolygons(type, styles);
                break;
            case 'show-region':
                var type = "region";
                var styles = {
                    fillColor: '#FF0000',
                    fillOpacity: 0.3
                };
                showPolygons(type, styles);
                break;
        }

    });

});


var geocoder, searchBoxStart, searchBoxFinish, map, markerStart, markerFinish, directionsDisplay, layer, zip_layer, county_layer, limit = 8, markers = {}, windows = {}, sublayers = [];
var directionsService, areaInfoWindow,
    is_optimize = false;
var cartodb_api = null, cartodb_table = null, cartodb_username = null;


function fetchCartodbConfigs(callback) {
    if (cartodb_api == null || cartodb_table == null) {
        $.get('/ui/api/cartodb/configs/get')
            .done(function (data) {
                cartodb_api = data.api_url;
                cartodb_table = data.tablename;
                cartodb_username = prepareCartdbUserName(cartodb_api);
                callback();
            });
    } else {
        callback();
    }
}

function prepareCartdbUserName(apiurl) {
    var cut = apiurl.substr(8);

    return cut.substr(0, cut.indexOf("."));
}

/** for google map */

function initialize() {
    geocoder = new google.maps.Geocoder();

    directionsService = new google.maps.DirectionsService();
    areaInfoWindow = new google.maps.InfoWindow();

    var mapOptions = {
        zoom: 10
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    var markerStartOptions = {
        title: 'Start position',
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/1436369218_Map-Marker-Marker-Outside-Chartreuse.png'
    };

    var markerFinishOptions = {
        title: 'Finish position',
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/1437069737_MapMarker_Marker_Outside_Pink.png'
    };

    markerStart = new google.maps.Marker(markerStartOptions);
    markerFinish = new google.maps.Marker(markerFinishOptions);

    // Try HTML5 geolocation
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            markerStartOptions.pos = new google.maps.LatLng(position.coords.latitude,
                position.coords.longitude);

            handleNoGeolocation(false);

        }, function () {
            handleNoGeolocation(true);
        });
    } else {
        handleNoGeolocation(false);
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag) {
            map.setCenter(new google.maps.LatLng(default_lat, default_lng));
        } else {
            map.setCenter(markerStartOptions.pos);
            markerStart.setPosition(markerStartOptions.pos);
            calcRoute();
            getAddress(markerStart.getPosition(), $('#start-point'));
        }
    }

    searchBoxStart = new google.maps.places.SearchBox(document.getElementById('start-point'));
    searchBoxFinish = new google.maps.places.SearchBox(document.getElementById('finish-point'));

    google.maps.event.addDomListener(searchBoxStart, "places_changed", function () {
        var places = searchBoxStart.getPlaces();
        var i, place;

        for (i = 0; place = places[i]; i++) {
            markerStart.setPosition(place.geometry.location);
        }

        calcRoute();
    });

    google.maps.event.addDomListener(searchBoxFinish, "places_changed", function () {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var places = searchBoxFinish.getPlaces();
        var count = limit - $("#sortable tr").length;
        places.length = count;
        var i, place;
        for (i = 0; place = places[i]; i++) {
            if ($("#sortable tr").length < limit) {
                $.post(
                    window.location.origin + '/pprofiles/routes/create',
                    {
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng(),
                        address: place.formatted_address,
                        sort: $("#sortable tr:last").data('sort') + 1
                    },
                    function (data) {
                        $('#sortable').append(
                            '<tr data-id="' + data.id + '" data-lat="' + data.lat + '" data-lng="' + data.lng + '" ' +
                            'data-sort="' + data.sortOrder + '">' +
                            '<td></td>' +
                            '<td>' + data.address + '</td>' +
                            '<td>' +
                            '<a href="' + window.location.origin + '/pprofiles/map/delete/' + data.id + '" ' +
                            'class="btn btn-danger btn-circle delete-confirm"' +
                            'data-toggle="tooltip" data-placement="right"' +
                            'data-original-title="">' +
                            '<i class="fa fa-trash fa-fw"></i></a>' +
                            '</td>' +
                            '</tr>');

                        calcRoute();
                    }
                );
            } else {
                markerFinish.setPosition(place.geometry.location);
            }
        }

        calcRoute();
    });

    google.maps.event.addDomListener(markerStart, 'dragend', function () {
        calcRoute();
        getAddress(markerStart.getPosition(), $('#start-point'));
    });

    google.maps.event.addDomListener(markerFinish, 'dragend', function () {
        calcRoute();
        getAddress(markerFinish.getPosition(), $('#finish-point'));
    });

    google.maps.event.addDomListener(map, 'zoom_changed', function () {
        $('.cartodb-infowindow').addClass('hidden');

    });

    google.maps.event.addDomListener(map, 'maptypeid_changed', function () {
        $('.cartodb-infowindow').addClass('hidden');
    });

    google.maps.event.addDomListener(map, 'click', function () {
        setTimeout(function () {
            $('.cartodb-infowindow').removeClass('hidden');
        }, 300);
    });

    /** for sortable */
    $("#sortable").sortable({
        stop: function (event, ui) {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            calcRoute();

            var items = [];
            _.each($("#sortable tr"), function (item) {
                items.push($(item).data('id'));
            });

            $.post(
                window.location.origin + '/pprofiles/routes/sort',
                {items: items},
                function (data) {

                }
            );
        }
    });

    google.maps.event.addDomListener(map, "click", function () {
        areaInfoWindow.close();
    })
}

function calcRoute() {

    $('#sortable tr').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        checkMarker($(this).data('id'), $(this).hasClass('active'));
    });

    $('#route-details').text('');

    if (directionsDisplay != null) {
        directionsDisplay.setMap(null);
        directionsDisplay = null;
    }

    directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});

    directionsDisplay.setMap(map);

    var finish, waypts = [];

    _.each(markers, function (m) {
        m.setMap(null);
    });

    markers = {};

    _.each($("#sortable tr"), function (result) {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var $item = $(result);

        if ($item.index() + 1 == $("#sortable tr").length) {
            var iconName = '1437069737_MapMarker_Marker_Outside_Pink.png';
        } else {
            var iconName = 'mark' + ($item.index() + 1) + '.png';
        }
        var mark = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng($item.data('lat'), $item.data('lng')),
            icon: window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/' + iconName
        });

        $item.find('td:first').html('<img src="' + window.location.origin + '/themes/frontend/jupiter/' +
            'extensions/bct/projects/assets/routes/img/icons/' + iconName + '">');

        var contentString;
        if ($item.data('project-id')) {
            var maps_url = "http://maps.google.com?q=loc:" + $item.data('lat') + ',' + $item.data('lng');
            var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
            var isAndroid = /android/i.test(navigator.userAgent.toLowerCase());
            var isWindowsPhone = /windows phone/i.test(navigator.userAgent.toLowerCase());

            if (isiDevice) {
                maps_url = "maps://maps.google.com?q=loc:" + $item.data('lat') + ',' + $item.data('lng');
            }
            else if (isAndroid) {
                maps_url = "geo:" + $item.data('lat') + ',' + $item.data('lng') + "?q=" + $item.data('lat') + ',' + $item.data('lng');
            }
            else if (isWindowsPhone) {
                maps_url = "maps:" + $item.data('lat') + ',' + $item.data('lng');
            }
            var url = window.location.origin + '/pprofiles/projects/summary/' + $item.data('project-id');
            contentString =
                '<div id="content">' +
                '<div id="bodyContent">' +
                '<p><b>Name: </b>' + $item.data('name') + '</p>' +
                '<p><b>Address: </b><a href="' + maps_url + '">' + $item.data('address') + '</a></p>' +
                '<p><b>Description: </b>' + $item.data('desc') + '</p>' +
                '<p><b>Tabs: </b>' +
                '<a class="label label-primary" href="' + url + '#details">' + 'Details</a> ' +
                '<a class="label label-primary" href="' + url + '#contractors">' + 'Contractors</a> ' +
                '<a class="label label-primary" href="' + url + '#contacts">' + 'Contacts</a> ' +
                '<a class="label label-primary" href="' + url + '#visits">' + 'Visits</a> ' +
                '<a class="label label-primary" href="' + url + '#actions-history">' + 'Actions History</a> ' +
                '<a class="label label-primary" href="' + url + '#files">' + 'Files</a> ' +
                '<a class="label label-primary" href="' + url + '#lists">' + 'Lists</a> ' +
                '</p>' +
                '</div>' +
                '</div>';
        } else {
            contentString =
                '<div id="content">' +
                '<div id="bodyContent">' +
                '<p><b>Address: </b>' + $item.find('td:eq(1)').text() + '</p>' +
                '</div>' +
                '</div>';
        }

        var markWindow = new google.maps.InfoWindow({
            content: contentString
        });

        mark.addListener('click', function (e) {
            $item.click();
        });

        google.maps.event.addListener(markWindow, 'closeclick', function () {
            $item.click();
        });

        windows[$item.data('id')] = markWindow;
        markers[$item.data('id')] = mark;

        waypts.push({
            location: mark.getPosition(),
            stopover: true,
        });

    });

    if (!markerFinish.getPosition()) {
        var lastProject = waypts.pop();
        if (lastProject)
            finish = lastProject.location;
    } else {
        finish = markerFinish.getPosition();
    }

    var request = {
        origin: markerStart.getPosition(),
        destination: finish,
        waypoints: waypts,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.IMPERIAL,

    };

    if (is_optimize === true) {
        request['optimizeWaypoints'] = true;
    }

    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {

            _.each(markers, function (m) {
                m.setMap(null);
            });

            markers = {};

            // Sort left list by optimize route
            var new_order = response.routes[0].waypoint_order,
                new_list_html = '',
                new_order_counter = 1,
                iconName, mark;

            _.each(new_order, function (num) {
                new_list_html += $('#sortable tr').eq(num)[0].outerHTML.replace(/mark\d/ig, 'mark' + new_order_counter);


                item_id = $('#sortable tr').eq(num).data('id');
                item_lat = $('#sortable tr').eq(num).data('lat');
                item_lng = $('#sortable tr').eq(num).data('lng');

                iconName = 'mark' + (new_order_counter) + '.png';

                mark = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(item_lat, item_lng),
                    icon: window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/' + iconName
                });

                markers[item_id] = mark;
                new_order_counter++;

            });

            last_mark = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng($('#sortable tr').last().data('lat'), $('#sortable tr').last().data('lng')),
                icon: window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/1437069737_MapMarker_Marker_Outside_Pink.png'
            });
            markers[$('#sortable tr').last().data('id')] = last_mark;


            new_list_html += $('#sortable tr').last()[0].outerHTML;

            directionsDisplay.setDirections(response);

            var i, route, distance = 0, duration = 0;
            for (i = 0; route = response.routes[i]; i++) {
                var j, leg;
                for (j = 0; leg = route.legs[j]; j++) {
                    distance += leg.distance.value;
                    duration += leg.duration.value;
                }
            }

            $('#route-details').text('distance - ' + (distance / 1609.34).toFixed(2) + ' miles, duration - ' + moment.duration(duration, "seconds").humanize());

            initAccordion(response);

            $('#sortable').html(new_list_html);

            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }

            var items = [];
            _.each($("#sortable tr"), function (item) {
                items.push($(item).data('id'));
            });

            $.post(
                window.location.origin + '/pprofiles/routes/sort',
                {items: items},
                function (data) {

                }
            );

        } else {
            $('#route-details').text('');
            swal({
                title: "Some problems with your route! Please, check your waypoints!",
                type: "warning",
                confirmButtonColor: "#019964",
                confirmButtonText: "Ok!"
            });
        }

    });
}
function initMarkers() {
    _.each($("#sortable tr"), function (result) {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var $item = $(result);

        if ($item.index() + 1 == $("#sortable tr").length) {
            var iconName = '1437069737_MapMarker_Marker_Outside_Pink.png';
        } else {
            var iconName = 'mark' + ($item.index() + 1) + '.png';
        }
        var mark = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng($item.data('lat'), $item.data('lng')),
            icon: window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/' + iconName
        });

        $item.find('td:first').html('<img src="' + window.location.origin + '/themes/frontend/jupiter/' +
            'extensions/bct/projects/assets/routes/img/icons/' + iconName + '">');

        var contentString;

        if ($item.data('project-id')) {
            var maps_url = "http://maps.google.com?q=loc:" + $item.data('lat') + ',' + $item.data('lng');
            var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
            var isAndroid = /android/i.test(navigator.userAgent.toLowerCase());
            var isWindowsPhone = /windows phone/i.test(navigator.userAgent.toLowerCase());

            if (isiDevice) {
                maps_url = "maps://maps.google.com?q=loc:" + $item.data('lat') + ',' + $item.data('lng');
            }
            else if (isAndroid) {
                maps_url = "geo:" + $item.data('lat') + ',' + $item.data('lng') + "?q=" + $item.data('lat') + ',' + $item.data('lng');
            }
            else if (isWindowsPhone) {
                maps_url = "maps:" + $item.data('lat') + ',' + $item.data('lng');
            }
            var url = window.location.origin + '/pprofiles/projects/summary/' + $item.data('project-id');
            contentString =
                '<div id="content">' +
                '<div id="bodyContent">' +
                '<p><b>Name: </b>' + $item.data('name') + '</p>' +
                '<p><b>Address: </b><a href="' + maps_url + '">' + $item.data('address') + '</a></p>' +
                '<p><b>Description: </b>' + $item.data('desc') + '</p>' +
                '<p><b>Tabs: </b>' +
                '<a class="label label-primary" href="' + url + '#details">' + 'Details</a> ' +
                '<a class="label label-primary" href="' + url + '#contractors">' + 'Contractors</a> ' +
                '<a class="label label-primary" href="' + url + '#contacts">' + 'Contacts</a> ' +
                '<a class="label label-primary" href="' + url + '#visits">' + 'Visits</a> ' +
                '<a class="label label-primary" href="' + url + '#actions-history">' + 'Actions History</a> ' +
                '<a class="label label-primary" href="' + url + '#files">' + 'Files</a> ' +
                '<a class="label label-primary" href="' + url + '#lists">' + 'Lists</a> ' +
                '</p>' +
                '</div>' +
                '</div>';
        } else {
            contentString =
                '<div id="content">' +
                '<div id="bodyContent">' +
                '<p><b>Address: </b>' + $item.find('td:eq(1)').text() + '</p>' +
                '</div>' +
                '</div>';
        }

        var markWindow = new google.maps.InfoWindow({
            content: contentString
        });


        mark.addListener('click', function (e) {
            $item.click();
        });

        google.maps.event.addListener(markWindow, 'closeclick', function () {
            $item.click();
        });

        windows[$item.data('id')] = markWindow;
        markers[$item.data('id')] = mark;

    });
}
function getAddress(position, input) {
    geocoder.geocode({
        latLng: position
    }, function (responses) {
        if (responses && responses.length > 0) {
            /** set address data to inputs */
            input.val(responses[0].formatted_address);
        } else {
            /** clear inputs */
            input.val('');
        }
    });
}

function showPolygons(type, styles) {
    if (type == "region") {
        if (typeof sublayers[0] == 'undefined') {

            fetchCartodbConfigs(function () {
                layer = {
                    user_name: cartodb_username,
                    type: 'cartodb',
                    infowindow: false,
                    tooltip: true,
                    sublayers: [{
                        sql: "SELECT * FROM " + cartodb_table,
                        cartocss: '#' + cartodb_table + ' {polygon-opacity: 0.7;line-color: #FFF;line-width: 0.5;line-opacity: 1;} ' +
                        '#' + cartodb_table + '[areaname="Area One"] {polygon-fill: #A6CEE3;} ' +
                        '#' + cartodb_table + '[areaname="Area Three"] {polygon-fill: #1F78B4;} ' +
                        '#' + cartodb_table + '[areaname="Area Two"] {polygon-fill: #B2DF8A;}'
                    }]
                };

                cartodb.createLayer(map, layer, {https: true})
                    .addTo(map)
                    .done(function (layer) {
                        for (var i = 0; i < layer.getSubLayerCount(); i++) {
                            sublayers[i] = layer.getSubLayer(i);
                        }
                        cdb.vis.Vis.addInfowindow(map, sublayers[0], ['areaname']);
                    })
                    .error(function (err) {
                        console.log("error: " + err);
                    });
            });

        } else {
            sublayers[0].show();
        }
    }
    else if (type == "county") {
        if (typeof sublayers[0] == 'undefined') {

            fetchCartodbConfigs(function () {
                layer = {
                    user_name: cartodb_username,
                    type: 'cartodb',
                    infowindow: false,
                    tooltip: true,
                    sublayers: [{
                        sql: "SELECT * FROM us_county_2013",
                        cartocss: '#us_county_2013 {polygon-opacity: 0.7;line-color: #FFF;line-width: 0.5;line-opacity: 1;}'
                    }]
                };

                cartodb.createLayer(map, layer, {https: true})
                    .addTo(map)
                    .done(function (layer) {
                        for (var i = 0; i < layer.getSubLayerCount(); i++) {
                            sublayers[i] = layer.getSubLayer(i);
                        }
                        cdb.vis.Vis.addInfowindow(map, sublayers[0], ['countyfp']);
                    })
                    .error(function (err) {
                        console.log("error: " + err);
                    });
            });
        }

    }
    else if (type == "zip") {

        if (typeof sublayers[0] == 'undefined') {

            fetchCartodbConfigs(function () {
                layer = {
                    user_name: cartodb_username,
                    type: 'cartodb',
                    infowindow: false,
                    tooltip: true,
                    sublayers: [{
                        sql: "SELECT * FROM us_zipcodes",
                        cartocss: '#us_zipcodes { polygon-fill: #F00; polygon-opacity: 0.3; line-color: #F00; }'
                    }]
                };
                setTimeout(function () {
                    cartodb.createLayer(map, layer, {https: true})
                        .addTo(map)
                        .done(function (layer) {

                            for (var i = 0; i < layer.getSubLayerCount(); i++) {
                                sublayers[i] = layer.getSubLayer(i);
                            }
                            cdb.vis.Vis.addInfowindow(map, sublayers[0], ['zip']);
                        })
                        .error(function (err) {
                            console.log("error: " + err);
                        });
                }, 1000);
            });

        }
    }


}

function showZipPolygons(show) {
    //1LVl0ApfDNzAF9Le7HpGQ-6loSiYLxgiMdkbpCc9v
    if (show) {
        zip_layer = new google.maps.FusionTablesLayer({
            suppressInfoWindows: true,
            query: {
                select: '*',
                from: '1Lae-86jeUDLmA6-8APDDqazlTOy1GsTXh28DAkw'
            },
            styles: [
                {
                    polygonOptions: {
                        fillColor: '#00FF00',
                        fillOpacity: 0.2
                    }
                }
            ]
        });

        zip_layer.setMap(map);

        google.maps.event.addDomListener(zip_layer, 'click', function (e) {

            e.infoWindowHtml = "<div class='googft-info-window'>" + "<b>Zip Code</b><br />" +
                e.row['ZIP'].value +
                "</div>";
            areaInfoWindow.setOptions(
                {
                    content: e.infoWindowHtml,
                    position: e.latLng,
                    pixelOffset: e.pixelOffset
                });

            areaInfoWindow.open(map);
        });

    } else {
        if (zip_layer) {
            zip_layer.setMap(null);
        }
    }
}

function showCountyPolygons(show) {
    if (show) {
        county_layer = new google.maps.FusionTablesLayer({
            suppressInfoWindows: true,
            query: {
                select: '*',
                from: '1xdysxZ94uUFIit9eXmnw1fYc6VcQiXhceFd_CVKa'
            },
            styles: [
                {
                    polygonOptions: {
                        fillColor: '#00FF00',
                        fillOpacity: 0.2
                    }
                }
            ]
        });

        county_layer.setMap(map);

        google.maps.event.addDomListener(county_layer, 'click', function (e) {

            e.infoWindowHtml = "<div class='googft-info-window'>" + "<b>County Name</b><br />" +
                e.row['County Name'].value +
                "</div>";
            areaInfoWindow.setOptions(
                {
                    content: e.infoWindowHtml,
                    position: e.latLng,
                    pixelOffset: e.pixelOffset
                });

            areaInfoWindow.open(map);
        });

    } else {
        if (county_layer) {
            county_layer.setMap(null);
        }

    }
}


function showDefaultMap() {
    if (sublayers.length) {
        sublayers[0].hide();
        sublayers.splice(0, sublayers.length);
    }

    showZipPolygons(false);
    showCountyPolygons(false);
}

function checkMarker(id, show) {
    if (typeof markers[id] != 'undefined') {
        var marker = markers[id];
        var markerWindow = windows[id];
        if (show) {
            markerWindow.open(map, marker);
            $('.cartodb-infowindow').addClass('hidden');
        } else {
            markerWindow.close(map, marker);
            setTimeout(function () {
                $('.cartodb-infowindow').removeClass('hidden').css('display', 'none');
            }, 300);
        }
    }
}

function initAccordion(response) {
    var mainItems = {};

    var i, j, route;
    for (i = 0; route = response.routes[i]; i++) {
        var leg;
        for (j = 0; leg = route.legs[j]; j++) {

            if (j == 0) {
                mainItems['start'] = {
                    title: 'Start address: ' + leg.start_address,
                };
            }

            mainItems[j] = {
                title: leg.end_address,
                distance: leg.distance.text,
                duration: leg.duration.text,
                steps: {}
            };

            var k, step;
            for (k = 0; step = leg.steps[k]; k++) {
                mainItems[j]['steps'][k] = {
                    title: step.instructions,
                    category: step.maneuver,
                    distance: step.distance.text,
                    duration: step.duration.text
                }
            }
        }
    }

    $('#accordion').html('');

    $.each(mainItems, function (index, mainItem) {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        if (index == 'start') {
            var main = '' +
                '<div class="panel panel-default">' +
                '<div class="panel-heading">' +
                '<h4 class="panel-title">' +
                '<a role="button" data-toggle="collapse">' +
                '<img src="' + window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/1436369218_Map-Marker-Marker-Outside-Chartreuse.png">' +
                mainItem.title +
                '</a>' +
                '</h4>' +
                '</div>' +
                '</div>';

            $('#accordion').prepend(main);

            return;
        }

        var subs = mainItem.steps;
        index++;

        if (index == j) {
            var iconName = '1437069737_MapMarker_Marker_Outside_Pink.png';
            mainItem.title = 'Finish Address: ' + mainItem.title;
        } else {
            var iconName = 'mark' + index + '.png';
        }

        var project_name = ($('#sortable tr').eq(index - 1).data('project-id')) ? $('#sortable tr').eq(index - 1).text() : '';

        var main = '' +
            '<div class="panel panel-default">' +
            '<div class="panel-heading" role="tab" id="heading' + index + '">' +
            '<h4 class="panel-title">' +
            '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + index + '" aria-expanded="true" aria-controls="collapse' + index + '">' +
            '<img src="' + window.location.origin + '/themes/frontend/jupiter/extensions/bct/projects/assets/routes/img/icons/' + iconName + '">' +
            mainItem.title +
            '<div class="route-distance padding-top">' + mainItem.distance + ' / ' + mainItem.duration + '</div>' +
            '<div class="padding-top">Project Name: ' + project_name + '</div>' +
            '</a>' +
            '</h4>' +
            '</div>' +
            '<div id="collapse' + index + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + index + '"></div>' +
            '</div>';

        $('#accordion').append(main);

        _.each(subs, function (subItem) {
            var catClass = '';
            if (subItem.category != '') {
                catClass = 'route-icon ' + subItem.category
            }

            var sub = '' +
                '<div class="panel-body">' +
                '<div class="' + catClass + '"></div>' +
                '<div class="route-text col-xs-10">' + subItem.title + '' +
                '<div class="route-distance">' + subItem.distance + ' / ' + subItem.duration + '</div>' +
                '</div>' +
                '</div>';

            sub = $(sub);

            if (sub.find('.route-text div').length > 1) {
                sub.find('.route-text div:first').prepend('<i class="fa fa-exclamation-circle margin-right5"></i>');
            }

            $('#collapse' + index).append(sub);
        });
    });
}