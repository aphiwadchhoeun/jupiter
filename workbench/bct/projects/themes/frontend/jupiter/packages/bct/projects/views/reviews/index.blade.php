@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::visits/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}

    {{ Asset::queue('reviews-index', 'bct/projects::reviews/js/index.js', 'extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::visits/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="reviews" role="form">

                            <div class="input-group">

                                <span class="input-group-btn">

                                    <button class="btn btn-default" type="button" disabled>
                                        {{{ trans('common.status') }}}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                        <li>
                                            <a href="#" data-filter="pp_reviews.approved:1">
                                                <i class="fa fa-check-circle"></i> {{ trans('bct/projects::reviews/model.general.approved') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter="pp_reviews.approved:0" data-filter-not-refresh>
                                                <i class="fa fa-check-circle"></i> {{ trans('bct/projects::reviews/model.general.not_approved') }}
                                            </a>
                                        </li>
                                    </ul>

                                    <button class="btn btn-default" type="button" data-grid-calendar
                                            data-range-filter="created_at">
                                        <i class="fa fa-calendar"></i>
                                    </button>

                                </span>

                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="project_name">{{ trans('bct/projects::projects/model.general.project') }}
                                        </option>
                                        <option value="users_names">{{ trans('bct/projects::visits/model.general.reps') }}
                                        </option>
                                        <option value="region">{{ trans('bct/projects::projects/model.general.region') }}
                                        </option>
                                        <option value="details">{{ trans('bct/projects::visits/model.general.notes') }}
                                        </option>
                                    </select>
                                </span>

                                <input class="form-control" name="filter" type="text" placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="reviews" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>

                            <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-start></button>
                            <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-end></button>


                        </form>

                        @if (!$currentUser->inRole('repuser') && !$currentUser->inRole('pp-guest'))

                            <div class="pull-right">

                                <a href="{{ route('bct.projects.visits.unapprove') }}"
                                   class="btn btn-danger btn-unapprove disabled ml10 btn-prevent-double-click"
                                   data-toggle="tooltip"
                                   data-url="{{ route('bct.projects.visits.unapprove') }}"
                                   data-original-title="{{{ trans('bct/projects::reviews/common.unapprove_selected') }}}">
                                    <i class="fa fa-times fa-fw"></i>
                                </a>

                            </div>

                            <div class="pull-right">

                                <a href="{{ route('bct.projects.visits.approve') }}"
                                   class="btn btn-warning btn-approve disabled btn-prevent-double-click"
                                   data-toggle="tooltip"
                                   data-url="{{ route('bct.projects.visits.approve') }}"
                                   data-original-title="{{{ trans('bct/projects::reviews/common.approve_selected') }}}">
                                    <i class="fa fa-check fa-fw"></i>
                                </a>

                            </div>

                        @endif

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="reviews"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover"
                       data-source="{{ route('bct.projects.visits.grid', 'filters[0][pp_reviews.approved]=0') }}"
                       data-source-default="{{ route('bct.projects.visits.grid') }}"
                       data-filename="{{ trans('bct/projects::visits/common.title') }}"
                       data-grid="reviews">
                    <thead>
                    <tr>
                        @if (!$currentUser->inRole('repuser') && !$currentUser->inRole('pp-guest'))
                            <th class="reviews-checkbox-head"><input data-grid-checkbox="all" type="checkbox"></th>
                        @endif
                        <th class="sortable col-md-2" data-sort="project_name">{{ trans('bct/projects::projects/model.general.project') }}
                        </th>

                        <th class="sortable col-md-2" data-sort="users_names">{{ trans('bct/projects::visits/model.general.reps') }}
                        </th>

                        @if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1)
                            <th class="sortable col-md-1" data-sort="weather">{{ trans('bct/projects::visits/model.general.weather') }}
                            </th>
                        @endif

                        @if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1)
                            <th class="sortable col-md-1" data-sort="workers_count">{{ trans('bct/projects::visits/model.general.workers_count') }}
                            </th>
                        @endif

                        <th class="sortable col-md-1" data-sort="region">{{ trans('bct/projects::projects/model.general.region') }}
                        </th>
                        <th class="sortable col-md-3" data-sort="details">{{ trans('bct/projects::projects/model.general.follow_up_reason') }} & {{ trans('bct/projects::visits/model.general.notes') }}
                        </th>
                        <th class="sortable" style="width: 12%"
                            data-sort="created_at">{{ trans('bct/projects::visits/model.general.visit_date_time') }}
                        </th>
                        <th class="sortable col-md-1"
                            data-sort="approved">{{ trans('bct/projects::projects/model.general.status_id') }}
                        </th>
                        <th class="col-md-1">{{ trans('bct/projects::general/common.details') }}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="reviews"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::reviews.grid.index.results')
        @include('bct/projects::reviews.grid.index.pagination')
        @include('bct/projects::reviews.grid.index.filters')
        @include('bct/projects::reviews.grid.index.no_results')

    </section>

@stop
