<script type="text/template" data-grid="projects" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
            <td><%- r.name %></td>
            <td><%- r.city %></td>
            <td><%- r.state %></td>
            <td><%- r.created %></td>
            <td>
                <% if (r.status == 'Active') { %>
                    <span class="label label-success"><%- r.status %></span>
                <% } else if (r.status == 'Disabled') {%>
                    <span class="label label-danger"><%- r.status %></span>
                <% } else {%>
                    <span class="label label-default"><%- r.status %></span>
                <% } %>            
            </td>
            <td><%- r.contact_type %></td>
            <td class="hidden-print"><a href="<%- r.view_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                            class="fa fa-book fa-fw"></i></a></td>
		</tr>

	<% }); %>

</script>
