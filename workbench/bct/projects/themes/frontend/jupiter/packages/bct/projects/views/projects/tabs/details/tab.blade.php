{{ Asset::queue('project-details-summary', 'bct/projects::projects/css/details/summary.css') }}

<header class="panel-heading mb15">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header hidden-print">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip"
                           @if (stripos(url()->previous(), url('pprofiles/lists')) !== false)
                           href="{{ route('bct.projects.lists.search') }}"
                           @elseif(
                               stripos(url()->previous(), 'contacts/details') !== false ||
                               stripos(url()->previous(), 'visits') !== false ||
                               stripos(url()->previous(), 'map') !== false ||
                               stripos(url()->previous(), 'route') !== false
                           )
                           href="{{ url()->previous()}}"
                           @else
                           href="{{ route('bct.projects.projects.all') }}"
                           @endif
                           data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>


            </div>

            <div class="pull-left col-md-10">
                <span class="navbar-brand">{{ $project->name }} {{ trans("bct/projects::general/common.details") }}
                    | {{ trans('bct/projects::projects/model.general.id') }}: {{ $project->id }}</span>
            </div>
            @if (!$currentUser->inRole('pp-guest'))

            <div class="pull-right">
                <a href="{{ route('bct.projects.projects.edit', $project->id) }}"
                   id="projects_edit"
                   name="projects_edit"
                   data-toggle="tooltip"
                   data-original-title="{{{ trans('action.edit') }}}" class="btn btn-primary"><i
                            class="fa fa-edit fa-fw"></i></a>
            </div>
            @endif
            <div class="pull-right" style="margin: 1px 10px 0 0">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-print fa-fw"></i><span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('bct.projects.projects.pdf.details', $project->id) }}">
                                <i class="fa fa-fw fa-file-pdf-o"></i>Project Details</a>
                        </li>
                        <li>
                            @if(config_projects('bct.projects.settings.project_field_report'))
                                <a href="{{ route('bct.projects.projects.pdf.fieldreport', $project->id) }}">
                                    <i class="fa fa-fw fa-file-pdf-o"></i>Field Report</a>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </nav>

</header>

<div class="panel-body">
    <div class="row visible-print">
        <h4 class="overview-heading text-center visible-print">
            {{ data_get($project, 'name', '') }} -
            {{ trans("bct/projects::projects/common.title") }} {{ trans("bct/projects::general/common.details") }}
        </h4>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="table-responsive">

                <table class="table table-bordered table-details">
                    <tbody>
                    <tr>
                        <td class="td-value">
                            <div>

                                <div class="col-md-6 col-sm-12 border-right col-panel">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.name') }}:</b>
                                            <span class="pre-spaces">
                                                {{ $project->name }}
                                                @if($project->status) -
                                                <span id="status_details"
                                                      data-update-url="{{ route('bct.projects.projects.status',array('id'=>$project->id)) }}"><span
                                                            class="label label-{{ $project->status_color }}">{{ $project->status->name }}</span></span>
                                                @endif
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.address') }}:</b>
                                            <span class="pre-spaces">{{ $project->full_address }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.percent_union') }}:</b>
                                            <span class="pre-spaces"><span id="union-value">{{ number_format($project->union_value * 100, 2) }}
                                                    %</span></span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}
                                                :</b>
                                            <span class="pre-spaces" id="follow_up_date_details"
                                                  data-update-url="{{ route('bct.projects.projects.follow_up_date',array('id'=>$project->id)) }}">{{ date_correct_details_projects($project->follow_up_date) }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.description') }}:</b>
                                            <span class="pre-spaces">{{ $project->description }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::tags/common.tags') }}:</b>
                                            <span class="pre-spaces">@foreach($project->tags as $tag)<span
                                                        class="tag">{{ $tag->name }}</span>@endforeach</span>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-md-6 col-sm-12 col-panel">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.classification_id') }}
                                                :</b>
                                            <span class="pre-spaces">{{ ($project->classification) ? $project->classification->name : '' }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.wage') }}:</b>
                                            <span class="pre-spaces">{{ ($project->prevailingWage) ? $project->prevailingWage->name : '' }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.region') }}:</b>
                                            <span class="pre-spaces">{{ $project->region_abrv  }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.visibility_id') }}:</b>
                                            <span class="pre-spaces">
                                                @if ($project->visibility)
                                                    <span class="label label-{{ $project->visibility_color }}">{{ $project->visibility->name }}</span>
                                                @endif
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.type_id') }}:</b>
                                            <span class="pre-spaces">{{ ($project->type) ? $project->type->type : '' }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::projects/model.general.value_id') }}:</b>
                                            <span class="pre-spaces">{{ ($project->value) ? $project->value->value : '' }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::projects/model.general.reference_details') }}
                                                :</b>
                                            <span class="pre-spaces">{{ $project->reference_details }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::tags/common.tag_groups') }}:</b>
                                            <span class="pre-spaces">
                                                {{ $tag_groups }}
                                            </span>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

        </div>
    </div>

    <div class="row">
        <div id="visits-div" data-update-url="{{ $visits_url }}" class="col-md-12">
            @include('bct/projects::projects.tabs.details.summary-block.visit-summary')
        </div>
    </div>

    <div class="row">
        <div id="contractors-div" data-update-url="{{ $contractors_url }}" class="col-md-12">
            @include('bct/projects::projects.tabs.details.summary-block.contractor-summary')
        </div>
    </div>

    <div class="row">
        <div id="contacts-div" data-update-url="{{ $contacts_url }}" class="col-md-12">
            @include('bct/projects::projects.tabs.details.summary-block.contact-summary')
        </div>
    </div>

    <div class="row">
        <div id="actions-div" data-update-url="{{ $actions_url }}" class="col-md-12">
            @include('bct/projects::projects.tabs.details.summary-block.actions-history-summary')
        </div>
    </div>

    <div class="row">
        <div id="files-div" data-update-url="{{ $files_url }}" class="col-md-12">
            @include('bct/projects::projects.tabs.details.summary-block.files-summary')
        </div>
    </div>

    <div class="row">
        <div id="lists-div" data-update-url="{{ $lists_url }}" class="col-md-12">
            @include('bct/projects::projects.tabs.details.summary-block.list-summary')
        </div>
    </div>

    <div class="row">
        <div id="members-div" data-update-url="{{ $members_url }}" class="col-md-12">
            @include('bct/projects::projects.tabs.details.summary-block.members-summary')
        </div>
    </div>

</div>