<script type="text/template" data-grid="groups-added" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td>
            <input input="" type="hidden" value="<%- r.id %>">
            <%- r.name %>
        </td>
        <td>
        <% if(r.individual) { %>
               {{ trans('bct/projects::groups/model.general.individual') }}
            <% } else { %>
                {{ trans('bct/projects::groups/model.general.group') }}
            <% } %>
        </td>
        <td><%- r.description %></td>
        <td>
            <% if(r.status) { %>
            <span class="label label-success"><%- r.status_name %></span>
            <% } else { %>
            <span class="label label-danger"><%- r.status_name %></span>
            <% } %>
        </td>
        <td>
            <button class="btn btn-danger btn-circle remove-from-group" data-toggle="tooltip" data-placement="right"
               data-original-title="{{{ trans('bct/projects::groups/common.remove_from_group') }}}"><i
                        class="fa fa-minus fa-fw"></i></button>
        </td>
    </tr>

    <% }); %>

</script>
