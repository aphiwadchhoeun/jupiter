var ProjectContactExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectContactExtension = Object.create(ExtensionBase);
    ProjectContactExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-contact'
    });

    // Initialize functions
    ProjectContactExtension.Index.init = function () {
        ProjectContactExtension.Index.dataGrid();
        ProjectContactExtension.Index.listeners();
    };

    // Add Listeners
    ProjectContactExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', ProjectContactExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    ProjectContactExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    ProjectContactExtension.Index.exporterStatus = function (grid) {
        $('#project-contact-exporter').toggleClass('hide', grid.pagination.filtered == 0);
        $('#project-contact-exporter').parent().toggleClass('hide', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    ProjectContactExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            events: {
                sorted: function(response) {
                    if (response.pagination.total== 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function(response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container' ).removeClass('hide');
                    }
                },
            },
            callback: function (data) {
                ProjectContactExtension.Index
                    .exporterStatus(data);
            }
        };

        ProjectContactExtension.Index.Grid = $.datagrid('project-contact', '#project-contact-data-grid', '.project-contact-data-grid_pagination', '#project-contact-data-grid_applied', config);
    };

    // Job done, lets run.
    ProjectContactExtension.Index.init();

})(window, document, jQuery);

// Project Contact Data Grid
$('#project-contact-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-gear');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-gear');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var delete_btn_modal = $('#delete-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectContactExtension.Index.Grid.refresh();
                                    contactsSummaryUpdate();
                                } else {
                                    swal(data);
                                }
                            })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });

                if (delete_btn_modal) {
                    delete_btn_modal.on('click', function () {
                            delete_btn_modal.prop('disabled', true);
                            var url = $(this).data('url');
                            $.post(url, {})
                                .done(function (data) {
                                    if (data == 'success') {
                                        $modal.modal('hide');
                                        ProjectContactExtension.Index.Grid.refresh();
                                        contactsSummaryUpdate();
                                    } else {
                                        swal('Deleting failed.');
                                    }
                                })
                                .always(function () {
                                    delete_btn_modal.removeAttr('disabled');
                                });
                        }
                    )
                    ;
                }
            }

        }
        else {
            location.reload();
        }
    });
})
;