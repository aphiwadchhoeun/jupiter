<span class="navbar-brand">{{ trans("bct/projects::visits/common.title") }} <span id="visits-counter" class="text14">( {{ $visits->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $visits->count() }} )</span></span>
@if (!$currentUser->inRole('pp-guest'))

<a data-id="#visits" id="visits_tab" name="visits_tab" class="btn btn-primary go-to-tab pull-right"
   data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
            class="fa fa-edit fa-fw"></i></a>

<a href="{{ route('bct.projects.projects.createVisit', $project->id) }}"
    id="visit_new"
    name="visit_new"
   data-modal
   class="btn btn-success pull-right visits-add-btn" data-toggle="tooltip"
   data-original-title="{{{ trans('action.create') }}}">
    <i class="fa fa-plus fa-fw"></i>
</a>
@endif
<table class="table table-striped table-bordered">
    <tbody>
    <tr>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::visits/model.general.reps') }}</td>
        <td class="td-label col-md-5 text-center">{{trans('bct/projects::projects/model.general.follow_up_reason')}} & {{ trans('bct/projects::visits/model.general.notes') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.stage') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.visit_date_time') }}</td>

    </tr>
    @foreach($visits->take(5) as $visit)
        <tr>
            <td class="td-value">{{ $visit->users_names }}</td>
            <td class="td-value">
                <div><span class="text-bold">{{trans('bct/projects::projects/model.general.follow_up')}}:</span> {{ $visit->reason }}</div>
                <div><span class="text-bold">{{ trans('bct/projects::visits/model.general.notes') }}:</span> {{ $visit->details }}</div>

                @if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1)
                    <div><span class="text-bold">{{ trans('bct/projects::visits/model.general.weather') }}:</span> {{ $visit->weather }}</div>
                @endif

                @if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1)
                    <div><span class="text-bold">{{ trans('bct/projects::visits/model.general.workers_count') }}:</span> {{ $visit->workers_count }}</div>
                @endif
            </td>
            <td class="td-value text-center">{{ $visit->stages_names }}</td>
            <td class="td-value text-center">{{ date_correct_details_projects( $visit->created_at ) }}</td>
        </tr>
    @endforeach
</table>

