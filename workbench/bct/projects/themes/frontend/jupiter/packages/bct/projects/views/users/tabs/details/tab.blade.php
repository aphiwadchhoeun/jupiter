<header class="panel-heading">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip" href="{{ route('bct.projects.users.all') }}"
                           data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $user->first_name }} {{ $user->last_name }} {{ trans("bct/projects::general/common.details") }}</span>
            </div>

            <div class="pull-right">
                <a href="{{ route('bct.projects.users.edit', $user->id) }}" data-toggle="tooltip"
                   id="user_edit"
                   name="user_edit"
                   data-original-title="{{{ trans('action.edit') }}}" class="btn btn-primary"><i
                            class="fa fa-edit fa-fw"></i></a>
            </div>

        </div>

    </nav>

</header>


<div class="panel-body">

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-details">
                <tbody>
                <tr>
                    <td class="td-value">
                        <div>

                            <div class="col-md-12 col-sm-12 col-panel">
                                <div class="row">
                                    <div class="col-md-8">
                                        <b>{{ trans('bct/projects::users/model.general.name') }}:</b>
                                        <span class="pre-spaces">{{ $user->full_name }}</span>
                                    </div>

                                    <div class="col-md-4">
                                        <b>{{ trans('bct/projects::users/model.general.phone') }}:</b>
                                        <span class="pre-spaces">
                                            @if ($user->profile)
                                                <a href="tel:{{ $user->profile->business_phone }}">{{ $user->profile->business_phone }}</a>
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <b>{{ trans('bct/projects::users/model.general.email') }}:</b>
                                        <span class="pre-spaces">
                                            <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                        </span>
                                    </div>

                                    <div class="col-md-4">
                                        <b>{{ trans('bct/projects::users/model.general.password') }}:</b>
                                        <span class="pre-spaces">
                                            <a href="{{ route('bct.password_reminder', $user->id) }}"
                                               class="btn btn-primary btn-sm margin-right5"
                                               data-toggle="tooltip"
                                               data-original-title="{{{ trans('bct/projects::general/common.reset') }}}"><i
                                                        class="fa fa-key fa-fw"></i>{{ trans('bct/projects::general/common.reset') }}
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        <div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>


    </div>


    <div class="row">
        <div class="col-md-6">
            <span class="navbar-brand text-bold">{{{ trans("bct/projects::groups/common.title") }}}
                        <span id="groups-counter" class="text14">({{ $grid_groups_count }} of {{ $groups_count }})</span>
                    </span>

            <a data-id="#groups" class="btn btn-primary pull-right go-to-tab"
               id="groups_tab"
               name="groups_tab"
               data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
                        class="fa fa-edit fa-fw"></i></a>

            <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                    <th class="td-label col-md-6 text-center">{{ trans('bct/projects::groups/model.general.name') }}</th>
                    <th class="td-label col-md-6 text-center">{{ trans('bct/projects::groups/model.general.status') }}</th>
                </tr>
                @if ($user_groups)
                    @foreach($user_groups as $group)
                        <tr>
                            <td class="td-value">{{ $group->name }}</td>
                            <td class="td-value text-center">
                                @if ($group->status)
                                    <span class="label label-success">{{ trans('bct/projects::general/common.active') }}</span>
                                @else
                                    <span class="label label-danger">{{ trans('bct/projects::general/common.disabled') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>

        <div class="col-md-6">
            <span class="navbar-brand text-bold">{{ trans("bct/projects::users/common.tabs.visits") }}
                <span id="visits-counter" class="text14">({{ $grid_visits_count }} of {{ $visits_count }})</span>
            </span>

            <a data-id="#visits" class="btn btn-primary pull-right go-to-tab"
               id="visits_tab"
               name="visits_tab"
               data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
                        class="fa fa-edit fa-fw"></i></a>

            <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                    <th class="td-label col-md-6 text-center">{{ trans('bct/projects::projects/common.project') }}</th>
                    <th class="td-label col-md-6 text-center">{{ trans('bct/projects::visits/model.general.date') }}</th>
                </tr>
                @if ($user_visits)
                    @foreach($user_visits as $visit)
                        <tr>
                            <td class="td-value">{{ $visit->project->name }}</td>
                            <td class="td-value text-center">{{ date('M-d-Y', strtotime($visit->created_at)) }}</td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>

    </div>

</div>