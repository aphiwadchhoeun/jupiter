{{ Asset::queue('project-action-grid', 'bct/projects::projects/js/actions/project-action.js', 'extension-base') }}

<section class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <header class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#actionshistory-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::projects/common.project_actions') }}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="actionshistory-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-actions-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span
                                        class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="project-action" role="form">

                        <div class="input-group">

                            <span class="input-group-btn">

                                <button class="btn btn-default" type="button" data-grid-calendar
                                        id="project-action-filter-calendar"
                                        data-range-filter="created_at">
                                    <i class="fa fa-calendar"></i>
                                </button>


                            </span>

                            <span class="input-group">
                                <select name="column" class="form-control" id="project-action-filter-select">
                                    <option value="action_type">{{ trans('bct/projects::actions/model.general.action_type') }}
                                    </option>
                                    <option value="details">{{ trans('bct/projects::actions/model.general.details') }}
                                    </option>
                                    <option value="username">{{ trans('bct/projects::actions/model.general.user') }}
                                    </option>
                                    <option value="contractor">{{ trans('bct/projects::actions/model.general.contractor_id') }}
                                    </option>
                                </select>
                            </span>

                            <span class="input-group-btn">
                                <input class="form-control" name="filter" type="text" id="project-action-filter-input-search"
                                       placeholder="{{{ trans('common.search') }}}">

                                <button class="btn btn-default" type="submit" id="project-action-filter-btn-search">
                                    <span class="fa fa-search"></span>
                                </button>

                                <button class="btn btn-default" data-grid="project-action" data-reset id="project-action-filter-btn-reset">
                                    <i class="fa fa-refresh fa-sm"></i>
                                </button>

                            </span>

                        </div>

                        <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-start></button>
                        <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-end></button>


                    </form>
                    @if (!$currentUser->inRole('pp-guest'))

                    <div class="nav navbar-nav navbar-right">

                        <a href="{{ route('bct.projects.projects.createAction', $project->id) }}"
                            id="action_new"
                            name="action_new"
                           data-modal
                           class="btn btn-success pull-right margin-right5 actions-add-btn" data-toggle="tooltip"
                           id="project-action-filter-btn-create"
                           data-original-title="{{{ trans('action.create') }}}">
                            <i class="fa fa-plus fa-fw"></i>
                        </a>

                    </div>
                    @endif

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="project-action-data-grid-applied-filters">

            <div id="project-action-data-grid_applied" class="btn-group" data-grid="project-action"></div>

        </div>

        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="project-action-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridActions', $project->id) }}"
                   data-filename="{{ $project->name }} {{ trans("bct/projects::projects/common.tabs.actions") }}"
                   data-grid="project-action">
                <thead>
                <tr>
                    <th class="sortable col-md-2"
                        data-sort="action_type">{{ trans('bct/projects::actions/model.general.action_type') }}</th>
                    <th class="sortable col-md-4"
                        data-sort="details">{{ trans('bct/projects::actions/model.general.details') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="username">{{ trans('bct/projects::actions/model.general.user') }}</th>
                    <th class="sortable"
                        data-sort="contractor">{{ trans('bct/projects::actions/model.general.contractor_id') }}</th>
                    <th class="sortable"
                        data-sort="action_date">{{ trans('bct/projects::actions/model.general.date') }}</th>
                    @if (!$currentUser->inRole('pp-guest'))
                    <th class="hidden-print">{{{ trans('bct/projects::general/common.actions') }}}</th>
                    @endif
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>

    <footer class="panel-footer clearfix hidden-print">

        {{-- Grid: Pagination --}}
        <div class="project-action-data-grid_pagination" data-grid="project-action"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.actions.grid.project-actions.results')
    @include('bct/projects::projects.tabs.actions.grid.project-actions.pagination')
    @include('bct/projects::projects.tabs.actions.grid.project-actions.filters')
    @include('bct/projects::projects.tabs.actions.grid.project-actions.no_results')

</section>