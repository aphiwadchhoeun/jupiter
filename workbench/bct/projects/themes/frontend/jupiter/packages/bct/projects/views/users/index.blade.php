@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::users/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
    {{ Asset::queue('index', 'bct/projects::users/js/index.js', 'extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::users/common.title_reps') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span
                                            class="visible-xs-inline">{{{ trans('action.export') }}}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="user" role="form">

                            <div class="input-group">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" disabled>
                                        {{{ trans('common.status') }}}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                        <li>
                                            <a href="#" data-filter="status:1">
                                                <i class="fa fa-check-circle"></i> {{ trans('bct/projects::general/common.active') }}
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#" data-filter="status:0">
                                                <i class="fa fa-times-circle"></i> {{ trans('bct/projects::general/common.disabled') }}
                                            </a>
                                        </li>
                                    </ul>

                                    <button class="btn btn-default hidden-xs" type="button" data-grid-calendar
                                            data-range-filter="users.created_at">
                                        <i class="fa fa-calendar"></i>
                                    </button>

                                </span>

                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="name">{{ trans('bct/projects::users/model.general.name') }}</option>
                                        <option value="role">{{ trans('bct/projects::users/model.general.role') }}</option>
                                        <option value="email">{{ trans('bct/projects::users/model.general.email') }}</option>
                                    </select>
                                </span>

                                    <input class="form-control" name="filter" type="text"
                                           placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="user" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>

                            <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-start></button>
                            <input type="hidden" data-range-filter="created_at" data-format="YYYY-MM-DD" data-range-end></button>


                        </form>

                        <div class="pull-right">
                            <a href="{{ route('bct.projects.users.create') }}"
                                id="user_new"
                                name="user_new"
                                class="btn btn-success"
                               data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
                                <i class="fa fa-plus fa-fw"></i>
                            </a>

                        </div>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="user"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.users.grid') }}"
                       data-filename="{{ trans('bct/projects::users/common.title_reps') }}"
                       data-grid="user">
                    <thead>
                    <tr>
                        <th class="sortable col-md-4" data-sort="name">{{ trans('bct/projects::users/model.general.name') }}</th>
                        <th class="sortable col-md-3" data-sort="email">{{ trans('bct/projects::users/model.general.email') }}</th>
                        <th class="sortable col-md-2" data-sort="created_at">{{{ trans('bct/projects::users/model.general.created_at') }}}</th>
                        <th class="col-md-1 sortable" data-sort="role">{{ trans('bct/projects::users/model.general.role') }}</th>
                        <th class="sortable col-md-1" data-sort="status">{{ trans('bct/projects::users/model.general.status') }}</th>
                        <th class="col-md-1 hidden-print">{{{ trans('bct/projects::general/common.details') }}}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>

        <footer class="panel-footer hidden-print">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination clearfix" data-grid="user"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::users.grid.index.results')
        @include('bct/projects::users.grid.index.pagination')
        @include('bct/projects::users.grid.index.filters')
        @include('bct/projects::users.grid.index.no_results')

    </section>

@stop
