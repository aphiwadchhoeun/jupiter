/**
 * Created by roman on 12.02.16.
 */

jQuery('document').ready(function ($) {
    var $map_radius = $('input[name="map_radius"]'),
        $map_count_projects = $('input[name="map_count_projects"]'),
        $map_week_start = $('.map_week_start'),
        $map_week_end = $('.map_week_end'),
        $map_union_start = $('.map_union_start'),
        $map_union_end = $('.map_union_end'),
        $map_less_4 = $('select[name="map_less_4"]'),
        $map_4_8 = $('select[name="map_4_8"]'),
        $map_more_8 = $('select[name="map_more_8"]'),
        $map_union_1 = $('select[name="map_union_1"]'),
        $map_union_2 = $('select[name="map_union_2"]'),
        $map_union_3 = $('select[name="map_union_3"]'),
        $btn_settings_update = $('.btn-settings-update'),
        $visit_auto_approve = $('input[name="visit_auto_approve"]'),
        $project_field_report = $('input[name="project_field_report"]'),
        $enable_project_visit_workers = $('input[name="enable_project_visit_workers"]'),
        $enable_project_visit_weather = $('input[name="enable_project_visit_weather"]'),
        $enable_project_visit_date    = $('input[name="enable_project_visit_date"]'),
        inputs_data = {};

    $visit_auto_approve.bootstrapToggle();

    $project_field_report.bootstrapToggle();
    $enable_project_visit_workers.bootstrapToggle();
    $enable_project_visit_weather.bootstrapToggle();
    $enable_project_visit_date.bootstrapToggle();

    $map_radius.numeric({
        negative: false
    }).keypress(function(e){
        if (this.value.length == 0 && e.which == 48 ){
            return false;
        }
    });

    $map_count_projects.numeric({
        negative: false
    }).keypress(function(e){
        if (this.value.length == 0 && e.which == 48 ){
            return false;
        }
    });

    $map_week_start.numeric({
        negative: false
    }).keypress(function(e){
        if (this.value.length == 0 && e.which == 48 ){
            return false;
        }
    });

    $map_week_end.numeric({
        negative: false
    }).keypress(function(e){
        if (this.value.length == 0 && e.which == 48 ){
            return false;
        }
    });

    $map_union_start.numeric({
        negative: false
    }).keypress(function(e){
        if (this.value.length == 0 && e.which == 48 ){
            return false;
        }
    });

    $map_union_end.numeric({
        negative: false
    }).keypress(function(e){
        if (this.value.length == 0 && e.which == 48 ){
            return false;
        }
    });

    $map_less_4.selectize({
        create: false,
        maxItems: 1,
        dropdownParent: 'body'
    });

    $map_4_8.selectize({
        create: false,
        maxItems: 1,
        dropdownParent: 'body'
    });

    $map_more_8.selectize({
        create: false,
        maxItems: 1,
        dropdownParent: 'body'
    });

    $map_union_1.selectize({
        create: false,
        maxItems: 1,
        dropdownParent: 'body'
    });

    $map_union_2.selectize({
        create: false,
        maxItems: 1,
        dropdownParent: 'body'
    });

    $map_union_3.selectize({
        create: false,
        maxItems: 1,
        dropdownParent: 'body'
    });

    $('.map_week_start_first').on('keyup', function(e) {
        $('.map_week_start_second').val($(this).val());
        $('input[name="map_week_start"]').val($(this).val());
    });

    $('.map_week_start_second').on('keyup', function(e) {
        $('.map_week_start_first').val($(this).val());
        $('input[name="map_week_start"]').val($(this).val());
    });

    $('.map_week_end_first').on('keyup', function(e) {
        $('.map_week_end_second').val($(this).val());
        $('input[name="map_week_end"]').val($(this).val());
    });

    $('.map_week_end_second').on('keyup', function(e) {
        $('.map_week_end_first').val($(this).val());
        $('input[name="map_week_end"]').val($(this).val());
    });

    $('.map_union_start_first').on('keyup', function(e) {
        $('.map_union_start_second').val($(this).val());
        $('input[name="map_union_start"]').val($(this).val());
    });

    $('.map_union_start_second').on('keyup', function(e) {
        $('.map_union_start_first').val($(this).val());
        $('input[name="map_union_start"]').val($(this).val());
    });

    $('.map_union_end_first').on('keyup', function(e) {
        $('.map_union_end_second').val($(this).val());
        $('input[name="map_union_end"]').val($(this).val());
    });

    $('.map_union_end_second').on('keyup', function(e) {
        $('.map_union_end_first').val($(this).val());
        $('input[name="map_union_end"]').val($(this).val());
    });

    $btn_settings_update.on('click', function(e) {
        e.preventDefault();

        var send_data = true;

        $(
            '.panel-body-configuration input[type="text"]:not(.not-send),' +
            '.panel-body-configuration input[type="hidden"]:not(.not-send),' +
            ' .panel-body-configuration select:not(.not-send)'
        ).each(function() {
            if (typeof $(this).attr('name') !== 'undefined' && !$(this).val()) {
                $(this).parent('div').addClass('has-error');
                $(this).next('.help-block').text('The field is required.');
                send_data = false;
            } else {
                $(this).parent('div').removeClass('has-error');
                $(this).next('.help-block').text('');
            }
        });

        if (send_data === true) {
            $(
                '.panel-body-configuration input:not(.not-send), ' +
                '.panel-body-configuration select:not(.not-send), ' +
                '.panel-body-configuration textarea:not(.not-send)'
            ).each(function() {
                if ($(this).attr('type') === 'checkbox') {
                    inputs_data[$(this).attr("name")] = $(this).is(":checked") ? 1 : 0;
                } else if (typeof $(this).attr('name') !== 'undefined') {
                    inputs_data[$(this).attr("name")] = $(this).val();
                }
            });

            var url = window.location.origin + '/pprofiles/settings/configuration';

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: inputs_data,
                success: function(response) {
                    swal({
                        title: "Success!",
                        text: "Settings were updated",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });
                },
                error: function(response) {
                    var errors = JSON.parse(response.responseText).errors;

                    swal({
                        title: "Error!",
                        text: "=(",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    });
                }
            });
        }

    });
});