<div class="panel-body">
    <div id="files-block">
        <div class="col-md-12">
            <span class="navbar-brand">{{ trans('bct/projects::projects/common.tabs.files') }}
                <div class="loader">
                        <div class="sk-circle">
                            <div class="sk-circle1 sk-child"></div>
                            <div class="sk-circle2 sk-child"></div>
                            <div class="sk-circle3 sk-child"></div>
                            <div class="sk-circle4 sk-child"></div>
                            <div class="sk-circle5 sk-child"></div>
                            <div class="sk-circle6 sk-child"></div>
                            <div class="sk-circle7 sk-child"></div>
                            <div class="sk-circle8 sk-child"></div>
                            <div class="sk-circle9 sk-child"></div>
                            <div class="sk-circle10 sk-child"></div>
                            <div class="sk-circle11 sk-child"></div>
                            <div class="sk-circle12 sk-child"></div>
                        </div>
                    </div>
            </span>
            <div class="pull-right">
                <div class="btn btn-success add-file" data-toggle="tooltip" data-placement="top" data-original-title="Add File">
                    <i class="fa fa-plus fa-fw ie11-btn-fa"></i>
                </div>
            </div>
            <table role="presentation" class="table table-striped table-bordered">
                <tbody class="files">
                <tr>
                    <th class="col-md-4">File Name</th>
                    <th class="col-md-4">Description</th>
                    <th class="text-center col-md-1">Size</th>
                    <th class="text-center col-md-2">Upload Date</th>
                    <th class="text-center col-md-1">Action</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    @include('bct/projects::projects/tabs/files/templates/template_files')
    @include('bct/projects::partials/modals/create_project')
</div>

