<div class="input-group select-width">
    <span class="input-group-btn">
        <button class="btn btn-default" type="button" disabled>
            {{ trans('bct/projects::reports/common.placeholder.project_type') }}
        </button>
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul name="column" class="dropdown-menu project_type" role="menu" data-filter-reset>
            @foreach($project_types as $type)
                <li><a href="#" data-filter="project_type:{{$type->id}}" value="{{$type->id}}">{{$type->type}}</a></li>
            @endforeach
        </ul>
    </span>

</div>