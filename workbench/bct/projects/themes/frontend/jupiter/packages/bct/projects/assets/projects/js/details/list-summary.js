/**
 * Created by Aphiwad on 10/28/2015.
 */
var projectListSummary;

if (!projectListSummary) {
    var options = {
        events: {
            'fetching': function () {
                $('#lists-counter').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            'fetched': function (result) {
                var num = (result.pagination.filtered > 5) ? 5 : result.pagination.filtered;
                $('#lists-counter').text('(' + num + ' of ' + result.pagination.total + ')');
            }
        }
    };

    projectListSummary = $.datagrid(
        'project-lists-summary',
        '#project-lists-summary',
        'null',
        'null',
        options);
}