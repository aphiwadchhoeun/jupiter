<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    {{ Asset::queue('normalize', 'bct/projects::custom/css/normalize.css') }}
    {{ Asset::queue('bootstrap-grid', 'bootstrap/css/bootstrap-grid-export.css') }}

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach

</head>
<body>

@include('bct/projects::pdf/partials/header')

@if ($results)

    <div class="hr10"></div>

    <div class="ext-profiles">

    @foreach($results as $region => $projects)

        @if (!$projects->isEmpty())

                <div class="row">

                    <div class="col-lg-12">

                        <div class="navbar-brand text-center w100">Active Projects - {{ $region }}</div>

                        <div style="clear:both; height: 10px;"></div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table class="table table-bordered table-details">

                            <thead>
                            <tr>
                                <th class="col-lg-1">{{ trans('bct/projects::reports/common.col.project_id') }}</th>
                                <th class="col-lg-4">{{ trans('bct/projects::reports/common.col.project_name') }}</th>
                                <th class="col-lg-3">{{ trans('bct/projects::reports/common.col.project_address') }}</th>
                                <th class="col-lg-1">{{ trans('bct/projects::reports/common.col.project_city') }}</th>
                                <th class="col-lg-3">{{ trans('bct/projects::reports/common.col.stewards') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $item)

                                <tr>
                                    <td>{{ $item->project_id }}</td>
                                    <td>{{ $item->project_name }}</td>
                                    <td>{{ $item->project_address }}</td>
                                    <td>{{ $item->project_city }}</td>
                                    <td>{{ $item->stewards }}</td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>

                    </div>
                </div>

        @endif

    @endforeach

    </div>

@endif

</body>
</html>
