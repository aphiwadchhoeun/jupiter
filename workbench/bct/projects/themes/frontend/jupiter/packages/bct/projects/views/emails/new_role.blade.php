<table cellspacing="0" cellpadding="0" style="padding:30px 10px;background:#eee;width:100%;font-family:arial">

    <tbody>
    <tr>
        <td>
            <table align="center" cellspacing="0" style="max-width:650px;min-width:320px">
                <tbody>
                <tr>
                    <td style="text-align:left;padding-bottom:14px">
                        <img align="left"
                             src="{{ getLogo() }}">
                    </td>
                </tr>
                <tr>
                    <td align="center" style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px">

                        <div>
                            <div>

                                <div>

                                </div>
                            </div>

                        </div>
                        <table align="center">
                            <tbody>
                            <tr>
                                <td style="border-bottom:1px solid #dfdfd0;color:#666;text-align:center;padding-bottom:30px">


                                    <table align="center" style="margin:auto">
                                        <tbody>

                                        <tr>
                                            <td style="color:#666;font-size:20px;font-weight:bold;text-align:center;font-family:arial">

                                                Hi {{ $user->first_name }}, You have just gained new role: {{ $role }}!
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="color:#aaa;padding:15px;font-size:11px;line-height:15px;text-align:left">

                                    <div style="color:#aaa;font-family:arial">
                                        It is also a good idea to add <a style="color:#aaa">noreply@unionimpact.com</a>
                                        to your address book to ensure that you receive our messages (no spam, we
                                        promise!).
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>

                </tbody>
            </table>
        </td>
    </tr>

    </tbody>
</table>
