/**
 * Created by roman on 22.01.16.
 */

String.prototype.capitalize = function(){
    return this.toLowerCase().replace( /\b\w/g, function (m) {
        return m.toUpperCase();
    });
};

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

function showDirectionPP(field){
    $('.ranges .direction').remove();
    $('.ranges').each(function(){
        if($(this).find('.direction').length == 0 && $(this).is(':visible')){
            $(this).append('<div class="direction text-bold" style="margin-top:10px;">Select the '+field+' date range to filter results</div>');
        }
    });

}