<script type="text/template" data-grid="contractor_summary" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>

            <td><%- r.contractor_name %></td>
            <td><span class="label label-<%- r.is_union_color %>"><%- r.is_union_text %></span></td>
            <% if(r.projects_total>0){ %>
            <td><a href="<%- r.all_projects_url %>"><%- r.projects_total %><a/></td>
            <% } else { %>
            <td><%- r.projects_total %></td>
            <% } %>
            <% if(r.projects_active>0){ %>
            <td><a href="<%- r.active_projects_url %>"><%- r.projects_active %><a/></td>
            <% } else { %>
            <td><%- r.projects_active %></td>
            <% } %>
            <% if(r.projects_completed>0){ %>
            <td><a href="<%- r.completed_projects_url %>"><%- r.projects_completed %><a/></td>
            <% } else { %>
            <td><%- r.projects_completed %></td>
            <% } %>

            <td><%- r.projects_visits %></td>
            <td><%- r.projects_actions %></td>
		</tr>

	<% }); %>

</script>
