<script type="text/template" data-grid="project-lists-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.name %></td>
        <td><%- r.username %></td>
        <td><%- r['created'] %></td>
    </tr>

    <% }); %>

</script>
