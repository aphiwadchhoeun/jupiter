<div class="input-group select-width">
    <span class="input-group-btn">
        <button class="btn btn-default" type="button" disabled>
           {{ trans('bct/projects::reports/common.placeholder.project_classification') }}
        </button>
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul name="column" class="dropdown-menu classification" role="menu" data-filter-reset>
            @foreach($project_classifications as $classification)
                <li><a href="#" data-filter="classification:{{$classification->id}}" value="{{$classification->id}}">{{$classification->name}}</a></li>
            @endforeach
        </ul>
    </span>

</div>
