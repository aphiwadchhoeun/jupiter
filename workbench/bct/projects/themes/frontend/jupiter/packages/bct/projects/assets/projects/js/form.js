var initialize;
var positionFromRequest = false;
jQuery('document').ready(function ($) {
    var submit_btn = $('#submit-btn');
    var main_form = $('#content-form');
    var searchmapInput = $('#searchmap');
    var stagesInput = $('#stages');
    var stateInput = $('#state');
    var countyInput = $('#county');
    var cityInput = $('#city');
    var zipCodeInput = $('#zipcode');
    var followUpDateInput = $('#follow_up_date');
    var latInput = $('#lat');
    var lngInput = $('#lng');
    var regionInput = $('#region');
    var regionAbrvInput = $('#region_abrv');
    var budgetgroupInput = $('#budgetgroup');
    var oldPosition;
    var addressSuggest = $('#address-suggest');
    var stateSuggest = $('#state-suggest');
    var countySuggest = $('#county-suggest');
    var citySuggest = $('#city-suggest');
    var zipcodeSuggest = $('#zipcode-suggest');
    var searchSuggest = $('#search-suggest');
    var classification = $('#z_project_classification_id');
    var contractors_count = classification.data('contractors-count');
    var cartodb_api = null, cartodb_table = null;
    var default_lat = (typeof BctProjects.default_lat !== 'undefined') ? BctProjects.default_lat : '',
        default_lng = (typeof BctProjects.default_lng !== 'undefined') ? BctProjects.default_lng : '';


    function fetchCartodbConfigs(callback) {
        if (cartodb_api == null || cartodb_table == null) {
            $.get('/ui/api/cartodb/configs/get')
                .done(function (data) {
                    cartodb_api = data.api_url;
                    cartodb_table = data.tablename;
                    callback();
                });
        } else {
            callback();
        }
    }

    $('input[name="zipcode"]').mask('99999?-9999');


    $('#project-tags-list').selectize({
        plugins: ['remove_button'],
    });
    stagesInput.selectize({
        plugins: ['remove_button'],
    });
    if (contractors_count > 0) {
        classification.on('click', function () {
            swal({
                title: "You must remove all contractors from this project before changing the classification value.",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#019964",
                confirmButtonText: "OK",
                showLoaderOnConfirm: true,
            }).then(function () {
                classification.attr('disabled', 'disabled');
            });
        });

    }
    submit_btn.click(function (e) {
        e.preventDefault();
        var submitbtn = $(this);
        toggleSubmitButton(submitbtn, true);

        var selected_status = $("#z_project_status_id option:selected").text();
        if (selected_status == "Completed") {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            var url = window.location.origin + window.location.pathname + "/check";
            $.ajax({
                type: 'POST',
                url: url,
                data: {},
                success: function (response) {
                    var text = "This project exists in some lists! Project will be deleted from lists below!\r\n";
                    text += "Lists: " + response + "\r\n";
                    text += "Do you agree?";
                    swal({
                        title: text,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#019964",
                        confirmButtonText: "Yes!",
                        cancelButtonText: "No",
                        showLoaderOnConfirm: true,
                    }).then(function () {
                        main_form.submit();
                        toggleSubmitButton(submitbtn, false);
                    });
                },
                error: function () {
                    swal({
                        title: "Are you sure you want to save?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#019964",
                        confirmButtonText: "Yes!",
                        cancelButtonText: "No",
                        showLoaderOnConfirm: true,
                    }).then(function () {
                        main_form.submit();
                        toggleSubmitButton(submitbtn, false);
                    });
                },
            })
                .always(function () {
                    toggleSubmitButton($(this), false);
                });
        }
        else {
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
            }).then(function () {
                main_form.submit();
                toggleSubmitButton(submitbtn, false);
            });
        }
    });

    function toggleSubmitButton(btn, enable) {
        btn.prop('disabled', enable);
    }

    /** datepicker */
    followUpDateInput.parents('.date').datepicker({
        format: 'M-dd-yyyy',
        assumeNearbyYear: true,
        startDate: new Date()
    }).on('changeDate', function() {
        if (followUpDateInput.val()) {
            submit_btn.prop('disabled',false);
            $(this).parent().removeClass('has-error');
            $(this).parent().find('.help-block').text('');
        }
    }).on('focusout', function () {
        var date = followUpDateInput.val();
        if (date !== '') {
            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
            if(valid_date===false){
                submit_btn.prop('disabled',true);
                $(this).parent().addClass('has-error');
                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
            }
            else{
                submit_btn.prop('disabled',false);
                $(this).parent().removeClass('has-error');
                $(this).parent().find('.help-block').text('');
            }
        }
    });

    /** disable enter event on searchmap input */
    searchmapInput.on('keypress', function (e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    var geocoder;
    var searchBox;

    /** for google map */
    initialize = function () {
        geocoder = new google.maps.Geocoder();

        var mapOptions = {
            zoom: 17,
            streetViewControl: false,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [
                        {visibility: 'off'}
                    ]
                }
            ]
        };

        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var markerOptions = {
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP
        };

        var marker = new google.maps.Marker(markerOptions);

        // Try HTML5 geolocation

        /** try to get cookies from browser */
        getLocationFromRequest();
        if (!latInput.val() || !lngInput.val()) {
            latInput.val(getCookie('lat'));
            lngInput.val(getCookie('lng'))
        }

        if (navigator.geolocation && (!latInput.val() || !lngInput.val())) {
            navigator.geolocation.getCurrentPosition(function (position) {
                markerOptions.pos = new google.maps.LatLng(position.coords.latitude,
                    position.coords.longitude);

                handleNoGeolocation(false);

            }, function () {
                handleNoGeolocation(true);
            });
        } else {
            markerOptions.pos = new google.maps.LatLng(latInput.val(), lngInput.val());

            handleNoGeolocation(false);
        }

        function handleNoGeolocation(errorFlag) {
            if (errorFlag) {
                map.setCenter(new google.maps.LatLng(default_lat, default_lng));
            } else {
                map.setCenter(markerOptions.pos);
                marker.setPosition(markerOptions.pos);
                getAddress(marker.getPosition());
            }
        }

        /** init marker in load page */
        changePosition(marker);

        // add a click event handler to the map object
        google.maps.event.addDomListener(map, "click", function (event) {
            applyPositionChange(function () {
                marker.setMap(null);

                marker = new google.maps.Marker(markerOptions);

                changePosition(marker);

                // place a marker
                marker.setPosition(event.latLng);

                getAddress(marker.getPosition());
            });

        });

        searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

        google.maps.event.addDomListener(searchBox, "places_changed", function () {
            applyPositionChange(function () {
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;

                for (i = 0; place = places[i]; i++) {
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }

                map.fitBounds(bounds);
                map.setZoom(17);

                getAddress(marker.getPosition());
            });
        });
    };

    google.maps.event.addDomListener(window, 'load', initialize);

    function changePosition(marker) {
        google.maps.event.addDomListener(marker, 'position_changed', function () {
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            latInput.val(lat);
            lngInput.val(lng);
        });

        google.maps.event.addDomListener(marker, 'dragstart', function () {
            oldPosition = marker.getPosition();
        });

        google.maps.event.addDomListener(marker, 'dragend', function () {
            applyPositionChange(function () {
                getAddress(marker.getPosition());
            }, function () {
                marker.setPosition(oldPosition);
            });
        });
    }

    searchSuggest.click(function (e) {
        e.preventDefault();
        var that = $(this);

        swal({
            title: "Are you sure you want to use the suggested address?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
        }).then(function () {
            $('#address').val(that.data('address'));
            $('#state').val(that.data('state'));
            $('#county').val(that.data('county'));
            $('#city').val(that.data('city'));
            $('#zipcode').val(that.data('zipcode'));
        });

    });

    function getAddress(position) {
        geocoder.geocode({
            latLng: position
        }, function (responses) {
            if (responses && responses.length > 0) {
                /** set address data to inputs */
                searchmapInput.val(responses[0].formatted_address);
                var address = extractFromAdress(responses[0].address_components, 'street_number') + ' ' + extractFromAdress(responses[0].address_components, 'route');


                searchSuggest.data('address', address);
                searchSuggest.data('state', extractFromAdress(responses[0].address_components, 'administrative_area_level_1'));
                searchSuggest.data('county', extractFromAdress(responses[0].address_components, 'administrative_area_level_2'));
                searchSuggest.data('city', extractFromAdress(responses[0].address_components, 'locality'));
                searchSuggest.data('zipcode', extractFromAdress(responses[0].address_components, 'postal_code'));

                if (positionFromRequest === true) {
                    $('#address').val(searchSuggest.data('address'));
                    $('#state').val(searchSuggest.data('state'));
                    $('#county').val(searchSuggest.data('county'));
                    $('#city').val(searchSuggest.data('city'));
                    $('#zipcode').val(searchSuggest.data('zipcode'));
                }
                var lt = latInput.val();
                var lng = lngInput.val();

                fetchCartodbConfigs(function () {
                    var url = cartodb_api + '/sql?q=select areaname, areaabrv, budgetgroup from ' + cartodb_table +
                        ' WHERE ST_Intersects(the_geom,CDB_LatLng(' + lt + ',' + lng + ')) LIMIT 1';

                    $.get(url, function (data) {
                        if (data.rows.length > 0) {
                            regionInput.val(data.rows[0].areaname);
                            regionAbrvInput.val(data.rows[0].areaabrv);
                            budgetgroupInput.val(data.rows[0].budgetgroup);
                        }
                        else if (regionInput.val() == '' || regionAbrvInput.val() == '') {
                            regionInput.val('n/a');
                            regionAbrvInput.val('n/a');
                            budgetgroupInput.val('n/a');
                        }
                    });
                });

            } else {
                /** clear inputs */
                searchmapInput.val('');
                stateInput.val('');
                countyInput.val('');
                cityInput.val('');
                zipCodeInput.val('');
            }
        });
    }

    function extractFromAdress(components, type) {
        for (var i = 0; i < components.length; i++) {
            for (var j = 0; j < components[i].types.length; j++) {
                if (components[i].types[j] == type && type == 'administrative_area_level_1') return components[i].short_name;
                if (components[i].types[j] == type) return components[i].long_name;
            }
        }

        return "";
    }

    function getLocationFromRequest() {

        if (location.search && location.search != '') {
            var query = location.search.substr(1);
            var params = {};
            query.split("&").forEach(function (part) {
                var item = part.split("=");
                params[item[0]] = decodeURIComponent(item[1]);
            });

            if (params.lat.length > 0 && params.lng.length > 0) {
                latInput.val(params.lat);
                lngInput.val(params.lng);
                positionFromRequest = true;
            }
        }

    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    function applyPositionChange(callback, reverse) {
        swal({
            title: "Are you sure you want to drop the pin here?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No"
        }).then(function () {
            callback();
        }, function () {
            if (typeof reverse != 'undefined') {
                reverse();
            }
        });
    }
});