<div class="modal-dialog">

    <div class="modal-content">

        <div class="modal-body upload">

            <div class="upload__instructions">
                <div class="dnd"></div>

                <i class="fa fa-upload fa-5x"></i>
                <h4>Select Files</h4>
                <p class="lead">Acceptable File Types.</p>
                <p class="small">
                    <i>
                        {{ $mimes }}
                    </i>
                </p>

            </div>

            <div class="upload__files" data-media-queue-list ></div>
            
            <div>
                <p class="saving" style="display: none;">Upload in progress<span>.</span><span>.</span><span>.</span></p>
            </div>
            
            <div class="btn btn-default btn-block upload__select">
                <div>Select</div>
                <input name="files" class="upload__select-input" type="file" multiple />
            </div>

        </div>


        <div class="modal-footer">

				<span class="pull-left text-left">
					<div><span data-media-total-files>0</span> files in queue</div>
					<div><span data-media-total-size>0</span> KB out of 30 000 KB</div>
				</span>

				<span class="pull-right text-right">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>

					<button type="button" class="btn btn-primary" id="start_upload" data-media-upload><i class="fa fa-upload"></i> Start Upload</button>
				</span>
        </div>

    </div>

</div>