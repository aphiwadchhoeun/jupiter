<script type="text/template" data-grid="reps-summary" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
            <td><%- r.full_name %></td>
            <td><a class="under-none" href="mailto:<%- r.email %>"><%- r.email %></a></td>
            <td><%- r.roles %></td>
            <td class="text-center">
                <% if(r.status == 'Active') { %>
                <span class="label label-success"><%- r.status %></span>
                <% } else { %>
                <span class="label label-danger"><%- r.status %></span>
                <% } %>
            </td>
		</tr>

	<% }); %>

</script>
