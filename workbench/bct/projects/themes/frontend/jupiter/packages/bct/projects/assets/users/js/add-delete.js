var Extension;

;(function(window, document, $, undefined)
{
	'use strict';
    var gridGroupAdd, gridGroupDel;
    var isBlocked = true;
	var config_groups_add = {
		preventHashChange: true,
		hash: false,
		events: {
			sorted: function(response) {
				if (response.pagination.total== 0) {
					$('[data-grid="groups"].download-container').addClass('hide');
				}
			},
			fetched: function(response) {
				if (response.pagination.filtered == 0) {
					$('[data-grid="groups"].download-container').addClass('hide');
				} else {
					$('[data-grid="groups"].download-container').removeClass('hide');
				}
			},
		},
	};
	var config_groups_del = {
		preventHashChange: true,
		hash: false,
		events: {
			sorted: function(response) {
				if (response.pagination.total== 0) {
					$('[data-grid="groups-added"].download-container').addClass('hide');
				}
			},
			fetched: function(response) {
				if (response.pagination.filtered == 0) {
					$('[data-grid="groups-added"].download-container').addClass('hide');
				} else {
					$('[data-grid="groups-added"].download-container').removeClass('hide');
				}

				if (!gridGroupAdd) {
					gridGroupAdd = $.datagrid('groups', '#data-grid_groups', '.data-grid_groups_pagination', '#data-grid_groups_applied', config_groups_add);
				}
			},
		},
	};

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'groups-added'
    });

	// Initialize functions
	Extension.Index.init = function()
	{
        if (!gridGroupDel) {
            gridGroupDel = $.datagrid('groups-added', '#data-grid_groups_added', '.data-grid_groups_added_pagination', '#data-grid_groups_added_applied', config_groups_del);
        }
        
		Extension.Index.listeners();
	};

	// Add Listeners
	Extension.Index.listeners = function() {
		Platform.Cache.$body
			.on('click', '.add-to-group', Extension.Index.addUserToGroup)
			.on('click', '.remove-from-group', Extension.Index.deleteUserFromGroup)
			.on('click', '#unlock-groups', Extension.Index.unlockActions)
		;
	};

	Extension.Index.setGetParameter = function(grid, paramName, paramValue)
	{
		var url = grid.source;
		if (url.indexOf(paramName + "=") >= 0)
		{
			var prefix = url.substring(0, url.indexOf(paramName));
			var suffix = url.substring(url.indexOf(paramName));
			suffix = suffix.substring(suffix.indexOf("=") + 1);
			suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
			url = prefix + paramName + "=" + paramValue + suffix;
		}
		else
		{
			if (url.indexOf("?") < 0)
				url += "?" + paramName + "=" + paramValue;
			else
				url += "&" + paramName + "=" + paramValue;
		}

		grid.source = url;
	};

	Extension.Index.removeParam = function(grid, key) {
		var sourceURL = grid.source,
				rtn = sourceURL.split("?")[0],
				param,
				params_arr = [],
				queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
		if (queryString !== "") {
			params_arr = queryString.split("&");
			for (var i = params_arr.length - 1; i >= 0; i -= 1) {
				param = params_arr[i].split("=")[0];
				if (param === key) {
					params_arr.splice(i, 1);
				}
			}
			rtn = rtn + "?" + params_arr.join("&");
		}
		grid.source = rtn;
	};

    
    
    Extension.Index.addUserToGroup = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

        if(!isBlocked) {
            var id = $(this).parents('tr').find('input').val();
        }
        else {
            swal("Tabs blocked!", "Please unlock tabs first before!", "warning");
        }
		if (id)
		{
			$.ajax({
				type: 'PUT',
				url: url,
				data: {
					action : 'attach',
					rows   : [id]
				},
				success: function(response)
				{
					gridGroupAdd.refresh();
                    gridGroupDel.refresh();
                   
				}
			});
		}
	};
    
    
    Extension.Index.deleteUserFromGroup = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;
        
        if(!isBlocked) {
            var id = $(this).parents('tr').find('input').val();
        }
        else {
            swal("Tabs blocked!", "Please unlock tabs first before!", "warning");
        }
		if (id)
		{
			$.ajax({
				type: 'PUT',
				url: url,
				data: {
					action : 'detach',
					rows   : [id]
				},
				success: function(response)
				{
					gridGroupAdd.refresh();
                    gridGroupDel.refresh();
				}
			});
		}
        
        
	};
    
    Extension.Index.unlockActions = function(event)
	{
        if (isBlocked)  {
            $('#unlock-groups').hide();
            swal("You can now edit groups!", "", "success");
            $('#groups-to-add').removeClass('hidden');
            $('html, body').animate({
                        scrollTop: $("#please-find-text").offset().top
            }, 1500);
            isBlocked = false;
        }
	};

	// Job done, lets run.
	Extension.Index.init();

})(window, document, jQuery);
