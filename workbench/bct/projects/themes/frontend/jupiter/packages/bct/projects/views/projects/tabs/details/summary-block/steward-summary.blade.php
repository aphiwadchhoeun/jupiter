<div class="panel panel-default">
    <div class="panel-heading">
        <span class="lead">{{ trans("bct/projects::stewards/common.title") }}
            <span id="stewards-counter">({{ $grid_stewards_count }} of {{ $stewards_count }})</span>
        </span>

        <div class="pull-right push-top-5 hidden-print">
            <a href="#" id="refresh-stewards-summary" class="btn btn-primary disabled"
               data-toggle="tooltip" data-original-title="{{{ trans('common.refresh') }}}">
                <i class="fa fa-refresh fa-fw"></i></a>
            @if (!$currentUser->inRole('pp-guest'))

                <a data-id="#stewards" class="btn btn-primary go-to-tab"
               data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
                        class="fa fa-edit fa-fw"></i></a>
            @endif
        </div>
    </div>

    <div class="panel-body">
        {{-- Grid: Table --}}
        <div class="table-responsive">
            <table id="project-stewards-summary" name= class="table table-condensed"
                   data-source="{{ route('bct.projects.projects.gridStewards', [$project->id, 'limit' => 5]) }}"
                   data-grid="project-stewards-summary">
                <thead>
                <tr>
                    <th>{{ trans('bct/projects::users/model.general.name') }}</th>
                    <th>{{ trans('bct/projects::users/model.general.local') }}</th>
                    <th>{{ trans('bct/projects::users/model.general.phone') }}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

            @include('bct/projects::projects.tabs.details.summary-block.grid.project-stewards-summary.results')
            @include('bct/projects::projects.tabs.details.summary-block.grid.project-stewards-summary.pagination')
            @include('bct/projects::projects.tabs.details.summary-block.grid.project-stewards-summary.filters')
            @include('bct/projects::projects.tabs.details.summary-block.grid.project-stewards-summary.no_results')
        </div>
    </div>
</div>