<div class="input-group select-width">
    <span class="input-group-btn">
         <button class="btn btn-default" type="button" disabled>
            {{{ trans('common.status') }}}
        </button>

        <button class="btn btn-default dropdown-toggle" type="button"
                data-toggle="dropdown" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>

        <ul class="dropdown-menu" role="menu" data-filter-reset>
                <li>
                    <a href="#" data-filter="is_union:1">
                       {{ trans('bct/projects::general/common.is_union') }}
                    </a>
                </li>
                <li>
                    <a href="#" data-filter="is_union:0">
                        {{ trans('bct/projects::general/common.is_not_union') }}
                    </a>
                </li>
        </ul>

    </span>
</div>