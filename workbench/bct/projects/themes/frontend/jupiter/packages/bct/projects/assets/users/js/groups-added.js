var ExtensionAdded;

;(function(window, document, $, undefined)
{

	'use strict';

    ExtensionAdded = Object.create(ExtensionBase);
    ExtensionAdded.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'groups-added'
    });

	// Initialize functions
	ExtensionAdded.Index.init = function()
	{
		ExtensionAdded.Index.dataGrid();
		ExtensionAdded.Index.listeners();
	};

	// Add Listeners
	ExtensionAdded.Index.listeners = function()
	{
		Platform.Cache.$body
			//.on('click', '[data-grid-row]', ExtensionAdded.Index.checkRow)
			.on('click', '#data-grid-added [data-grid-row] a', ExtensionAdded.Index.titleClick)
			.on('click', '#data-grid-added [data-grid-checkbox]', ExtensionAdded.Index.checkboxes)
			.on('click', '#btn-remove-groups', ExtensionAdded.Index.bulkActions)
			.on('click', '.remove-from-group', ExtensionAdded.Index.bulkRow)
		;
	};

	// Handle Data Grid checkboxes
	ExtensionAdded.Index.checkboxes = function(event)
	{
		event.stopPropagation();

		var type = $(this).attr('data-grid-checkbox');

		if (type === 'all')
		{
			$('#data-grid-added [data-grid-checkbox]').not(this).prop('checked', this.checked);

			$('#data-grid-added [data-grid-row]').not(this).toggleClass('active', this.checked);
		}

		$(this).parents('[data-grid-row]').toggleClass('active');

		ExtensionAdded.Index.bulkStatus();
	};

	// Handle Data Grid row checking
	ExtensionAdded.Index.checkRow = function()
	{
		$(this).toggleClass('active');

		var checkbox = $(this).find('[data-grid-checkbox]');

		checkbox.prop('checked', ! checkbox.prop('checked'));

		ExtensionAdded.Index.bulkStatus();
	};

	ExtensionAdded.Index.bulkStatus = function()
	{
		var rows = $('#data-grid-added [data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

		var checked = $('#data-grid-added [data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;

		$('[data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);

		if (checked > 0)
		{
			$('[data-grid-bulk-action="delete"]').attr('data-modal', true);
			$('#btn-remove-groups').removeClass('disabled');
		}
		else
		{
			$('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
			$('#btn-remove-groups').addClass('disabled');
		}

		$('#data-grid-added [data-grid-checkbox="all"]')
			.prop('disabled', rows < 1)
			.prop('checked', rows < 1 ? false : rows === checked)
		;
	};

	// Handle Data Grid bulk actions
	ExtensionAdded.Index.bulkActions = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

		var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
		{
			return +event.value;
		});

		if (rows.length > 0)
		{
			$.ajax({
				type: 'PUT',
				url: url,
				data: {
					action : 'detach',
					rows   : rows
				},
				success: function(response)
				{
					ExtensionAdded.Index.Grid.refresh();
					Extension.Index.Grid.refresh();
				}
			});
		}
	};

	ExtensionAdded.Index.bulkRow = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

		var id = $(this).parents('tr').find('input').val();

		if (id)
		{
			$.ajax({
				type: 'PUT',
				url: url,
				data: {
					action : 'detach',
					rows   : [id]
				},
				success: function(response)
				{
					ExtensionAdded.Index.Grid.refresh();
					Extension.Index.Grid.refresh();
				}
			});
		}
	};

	// Ignore row selection on title click
	ExtensionAdded.Index.titleClick = function(event)
	{
		event.stopPropagation();
	};

	// Data Grid initialization
	ExtensionAdded.Index.dataGrid = function()
	{
		var config = {
			scroll: '#data-grid',
			callback: function()
			{
				$('[data-grid-checkbox-all]').prop('checked', false);

				$('[data-action]').prop('disabled', true);

				ExtensionAdded.Index.bulkStatus();
			}
		};

		ExtensionAdded.Index.Grid = $.datagrid('groups-added', '#data-grid-added', '.data-grid_pagination', '#data-grid_applied', config);
	};

	// Job done, lets run.
	ExtensionAdded.Index.init();

})(window, document, jQuery);
