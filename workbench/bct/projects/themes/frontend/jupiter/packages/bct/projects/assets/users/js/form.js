/**
 * Created by Aphiwad on 4/17/2015.
 */
jQuery('document').ready(function ($) {
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    var submit_btn = $('#submit-btn');
    var main_form = $('#content-form');

    $('#individual_group').bootstrapToggle({
        on: 'Yes',
        off: 'No'
    });

    $('#status').bootstrapToggle({
        onstyle: 'success',
        offstyle: 'danger',
        on: 'Active',
        off: 'Disabled'
    });


    $('input[name="phone"]').mask('(999) 999-9999? x999');

    submit_btn.click(function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var isValid = true;
                    $("#content-form input,#content-form select").each(function () {
                        var element = $(this);
                        if (element.val() == "") {
                            isValid = false;
                        }
                    });
                    if (isValid) {
                        var role = $("#role").val();
                        var email = $("#email").val();
                        $.ajax({
                            type: 'POST',
                            url: window.location.origin + "/pprofiles/settings/users/check",
                            data: {
                                email: email,
                                role: role
                            }
                        }).done(function (response) {
                            resolve(response);
                        }).fail(function () {
                            reject("Error!");
                        });
                    }
                    else {
                        main_form.submit();
                    }
                });
            }
        }).then(function (response) {
            if (response.status != "ok") {
                var text = "User already exist! \r\n";
                text += "User: " + response.user.first_name + " " + response.user.last_name + "\r\n";
                text += "Roles: " + response.roles + "\r\n";
                text += "Do you want to change role?";

                swal({
                    title: text,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#019964",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve, reject) {
                            var role = $("#role").val();
                            var email = $("#email").val();
                            $.ajax({
                                type: 'POST',
                                url: window.location.origin + "/pprofiles/settings/users/addrole",
                                data: {
                                    email: email,
                                    role: role
                                }
                            }).done(function () {
                                resolve();
                            }).fail(function () {
                                reject("Error!");
                            });
                        });

                    }
                }).then(function () {
                    window.location.href = window.location.origin + "/pprofiles/settings/users/";
                });

            }
            else {
                main_form.submit();
            }

        });
    });
});