var ProjectMemberExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectMemberExtension = Object.create(ExtensionBase);
    ProjectMemberExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-member'
    });

    // Initialize functions
    ProjectMemberExtension.Index.init = function () {
        ProjectMemberExtension.Index.dataGrid();
        ProjectMemberExtension.Index.listeners();
    };

    // Add Listeners
    ProjectMemberExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', ProjectMemberExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    ProjectMemberExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    ProjectMemberExtension.Index.exporterStatus = function (grid) {
        $('#project-member-exporter').toggleClass('hide', grid.pagination.filtered == 0);
        $('#project-member-exporter').parent().toggleClass('hide', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    ProjectMemberExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            events: {
                sorted: function(response) {
                    if (response.pagination.total== 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function(response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container' ).removeClass('hide');
                    }
                },
            },
            callback: function (data) {
                ProjectMemberExtension.Index
                    .exporterStatus(data);
            }
        };

        ProjectMemberExtension.Index.Grid = $.datagrid('project-member', '#project-member-data-grid', '.project-member-data-grid_pagination', '#project-member-data-grid_applied', config);
    };

    // Job done, lets run.
    ProjectMemberExtension.Index.init();

})(window, document, jQuery);

// Project Member Data Grid
$('#project-member-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-gear');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-gear');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            $modal.find('select[name="scope_id"]').selectize();
            $modal.find('input[name="number_hours"]').inputmask("integer", { rightAlign: false });
            $modal.find('input[name="wage"]').inputmask("currency", { rightAlign: false });

            var submit_btn_modal = $('#submit-btn-modal');
            var delete_btn_modal = $('#delete-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectMemberExtension.Index.Grid.refresh();
                                    membersSummaryUpdate();
                                } else {
                                    swal(data);
                                }
                            })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });

                if (delete_btn_modal) {
                    delete_btn_modal.on('click', function () {
                            delete_btn_modal.prop('disabled', true);
                            var url = $(this).data('url');
                            $.post(url, {})
                                .done(function (data) {
                                    if (data == 'success') {
                                        $modal.modal('hide');
                                        ProjectMemberExtension.Index.Grid.refresh();
                                        membersSummaryUpdate();
                                    } else {
                                        swal('Deleting failed.');
                                    }
                                })
                                .always(function () {
                                    delete_btn_modal.removeAttr('disabled');
                                });
                        }
                    )
                    ;
                }
            }

        }
        else {
            location.reload();
        }
    });
})
;