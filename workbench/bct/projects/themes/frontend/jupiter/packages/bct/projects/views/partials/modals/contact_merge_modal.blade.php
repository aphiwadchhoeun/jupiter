<div id="merge-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <form action="{{ route('bct.projects.contacts.merge') }}" id="merge-form" role="form" method="post">

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{{ trans('bct/projects::contacts/common.merge_title') }}}</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        {{--<div class="row">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-default btn-replace" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                                    <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>--}}

                        {{--<div class="row">
                            <div class="col-md-6 text-center">
                                <h4>This contact will be merged</h4>
                            </div>
                            <div class="col-md-6 text-center">
                                <h4>This contact will be deleted</h4>
                            </div>
                        </div>--}}

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h4>{{ trans('bct/projects::contacts/common.merge_text') }}</h4>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6 border-right contact-one">

                                <div class="row">

                                    <table class="table table-condensed">

                                        <input type="hidden" name="contact-one-id" id="contact-one-id" value="">

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-full_name" id="contact-one-keep-full_name" value="">
                                                    <label for="contact-one-keep-full_name"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.name') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-full_name"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-location" id="contact-one-keep-location" value="">
                                                    <label for="contact-one-keep-location"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.location') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-location"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-company" id="contact-one-keep-company" value="">
                                                    <label for="contact-one-keep-company"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.company') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-company"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-title" id="contact-one-keep-title" value="">
                                                    <label for="contact-one-keep-title"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.title') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-title"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-phone" id="contact-one-keep-phone" value="">
                                                    <label for="contact-one-keep-phone"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.phone') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-phone"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-phone2" id="contact-one-keep-phone2" value="">
                                                    <label for="contact-one-keep-phone2"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.phone2') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-phone2"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-fax" id="contact-one-keep-fax" value="">
                                                    <label for="contact-one-keep-fax"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.fax') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-fax"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-email" id="contact-one-keep-email" value="">
                                                    <label for="contact-one-keep-email"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.email') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-email"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-notes" id="contact-one-keep-notes" value="">
                                                    <label for="contact-one-keep-notes"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.notes') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-one-notes"></div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </div>

                            <div class="col-md-6 contact-two">

                                <div class="row">

                                    <table class="table table-condensed">

                                        <input type="hidden" name="contact-two-id" id="contact-two-id" value="">

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-full_name" id="contact-two-keep-full_name" value="">
                                                    <label for="contact-two-keep-full_name"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.name') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-full_name"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-location" id="contact-two-keep-location" value="">
                                                    <label for="contact-two-keep-location"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.location') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-location"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-company" id="contact-two-keep-company" value="">
                                                    <label for="contact-two-keep-company"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.company') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-company"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-title" id="contact-two-keep-title" value="">
                                                    <label for="contact-two-keep-title"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.title') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-title"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-phone" id="contact-two-keep-phone" value="">
                                                    <label for="contact-two-keep-phone"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.phone') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-phone"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-phone2" id="contact-two-keep-phone2" value="">
                                                    <label for="contact-two-keep-phone2"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.phone2') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-phone2"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-fax" id="contact-two-keep-fax" value="">
                                                    <label for="contact-two-keep-fax"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.fax') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-fax"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-email" id="contact-two-keep-email" value="">
                                                    <label for="contact-two-keep-email"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.email') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-email"></div>
                                            </td>
                                        </tr>

                                        <tr style="height: 60px;">
                                            <td class="col-md-2">
                                                <div class="checkbox checkbox-success">
                                                    <input type="radio" name="contact-keep-notes" id="contact-two-keep-notes" value="">
                                                    <label for="contact-two-keep-notes"></label>
                                                </div>
                                            </td>
                                            <td class="col-md-3">
                                                <strong>{{ trans('bct/projects::contacts/model.general.notes') }}</strong>
                                            </td>
                                            <td class="col-md-7">
                                                <div id="contact-two-notes"></div>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger"
                            data-dismiss="modal">{{{ trans('bct/projects::general/common.cancel') }}}
                    </button>
                    <button type="submit" id="merge-submit-btn"
                            class="btn btn-primary">{{{ trans('bct/projects::general/common.merge') }}}
                    </button>
                </div>
            </div>
        </div>

    </form>
</div>