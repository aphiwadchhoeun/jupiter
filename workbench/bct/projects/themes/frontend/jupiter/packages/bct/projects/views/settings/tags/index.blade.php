@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::tags/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('index', 'bct/projects::settings/tags/js/index.js','extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')
{{-- Grid --}}
<section class="panel panel-default panel-grid">

{{-- Grid: Header --}}
<header class="panel-heading">
<nav class="navbar navbar-default navbar-actions">

    <div class="container-fluid">
        <span class="navbar-brand">{{{ trans('bct/projects::tags/common.title') }}}</span>
        {{-- Grid: Actions --}}
        <div class="collapse navbar-collapse" id="actions">

            <ul class="nav navbar-nav navbar-left download-container">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                       aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                        <i class="fa fa-download"></i> <span
                                class="visible-xs-inline">{{{ trans('action.export') }}}</span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> {{ trans('bct/projects::general/common.buttons.csv') }}</a></li>
                        <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> {{ trans('bct/projects::general/common.buttons.pdf') }}</a></li>
                    </ul>
                </li>

            </ul>

            {{-- Grid: Filters --}}
            <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                  data-grid="tags" role="form">

                <div class="input-group">

                    <span class="input-group hide">
                        <select name="column" class="form-control">
                            <option selected value="name">{{ trans('bct/projects::general/form.name') }}</option>
                            <option selected value="group">{{trans('bct/projects::tags/common.group_description')}}</option>
                        </select>
                    </span>

                    <span class="input-group-btn">
                        <input class="form-control" name="filter" type="text"
                               placeholder="{{{ trans('common.search') }}}">
                        <button class="btn btn-default" type="submit">
                            <span class="fa fa-search"></span>
                        </button>

                        <button class="btn btn-default" data-grid="tags" data-reset>
                            <i class="fa fa-refresh fa-sm"></i>
                        </button>

                    </span>

                </div>

            </form>

            <div class="pull-right">
                <a href="{{ route('bct.projects.tags.create') }}"
                   class="btn btn-success pull-right"
                   id="tags-new"
                   name="tags-new"
                   data-toggle="tooltip"
                   data-original-title="{{ trans('action.create') }}">
                    <i class="fa fa-plus fa-fw"></i>
                </a>
            </div>

        </div>

    </div>

</nav>
</header>


<div class="panel-body">

    {{-- Grid: Applied Filters --}}
    <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

        <div id="data-grid_applied" class="btn-group" data-grid="tags"></div>

    </div>

    {{-- Grid: Table --}}
    <div class="table-responsive">

        <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.tags.grid') }}"
               data-filename="{{ trans('bct/projects::tags/common.title') }}"
               data-grid="tags">
            <thead>
            <tr>
                <th class="sortable"
                    data-sort="name">{{ trans('bct/projects::general/form.name') }}</th>
                <th class="sortable"
                    data-sort="group">{{ trans('bct/projects::tags/common.group_description') }}</th>
                <th class="col-md-1">{{ trans('bct/projects::general/common.details') }}</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>

    </div>

</div>

<footer class="panel-footer clearfix">

    {{-- Grid: Pagination --}}
    <div class="data-grid_pagination" data-grid="tags"></div>

</footer>

{{-- Grid: templates --}}
@include('bct/projects::settings/tags/grid/results')
@include('bct/projects::settings/tags/grid/pagination')
@include('bct/projects::settings/tags/grid/filters')
@include('bct/projects::settings/tags/grid/no_results')
    </section>
@stop