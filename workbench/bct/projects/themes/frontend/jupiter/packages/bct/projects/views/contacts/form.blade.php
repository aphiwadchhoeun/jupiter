@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans("action.{$mode}") }}} {{ trans('bct/projects::contacts/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}
{{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
{{ Asset::queue('contact-form', 'bct/projects::contacts/js/form.js', 'jquery') }}

{{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}


{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default">

        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-refresh="contacts">

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip"
                                       @if ($mode === 'create')
                                            href="{{ route('bct.projects.contacts.all') }}"
                                       @else
                                            href="{{ route('bct.projects.contacts.details', $contact->id) }}"
                                       @endif

                                       data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{ trans("bct/projects::contacts/common.title") }} {{ trans("action.{$mode}") }}</span>
                        </div>

                        <div class="pull-right">

                            <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                    data-original-title="{{{ trans('action.save') }}}">
                                <i class="fa fa-save fa-fw"></i></button>
                        </div>

                    </div>

                </nav>

            </header>

            @include('bct/projects::contacts._form')

        </form>

    </section>
@stop
