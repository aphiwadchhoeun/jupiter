/**
 * Created by roman on 17.02.16.
 */
/**
 * Created by Aphiwad on 4/17/2015.
 */
jQuery('document').ready(function ($) {
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    var submit_btn = $('#submit-btn');
    var main_form = $('#content-form');


    $('input[name="phone"]').mask('(999) 999-9999? x999');

    submit_btn.click(function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            main_form.submit();
        });
    });
});