@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent
    <script type="text/javascript"
            src="{{ URL::asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyBvReYkUqZuyu4HLSHepBRsGax30FnaWnI&libraries=places&language=en&callback=loadMap') }}"></script>

    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
    {{ Asset::queue('bloodhound', 'bower_components/typeahead.js/dist/bloodhound.min.js', 'jquery') }}
    {{ Asset::queue('typeahead', 'bower_components/typeahead.js/dist/typeahead.jquery.js', 'jquery') }}

    {{ Asset::queue('index', 'bct/projects::map/js/index.js', ['extension-base']) }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
    {{ Asset::queue('search-css', 'bct/projects::projects/css/search.css') }}
    {{ Asset::queue('typeahead', 'bower_components/typeahead.js-bootstrap-css/typeaheadjs.css') }}
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section  class="panel panel-default projects-search-section">

        <div class="panel-body">

            <div class="col-md-12">

                {{-- Form: General --}}
                <fieldset>
                    <div class="row  mrg-btm-10">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>

                                <input type="text" id="searchmap" value="" class="form-control"
                                       placeholder="{{ trans('bct/projects::projects/model.general.map') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-btn" >
                                    <button class="btn btn-default" type="button" disabled>
                                        {{{ trans('common.status') }}}
                                    </button>
                                </span>
                                <span class="input-group-btn" style="width: 81px">
                                    <select id="project_status" name="status" class="form-control">

                                        <option value="Active">Active
                                        </option>
                                        <option value="Completed">Completed
                                        </option>

                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-3">

                            <div class="input-group">
                                <span class="input-group-btn" >
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::map/common.radius') }}
                                    </button>
                                </span>

                                <span class="input-group-btn" style="width: 81px">
                                    <select class="form-control"
                                            id="map_radius"
                                            data-url="{{ route('bct.projects.configuration.change') }}">
                                        <option value="0.03" @if($radius === 0.03) selected @endif>0.03</option>
                                        <option value="0.12" @if($radius === 0.12) selected @endif>0.12</option>
                                        <option value="0.25" @if($radius === 0.25) selected @endif>0.25</option>
                                        <option value="0.5" @if($radius === 0.5) selected @endif>0.5</option>
                                        <option value="1" @if($radius === 1) selected @endif>1</option>
                                        <option value="1.5" @if($radius === 1.5) selected @endif>1.5</option>
                                        <option value="2" @if($radius === 2) selected @endif>2</option>
                                        <option value="2.5" @if($radius === 2.5) selected @endif>2.5</option>
                                        <option value="10" @if($radius === 10) selected @endif>10</option>
                                        <option value="30" @if($radius === 30) selected @endif>30</option>
                                        <option value="50" @if($radius === 50) selected @endif>50</option>
                                        <option value="70" @if($radius === 70) selected @endif>70</option>
                                    </select>
                                </span>

                            </div>

                        </div>

                    </div>
                    <div class="row mrg-btm-10">
                        <div class="col-md-3 ">

                            <div class="input-group">
                                <span class="input-group-btn" >
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::map/common.projects_number') }}
                                    </button>
                                </span>

                                <span class="input-group-btn" style="width: 81px">
                                   <select class="form-control"
                                           id="map_count_projects"
                                           data-url="{{ route('bct.projects.configuration.change') }}">
                                       <option value="50" @if($count_projects === '50') selected @endif>50</option>
                                       <option value="100" @if($count_projects === '100') selected @endif>100</option>
                                       <option value="150" @if($count_projects === '150') selected @endif>150</option>
                                       <option value="200" @if($count_projects === '200') selected @endif>200</option>
                                       <option value="250" @if($count_projects === '250') selected @endif>250</option>
                                   </select>
                                </span>

                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-btn" >
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::projects/model.general.stage') }}
                                    </button>
                                </span>
                                <span class="input-group-btn"  style="width: 81px">
                                      <select class="form-control"
                                              id="project_stage">
                                                <option value="null" selected>-None-</option>
                                          @foreach($stages as $stage)
                                              <option value="{{$stage->name}}">{{$stage->name}}</option>
                                          @endforeach
                                      </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-btn" >
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}
                                    </button>
                                    <button id="follow_up_date" class="btn btn-default btn-h43" type="button" data-map-calendar>
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                    <button id="follow_up_clear" class="btn btn-default btn-h43" type="button">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </span>
                                <span class="input-group-btn hide">
                                    <button id="follow_up_display" class="btn btn-default" type="button" disabled>

                                    </button>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-btn" >
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::projects/model.general.region') }}
                                    </button>
                                </span>
                                <span class="input-group-btn"  style="width: 81px">
                                      <select class="form-control"
                                              id="project_region">
                                          <option value="null" selected>-None-</option>
                                      </select>
                                </span>
                            </div>
                        </div>

                    </div>

                    <div class="row mrg-btm-10">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::contractors/model.general.contractor') }}
                                    </button>
                                </span>
                                <input id="project_contractor" class="form-control" type="text" data-url="{{route('bct.projects.contractors.list')}}"/>
                                <span class="input-group-btn">
                                    <button id="project_contractor_clear" class="btn btn-default" type="button">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-btn" >
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::general/common.tags') }}
                                    </button>
                                </span>
                                <input class="form-control" id="project_tags" type="text"/>
                                <span class="input-group-btn" >
                                    <button id="project_tags_submit" class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>
                                    <button id="tags_clear" class="btn btn-default" type="button">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </span>

                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group">
                            <input type="hidden" id="lat" value="">
                            <input type="hidden" id="lng" value="">
                            <input type="hidden" id="radius" value="0.5">

                            <div id="map-canvas"></div>
                            <div id="map-loader-animation" class="loader-anim" style="display: none;">
                                <span class="loader-anim-spinner"><i class="fa fa-refresh fa-spin"></i></span>
                            </div>
                        </div>

                    </div>

                </fieldset>

            </div>

        </div>

    </section>

    <section class="panel panel-default projects-search-section">
        {{-- Grid: Header --}}
        <header class="panel-heading">

            {{--<span class="lead text-bold">{{ trans('bct/projects::projects/message.create_instruction_step2') }}</span>--}}

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::projects/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{ trans('action.export') }}">
                                    <i class="fa fa-download"></i> <span
                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="projects" role="form">

                            <div class="input-group">

							<span class="input-group-btn">

								<button class="btn btn-default" type="button" disabled>
                                    {{{ trans('common.filters') }}}
                                </button>

							</span>

                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="name">{{ trans('bct/projects::projects/model.general.name') }}
                                        </option>
                                        <option value="city">{{ trans('bct/projects::projects/model.general.city') }}
                                        </option>
                                        <option value="region_abrv">{{ trans('bct/projects::projects/model.general.region') }}
                                        </option>
                                        <option value="tags">{{ trans('bct/projects::general/common.tags') }}
                                        </option>
                                        <option value="id">{{ trans('bct/projects::general/common.id') }}
                                        </option>
                                        <option value="contractors">{{ trans('bct/projects::contractors/model.general.contractor') }}
                                        </option>
                                        <option value="stages">{{ trans('bct/projects::visits/model.general.stage') }}
                                        </option>
                                    </select>
                                </span>

                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">

								<button class="btn btn-default" type="submit">
                                    <span class="fa fa-search"></span>
                                </button>

								<button class="btn btn-default" data-grid="projects" data-reset>
                                    <i class="fa fa-refresh fa-sm"></i>
                                </button>

							</span>

                            </div>

                        </form>
                        @if (!$currentUser->inRole('pp-guest'))

                        <div class="pull-right">
                            <a href="{{ route('bct.projects.projects.all')."?action=storeLists" }}"
                               data-href="{{ route('bct.projects.projects.all')."?action=storeLists" }}"
                               id="lists-modal"
                               style="margin-top:4px;margin-left:10px;"
                               class="btn btn-warning disabled"
                               data-toggle="tooltip"
                               data-original-title="{{{ trans('bct/projects::projects/common.add_to_list') }}}"
                               data-modal>
                                <i class="fa fa-list fa-fw"></i>
                            </a>

                            <a href="{{ route('bct.projects.projects.create') }}" id="create-new-project"
                               class="btn btn-success btn-h43" data-toggle="tooltip" disabled
                               style="margin-top:4px;margin-left:10px;"
                               data-lat=""
                               data-lng=""
                               data-original-title="{{{ trans('bct/projects::projects/common.not_found') }}}">
                                <i class="fa fa-plus fa-fw"></i></a>
                        </div>

                        <div class="text-right">
                            <button id="map-submit-btn" class="btn btn-success btn-h43"
                                    style="margin-top:4px" data-toggle="tooltip"
                                    data-submit-href="{{ route('bct.projects.routes.submit.route') }}"
                                    data-original-title="{{{ trans('bct/projects::lists/common.map_selected') }}}">
                                <i class="fa fa-map-marker fa-fw"></i>{{ trans('bct/projects::lists/common.map_selected') }}
                            </button>
                        </div>
                        @endif


                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="projects"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover"
                       data-source="{{ route('bct.projects.projects.newproject') }}"
                       data-filename="Projects from Map Search"
                       data-grid="projects">
                    <thead>
                    <tr>
                        @if (!$currentUser->inRole('pp-guest'))
                        <th></th>
                        @endif
                        <th class="sortable" data-sort="id">{{ trans('bct/projects::general/common.id') }}</th>
                        <th class="sortable" data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}</th>
                        <th class="sortable" data-sort="city">{{ trans('bct/projects::projects/model.general.city') }}</th>
                        <th class="sortable" data-sort="region_abrv">{{ trans('bct/projects::projects/model.general.region') }}</th>
                        <th class="sortable"
                            data-sort="follow_up_date">{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}
                        </th>
                        <th class="sortable"
                            data-sort="tags">{{ trans('bct/projects::general/common.tags') }}
                        </th>

                        <th class="sortable"
                            data-sort="status">{{ trans('bct/projects::projects/model.general.status_id') }}
                        </th>
                        <th>{{ trans('bct/projects::general/common.details') }}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div id="data-grid_pagination" data-grid="projects"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::projects.grid.map.results')
        @include('bct/projects::projects.grid.map.pagination')
        @include('bct/projects::projects.grid.map.filters')
        @include('bct/projects::projects.grid.map.no_results')

    </section>

@stop
