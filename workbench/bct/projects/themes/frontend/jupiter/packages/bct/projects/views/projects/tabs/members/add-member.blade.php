<div class="modal-dialog ext-profiles">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">{{{ trans('bct/projects::projects/common.member_' . $mode) }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="
                    @if(isset($pivot))
                        {{ route('bct.projects.projects.member.edit', [$project->id, $member->id, $pivot->id]) }}
                    @else
                        {{ route('bct.projects.projects.member.add', [$project->id, $member->id]) }}
                    @endif" data-refresh="members">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input type="hidden" name="mode" value="{{ $mode }}">
                <input type="hidden" name="member_id" value="{{ $member->id }}">

            @if(isset($pivot))
                    <input type="hidden" name="pivot_id" value="{{ $pivot->id }}">
                @endif

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>
                            <div class="row">
                                <table class="table table-bordered table-details">
                                    <tbody>
                                    <tr>
                                        <td class="td-value">
                                            <div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <b>{{ trans('bct/projects::members/model.general.name') }}:</b>
                                                        <span class="pre-spaces">{{ $member->full_name }}</span>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <b>{{ trans('bct/projects::general/common.updated') }}:</b>
                                                        <span class="pre-spaces">{{ date_correct_details_projects($member->updated_at) }}</span>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <b>{{ trans('bct/projects::members/model.general.phone') }}:</b>
                                                        <span class="pre-spaces"><a class="under-none" href="tel:{{ $member->phone }}">{{ $member->phone }}</a></span>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <b>{{ trans('bct/projects::members/model.general.email') }}:</b>
                                                        <span class="pre-spaces"><a class="under-none" href="mailto:{{ $member->email }}">{{ $member->email }}</a></span>
                                                    </div>

                                                </div>

                                            </div>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="td-value">
                                            <div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-group">

                                                            <label for="scope_id" class="control-label">
                                                                {{{ trans('bct/projects::projects/model.general.scope_id') }}}
                                                            </label>

                                                            <select class="form-control" id="scope_id" name="scope_id" required>
                                                                @foreach($scopes as $scope)
                                                                    <option value="{{ $scope->id }}" @if(isset($pivot) && $pivot->z_scopes_id === $scope->id) selected @endif
                                                                    >{{ $scope->name }} ({{ $scope->value }})</option>
                                                                @endforeach
                                                            </select>

                                                            <span class="help-block">{{{ Alert::onForm('scope_id') }}}</span>

                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-group">

                                                            <label for="number_hours" class="control-label">
                                                                {{{ trans('bct/projects::members/model.general.number_hours') }}}
                                                            </label>

                                                            <input type="text" class="form-control" name="number_hours"
                                                                   data-parsley-required="true"
                                                                   required
                                                                   pattern="^(?![.0]*$)\d+(?:\.\d{1,2})?$"
                                                                   value="{{ data_get($pivot, 'number_of_hours', '') }}">

                                                            <span class="help-block">{{{ Alert::onForm('number_hours') }}}</span>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="form-group">

                                                            <label for="wage" class="control-label">
                                                                {{{ trans('bct/projects::members/model.general.wage') }}}
                                                            </label>

                                                            <input type="text" class="form-control" name="wage"
                                                                   data-parsley-required="true"
                                                                   reguired
                                                                   pattern="^\$?\s?(?![.0]*$)\d+(?:\.\d{1,2})?$" value="{{ data_get($pivot, 'wage', '') }}">

                                                            <span class="help-block">{{{ Alert::onForm('wage') }}}</span>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="submit-btn-modal" name="submit-btn-modal" class="btn btn-primary">
                {{{ trans('action.save') }}}</button>
            @if(isset($pivot))
                <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-action="delete"
                        data-url="{{ route('bct.projects.projects.deleteMember', [
                            'id' => $project->id,
                            'pivot_id' => $pivot->id,
                        ]) }}">
                    {{{ trans('action.delete') }}}</button>
            @endif
            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
