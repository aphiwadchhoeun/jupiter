<div class="row">
    <div class="col-md-6">
        <section class="panel panel-default contact-section">
            <div class="panel-body">
                <table class="table table-hover" id="company-list">
                    <thead>
                    <tr>
                        <th class="col-md-5">Company</th>
                        <th class="col-md-3">Type</th>
                        <th class="col-md-3">Modified</th>
                        <th class="col-md-1">

                            <button data-original-title="Create"
                                    data-toggle="tooltip"
                                    class="btn btn-sm btn-success pull-right btn-form-create"
                                    name="company-new"
                                    id="company-new"
                                    data-contractor_id=""
                                    data-url="" type="button">
                                <i class="fa fa-plus fa-fw"></i>
                            </button>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($companies) && $companies->count()>0)
                        @foreach($companies as $item)
                            <tr >
                                <td>{{$item->company}}</td>
                                <td>{{$item->type}}</td>
                                <td>{{date_correct_details_projects($item->updated_at)}}</td>
                                <td class="text-right"><a data-original-title="Details"
                                                          data-placement="right"
                                                          data-toggle="tooltip"
                                                          class="btn btn-primary btn-circle company-update"
                                                          data-company_id="{{$item->id}}"
                                                          data-company="{{$item->company}}"
                                                          data-type="{{$item->type}}"
                                                          href="">
                                        <i class="fa fa-book fa-fw"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td class="no-results" colspan="4">No Results</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </section>

    </div>
    <div class="col-md-6">
        <section class="panel panel-default contact-section">
            <div class="panel-body">
                <table class="table table-hover" id="license-list">
                    <thead>
                    <tr>
                        <th class="col-md-5">License</th>
                        <th class="col-md-3">Type</th>
                        <th class="col-md-3">Modified</th>
                        <th class="col-md-1">
                            <button data-original-title="Create"
                                    data-toggle="tooltip"
                                    class="btn btn-sm btn-success pull-right btn-form-create"
                                    name="license-new"
                                    id="license-new"
                                    data-contractor_id=""
                                    data-url="" type="button">
                                <i class="fa fa-plus fa-fw"></i>
                            </button>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($licenses) && $licenses->count()>0)
                        @foreach($licenses as $item)
                            <tr >
                                <td>{{$item->license}}</td>
                                <td>{{$item->type}}</td>
                                <td>{{date_correct_details_projects($item->updated_at)}}</td>
                                <td class="text-right"><a data-original-title="Details"
                                                          data-placement="right"
                                                          data-toggle="tooltip"
                                                          class="btn btn-primary btn-circle license-update"
                                                          data-license_id="{{$item->id}}"
                                                          data-license="{{$item->license}}"
                                                          data-type="{{$item->type}}"
                                                          href="">
                                        <i class="fa fa-book fa-fw"></i></a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td class="no-results" colspan="4">No Results</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </section>

    </div>

</div>
<section class="panel panel-default ">
    <div class="panel-body">
        <table class="table table-hover" id="address-list">
            <thead>
            <tr>
                <th class="col-md-5">Address</th>
                <th class="col-md-3">Type</th>
                <th class="col-md-3">Modified</th>
                <th class="col-md-1">
                    <button data-original-title="Create"
                            data-toggle="tooltip"
                            class="btn btn-sm btn-success pull-right btn-form-create"
                            name="address-new"
                            id="address-new"
                            data-contractor_id=""
                            data-url="" type="button">
                        <i class="fa fa-plus fa-fw"></i>
                    </button>
                </th>
            </tr>
            </thead>
            <tbody>
            @if(isset($addresses) && $addresses->count()>0)
                @foreach($addresses as $item)
                    <tr >
                        <td>{{$item->getFullAddress()}}</td>
                        <td>{{$item->type}}</td>
                        <td>{{date_correct_details_projects($item->updated_at)}}</td>
                        <td class="text-right">
                            <a data-original-title="Actions"
                               data-placement="right"
                               data-toggle="tooltip"
                               data-address_id="{{$item->id}}"
                               data-address="{{$item->address}}"
                               data-city="{{$item->city}}"
                               data-state="{{$item->state}}"
                               data-zipcode="{{$item->zipcode}}"
                               data-type="{{$item->type}}"
                               class="btn btn-primary btn-circle address-update"
                               href="">
                                <i class="fa fa-book fa-fw"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td class="no-results" colspan="4">No Results</td></tr>
            @endif

            </tbody>
        </table>
    </div>
</section>
<div class="row">
    <div class="col-md-6">
        <section class="panel panel-default ">
            <div class="panel-body">
                <table class="table table-hover" id="phone-list">
                    <thead>
                    <tr>
                        <th class="col-md-5">Phone</th>
                        <th class="col-md-3">Type</th>
                        <th class="col-md-3">Modified</th>
                        <th class="col-md-1">
                            <button data-original-title="Create"
                                    data-toggle="tooltip"
                                    class="btn btn-sm btn-success pull-right btn-form-create"
                                    name="phone-new"
                                    id="phone-new"
                                    data-contractor_id=""
                                    data-url="" type="button">
                                <i class="fa fa-plus fa-fw"></i>
                            </button>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($phones) && $phones->count()>0)
                        @foreach($phones as $item)
                            <tr >
                                <td><a href="tel:{{$item->phone}}">{{$item->phone}}</a></td>
                                <td>{{$item->type}}</td>
                                <td>{{date_correct_details_projects($item->updated_at)}}</td>
                                <td class="text-right"><a data-original-title="Details"
                                                          data-placement="right"
                                                          data-toggle="tooltip"
                                                          data-phone_id="{{$item->id}}"
                                                          data-phone="{{$item->phone}}"
                                                          data-type="{{$item->type}}"
                                                          class="btn btn-primary btn-circle phone-update"
                                                          href="">
                                        <i class="fa fa-book fa-fw"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td class="no-results" colspan="4">No Results</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </section>

    </div>
    <div class="col-md-6">
        <section class="panel panel-default ">
            <div class="panel-body">
                <table class="table table-hover" id="email-list">
                    <thead>
                    <tr>
                        <th class="col-md-5">Email</th>
                        <th class="col-md-3">Type</th>
                        <th class="col-md-3">Modified</th>
                        <th class="col-md-1">
                            <button data-original-title="Create"
                                    data-toggle="tooltip"
                                    class="btn btn-sm btn-success pull-right btn-form-create"
                                    name="email-new"
                                    id="email-new"
                                    data-contractor_id=""
                                    data-url="" type="button">
                                <i class="fa fa-plus fa-fw"></i>
                            </button>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($emails) && $emails->count()>0)
                        @foreach($emails as $item)
                            <tr >
                                <td><a href="mailto:{{$item->email}}">{{$item->email}}</a></td>
                                <td>{{$item->type}}</td>
                                <td>{{date_correct_details_projects($item->updated_at)}}</td>
                                <td class="text-right"><a data-original-title="Details"
                                                          data-placement="right"
                                                          data-toggle="tooltip"
                                                          class="btn btn-primary btn-circle email-update"
                                                          data-email_id="{{$item->id}}"
                                                          data-email="{{$item->email}}"
                                                          data-type="{{$item->type}}"
                                                          href="">
                                        <i class="fa fa-book fa-fw"></i></a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td class="no-results" colspan="4">No Results</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
<section class="panel panel-default ">
    <div class="panel-body">
        <table class="table table-hover" id="notes-list">
            <thead>
            <tr>
                <th class="col-md-5">Notes</th>
                <th class="col-md-3">Type</th>
                <th class="col-md-3">Modified</th>
                <th class="col-md-1">
                    <button data-original-title="Create"
                            data-toggle="tooltip"
                            class="btn btn-sm btn-success pull-right btn-form-create"
                            name="notes-new"
                            id="notes-new"
                            data-contractor_id=""
                            data-url="" type="button">
                        <i class="fa fa-plus fa-fw"></i>
                    </button>
                </th>
            </tr>
            </thead>
            <tbody>
            @if(isset($notes) && $notes->count()>0)
                @foreach($notes as $item)
                    <tr >
                        <td>{{$item->notes}}</td>
                        <td>{{$item->type}}</td>
                        <td>{{date_correct_details_projects($item->updated_at)}}</td>
                        <td class="text-right"><a data-original-title="Details"
                                                  data-placement="right"
                                                  data-toggle="tooltip"
                                                  class="btn btn-primary btn-circle notes-update"
                                                  data-notes_id="{{$item->id}}"
                                                  data-notes="{{$item->notes}}"
                                                  data-type="{{$item->type}}"
                                                  href="">
                                <i class="fa fa-book fa-fw"></i></a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td class="no-results" colspan="4">No Results</td></tr>
            @endif
            </tbody>
        </table>
    </div>
</section>
<div id="email-form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-tite">Contractor Email</h4>
            </div>
            <div class="modal-body">
                <form id="email-form-modal" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="z_contractors_id" value="{{ $contractor->id }}">


                    <div class="panel-body">

                        <div class="col-md-12">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                                            <label for="email" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.email') }}
                                            </label>

                                            <input type="email" class="form-control" name="email" id="email"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.email') }}"
                                                   autocomplete="on"
                                                   required
                                                   value="">

                                            <span class="help-block">{{{ Alert::onForm('email') }}}</span>

                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('type', ' has-error') }}">

                                            <label for="type" class="control-label pull-left">
                                                {{ trans('bct/projects::contractors/model.general.type') }}
                                            </label>
                                            <span class="col-md-12">
                                                 <input type="radio" required name="type" id="primary-type" value="Primary" checked>Primary
                                            </span>
                                            <span class="col-md-8">
                                                <input type="radio" name="type" id="other-type" value="Other">Other:
                                            <input type="text" class="form-control" name="other-type-input" id="other-type-input"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.type') }}"
                                                   autocomplete="on"
                                                   maxlength="10"
                                                   data-model="contact_type"
                                                   value=""/>
                                            </span>


                                            <span class="help-block">{{{ Alert::onForm('type') }}}</span>

                                        </div>
                                    </div>

                                </div>

                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button  name="delete-btn" data-phone_id="" class="btn btn-danger" id="email-delete-btn">
                    <i class="fa fa-trash-o fa-fw"></i>Delete
                </button>

                <button  class="btn btn-primary" id="email-submit-btn">
                    <i class="fa fa-save fa-fw"></i>Save
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
<div id="phone-form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-tite">Contractor Phone</h4>

            </div>
            <div class="modal-body">
                <form id="phone-form-modal" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="z_contractors_id" value="{{ $contractor->id }}">


                    <div class="panel-body">

                        <div class="col-md-12">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('phone', ' has-error') }}">

                                            <label for="phone" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.phone') }}
                                            </label>

                                            <input type="text" class="form-control" name="phone" id="phone"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.phone') }}"
                                                   autocomplete="off"
                                                   required
                                                   value="">

                                            <span class="help-block">{{{ Alert::onForm('phone') }}}</span>

                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('type', ' has-error') }}">

                                            <label for="type" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.type') }}
                                            </label>

                                            <span class="col-md-12">
                                                 <input type="radio" required name="type" id="primary-type" value="Primary" checked>Primary
                                            </span>
                                            <span class="col-md-8">
                                                <input type="radio" name="type" id="other-type" value="Other">Other:
                                            <input type="text" class="form-control" name="other-type-input" id="other-type-input"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.type') }}"
                                                   autocomplete="on"
                                                   maxlength="10"
                                                   data-model="contact_type"
                                                   value=""/>
                                            </span>

                                            <span class="help-block">{{{ Alert::onForm('type') }}}</span>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button  name="delete-btn" class="btn btn-danger" id="phone-delete-btn">
                    <i class="fa fa-trash-o fa-fw"></i>Delete
                </button>

                <button  class="btn btn-primary" id="phone-submit-btn">
                    <i class="fa fa-save fa-fw"></i>Save
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="license-form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-tite">Contractor License</h4>
            </div>
            <div class="modal-body">
                <form id="license-form-modal" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="z_contractors_id" value="{{ $contractor->id }}">


                    <div class="panel-body">

                        <div class="col-md-12">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('license', ' has-error') }}">

                                            <label for="license" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.license') }}
                                            </label>

                                            <input type="license" class="form-control" name="license" id="license"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.license') }}"
                                                   autocomplete="on"
                                                   required
                                                   value="">

                                            <span class="help-block">{{{ Alert::onForm('license') }}}</span>

                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('type', ' has-error') }}">

                                            <label for="type" class="control-label pull-left">
                                                {{ trans('bct/projects::contractors/model.general.type') }}
                                            </label>
                                            <span class="col-md-12">
                                                 <input type="radio" required name="type" id="primary-type" value="Primary" checked>Primary
                                            </span>
                                            <span class="col-md-8">
                                                <input type="radio" name="type" id="other-type" value="Other">Other:
                                            <input type="text" class="form-control" name="other-type-input" id="other-type-input"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.type') }}"
                                                   autocomplete="on"
                                                   maxlength="10"
                                                   data-model="contact_type"
                                                   value=""/>
                                            </span>


                                            <span class="help-block">{{{ Alert::onForm('type') }}}</span>

                                        </div>
                                    </div>

                                </div>

                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button name="delete-btn" data-phone_id="" class="btn btn-danger" id="license-delete-btn">
                    <i class="fa fa-trash-o fa-fw"></i>Delete
                </button>

                <button  class="btn btn-primary" id="license-submit-btn">
                    <i class="fa fa-save fa-fw"></i>Save
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
<div id="company-form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-tite">Contractor Company</h4>
            </div>
            <div class="modal-body">
                <form id="company-form-modal" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="z_contractors_id" value="{{ $contractor->id }}">


                    <div class="panel-body">

                        <div class="col-md-12">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('company', ' has-error') }}">

                                            <label for="company" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.company') }}
                                            </label>

                                            <input type="company" class="form-control" name="company" id="company"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.company') }}"
                                                   autocomplete="on"
                                                   required
                                                   value="">

                                            <span class="help-block">{{{ Alert::onForm('company') }}}</span>

                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('type', ' has-error') }}">

                                            <label for="type" class="control-label pull-left">
                                                {{ trans('bct/projects::contractors/model.general.type') }}
                                            </label>
                                            <span class="col-md-12">
                                                 <input type="radio" required name="type" id="primary-type" value="Primary" checked>Primary
                                            </span>
                                            <span class="col-md-8">
                                                <input type="radio" name="type" id="other-type" value="Other">Other:
                                            <input type="text" class="form-control" name="other-type-input" id="other-type-input"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.type') }}"
                                                   autocomplete="on"
                                                   maxlength="10"
                                                   data-model="contact_type"
                                                   value=""/>
                                            </span>


                                            <span class="help-block">{{{ Alert::onForm('type') }}}</span>

                                        </div>
                                    </div>

                                </div>

                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button name="delete-btn" data-phone_id="" class="btn btn-danger" id="company-delete-btn">
                    <i class="fa fa-trash-o fa-fw"></i>Delete
                </button>

                <button  class="btn btn-primary" id="company-submit-btn">
                    <i class="fa fa-save fa-fw"></i>Save
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
<div id="notes-form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-tite">Contractor Notes</h4>
            </div>
            <div class="modal-body">
                <form id="notes-form-modal" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="z_contractors_id" value="{{ $contractor->id }}">


                    <div class="panel-body">

                        <div class="col-md-12">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('notes', ' has-error') }}">

                                            <label for="notes" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.notes') }}
                                            </label>

                                            <textarea type="notes" class="form-control" name="notes" id="notes"
                                                      maxlength="256"
                                                      placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.notes') }}"
                                                      autocomplete="on"
                                                      required>{{{ input()->old('notes', '') }}}</textarea>

                                            <span class="help-block">{{{ Alert::onForm('notes') }}}</span>

                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('type', ' has-error') }}">

                                            <label for="type" class="control-label pull-left">
                                                {{ trans('bct/projects::contractors/model.general.type') }}
                                            </label>
                                            <span class="col-md-12">
                                                 <input type="radio" required name="type" id="primary-type" value="Primary" checked>Primary
                                            </span>
                                            <span class="col-md-8">
                                                <input type="radio" name="type" id="other-type" value="Other">Other:
                                            <input type="text" class="form-control" name="other-type-input" id="other-type-input"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.type') }}"
                                                   autocomplete="on"
                                                   maxlength="10"
                                                   data-model="contact_type"
                                                   value=""/>
                                            </span>
                                            <span class="help-block">{{{ Alert::onForm('type') }}}</span>

                                        </div>
                                    </div>

                                </div>

                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button name="delete-btn" data-phone_id="" class="btn btn-danger" id="notes-delete-btn">
                    <i class="fa fa-trash-o fa-fw"></i>Delete
                </button>

                <button  class="btn btn-primary" id="notes-submit-btn">
                    <i class="fa fa-save fa-fw"></i>Save
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
<div id="address-form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-tite">Contractor Address</h4>

            </div>
            <div class="modal-body">
                <form id="address-form-modal" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <input type="hidden" name="z_contractors_id" value="{{ $contractor->id }}"/>

                    <div class="panel-body">

                        <div class="col-md-12">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group{{ Alert::onForm('address', ' has-error') }}">

                                            <label for="name" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.address') }}
                                            </label>

                                            <input type="text" class="form-control" name="address" id="address"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.address') }}"
                                                   autocomplete="off"
                                                   required
                                                   value="">

                                            <span class="help-block">{{{ Alert::onForm('address') }}}</span>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ Alert::onForm('city', ' has-error') }}">

                                            <label for="name" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.city') }}
                                            </label>

                                            <input type="text" class="form-control" name="city" id="city"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.city') }}"
                                                   autocomplete="off"
                                                   required
                                                   value="">

                                            <span class="help-block">{{{ Alert::onForm('city') }}}</span>

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ Alert::onForm('state', ' has-error') }}">

                                            <label for="name" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.state') }}
                                            </label>

                                            <select class="form-control" name="state" id="state"
                                                    placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.state') }}"
                                                    required>
                                                @foreach($states as $state)
                                                    <option value="{{$state}}">{{ $state }}</option>
                                                @endforeach
                                            </select>

                                            <span class="help-block">{{{ Alert::onForm('state') }}}</span>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ Alert::onForm('zipcode', ' has-error') }}">

                                            <label for="name" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.zipcode') }}
                                            </label>

                                            <input type="text" class="form-control" name="zipcode" id="zipcode"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.zipcode') }}"
                                                   autocomplete="off"
                                                   required
                                                   value="">

                                            <span class="help-block">{{{ Alert::onForm('zipcode') }}}</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group{{ Alert::onForm('type', ' has-error') }}">

                                            <label for="name" class="control-label">
                                                {{ trans('bct/projects::contractors/model.general.type') }}
                                            </label>

                                            <span class="col-md-12">
                                                 <input type="radio" name="type" required id="primary-type" value="Primary" checked>Primary
                                            </span>
                                            <span class="col-md-8">
                                                <input type="radio" name="type" id="other-type" value="Other">Other:
                                            <input type="text" class="form-control" name="other-type-input" id="other-type-input"
                                                   placeholder="{{ trans('bct/projects::general/common.placeholder.fill_this') }}{{ trans('bct/projects::contractors/model.general.type') }}"
                                                   autocomplete="on"
                                                   maxlength="10"
                                                   data-model="contact_type"
                                                   value=""/>
                                            </span>

                                            <span class="help-block">{{{ Alert::onForm('type') }}}</span>

                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button  name="delete-btn" class="btn btn-danger" id="address-delete-btn">
                    <i class="fa fa-trash-o fa-fw"></i>Delete
                </button>

                <button  class="btn btn-primary" id="address-submit-btn" >
                    <i class="fa fa-save fa-fw"></i>Save
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
