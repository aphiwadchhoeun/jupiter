<header class="panel-heading hidden-print">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip go-to-details" data-toggle="tooltip"
                           data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $project->name }} {{ trans("bct/projects::projects/common.tabs.actions") }}</span>

            </div>

        </div>

    </nav>

</header>

<div class="panel-body">
    <div class="row visible-print">
        <h4 class="overview-heading text-center visible-print">
            {{ data_get($project, 'name', '') }} -
            {{ trans("bct/projects::projects/common.title") }} {{ trans("bct/projects::projects/common.tabs.actions") }}
        </h4>
    </div>

    <div class="row">

        <div class="col-md-12">
            @include('bct/projects::projects.tabs.actions.project-action')
        </div>

    </div>

</div>
