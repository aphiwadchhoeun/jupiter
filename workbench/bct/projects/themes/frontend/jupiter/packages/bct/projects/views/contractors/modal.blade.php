<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">{{{ trans("bct/projects::contractors/common.title") }}} {{{ trans("action.{$mode}") }}}</h4>
</div>
<div class="modal-body">
    {{-- Form --}}
    <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post">

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @include('bct/projects::contractors._form')
    </form>
</div>