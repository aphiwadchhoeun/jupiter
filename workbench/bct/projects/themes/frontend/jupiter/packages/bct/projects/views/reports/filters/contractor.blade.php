<div class="input-group select-width">
    <span class="input-group-btn">
        <button class="btn btn-default" type="button" disabled>
            {{ trans('bct/projects::reports/common.placeholder.contractor') }}
        </button>
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul name="column" class="dropdown-menu contractor" role="menu" data-filter-reset>
            @foreach($contractors as $contractor)
                <li ><a href="#" value="{{$contractor->id}}" data-filter="contractor:{{$contractor->id}}">{{$contractor->name}}</a></li>
            @endforeach
        </ul>
    </span>
</div>