@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
@if ($user->inRole('admin') || $user->inRole('repadmin') || $user->inRole('super-repadmin'))
{{ Asset::queue('index', 'bct/projects::projects/js/index_merge.js', 'extension-base') }}
@else
{{ Asset::queue('index', 'bct/projects::projects/js/index.js', 'extension-base') }}
@endif
{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::projects/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                    <li class="disabled"><a data-download-custom="pdf_details" data-url="{{ route('bct.projects.projects.grid.pdf.details') }}"><i class="fa fa-file-pdf-o"></i> PDF Details</a></li>
                                    @if(config_projects('bct.projects.settings.project_field_report'))
                                        <li class="disabled"><a data-download-custom="field_reports"
                                                                data-url="{{ route('bct.projects.projects.grid.pdf.fieldreports') }}"><i
                                                        class="fa fa-fw fa-file-pdf-o"></i>Field Reports</a></li>
                                    @endif
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="projects" role="form">

                            <div class="input-group">

                                <span class="input-group-btn">

                                    <button class="btn btn-default" type="button" disabled>
                                        {{{ trans('common.status') }}}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                        @foreach(\Bct\Projects\Models\Status::all() as $status)
                                            <li>
                                                <a href="#" data-filter="z_project_status.name:{{$status->name}}">
                                                    <i class="fa @if($status->name == 'Completed') fa-times-circle @else fa-check-circle @endif"></i> {{$status->name}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <button class="btn btn-default" type="button" data-grid-calendar
                                            data-range-filter="follow_up_date">
                                        <i class="fa fa-calendar"></i>
                                    </button>

                                </span>

                                <span class="input-group-btn">

                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::projects/common.stage') }}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul id="stage-dropdown" class="dropdown-menu" role="menu" data-filter-reset>
                                        @foreach($stages as $stage)
                                            <li>
                                                <a href="#" data-filter="z_project_stage.name:{{$stage->name}}">
                                                    <i class="fa fa-check-circle "></i> {{$stage->name}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </span>


                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="z_projects.name">{{ trans('bct/projects::projects/model.general.name') }}
                                        </option>
                                        <option value="z_projects.city">{{ trans('bct/projects::projects/model.general.city') }}
                                        </option>
                                        <option value="z_projects.region_abrv">{{ trans('bct/projects::projects/model.general.region') }}
                                        </option>
                                        <option value="tags.name">{{ trans('bct/projects::general/common.tags') }}
                                        </option>
                                        <option value="z_projects.id">{{ trans('bct/projects::general/common.id') }}
                                        </option>
                                    </select>
                                </span>

                                <input class="form-control" name="filter" type="text" placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="projects" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>

                            <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-start></button>
                            <input type="hidden" data-range-filter="follow_up_date" data-format="YYYY-MM-DD" data-range-end></button>


                        </form>

                        <div class="pull-right">

                            @if ($user->inRole('admin') || $user->inRole('repadmin') || $user->inRole('super-repadmin'))
                                <a id="merge-btn" data-toggle="tooltip"
                                   data-original-title="{{ trans('bct/projects::general/common.merge') }}"
                                   class="btn btn-primary disabled">
                                    <i class="fa fa-compress"></i>
                                </a>
                            @endif
                            @if (!$currentUser->inRole('pp-guest'))

                                <a href="{{ route('bct.projects.projects.all')."?action=storeLists" }}"
                               data-href="{{ route('bct.projects.projects.all')."?action=storeLists" }}"
                               id="lists-modal"
                               class="btn btn-warning"
                               data-toggle="tooltip"
                               data-original-title="{{{ trans('bct/projects::projects/common.add_to_list') }}}"
                               data-modal>
                                <i class="fa fa-list fa-fw"></i>
                                </a>

                                <a href="{{ route('bct.projects.projects.search') }}"
                                    id="project_new"
                                    name="project_new"
                                   class="btn btn-success"
                                   data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
                                    <i class="fa fa-plus fa-fw"></i>
                                </a>
                            @endif

                        </div>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="projects"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.projects.grid', 'filters[0][z_project_status.name]=Active') }}"
                       data-source-default="{{ route('bct.projects.projects.grid') }}"
                       data-filename="{{ trans('bct/projects::projects/common.title') }}"
                       data-grid="projects">
                    <thead>
                    <tr>
                        <th class="projects-checkbox-head">
                            <input data-grid-checkbox="all" type="checkbox"
                                   data-all-count="{{ $projects_all_ids->count() }}">
                        </th>
                        <th class="sortable" data-sort="id">{{ trans('bct/projects::general/common.id') }}</th>
                        <th class="sortable" data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}</th>
                        <th class="sortable" data-sort="city">{{ trans('bct/projects::projects/model.general.city') }}</th>
                        <th class="sortable" data-sort="region_abrv">{{ trans('bct/projects::projects/model.general.region') }}</th>
                        <th class="sortable"
                            data-sort="follow_up_date">{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}
                        </th>
                        <th class="sortable"
                            data-sort="tags">{{ trans('bct/projects::general/common.tags') }}
                        </th>

                        <th class="sortable"
                            data-sort="status">{{ trans('bct/projects::projects/model.general.status_id') }}
                        </th>
                        <th class="col-md-1">{{ trans('bct/projects::general/common.details') }}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="projects"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::projects.grid.index.results')
        @include('bct/projects::projects.grid.index.pagination')
        @include('bct/projects::projects.grid.index.filters')
        @include('bct/projects::projects.grid.index.no_results')

    </section>

@stop
