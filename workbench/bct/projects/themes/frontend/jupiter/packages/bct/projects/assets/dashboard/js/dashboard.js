/**
 * Created by Aphiwad on 4/24/2015.
 */
jQuery('document').ready(function ($) {
    var token = $("input[name=_token]").val();
    var region = $("[name=select_area]");
    var regionNonUnion = $("[name=select_area_non_union]");
    var regionUnion = $("[name=select_area_union]");
    var regionFollowUp = $("[name=select_area_follow_up]");
    var regionRepVisits = $("[name=select_area_rep_visits]");
    var regionArray = [region, regionNonUnion, regionUnion, regionFollowUp, regionRepVisits];
    var cartodb_api = null, cartodb_table = null;


    function fetchCartodbConfigs(callback) {
        if (cartodb_api == null || cartodb_table == null) {
            $.get('/ui/api/cartodb/configs/get')
                .done(function (data) {
                    cartodb_api = data.api_url;
                    cartodb_table = data.tablename;
                    callback();
                });
        } else {
            callback();
        }
    }


    fetchCartodbConfigs(function () {
        /*
         Fetching region select boxes for global use
         */
        var regionService = cartodb_api + '/sql?q=select distinct areaabrv, areaname from ' + cartodb_table + ' order by areaabrv asc';

        // make a call to cartodb service and retrieve regions data, then populate them into select boxes
        $.get(regionService, function (data) {
            if (data.rows.length > 0) {

                $.each(data.rows, function (i, row) {
                    $.each(regionArray, function (j, item) {
                        item.append($('<option>', {
                            value: row.areaabrv,
                            text: row.areaabrv
                        }));
                    });
                });
            }
        });
    });

    /*
     ------------------------------------------------------
     */


    /*
     Market Share by Region
     */
    var scope = $("[name=select_scope]");
    // init pie chart
    var shareByAreaPie = c3.generate({
        bindto: '#market-share-by-area',
        size: {
            height: 400
        },
        data: {
            columns: [],
            type: 'pie'
        }
    });

    // query market share data and render pie chart
    function getMarketShareData() {
        disableStateMarketShareSelectBoxes(true);

        $.get("/pprofiles/dashboard/share-by-area", {
            token: token,
            area: region.val(),
            scope: scope.val()
        })
            .done(function (data) {
                var cols = [];

                for (var index in data) {
                    cols.push([data[index].status, data[index].count]);
                }

                shareByAreaPie.load({
                    columns: cols
                });
            })
            .always(function () {
                disableStateMarketShareSelectBoxes(false);
            });
    }

    function disableStateMarketShareSelectBoxes(disabled) {
        scope.prop('disabled', disabled);
        region.prop('disabled', disabled);
    }

    scope.on('change', function () {
        getMarketShareData();
    });
    region.on('change', function () {
        getMarketShareData();
    });

    // bootstrap Market Share by Region
    getMarketShareData();
    /*
     ------------------------------------------------------
     */


    /*
     Project Totals
     */
    var years = $("[name=select_year]");
    // init bar chart
    var totalsBar = c3.generate({
        bindto: '#totals',
        size: {
            height: 400
        },
        data: {
            columns: [],
            type: 'bar'
        },
        tooltip: {
            format: {
                title: function (x) {
                    return 'Details';
                }
            },
            grouped: false
        }
    });

    // query project totals data and render bar chart
    function getProjectTotalsData() {
        disableProjectTotalsSelectBoxes(true);

        $.get("/pprofiles/dashboard/totals", {
            token: token,
            year: years.val()
        })
            .done(function (data) {
                var cols = [];

                for (var index in data) {
                    cols.push([data[index].status, data[index].count]);
                }

                totalsBar.load({
                    columns: cols
                });
            })
            .always(function () {
                disableProjectTotalsSelectBoxes(false);
            });
    }

    function disableProjectTotalsSelectBoxes(disabled) {
        years.prop('disabled', disabled);
    }

    years.on('change', function () {
        getProjectTotalsData();
    });

    // bootstrap
    getProjectTotalsData();
    /*
     ------------------------------------------------------
     */


    /*
     Top Non-Union Contractors table
     */
    var top_non_union_contractors = $("#top-non-union-contractors");
    var scopeNonUnion = $("[name=select_scope_non_union]");

    // query top non union contractors and render the table
    function getNonUnionContractors() {
        disableTopNonUnionContractorSelectBoxes(true);

        $.get("/pprofiles/dashboard/top-contractors", {
            token: token,
            area: regionNonUnion.val(),
            scope: scopeNonUnion.val(),
            isUnion: 0
        })
            .done(function (data) {
                var contractors = '';
                for (var index in data) {
                    contractors += '<tr>' + '<td><a href="/pprofiles/contractors/details/' + data[index].id + '">' +
                        data[index].name + '</a></td>' + '<td>' + data[index].value + '</td>' + '</tr>';
                }
                top_non_union_contractors.hide().html(contractors).show('fast');
            })
            .always(function () {
                disableTopNonUnionContractorSelectBoxes(false);
            });
    }

    function disableTopNonUnionContractorSelectBoxes(disabled) {
        regionNonUnion.prop('disabled', disabled);
        scopeNonUnion.prop('disabled', disabled);
    }

    regionNonUnion.on('change', function () {
        getNonUnionContractors();
    });
    scopeNonUnion.on('change', function () {
        getNonUnionContractors();
    });

    // bootstrap
    getNonUnionContractors();
    /*
     ------------------------------------------------------
     */


    /*
     Top Union Contractors
     */
    var top_union_contractors = $("#top-union-contractors");
    var scopeUnion = $("[name=select_scope_union]");

    // query top union contractors and render the table
    function getUnionContractors() {
        disableTopUnionContractorSelectBoxes(true);

        $.get("/pprofiles/dashboard/top-contractors", {
            token: token,
            area: regionUnion.val(),
            scope: scopeUnion.val(),
            isUnion: 1
        })
            .done(function (data) {
                var contractors = '';
                for (var index in data) {
                    contractors += '<tr>' + '<td><a href="/pprofiles/contractors/details/' + data[index].id + '">' +
                        data[index].name + '</a></td>' + '<td>' + data[index].value + '</td>' + '</tr>';
                }
                top_union_contractors.hide().html(contractors).show('fast');
            })
            .always(function () {
                disableTopUnionContractorSelectBoxes(false);
            });
    }

    function disableTopUnionContractorSelectBoxes(disabled) {
        regionUnion.prop('disabled', disabled);
        scopeUnion.prop('disabled', disabled);
    }

    regionUnion.on('change', function () {
        getUnionContractors();
    });
    scopeUnion.on('change', function () {
        getUnionContractors();
    });

    // bootstrap
    getUnionContractors();
    /*
     ------------------------------------------------------
     */


    /*
     Follow up project table
     */
    var followUp = $("#follow_up");

    // query follow up project data and render the table
    function getFollowUp() {
        disableFollowUpSelectBoxes(true);

        $.get("/pprofiles/dashboard/follow-up", {
            token: token,
            area: regionFollowUp.val()
        })
            .done(function (data) {
                var items = '';
                for (var index in data) {
                    var date=moment().format('MMM-DD-YYYY');

                    if(data[index].follow_up_date !== null){
                        date=moment(data[index].follow_up_date).format('MMM-DD-YYYY');
                    }
                    items += '<tr>' + '<td><a href="/pprofiles/projects/summary/' + data[index].id + '#details">' +
                        data[index].name + '</a></td>' + '<td>' + date + '</td>' + '</tr>';
                }
                followUp.hide().html(items).show('fast');
            })
            .always(function () {
                disableFollowUpSelectBoxes(false);
            });
    }

    function disableFollowUpSelectBoxes(disabled) {
        regionFollowUp.prop('disabled', disabled);
    }

    regionFollowUp.on('change', function () {
        getFollowUp();
    });

    // bootstrap
    getFollowUp();
    /*
     ------------------------------------------------------
     */


    /*
     latest 10 Rep visits table
     */
    var repVisits = $("#rep-visits");

    // query rep visits data and render the table
    function getRepVisits() {
        disableRepVisitsSelectBoxes(true);

        $.get("/pprofiles/dashboard/rep-visits", {
            token: token,
            area: regionRepVisits.val()
        })
            .done(function (data) {
                var items = '';
                for (var index in data) {
                    items += '<tr>' + '<td><a href="/pprofiles/settings/users/link/' + data[index].user_id + '#visits">' + data[index].name + '</a></td>' +
                        '<td>' + data[index].project_name + '</td>' +
                        '<td>' + moment(data[index].visit_date).format('MMM-DD-YYYY') + '</td>' + '</tr>';
                }
                repVisits.hide().html(items).show('fast');
            })
            .always(function () {
                disableRepVisitsSelectBoxes(false);
            });
    }

    function disableRepVisitsSelectBoxes(disabled) {
        regionRepVisits.prop('disabled', disabled);
    }

    regionRepVisits.on('change', function () {
        getRepVisits();
    });

    // bootstrap
    getRepVisits();
    /*
     ------------------------------------------------------
     */

});

