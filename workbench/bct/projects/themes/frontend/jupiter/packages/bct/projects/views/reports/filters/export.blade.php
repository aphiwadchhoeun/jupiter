<ul class="nav navbar-nav navbar-left download-container">

    <li class="dropdown">
        <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
           aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
            <i class="fa fa-download"></i> <span
                    class="visible-xs-inline">{{ trans('action.export') }}</span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
            <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
        </ul>
    </li>

</ul>