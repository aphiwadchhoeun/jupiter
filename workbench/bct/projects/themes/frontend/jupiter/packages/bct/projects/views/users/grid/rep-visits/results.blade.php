<script type="text/template" data-grid="rep-visits" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td>
            <%- r.project.name %>
            <% if(r.project.deleted_at) { %>
                <span class="label label-danger">Deleted</span>
            <% } %>
        </td>
        <td><%- r.details %></td>
        <td><%- r.stages_names %></td>
        <td><%- r.created %></td>
        <td class="hidden-print">
            <% if(r.view_url) { %>
                <a href="<%- r.view_url %>#visits" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                            class="fa fa-book fa-fw"></i></a>
            <% } %>
        </td>
    </tr>

    <% }); %>

</script>
