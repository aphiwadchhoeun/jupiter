@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans("action.{$mode}") }}} {{ trans('bct/projects::groups/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}

{{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
{{ Asset::queue('user-form', 'bct/projects::groups/js/form.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default">

        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip"
                                        href="{{ route('bct.projects.groups.all') }}"
                                       data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{{ trans("bct/projects::groups/common.title") }}} {{{ trans("action.{$mode}") }}}</span>
                        </div>

                        <div class="pull-right">
                            @if ($mode === 'edit')
                                <a href="{{ route('bct.projects.groups.delete', $group->id) }}"
                                   id="group_delete"
                                   name="group_delete"
                                   class="btn btn-danger"
                                   data-action-delete data-toggle="tooltip"
                                   data-original-title="{{{ trans('action.delete') }}}" type="delete">
                                    <i class="fa fa-trash-o fa-fw"></i></a>
                            @endif

                            <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                    data-original-title="{{ trans('action.save') }}">
                                <i class="fa fa-save fa-fw"></i></button>
                        </div>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <div class="col-md-12">

                    {{-- Form: General --}}
                    <fieldset>
                        <div class="row">

                            {{--add more form-group for other fields, just keep the same uniform--}}
                            <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                                <label for="firstname" class="control-label">
                                    {{{ trans('bct/projects::groups/model.general.name') }}}
                                </label>

                                <input type="text" class="form-control" name="name" id="name" required autofocus
                                       placeholder="{{{ trans('bct/projects::groups/model.general.name_help') }}}"
                                       value="{{{ input()->old('name', $group->name) }}}">

                                <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                            </div>

                            {{-- Form:status --}}
                            <div class="form-group{{ Alert::onForm('status', ' has-error') }}">

                                <label for="status" class="control-label">
                                    {{{ trans('bct/projects::groups/model.general.status') }}}
                                </label>

                                <select class="form-control" name="status" id="status">
                                    <option value="1" @if(input()->old('status', $group->status) === 1) selected @endif>{{{ trans('bct/projects::groups/model.general.active') }}}</option>
                                    <option value="0" @if($mode === 'create' || input()->old('status', $group->status) === 0) selected @endif>{{{ trans('bct/projects::groups/model.general.inactive') }}}</option>
                                </select>

                                <span class="help-block">{{{ Alert::onForm('z_project_status_id') }}}</span>

                            </div>


                            {{-- Form:type --}}
                            <div class="form-group{{ Alert::onForm('individual', ' has-error') }}">

                                <label for="individual" class="control-label">
                                    {{{ trans('bct/projects::groups/model.general.type') }}}
                                </label>

                                <select class="form-control" name="individual" id="individual">
                                    <option @if($group->individual==0) selected @endif value="0">{{trans('bct/projects::groups/model.general.group')}}</option>
                                    <option @if($group->individual==1) selected @endif value="1">{{trans('bct/projects::groups/model.general.individual')}}</option>
                                </select>

                                <span class="help-block">{{{ Alert::onForm('individual') }}}</span>

                            </div>


                            {{-- Form:description --}}
                            <div class="form-group{{ Alert::onForm('description', ' has-error') }}">

                                <label for="description" class="control-label">
                                    {{{ trans('bct/projects::groups/model.general.description') }}}
                                </label>

                                <textarea class="form-control" name="description" rows="4"
                                          placeholder="{{{ trans('bct/projects::groups/model.general.description_help') }}}"
                                >{{{ input()->old('description', $group->description) }}}</textarea>

                                <span class="help-block">{{{ Alert::onForm('description') }}}</span>

                            </div>

                        </div>

                    </fieldset>

                </div>

            </div>

        </form>

    </section>
@stop