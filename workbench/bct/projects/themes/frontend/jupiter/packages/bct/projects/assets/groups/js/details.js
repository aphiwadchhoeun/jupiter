var $modal = $('#modal-form');

jQuery('document').ready(function ($) {
    /** TABS */
    var hash = window.location.hash;

    $('.nav-tabs a').click(function (e) {
        $(this).tab('show');
        window.location.hash = this.hash;
        var scrollmem = $('body').scrollTop();
        $('html,body').scrollTop(scrollmem);
    });

    if (!hash) {
        $('.nav-tabs a[href="#details"]').click();
    }

    var config = {
        preventHashChange: true,
        hash: false
    };

    var gridRepsSummary, gridRepsAdd, gridRepsDel;


    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        var $id = $(e.target).attr('href');
        var $tabContent = $($id);
        if ($id == '#details') {

            if (!gridRepsSummary) {
                gridRepsSummary = $.datagrid('reps-summary', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);

            }
            else {
                gridRepsSummary.refresh();
                if (!window.location.origin) {
                    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
                }
                var url = window.location.origin + window.location.pathname + "/refresh";
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {},
                    success: function (response) {
                        $('#reps-counter').text(response);
                    }
                });
            }
        } else if ($id == '#reps') {
            if (!gridRepsDel) {
                gridRepsDel = $.datagrid('users-added', '#data-grid_users_in', '.data-grid_users_in_pagination', '#data-grid_users_in_applied', $.extend({}, config, {
                    events: {
                        fetched: function () {
                            if (!gridRepsAdd) {
                                gridRepsAdd = $.datagrid('users', '#data-grid_users_all', '.data-grid_users_all_pagination', '#data-grid_users_all_applied', config);
                            }
                        }
                    }
                }));
            }
        }
    });

    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.go-to-details').on('click', function (e) {
        e.preventDefault();
        $('.nav-tabs a[href="#details"]').click();
    });

    $('.go-to-tab').on('click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $('.nav-tabs a[href="' + id + '"]').click();
    });

    $('#show-details').on('click', function () {
        $('.nav-tabs a#dropdown1-tab').click();
    });

    $('#show-details-edit').on('click', function () {
        $('.nav-tabs a#dropdown2-tab').click();
        $('#show-details').parent().removeClass('active');
    });

    $('body').on('click', '.redirect-to', function () {
        var url = $(this).data('url');
        if (url) {
            window.location.href = url;
        }
    });

    function refreshGrid(refreshItem) {
        if (refreshItem == 'contractors') {
            if (gridProjectContractors) gridProjectContractors.refresh();
            if (gridProjectContractorsSummary) gridProjectContractorsSummary.refresh();
            if (gridContractors) gridContractors.refresh();
        } else if (refreshItem == 'contacts') {
            if (gridProjectContacts) gridProjectContacts.refresh();
            if (gridProjectContactsSummary) gridProjectContactsSummary.refresh();
            if (gridContacts) gridContacts.refresh();
        } else if (refreshItem == 'visits') {
            if (gridProjectVisits) gridProjectVisits.refresh();
            if (gridProjectVisitsSummary) gridProjectVisitsSummary.refresh();
            if (gridProjectLists) gridProjectLists.refresh();
            if (gridProjectListsSummary) gridProjectListsSummary.refresh();
        } else if (refreshItem == 'actions') {
            if (gridProjectActions) gridProjectActions.refresh();
            if (gridProjectActionsSummary) gridProjectActionsSummary.refresh();
        } else if (refreshItem == 'lists') {
            if (gridProjectLists) gridProjectLists.refresh();
            if (gridProjectListsSummary) gridProjectListsSummary.refresh();
        } else if (refreshItem == 'files') {
            Extension.Index.Grid.refresh();
            if (gridProjectFilesSummary) gridProjectFilesSummary.refresh();
        } else if (refreshItem == 'stewards') {
            if (gridProjectStewards) gridProjectStewards.refresh();
            if (gridProjectStewardsSummary) gridProjectStewardsSummary.refresh();
            if (gridStewards) gridStewards.refresh();
        }
    }

    $('body').on('click', '[data-modal]', function () {

        $modal.load($(this).attr('href'), '', function (response, status, xhr) {
            if (response) {
                $modal.modal('show');

                var submit_btn_modal = $('#submit-btn-modal');
                var delete_btn_modal = $('#delete-btn-modal');
                var form_modal = $('#content-form-modal');

                if (form_modal) {
                    var refreshItem = form_modal.data('refresh');

                    /** init select2 */
                    var input = $("#contact_type");
                    var stages = $("#stages");
                    var users = $("#users");
                    var lists = $("#project_lists");


                    if (input) {
                        input.select2({
                            createSearchChoice: function (term, data) {
                                if ($(data).filter(function () {
                                        return this.text.localeCompare(term) === 0;
                                    }).length === 0) {
                                    return {
                                        id: term,
                                        text: term
                                    };
                                }
                            },
                            multiple: false,
                            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                                url: input.data('href'),
                                dataType: 'json',
                                quietMillis: 250,
                                data: function (term, page) {
                                    return {
                                        q: term // search term
                                    };
                                },
                                results: function (data, page) { // parse the results into the format expected by Select2.
                                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                                    var arr = [];
                                    _.each(data, function (item) {
                                        arr.push({
                                            id: item.name,
                                            text: item.name
                                        });
                                    });
                                    return {results: arr};
                                },
                                cache: true
                            },
                            initSelection: function (element, callback) {
                                // the input tag has a value attribute preloaded that points to a preselected repository's id
                                // this function resolves that id attribute to an object that select2 can render
                                // using its formatResult renderer - that way the repository name is shown preselected
                                var id = $(element).val();
                                if (id !== "") {
                                    $.ajax(input.data('href') + '?id=' + id, {
                                        dataType: "json"
                                    }).done(function (data) {
                                        callback({
                                            id: data.name,
                                            text: data.name
                                        });
                                    });
                                }
                            }
                        });
                    }

                    if (stages) stages.select2();
                    if (users) users.select2();
                    if (lists) lists.select2();

                    /** init datetimepicker */
                    var inputDate = form_modal.find("#follow_up_date");

                    if (inputDate) {
                        inputDate.parents('.date').datepicker({
                            format: 'yyyy-mm-dd',
                            assumeNearbyYear: true,
                            showOnFocus: false
                        });
                    }

                    form_modal.on('submit', function (e) {
                        e.preventDefault();
                    });

                    form_modal.on('keypress', function (e) {

                        var keycode = e.which || e.keyCode;

                        if (keycode == 13) {
                            e.preventDefault();
                        }
                    });

                    if (form_modal.parsley()) {
                        form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
                            if (formInstance.isValid() === true) {
                                formInstance.asyncIsValid().done(function () {
                                    $.post(
                                        form_modal.attr('action'),
                                        form_modal.serialize(),
                                        function (data) {
                                            $modal.modal('hide');
                                            refreshGrid(refreshItem);

                                            if (typeof data.refreshUnion != 'undefined') {
                                                $('#union-value').text(data.refreshUnion);
                                            }
                                        }
                                    );
                                });
                            }
                        });

                        submit_btn_modal.on('click', function () {
                            form_modal.parsley().asyncValidate();
                        });
                    }

                    if (delete_btn_modal) {
                        delete_btn_modal.on('click', function () {
                            var url = $(this).data('url');
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {},
                                success: function (response) {
                                    $modal.modal('hide');
                                    refreshGrid(refreshItem);

                                    if (typeof response.refreshUnion != 'undefined') {
                                        $('#union-value').text(response.refreshUnion);
                                    }
                                }
                            });
                        });
                    }
                }
            } else {
                location.reload();
            }
        });
    });
    $('body').on('click', '#submit-btn', function (e) {
        e.preventDefault();
        var form_modal = $('#content-form');

        var refreshItem = form_modal.data('refresh');
        var status = $("#status").val();
        if (status == 0) {
            var group_id = $("#group_id").val();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            var url = window.location.origin + "/pprofiles/settings/groups/check";

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    id: group_id
                },
                success: function (response) {
                    if (response.success) {
                        swal({
                            title: "Are you sure you want to save?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#019964",
                            confirmButtonText: "Yes!",
                            cancelButtonText: "No",
                            showLoaderOnConfirm: true,
                            preConfirm: function () {
                                return new Promise(function (resolve, reject) {
                                    $.post(
                                        form_modal.attr('action'),
                                        form_modal.serialize(),
                                        function (data) {
                                            if (data.error) {
                                                window.location.reload();
                                            } else {
                                                resolve();
                                            }
                                        }
                                    );
                                });
                            }
                        }).then(function () {
                            window.location.href = window.location.origin + window.location.pathname;
                        });
                    }
                    else {
                        swal({
                            title: "Some lists will not be accessible: " + response.lists,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#019964",
                            confirmButtonText: "Yes!",
                            cancelButtonText: "No",
                            showLoaderOnConfirm: true,
                            preConfirm: function () {
                                return new Promise(function (resolve, reject) {
                                    $.post(
                                        form_modal.attr('action'),
                                        form_modal.serialize(),
                                        function (data) {
                                            resolve();
                                        }
                                    );
                                });
                            }
                        }).then(function () {
                            window.location.href = window.location.origin + window.location.pathname;
                        });
                    }
                }
            });
        }
        else if (status == 1) {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            swal({
                title: "Are you sure you want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve, reject) {
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize(),
                            function (data) {
                                if (data.error) {
                                    window.location.reload();
                                }
                                else {
                                    resolve();
                                }
                            }
                        );
                    })
                }
            }).then(function () {
                window.location.href = window.location.origin + window.location.pathname;
            });
        }

    });

});
