var ProjectContractorExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectContractorExtension = Object.create(ExtensionBase);
    ProjectContractorExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-contractor',
        buttons: {
            na_only_yes: $('#project-contractor-filter-na-only-yes'),
            na_only_no: $('#project-contractor-filter-na-only-no')
        }
    });

    // Initialize functions
    ProjectContractorExtension.Index.init = function () {
        ProjectContractorExtension.Index.dataGrid();
        ProjectContractorExtension.Index.listeners();
    };

    // Add Listeners
    ProjectContractorExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', ProjectContractorExtension.Index.titleClick)
            .on('change', '#project-contractor-filter-checkbox-na', ProjectContractorExtension.Index.changeNaOnly)
        ;
    };

    ProjectContractorExtension.Index.changeNaOnly = function()
    {
        if ($(this).is(':checked') === true) {
            ProjectContractorExtension.Index.setGetParameter('na_only', 1);
        } else {
            ProjectContractorExtension.Index.setGetParameter('na_only', 0);
        }

        ProjectContractorExtension.Index.Grid.refresh();
    };

    // Ignore row selection on title click
    ProjectContractorExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    ProjectContractorExtension.Index.setGetParameter = function(paramName, paramValue)
    {
        var url = ProjectContractorExtension.Index.Grid.source;
        if (url.indexOf(paramName + "=") >= 0)
        {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
        }
        else
        {
            if (url.indexOf("?") < 0)
                url += "?" + paramName + "=" + paramValue;
            else
                url += "&" + paramName + "=" + paramValue;
        }

        ProjectContractorExtension.Index.Grid.source = url;
    };

    ProjectContractorExtension.Index.removeParam = function(key) {
        var sourceURL = ProjectContractorExtension.Index.Grid.source,
            rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        ProjectContractorExtension.Index.Grid.source = rtn;
    };


    ProjectContractorExtension.Index.exporterStatus = function (grid) {
        $('#project-contractor-exporter').toggleClass('hide', grid.pagination.filtered == 0);
        $('#project-contractor-exporter').parent().toggleClass('hide', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    ProjectContractorExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            callback: function (data) {
                ProjectContractorExtension.Index
                    .exporterStatus(data);
            }
        };

        ProjectContractorExtension.Index.Grid = $.datagrid('project-contractor', '#project-contractor-data-grid', '.project-contractor-data-grid_pagination', '#project-contractor-data-grid_applied', config);
    };

    // Job done, lets run.
    ProjectContractorExtension.Index.init();

})(window, document, jQuery);

// Project Contractor Data Grid
$('#project-contractor-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-gear');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-gear');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var delete_btn_modal = $('#delete-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectContractorExtension.Index.Grid.refresh();
                                    contractorsSummaryUpdate();
                                } else {
                                    swal(data);
                                }
                            })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });

                if (delete_btn_modal) {
                    delete_btn_modal.on('click', function () {
                            delete_btn_modal.prop('disabled', true);
                            var url = $(this).data('url');
                            $.post(url, {})
                                .done(function (data) {
                                    if (data == 'success') {
                                        $modal.modal('hide');
                                        ProjectContractorExtension.Index.Grid.refresh();
                                        contractorsSummaryUpdate();
                                    } else {
                                        swal('Deleting failed.');
                                    }
                                })
                                .always(function () {
                                    delete_btn_modal.removeAttr('disabled');
                                });
                        }
                    );
                }
            }

        }
        else {
            location.reload();
        }
    });
})
;