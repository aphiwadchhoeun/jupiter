var ExtensionAdded;

;(function(window, document, $, undefined)
{

	'use strict';

    ExtensionAdded = Object.create(ExtensionBase);
    ExtensionAdded.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'users-added'
    });

	// Initialize functions
	ExtensionAdded.Index.init = function()
	{
		ExtensionAdded.Index.dataGrid();
		ExtensionAdded.Index.listeners();
	};

	// Add Listeners
	ExtensionAdded.Index.listeners = function()
	{
		Platform.Cache.$body
			//.on('click', '[data-grid-row]', ExtensionAdded.Index.checkRow)
			.on('click', '#data-grid-added [data-grid-row] a', ExtensionAdded.Index.titleClick)
			.on('click', '#data-grid-added [data-grid-checkbox]', ExtensionAdded.Index.checkboxes)
			.on('click', '#btn-remove-groups', ExtensionAdded.Index.bulkActions)
			.on('click', '.remove-from-group', ExtensionAdded.Index.bulkRow)
		;
	};

	// Handle Data Grid checkboxes
	ExtensionAdded.Index.checkboxes = function(event)
	{
		event.stopPropagation();

		var type = $(this).attr('data-grid-checkbox');

		if (type === 'all')
		{
			$('#data-grid-added [data-grid-checkbox]').not(this).prop('checked', this.checked);

			$('#data-grid-added [data-grid-row]').not(this).toggleClass('active', this.checked);
		}

		$(this).parents('[data-grid-row]').toggleClass('active');

		ExtensionAdded.Index.bulkStatus();
	};

	// Handle Data Grid row checking
	ExtensionAdded.Index.checkRow = function()
	{
		$(this).toggleClass('active');

		var checkbox = $(this).find('[data-grid-checkbox]');

		checkbox.prop('checked', ! checkbox.prop('checked'));

		ExtensionAdded.Index.bulkStatus();
	};

	ExtensionAdded.Index.bulkStatus = function()
	{
		var rows = $('#data-grid-added [data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

		var checked = $('#data-grid-added [data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;

		$('[data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);

		if (checked > 0)
		{
			$('[data-grid-bulk-action="delete"]').attr('data-modal', true);
			$('#btn-remove-groups').removeClass('disabled');
		}
		else
		{
			$('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
			$('#btn-remove-groups').addClass('disabled');
		}

		$('#data-grid-added [data-grid-checkbox="all"]')
			.prop('disabled', rows < 1)
			.prop('checked', rows < 1 ? false : rows === checked)
		;
	};

	// Handle Data Grid bulk actions
	ExtensionAdded.Index.bulkActions = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

		var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
		{
			return +event.value;
		});

		if (rows.length > 0)
		{
			$.ajax({
				type: 'POST',
				url: url,
				data: {
					action : 'detach',
					rows   : rows
				},
				success: function(response)
				{
					ExtensionAdded.Index.Grid.refresh();
					ExtensionAdded.Index.Grid.refresh();
				}
			});
		}
	};

	ExtensionAdded.Index.bulkRow = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

		var id = $(this).parents('tr').find('input').val();
		if (id)
		{
			$.ajax({
				type: 'POST',
				url: url,
				data: {
					action : 'detach',
					rows   : [id]
				},
				success: function(response)
				{
					ExtensionAdded.Index.Grid.refresh();
					ExtensionAdded.Index.Grid.refresh();
				}
			});
		}
	};

	// Ignore row selection on title click
	ExtensionAdded.Index.titleClick = function(event)
	{
		event.stopPropagation();
	};

	ExtensionAdded.Index.setGetParameter = function(paramName, paramValue)
	{
		var url = ExtensionAdded.Index.Grid.source;
		if (url.indexOf(paramName + "=") >= 0)
		{
			var prefix = url.substring(0, url.indexOf(paramName));
			var suffix = url.substring(url.indexOf(paramName));
			suffix = suffix.substring(suffix.indexOf("=") + 1);
			suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
			url = prefix + paramName + "=" + paramValue + suffix;
		}
		else
		{
			if (url.indexOf("?") < 0)
				url += "?" + paramName + "=" + paramValue;
			else
				url += "&" + paramName + "=" + paramValue;
		}

		ExtensionAdded.Index.Grid.source = url;
	};

	ExtensionAdded.Index.removeParam = function(key) {
		var sourceURL = ExtensionAdded.Index.Grid.source,
				rtn = sourceURL.split("?")[0],
				param,
				params_arr = [],
				queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
		if (queryString !== "") {
			params_arr = queryString.split("&");
			for (var i = params_arr.length - 1; i >= 0; i -= 1) {
				param = params_arr[i].split("=")[0];
				if (param === key) {
					params_arr.splice(i, 1);
				}
			}
			rtn = rtn + "?" + params_arr.join("&");
		}
		ExtensionAdded.Index.Grid.source = rtn;
	};

	// Data Grid initialization
	ExtensionAdded.Index.dataGrid = function()
	{
		var filter_column_calendar = $("[data-grid-calendar]").data('range-filter');
		var config = {
			scroll: '#data-grid',
			events: {
				removing: function(dg) {
					_.each(dg.applied_filters, function(filter) {
						if (filter.column === filter_column_calendar && filter.from !== undefined && filter.to !== undefined) {
							$('[data-grid-calendar]').val('');
						}
					});
				},
				sorted: function(response) {
					if (response.pagination.total== 0) {
						$('[data-grid="users-added"].download-container').addClass('hide');
					}
				},
				fetched: function(response) {
					if (response.pagination.filtered == 0) {
						$('[data-grid="users-added"].download-container').addClass('hide');
					} else {
						$('[data-grid="users-added"].download-container').removeClass('hide');
					}
				},
			},
			callback: function()
			{
				$('[data-grid-checkbox-all]').prop('checked', false);

				$('[data-action]').prop('disabled', true);

				ExtensionAdded.Index.bulkStatus();
			}
		};

		ExtensionAdded.Index.Grid = $.datagrid('users-added', '#data-grid-added', '.data-grid_pagination', '#data-grid_applied', config);
	};

	// Job done, lets run.
	ExtensionAdded.Index.init();

})(window, document, jQuery);
