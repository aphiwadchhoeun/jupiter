var Extension;

/**
 * TODO: TEST ON SAFARI, KNOW ISSUE WITH IPAD
 * Array can't use computed index
 */

;(function (window, document, $, undefined) {
    $('#state').on('keypress', function (event) {
        if (null !== String.fromCharCode(event.which).match(/[a-z]/g)) {
            event.preventDefault();
            $(this).val($(this).val() + String.fromCharCode(event.which).toUpperCase());
        }
    });
    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'contractors'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid-row]', Extension.Index.checkRow)
            .on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
        ;
    };


    Extension.Index.bulkStatus = function () {
        var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

        var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;

        $('[data-grid-bulk-action]').closest('li').toggleClass('disabled', !checked);
        $('[data-grid-bulk-action]').toggleClass('disabled', !checked);

        if (checked > 0) {
            $('[data-grid-bulk-action="delete"]').attr('data-modal', true);
        }
        else {
            $('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
        }

        $('[data-grid-checkbox="all"]')
            .prop('disabled', rows < 1)
            .prop('checked', rows < 1 ? false : rows === checked)
        ;
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxes = function (event) {
        event.stopPropagation();

        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all') {
            $('[data-grid-checkbox]').not(this).prop('checked', this.checked);

            $('[data-grid-row]').not(this).toggleClass('active', this.checked);
        }

        $(this).parents('[data-grid-row]').toggleClass('active');

        Extension.Index.bulkStatus();
        
    };

    // Handle Data Grid row checking
    Extension.Index.checkRow = function () {
        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', !checkbox.prop('checked'));

        Extension.Index.bulkStatus();
        
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            hash: false,
            events: {
                removing: function (dg) {
                    _.each(dg.applied_filters, function (filter) {
                        if (filter.column === 'created_at' && filter.from !== undefined && filter.to !== undefined) {
                            $('[data-grid-calendar]').val('');
                        }
                    });
                },
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
            callback: function () {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

                Extension.Index.bulkStatus();
                
            }
        };

        Extension.Index.Grid = $.datagrid('contractors', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
