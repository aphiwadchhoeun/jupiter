<script type="text/template" data-grid="projects" data-template="results">

	<% _.each(results, function(r) { %>

        <tr data-grid-row>
            <td><%- r.name %></td>
            <td><%- r.description %></td>
            <td><%- r.follow_up_date_custom %></td>
            <td><a href="<%- r.view_uri %>#details" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.select') }}}"><i
                            class="fa fa-check fa-fw"></i></a></td>
        </tr>

	<% }); %>

</script>
