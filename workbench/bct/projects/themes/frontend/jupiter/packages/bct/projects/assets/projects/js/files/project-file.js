var ProjectFileExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectFileExtension = Object.create(ExtensionBase);
    ProjectFileExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-file'
    });

    // Initialize functions
    ProjectFileExtension.Index.init = function () {
        ProjectFileExtension.Index
            .listeners()
            .dataGrid()
            .mediaManager()
        ;
    };

    // Add Listeners
    ProjectFileExtension.Index.listeners = function () {
        $('#project-file-data-grid')
            .on('click', '[data-grid-row]', ProjectFileExtension.Index.checkRow)
            .on('click', '[data-grid-row] a', ProjectFileExtension.Index.titleClick)
            .on('click', '[data-grid-checkbox]', ProjectFileExtension.Index.checkboxes)
            .on('click', '#modal-confirm a.confirm', ProjectFileExtension.Index.bulkActions)
            .on('click', '[data-grid-bulk-action]:not([data-grid-bulk-action="delete"])', ProjectFileExtension.Index.bulkActions)
        ;
        return this;
    };

    ProjectFileExtension.Index.exporterStatus = function (grid) {
        $('#project-file-exporter').toggleClass('hide', grid.pagination.filtered == 0);
        $('#project-file-exporter').parent().toggleClass('hide', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    ProjectFileExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            callback: function (data) {
                ProjectFileExtension.Index
                    .bulkStatus()
                    .exporterStatus(data);
            }
        };

        ProjectFileExtension.Index.Grid = $.datagrid('project-file', '#project-file-data-grid', '.project-file-data-grid_pagination', '#project-file-data-grid_applied', config);

        return this;
    };

    // Handle Data Grid checkboxes
    ProjectFileExtension.Index.checkboxes = function (event) {
        event.stopPropagation();

        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all') {
            $('[data-grid-checkbox]').not(this).not('[data-grid-checkbox][disabled]').prop('checked', this.checked);

            $('[data-grid-row]').not('[data-grid-row][disabled]').not(this).toggleClass('active', this.checked);
        }

        $(this).parents('[data-grid-row]').not('[data-grid-row][disabled]').toggleClass('active');

        ProjectFileExtension.Index.bulkStatus();
    };

    // Handle Data Grid row checking
    ProjectFileExtension.Index.checkRow = function () {
        if ($(this).find('[data-grid-checkbox]').prop('disabled')) return false;

        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', !checkbox.prop('checked'));

        ProjectFileExtension.Index.bulkStatus();
    };

    ProjectFileExtension.Index.bulkStatus = function () {
        var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').not('[data-grid-checkbox][disabled]').length;

        var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').not('[data-grid-checkbox][disabled]').length;

        $('[data-grid-bulk-action]').closest('li').toggleClass('disabled', !checked);
        $('[data-grid-bulk-action]').toggleClass('disabled', !checked);

        if (checked > 0) {
            $('[data-grid-bulk-action="delete"]').attr('data-modal', true);
        }
        else {
            $('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
        }

        $('[data-grid-checkbox="all"]')
            .prop('disabled', rows < 1)
            .prop('checked', rows < 1 ? false : rows === checked);

        return this;
    };

    // Handle Data Grid bulk actions
    ProjectFileExtension.Index.bulkActions = function (event) {
        event.preventDefault();
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var url = window.location.origin + window.location.pathname;

        var action = $(this).data('grid-bulk-action') ? $(this).data('grid-bulk-action') : 'delete';

        if (action == 'delete') {
            url = $('[data-grid-bulk-action="delete"]').data('grid-url') ? $('[data-grid-bulk-action="delete"]').data('grid-url') : url;
        }

        var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function (event) {
            return +event.value;
        });

        if (rows.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    action: action,
                    rows: rows
                },
                success: function (response) {
                    ProjectFileExtension.Index.Grid.refresh();
                    filesSummaryUpdate();
                }
            });
        }
    };

    // Ignore row selection on title click
    ProjectFileExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    ProjectFileExtension.Index.mediaManager = function () {
        var $modal = $('#modal-form');

        ProjectFileExtension.Index.MediaManager = $.mediamanager({
            uploadUrl: $('.files-add-btn').attr('href'),
            onFileQueued: function (file) {
                $('input.file-tags').not('.selectize-control').selectize({
                    delimiter: ',',
                    persist: false,
                    maxItems: 3,
                    create: function (input) {
                        return {
                            value: input,
                            text: input
                        }
                    }
                });

                $('.upload__instructions').hide();
            },
            onProgress: function (evt) {
                $('.file-error-help').text('');
                $('.saving').html('Upload in progress<span>.</span><span>.</span><span>.</span>');
                $('.saving').show();
                $('#start_upload').prop('disabled', true);
            },
            onComplete: function () {
                $('.file-error-help').text('');
                $('.saving').hide();
                $modal.modal('hide');

                $('.upload__instructions').show();

                ProjectFileExtension.Index.Grid.refresh();
                filesSummaryUpdate();
                $('#start_upload').prop('disabled', false);
            },
            onFail: function (e) {
                $('.file-error-help').hide();
                $('.saving').html(e.responseText);
                $('.saving').show();
                $('#start_upload').prop('disabled', false);
            },
            onRemove: function (manager, file) {
                if (manager.totalFiles == 0) {
                    $('.upload__instructions').show();
                }
            }
        });

        return this;
    };

    // Job done, lets run.
    ProjectFileExtension.Index.init();

})(window, document, jQuery);


$('.files-add-btn').on('click', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');
        }
        else {
            location.reload();
        }
    });
});

// Project File Data Grid
$('#project-file-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-gear');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-gear');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');
            var delete_btn_modal = $('#delete-btn-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                            if (data == 'success') {
                                $modal.modal('hide');
                                ProjectFileExtension.Index.Grid.refresh();
                                filesSummaryUpdate();
                            } else {
                                swal(data);
                            }
                        });
                    }
                });

                submit_btn_modal.on('click', function () {
                    form_modal.parsley().validate();
                });
                delete_btn_modal.on('click', function () {
                    var href = $(this).data('href');
                    swal({
                        title: "Are you sure you want to delete?",
                        text: false,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No",
                        showLoaderOnConfirm: true,
                        preConfirm: function () {
                            return new Promise(function (resolve, reject) {
                                $.ajax({
                                    url: href,
                                    type: 'DELETE',
                                    success: function (result) {
                                        if (result.success) {
                                            resolve();
                                        } else {
                                            reject('Error!');
                                        }
                                    }
                                });
                            });
                        }
                    }).then(function () {
                        $modal.modal('hide');
                        ProjectFileExtension.Index.Grid.refresh();
                        filesSummaryUpdate();
                    });

                });
            }

        } else {
            location.reload();
        }
    });
});