<span class="navbar-brand">{{ trans("bct/projects::contacts/common.title") }} <span id="contacts-counter" class="text14">( {{ $contacts->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $contacts->count() }} )</span></span>
@if (!$currentUser->inRole('pp-guest'))

<a data-id="#contacts" id="contacts_tab" name="contacts_tab" class="btn btn-primary go-to-tab pull-right"
   data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
            class="fa fa-edit fa-fw"></i></a>
@endif
<table class="table table-striped table-bordered">
    <tbody>
    <tr>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::contacts/model.general.company') }}</td>
        <td class="td-label col-md-5 text-center">{{ trans('bct/projects::contacts/model.general.name') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::projects/model.general.type') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::contacts/model.general.phone') }}</td>

    </tr>
    @foreach($contacts->take(5) as $contact)
        <tr>
            <td class="td-value"><a href="{{ route('bct.projects.contacts.details', $contact->id) }}">{{ $contact->company }}</a></td>
            <td class="td-value text-center"><a href="{{ route('bct.projects.contacts.details', $contact->id) }}">{{ $contact->full_name }}</a></td>
            <td class="td-value text-center">{{ $contact->type }}</td>
            <td class="td-value text-center"><a href="tel:{{ $contact->phone }}">{{ $contact->phone }}</a></td>
        </tr>
    @endforeach
</table>