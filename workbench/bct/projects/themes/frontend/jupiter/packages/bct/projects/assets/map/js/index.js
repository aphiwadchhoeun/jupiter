/**
 * Callback function for Google Map API
 */
function loadMap() {
    MapComponent.init();
}

$('body').on('click', '[data-modal]', function () {
    var $modal = $('#modal-form');

    $modal.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
        if (response) {
            $modal.modal('show');
            $('#add-to-list').addClass('disabled');
        } else {
            location.reload();
        }
    });
});

$('#map-submit-btn').on('click', function (e) {
    e.preventDefault();
    var url = $(this).data('submit-href');
    var source = $(this);

    source.prop('disabled', true);
    source.find('i.fa').removeClass('fa-map-marker')
        .addClass('fa-spinner fa-spin');

    $.post(url, {
        'entries': MapExtension.Index.checkedRows
    }).done(function (data) {
        window.location = data;
    })
        .fail(function () {
            source.removeAttr('disabled');
            source.find('i.fa').removeClass('fa-spinner fa-spin')
                .addClass('fa-map-marker');
        });
});


/**
 * Define MapComponent
 *
 */
var MapComponent = MapComponent || {

        default_lat: (typeof BctProjects.default_lat !== 'undefined') ? BctProjects.default_lat : '',
        default_lng: (typeof BctProjects.default_lng !== 'undefined') ? BctProjects.default_lng : '',

        cartodb_api: null,
        cartodb_table: null,

        projectIcon: {
            path: 'M -5,-5 0,-10 5,-5 5,0 1,0 1,-3 -1,-3 -1,0 -5,0 z',
            fillColor: '#4CAF50',
            fillOpacity: 0.8,
            scale: 2,
            strokeColor: '#000000',
            strokeWeight: 2
        },

        currentLat: null,
        currentLng: null,
        currentProjectLimit: (typeof BctProjects.count_projects !== 'undefined') ? BctProjects.count_projects : 250,
        currentStatus: 'Active',
        currentStartDate: '',
        currentEndDate: '',
        currentContractor: '',
        currentTags: '',
        currentStage: '',
        currentRegion: '',
        currentRadius: (typeof BctProjects.radius !== 'undefined') ? BctProjects.radius : 0.5,

        map: null,
        geocoder: null,

        marker: null,
        circle: null,

        icons: [],
        searchInput: null,
        searchRealInput: $('#searchmap'),

        projectLimitInput: $('#map_count_projects'),
        statusInput: $('#project_status'),
        startDateInput: $('#'),
        followUpDateDisplay: $('#follow_up_display'),
        contractorInput: $('#project_contractor'),
        tagsInput: $('#project_tags'),
        stageInput: $('#project_stage'),
        regionInput: $('#project_region'),
        radiusInput: $('#map_radius'),

        tagsBtn: $('#project_tags_submit'),
        followUpClearBtn: $('#follow_up_clear'),
        contractorClearBtn: $('#project_contractor_clear'),
        tagsClearBtn: $('#tags_clear'),

        createNewProjectBtn: $('#create-new-project'),
    };

/**
 * Initialize
 */
MapComponent.init = function () {

    // init geocoder
    this.geocoder = new google.maps.Geocoder();

    //init map and pan to a default location
    this.map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 15,
        streetViewControl: false,
        styles: [
            {
                featureType: "poi",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ]
    });
    var map = this.map;
    map.panTo(new google.maps.LatLng(this.default_lat, this.default_lng));

    // move to current gps location
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.panTo(new google.maps.LatLng(pos.lat, pos.lng));
        });
    }

    // init marker on map
    this.marker = new google.maps.Marker({
        title: 'Your current location',
        map: this.map,
    });

    // init circle layer
    this.circle = new google.maps.Circle({
        strokeColor: '#0f27a1',
        strokeOpacity: 0.6,
        strokeWeight: 1,
        fillColor: '#0f27a1',
        fillOpacity: 0.1,
        map: this.map,
        radius: this.currentRadius * 1609.344
    });

    // init search input box
    this.searchInput = new google.maps.places.SearchBox(document.getElementById('searchmap'));

    this.followUpDateInit();

    this.initRegions();

    this.events();

    // init Grid
    MapExtension.Index.init();
};

/**
 * Event listeners
 */
MapComponent.events = function () {

    var self = this;

    // add a click event handler to the map object
    self.map.addListener("click", function (event) {
        self.fireSearch(event.latLng);
    });

    // enable click event on circle
    google.maps.event.addListener(self.circle, "click", function (event) {
        google.maps.event.trigger(self.map, 'click', event);
    });

    // event search input box
    self.searchInput.addListener("places_changed", function () {
        var places = self.searchInput.getPlaces();

        if (places.length <= 0) return;

        self.fireSearch(places[0].geometry.location);
    });

    // project limit select box changed
    self.projectLimitInput.on('change', function (e) {
        self.currentProjectLimit = parseInt(self.projectLimitInput.val());
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // project status select box changed
    self.statusInput.on('change', function (e) {
        self.currentStatus = self.statusInput.val();
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // contractor typeahead
    self.contractorTypeahead();

    // follow up date clear button
    self.followUpClearBtn.on('click', function (e) {
        self.currentStartDate = self.currentEndDate = '';
        self.followUpDateDisplay.parent().addClass('hide');
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // contractor clear button
    self.contractorClearBtn.on('click', function (e) {
        self.currentContractor = '';
        self.contractorInput.typeahead('val', '');
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // tags search button
    self.tagsBtn.on('click', function (e) {
        self.currentTags = self.tagsInput.val();
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // tags clear button
    self.tagsClearBtn.on('click', function (e) {
        self.currentTags = '';
        self.tagsInput.val('');
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // stage select box changed
    self.stageInput.on('change', function (e) {
        self.currentStage = self.stageInput.val();
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // region select box changed
    self.regionInput.on('change', function (e) {
        self.currentRegion = self.regionInput.val();
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

    // radius select box changed
    self.radiusInput.on('change', function (e) {
        self.currentRadius = parseFloat(self.radiusInput.val());
        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();

        self.dropPin(new google.maps.LatLng(self.currentLat, self.currentLng));
    });

    // create new project button clicked
    self.createNewProjectBtn.on('click', function (e) {
        e.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');

        var params = "?lat=" + self.currentLat
            + "&lng=" + self.currentLng;
        $(location).attr("href", url + params);
    });

};

/**
 * Contractor typeahead event
 */
MapComponent.contractorTypeahead = function () {
    var self = this;

    var bloodhound = new Bloodhound({
        remote: {
            url: self.contractorInput.data('url') + '?q=%QUERY',
            wildcard: '%QUERY'
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.name);
        },
        rateLimitWait: 500
    });

    self.contractorInput.typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
            limit: 5,
        },
        {
            name: 'contractor',
            display: 'name',
            source: bloodhound
        }).bind('typeahead:selected', function (event, data) {

        self.currentContractor = data.name;

        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();

    }).bind('typeahead:autocompleted', function (event, data) {
        self.currentContractor = data.name;

        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();

    }).on('change', function (e, data) {
        if (!data) {
            self.currentContractor = $(this).val();

            self.AjaxCallOnMap();
            MapExtension.Index.Grid.refresh();
        }
    });
};

/**
 * Init follwup date input
 */
MapComponent.followUpDateInit = function () {
    var self = this;

    $('[data-map-calendar]').daterangepicker({
        opens: 'left'
    }, function (start, end, label) {
        $('input[name="daterangepicker_start"]').trigger('change');
    });

    $('input[name="daterangepicker_start"]')
        .attr('data-format', 'MM/DD/YYYY')
        .attr('data-range-start', '');

    $('input[name="daterangepicker_end"]')
        .attr('data-format', 'MM/DD/YYYY')
        .attr('data-range-end', '');

    $('body').on('click', '.range_inputs .applyBtn', function () {
        $('input[name="daterangepicker_start"]').trigger('change');

        if ($('input[name="daterangepicker_start"]').val() != '' && $('input[name="daterangepicker_end"]').val() != '') {
            self.currentStartDate = moment($('input[name="daterangepicker_start"]').val()).format('MM/DD/YYYY');
            self.currentEndDate = moment($('input[name="daterangepicker_end"]').val()).format('MM/DD/YYYY');

            $('[data-map-calendar]').val(
                self.currentStartDate + ' - ' + self.currentEndDate
            );
        }
        self.followUpDateDisplay.text(moment(self.currentStartDate).format('MMM DD, YYYY') + ' - ' + moment(self.currentEndDate).format('MMM DD, YYYY'));
        self.followUpDateDisplay.parent().removeClass('hide');

        self.AjaxCallOnMap();
        MapExtension.Index.Grid.refresh();
    });

};

/**
 * Fetching cartodb api and table name
 * @param callback
 */
MapComponent.fetchCartoDbConfigs = function (callback) {
    var self = this;

    if (self.cartodb_api == null || self.cartodb_table == null) {
        $.get('/ui/api/cartodb/configs/get')
            .done(function (data) {
                callback(data.api_url, data.tablename);
            });
    }
};

/**
 * load regions into filter select box
 */
MapComponent.loadRegions = function (cartodb_api, cartodb_table) {
    var self = this;
    self.cartodb_api = cartodb_api;
    self.cartodb_table = cartodb_table;

    // fetching region select box
    var regionService = self.cartodb_api + '/sql?q=select distinct areaabrv, areaname from ' + self.cartodb_table + ' order by areaabrv asc';

    // make a call to cartodb service and retrieve regions data, then populate them into select boxe
    $.get(regionService, function (data) {
        if (data.rows.length > 0) {

            $.each(data.rows, function (i, row) {
                $('#project_region').append($('<option>', {
                    value: row.areaabrv,
                    text: row.areaabrv
                }));
            });
        }
    });
};

/**
 * Initi region select box
 */
MapComponent.initRegions = function () {
    this.fetchCartoDbConfigs(this.loadRegions);
};

/**
 * Drop a pin on map
 *
 * @param position
 */
MapComponent.dropPin = function (position) {
    this.currentLat = position.lat();
    this.currentLng = position.lng();

    this.marker.setPosition(position);
    this.circle.setCenter(position);
    this.circle.setRadius(this.currentRadius * 1609.344);
};

/**
 * Searching fired
 */
MapComponent.fireSearch = function (position) {
    this.createNewProjectBtn.removeAttr('disabled');

    this.map.panTo(position);
    this.dropPin(position);
    this.searchProjects(position);
};

/**
 * Searching projects
 */
MapComponent.searchProjects = function (position) {
    var self = this;

    this.geocoder.geocode({
        latLng: position
    }, function (responses) {
        if (responses && responses.length > 0) {
            self.searchRealInput.val(responses[0].formatted_address);

            self.AjaxCallOnMap();

            // refresh grid
            MapExtension.Index.Grid.refresh();
        } else {
            /** clear inputs */
            self.searchRealInput.val('');
        }
    });
};

/**
 * Ajax call to search projects for map
 */
MapComponent.AjaxCallOnMap = function () {
    var self = this;

    self.clearProjectIconsOnMap();

    self.showLoaderAnimation();
    $.post('/pprofiles/projects/map/search', {
        lat: self.currentLat,
        lng: self.currentLng,
        radius: self.currentRadius,
        count_projects: self.currentProjectLimit,
        status: self.currentStatus,
        start_date: self.currentStartDate,
        end_date: self.currentEndDate,
        contractor: self.currentContractor,
        tags: self.currentTags,
        stage: self.currentStage,
        region: self.currentRegion,
    })
        .success(function (data) {
            self.putProjectIconsOnMap(data);
        })
        .always(function () {
            self.hideLoaderAnimation();

            if (typeof callback !== 'undefined') {
                callback();
            }
        });
};

MapComponent.showLoaderAnimation = function () {
    $('#map-loader-animation').show();
};

MapComponent.hideLoaderAnimation = function () {
    $('#map-loader-animation').fadeOut('fast');
};

/**
 * Populate project icons on map
 *
 * @param data
 */
MapComponent.putProjectIconsOnMap = function (data) {
    var self = this;

    $.each(data, function (i, item) {

        var icon = new google.maps.Marker({
            title: item.name,
            map: self.map,
            position: new google.maps.LatLng(item.lat, item.lng),
            icon: self.getIconUrl(item)
        });

        var infoWindow = self.getWindowInfo(item);

        icon.addListener('click', function () {
            infoWindow.open(self.map, icon);
        });


        self.icons.push(icon);
    });
};

/**
 * Clear project icons on map
 */
MapComponent.clearProjectIconsOnMap = function () {
    var self = this;

    $.each(self.icons, function (i, item) {
        item.setMap(null);
    });
    self.icons = [];
};

/**
 * Prepare icon url for project marker
 * @returns {string}
 */
MapComponent.getIconUrl = function (item) {
    var projectIcon_color, projectIcon_smile;

    var week_start = (typeof BctProjects.week_start !== 'undefined') ? BctProjects.week_start : 4,
        week_end = (typeof BctProjects.week_end !== 'undefined') ? BctProjects.week_end : 8,
        union_start = (typeof BctProjects.union_start !== 'undefined') ? BctProjects.union_start : 50,
        union_end = (typeof BctProjects.union_end !== 'undefined') ? BctProjects.union_end : 75;

    if (parseInt(item.weeks) < week_start) {
        projectIcon_color = (typeof BctProjects.color_less_4 !== 'undefined') ? BctProjects.color_less_4 : 'green';
    } else if (parseInt(item.weeks) >= week_start && parseInt(item.weeks) < week_end) {
        projectIcon_color = (typeof BctProjects.color_4_8 !== 'undefined') ? BctProjects.color_4_8 : 'yellow';
    } else {
        projectIcon_color = (typeof BctProjects.color_more_8 !== 'undefined') ? BctProjects.color_more_8 : 'red';
    }

    if (parseInt(item.union_percent) < union_start) {
        projectIcon_smile = (typeof BctProjects.union_1 !== 'undefined') ? BctProjects.union_1 : 'sad';
    } else if (parseInt(item.union_percent) >= union_start && parseInt(item.union_percent) < union_end) {
        projectIcon_smile = (typeof BctProjects.union_2 !== 'undefined') ? BctProjects.union_2 : 'neutral';
    } else {
        projectIcon_smile = (typeof BctProjects.union_3 !== 'undefined') ? BctProjects.union_3 : 'smile';
    }

    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }

    return window.location.origin + "/ui/api/icon?w=32&h=32&icon=" + projectIcon_smile + "&c=" + projectIcon_color;
};

/**
 * Prepare window info
 * @param item
 * @returns {google.maps.InfoWindow}
 */
MapComponent.getWindowInfo = function (item) {
    var fullAddress = [item.address, item.city, item.state, item.zipcode].join(' ');

    var contentString =
        '<div id="content">' +
        '<div id="bodyContent">' +
        '<p><b>Name: </b>' + item.name + '</p>';

    contentString += '<p><b>Address: </b>' + fullAddress + '</p>';

    if (item.follow_up_date_custom != null && item.follow_up_date_custom != '') {
        contentString += '<p><b>Follow-Up: </b>' + item.follow_up_date_custom + '</p>';
    }

    if (item.union_percent !== null && item.union_percent !== '') {
        contentString += '<p><b>Percent Union: </b>' + item.union_percent + '</p>';
    }

    if (item.description != null && item.description != '') {
        contentString += '<p><b>Description: </b>' + item.description + '</p>';
    }

    if (item.details_url != null && item.details_url != '') {
        contentString += '<p><a class=\"label label-primary\" href="' + item.details_url + '">Details</a></p>';
    }

    contentString += '</div></div>';

    return new google.maps.InfoWindow({
        content: contentString
    });
};

/**------------------
 * Grid
 * ------------------
 */

MapExtension = Object.create(ExtensionBase);
MapExtension.Index = $.extend({}, ExtensionBase.Index, {
    grid: 'projects',
    checkedRows: [],
    checkedRowsLimit: 8,
});

// Initialize Grid
MapExtension.Index.init = function () {
    this.datagrid();
    this.listeners();
};

// Add Grid Listeners
MapExtension.Index.listeners = function () {
    Platform.Cache.$body
        .on('click', '[data-grid-row]', MapExtension.Index.checkRow)
        .on('click', '[data-grid-row] a', MapExtension.Index.titleClick)
        .on('change', '#list_id', MapExtension.Index.selectedList)
        .on('change', '#group_id', MapExtension.Index.getLists)
        .on('change', '#group_type', MapExtension.Index.getGroups)
        .on('click', '#modal-confirm a.confirm', MapExtension.Index.bulkActions)
        .on('click', '#add-to-list', MapExtension.Index.bulkActions)
        .on('click', '[data-grid-bulk-action]:not([data-grid-bulk-action="delete"])', MapExtension.Index.bulkActions)

};

// Ignore row selection on title click
MapExtension.Index.titleClick = function (event) {
    event.stopPropagation();
};

// Handle Data Grid row checking
MapExtension.Index.checkRow = function () {
    var checkbox = $(this).find('[data-grid-checkbox]');

    if (checkbox.is(':disabled')) return false;

    $(this).toggleClass('active');

    checkbox.prop('checked', !checkbox.prop('checked'));

    MapExtension.Index.rememberChecked(checkbox);
    MapExtension.Index.checkProjectsAvailable();
    MapExtension.Index.bulkStatus();

};

MapExtension.Index.rememberChecked = function (checkbox) {
    if (checkbox.prop('checked')) {
        if (MapExtension.Index.checkedRows.indexOf(checkbox.data('checkbox-id')) == -1) {
            MapExtension.Index.checkedRows.push(checkbox.data('checkbox-id'));
        }
    } else {
        var ind = MapExtension.Index.checkedRows.indexOf(checkbox.data('checkbox-id'));
        if (ind != -1) {
            MapExtension.Index.checkedRows.splice(ind, 1);
        }
    }
};

MapExtension.Index.selectedList = function () {
    if ($('#list_id').val().length > 0 && $('#group_id').val().length > 0) {
        $('#add-to-list').removeClass('disabled');
    }
    else {
        $('#add-to-list').addClass('disabled');
    }

};

MapExtension.Index.getGroups = function () {
    var group_type = $('#group_type').val();
    if (group_type.length > 0) {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var url = window.location.origin + "/pprofiles/projects/get-groups/" + group_type;
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                group_type: group_type
            },
            success: function (response) {
                var items = response.lists;
                $('#group_id')
                    .find('option:gt(0)')
                    .remove()
                    .end()
                    .val('');
                $.each(items, function (i, item) {
                    $('#group_id').append($('<option>', {
                        value: item.id,
                        text: item.name
                    }));
                });
                $("#group_id").removeAttr('disabled');
            }
        });
    }
    else {
        $('#group_id')
            .find('option:gt(0)')
            .remove()
            .end()
            .val('')
    }

};

MapExtension.Index.getLists = function () {
    var group_id = $('#group_id').val();
    if (group_id.length > 0) {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var url = window.location.origin + "/pprofiles/projects/get-lists/" + group_id;
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                group_id: group_id
            },
            success: function (response) {
                var items = response.lists;
                $('#list_id')
                    .find('option:gt(0)')
                    .remove()
                    .end()
                    .val('');
                $.each(items, function (i, item) {
                    $('#list_id').append($('<option>', {
                        value: item.id,
                        text: item.name
                    }));
                });
                $("#list_id").removeAttr('disabled');

            }
        });
    }
    else {
        $('#list_id')
            .find('option:gt(0)')
            .remove()
            .end()
            .val('')
    }

};


MapExtension.Index.bulkStatus = function () {
    var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

    var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;
    var list_modal_href = $('#lists-modal').data('href');

    $('[data-grid-bulk-action]').closest('li').toggleClass('disabled', !checked);
    $('[data-grid-bulk-action]').toggleClass('disabled', !checked);

    if (checked > 0) {

        if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
            $('#lists-modal').attr('href', list_modal_href + '&number=' + $('[data-grid-checkbox="all"]').data('all-count')).removeClass('disabled');
        } else {
            $('#lists-modal').attr('href', list_modal_href + '&number=' + checked).removeClass('disabled');
        }

    } else {
        $('#lists-modal').attr('href', list_modal_href).addClass('disabled');
    }

    $('[data-grid-checkbox="all"]')
        .prop('disabled', rows < 1)
    ;

};

// Handle Data Grid bulk actions
MapExtension.Index.bulkActions = function (event) {
    event.preventDefault();
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    var url = window.location.origin + window.location.pathname;

    var action = $(this).data('grid-bulk-action') ? $(this).data('grid-bulk-action') : 'delete';

    var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function (event) {
        return +event.value;
    });

    if (rows.length > 0) {
        url = window.location.origin + "/pprofiles/projects/check";
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                rows: rows,
                list_id: $("#list_id").val()
            },
            success: function (response) {
                if (response == 'success') {
                    var add_url = window.location.origin + '/pprofiles/projects';

                    if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
                        rows = 'all';
                    }

                    $.ajax({
                        type: 'POST',
                        url: add_url,
                        data: {
                            action: "storeLists",
                            rows: rows,
                            list_id: $("#list_id").val()
                        },
                        success: function (response) {
                            MapExtension.Index.Grid.refresh();
                            $('#modal-form').modal('hide');
                        }
                    });
                } else {
                    swal({
                        title: "Some projects cannot be added to list because they already in the list!",
                        text: 'Do you want to continue?',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#019964",
                        confirmButtonText: "Yes!",
                        cancelButtonText: "No",
                        showLoaderOnConfirm: true,
                        preConfirm: function () {
                            return new Promise(function (resolve, reject) {
                                var add_url = window.location.origin + '/pprofiles/projects';

                                if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
                                    rows = 'all';
                                }

                                $.ajax({
                                    type: 'POST',
                                    url: add_url,
                                    data: {
                                        action: "storeLists",
                                        rows: rows,
                                        list_id: $("#list_id").val()
                                    }
                                }).done(function () {
                                    resolve();
                                }).fail(function () {
                                    reject("Error!");
                                });
                            });
                        }
                    }).then(function () {
                        MapExtension.Index.Grid.refresh();
                        $('#modal-form').modal('hide');

                    }, function () {
                        MapExtension.Index.Grid.refresh();
                        $('#modal-form').modal('hide');

                    });
                }
            },
        });

    }
};

MapExtension.Index.checkProjectsAvailable = function () {
    if (MapExtension.Index.checkedRows.length > 0) {
        $('#map-submit-btn').fadeIn();
    } else {
        $('#map-submit-btn').fadeOut();
    }

    /** disable/enable for limit projects */
    if (MapExtension.Index.checkedRows.length >= MapExtension.Index.checkedRowsLimit) {
        $('[data-grid-checkbox]:not(:checked)').attr('disabled', true);
    } else {
        $('[data-grid-checkbox]:not(:checked)').attr('disabled', false);
    }
};

MapExtension.Index.applyRememberChecked = function () {
    $.each(MapExtension.Index.checkedRows, function (index, value) {
        $('[data-grid-checkbox][value="' + value + '"]').prop('checked', true);
    });
};


// Data Grid initialization
MapExtension.Index.datagrid = function () {
    var config = {
        cache_response: true,
        events: {
            fetching: function (dg) {
                dg.source = dg.source.split('?')[0];
                if (MapComponent.currentLat && MapComponent.currentLng && MapComponent.currentRadius) {
                    dg.source += '?' + $.param({
                            lat: MapComponent.currentLat,
                            lng: MapComponent.currentLng,
                            radius: MapComponent.currentRadius,
                            count_projects: MapComponent.currentProjectLimit,
                            status: MapComponent.currentStatus,
                            start_date: MapComponent.currentStartDate,
                            end_date: MapComponent.currentEndDate,
                            contractor: MapComponent.currentContractor,
                            tags: MapComponent.currentTags,
                            stage: MapComponent.currentStage,
                            region: MapComponent.currentRegion,
                        });
                } else {
                    dg.source += '?hide=1';
                }
            },
            sorted: function (response) {
                if (response.pagination.total == 0) {
                    $('.download-container').addClass('hide');
                }

                MapExtension.Index.checkProjectsAvailable();
            },
            fetched: function (response) {
                if (response.pagination.filtered == 0) {
                    $('.download-container').addClass('hide');
                } else {
                    $('.download-container').removeClass('hide');
                }

                MapExtension.Index.checkProjectsAvailable();
            },
        },
        callback: function () {
            // after everything is rendered, apply any remember checkboxes on grid
            MapExtension.Index.applyRememberChecked();
        }
    };

    MapExtension.Index.Grid = $.datagrid('projects', '#data-grid', '#data-grid_pagination', '#data-grid_applied', config);
};