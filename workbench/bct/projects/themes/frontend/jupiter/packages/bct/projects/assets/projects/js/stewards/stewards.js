/**
 * Created by user on 30/06/15.
 */
var AllStewardsExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    AllStewardsExtension = Object.create(ExtensionBase);
    AllStewardsExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'stewards'
    });

    // Initialize functions
    AllStewardsExtension.Index.init = function () {
        AllStewardsExtension.Index.dataGrid();
        AllStewardsExtension.Index.listeners();
    };

    // Add Listeners
    AllStewardsExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', AllStewardsExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    AllStewardsExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    // Data Grid initialization
    AllStewardsExtension.Index.dataGrid = function () {
        var config = {
            scroll: '#data-grid',
            events: {
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
            callback: function () {
            }
        };

        AllStewardsExtension.Index.Grid = $.datagrid('stewards', '#stewards-data-grid', '.stewards-data-grid_pagination', '#stewards-data-grid_applied', config);
    };

    // Job done, lets run.
    AllStewardsExtension.Index.init();

})(window, document, jQuery);


$('#stewards-add-btn').on('click', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modalAjax = $('#modal-form-ajax');

    $modalAjax.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (response) {
            $modalAjax.modal('show');

            initAjaxModal();

            $('#modal-form-ajax').on('click', '#submit-btn', function (e) {
                e.preventDefault();

                var form_modal = $modalAjax.find('#content-form');

                swal({
                    title: "Are you sure you want to save?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#019964",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve, reject) {
                            $.post(
                                form_modal.attr('action'),
                                form_modal.serialize(),
                                function (data) {
                                    if (typeof data.success == 'undefined') {
                                        $modalAjax.find('.modal-content').html(data);
                                        initAjaxModal();
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        });
                    }
                }).then(function () {
                    $modalAjax.modal('hide');
                    AllStewardsExtension.Index.Grid.refresh();
                });
            });
        } else {
            location.reload();
        }
    });

    function initAjaxModal() {
        $modalAjax.find('[data-mask]').mask('(999) 999-9999');
    }
});

// Project Steward Data Grid
$('#stewards-data-grid').on('click', '.add-confirm', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $this = $(this);

    swal({
        title: "Are you sure you want to add this steward?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes!",
        cancelButtonText: "No",
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(
                    $this.attr('href'), {},
                    function (data) {
                        ajaxBtnStatus(sourceBtn, 'fa-plus');

                        if (data == 'success') {
                            resolve();
                        } else {
                            reject('Error!');
                        }
                    }
                );
            });
        }
    }).then(function () {
        swal('Steward Added!', '', 'success');
        ProjectStewardExtension.Index.Grid.refresh();

    }, function () {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

    });
});
