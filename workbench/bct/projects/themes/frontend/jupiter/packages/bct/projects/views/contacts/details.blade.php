@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::contacts/common.title') }}
@stop
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}

{{ Asset::queue('index', 'bct/projects::contacts/js/details.js', 'extension-base') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-tabs">

        <header class="panel-heading hidden-print">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{ route('bct.projects.contacts.all') }}"
                                   data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                                    <i class="fa fa-reply"></i> <span
                                            class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                </a>
                            </li>
                        </ul>

                        <span class="navbar-brand">{{ $contact->full_name }} {{ trans("bct/projects::general/common.details") }} | ID:{{ data_get($contact, 'id', '') }}</span>
                    </div>
                    @if (!$currentUser->inRole('pp-guest'))

                    <div class="pull-right">

                        <a href="{{ route('bct.projects.contacts.edit', $contact->id) }}"
                           class="btn btn-primary"
                           data-toggle="tooltip"
                           id="contact_edit"
                           name="contact_edit"
                           data-original-title="{{{ trans('bct/projects::general/common.edit') }}}"><i
                                    class="fa fa-edit fa-fw"></i></a>
                    </div>
                    @endif

                    <div class="pull-right" style="margin: 1px 10px 0 0">
                        <a href="{{ route('bct.projects.contacts.pdf.details', $contact->id) }}"
                                class="btn btn-link pull-right"
                                data-toggle="tooltip"
                                data-original-title="{{{ trans('bct/projects::general/common.print') }}}">
                            <i class="fa fa-print fa-fw"></i>
                        </a>
                    </div>



                </div>

            </nav>

        </header>

        <div class="panel-body">
            <div class="row visible-print">
                <h4 class="overview-heading text-center visible-print">
                    {{ data_get($contact, 'first_name', '') . ' ' . data_get($contact, 'last_name', '') }} -
                    {{ trans("bct/projects::contacts/common.title") }} {{ trans("bct/projects::general/common.details") }}
                </h4>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <input type="hidden" id="id" name="id" value="{{ $contact->id }}"/>

                    <table class="table table-bordered table-details">
                        <tbody>
                        <tr>
                            <td class="td-value">
                                <div>

                                    <div class="col-md-6 col-sm-12 border-right col-panel">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>{{ trans('bct/projects::contacts/model.general.name') }}:</b>
                                                <span class="pre-spaces">{{ $contact->full_name }}</span>
                                            </div>

                                            <div class="col-md-6">
                                                <b>{{ trans('bct/projects::general/common.updated') }}:</b>
                                                <span class="pre-spaces">{{ date_correct_details_projects($contact->updated_at) }}</span>
                                                @if($contact->active == 1)
                                                    <span class="label label-success">{{ trans('bct/projects::general/common.active') }}</span>
                                                @else
                                                    <span class="label label-danger">{{ trans('bct/projects::general/common.disabled') }}</span>

                                                @endif
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <b>{{ trans('bct/projects::contacts/model.general.address') }}:</b>
                                                <span class="pre-spaces">{{ $contact->location }}</span>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <b>{{ trans('bct/projects::contacts/model.general.phone') }}:</b>
                                                <span class="pre-spaces"><a class="under-none" href="tel:{{ $contact->phone }}">{{ $contact->phone }}</a></span>
                                            </div>

                                            <div class="col-md-6">
                                                <b>{{ trans('bct/projects::contacts/model.general.phone2') }}:</b>
                                                <span class="pre-spaces"><a class="under-none" href="tel:{{ $contact->phone2 }}">{{ $contact->phone2 }}</a></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-sm-12 col-panel">

                                        <div class="row">

                                            <div class="col-md-6">
                                                <b>{{ trans('bct/projects::contacts/model.general.company') }}:</b>
                                                <span class="pre-spaces">{{ $contact->company }}</span>
                                            </div>

                                            <div class="col-md-6">
                                                <b>{{ trans('bct/projects::contacts/model.general.title') }}:</b>
                                                <span class="pre-spaces">{{ $contact->title }}</span>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <b>{{ trans('bct/projects::contacts/model.general.email') }}:</b>
                                                <span class="pre-spaces"><a class="under-none" href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></span>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <b>{{ trans('bct/projects::contacts/model.general.notes') }}:</b>
                                                <span class="pre-spaces">{{ $contact->notes_short }}</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </td>
                        </tr>
                        </tbody>
                    </table>

            </div>
        </div>

        <div>

            <div class="row">

                <div class="col-md-12">

                    <section class="panel panel-default panel-grid">

                        {{-- Grid: Header --}}
                        <header class="panel-heading hidden-print">

                            <nav class="navbar navbar-default navbar-actions">

                                <div class="container-fluid">

                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                data-target="#actions">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>

                                        <span class="navbar-brand">{{ trans('bct/projects::projects/common.title') }}</span>

                                    </div>

                                    {{-- Grid: Actions --}}
                                    <div class="collapse navbar-collapse" id="actions">

                                        <ul class="nav navbar-nav navbar-left download-container">

                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown"
                                                   role="button" aria-expanded="false"
                                                   data-original-title="{{{ trans('action.export') }}}">
                                                    <i class="fa fa-download"></i> <span
                                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i>
                                                            CSV</a></li>
                                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        </ul>

                                        {{-- Grid: Filters --}}
                                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8"
                                              data-search data-grid="projects" role="form">

                                            <div class="input-group">

                                            <span class="input-group-btn">

                                                <button class="btn btn-default" type="button" disabled>
                                                    {{{ trans('common.status') }}}
                                                </button>

                                                <button class="btn btn-default dropdown-toggle" type="button"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>

                                                <ul class="dropdown-menu" role="menu">
                                                    @foreach(\Bct\Projects\Models\Status::all() as $status)
                                                        <li>
                                                            <a data-filter="z_project_status.name:{{$status->name}}" href="#" data-reset>
                                                                <i class="fa @if($status->name == 'Completed') fa-times-circle @else fa-check-circle @endif"></i> {{$status->name}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>

                                                <button class="btn btn-default hidden-xs" type="button"
                                                        data-grid-calendar data-range-filter="z_projects.created_at">
                                                    <i class="fa fa-calendar"></i>
                                                </button>

                                            </span>
                                             <span class="input-group-btn">
                                                   <select name="column" class="form-control">
                                                       <option value="z_projects.name">{{ trans('bct/projects::contacts/model.general.name') }}
                                                       </option>
                                                       <option value="z_projects.city">{{ trans('bct/projects::projects/model.general.city') }}
                                                       </option>
                                                       <option value="z_projects.state">{{ trans('bct/projects::projects/model.general.state') }}
                                                       </option>
                                                       <option value="z_contact_type.name">{{ trans('bct/projects::projects/model.general.contact_type') }}
                                                       </option>
                                                   </select>
                                               </span>
                                                <input class="form-control" name="filter" type="text"
                                                       placeholder="{{{ trans('common.search') }}}">

                                            <span class="input-group-btn">

                                                <button class="btn btn-default" type="submit">
                                                    <span class="fa fa-search"></span>
                                                </button>

                                                <button class="btn btn-default" data-grid="projects" data-reset>
                                                    <i class="fa fa-refresh fa-sm"></i>
                                                </button>

                                            </span>

                                            </div>

                                            <input type="hidden" data-range-filter="z_projects.created_at" data-format="YYYY-MM-DD" data-range-start></button>
                                            <input type="hidden" data-range-filter="z_projects.created_at" data-format="YYYY-MM-DD" data-range-end></button>

                                        </form>

                                    </div>

                                </div>

                            </nav>

                        </header>

                        <div class="panel-body">

                            <div class="row visible-print">
                                <div class="col-md-12">
                                    <span class="lead">{{ trans('bct/projects::contacts/model.general.projects_belong') }}
                                        :</span>
                                </div>
                            </div>

                            {{-- Grid: Applied Filters --}}
                            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                                <div id="data-grid_applied" class="btn-group" data-grid="projects"></div>

                            </div>

                            {{-- Grid: Table --}}
                            <div class="table-responsive">

                                <table id="data-grid" class="table table-hover"
                                       data-source="{{ route('bct.projects.contacts.gridProjects', $contact->id) }}"
                                       data-filename="Projects for Contact - {{ $contact->full_name  }}"
                                       data-grid="projects">
                                    <thead>
                                    <tr>
                                        <th class="sortable"
                                            data-sort="name">{{ trans('bct/projects::projects/model.general.name') }}
                                        </th>
                                        <th class="sortable"
                                            data-sort="city">{{ trans('bct/projects::projects/model.general.city') }}
                                        </th>
                                        <th class="sortable"
                                            data-sort="state">{{ trans('bct/projects::projects/model.general.state') }}
                                        </th>
                                        <th class="sortable"
                                            data-sort="created_at">{{ trans('bct/projects::projects/model.general.created_at') }}
                                        </th>
                                        <th>{{ trans('bct/projects::projects/model.general.status_id') }}
                                        </th>
                                        <th>{{ trans('bct/projects::projects/model.general.contact_type') }}
                                        </th>
                                        <th class="hidden-print">{{{ trans('bct/projects::general/common.details') }}}</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                            </div>

                        </div>

                        <footer class="panel-footer clearfix hidden-print">

                            {{-- Grid: Pagination --}}
                            <div class="data-grid_pagination" data-grid="projects"></div>

                        </footer>

                        {{-- Grid: templates --}}
                        @include('bct/projects::contacts.grid.projects.results')
                        @include('bct/projects::contacts.grid.projects.pagination')
                        @include('bct/projects::contacts.grid.projects.filters')
                        @include('bct/projects::contacts.grid.projects.no_results')

                    </section>

                </div>

            </div>

        </div>

    </section>
@stop
