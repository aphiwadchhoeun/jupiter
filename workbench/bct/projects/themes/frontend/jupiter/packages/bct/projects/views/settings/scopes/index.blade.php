@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::scopes/common.title') }}
@stop

{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('index', 'bct/projects::settings/scopes/js/index.js', 'extension-base') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::scopes/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="scopes" role="form">

                            <div class="input-group">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::scopes/model.general.classification') }}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                        @foreach($classifications as $cl)
                                            <li>
                                                <a href="#" data-filter="classification:{{ $cl->name }}">
                                                    {{ $cl->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </span>

                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="name">{{ trans('bct/projects::scopes/model.general.name') }}</option>
                                        <option value="value">{{ trans('bct/projects::scopes/model.general.value') }}</option>
                                    </select>
                                </span>



                                <span class="input-group-btn">

                                    <input class="form-control" name="filter" type="text"
                                           placeholder="{{{ trans('common.search') }}}">

                                    <button class="btn btn-default" type="submit" >
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="scopes" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>

                        </form>


                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="scopes"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.scopes.grid') }}"
                       data-filename="{{ trans('bct/projects::scopes/common.title') }}"
                       data-grid="scopes">
                    <thead>
                    <tr>
                        <th class="sortable col-md-5" data-sort="name">{{{ trans('bct/projects::scopes/model.general.name') }}}</th>
                        <th class="sortable col-md-1" data-sort="value">{{{ trans('bct/projects::scopes/model.general.value') }}}
                        </th>
                        <th class="sortable col-md-3" data-sort="classification">{{{ trans('bct/projects::scopes/model.general.classification') }}}
                        </th>
                        <th class="sortable col-md-2" data-sort="order">{{{ trans('bct/projects::scopes/model.general.order') }}}</th>
                        <th>{{{ trans('bct/projects::general/common.details') }}}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="scopes"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::settings.scopes.grid.index.results')
        @include('bct/projects::settings.scopes.grid.index.pagination')
        @include('bct/projects::settings.scopes.grid.index.filters')
        @include('bct/projects::settings.scopes.grid.index.no_results')

    </section>

@stop
