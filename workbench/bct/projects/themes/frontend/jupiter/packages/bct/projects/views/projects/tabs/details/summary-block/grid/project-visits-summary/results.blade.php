<script type="text/template" data-grid="project-visits-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.users %></td>
        <td><%- r.details %></td>
        <td><%- r.stages %></td>
        <td><%- r['created'] %></td>
    </tr>

    <% }); %>

</script>
