<script type="text/template" data-grid="project-contractor" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.id %></td>
        <td><%- r.scope %> (<%- r.value %>)</td>
        <td>
            <% if (r.id !== null) { %>
                <a href="<%- r.edit_uri %>"><%- r.name %></a>
            <% } else { %>
                <%- r.name %>
            <% } %>
        </td>
        <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
        <td>
            <span class="label label-<%- r.union_color %>"><%- r.union_text %></span>
        </td>
        <td>
            <% if (r.has_agreement == true) { %>
            <span class="label label-success">Yes</span>
            <% } else { %>
            <span class="label label-danger">No</span>
            <% } %>
        </td>
        @if (!$currentUser->inRole('pp-guest'))
        <td class="hidden-print"><a href="<%- r.view_uri %>" data-modal
                id="contractor_edit"
                name="contractor_edit"
                data-toggle="tooltip" class="btn btn-primary btn-circle" data-placement="right"
                data-original-title="{{{ trans('bct/projects::general/common.edit') }}}">
                <i class="fa fa-book fa-fw"></i></a></td>
         @endif
    </tr>

    <% }); %>

</script>
