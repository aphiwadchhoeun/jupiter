<script type="text/template" data-grid="projects" data-template="filters">

	<% _.each(filters, function(f) { %>

	<button class="btn btn-default">

		<% if (f.from !== undefined && f.to !== undefined) { %>

		<% if (/[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.from) && /[0-9]{4}-[0-9]{2}-[0-9]{2}/g.test(f.to)) { %>

		<%- f.label %> <em><%- moment(f.from).format('MMM DD, YYYY') %> - <%- moment(f.to).format('MMM DD, YYYY') %></em>

		<% } else { %>

		<%- f.label %> <em><%- f.from %> - <%- f.to %></em>

		<% } %>

		<% } else if (f.col_mask !== undefined && f.val_mask !== undefined) { %>

		<%- f.col_mask %> <em><%- f.val_mask %></em>

		<% } else { %>

		<% if (f.column === 'all') { %>

		<%- f.value %>

		<% } else if (f.column === 'z_project_status.name') { %>

		<%- f.value %>

		<% } else if (f.column === 'z_projects.id') { %>

		<%- f.value %> in <em>{{ trans('bct/projects::general/common.id') }}</em>

		<% } else if (f.column === 'z_projects.name') { %>

		<%- f.value %> in <em>{{ trans('bct/projects::projects/model.general.name') }}</em>

		<% } else if (f.column === 'z_projects.description') { %>

		<%- f.value %> in <em>{{ trans('bct/projects::projects/model.general.description') }}</em>

		<% } else if (f.column === 'z_projects.city') { %>

		<%- f.value %> in <em>{{ trans('bct/projects::projects/model.general.city') }}</em>

		<% } else if (f.column === 'region_abrv') { %>

		<%- f.value %> in <em>{{ trans('bct/projects::projects/model.general.region') }}</em>


		<% } else if (f.column === 'tags.name') { %>


		<%- f.value %> in <em>{{ trans('bct/projects::tags/common.tags') }}</em>


		<% } else { %>

		<%- f.value %> in <em><%- f.column.capitalize() %></em>

		<% } %>

		<% } %>

		&nbsp;<span><i class="fa fa-times-circle"></i></span>

	</button>

	<% }); %>

</script>
