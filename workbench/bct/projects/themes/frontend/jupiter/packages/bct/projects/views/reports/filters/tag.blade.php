<div class="input-group select-width">
    <span class="input-group-btn">
        <button class="btn btn-default" type="button" disabled>
            {{ trans('bct/projects::reports/common.placeholder.tag') }}
        </button>
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul name="column" class="dropdown-menu tags" role="menu" data-filter-reset>
            @foreach($tags as $tag)
                <li><a href="#" data-filter="tag:{{$tag->id}}" value="{{$tag->id}}">{{$tag->name}}</a></li>
            @endforeach
        </ul>
    </span>
</div>