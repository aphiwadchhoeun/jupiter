{{ Asset::queue('all-contractors-grid', 'bct/projects::projects/js/contractors/contractors.js', 'extension-base') }}

<section class="panel panel-default panel-grid">
    {{-- Grid --}}

    {{-- Grid: Header --}}
    <header class="panel-heading">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#contractors-all-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::contractors/common.available_contractors') }}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="contractors-all-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="contractors-all-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="contractors" role="form">

                        <div class="input-group">

                            <span class="input-group-btn">
                                <select name="column" class="form-control" id="project-available-contractors-filter-select">

                                    <option value="name">{{ trans('bct/projects::contractors/model.general.name') }}
                                    </option>
                                    <option value="city">{{ trans('bct/projects::contractors/model.general.city') }}
                                    </option>
                                    <option value="state">{{ trans('bct/projects::contractors/model.general.state') }}
                                    </option>
                                    <option value="phone">{{ trans('bct/projects::contractors/model.general.phone') }}
                                    </option>
                                    <option value="license">{{ trans('bct/projects::contractors/model.general.license') }}
                                    </option>
                                    <option value="id">{{ trans('bct/projects::contractors/model.general.id') }}
                                    </option>
                                </select>
                            </span>

                            <input class="form-control" name="filter" type="text" id="project-available-contractors-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="project-available-contractors-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="contractors" data-reset id="project-available-contractors-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                        </div>

                    </form>

                    <div class="nav navbar-nav navbar-right">

                        <a href="{{ route('bct.projects.contractors.create') }}"
                           id="contractors-add-btn"
                           name="contractors-add-btn"
                            data-modal-ajax
                           class="btn btn-success pull-right margin-right5" data-toggle="tooltip"
                           id="project-available-contractors-btn-create"
                           data-original-title="{{{ trans('action.create') }}}">
                           {{ trans('bct/projects::contractors/common.create_contractor') }}
                        </a>

                    </div>

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="contractors-data-grid-applied-filters">

            <div id="contractors-data-grid_applied" class="btn-group" data-grid="contractors"></div>

        </div>


        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="contractors-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridAllContractors', $project->id) }}"
                   data-filename="{{ trans('bct/projects::contractors/common.title') }}"
                   data-grid="contractors">
                <thead>
                <tr>
                    <th class="sortable col-md-1"
                        data-sort="id">{{{ trans('bct/projects::contractors/model.general.id') }}}
                    </th>
                    <th class="sortable col-md-5"
                        data-sort="name">{{{ trans('bct/projects::contractors/model.general.name') }}}
                    </th>
                    <th class="sortable col-md-2"
                        data-sort="city">{{{ trans('bct/projects::contractors/model.general.city') }}}
                    </th>
                    <th class="sortable col-md-1"
                        data-sort="state">{{{ trans('bct/projects::contractors/model.general.state') }}}
                    </th>
                    <th class="sortable col-md-2"
                        data-sort="phone">{{{ trans('bct/projects::contractors/model.general.phone') }}}
                    </th>
                    <th class="sortable col-md-2"
                        data-sort="license">{{{ trans('bct/projects::contractors/model.general.license_number') }}}
                    </th>
                    <th class="col-md-1">{{{ trans('bct/projects::general/common.actions') }}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>


    <footer class="panel-footer clearfix">

        {{-- Grid: Pagination --}}
        <div class="contractors-data-grid_pagination" data-grid="contractors"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.contractors.grid.contractors.results')
    @include('bct/projects::projects.tabs.contractors.grid.contractors.pagination')
    @include('bct/projects::projects.tabs.contractors.grid.contractors.filters')
    @include('bct/projects::projects.tabs.contractors.grid.contractors.no_results')

</section>