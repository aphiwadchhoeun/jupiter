<script type="text/template" data-grid="contractors" data-template="no_results">

	<tr>
		<td class="no-results text-center" colspan="8">{{{ trans('common.no_results') }}}</td>
	</tr>

</script>
