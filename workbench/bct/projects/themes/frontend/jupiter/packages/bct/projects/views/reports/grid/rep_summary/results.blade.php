<script type="text/template" data-grid="rep_summary" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>

            <td><%- r.repname %></td>
            <td><%- r.role %></td>
            <td><%- r.projects_created %></td>
            <td><%- r.projects_visited %></td>
            <td><%- r.total_visits %></td>
		</tr>

	<% }); %>

</script>
