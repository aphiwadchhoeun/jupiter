@extends('bct/projects::default')


{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::reports/common.tabs.wage_summary') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}

{{ Asset::queue('index', 'bct/projects::reports/js/wage_summary.js', 'extension-base') }}


{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')



    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{route('bct.projects.reports.index')}}" data-toggle="tooltip" data-original-title="Cancel">
                                    <i class="fa fa-reply"></i> <span class="visible-xs-inline">Cancel</span>
                                </a>
                            </li>
                        </ul>

                        <span class="navbar-brand">{{{ trans('bct/projects::reports/common.tabs.wage_summary') }}}</span>

                    </div>

                </div>

            </nav>

        </header>
        <div class="panel-body report-filters">
            @include('bct/projects::reports.filters.export')

            {{-- Grid: Filters --}}
            <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                  data-grid="wage_summary" role="form">

                @include('bct/projects::reports.filters.date')
                @include('bct/projects::reports.filters.region')
                @include('bct/projects::reports.filters.contractor')
                @include('bct/projects::reports.filters.tag')
                @include('bct/projects::reports.filters.project_type')
                @include('bct/projects::reports.filters.reset')


            </form>

        </div>
        <hr/>
        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="wage_summary"></div>

            </div>

            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.reports.wage_summary.grid') }}"
                       data-filename="{{ trans('bct/projects::reports/common.tabs.wage_summary') }}"
                       data-grid="wage_summary">
                    <thead>
                    <tr>
                        <th class="sortable col-md-10" data-sort="wage_type">{{ trans('bct/projects::reports/common.col.wage_type') }}</th>
                        <th class="sortable col-md-" data-sort="projects_total">{{ trans('bct/projects::reports/common.col.project_count') }}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="wage_summary"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::reports.grid.wage_summary.results')
        @include('bct/projects::reports.grid.wage_summary.pagination')
        @include('bct/projects::reports.grid.wage_summary.filters')
        @include('bct/projects::reports.grid.wage_summary.no_results')

    </section>


@stop
