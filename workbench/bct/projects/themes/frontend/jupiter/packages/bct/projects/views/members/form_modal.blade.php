@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans("action.{$mode}") }}} {{ trans('bct/projects::members/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
{{ Asset::queue('contact-form', 'bct/projects::members/js/form.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default">

        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-refresh="members">

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip"
                                       @if ($mode === 'create')
                                        href="{{ route('bct.projects.members.all') }}"
                                       @else
                                        href="{{ route('bct.projects.members.details', $contact->id) }}"
                                       @endif

                                       data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{ trans("bct/projects::members/common.title") }} {{ trans("action.{$mode}") }}</span>
                        </div>

                        <div class="pull-right">
                            @if ($contact->exists)
                                <a href="{{ route('bct.projects.members.delete', $contact->id) }}"
                                   class="btn btn-danger"
                                   id="contact_delete"
                                   name="contact_delete"
                                   data-action-delete data-toggle="tooltip"
                                   data-original-title="{{{ trans('action.delete') }}}" type="delete">
                                    <i class="fa fa-trash-o fa-fw"></i></a>
                            @endif

                            <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                    data-original-title="{{{ trans('action.save') }}}">
                                <i class="fa fa-save fa-fw"></i></button>
                        </div>

                    </div>

                </nav>

            </header>

            @include('bct/projects::members._form')

        </form>

    </section>
@stop
