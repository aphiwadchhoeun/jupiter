<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{{ trans("bct/projects::scopes/common.title") }}} {{{ trans("action.{$mode}") }}}</h4>
</div>
<div class="modal-body">
    {{-- Form --}}
    <form id="content-form-modal" action="
        @if ($scope->exists)
            {{ route('bct.projects.scopes.edit', $scope->id) }}
        @else
    {{ route('bct.projects.scopes.create') }}
        @endif
            " method="post" data-refresh="scopes">

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="panel-body">

            <div class="col-md-12">

                {{-- Form: General --}}
                <fieldset>
                    <div class="row">

                        {{-- Form:name --}}
                        <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                            <label for="name" class="control-label">
                                {{{ trans('bct/projects::scopes/model.general.name') }}}
                            </label>

                            <input type="text" class="form-control" name="name" id="name" required autofocus
                                   placeholder="{{{ trans('bct/projects::scopes/model.general.name_help') }}}"
                                   value="{{{ input()->old('name', $scope->name) }}}">

                            <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                        </div>

                        {{-- Form:value --}}
                        <div class="form-group{{ Alert::onForm('value', ' has-error') }}">

                            <label for="value" class="control-label">
                                {{{ trans('bct/projects::scopes/model.general.value') }}}
                            </label>

                            <input type="text" class="form-control" name="value" id="value" required autofocus
                                   placeholder="{{{ trans('bct/projects::scopes/model.general.value_help') }}}"
                                   value="{{{ input()->old('value', $scope->value) }}}">

                            <span class="help-block">{{{ Alert::onForm('value') }}}</span>

                        </div>

                        {{-- Form:order --}}
                        <div class="form-group{{ Alert::onForm('order', ' has-error') }}">

                            <label for="order" class="control-label">
                                {{{ trans('bct/projects::scopes/model.general.order') }}}
                            </label>

                            <input type="text" class="form-control" name="order" id="order" required autofocus
                                   placeholder="{{{ trans('bct/projects::scopes/model.general.order_help') }}}"
                                   value="{{{ input()->old('order', $scope->order) }}}">

                            <span class="help-block">{{{ Alert::onForm('order') }}}</span>

                        </div>

                    </div>

                </fieldset>

            </div>

        </div>
        
    </form>
</div>

<div class="modal-footer">

    <button id="submit-btn-modal" class="btn btn-primary" data-toggle="tooltip"
            data-original-title="{{{ trans('action.save') }}}">
        <i class="fa fa-save fa-fw"></i></button>

    <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>

</div>