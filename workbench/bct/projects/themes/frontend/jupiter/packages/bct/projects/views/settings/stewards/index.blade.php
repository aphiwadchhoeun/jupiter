@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::users/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
    {{ Asset::queue('index', 'bct/projects::settings/stewards/js/index.js', 'extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::stewards/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span
                                            class="visible-xs-inline">{{{ trans('action.export') }}}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="steward" role="form">

                            <div class="input-group">

                                <span class="input-group-btn">

                                    <button class="btn btn-default" type="button" disabled>
                                        {{{ trans('common.filters') }}}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                            aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu" data-filter-reset>
                                        <li>
                                            <a href="#" data-filter="status:Active">
                                                <i class="fa fa-check-circle"></i> Active
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#" data-filter="status:Disabled">
                                                <i class="fa fa-times-circle"></i> Disabled
                                            </a>
                                        </li>
                                    </ul>

                                    <button class="btn btn-default hidden-xs" type="button" data-grid-calendar
                                            data-range-filter="projects_user.created_at">
                                        <i class="fa fa-calendar"></i>
                                    </button>

                                </span>

                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="name">{{ trans('bct/projects::users/model.general.name') }}</option>
                                        <option value="email">{{ trans('bct/projects::users/model.general.email') }}</option>
                                        <option value="phone">{{ trans('bct/projects::users/model.general.phone') }}</option>
                                        <option value="local">{{ trans('bct/projects::users/model.general.local') }}</option>
                                    </select>
                                </span>

                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="user" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>

                        </form>

                        <div class="pull-right">

                            <a href="" class="btn btn-danger disabled" data-toggle="tooltip"
                               data-grid-bulk-action="delete" data-target="modal-confirm"
                               data-original-title="{{{ trans('action.bulk.delete') }}}">
                                <i class="fa fa-trash fa-fw"></i>
                            </a>

                            <a href="{{ route('bct.projects.stewards.create') }}"
                                id="steward_new"
                                name="steward_new"
                               class="btn btn-success" data-toggle="tooltip"
                               data-original-title="{{{ trans('action.create') }}}">
                                <i class="fa fa-plus fa-fw"></i>
                            </a>

                        </div>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="steward"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.stewards.grid') }}"
                       data-grid="steward">
                    <thead>
                    <tr>
                        <th><input data-grid-checkbox="all" type="checkbox"></th>
                        <th class="sortable" data-sort="name">{{ trans('bct/projects::contacts/model.general.name') }}</th>
                        <th class="sortable"
                            data-sort="company">{{ trans('bct/projects::contacts/model.general.company') }}</th>
                        <th class="sortable"
                            data-sort="phone">{{ trans('bct/projects::contacts/model.general.phone') }}</th>
                        <th class="sortable"
                            data-sort="email">{{ trans('bct/projects::contacts/model.general.email') }}</th>
                        <th class="sortable"
                            data-sort="created">{{{ trans('bct/projects::contacts/model.general.created_at') }}}
                        </th>
                        <th>{{{ trans('bct/projects::general/common.actions') }}}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination clearfix" data-grid="steward"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::settings.stewards.grid.index.results')
        @include('bct/projects::settings.stewards.grid.index.pagination')
        @include('bct/projects::settings.stewards.grid.index.filters')
        @include('bct/projects::settings.stewards.grid.index.no_results')

    </section>

@stop
