/**
 * Created by Aphiwad on 10/28/2015.
 */
var projectFilesSummary;

if (!projectFilesSummary) {
    var options = {
        events: {
            'fetching': function () {
                $('#files-counter').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            'fetched': function (result) {
                var num = (result.pagination.filtered > 5) ? 5 : result.pagination.filtered;
                $('#files-counter').text('(' + num + ' of ' + result.pagination.total + ')');
            }
        }
    };

    projectFilesSummary = $.datagrid(
        'project-files-summary',
        '#project-files-summary',
        'null',
        'null',
        options);
}