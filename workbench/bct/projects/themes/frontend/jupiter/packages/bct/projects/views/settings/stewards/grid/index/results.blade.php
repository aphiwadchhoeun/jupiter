<script type="text/template" data-grid="steward" data-template="results">
    <% _.each(results, function(r) { %>
    		<tr data-grid-row>
    			<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%- r.id %>"></td>
                <td><%- r.name %></td>
                <td><%- r.company %></td>
                <td><%- r.phone %></td>
                <td><a href="mailto:<%- r.email %>"><%- r.email %></a></td>
    			<td><%- r['created'] %></td>
                <td><a href="<%- r.view_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                        data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                                class="fa fa-book fa-fw"></i></a></td>
    		</tr>

    	<% }); %>

</script>
