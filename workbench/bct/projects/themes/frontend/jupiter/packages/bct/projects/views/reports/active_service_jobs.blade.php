@extends('bct/projects::default')

{{-- Page title --}}
@section('title'){{--
    --}}@parent{{--
    --}} Projects Report{{--
--}}@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('selectize', 'bower_components/selectize/dist/js/standalone/selectize.min.js', 'jquery') }}

    {{ Asset::queue('report3', 'bct/projects::reports/js/report3.js', ['jquery', 'swal']) }}

@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('selectize-css', 'bower_components/selectize/dist/css/selectize.css') }}
    {{ Asset::queue('selectize-bootstrap3', 'bower_components/selectize/dist/css/selectize.bootstrap3.css', 'bootstrap') }}
@stop

{{-- Page --}}
@section('page-extension')
    @parent

    {{-- Grid --}}
    <section class="panel panel-default">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{route('bct.projects.reports.index')}}" data-toggle="tooltip" data-original-title="Cancel">
                                    <i class="fa fa-reply"></i> <span class="visible-xs-inline">Cancel</span>
                                </a>
                            </li>
                        </ul>
                        <span class="navbar-brand">{{ trans('bct/projects::reports/common.tabs.report3_projects') }}</span>
                    </div>

                    <div class="pull-right">
                        <a href="#"
                           class="btn btn-link pull-right btn-download-pdf with-select hide"
                           data-toggle="tooltip"
                           data-original-title="{{{ trans('bct/projects::general/common.print') }}}">
                            <i class="fa fa-download fa-fw"></i>
                        </a>
                    </div>

                </div>

            </nav>

        </header>


        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">

                    <nav class="navbar navbar-default navbar-actions">


                        <div class="row">

                            <div class="col-md-2">
                                <b>{{ trans('bct/projects::reports/common.text.report3.select_regions') }}:</b>
                            </div>

                            <div class="col-md-6">
                                <select name="regions[]" id="regions-report" class="form-control" multiple>
                                    <option value="">{{ trans('bct/projects::reports/common.text.report3.select_regions') }}
                                        ...
                                    </option>

                                </select>
                            </div>

                            <div class="col-md-4">
                                <button class="btn btn-default navbar-left regions-report-select-all margin-hor-sides">{{ trans('bct/projects::general/common.select_all') }}</button>
                                <button class="btn btn-default navbar-left regions-report-deselect-all hide margin-hor-sides">{{ trans('bct/projects::general/common.deselect_all') }}</button>
                            </div>

                        </div>

                    </nav>

                </div>

            </div>


            <div class="row">
                <div class="loader" style="position: static">
                    <div class="sk-circle">
                        <div class="sk-circle1 sk-child"></div>
                        <div class="sk-circle2 sk-child"></div>
                        <div class="sk-circle3 sk-child"></div>
                        <div class="sk-circle4 sk-child"></div>
                        <div class="sk-circle5 sk-child"></div>
                        <div class="sk-circle6 sk-child"></div>
                        <div class="sk-circle7 sk-child"></div>
                        <div class="sk-circle8 sk-child"></div>
                        <div class="sk-circle9 sk-child"></div>
                        <div class="sk-circle10 sk-child"></div>
                        <div class="sk-circle11 sk-child"></div>
                        <div class="sk-circle12 sk-child"></div>
                    </div>
                </div>
            </div>


            <div class="row report-results"></div>

        </div>

        <script type="text/template" id="report_results_template">
            <% if (Object.keys(results).length > 0) { %>

            <div class="row">

                <div class="col-lg-12">

                    <div style="clear:both; height: 10px;"></div>

                    <div class="table-responsive">
                        <table class="table table-borderd">
                            <thead>
                            <tr>
                                <th>{{ trans('bct/projects::reports/common.col.region') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.project_name') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.contractor_name') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.project_address') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.project_city') }}</th>
                                <th>{{ trans('bct/projects::reports/common.col.stewards') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <% _.each(results, function(project) { %>

                            <tr>
                                <td><%- project.region %></td>
                                <td><%- project.name %></td>
                                <td><%- project.contractors %></td>
                                <td><%- project.address %></td>
                                <td><%- project.city %></td>
                                <td><%- project.stewards %></td>
                            </tr>

                            <% }) %>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

            <% } %>

        </script>

    </section>

@stop
