<script type="text/template" data-grid="project-steward" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.full_name %></td>
        <td><%- r.phone %></td>
        <td>
            <a href="mailto:<%- r.email %>" class="hidden-print"><%- r.email %></a>
            <span class="visible-print"><%- r.email %></span>
        </td>
        <td><%- r.local %></td>
        <td class="hidden-print">
            <a href="<%- r.delete_uri %>" data-modal
               data-toggle="tooltip" class="btn btn-danger btn-circle delete-confirm" data-placement="right"
               data-original-title="{{{ trans('bct/projects::general/common.remove') }}}">
                <i class="fa fa-trash fa-fw"></i></a></td>
    </tr>

    <% }); %>

</script>
