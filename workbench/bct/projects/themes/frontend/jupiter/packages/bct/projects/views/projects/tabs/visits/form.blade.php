<div class="modal-dialog">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">{{{ trans('bct/projects::projects/common.visit_' . $mode) }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="
                @if($visit->exists)
                    {{ route('bct.projects.projects.editVisit', [$project->id, $visit->id]) }}
                @else
                    {{ route('bct.projects.projects.createVisit', $project->id) }}
                @endif
                " data-refresh="visits">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>
                            <div class="row">

                                <div class="form-group half-width">

                                    <label for="follow_up_date" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.follow_up_date') }}}
                                    </label>

                                    <div class='input-group date'>
                                        <input type="text" class="form-control" name="follow_up_date" id="follow_up_date"
                                               placeholder="{{{ trans('bct/projects::projects/model.general.follow_up_date_help') }}}"
                                               required autofocus data-parsley-date value="{{ $project->follow_up_date_custom }}">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>

                                    <span class="help-block">{{{ Alert::onForm('follow_up_date') }}}</span>

                                </div>

                                <div class="form-group half-width">

                                    <label for="reason" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.follow_up_reason') }}}
                                    </label>


                                    <input type="text" class="form-control" name="reason" id="reason"
                                               placeholder="{{{ trans('bct/projects::projects/model.general.follow_up_reason_help') }}}"
                                               required value="{{ $visit->reason }}">


                                    <span class="help-block">{{{ Alert::onForm('reason') }}}</span>

                                </div>

                                <div class="form-group">

                                    <label for="details" class="control-label">
                                        {{{ trans('bct/projects::visits/model.general.details') }}}
                                    </label>

                                    <textarea class="form-control" name="details"
                                              placeholder="{{{ trans('bct/projects::visits/model.general.details_help') }}}"
                                              required rows="5">{{ $visit->details }}</textarea>

                                    <span class="help-block">{{{ Alert::onForm('details') }}}</span>

                                </div>

                                <div class="form-group{{ Alert::onForm('stages', ' has-error') }}">

                                    <label for="stages" class="control-label">
                                        {{{ trans('bct/projects::visits/model.general.stages') }}}
                                    </label>

                                    <select class="form-control selectize-input" name="stages[]" id="stages" style="width: 100%" required multiple>
                                        @foreach($stages as $stage)
                                            <option value="{{ $stage->id }}" @if($visit->stages->contains($stage->id)) selected @endif
                                                    >{{ $stage->name }}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('stages') }}}</span>

                                </div>

                                @if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1)
                                    <div class="form-group half-width{{ Alert::onForm('workers_count', ' has-error') }}">

                                        <label for="workers_count" class="control-label">
                                            {{{ trans('bct/projects::visits/model.general.workers_count') }}}
                                        </label>

                                        <input type="text" class="form-control" name="workers_count" id="workers_count"
                                               placeholder="{{{ trans('bct/projects::projects/model.general.follow_up_workers_help') }}}"
                                               value="{{ input()->old('workers_count', data_get($visit, 'workers_count', 1)) }}">

                                        <span class="help-block">{{{ Alert::onForm('workers_count') }}}</span>

                                    </div>
                                @endif

                                @if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1)
                                    <div class="form-group half-width{{ Alert::onForm('weather', ' has-error') }}">

                                        <label for="weather" class="control-label">
                                            {{{ trans('bct/projects::visits/model.general.weather') }}}
                                        </label>

                                        <select class="form-control" name="weather" id="weather" style="width: 100%">
                                            @foreach ($weather_options as $weather)
                                                <option value="{{ $weather }}" @if(input()->old('weather', $visit->weather) === $weather) selected @endif>{{ $weather }}</option>
                                            @endforeach
                                        </select>

                                        <span class="help-block">{{{ Alert::onForm('weather') }}}</span>

                                    </div>
                                @endif



                                <div class="form-group half-width{{ Alert::onForm('users', ' has-error') }}">

                                    <label for="users" class="control-label
                                    @if (!$is_update && $lists && config_projects('bct.projects.settings.enable_project_visit_weather')===
                                    config_projects('bct.projects.settings.enable_project_visit_workers')) label-height @endif">
                                        {{{ trans('bct/projects::visits/model.general.users_new') }}}
                                    </label>

                                    <select class="form-control selectize-input" name="users[]" id="users" style="width: 100%" multiple>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}" @if($visit->users->contains($user->id)) selected @endif
                                                    >{{ $user->first_name }} {{ $user->last_name }}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('users') }}}</span>

                                </div>
                                @if(!$is_update)
                                    @if($lists)

                                        <div class="form-group half-width{{ Alert::onForm('lists', ' has-error') }}">

                                            <label class="control-label">
                                                {{{ trans('bct/projects::visits/model.general.lists_new') }}}
                                            </label>

                                            <select class="form-control selectize-input" name="lists[]" id="project_lists" style="width: 100%" multiple>
                                                @foreach($lists as $list)
                                                    <option value="{{ $list->id }}" @if($list->pivot->is_active == false) selected @endif>{{ $list->name }} </option>
                                                @endforeach
                                            </select>
                                            <span class="help-block">{{{ Alert::onForm('lists') }}}</span>

                                        </div>

                                    @endif
                                @endif
                                @if (config_projects('bct.projects.settings.enable_project_visit_date') === 1)
                                    <div class="form-group half-width{{ Alert::onForm('visit_date', ' has-error') }}">

                                        <label for="visit_date" class="control-label
                                         @if (!$is_update && $lists && config_projects('bct.projects.settings.enable_project_visit_weather')!==
                                    config_projects('bct.projects.settings.enable_project_visit_workers')) label-height @endif">
                                            {{{ trans('bct/projects::visits/model.general.visit_date') }}}
                                        </label>
                                        <div class='input-group date'>

                                            <input type="text" class="form-control" name="visit_date" id="visit_date" data-parsley-date
                                                   placeholder="{{{ trans('bct/projects::projects/model.general.visit_date_help') }}}"
                                                   value="{{ input()->old('visit_date', date_correct_details_projects($visit->created_at)) }}">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>

                                        <span class="help-block">{{{ Alert::onForm('visit_date') }}}</span>

                                    </div>

                                    <div class="form-group half-width{{ Alert::onForm('visit_time', ' has-error') }}">

                                        <label for="visit_time" class="control-label">
                                            {{{ trans('bct/projects::visits/model.general.visit_time') }}}
                                        </label>
                                        <div class='input-group bootstrap-timepicker timepicker'>

                                            <input type="text" class="form-control" name="visit_time" id="visit_time" data-parsley-time
                                                   placeholder="{{{ trans('bct/projects::projects/model.general.visit_time_help') }}}"
                                                   value="{{ input()->old('visit_time', time_correct_details_projects($visit->created_at)) }}">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>

                                        <span class="help-block">{{{ Alert::onForm('visit_time') }}}</span>

                                    </div>
                                @endif
                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            @if (!$visit->approved_user && !$is_repuser && $mode === 'update')
                <button class="btn btn-warning btn-approve"
                        data-url="{{ route('bct.projects.visits.approve') }}"
                        data-visit-id="{{ $visit->id }}">
                    {{{ trans('bct/projects::general/common.approve') }}}</button>
            @elseif ($visit->approved_user)
                <span>
                    <b>{{ trans('bct/projects::visits/model.general.approved_by') }}:</b>
                    {{ $visit->approved_user->full_name}}
                </span>
                <button class="btn btn-warning btn-unapprove ml10"
                        data-url="{{ route('bct.projects.visits.unapprove') }}"
                        data-visit-id="{{ $visit->id }}">
                    {{{ trans('bct/projects::general/common.unapprove') }}}</button>
            @endif

            <button id="submit-btn-modal" class="btn btn-primary ml10">
                {{{ trans('action.save') }}}</button>

            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
