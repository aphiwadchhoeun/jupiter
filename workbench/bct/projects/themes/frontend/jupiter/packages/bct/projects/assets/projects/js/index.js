jQuery('document').ready(function ($) {
    $('body').on('click', '[data-modal]', function () {
        var $modal = $('#modal-form');

        $modal.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
            if (response) {
                $modal.modal('show');
                $('#add-to-list').addClass('disabled');
            } else {
                location.reload();
            }
        });
    });
});

var Extension;

;
(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'projects',
        defaults: {
            export_pdf_details_filename: 'Projects Details',
            export_field_reports_filename: 'Project Field Reports'
        }
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.datePicker(Extension);
        Extension.Index.dataGrid();
        Extension.Index.listeners();
        Extension.Index.datePicker.on('show.daterangepicker', function () {
            showDirectionPP('Follow Up');
        });
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row]', Extension.Index.checkRow)
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('change', '#list_id', Extension.Index.selectedList)
            .on('change', '#group_id', Extension.Index.getLists)
            .on('change', '#group_type', Extension.Index.getGroups)
            .on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
            .on('click', '#modal-confirm a.confirm', Extension.Index.bulkActions)
            .on('click', '#add-to-list', Extension.Index.bulkActions)
            .on('click', '[data-grid-calendar-preset]', Extension.Index.calendarPresets)
            .on('click', '[data-grid-bulk-action]:not([data-grid-bulk-action="delete"])', Extension.Index.bulkActions)
            .on('click', 'li:not(.disabled)>[data-download-custom="pdf_details"]', Extension.Index.exportPdfFewProjectDetails)
            .on('click', 'li:not(.disabled)>[data-download-custom="field_reports"]', Extension.Index.exportPdfFieldReports)
        ;
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxes = function (event) {
        event.stopPropagation();

        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all') {
            $('[data-grid-checkbox]').not(this).prop('checked', this.checked);

            $('[data-grid-row]').not(this).toggleClass('active', this.checked);
        }

        $(this).parents('[data-grid-row]').toggleClass('active');

        Extension.Index.bulkStatus();
        

    };

    // Handle Data Grid row checking
    Extension.Index.checkRow = function () {
        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', !checkbox.prop('checked'));

        Extension.Index.bulkStatus();
        

    };
    



    
    Extension.Index.bulkStatus = function () {
        var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

        var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;
        var list_modal_href = $('#lists-modal').data('href');

        $('[data-grid-bulk-action]').closest('li').toggleClass('disabled', !checked);
        $('[data-grid-bulk-action]').toggleClass('disabled', !checked);

        if (checked > 0) {
            $('[data-grid-bulk-action="delete"]').attr('data-modal', true);
            $('[data-download-custom="pdf_details"]').parent().removeClass('disabled');
            $('[data-download-custom="field_reports"]').parent().removeClass('disabled');

            if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
                $('#lists-modal').attr('href', list_modal_href + '&number=' + $('[data-grid-checkbox="all"]').data('all-count')).removeClass('disabled');
            } else {
                $('#lists-modal').attr('href', list_modal_href + '&number=' + checked).removeClass('disabled');
            }

        } else {
            $('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
            $('#lists-modal').attr('href', list_modal_href).addClass('disabled');
            $('[data-download-custom="pdf_details"]').parent().addClass('disabled');
            $('[data-download-custom="field_reports"]').parent().addClass('disabled');
        }

        $('[data-grid-checkbox="all"]')
            .prop('disabled', rows < 1)
        ;

    };

    // Handle Data Grid bulk actions
    Extension.Index.bulkActions = function (event) {
        event.preventDefault();
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }
        var url = window.location.origin + window.location.pathname;

        var action = $(this).data('grid-bulk-action') ? $(this).data('grid-bulk-action') : 'delete';

        var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function (event) {
            return +event.value;
        });

        if (rows.length > 0) {
            url = window.location.origin + "/pprofiles/projects/check";
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    rows: rows,
                    list_id: $("#list_id").val()
                },
                success: function (response) {
                    if (response == 'success') {
                        var add_url = window.location.origin + window.location.pathname;

                        if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
                            rows = 'all';
                        }

                        $.ajax({
                            type: 'POST',
                            url: add_url,
                            data: {
                                action: "storeLists",
                                rows: rows,
                                list_id: $("#list_id").val()
                            },
                            success: function (response) {
                                Extension.Index.Grid.refresh();
                                $('#modal-form').modal('hide');
                            }
                        });
                    } else {
                        swal({
                            title: "Some projects cannot be added to list because they already in the list!",
                            text: 'Do you want to continue?',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#019964",
                            confirmButtonText: "Yes!",
                            cancelButtonText: "No",
                            showLoaderOnConfirm: true,
                            preConfirm: function () {
                                return new Promise(function (resolve, reject) {
                                    var add_url = window.location.origin + window.location.pathname;

                                    if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
                                        rows = 'all';
                                    }

                                    $.ajax({
                                        type: 'POST',
                                        url: add_url,
                                        data: {
                                            action: "storeLists",
                                            rows: rows,
                                            list_id: $("#list_id").val()
                                        },
                                        success: function () {
                                            resolve();
                                        }
                                    });
                                });
                            }
                        }).then(function () {
                            Extension.Index.Grid.refresh();
                            $('#modal-form').modal('hide');

                        }, function () {
                            Extension.Index.Grid.refresh();
                            $('#modal-form').modal('hide');

                        });
                    }
                },
            });


        }
    };

    // Handle Data Grid calendar
    Extension.Index.calendarPresets = function (event) {
        event.preventDefault();

        var start, end;

        switch ($(this).data('grid-calendar-preset')) {
            case 'day':
                start = end = moment().subtract(1, 'day').startOf('day').format('MM/DD/YYYY');
                break;

            case 'week':
                start = moment().startOf('week').format('MM/DD/YYYY');
                end = moment().endOf('week').format('MM/DD/YYYY');
                break;

            case 'month':
                start = moment().startOf('month').format('MM/DD/YYYY');
                end = moment().endOf('month').format('MM/DD/YYYY');
                break;

            default:
        }

        $('input[name="daterangepicker_start"]').val(start);

        $('input[name="daterangepicker_end"]').val(end);

        $('.range_inputs .applyBtn').trigger('click');
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    Extension.Index.selectedList = function () {
        if ($('#list_id').val().length > 0 && $('#group_id').val().length > 0) {
            $('#add-to-list').removeClass('disabled');
        }
        else {
            $('#add-to-list').addClass('disabled');
        }

    };

    Extension.Index.getGroups = function () {
        var group_type = $('#group_type').val();
        if (group_type.length > 0) {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            var url = window.location.origin + window.location.pathname + "/get-groups/" + group_type;
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    group_type: group_type
                },
                success: function (response) {
                    var items = response.lists;
                    $('#group_id')
                        .find('option:gt(0)')
                        .remove()
                        .end()
                        .val('');
                    $.each(items, function (i, item) {
                        $('#group_id').append($('<option>', {
                            value: item.id,
                            text: item.name
                        }));
                    });
                    $("#group_id").removeAttr('disabled');
                }
            });
        }
        else {
            $('#group_id')
                .find('option:gt(0)')
                .remove()
                .end()
                .val('')
        }

    };

    Extension.Index.getLists = function () {
        var group_id = $('#group_id').val();
        if (group_id.length > 0) {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            var url = window.location.origin + window.location.pathname + "/get-lists/" + group_id;
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    group_id: group_id
                },
                success: function (response) {
                    var items = response.lists;
                    $('#list_id')
                        .find('option:gt(0)')
                        .remove()
                        .end()
                        .val('');
                    $.each(items, function (i, item) {
                        $('#list_id').append($('<option>', {
                            value: item.id,
                            text: item.name
                        }));
                    });
                    $("#list_id").removeAttr('disabled');

                }
            });
        }
        else {
            $('#list_id')
                .find('option:gt(0)')
                .remove()
                .end()
                .val('')
        }

    };

    Extension.Index.exportPdfFewProjectDetails = function (e) {
        e.preventDefault();

        var self = this;

        Extension.Index.askFileName(function (filename) {

            var url = $(self).data('url'),
                projects_ids;

            if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
                projects_ids = 'all';
            } else {
                projects_ids = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function (event) {
                    return +event.value;
                });
            }

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    projects_ids: projects_ids,
                    filename: filename
                }
            })
                .done(function () {
                    swal('PDF document has been requested!', 'Your document will be ready very soon.', 'success');
                })
                .fail(function () {
                    swal('Something went wrong.', '', 'error');
                });
        }, Extension.Index.defaults.export_pdf_details_filename);
    };

    /**
     * Handling event export project field reports
     *
     * @param e
     */
    Extension.Index.exportPdfFieldReports = function (e) {
        e.preventDefault();

        // prevent from export field reports if user selected all projects
        // because too heavy processing
        if ($('[data-grid-checkbox="all"]:checked').length > 0 && $('#data-grid_applied').html() === '') {
            swal('Due to heavy workload, Field Reports cannot be processed for all projects at once. Please select Projects to be processed.', '', 'warning');
            return false;
        }

        var self = this;

        Extension.Index.askFileName(function (filename) {

            var url = $(self).data('url'),
                projects_ids;

            projects_ids = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function (event) {
                return +event.value;
            });

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    projects_ids: projects_ids,
                    filename: filename
                }
            })
                .done(function () {
                    swal('PDF document has been requested!', 'Your document will be ready very soon.', 'success');
                })
                .fail(function () {
                    swal('Something went wrong.', '', 'error');
                });
        }, Extension.Index.defaults.export_field_reports_filename);
    };

    /**
     * Ask for file name when try to export
     */
    Extension.Index.askFileName = function (callback, defaultFilename) {
        swal({
            title: 'Enter filename',
            input: 'text',
            showCancelButton: true,
            inputValue: defaultFilename,
            showLoaderOnConfirm: true,
            inputValidator: function(value) {
                return new Promise(function(resolve, reject) {
                    if (value) {
                        if(value.length > 80) {
                            reject('Filename length should not be longer than 80 characters.');
                        }

                        resolve();
                    } else {
                        reject('You need to write something!');
                    }
                });
            }
        }).then(function (input) {
            callback(input);
            swal("Thank you!", "", "success");
        });
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            hash: false,
            source_default: $('#data-grid').data('source-default'),
            callback_local: function () {
                $('[data-filter="status:Active"]').click();
            },
            events: {
                removing: function (dg) {
                    _.each(dg.applied_filters, function (filter) {
                        if (filter.column === 'follow_up_date' && filter.from !== undefined && filter.to !== undefined) {
                            $('[data-grid-calendar]').val('');
                        }

                    });
                },
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                applying_dev: function (dg) {
                    var tags = '';
                    var many_tags = false;
                    _.each(dg.applied_filters, function (filter) {

                        var index = dg.applied_filters.indexOf(filter);

                        if (filter.column === 'tags' && typeof(filter.value) !== 'undefined' && filter.value !== '') {

                            var searchValue = filter.value;

                            tags = searchValue.split(',');
                            if (tags === searchValue) {
                                tags = searchValue.split(', ');
                                if (tags !== searchValue) {
                                    many_tags = true;
                                    dg.applied_filters.splice(index, 1);
                                }
                            }
                            else {
                                many_tags = true;
                                dg.applied_filters.splice(index, 1);
                            }

                        }
                    });
                    if ((tags !== '' || tags !== null) && many_tags) {
                        $.each(tags, function (index, tag) {
                            var obj = {column: 'tags', value: tag, operator: ''}
                            dg.applied_filters.push(obj)
                        });
                    }

                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                }
            },
            callback: function () {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

                Extension.Index.bulkStatus();
                
            }
        };

        Extension.Index.Grid = $.datagrid('projects', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
