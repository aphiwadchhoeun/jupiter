<div class="modal-dialog ext-profiles" id="project-contractors-modal">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">{{{ trans('bct/projects::projects/common.na_' . $mode) }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="
                @if(isset($pivot))
                    {{ route('bct.projects.projects.contractor.edit.na', [$project->id, $pivot->id]) }}
                @else
                    {{ route('bct.projects.projects.contractor.add.na', [$project->id]) }}
                @endif" data-refresh="contractors">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(isset($pivot))
                    <input type="hidden" name="pivot_id" value="{{ $pivot->id }}">
                @endif

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>

                            <div class="row">

                                {{--add more form-group for other fields, just keep the same uniform--}}
                                <div class="form-group">

                                    <label for="scope_id" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.scope_id') }}}
                                    </label>
                                    @if($mode==='update')
                                        <select class="form-control" id="scope_id" name="scope_id" style="width: 100%" required>
                                            @foreach($scopes as $scope)
                                                <option value="{{ $scope->id }}" @if(isset($pivot) && $pivot->z_scopes_id == $scope->id) selected @endif
                                                >{{ $scope->name }} ({{ $scope->value }})</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select class="form-control" name="scope_id[]" multiple data-parsley-multiple="scope_id[]" id="scope_id" style="width: 100%" required>
                                            @foreach($scopes as $scope)
                                                <option value="{{ $scope->id }}" @if(isset($pivot) && $pivot->z_scopes_id == $scope->id) selected @endif
                                                >{{ $scope->name }} ({{ $scope->value }})</option>
                                            @endforeach
                                        </select>
                                    @endif

                                    <span class="help-block">{{{ Alert::onForm('scope_id') }}}</span>

                                </div>

                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="submit-btn-modal" name="submit-btn-modal" class="btn btn-primary">
                {{{ trans('action.save') }}}</button>

            @if(isset($pivot))
                <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-action="delete"
                        data-url="{{ route('bct.projects.projects.deleteContractor', [
                                'id' => $project->id,
                                'pivot_id' => $pivot->id,
                            ]) }}">
                    {{{ trans('action.delete') }}}</button>
            @endif

            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
