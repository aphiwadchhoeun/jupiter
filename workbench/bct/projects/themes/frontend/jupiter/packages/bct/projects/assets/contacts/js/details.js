/**
 * Created by Aphiwad on 9/28/2016.
 */
var Extension;

;(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'projects'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.datePicker(Extension);
        Extension.Index.dataGrid();
        Extension.Index.listeners();
        Extension.Index.datePicker.on('show.daterangepicker',function(){
            showDirectionPP('Follow Up');
        });
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid-calendar-preset]', Extension.Index.calendarPresets)
        ;
    };

    // Handle Data Grid calendar
    Extension.Index.calendarPresets = function (event) {
        event.preventDefault();

        var start, end;

        switch ($(this).data('grid-calendar-preset')) {
            case 'day':
                start = end = moment().subtract(1, 'day').startOf('day').format('MM/DD/YYYY');
                break;

            case 'week':
                start = moment().startOf('week').format('MM/DD/YYYY');
                end = moment().endOf('week').format('MM/DD/YYYY');
                break;

            case 'month':
                start = moment().startOf('month').format('MM/DD/YYYY');
                end = moment().endOf('month').format('MM/DD/YYYY');
                break;

            default:
        }

        $('input[name="daterangepicker_start"]').val(start);

        $('input[name="daterangepicker_end"]').val(end);

        $('.range_inputs .applyBtn').trigger('click');
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            hash: false,
            events: {
                removing: function (dg) {
                    _.each(dg.applied_filters, function (filter) {
                        if (filter.column === 'follow_up_date' && filter.from !== undefined && filter.to !== undefined) {
                            $('[data-grid-calendar]').val('');
                        }

                    });
                },
                sorted: function(response) {
                    if (response.pagination.total== 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function(response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container' ).removeClass('hide');
                    }
                }
            },
        };

        Extension.Index.Grid = $.datagrid('projects', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
