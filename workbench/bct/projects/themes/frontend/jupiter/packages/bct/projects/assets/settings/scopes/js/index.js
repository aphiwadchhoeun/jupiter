jQuery('document').ready(function ($) {
	$('body').on('click', '[data-modal]', function(){
		var $modal = $('#modal-form');

		$modal.find('.modal-content').load($(this).attr('href'), '', function(response, status, xhr){
			if (response) {
				$modal.modal('show');

				var submit_btn_modal = $('#submit-btn-modal');
				var delete_btn_modal = $('#delete-btn-modal');
				var form_modal = $('#content-form-modal');

				if (form_modal) {
					form_modal.on('submit', function(e){
						e.preventDefault();
					});

					if (form_modal.parsley()) {
						form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
							if (formInstance.isValid() === true) {
								$.post(
									form_modal.attr('action'),
									form_modal.serialize(),
									function(data){
										$modal.modal('hide');
                                        window.location.reload();
										//Extension.Index.Grid.refresh();
									}
								);
							}
						});

						submit_btn_modal.on('click', function(){
							form_modal.parsley().validate();
						});
					}

					if (delete_btn_modal) {
						delete_btn_modal.on('click', function(){
							var url = $(this).attr('data-url');
							$.ajax({
								type: 'POST',
								url: url,
								data: {},
								success: function(response)
								{
									$modal.modal('hide');
									Extension.Index.Grid.refresh();
								}
							});
						});
					}
				}
			} else {
				location.reload();
			}
		});
	});
});

var Extension;

;(function(window, document, $, undefined)
{

	'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'scopes'
    });

	// Initialize functions
	Extension.Index.init = function()
	{
		Extension.Index.dataGrid();
		Extension.Index.listeners();
	};

	// Add Listeners
	Extension.Index.listeners = function()
	{
		Platform.Cache.$body
			.on('click', '[data-grid-row]', Extension.Index.checkRow)
			.on('click', '[data-grid-row] a', Extension.Index.titleClick)
			.on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
			.on('click', '#modal-confirm a.confirm', Extension.Index.bulkActions)
			.on('click', '[data-grid-bulk-action]:not([data-grid-bulk-action="delete"])', Extension.Index.bulkActions)
		;
	};

	// Handle Data Grid checkboxes
	Extension.Index.checkboxes = function(event)
	{
		event.stopPropagation();

		var type = $(this).attr('data-grid-checkbox');

		if (type === 'all')
		{
			$('[data-grid-checkbox]').not(this).prop('checked', this.checked);

			$('[data-grid-row]').not(this).toggleClass('active', this.checked);
		}

		$(this).parents('[data-grid-row]').toggleClass('active');

		Extension.Index.bulkStatus();
	};

	// Handle Data Grid row checking
	Extension.Index.checkRow = function()
	{
		$(this).toggleClass('active');

		var checkbox = $(this).find('[data-grid-checkbox]');

		checkbox.prop('checked', ! checkbox.prop('checked'));

		Extension.Index.bulkStatus();
	};

	Extension.Index.bulkStatus = function()
	{
		var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

		var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;

		$('[data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);
		$('[data-grid-bulk-action]').toggleClass('disabled', ! checked);

		$('[data-grid-bulk-action="add"]').tooltip();

		if (checked > 0)
		{
			$('[data-grid-bulk-action="delete"]').attr('data-modal', true);
			$('[data-grid-bulk-action="add"]').off('click');
		}
		else
		{
			$('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
			$('[data-grid-bulk-action="add"]').on('click', function(){
				return false;
			});
		}

		$('[data-grid-checkbox="all"]')
			.prop('disabled', rows < 1)
			.prop('checked', rows < 1 ? false : rows === checked)
		;
	};

	// Handle Data Grid bulk actions
	Extension.Index.bulkActions = function(event)
	{
		event.preventDefault();
        
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
        
		var url = window.location.origin + window.location.pathname;

		var action = $(this).data('grid-bulk-action') ? $(this).data('grid-bulk-action') : 'delete';

		var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
		{
			return +event.value;
		});

		if (rows.length > 0)
		{
			if (action == 'delete') {
				$.ajax({
					type: 'POST',
					url: url,
					data: {
						action : action,
						rows   : rows
					},
					success: function(response)
					{
						window.location.reload();
					}
				});
			} else {
				var create_modal = $('#modal-form');

				create_modal.off('loaded.bs.modal').on('loaded.bs.modal', function () {

					/** init autocomplete */
					$("#name").typeahead({
						source	: function (query, process) {
							return $.get($("#name").data('href'), function (data) {
								return process(data);
							});
						},
						updater	: function(item){
							$("#id").val(item.id);
							return item;
						}
					});

					var submit_btn_modal = $('#submit-btn-modal');
					var form_modal = $('#content-form-modal');

					form_modal.find('#ids').val(rows);

					if (form_modal) {
						form_modal.on('submit', function(e){
							e.preventDefault();
						});

						form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
							if (formInstance.isValid() == true) { // check if form valid or not
								$.post(
									form_modal.attr('action'),
									form_modal.serialize(),
									function(data){
										create_modal.modal('hide');
										Extension.Index.Grid.refresh();
									}
								);
							}
						});

						submit_btn_modal.on('click', function(){
							form_modal.parsley().validate();
						});
					}
				});

				create_modal.on('hidden.bs.modal', function () {
					create_modal.removeData('bs.modal')
				});
			}

		}
	};

	// Ignore row selection on title click
	Extension.Index.titleClick = function(event)
	{
		event.stopPropagation();
	};

	// Data Grid initialization
	Extension.Index.dataGrid = function()
	{
		var config = {
			events: {
				removing: function(dg)
				{
					_.each(dg.applied_filters, function(filter)
					{
						if (filter.column === 'created_at' && filter.from !== undefined && filter.to !== undefined)
						{
							$('[data-grid-calendar]').val('');
						}
					});
				},
				sorted: function(response) {
					if (response.pagination.total== 0) {
						$('.download-container').addClass('hide');
					}
				},
				fetched: function(response) {
					if (response.pagination.filtered == 0) {
						$('.download-container').addClass('hide');
					} else {
						$('.download-container' ).removeClass('hide');
					}
				},
			},
			callback: function()
			{
				$('[data-grid-checkbox-all]').prop('checked', false);

				$('[data-action]').prop('disabled', true);

				Extension.Index.bulkStatus();
			}
		};

		Extension.Index.Grid = $.datagrid('scopes', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
	};

	// Job done, lets run.
	Extension.Index.init();

})(window, document, jQuery);
