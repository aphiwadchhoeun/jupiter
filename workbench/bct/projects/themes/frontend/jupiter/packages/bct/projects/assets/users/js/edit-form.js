/**
 * Created by Aphiwad on 4/17/2015.
 */
jQuery('document').ready(function ($) {
    var submit_btn = $('#submit-edit-btn');
    var main_form = $('#content-form');

    $('input[name="phone"]').mask('(999) 999-9999? x999');

    submit_btn.click(function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            main_form.submit();
        });
    });


});