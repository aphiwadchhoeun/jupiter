@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::users/common.title') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('index', 'bct/projects::groups/js/index.js', 'extension-base') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::groups/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span
                                            class="visible-xs-inline">{{{ trans('action.export') }}}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="groups" role="form">

                            <div class="input-group">

							<span class="input-group-btn">

								<button class="btn btn-default" type="button" disabled>
                                    {{{ trans('common.status') }}}
                                </button>

								<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

								<ul class="dropdown-menu" role="menu" data-filter-reset>
                                    <li>
                                        <a href="#" data-filter="status:1">
                                            <i class="fa fa-check-circle"></i> {{trans('bct/projects::general/common.active')}}
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" data-filter="status:0">
                                            <i class="fa fa-times-circle"></i> {{trans('bct/projects::general/common.disabled')}}
                                        </a>
                                    </li>
                                </ul>
							</span>

                            <span class="input-group-btn">

								<button class="btn btn-default" type="button" disabled>
                                    {{ trans('bct/projects::groups/model.general.type') }}
                                </button>

								<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

								<ul class="dropdown-menu" role="menu" data-filter-reset>
                                    <li>
                                        <a href="#" data-filter="individual:1">
                                            <i class="fa fa-check-circle"></i> {{trans('bct/projects::groups/model.general.individual')}}
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" data-filter="individual:0">
                                            <i class="fa fa-check-circle"></i> {{trans('bct/projects::groups/model.general.group')}}
                                        </a>
                                    </li>
                                </ul>
							</span>

                            <span class="input-group-btn">
                                <select name="column" class="form-control">
                                    <option value="z_projects_group.name">{{ trans('bct/projects::groups/model.general.name') }}</option>
                                    <option value="z_projects_group.description">{{ trans('bct/projects::groups/model.general.description') }}</option>
                                </select>
                            </span>

                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">
                                    <span class="fa fa-search"></span>
                                </button>

								<button class="btn btn-default" data-grid="groups" data-reset>
                                    <i class="fa fa-refresh fa-sm"></i>
                                </button>

							</span>

                            </div>

                        </form>

                        <div class="pull-right">

                            <a href="{{ route('bct.projects.groups.create') }}"
                                id="project_new"
                                name="project_new"
                               class="btn btn-success" data-toggle="tooltip"
                               data-original-title="{{{ trans('action.create') }}}">
                                <i class="fa fa-plus fa-fw"></i>
                            </a>

                        </div>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="groups"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.groups.grid') }}"
                       data-filename="{{ trans('bct/projects::groups/common.title') }}"
                       data-grid="groups">
                    <thead>
                    <tr>
                        <th class="sortable" data-sort="name">{{ trans('bct/projects::groups/model.general.name') }}</th>
                        <th class="sortable"
                            data-sort="individual">{{ trans('bct/projects::groups/model.general.type') }}</th>
                        <th class="sortable"
                            data-sort="description">{{ trans('bct/projects::groups/model.general.description') }}</th>
                        <th class="sortable"
                            data-sort="status">{{ trans('bct/projects::groups/model.general.status') }}</th>
                        <th class="hidden-print">{{{ trans('bct/projects::general/common.details') }}}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer hidden-print">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination clearfix" data-grid="groups"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::groups.grid.index.results')
        @include('bct/projects::groups.grid.index.pagination')
        @include('bct/projects::groups.grid.index.filters')
        @include('bct/projects::groups.grid.index.no_results')

    </section>

@stop
