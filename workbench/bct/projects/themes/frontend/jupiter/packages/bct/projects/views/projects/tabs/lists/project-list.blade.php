{{ Asset::queue('project-list-grid', 'bct/projects::projects/js/lists/project-list.js', 'extension-base') }}

<section class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <header class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-lists-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::projects/common.title') }} {{ trans('bct/projects::lists/common.title') }}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-lists-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-lists-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span
                                        class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="project-list" role="form">


                        <div class="input-group">

                            <span class="input-group-btn">

                                <button class="btn btn-default" type="button" data-grid-calendar id="project-list-filter-calendar"
                                        data-range-filter="date_added">
                                    <i class="fa fa-calendar"></i>
                                </button>



                            </span>

                            <span class="input-group-btn">
                                <select name="column" class="form-control" id="project-list-filter-select">
                                    <option value="name">{{ trans('bct/projects::lists/model.general.name') }}
                                    </option>
                                    <option value="username">{{ trans('bct/projects::lists/model.general.username') }}
                                    </option>
                                </select>
                            </span>

                            <input class="form-control" name="filter" type="text" id="project-list-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">

                            <span class="input-group-btn">

                                <button class="btn btn-default" type="submit" id="project-list-filter-btn-search">
                                    <span class="fa fa-search"></span>
                                </button>

                                <button class="btn btn-default" data-grid="project-list" data-reset id="project-list-filter-btn-reset">
                                    <i class="fa fa-refresh fa-sm"></i>
                                </button>

                            </span>


                        </div>

                        <input type="hidden" data-range-filter="date_added" data-format="YYYY-MM-DD" data-range-start>
                        <input type="hidden" data-range-filter="date_added" data-format="YYYY-MM-DD" data-range-end>

                    </form>
                    @if (!$currentUser->inRole('pp-guest'))

                    <div class="nav navbar-nav navbar-right">

                        <a href="{{ route('bct.projects.projects.createLists', $project->id) }}"
                            data-modal
                           id="list_new"
                           name="list_new"
                           class="btn btn-success pull-right margin-right5 lists-add-btn" data-toggle="tooltip"
                           id="project-list-filter-btn-add"
                           data-original-title="{{{ trans('action.add') }}}">
                            <i class="fa fa-plus fa-fw"></i>
                        </a>

                    </div>
                    @endif

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="project-list-data-grid-applied-filters">

            <div id="project-list-data-grid_applied" class="btn-group" data-grid="project-list"></div>

        </div>

        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="project-list-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridLists', $project->id) }}"
                   data-filename="{{ $project->name }} {{ trans("bct/projects::projects/common.tabs.lists") }}"
                   data-grid="project-list">
                <thead>
                <tr>
                    <th class="sortable col-md-5"
                        data-sort="name">{{ trans('bct/projects::lists/model.general.name') }}</th>
                    <th class="sortable col-md-4"
                        data-sort="username">{{ trans('bct/projects::lists/model.general.username') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="date_added">{{ trans('bct/projects::lists/model.general.date_added') }}</th>
                    @if (!$currentUser->inRole('pp-guest'))
                    <th class="hidden-print col-md-1">{{{ trans('bct/projects::general/common.actions') }}}</th>
                    @endif
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>

    <footer class="panel-footer clearfix hidden-print">

        {{-- Grid: Pagination --}}
        <div class="project-list-data-grid_pagination" data-grid="project-list"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.lists.grid.project-lists.results')
    @include('bct/projects::projects.tabs.lists.grid.project-lists.pagination')
    @include('bct/projects::projects.tabs.lists.grid.project-lists.filters')
    @include('bct/projects::projects.tabs.lists.grid.project-lists.no_results')

</section>