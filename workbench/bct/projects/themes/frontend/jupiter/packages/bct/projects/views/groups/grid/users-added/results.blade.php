<script type="text/template" data-grid="users-added" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td>
            <input input="" type="hidden" value="<%- r.id %>">
            <%- r.full_name %>
        </td>
        <td><a class="under-none" href="mailto:<%- r.email %>"><%- r.email %></a></td>
        <td><%- r.roles %></td>
        <td>
            <% if(r.status == 'Active') { %>
            <span class="label label-success"><%- r.status %></span>
            <% } else { %>
            <span class="label label-danger"><%- r.status %></span>
            <% } %>
        </td>
        <td>
            <button class="btn btn-danger btn-circle remove-from-group" data-toggle="tooltip" data-placement="right"
               data-original-title="{{{ trans('bct/projects::groups/common.remove_from_group') }}}"><i
                        class="fa fa-minus fa-fw"></i></button>
        </td>
    </tr>

    <% }); %>

</script>
