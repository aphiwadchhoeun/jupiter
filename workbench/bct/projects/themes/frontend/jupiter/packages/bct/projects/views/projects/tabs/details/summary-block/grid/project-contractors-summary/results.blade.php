<script type="text/template" data-grid="project-contractors-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row class="redirect-to" data-url="<%- r.edit_uri%>">
        <td><%- r.scope %> (<%- r.value %>)</td>
        <td><%- r.name %></td>
        <td>
            <% if(r.is_union == true) { %>
            <span class="label label-success">Yes</span>
            <% } else { %>
            <span class="label label-danger">No</span>
            <% } %>
        </td>
        <td>
            <% if(r.has_agreement == true) { %>
            <span class="label label-success">Yes</span>
            <% } else { %>
            <span class="label label-danger">No</span>
            <% } %>
        </td>
    </tr>

    <% }); %>

</script>
