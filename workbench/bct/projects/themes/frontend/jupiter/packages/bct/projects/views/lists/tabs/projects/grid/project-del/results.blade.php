<script type="text/template" data-grid="project-del" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
            <td>
                <input input="" type="hidden" value="<%- r.id %>">
                <%- r.name %>
            </td>
            <td><%- r.city %></td>
            <td><%- r.zipcode %></td>
            <td><%- r.follow_up_date_custom %></td>
            <td><%- r.tags %></td>
            <td>
                <% if (r.status == 'Visited') { %>
                <span class="label label-success"><%- r.status %></span>
                <% } else if (r.status == 'Not visited') {%>
                <span class="label label-danger"><%- r.status %></span>
                <% } else {%>
                <span class="label label-default"><%- r.status %></span>
                <% } %>
            </td>
            <td class="hidden-print">

                @if ($has_access && !$currentUser->inRole('pp-guest'))
                    <button class="btn btn-danger btn-circle remove-from-list" data-toggle="tooltip" data-placement="right"
                        data-original-title="{{{ trans('bct/projects::lists/common.remove_from_list') }}}">
                        <i class="fa fa-minus fa-fw"></i>
                    </button>
                @endif

            </td>
		</tr>

	<% }); %>

</script>
