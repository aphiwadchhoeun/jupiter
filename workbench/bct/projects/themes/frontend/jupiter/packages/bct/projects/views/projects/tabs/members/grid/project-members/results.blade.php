<script type="text/template" data-grid="project-member" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.member_number %></td>
        <td><%- r.member_name %></td>
        <td><%- r.scope_name %></td>
        <td><%- r.number_of_hours %></td>
        <td><%- r.wage_correct %></td>
        @if (!$currentUser->inRole('pp-guest'))
        <td class="hidden-print"><a href="<%- r.view_uri %>" data-modal
               data-toggle="tooltip" id="member_edit" name="member_edit" class="btn btn-primary btn-circle" data-placement="right"
               data-original-title="{{{ trans('bct/projects::general/common.edit') }}}"><i
                        class="fa fa-book fa-fw"></i></a></td>
        @endif
    </tr>

    <% }); %>

</script>
