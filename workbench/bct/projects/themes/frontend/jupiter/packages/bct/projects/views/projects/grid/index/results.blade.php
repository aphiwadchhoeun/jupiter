<script type="text/template" data-grid="projects" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
			<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%- r.id %>"></td>
            <td><%- r.id %></td>
            <td class="project-name-column"><%- r.name_short %></td>
            <td><%- r.city %></td>
            <td><%- r.region_abrv %></td>
            <td class="follow-date-column"><%- r.follow_up_date_custom %></td>
            <td><%= r.tags %></td>
            <td>
                <% if (r.status) { %>
                    <% if (r.status == 'Active') { %>
                        <span class="label label-success"><%- r.status %></span>
                    <% } else if (r.status == 'Disabled') {%>
                        <span class="label label-danger"><%- r.status %></span>
                    <% } else {%>
                        <span class="label label-default"><%- r.status %></span>
                    <% } %>
                <% } %>
            </td>
            <td><a href="<%- r.view_uri %>#details" class="btn btn-primary btn-circle btn-details" data-toggle="tooltip" data-placement="right"
                    data-name="<%- r.name_short %>"
                    data-id="<%- r.id %>"
                    data-status="<%- r.status %>"
                    data-classification="<%- r.classification %>"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                            class="fa fa-book fa-fw"></i></a></td>
		</tr>

	<% }); %>

</script>
