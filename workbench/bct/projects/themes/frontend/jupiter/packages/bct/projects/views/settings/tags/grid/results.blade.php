<script type="text/template" data-grid="tags" data-template="results">

	<% _.each(results, function(r) { %>
		<tr data-grid-row>
			<td><%- r.name %></td>
			<td><%- r.group %></td>
            <td>
                <a href="<%- r.edit_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                            class="fa fa-book fa-fw"></i></a>
            </td>
		</tr>

	<% }); %>

</script>
