{{ Asset::queue('project-contractor-grid', 'bct/projects::projects/js/contractors/project-contractor.js', 'extension-base') }}

<div class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <div class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-contractors-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::projects/common.project') }} {{ trans('bct/projects::contractors/common.title') }}</span>
                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-contractors-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-contractor-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="project-contractor" role="form">

                        <div class="input-group">

                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" disabled>
                                    {{ trans('bct/projects::contractors/model.general.union') }}
                                </button>

                                <button class="btn btn-default dropdown-toggle" type="button"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

                                <ul class="dropdown-menu" role="menu" data-filter-reset>
                                    <li><a href="#" data-filter="z_project_contractor_join.is_union:1" id="project-contractor-filter-union">Union</a></li>
                                    <li><a href="#" data-filter="z_project_contractor_join.is_union:0" id="project-contractor-filter-not-union">Not union</a></li>
                                </ul>
                            </span>

                            <span class="input-group-btn">
                                <select name="column" class="form-control" id="project-contractor-filter-select">

                                    <option value="scope.name">{{ trans('bct/projects::projects/model.general.scope_id') }}
                                    </option>
                                    <option value="z_contractors.name">{{ trans('bct/projects::contractors/model.general.name') }}
                                    </option>
                                    <option value="z_contractors.id">{{ trans('bct/projects::projects/model.general.id') }}
                                    </option>
                                </select>
                            </span>

                            <input class="form-control" name="filter" type="text" id="project-contractor-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="project-contractor-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="project-contractor" data-reset id="project-contractor-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>
                                </span>

                            <span class="input-group-btn">
                                <button type="button" id="project-contractor-filter-na-only-yes" class="hide" data-filter="na_only:1" data-grid="project-contractor"></button>
                                <button type="button" id="project-contractor-filter-na-only-no" class="hide" data-filter="na_only:0" data-grid="project-contractor"></button>

                                <label class="btn btn-default">
                                    <input type="checkbox" id="project-contractor-filter-checkbox-na"> {{ trans('bct/projects::general/common.na_only') }}
                                </label>
                            </span>
                        </div>

                    </form>
                    @if (!$currentUser->inRole('pp-guest'))
                    <div class="nav navbar-nav navbar-right">

                        <a href="{{ route('bct.projects.projects.contractor.add.na', [$project->id]) }}"
                           data-modal-create-na-ajax
                           class="btn btn-success pull-right margin-right5" data-toggle="tooltip"
                           id="project-contractors-btn-create-na"
                           data-original-title="{{{ trans('action.create') }}}">
                            {{ trans('bct/projects::contractors/common.add_na') }}
                        </a>

                    </div>
                    @endif

                </div>

            </div>

        </nav>

    </div>

    <div class="panel-body">
        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="project-contractor-data-grid-applied-filters">
            <div id="project-contractor-data-grid_applied" class="btn-group" data-grid="project-contractor"></div>
        </div>

        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="project-contractor-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridContractors', $project->id) }}"
                   data-filename="{{ $project->name }} {{ trans("bct/projects::projects/common.tabs.contractors") }}"
                   data-grid="project-contractor">
                <thead>
                <tr>
                    <th class="sortable col-md-1"
                        data-sort="id">{{ trans('bct/projects::projects/model.general.id') }}
                    </th>
                    <th class="sortable col-md-3"
                        data-sort="value">{{ trans('bct/projects::projects/model.general.scope_id') }}
                    </th>
                    <th class="sortable col-md-4"
                        data-sort="name">{{ trans('bct/projects::contractors/model.general.name') }}
                    </th>
                    <th class="sortable col-md-2"
                        data-sort="phone">{{ trans('bct/projects::contractors/model.general.phone') }}
                    </th>
                    <th class="sortable col-md-1"
                        data-sort="is_union">{{ trans('bct/projects::contractors/model.general.union') }}
                    </th>
                    <th class="sortable col-md-1"
                        data-sort="pivot_is_agreement">{{ trans('bct/projects::projects/model.general.pa') }}
                    </th>
                    @if (!$currentUser->inRole('pp-guest'))
                    <th class="hidden-print col-md-1">{{ trans('bct/projects::general/common.actions') }}</th>
                    @endif
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>

    <footer class="panel-footer clearfix hidden-print">
        {{-- Grid: Pagination --}}
        <div class="project-contractor-data-grid_pagination" data-grid="project-contractor"></div>
    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.contractors.grid.project-contractors.results')
    @include('bct/projects::projects.tabs.contractors.grid.project-contractors.pagination')
    @include('bct/projects::projects.tabs.contractors.grid.project-contractors.filters')
    @include('bct/projects::projects.tabs.contractors.grid.project-contractors.no_results')

</div>