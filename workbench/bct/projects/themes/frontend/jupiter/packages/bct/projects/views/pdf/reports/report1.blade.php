<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    {{ Asset::queue('normalize', 'bct/projects::custom/css/normalize.css') }}
    {{ Asset::queue('bootstrap-grid', 'bct/projects::custom/css/bootstrap-grid-export.css') }}
    {{ Asset::queue('report', 'bct/projects::custom/css/report.css') }}

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach

</head>
<body>

@include('bct/projects::pdf/partials/header')

@if ($results)

    @foreach($results as $region => $projects)

        @if (!empty($projects))

            <div class="not-break">
                <div class="row">

                    <div class="col-lg-12">

                        <div class="navbar-brand text-center w100">Job List - {{ $region }}</div>

                        <div style="clear:both; height: 10px;"></div>

                    </div>

                </div>

                <div style="clear:both; height: 10px;"></div>

                <div class="row">
                    <div class="panel-body">
                        <div class="row" style="width:100%;">

                            @foreach($projects as $item)

                                <table class="table table-bordered table-details not-break">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="row row-table">
                                                <div class="col-xs-4 col-panel">
                                                    {{ strtoupper(trans('bct/projects::projects/common.project')) }}: <u>{{ str_limit($item->project_name, 130) }}</u>
                                                </div>
                                                <div class="col-xs-6 col-panel">
                                                    {{ strtoupper(trans('bct/projects::general/common.address')) }}: <u>{{ $item->project_full_address }}</u>
                                                </div>

                                                <div class="col-xs-2 col-panel text-right">
                                                    {{ strtoupper(trans('bct/projects::projects/model.general.value_id')) }}: <u>{{ $item->value }}</u>
                                                </div>
                                            </div>

                                            <div style="clear:both;"></div>

                                            <div class="row row-table ">
                                                <div class="col-xs-9 col-panel">
                                                    {{ strtoupper(trans('bct/projects::projects/common.project_stages')) }}: <u>{{ $item->stages }}</u>
                                                </div>
                                                <div class="col-xs-3 col-panel text-right">
                                                    {{ strtoupper(trans('bct/projects::projects/common.last_jobsite_visit')) }}: <u>{{ $item->last_visit_date }}</u>
                                                </div>
                                            </div>

                                            <div style="clear:both; height: 10px;"></div>

                                            @if (isset($item->general_contractor) && !empty($item->general_contractor))
                                                <?php
                                                $general_contractors = explode('!!', $item->general_contractor);
                                                ?>

                                                <div class="row row-table ">
                                                    @if ($general_contractors)
                                                        @foreach ($general_contractors as $gc)

                                                            <div class="col-xs-4" @if (!$loop->last) style="padding-right: 5px" @endif>
                                                                <u>{{ $gc }}</u>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>

                                            @endif

                                            <div style="clear:both;"></div>

                                            @if (isset($item->others_contractors) && !is_null($item->others_contractors))
                                            <?php
                                                $others_contractors = explode('!!', $item->others_contractors);
                                            ?>

                                                <div class="row row-table ">
                                                    @if ($others_contractors)
                                                        @foreach ($others_contractors as $contractor)

                                                        <?php
                                                            $scope_contractor = explode(':', $contractor);
                                                        ?>
                                                            <div class="col-xs-4" @if (!$loop->last) style="padding-right: 5px" @endif>
                                                                {{ strtoupper(array_get($scope_contractor, 0, '')) }}: <u>{{ array_get($scope_contractor, 1, '') }}</u>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>

                                            @endif

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            @endforeach

                        </div>
                    </div>
                </div>
            </div>

        @endif

    @endforeach

@endif

</body>
</html>
