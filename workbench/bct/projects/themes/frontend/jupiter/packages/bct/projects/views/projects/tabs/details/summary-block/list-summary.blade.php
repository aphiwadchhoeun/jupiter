<span class="navbar-brand">{{ trans("bct/projects::lists/common.title") }} <span id="lists-counter" class="text14">( {{ $lists->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $lists->count() }} )</span></span>
@if (!$currentUser->inRole('pp-guest'))

<a data-id="#lists" id="lists_tab" name="lists_tab" class="btn btn-primary go-to-tab pull-right"
   data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
            class="fa fa-edit fa-fw"></i></a>
@endif
{{--<a href="{{ route('bct.projects.projects.createLists', $project->id) }}"
    data-modal
   class="btn btn-success pull-right margin-right5 lists-add-btn" data-toggle="tooltip"
   data-original-title="{{{ trans('action.add') }}}">
    <i class="fa fa-plus fa-fw"></i>
</a>--}}


<table class="table table-striped table-bordered">
    <tbody>
    <tr>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.name') }}</td>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.category') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.group') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.status') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.date_added') }}</td>

    </tr>
    @foreach($lists->take(5) as $list)
        <tr>
            <td class="td-value">{{ $list->name }}</td>
            <td class="td-value text-center">{{ $list->category }}</td>
            <td class="td-value text-center">{{ $list->group->name }}</td>
            <td class="td-value text-center"><span class="label @if($list->status_name=="Active") label-success @else label-danger @endif">{{ $list->status_name }}</span></td>
            <td class="td-value text-center">{{ date_correct_details_projects( $list->due_date ) }}</td>
        </tr>
    @endforeach
</table>