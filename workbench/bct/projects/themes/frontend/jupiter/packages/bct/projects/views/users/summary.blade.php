@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('validate', 'platform/js/parsley.min.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}

    {{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}

    {{ Asset::queue('user-details', 'bct/projects::users/js/details.js', 'extension-base') }}
    {{ Asset::queue('add-delete', 'bct/projects::users/js/add-delete.js', 'extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-grid">
        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">

                    <ul class="nav nav-tabs">
                        <li class="dropdown">
                            <a href="#details" data-toggle="tab">
                                {{ trans("bct/projects::lists/common.tabs.details") }}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                <li class=""><a href="#details" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">@fat</a></li>
                                <li class=""><a href="#details-edit" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">@mdo</a></li>
                            </ul>
					    </li>
                        <li>
                            <a href="#groups" data-toggle="tab">
                                {{ trans("bct/projects::users/common.tabs.groups") }}
                            </a>
                        </li>
                        <li>
                            <a href="#visits" data-toggle="tab">
                                {{ trans('bct/projects::users/common.tabs.visits') }}
                            </a>
                        </li>
                       
                    </ul>

                    <div class="tab-content">
                         
                        <div role="tabpanel" class="tab-pane fade" id="details">
                            @include('bct/projects::users.tabs.details.tab')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="details-edit">
                            @include('bct/projects::users.tabs.details.tab-edit')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="groups">
                            @include('bct/projects::users.tabs.groups.tab')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="visits">
                            @include('bct/projects::users.tabs.visits.tab')
                        </div>
                        
                    </div>

                </div>

            </div>
        </div>

    </section>

@stop
