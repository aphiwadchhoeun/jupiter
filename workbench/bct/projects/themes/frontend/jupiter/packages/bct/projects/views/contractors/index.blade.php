@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::contractors/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    @if ($user->inRole('admin') || $user->inRole('repadmin') || $user->inRole('super-repadmin'))
        {{ Asset::queue('index', 'bct/projects::contractors/js/index_merge.js', ['extension-base']) }}
    @else
        {{ Asset::queue('index', 'bct/projects::contractors/js/index.js', ['extension-base']) }}
    @endif
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::contractors/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="contractors" role="form">

                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" disabled>
                                        {{{ trans('common.status') }}}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button"
                                            data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="#" data-filter="is_union:1" data-reset>
                                                   {{ trans('bct/projects::general/common.is_union') }}
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-filter="is_union:0" data-reset>
                                                    {{ trans('bct/projects::general/common.is_not_union') }}
                                                </a>
                                            </li>
                                    </ul>
                                </span>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::general/common.active') }}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button"
                                            data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="#" data-filter="active:1" data-reset>
                                                   {{ trans('bct/projects::general/common.yes') }}
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-filter="active:0" data-reset>
                                                    {{ trans('bct/projects::general/common.no') }}
                                                </a>
                                            </li>
                                    </ul>
                                </span>

                                <span class="input-group-btn">
                                    <select name="column" class="form-control">

                                        <option value="name">{{ trans('bct/projects::contractors/model.general.name') }}
                                        </option>
                                        <option value="city">{{ trans('bct/projects::contractors/model.general.city') }}
                                        </option>
                                        <option value="state">{{ trans('bct/projects::contractors/model.general.state') }}
                                        </option>
                                        <option value="phone">{{ trans('bct/projects::contractors/model.general.phone') }}
                                        </option>
                                        <option value="license">{{ trans('bct/projects::contractors/model.general.license') }}
                                        </option>
                                        <option value="id">{{ trans('bct/projects::contractors/model.general.id') }}
                                        </option>
                                    </select>
                                </span>

                                    <input class="form-control" name="filter" type="text"
                                           placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="contractors" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>

                        </form>

                        <div class="pull-right">

                            @if (!$user->inRole('pp-guest'))

                                @if ($user->inRole('admin') || $user->inRole('repadmin') || $user->inRole('super-repadmin'))
                                    <a id="merge-btn" data-toggle="tooltip"
                                       data-original-title="{{ trans('bct/projects::general/common.merge') }}"
                                       class="btn btn-primary disabled">
                                        <i class="fa fa-compress"></i>
                                    </a>
                                @endif


                                <a href="{{ route('bct.projects.contractors.create') }}" class="btn btn-success"
                                    id="contractor_new"
                                    name="contractor_new"
                                   data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
                                    <i class="fa fa-plus fa-fw"></i>
                                </a>
                            @endif

                        </div>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="contractors"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.contractors.grid') }}"
                       data-filename="{{ trans('bct/projects::contractors/common.title') }}"
                       data-grid="contractors">
                    <thead>
                    <tr>
                        @if ($user->inRole('admin') || $user->inRole('repadmin') || $user->inRole('super-repadmin'))
                            <th class="projects-checkbox-head"></th>
                        @endif
                        <th class="sortable col-md-1" data-sort="id">{{ trans('bct/projects::contractors/model.general.id') }}
                        </th>
                        <th class="sortable col-md-3" data-sort="name">{{ trans('bct/projects::contractors/model.general.name') }}
                        </th>
                        <th class="sortable col-md-2" data-sort="city">{{ trans('bct/projects::contractors/model.general.city') }}
                        </th>
                        <th class="sortable col-md-1" data-sort="state">{{ trans('bct/projects::contractors/model.general.state') }}
                        </th>
                        <th class="sortable col-md-2" data-sort="phone">{{ trans('bct/projects::contractors/model.general.phone') }}
                        </th>
                        <th class="sortable col-md-2" data-sort="license">{{ trans('bct/projects::contractors/model.general.license') }}
                        </th>
                        <th class="sortable col-md-1" data-sort="is_union">{{ trans('bct/projects::general/form.union') }}
                        </th>
                        <th class="sortable col-md-1" data-sort="active">{{ trans('bct/projects::general/common.active') }}</th>
                        <th class="col-md-1">{{ trans('bct/projects::general/common.details') }}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="contractors"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::contractors.grid.index.results')
        @include('bct/projects::contractors.grid.index.pagination')
        @include('bct/projects::contractors.grid.index.filters')
        @include('bct/projects::contractors.grid.index.no_results')

    </section>

@stop
