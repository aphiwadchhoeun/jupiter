<script type="text/template" data-grid="scopes" data-template="results">

	<% _.each(results, function(r) { %>
		<tr data-grid-row>
			<td><%- r.name %></td>
			<td><%- r.value %></td>
			<td><% if(!r.classification) { %>
					<span class="text-muted">Default</span>
				<% } else { %>
					<%- r.classification %>
				<% } %>
			</td>
			<td><%- r.order %></td>
            <td>
                <a href="<%- r.view_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}" data-modal><i
                            class="fa fa-book fa-fw"></i></a>
            </td>
		</tr>

	<% }); %>

</script>
