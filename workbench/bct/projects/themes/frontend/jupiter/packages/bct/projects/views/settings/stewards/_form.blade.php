<input type="hidden" name="is_steward" value="1">

<div class="panel-body">

    <div class="col-md-12">

        {{-- Form: General --}}
        <fieldset>
            <div class="row">

                {{--add more form-group for other fields, just keep the same uniform--}}
                <div class="form-group{{ Alert::onForm('first_name', ' has-error') }}">

                    <label for="first_name" class="control-label">
                        {{{ trans('bct/projects::users/model.general.first_name') }}}
                    </label>

                    <input type="text" class="form-control" name="first_name" id="first_name"
                           placeholder="{{{ trans('bct/projects::users/model.general.first_name_help') }}}"
                           value="{{{ input()->old('first_name', $user->first_name) }}}">

                    <span class="help-block">{{{ Alert::onForm('first_name') }}}</span>

                </div>

                <div class="form-group{{ Alert::onForm('last_name', ' has-error') }}">

                    <label for="last_name" class="control-label">
                        {{{ trans('bct/projects::users/model.general.last_name') }}}
                    </label>

                    <input type="text" class="form-control" name="last_name" id="last_name"
                           placeholder="{{{ trans('bct/projects::users/model.general.last_name_help') }}}"
                           value="{{{ input()->old('last_name', $user->last_name) }}}">

                    <span class="help-block">{{{ Alert::onForm('last_name') }}}</span>

                </div>

                <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                    <label for="email" class="control-label">
                        {{{ trans('bct/projects::users/model.general.email') }}}
                    </label>

                    <input type="text" class="form-control" name="email" id="email"
                           placeholder="{{{ trans('bct/projects::users/model.general.email_help') }}}"
                           value="{{{ input()->old('email', $user->email) }}}">

                    <span class="help-block">{{{ Alert::onForm('email') }}}</span>

                </div>

                <div class="form-group{{ Alert::onForm('phone', ' has-error') }}">

                    <label for="phone" class="control-label">
                        {{{ trans('bct/projects::users/model.general.phone') }}}
                    </label>

                    <input type="text" class="form-control" name="phone" id="phone" data-mask
                           placeholder="{{{ trans('bct/projects::users/model.general.phone_help') }}}"
                           value="@if ($user->profile) {{{ input()->old('phone', $user->profile->business_phone) }}} @endif">

                    <span class="help-block">{{{ Alert::onForm('phone') }}}</span>

                </div>

                <div class="form-group{{ Alert::onForm('local', ' has-error') }}">

                    <label for="phone" class="control-label">
                        {{{ trans('bct/projects::users/model.general.local') }}}
                    </label>

                    <input type="text" class="form-control" name="local" id="local" maxlength="30"
                           placeholder="{{{ trans('bct/projects::users/model.general.local_help') }}}"
                           value="@if ($user->profile){{{ input()->old('local', $user->profile->business_local) }}}@endif">

                    <span class="help-block">{{{ Alert::onForm('local') }}}</span>

                </div>
                <input type="hidden" name="role" value="{{ data_get($role, 'id', 0) }}">

            </div>

        </fieldset>

    </div>

</div>

<div class="panel-footer">
    @if (request()->ajax())
        <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                data-original-title="{{{ trans('action.save') }}}">
            <i class="fa fa-save fa-fw"></i>{{{ trans('action.save') }}}
        </button>

        <div class="pull-right">
            @if ($user->exists)
                <a href="{{ route('bct.projects.stewards.delete', $user->id) }}"
                   id="steward_delete"
                   name="steward_delete"
                   class="btn btn-danger"
                   data-action-delete data-toggle="tooltip"
                   data-original-title="{{{ trans('action.delete') }}}" type="delete">
                    <i class="fa fa-trash-o fa-fw"></i>{{{ trans('action.delete') }}}</a>
            @endif

            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('action.cancel') }}
            </button>
        </div>
    @endif
</div>