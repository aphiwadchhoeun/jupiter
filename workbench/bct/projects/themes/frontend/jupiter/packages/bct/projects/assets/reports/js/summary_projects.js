var Extension;
var checked_rows = [];

;(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'summary_projects'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.datePicker(Extension);
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid-calendar-preset]', Extension.Index.calendarPresets)
            .on('click', '#data-grid_applied_custom button', Extension.Index.clickFilterButton)

        ;
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    Extension.Index.clickFilterButton = function(e) {
        e.preventDefault();

        var column = $(this).data('column');

        Extension.Index.refreshSearch();
        Extension.Index.removeFilter(column);
    };

    Extension.Index.refreshSearch = function() {
        $('form [name="filter"]').val('');
    };

    Extension.Index.syncFilters = function(filter) {
        var filters = _.without(Extension.Index.filters, _.findWhere(Extension.Index.filters, {
            column: filter.column
        }));

        filters.push(filter);

        Extension.Index.filters = filters;
    };

    Extension.Index.removeFilter = function(column) {
        var filters = _.without(Extension.Index.filters, _.findWhere(Extension.Index.filters, {
            column: column
        }));

        Extension.Index.filters = filters;

        if (column === 'created_at') {
            Extension.Index.removeParam('start');
            Extension.Index.removeParam('end');
        } else {
            Extension.Index.removeParam(column);
        }

        Extension.Index.Grid.refresh();
    };

    Extension.Index.setGetParameter = function(paramName, paramValue)
    {
        if (typeof Extension.Index.Grid === 'undefined') {
            var url = $('table[data-grid="' + Extension.Index.grid + '"]').data('source');
        } else {
            var url = Extension.Index.Grid.source;
        }

        if (url.indexOf(paramName + "=") >= 0)
        {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
        }
        else
        {
            if (url.indexOf("?") < 0)
                url += "?" + paramName + "=" + paramValue;
            else
                url += "&" + paramName + "=" + paramValue;
        }

        if (typeof Extension.Index.Grid === 'undefined') {
            $('table[data-grid="' + Extension.Index.grid + '"]').data('source', url);
        } else {
            Extension.Index.Grid.source = url;
        }
    };



    Extension.Index.removeParam = function(key) {
        var sourceURL = Extension.Index.Grid.source,
            rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";

        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];

                if (param.indexOf(key) !== -1) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        Extension.Index.Grid.source = rtn;
    };

    // Handle Data Grid calendar
    Extension.Index.calendarPresets = function (event) {
        event.preventDefault();

        var start, end;

        switch ($(this).data('grid-calendar-preset')) {
            case 'day':
                start = end = moment().subtract(1, 'day').startOf('day').format('MM/DD/YYYY');
                break;

            case 'week':
                start = moment().startOf('week').format('MM/DD/YYYY');
                end = moment().endOf('week').format('MM/DD/YYYY');
                break;

            case 'month':
                start = moment().startOf('month').format('MM/DD/YYYY');
                end = moment().endOf('month').format('MM/DD/YYYY');
                break;

            case 'last30days':
                start = moment().subtract('29','days').format('MM/DD/YYYY');
                end = moment().format('MM/DD/YYYY');
                break;

            case 'last90days':
                start = moment().subtract('89','days').format('MM/DD/YYYY');
                end = moment().format('MM/DD/YYYY');
                break;

            default:
        }

        var $daterange_start_date = $('[data-grid="' + Extension.Index.grid + '"] [data-range-start]'),
            $daterange_end_date = $('[data-grid="' + Extension.Index.grid + '"] [data-range-end]');


        $daterange_start_date.val(start);
        $daterange_end_date.val(end);
        $daterange_start_date.trigger('change');
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            //hash: false,
            events: {
                fetching: function (response) {

                    if (!Extension.Index.filters || Extension.Index.filters.length === 0) {
                        var url_parameters = Extension.Index.URLToArray(response.source);

                        var created_start_date,
                            created_end_date;

                        _.mapObject(url_parameters, function(value, key) {
                            if (key.indexOf('status') !== -1) {
                                Extension.Index.syncFilters({
                                    column: 'status',
                                    field_text: 'Status',
                                    value: value
                                });

                            } else if (key.indexOf('contractor_name')!== -1) {
                                Extension.Index.syncFilters({
                                    column: 'contractor',
                                    field_text: 'Contractor',
                                    value: value
                                });

                            } else if (key.indexOf('region')!== -1) {
                                Extension.Index.syncFilters({
                                    column: 'region',
                                    field_text: 'Region',
                                    value: value
                                });
                            } else if (key.indexOf('classification_name')!== -1) {
                                Extension.Index.syncFilters({
                                    column: 'classification',
                                    field_text: 'Project Classification',
                                    value: value
                                });
                            } else if (key.indexOf('project_type_name')!== -1) {
                                Extension.Index.syncFilters({
                                    column: 'project_type',
                                    field_text: 'Project Type',
                                    value: value
                                });
                            } else if (key.indexOf('tag_name')!== -1) {
                                Extension.Index.syncFilters({
                                    column: 'tag',
                                    field_text: 'Tag',
                                    value: value
                                });

                            } else if (key.indexOf('start')!== -1) {
                                created_start_date = value;
                            } else if (key.indexOf('end')!== -1) {
                                created_end_date = value;
                            }

                        });

                        if (created_start_date && created_end_date) {
                            Extension.Index.syncFilters({
                                column: 'created_at',
                                from:  created_start_date,
                                to: created_end_date,
                                type: ''
                            });
                        }

                    }
                },
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }

                    var template = _.template( $('[data-grid="summary_projects"][data-template="filters_custom"]').html() );
                    $('#data-grid_applied_custom').html( template({ filters: Extension.Index.filters }) );
                },
            },
            callback: function () {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

            }
        };

        Extension.Index.Grid = $.datagrid(Extension.Index.grid, '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    Extension.Index.URLToArray = function(url) {
        var request = {};
        var pairs = url.substring(url.indexOf('?') + 1).split('&');
        for (var i = 0; i < pairs.length; i++) {
            if(!pairs[i])
                continue;
            var pair = pairs[i].split('=');
            request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]).split('+').join(' ');
        }
        return request;
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
