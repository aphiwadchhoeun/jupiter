<header class="panel-heading hidden-print">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#actions">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip go-to-details" data-toggle="tooltip"
                           data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $list->name}} {{ trans("bct/projects::lists/common.tabs.reps") }}</span>

            </div>
        </div>

    </nav>

</header>

<div class="panel-body">

    <div class="row visible-print">
        <h4 class="overview-heading text-center visible-print">
            {{ data_get($list, 'name', '') }} -
            {{ trans("bct/projects::lists/common.title") }} {{ trans("bct/projects::lists/common.tabs.reps") }}
        </h4>
    </div>

    <div class="row">

        <div class="col-md-12">

            <section class="panel panel-default panel-grid">

                {{-- Grid: Header --}}
                <header class="panel-heading hidden-print">

                    <nav class="navbar navbar-default navbar-actions">

                        <div class="container-fluid">

                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#actions">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            {{-- Grid: Actions --}}
                            <div class="collapse navbar-collapse" id="actions">
                             <span class="navbar-brand">{{ trans('bct/projects::lists/common.title') }} {{ trans('bct/projects::lists/common.tabs.reps') }}</span>

                                <ul class="nav navbar-nav navbar-left" data-grid="reps-info">

                                    <li class="dropdown">
                                        <a href="#" id="lists-reps-exporter"
                                           class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                           aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                            <i class="fa fa-print"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                            <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                        </ul>
                                    </li>

                                </ul>
                                {{-- Grid: Filters --}}
                                <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                                      data-grid="reps-info" role="form">

                                    <div class="input-group">

                                           <span class="input-group-btn">

                                               <button class="btn btn-default" type="button" disabled>
                                                   {{{ trans('common.status') }}}
                                               </button>

                                               <button class="btn btn-default dropdown-toggle" type="button"
                                                       data-toggle="dropdown" aria-expanded="false">
                                                   <span class="caret"></span>
                                                   <span class="sr-only">Toggle Dropdown</span>
                                               </button>

                                               <ul class="dropdown-menu" role="menu">

                                                   <li>
                                                       <a href="#" data-filter="activations.completed:1" id="list-available-project-filter-status-active">
                                                           Active
                                                       </a>
                                                   </li>

                                                   <li>
                                                       <a href="#" data-filter="activations.completed:0" id="list-available-project-filter-status-disabled">
                                                           Disabled
                                                       </a>
                                                   </li>

                                               </ul>

                                           </span>

                                           <span class="input-group-btn">
                                               <select name="column" class="form-control" id="list-available-project-filter-select">
                                                   <option value="name">{{ trans('bct/projects::users/model.general.name') }}
                                                   </option>
                                                   <option value="z_user_profiles.business_phone">{{ trans('bct/projects::users/model.general.phone') }}
                                                   </option>
                                                   <option value="email">{{ trans('bct/projects::users/model.general.email') }}
                                                   </option>
                                                   <option value="roles.name">{{ trans('bct/projects::users/model.general.role') }}
                                                   </option>
                                               </select>
                                           </span>
                                        <input class="form-control" name="filter" type="text" id="list-available-project-filter-input-search"
                                               placeholder="{{{ trans('common.search') }}}">

                                        <span class="input-group-btn">

                                            <button class="btn btn-default" type="submit" id="list-available-project-filter-btn-search">
                                                <span class="fa fa-search"></span>
                                            </button>

                                            <button class="btn btn-default" data-grid="reps-info" data-reset id="list-available-project-filter-btn-reset">
                                                <i class="fa fa-refresh fa-sm"></i>
                                            </button>

                                        </span>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </nav>

                </header>

                <div class="panel-body">

                    {{-- Grid: Applied Filters --}}
                    <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                        <div id="data-grid-reps-info_applied" class="btn-group" data-grid="reps-info"></div>

                    </div>

                    {{-- Grid: Table --}}
                    <div class="table-responsive">

                        <table id="data-grid-reps-info" class="table table-hover"
                               data-source="{{ route('bct.projects.lists.gridForReps', $list->id) }}"
                               data-filename="{{ $list->name }} {{ trans("bct/projects::lists/common.tabs.reps") }}"
                               data-grid="reps-info">
                            <thead>
                            <tr>
                                <th class="sortable col-md-3"
                                    data-sort="name">{{ trans('bct/projects::users/model.general.name') }}</th>
                                <th class="sortable col-md-2"
                                    data-sort="phone">{{ trans('bct/projects::users/model.general.phone') }}</th>
                                <th class="sortable col-md-3"
                                    data-sort="email">{{ trans('bct/projects::users/model.general.email') }}</th>
                                <th class="sortable col-md-1"
                                    data-sort="status">{{ trans('bct/projects::users/model.general.status') }}</th>
                                <th class="col-md-1">{{ trans('bct/projects::users/model.general.role') }}</th>
                                <th class="sortable col-md-2"
                                    data-sort="created_at">{{ trans('bct/projects::users/model.general.created_at') }}
                                </th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>

                    </div>

                </div>

                <footer class="panel-footer clearfix hidden-print">

                    {{-- Grid: Pagination --}}
                    <div class="data-grid-reps-info_pagination" data-grid="reps-info"></div>

                </footer>

                {{-- Grid: templates --}}
                @include('bct/projects::lists.tabs.reps.grid.reps-info.results')
                @include('bct/projects::lists.tabs.reps.grid.reps-info.pagination')
                @include('bct/projects::lists.tabs.reps.grid.reps-info.filters')
                @include('bct/projects::lists.tabs.reps.grid.reps-info.no_results')

            </section>

        </div>

    </div>

</div>
