<script type="text/template" data-grid="groups-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.name %></td>
        <td class="text-center">
            <% if(r.status == 'Active') { %>
                <span class="label label-success"><%- r.status %></span>
            <% } else { %>
                <span class="label label-danger"><%- r.status %></span>
            <% } %>
        </td>
    </tr>

    <% }); %>

</script>
