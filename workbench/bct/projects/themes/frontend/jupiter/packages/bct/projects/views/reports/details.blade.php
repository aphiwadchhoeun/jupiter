@extends('bct/projects::default')


{{-- Page title --}}
@section('title')
    @parent
    {{ $title }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}

{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}

{{ Asset::queue('index', 'bct/projects::reports/js/summary_projects.js', 'extension-base') }}


{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')



    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{route('bct.projects.reports.'.$page)}}" data-toggle="tooltip" data-original-title="Cancel">
                                    <i class="fa fa-reply"></i> <span class="visible-xs-inline">Cancel</span>
                                </a>
                            </li>
                        </ul>

                        <span class="navbar-brand">{{{ $title }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                       <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span
                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="summary_projects" role="form">

                            <div class="input-group">

                                <span class="input-group-btn">
                                    <select name="column" class="form-control">
                                        <option value="z_projects.name">{{ trans('bct/projects::projects/model.general.name') }}
                                        </option>
                                    </select>
                                </span>

                                <input class="form-control" name="filter" type="text" placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="projects" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>
                        </form>

                    </div>



                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="summary_projects"></div>
                <div id="data-grid_applied_custom" class="btn-group" data-grid="summary_projects"></div>

            </div>

            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ $grid_url }}"
                       data-filename="{{ $title }}"
                       data-grid="summary_projects">
                    <thead>
                    <tr>
                        <th class="sortable col-md-8" data-sort="project_name">{{ trans('bct/projects::reports/common.col.name_project') }}</th>
                        <th class="sortable col-md-3" data-sort="last_visit">{{ trans('bct/projects::reports/common.col.last_visit') }}</th>
                        <th class="col-md-1">{{ trans('bct/projects::general/common.details') }}</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="summary_projects"></div>

        </footer>



        {{-- Grid: templates --}}
        @include('bct/projects::reports.grid.summary_projects.results')
        @include('bct/projects::reports.grid.summary_projects.pagination')
        @include('bct/projects::reports.grid.summary_projects.filters')
        @include('bct/projects::reports.grid.summary_projects.no_results')

    </section>


@stop
