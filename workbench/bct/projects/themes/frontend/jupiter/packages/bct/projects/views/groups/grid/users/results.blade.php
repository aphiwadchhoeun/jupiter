<script type="text/template" data-grid="users" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td>
            <input input="" type="hidden" value="<%- r.id %>">
            <%- r.full_name %>
        </td>
        <td><a class="under-none" href="mailto:<%- r.email %>"><%- r.email %></a></td>
        <td><%- r.roles %></td>
        <td>
            <% if(r.status == 'Active') { %>
            <span class="label label-success"><%- r.status %></span>
            <% } else { %>
            <span class="label label-danger"><%- r.status %></span>
            <% } %>
        </td>
        <td>
            <button class="btn btn-success btn-circle add-to-group" data-toggle="tooltip" data-placement="right"
               data-original-title="{{{ trans('bct/projects::groups/common.add_to_group') }}}"><i
                        class="fa fa-plus fa-fw"></i></button>
        </td>
    </tr>

    <% }); %>

</script>
