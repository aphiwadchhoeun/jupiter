@extends('bct/projects::default')

{{-- Page title --}}
@section('title'){{--
    --}}@parent{{--
    --}} Projects Report{{--
--}}@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('selectize', 'bower_components/selectize/dist/js/standalone/selectize.min.js', 'jquery') }}
    {{ Asset::queue('encodeurl', 'bct/projects::custom/js/encodeurl.js', 'jquery') }}

    {{ Asset::queue('report2', 'bct/projects::reports/js/report2.js', ['jquery', 'swal']) }}

@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('selectize-css', 'bower_components/selectize/dist/css/selectize.css') }}
    {{ Asset::queue('selectize-bootstrap3', 'bower_components/selectize/dist/css/selectize.bootstrap3.css', 'bootstrap') }}
@stop

{{-- Page --}}
@section('page-extension')
    @parent

    {{-- Grid --}}
    <section class="panel panel-default">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{route('bct.projects.reports.index')}}" data-toggle="tooltip" data-original-title="Cancel">
                                    <i class="fa fa-reply"></i> <span class="visible-xs-inline">Cancel</span>
                                </a>
                            </li>
                        </ul>
                        <span class="navbar-brand">{{ trans('bct/projects::reports/common.tabs.report2_projects') }}</span>
                    </div>

                </div>

            </nav>

        </header>


        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">

                    <nav class="navbar navbar-default navbar-actions">


                            <div class="nav navbar-nav navbar-left">
                                <b>{{ trans('bct/projects::reports/common.text.report2.select_regions') }}:</b>
                            </div>

                            <div class="nav navbar-nav navbar-left margin-hor-sides">
                                <select name="regions[]" id="regions-report" class="form-control w300" multiple>
                                    <option value="">{{ trans('bct/projects::reports/common.text.report2.select_regions') }}...</option>

                                </select>
                            </div>

                            <button class="btn btn-default navbar-left regions-report-select-all margin-hor-sides">{{ trans('bct/projects::general/common.select_all') }}</button>
                            <button class="btn btn-default navbar-left regions-report-deselect-all hide margin-hor-sides">{{ trans('bct/projects::general/common.deselect_all') }}</button>

                            <div class="nav navbar-nav navbar-left ">
                                <a href="#"
                                   class="btn btn-link pull-right btn-download-pdf with-select hide"
                                   data-toggle="tooltip"
                                   data-original-title="{{{ trans('bct/projects::general/common.print') }}}">
                                    <i class="fa fa-download fa-fw"></i>
                                </a>
                            </div>

                    </nav>

                </div>

            </div>


            <div class="row">
                <div class="loader" style="position: static">
                    <div class="sk-circle">
                        <div class="sk-circle1 sk-child"></div>
                        <div class="sk-circle2 sk-child"></div>
                        <div class="sk-circle3 sk-child"></div>
                        <div class="sk-circle4 sk-child"></div>
                        <div class="sk-circle5 sk-child"></div>
                        <div class="sk-circle6 sk-child"></div>
                        <div class="sk-circle7 sk-child"></div>
                        <div class="sk-circle8 sk-child"></div>
                        <div class="sk-circle9 sk-child"></div>
                        <div class="sk-circle10 sk-child"></div>
                        <div class="sk-circle11 sk-child"></div>
                        <div class="sk-circle12 sk-child"></div>
                    </div>
                </div>
            </div>


            <div class="row report-results"></div>

        </div>

        <script type="text/template" id="report_results_template">
            <% if (Object.keys(results).length > 0) { %>

                 <%  _.each(results, function(projects, region) { %>

                    <% if (projects.length > 0) { %>

                    <div class="row">

                        <div class="col-lg-12">

                            <div class="navbar-brand text-center w100">Active Projects - <%- region %></div>

                            <div style="clear:both; height: 10px;"></div>

                                <div class="table-responsive">
                                    <table class="table table-borderd">
                                        <thead>
                                            <tr>
                                                <th class="col-lg-1">{{ trans('bct/projects::reports/common.col.project_id') }}</th>
                                                <th class="col-lg-4">{{ trans('bct/projects::reports/common.col.project_name') }}</th>
                                                <th class="col-lg-3">{{ trans('bct/projects::reports/common.col.project_address') }}</th>
                                                <th class="col-lg-1">{{ trans('bct/projects::reports/common.col.project_city') }}</th>
                                                <th class="col-lg-3">{{ trans('bct/projects::reports/common.col.stewards') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <%  _.each(projects, function(project, key) { %>

                                            <tr>
                                                <td><%- project.project_id %></td>
                                                <td><%- project.project_name %></td>
                                                <td><%- project.project_address %></td>
                                                <td><%- project.project_city %></td>
                                                <td><%- project.stewards %></td>
                                            </tr>

                                        <% }) %>
                                        </tbody>
                                    </table>
                                </div>

                        </div>

                    </div>

                    <% } %>

                <% }) %>

            <% } %>

        </script>

    </section>

@stop
