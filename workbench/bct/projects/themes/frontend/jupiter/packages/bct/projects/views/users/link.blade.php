@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::users/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}
    {{ Asset::queue('mrpuser-link', 'bct/projects::users/js/link.js', 'bootstrap-toggle') }}
    {{ Asset::queue('groups', 'bct/projects::users/js/groups.js', 'extension-base') }}
    {{ Asset::queue('groups-added', 'bct/projects::users/js/groups-added.js', 'extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Asset::queue('user-link', 'bct/projects::users/css/link.css') }}
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-tabs">

        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip" href="{{ route('bct.projects.users.all') }}" data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{{ trans("bct/projects::users/common.title") }}} {{{ trans('bct/projects::users/common.settings') }}}</span>
                        </div>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('bct.projects.users.resetPassword', $user->id) }}" class="btn btn-primary pull-left margin-right5"
                           data-toggle="tooltip"
                           data-original-title="{{{ trans('bct/projects::general/common.reset') }}}"><i
                                    class="fa fa-key fa-fw"></i>{{{ trans('bct/projects::general/common.reset') }}}</a>
                        <a href="{{ route('bct.projects.users.edit', $user->id) }}"
                           id="user_edit"
                           name="user_edit"
                           class="btn btn-primary pull-left margin-right5"
                           data-toggle="tooltip"
                           data-original-title="{{{ trans('bct/projects::general/common.edit') }}}"><i
                                    class="fa fa-edit fa-fw"></i>{{{ trans('bct/projects::general/common.edit') }}}</a>

                        <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                data-original-title="{{{ trans('action.save') }}}">
                            <i class="fa fa-save fa-fw"></i>{{{ trans('action.save') }}}</button>
                    </div>
                </div>

                {{-- Form: General --}}
                <fieldset>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                <tr>
                                    <td class="td-label">{{{ trans('bct/projects::users/model.general.first_name') }}}</td>
                                    <td>{{ $user->first_name }}</td>
                                </tr>
                                <tr>
                                    <td class="td-label">{{{ trans('bct/projects::users/model.general.last_name') }}}</td>
                                    <td>{{ $user->last_name }}</td>
                                </tr>
                                <tr>
                                    <td class="td-label">{{{ trans('bct/projects::users/model.general.email') }}}</td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <td class="td-label">{{{ trans('bct/projects::users/model.general.phone') }}}</td>
                                    <td>{{ ($user->profile) ? $user->profile->business_phone : '' }}</td>
                                </tr>
                                <tr>
                                    <td class="td-label">{{{ trans('bct/projects::users/model.general.role') }}}</td>
                                    <td>
                                        <div class="form-group">
                                            
                                            <?php 
                                                $user_roles = $user->roles()->lists('name');
                                                echo $user_roles->implode(", ");
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label">{{{ trans('bct/projects::users/model.general.status') }}}</div>
                            <?php
                            $status = $user->activated;
                            $activated = $status ? 'checked' : '';
                            ?>
                            <div class="form-value">
                                <input type="checkbox" name="activated" id="activated" value="1" {{ $activated }}/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </form>
        <div class="panel-body">
            <hr/>

            <div class="row">
                <div class="col-md-6">
                    <div class="clearfix">
                        <span class="navbar-brand">{{{ trans('bct/projects::users/common.not_assigned_groups') }}}</span>

                        <div class="nav navbar-nav navbar-right">
                        <span id="btn-add-groups" class="btn btn-success pull-right disabled" data-toggle="tooltip"
                              data-original-title="{{{ trans('bct/projects::users/common.add_to_all_group') }}}">
                            <i class="fa fa-plus">&nbsp;{{{ trans('bct/projects::users/common.add_to_all_group') }}}</i>
                        </span>
                        </div>
                    </div>
                    {{-- Grid: Header --}}
                    <header class="panel-heading">

                        <nav class="navbar navbar-default navbar-actions">

                            <div class="container-fluid">

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#actions">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                {{-- Grid: Actions --}}
                                <div class="collapse navbar-collapse" id="actions">

                                    {{-- Grid: Filters --}}
                                    <form class="navbar-form navbar-right" method="post" accept-charset="utf-8" data-search
                                          data-grid="groups" role="form">

                                        <div class="input-group">

                                        <span class="input-group-btn">
                                            <input class="form-control" name="filter" type="text"
                                                   placeholder="{{{ trans('common.search') }}}">
                                            <button class="btn btn-default" type="submit">
                                                <span class="fa fa-search"></span>
                                            </button>

                                            <button class="btn btn-default" data-grid="groups" data-reset>
                                                <i class="fa fa-refresh fa-sm"></i>
                                            </button>

                                        </span>

                                        </div>

                                    </form>

                                </div>

                            </div>

                        </nav>

                    </header>

                    <div class="panel-body">

                        {{-- Grid: Applied Filters --}}
                        <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                            <div id="data-grid_applied" class="btn-group" data-grid="groups"></div>

                        </div>


                        {{-- Grid: Table --}}
                        <div class="table-responsive">

                            <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.users.gridGroups', $user->id) }}"
                                   data-grid="groups">
                                <thead>
                                <tr>
                                    <th><input data-grid-checkbox="all" type="checkbox"></th>
                                    <th class="sortable" data-sort="name">{{ trans('bct/projects::groups/model.general.name') }}</th>
                                    <th>{{{ trans('bct/projects::general/common.actions') }}}</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>

                    </div>


                    <footer class="panel-footer">

                        {{-- Grid: Pagination --}}
                        <div class="data-grid_pagination clearfix" data-grid="groups"></div>

                    </footer>

                    {{-- Grid: templates --}}
                    @include('bct/projects::users.grid.groups.results')
                    @include('bct/projects::users.grid.groups.pagination')
                    @include('bct/projects::users.grid.groups.filters')
                    @include('bct/projects::users.grid.groups.no_results')
                </div>
                <div class="col-md-6">
                    <div class="clearfix">
                        <span class="navbar-brand">{{{ trans('bct/projects::users/common.assigned_groups') }}}</span>

                        <div class="nav navbar-nav navbar-right">
                        <span id="btn-remove-groups" class="btn btn-danger pull-right disabled" data-toggle="tooltip"
                              data-original-title="{{{ trans('bct/projects::users/common.remove_from_all_group') }}}">
                            <i class="fa fa-minus">&nbsp;{{{ trans('bct/projects::users/common.remove_from_all_group') }}}</i>
                        </span>
                        </div>
                    </div>
                    {{-- Grid: Header --}}
                    <header class="panel-heading">

                        <nav class="navbar navbar-default navbar-actions">

                            <div class="container-fluid">

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#actions">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                {{-- Grid: Actions --}}
                                <div class="collapse navbar-collapse" id="actions">

                                    {{-- Grid: Filters --}}
                                    <form class="navbar-form navbar-right" method="post" accept-charset="utf-8" data-search
                                          data-grid="groups-added" role="form">

                                        <div class="input-group">

                                        <span class="input-group-btn">
                                            <input class="form-control" name="filter" type="text"
                                                   placeholder="{{{ trans('common.search') }}}">
                                            <button class="btn btn-default" type="submit">
                                                <span class="fa fa-search"></span>
                                            </button>

                                            <button class="btn btn-default" data-grid="groups-added" data-reset>
                                                <i class="fa fa-refresh fa-sm"></i>
                                            </button>

                                        </span>

                                        </div>

                                    </form>

                                </div>

                            </div>

                        </nav>

                    </header>

                    <div class="panel-body">

                        {{-- Grid: Applied Filters --}}
                        <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                            <div id="data-grid_applied" class="btn-group" data-grid="groups-added"></div>

                        </div>


                        {{-- Grid: Table --}}
                        <div class="table-responsive">

                            <table id="data-grid-added" class="table table-hover" data-source="{{ route('bct.projects.users.gridGroups', [$user->id, 'reverse' => true]) }}"
                                   data-grid="groups-added">
                                <thead>
                                <tr>
                                    <th><input data-grid-checkbox="all" type="checkbox"></th>
                                    <th class="sortable" data-sort="name">{{ trans('bct/projects::groups/model.general.name') }}</th>
                                    <th>{{{ trans('bct/projects::general/common.actions') }}}</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>

                    </div>


                    <footer class="panel-footer">

                        {{-- Grid: Pagination --}}
                        <div class="data-grid_pagination clearfix" data-grid="groups-added"></div>

                    </footer>

                    {{-- Grid: templates --}}
                    @include('bct/projects::users.grid.groups-added.results')
                    @include('bct/projects::users.grid.groups-added.pagination')
                    @include('bct/projects::users.grid.groups-added.filters')
                    @include('bct/projects::users.grid.groups-added.no_results')
                </div>
            </div>

        </div>

    </section>
@stop
