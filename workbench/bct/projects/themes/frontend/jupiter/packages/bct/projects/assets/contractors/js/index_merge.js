var Extension;
var checked_rows = [];

/**
 * TODO: TEST ON SAFARI, KNOW ISSUE WITH IPAD
 * Array can't use computed index
 */

;(function (window, document, $, undefined) {
    $('#state').on('keypress', function (event) {
        if (null !== String.fromCharCode(event.which).match(/[a-z]/g)) {
            event.preventDefault();
            $(this).val($(this).val() + String.fromCharCode(event.which).toUpperCase());
        }
    });
    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'contractors'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid-row]', Extension.Index.checkRow)
            .on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
            .on('click', '#merge-btn', Extension.Index.mergeAction)
        ;

    };

    Extension.Index.mergeAction = function (event) {
         event.preventDefault();
    
         if (checked_rows.length == 2) {
             var one_id = parseInt(checked_rows[0].id),
                 two_id = parseInt(checked_rows[1].id);


             var resolve_obj = {};
             resolve_obj[one_id] = checked_rows[0].name+" (ID: "+checked_rows[0].id+")";
             resolve_obj[two_id] = checked_rows[1].name+" (ID: "+checked_rows[1].id+")";

             var inputOptions = new Promise(function (resolve) {
                 resolve(resolve_obj);
             });

             var confirm_url;

             $(document).on("click",".swal2-container input[name='swal2-radio']", function() {
                 var stay_id = $('input[name=swal2-radio]:checked').val();
                 var merge_id = $('input[name=swal2-radio]:not(:checked)').val();
                 confirm_url = '/pprofiles/contractors/merge/confirm/'+stay_id+'/'+merge_id;
             });

             swal({
                 title: 'Select project that you want to remain after merge',
                 input: 'radio',
                 inputOptions: inputOptions,
                 showLoaderOnConfirm: true,
                 showCancelButton: true,
                 inputValidator: function (result) {
                     return new Promise(function (resolve, reject) {
                         if (result) {
                             resolve();
                         } else {
                             reject('You need to select something!');
                         }
                     });
                 }
             }).then(function (result) {
                 window.location.href=confirm_url;
             })
         }
    
     };

    Extension.Index.bulkStatus = function () {
        var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

        $('[data-grid-bulk-action]').closest('li').toggleClass('disabled', !checked_rows);
        $('[data-grid-bulk-action]').toggleClass('disabled', !checked_rows);

        if (checked_rows > 0) {
            $('[data-grid-bulk-action="delete"]').attr('data-modal', true);
        }
        else {
            $('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
        }

        $('[data-grid-checkbox="all"]')
            .prop('disabled', rows < 1)
            .prop('checked', rows < 1 ? false : rows === checked_rows)
        ;
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxes = function (event) {
        event.stopPropagation();
        var checkbox = $(this);
        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all') {
            $('[data-grid-checkbox]').not(this).prop('checked', this.checked);

            $('[data-grid-row]').not(this).toggleClass('active', this.checked);
        }
        else{
            Extension.Index.saveChecked(checkbox);
        }

        $(this).parents('[data-grid-row]').toggleClass('active');

        Extension.Index.bulkStatus();
        Extension.Index.mergeStatus();

    };

    // Handle Data Grid row checking
    Extension.Index.checkRow = function () {
        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', !checkbox.prop('checked'));
        Extension.Index.saveChecked(checkbox);


        Extension.Index.bulkStatus();
        Extension.Index.mergeStatus();
    };

    Extension.Index.saveChecked = function(checkbox){
        var parent = $(checkbox).closest('tr'),
            $btn_details = $(parent).find('.btn-details');
        var r = {
            id: $btn_details.data('id'),
            name: $btn_details.data('name')
        };
        if ($(checkbox).prop('checked')) {
            checked_rows.push(r);
        }
        else{
            checked_rows = checked_rows.filter(function(el) {
                return el.id !== r.id;
            });
        }
    }

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

     Extension.Index.mergeStatus = function () {
         var count_rows = checked_rows.length;
         if (count_rows == 2) {
             $('#merge-btn').removeClass('disabled');
         } else {
             $('#merge-btn').addClass('disabled');
         }
     };

     // Applying any saved checked row on to the grid
     Extension.Index.applyCheckedRows = function () {
         _.each(checked_rows, function (item) {
             var row = $('[data-grid-checkbox][value=' + (item.id) + ']');
             row.prop('checked', true);
             row.closest('tr').toggleClass('active');
         });
     };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            hash: false,
            events: {
                removing: function (dg) {
                    _.each(dg.applied_filters, function (filter) {
                        if (filter.column === 'created_at' && filter.from !== undefined && filter.to !== undefined) {
                            $('[data-grid-calendar]').val('');
                        }
                    });
                },
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
            callback: function () {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

                Extension.Index.applyCheckedRows();
                Extension.Index.bulkStatus();
                Extension.Index.mergeStatus();
            }
        };

        Extension.Index.Grid = $.datagrid('contractors', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
