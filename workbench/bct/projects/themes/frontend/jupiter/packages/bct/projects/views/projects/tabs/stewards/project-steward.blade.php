{{ Asset::queue('project-steward-grid', 'bct/projects::projects/js/stewards/project-steward.js', 'extension-base') }}

<section class="panel panel-default panel-grid">

    {{-- Grid: Header --}}
    <header class="panel-heading hidden-print">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-stewards-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{ trans('bct/projects::projects/common.title') }} {{ trans('bct/projects::stewards/common.title') }}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-stewards-actions">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-steward-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="project-steward" role="form">

                        <div class="input-group">

                            <span class="input-group-btn">

                                <button class="btn btn-default" type="button" disabled>
                                    {{{ trans('common.filters') }}}
                                </button>

                                <button class="btn btn-default dropdown-toggle" type="button"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

                                <ul class="dropdown-menu" role="menu" data-filter-reset>

                                    @foreach(\Bct\Projects\Models\ContactType::all() as $type)
                                        <li>
                                            <a href="#" data-filter="type:{{ $type->name }}">
                                                <i class="fa fa-user"></i> {{ $type->name }}
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>

                            </span>

                            <span class="input-group">
                                <select name="column" class="form-control">
                                    <option value="full_name">{{ trans('bct/projects::users/model.general.name') }}
                                    </option>
                                    <option value="phone">{{ trans('bct/projects::users/model.general.phone') }}
                                    </option>
                                    <option value="email">{{ trans('bct/projects::users/model.general.email') }}
                                    </option>
                                    <option value="local">{{ trans('bct/projects::users/model.general.local') }}
                                    </option>
                                </select>
                            </span>

                            <span class="input-group-btn">
                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

                                <button class="btn btn-default" type="submit">
                                    <span class="fa fa-search"></span>
                                </button>

                                <button class="btn btn-default" data-grid="project-steward" data-reset>
                                    <i class="fa fa-refresh fa-sm"></i>
                                </button>

                            </span>

                        </div>

                    </form>

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="project-steward-data-grid-applied-filters">

            <div id="project-steward-data-grid_applied" class="btn-group" data-grid="project-steward"></div>

        </div>

        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="project-steward-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridStewards', ['id' => $project->id]) }}"
                   data-grid="project-steward">
                <thead>
                <tr>
                    <th class="sortable"
                        data-sort="full_name">{{ trans('bct/projects::users/model.general.name') }}</th>
                    <th class="sortable"
                        data-sort="phone">{{ trans('bct/projects::users/model.general.phone') }}</th>
                    <th class="sortable"
                        data-sort="email">{{ trans('bct/projects::users/model.general.email') }}</th>
                    <th class="sortable"
                        data-sort="local">{{ trans('bct/projects::users/model.general.local') }}</th>
                    <th class="hidden-print">{{{ trans('bct/projects::general/common.actions') }}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>

    <footer class="panel-footer clearfix hidden-print">

        {{-- Grid: Pagination --}}
        <div class="project-steward-data-grid_pagination" data-grid="project-steward"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.stewards.grid.project-stewards.results')
    @include('bct/projects::projects.tabs.stewards.grid.project-stewards.pagination')
    @include('bct/projects::projects.tabs.stewards.grid.project-stewards.filters')
    @include('bct/projects::projects.tabs.stewards.grid.project-stewards.no_results')

</section>