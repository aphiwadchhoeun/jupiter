@extends('bct/projects::default')


{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::contacts/common.title') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}

{{ Asset::queue('index', 'bct/projects::contacts/js/index.js', 'extension-base') }}


{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')



    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{{ trans('bct/projects::contacts/common.title') }}}</span>

                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-left download-container">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span
                                            class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="contact" role="form">

                            <div class="input-group">

                                 <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" disabled>
                                        {{ trans('bct/projects::general/common.active') }}
                                    </button>

                                    <button class="btn btn-default dropdown-toggle" type="button"
                                            data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="#" data-filter="active:1" data-reset>
                                                   {{ trans('bct/projects::general/common.yes') }}
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-filter="active:0" data-reset>
                                                    {{ trans('bct/projects::general/common.no') }}
                                                </a>
                                            </li>
                                    </ul>
                                </span>

                                <input class="form-control" name="filter" type="text"
                                       placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="contact" data-reset>
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                            </div>

                        </form>

                        <div class="pull-right">
                        @if (!$currentUser->inRole('pp-guest'))

                            @if (!$currentUser->inRole('repuser'))
                                <a id="merge-btn" data-toggle="tooltip"
                                   data-original-title="{{ trans('bct/projects::general/common.merge') }}"
                                   class="btn btn-primary disabled">
                                    <i class="fa fa-compress"></i>
                                </a>
                            @endif

                            <a href="{{ route('bct.projects.contacts.create') }}"
                                id="contact_new"
                                name="contact_new"
                               class="btn btn-success" data-toggle="tooltip"
                               data-original-title="{{{ trans('action.create') }}}">
                                <i class="fa fa-plus fa-fw"></i>
                            </a>
                        @endif

                        </div>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="data-grid_applied" class="btn-group" data-grid="contact"></div>

            </div>

            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="data-grid" class="table table-hover" data-source="{{ route('bct.projects.contacts.grid') }}"
                       data-filename="{{ trans('bct/projects::contacts/common.title') }}"
                       data-grid="contact">
                    <thead>
                    <tr>
                        @if (!$currentUser->inRole('repuser'))
                            <th class="projects-checkbox-head"></th>
                        @endif
                        <th class="sortable col-md-1" data-sort="id">{{ trans('bct/projects::contacts/model.general.id') }}</th>
                        <th class="sortable col-md-4" data-sort="name">{{ trans('bct/projects::contacts/model.general.name') }}</th>
                        <th class="sortable col-md-3"
                            data-sort="company">{{ trans('bct/projects::contacts/model.general.company') }}</th>
                        <th class="sortable col-md-2"
                            data-sort="phone">{{ trans('bct/projects::contacts/model.general.phone') }}</th>
                        <th class="sortable col-md-2"
                            data-sort="email">{{ trans('bct/projects::contacts/model.general.email') }}</th>
                        <th class="sortable col-md-1"
                                data-sort="active">{{ trans('bct/projects::general/common.active') }}</th>
                        <th class="col-md-1">{{{ trans('bct/projects::general/common.details') }}}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="data-grid_pagination" data-grid="contact"></div>

        </footer>

        {{-- Grid: templates --}}
        @include('bct/projects::contacts.grid.index.results')
        @include('bct/projects::contacts.grid.index.pagination')
        @include('bct/projects::contacts.grid.index.filters')
        @include('bct/projects::contacts.grid.index.no_results')

    </section>

    @include('bct/projects::partials.modals.contact_merge_modal')

@stop
