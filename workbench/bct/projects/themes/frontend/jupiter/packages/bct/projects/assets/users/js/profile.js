/**
 * Created by Aphiwad on 4/17/2015.
 */
jQuery('document').ready(function ($) {
    $('input[name="zipcode"]').mask("99999?-9999");
    $('input[name="phone"]').mask('(999) 999-9999? x999');
    $('input[name="fax"]').mask('(999) 999-9999');
});