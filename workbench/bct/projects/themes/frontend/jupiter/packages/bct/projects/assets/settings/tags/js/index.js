jQuery('document').ready(function ($) {
    $('body').on('click', '[data-modal]', function(){
        var $modal = $('#modal-form');

        $modal.find('.modal-content').load($(this).attr('href'), '', function(response, status, xhr){
            if (response) {
                $modal.modal('show');

                var submit_btn_modal = $('#submit-btn-modal');
                var delete_btn_modal = $('#delete-btn-modal');
                var form_modal = $('#content-form-modal');

                if (form_modal) {
                    form_modal.on('submit', function(e){
                        e.preventDefault();
                    });

                    if (form_modal.parsley()) {
                        form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
                            if (formInstance.isValid() === true) {
                                $.post(
                                    form_modal.attr('action'),
                                    form_modal.serialize(),
                                    function(data){
                                        $modal.modal('hide');
                                        window.location.reload();
                                        //Extension.Index.Grid.refresh();
                                    }
                                );
                            }
                        });

                        submit_btn_modal.on('click', function(){
                            form_modal.parsley().validate();
                        });
                    }

                    if (delete_btn_modal) {
                        delete_btn_modal.on('click', function(){
                            var url = $(this).attr('data-url');
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {},
                                success: function(response)
                                {
                                    $modal.modal('hide');
                                    Extension.Index.Grid.refresh();
                                }
                            });
                        });
                    }
                }
            } else {
                location.reload();
            }
        });
    });
});

var Extension;

;(function(window, document, $, undefined)
{

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'tags'
    });

    // Initialize functions
    Extension.Index.init = function()
    {
        Extension.Index.dataGrid();
    };


    // Ignore row selection on title click
    Extension.Index.titleClick = function(event)
    {
        event.stopPropagation();
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function()
    {

        var config = {
            events: {
                removing: function(dg)
                {
                    _.each(dg.applied_filters, function(filter)
                    {
                        if (filter.column === 'created_at' && filter.from !== undefined && filter.to !== undefined)
                        {
                            $('[data-grid-calendar]').val('');
                        }
                    });
                },
                sorted: function(response) {
                    if (response.pagination.total== 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function(response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container' ).removeClass('hide');
                    }
                },
            },
            callback: function()
            {
                $('[data-action]').prop('disabled', true);

            }
        };

        Extension.Index.Grid = $.datagrid('tags', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);

    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
