<header class="panel-heading">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#actions">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip" href="{{ route('bct.projects.groups.all') }}"
                           data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{{ $group->name }}} {{{ trans("bct/projects::general/common.details") }}}</span>
            </div>

            <div class="pull-right">
                <a href="{{ route('bct.projects.groups.edit', $group->id) }}" data-toggle="tooltip"
                   id="group_edit"
                   name="group_edit"
                   data-original-title="{{{ trans('action.edit') }}}" class="btn btn-primary pull-right"><i
                            class="fa fa-edit fa-fw"></i></a>
            </div>

        </div>

    </nav>

</header>

<div class="panel-body">

    <div class="row">
        <div class="col-md-12">

            <table class="table table-bordered table-details">
                <tbody>
                <tr>
                    <td class="td-value">
                        <div>

                            <div class="col-md-12 col-sm-12 col-panel">
                                <div class="row">
                                    <div class="col-md-8">
                                        <b>{{ trans('bct/projects::groups/model.general.name') }}:</b>
                                        <span class="pre-spaces">{{ $group->name }}</span>
                                    </div>

                                    <div class="col-md-4">
                                        <b>{{ trans('bct/projects::groups/model.general.type') }}:</b>
                                        <span class="pre-spaces">
                                            @if($group->individual==0)
                                                {{ trans('bct/projects::groups/model.general.group') }}
                                            @else
                                                {{ trans('bct/projects::groups/model.general.individual') }}
                                            @endif
                                        </span>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <b>{{ trans('bct/projects::groups/model.general.description') }}:</b>
                                        <span class="pre-spaces">{{ $group->description }}</span>
                                    </div>

                                    <div class="col-md-4">
                                        <b>{{ trans('bct/projects::groups/model.general.status') }}:</b>
                                        <span class="pre-spaces">
                                            @if($group->status == true)
                                                <span class="label label-success">{{{ trans('bct/projects::groups/model.general.active') }}}</span>
                                            @else
                                                <span class="label label-danger">{{{ trans('bct/projects::groups/model.general.inactive') }}}</span>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>

                        <div>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <span class="navbar-brand text-bold">{{{ trans("bct/projects::users/common.title_reps") }}}
                <span id="reps-counter" class="text14">({{ $grid_count }} of {{ $reps_count }})</span>
            </span>

            <a data-id="#reps" class="btn btn-primary pull-right go-to-tab"
               id="reps_tab"
               name="reps_tab"
               data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
                        class="fa fa-edit fa-fw"></i></a>

            <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                    <th class="td-label col-md-6 text-center">{{ trans('bct/projects::users/model.general.name') }}</th>
                    <th class="sortable col-md-3 text-center">{{ trans('bct/projects::users/model.general.email') }}</th>
                    <th class="col-md-2 text-center">{{ trans('bct/projects::users/model.general.role') }}</th>
                    <th class="td-label col-md-1 text-center">{{ trans('bct/projects::users/model.general.status') }}</th>
                </tr>
                @if ($group_reps)
                    @foreach($group_reps as $rep)
                        <tr>
                            <td class="td-value">{{ $rep->full_name }}</td>
                            <td class="td-value"><a class="under-none" href="mailto:{{ $rep->email }}">{{ $rep->email }}</a></td>
                            <td class="td-value">{{ array_to_string_projects($rep->roles->pluck('name')) }}</td>
                            <td class="td-value text-center">
                                @if ($rep->activations->last()->completed)
                                    <span class="label label-success">{{ trans('bct/projects::general/common.active') }}</span>
                                @else
                                    <span class="label label-danger">{{ trans('bct/projects::general/common.disabled') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>

</div>