<span class="navbar-brand">{{ trans("bct/projects::actions/common.title") }} <span id="actions-counter" class="text14">( {{ $actions->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $actions->count() }} )</span></span>
@if (!$currentUser->inRole('pp-guest'))

<a data-id="#actions-history" id="actions_tab" name="actions_tab" class="btn btn-primary go-to-tab pull-right"
   data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
            class="fa fa-edit fa-fw"></i></a>
@endif

{{--<a href="{{ route('bct.projects.projects.createAction', $project->id) }}"
   data-modal
   class="btn btn-success pull-right margin-right5 actions-add-btn" data-toggle="tooltip"
   data-original-title="{{{ trans('action.create') }}}">
    <i class="fa fa-plus fa-fw"></i>
</a>--}}


<table class="table table-striped table-bordered">
    <tbody>
    <tr>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::actions/model.general.contractor_id') }}</td>
        <td class="td-label col-md-5 text-center">{{ trans('bct/projects::actions/model.general.details') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.action_type') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.date') }}</td>

    </tr>
    @foreach($actions->take(5) as $action)
        <tr>
            <td class="td-value">{{ $action->contractor }}</td>
            <td class="td-value text-center">{{ $action->details }}</td>
            <td class="td-value text-center">{{ $action->action_type }}</td>
            <td class="td-value text-center">{{ date_correct_details_projects( $action->action_date ) }}</td>
        </tr>
    @endforeach
</table>