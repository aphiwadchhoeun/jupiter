<script type="text/template" data-grid="project-contacts-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row class="redirect-to" data-url="<%- r.view_uri%>">
        <td><%- r.name %></td>
        <td><%- r.type %></td>
        <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
    </tr>

    <% }); %>

</script>
