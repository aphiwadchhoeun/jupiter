@extends('bct/projects::default')

{{-- Page title --}}
@section('title'){{--
    --}}@parent{{--
    --}} {{ trans('bct/projects::reports/common.title') }}{{--
--}}@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

@stop

{{-- Page --}}
@section('page-extension')
    @parent

    {{-- Grid --}}
    <section class="panel panel-default">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <span class="navbar-brand">{{{ trans('bct/projects::reports/common.title') }}}</span>
                    </div>

                </div>

            </nav>

        </header>


        <div class="panel-body">

            <div class="row">

                <div class="col-md-6">

                    <div class="well h-fixed">
                        <nav class="block-title col-md-12">
                            <div class="navbar-brand">{{ trans('bct/projects::reports/common.blocks.project') }}</div>
                        </nav>
                        <ul class="list-unstyled">

                            <li>
                                <a href="{{{ route('bct.projects.reports.project_classification_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.project_classification_summary') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{{ route('bct.projects.reports.project_region_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.project_region_summary') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{{ route('bct.projects.reports.project_type_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.project_type_summary') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{{ route('bct.projects.reports.actions_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.actions_summary') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{{ route('bct.projects.reports.stage_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.stage_summary') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{{ route('bct.projects.reports.wage_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.wage_summary') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{{ route('bct.projects.reports.rep_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.rep_summary') }}
                                </a>
                            </li>
                        </ul>

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="well h-auto">
                        <nav class="block-title col-md-12">
                            <div class="navbar-brand">{{ trans('bct/projects::reports/common.blocks.contractor') }}</div>
                        </nav>
                        <ul class="list-unstyled">
                            <li>
                                <a href="{{{ route('bct.projects.reports.contractor_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.contractor_summary') }}
                                </a>
                            </li>
                        </ul>

                    </div>
                    <div class="well h-auto">
                        <nav class="block-title col-md-12">
                            <div class="navbar-brand">{{ trans('bct/projects::reports/common.blocks.contact') }}</div>
                        </nav>
                        <ul class="list-unstyled">
                            <li>
                                <a href="{{{ route('bct.projects.reports.contacts_summary') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.contacts_summary') }}
                                </a>
                            </li>
                        </ul>

                    </div>

                    <div class="well h-auto">
                        <nav class="block-title col-md-12">
                            <div class="navbar-brand">{{ trans('bct/projects::reports/common.blocks.custom') }}</div>
                        </nav>
                        <ul class="list-unstyled">

                            <li>
                                <a href="{{{ route('bct.projects.reports.report1.projects') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.report1_projects') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{{ route('bct.projects.reports.active_projects_with_stewards') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.report2_projects') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{{ route('bct.projects.reports.active_service_jobs') }}}" class="btn btn-link">
                                    <i class="fa fa-file fa-fw"></i> {{ trans('bct/projects::reports/common.tabs.report3_projects') }}
                                </a>
                            </li>

                        </ul>

                    </div>
                </div>

            </div>

        </div>

    </section>

@stop
