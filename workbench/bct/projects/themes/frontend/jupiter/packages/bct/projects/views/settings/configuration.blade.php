@extends('bct/projects::default')

{{-- Page title --}}
@section('title'){{--
    --}}@parent{{--
    --}} {{ trans('bct/projects::settings/common.settings') }} \ {{ trans('bct/projects::settings/common.configuration') }}{{--
--}}@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('numeric', 'bower_components/jquery-numeric/jquery.numeric.js', 'jquery') }}
    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}
    {{ Asset::queue('jquery-serialize-object', 'bower_components/jquery-serialize-object/dist/jquery.serialize-object.min.js', 'jquery') }}
    {{ Asset::queue('selectize', 'bower_components/selectize/dist/js/standalone/selectize.min.js', 'jquery') }}

    {{ Asset::queue('index', 'bct/projects::settings/configuration/js/configuration.js', 'extension-base') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Asset::queue('selectize-css', 'bower_components/selectize/dist/css/selectize.css') }}
    {{ Asset::queue('selectize-bootstrap3', 'bower_components/selectize/dist/css/selectize.bootstrap3.css', 'bootstrap') }}
@stop


{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid top-buffer">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#actions">
                            <span class="sr-only">{{ trans('bct/projects::general/common.buttons.toggle_navigation') }}</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{ trans('bct/projects::settings/common.settings') }}
                            \ {{ trans('bct/projects::settings/common.configuration') }}</span>

                    </div>

                    <div class="nav navbar-nav pull-right">
                        <button
                                class="btn btn-primary btn-settings-update">
                            <i class="fa fa-edit fa-fw"></i> {{ trans('bct/projects::general/common.update') }}
                        </button>
                    </div>

                </div>

            </nav>

            <div class="panel-body-configuration">

                <table class="table table-striped table-bordered table-details-member">
                    <tbody>

                    <input type="hidden" name="map_week_start"
                           value="{{ config_projects('bct.projects.settings.map_week_start') }}">
                    <input type="hidden" name="map_week_end"
                           value="{{ config_projects('bct.projects.settings.map_week_end') }}">

                    <input type="hidden" name="map_union_start"
                           value="{{ config_projects('bct.projects.settings.map_union_start') }}">
                    <input type="hidden" name="map_union_end"
                           value="{{ config_projects('bct.projects.settings.map_union_end') }}">

                    <tr>
                        <td class="td-label text-center col-md-6">{{ trans('bct/projects::settings/form.visit_auto_approve') }}</td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <input type="checkbox" class="form-control" name="visit_auto_approve"
                                       @if (config_projects('bct.projects.settings.visit_auto_approve'))
                                       checked
                                        @endif
                                />
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">{{ trans('bct/projects::settings/form.project_field_report') }}</td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <input type="checkbox" class="form-control" name="project_field_report"
                                       @if (config_projects('bct.projects.settings.project_field_report'))
                                       checked
                                        @endif
                                />
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">{{ trans('bct/projects::settings/form.enable_project_visit_workers') }}</td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <input type="checkbox" class="form-control" name="enable_project_visit_workers"
                                       @if (config_projects('bct.projects.settings.enable_project_visit_workers') === 1)
                                       checked
                                        @endif
                                />
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">{{ trans('bct/projects::settings/form.enable_project_visit_weather') }}</td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <input type="checkbox" class="form-control" name="enable_project_visit_weather"
                                       @if (config_projects('bct.projects.settings.enable_project_visit_weather') === 1)
                                       checked
                                        @endif
                                />
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">{{ trans('bct/projects::settings/form.enable_project_visit_date') }}</td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <input type="checkbox" class="form-control" name="enable_project_visit_date"
                                       @if (config_projects('bct.projects.settings.enable_project_visit_date') === 1)
                                       checked
                                        @endif
                                />
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-vcenter">{{ trans('bct/projects::settings/form.interval_week', ['num' => 1]) }}</div>
                                <div class="col-md-2 col-vcenter">0</div>
                                <div class="col-md-1 col-vcenter">-</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_week_start map_week_start_first text-center"
                                           value="{{ config_projects('bct.projects.settings.map_week_start') }}"/>
                                </div>
                            </div>
                        </td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <select name="map_less_4" class="form-control">
                                    <option value="">Select color</option>
                                    <option value="green"
                                            @if (config('bct.projects.settings.map_less_4') === 'green')
                                            selected
                                            @endif
                                    >
                                        Green
                                    </option>
                                    <option value="yellow"
                                            @if (config('bct.projects.settings.map_less_4') === 'yellow')
                                            selected
                                            @endif
                                    >
                                        Yellow
                                    </option>
                                    <option value="red"
                                            @if (config('bct.projects.settings.map_less_4') === 'red')
                                            selected
                                            @endif
                                    >
                                        Red
                                    </option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-vcenter">{{ trans('bct/projects::settings/form.interval_week', ['num' => 2]) }}</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_week_start map_week_start_second text-center"
                                           value="{{ config_projects('bct.projects.settings.map_week_start') }}"/>
                                </div>
                                <div class="col-md-1 col-vcenter">-</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_week_end map_week_end_first text-center"
                                           value="{{ config_projects('bct.projects.settings.map_week_end') }}"/>
                                </div>
                            </div>
                        </td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <select name="map_4_8" class="form-control">
                                    <option value="">Select color</option>
                                    <option value="green"
                                            @if (config('bct.projects.settings.map_4_8') === 'green')
                                            selected
                                            @endif
                                    >
                                        Green
                                    </option>
                                    <option value="yellow"
                                            @if (config('bct.projects.settings.map_4_8') === 'yellow')
                                            selected
                                            @endif
                                    >
                                        Yellow
                                    </option>
                                    <option value="red"
                                            @if (config('bct.projects.settings.map_4_8') === 'red')
                                            selected
                                            @endif
                                    >
                                        Red
                                    </option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-vcenter">{{ trans('bct/projects::settings/form.interval_week', ['num' => 3]) }}</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_week_end map_week_end_second text-center"
                                           value="{{ config_projects('bct.projects.settings.map_week_end') }}"/>
                                </div>
                                <div class="col-md-1 col-vcenter">-</div>
                                <div class="col-md-2 col-vcenter size35">&infin;</div>
                            </div>
                        </td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <select name="map_more_8" class="form-control">
                                    <option value="">Select color</option>
                                    <option value="green"
                                            @if (config('bct.projects.settings.map_more_8') === 'green')
                                            selected
                                            @endif
                                    >
                                        Green
                                    </option>
                                    <option value="yellow"
                                            @if (config('bct.projects.settings.map_more_8') === 'yellow')
                                            selected
                                            @endif
                                    >
                                        Yellow
                                    </option>
                                    <option value="red"
                                            @if (config('bct.projects.settings.map_more_8') === 'red')
                                            selected
                                            @endif
                                    >
                                        Red
                                    </option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-vcenter">{{ trans('bct/projects::settings/form.interval_union', ['num' => 1]) }}</div>
                                <div class="col-md-2 col-vcenter">0</div>
                                <div class="col-md-1 col-vcenter">-</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_union_start map_union_start_first text-center"
                                           value="{{ config_projects('bct.projects.settings.map_union_start') }}"/>
                                </div>
                            </div>
                        </td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <select name="map_union_1" class="form-control">
                                    <option value="">Select smile</option>
                                    <option value="sad"
                                            @if (config('bct.projects.settings.map_union_1') === 'sad')
                                            selected
                                            @endif
                                    >
                                        Sad
                                    </option>
                                    <option value="neutral"
                                            @if (config('bct.projects.settings.map_union_1') === 'neutral')
                                            selected
                                            @endif
                                    >
                                        Neutral
                                    </option>
                                    <option value="smile"
                                            @if (config('bct.projects.settings.map_union_1') === 'smile')
                                            selected
                                            @endif
                                    >
                                        Smile
                                    </option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-vcenter">{{ trans('bct/projects::settings/form.interval_union', ['num' => 2]) }}</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_union_start map_union_start_second text-center"
                                           value="{{ config_projects('bct.projects.settings.map_union_start') }}"/>
                                </div>
                                <div class="col-md-1 col-vcenter">-</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_union_end map_union_end_first text-center"
                                           value="{{ config_projects('bct.projects.settings.map_union_end') }}"/>
                                </div>
                            </div>
                        </td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <select name="map_union_2" class="form-control">
                                    <option value="">Select smile</option>
                                    <option value="sad"
                                            @if (config('bct.projects.settings.map_union_2') === 'sad')
                                            selected
                                            @endif
                                    >
                                        Sad
                                    </option>
                                    <option value="neutral"
                                            @if (config('bct.projects.settings.map_union_2') === 'neutral')
                                            selected
                                            @endif
                                    >
                                        Neutral
                                    </option>
                                    <option value="smile"
                                            @if (config('bct.projects.settings.map_union_2') === 'smile')
                                            selected
                                            @endif
                                    >
                                        Smile
                                    </option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="td-label text-center col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-vcenter">{{ trans('bct/projects::settings/form.interval_union', ['num' => 3]) }}</div>
                                <div class="col-md-2">
                                    <input type="text"
                                           class="form-control not-send map_union_end map_union_end_second text-center"
                                           value="{{ config_projects('bct.projects.settings.map_union_end') }}"/>
                                </div>
                                <div class="col-md-1 col-vcenter">-</div>
                                <div class="col-md-2 col-vcenter">100</div>
                            </div>
                        </td>
                        <td class="td-value text-center col-md-6">
                            <div class="form-group">
                                <select name="map_union_3" class="form-control">
                                    <option value="">Select smile</option>
                                    <option value="sad"
                                            @if (config('bct.projects.settings.map_union_3') === 'sad')
                                            selected
                                            @endif
                                    >
                                        Sad
                                    </option>
                                    <option value="neutral"
                                            @if (config('bct.projects.settings.map_union_3') === 'neutral')
                                            selected
                                            @endif
                                    >
                                        Neutral
                                    </option>
                                    <option value="smile"
                                            @if (config('bct.projects.settings.map_union_3') === 'smile')
                                            selected
                                            @endif
                                    >
                                        Smile
                                    </option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>


        </header>

    </section>

@stop