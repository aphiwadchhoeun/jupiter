@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent
    <script type="text/javascript" src="{{ URL::asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyBvReYkUqZuyu4HLSHepBRsGax30FnaWnI&libraries=places&language=en&callback=initialize') }}"></script>
    <script type="text/javascript" src="{{ asset('themes/frontend/jupiter/assets/cartodb/js/cartodb.js') }}"></script>

    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
    {{ Asset::queue('index', 'bct/projects::routes/js/index.js', ['extension-base', 'swal']) }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('themes/frontend/jupiter/assets/cartodb/css/cartodb.css') }}">

    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
    {{ Asset::queue('map-css', 'bct/projects::routes/css/index.css') }}
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Grid --}}
    <section class="panel panel-default panel-grid">

        <div class="panel-body">

            <div class="row">

                <div class="col-md-4">

                    <fieldset class="hidden-print">

                        <div class="form-group">

                            <label class="control-label">
                                {{{ trans('bct/projects::routes/common.start') }}}
                            </label>

                            <input type="text" class="form-control" id="start-point" value="">
                        </div>

                        <section class="panel panel-default panel-grid">
                            <div class="panel-body">
                                <div class="row">
                                    {{-- Grid: Table --}}
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>{{{ trans('bct/projects::projects/model.general.name') }}}</th>
                                                <th>{{{ trans('bct/projects::general/common.actions') }}}</th>
                                            </tr>
                                            </thead>
                                            <tbody id="sortable">
                                            @foreach($user->tempItems as $index => $item)
                                                @if($item->project)
                                                    <tr data-id="{{ $item->id }}" data-lat="{{ $item->project->lat }}"
                                                        data-lng="{{ $item->project->lng }}" data-sort="{{ $item->sortOrder }}"
                                                        data-project-id="{{ $item->project->id }}" data-name="{{ $item->project->name }}"
                                                        data-desc="{{ str_limit( $item->project->description, 120) }}"
                                                        data-address="{{ $item->project->fullAddress }}">
                                                        <td><img src="{{ Asset::getUrl('bct/projects::routes/img/icons/mark' . ($index + 1) . '.png') }}"></td>
                                                        <td>{{ $item->project->name }}</td>
                                                        <td>

                                                            <a href="{{ route('bct.projects.projects.details', $item->project->id) }}"
                                                               id="project_details"
                                                               name="project_details"
                                                               target="_blank"
                                                               class="btn btn-primary btn-circle"
                                                               data-toggle="tooltip" data-placement="left"
                                                               data-original-title="{{{ trans('bct/projects::actions/model.general.details') }}}">
                                                                <i class="fa fa-book fa-fw"></i></a>

                                                            <a href="{{ route('bct.projects.routes.delete', $item->id) }}"
                                                               id="route_delete"
                                                               name="route_delete"
                                                               class="btn btn-danger btn-circle delete-confirm"
                                                               data-toggle="tooltip" data-placement="right"
                                                               data-original-title="{{{ trans('action.delete') }}}">
                                                                <i class="fa fa-trash fa-fw"></i></a>

                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr data-id="{{ $item->id }}" data-lat="{{ $item->lat }}"
                                                        data-lng="{{ $item->lng }}"  data-sort="{{ $item->sortOrder }}">
                                                        <td><img src="{{ Asset::getUrl('bct/projects::routes/img/icons/mark' . ($index + 1) . '.png') }}"></td>
                                                        <td>{{ $item->address }}</td>
                                                        <td>
                                                            <a href="{{ route('bct.projects.routes.delete', $item->id) }}"
                                                               id="route_delete"
                                                               name="route_delete"
                                                               class="btn btn-danger btn-circle delete-confirm"
                                                               data-toggle="tooltip" data-placement="right"
                                                               data-original-title="{{{ trans('action.delete') }}}">
                                                                <i class="fa fa-trash fa-fw"></i></a>
                                                        </td>
                                                    </tr>
                                                @endif

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <div class="form-group">

                            <label class="control-label">
                                {{{ trans('bct/projects::routes/common.finish') }}}
                            </label>

                            <input type="text" class="form-control" id="finish-point" value="">
                        </div>

                        <div class="form-group">

                            <button class="btn btn-primary" id="optimize">{{ trans('bct/projects::routes/common.optimizing') }}</button>

                        </div>

                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default">
                                <input type="radio" name="options" value="show-zip"> {{{ trans('bct/projects::routes/common.showZip') }}}
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="options" value="show-county"> {{{ trans('bct/projects::routes/common.showCounty') }}}
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="options" value="show-region"> {{{ trans('bct/projects::routes/common.showRegion') }}}
                            </label>
                            <label class="btn btn-default active">
                                <input type="radio" name="options" value="show-default" selected="true">  {{{ trans('bct/projects::routes/common.default') }}}
                            </label>
                        </div> 
                            
                        
                        <div class="form-group">

                            <label class="control-label">
                                {{{ trans('bct/projects::routes/common.details') }}}:
                            </label>

                            <span id="route-details"></span>
                        </div>

                    </fieldset>

                </div>

                <div class="col-md-8">

                    {{-- Form: General --}}
                    <fieldset>
                        <div class="form-group">
                            <div id="map-canvas"></div>
                        </div>
                    </fieldset>

                </div>

            </div>

        </div>

        <div class="panel-body">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>

        </div>

    </section>

@stop
