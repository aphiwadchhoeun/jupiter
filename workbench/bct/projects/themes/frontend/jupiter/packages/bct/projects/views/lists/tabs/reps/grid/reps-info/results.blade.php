<script type="text/template" data-grid="reps-info" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
       <td><%- r.name %></td>
        <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
        <td><a class="under-none" href="mailto:<%- r.email %>"><%- r.email %></a></td>
        <td>
            <% if(r.status == true) { %>
            <span class="label label-success">Active</span>
            <% } else { %>
            <span class="label label-danger">Disabled</span>
            <% } %>
        </td>
        <td><%- r.role %></td>
        <td><%- r.created_at_custom %></td>
    </tr>

    <% }); %>

</script>
