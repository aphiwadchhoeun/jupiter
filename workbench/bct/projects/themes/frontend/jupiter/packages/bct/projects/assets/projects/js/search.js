var latInput = $('#lat');
var lngInput = $('#lng');
var radius = 0.5;
var searchmapInput = $('#searchmap');
var createBtn = $('#create-new-project');
var searchBtn = $('#search-btn');
var map, marker, icons = {}, windows = {}, searchInput;
var projectIcon = {
    path: 'M -5,-5 0,-10 5,-5 5,0 1,0 1,-3 -1,-3 -1,0 -5,0 z',
    fillColor: '#4CAF50',
    fillOpacity: 0.8,
    scale: 2,
    strokeColor: '#000000',
    strokeWeight: 2
};
var default_lat = (typeof BctProjects.default_lat !== 'undefined') ? BctProjects.default_lat : '',
    default_lng = (typeof BctProjects.default_lng !== 'undefined') ? BctProjects.default_lng : '';

jQuery('document').ready(function ($) {
    var geocoder, searchBox, markerOptions;

    createBtn.click(function (e) {
        e.preventDefault();
        var self = $(this);
        var url = self.attr('href');

        var params = "?lat=" + latInput.val()
            + "&lng=" + lngInput.val();
        window.location.href = (url + params);
    });

    searchBtn.click(function (e) {
        e.preventDefault();
        var source = $(this);

        source.prop('disabled', true);
        source.find('i.fa').removeClass('fa-search')
            .addClass('fa-spinner fa-spin');

        changePosition(marker);

        searchProjectOnMap(function () {
            createBtn.removeAttr('disabled');

            source.removeAttr('disabled');
            source.find('i.fa').removeClass('fa-spinner fa-spin')
                .addClass('fa-search');
        });
    });

    function searchProjectOnMap(callback) {
        Extension.Index.Grid.refresh();

        $.post('/pprofiles/projects/map/search', {
            lat: latInput.val(),
            lng: lngInput.val(),
            radius: radius,
            status: 'Active'
        }).success(function (data) {
            putIconsOnMap(data);
        })
            .always(function () {
                if (typeof callback !== 'undefined') {
                    callback();
                }
            });
    }

    function clearIconsOnMap() {
        $.each(icons, function (i, item) {
            item.setMap(null);
        });
        icons = {};
    }

    function putIconsOnMap(data) {
        clearIconsOnMap();
        $.each(data, function (i, item) {
            var icon = new google.maps.Marker({
                title: item.name,
                map: map,
                position: new google.maps.LatLng(item.lat, item.lng),
                icon: projectIcon
            });

            var fullAddress = [item.address, item.city, item.state, item.zipcode].join(' ');

            var contentString =
                '<div id="content">' +
                '<div id="bodyContent">' +
                '<p><b>Name: </b>' + item.name + '</p>' +
                '<p><b>Address: </b>' + fullAddress + '</p>';
            if (item.description != null && item.description != '') {
                contentString += '<p><b>Description: </b>' + item.description + '</p>';
            }
            contentString += '<p>' +
                '<a class="label label-primary" href="' + item.details_url + '#details">' + 'Select</a> ' +
                '</p>' +
                '</div>' +
                '</div>';

            var infoWindow = new google.maps.InfoWindow({
                content: contentString
            });

            icon.addListener('click', function () {
                infoWindow.open(map, icon);
            });

            icons[item.id] = icon;
        });
    }

    /** for google map */
    function initialize() {
        geocoder = new google.maps.Geocoder();
        var defaultLatLng = new google.maps.LatLng(default_lat, default_lng);

        // Map
        // --------------------------------
        var mapOptions = {
            center: defaultLatLng,
            zoom: 10,
            streetViewControl: false,
            styles: [
                {
                    featureType: "poi",
                    stylers: [
                        {visibility: "off"}
                    ]
                }
            ]
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        // add a click event handler to the map object
        google.maps.event.addDomListener(map, "click", function (event) {
            // place a marker
            marker.setPosition(event.latLng);
        });
        // --------------------------------

        // Marker
        // --------------------------------
        markerOptions = {
            //position: defaultLatLng,
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
        };

        marker = new google.maps.Marker(markerOptions);

        google.maps.event.addDomListener(marker, 'position_changed', function () {
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            latInput.val(lat);
            lngInput.val(lng);
            document.cookie = "lat=" + lat;
            document.cookie = "lng=" + lng;

            searchBtn.removeAttr('disabled');
        });

        google.maps.event.addDomListener(marker, 'dragend', function () {
            getAddress(marker.getPosition());
            setCircle(marker.getPosition());
        });
        // -----------------------------------------------

        // Search box
        // -----------------------------------------------
        searchInput = document.getElementById('searchmap');
        searchBox = new google.maps.places.SearchBox(searchInput);

        searchBox.addListener("places_changed", function () {
            fireSearchBox();
        });
        // -----------------------------------------------

        // Try HTML5 geolocation
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                map.panTo(new google.maps.LatLng(position.coords.latitude,
                    position.coords.longitude));
            });
        }

    }

    function fireSearchBox() {
        var places = searchBox.getPlaces();

        if (places.length == 0) return;

        marker.setPosition(places[0].geometry.location);
        changePosition(marker);
        searchProjectOnMap(function() {
            createBtn.removeAttr('disabled');
        });
    }

    function changePosition(marker) {
        map.setCenter(markerOptions.pos);
        getAddress(marker.getPosition());
        setCircle(marker.getPosition());
        map.fitBounds(circle.getBounds());
    }

    function getAddress(position) {
        geocoder.geocode({
            latLng: position
        }, function (responses) {
            if (responses && responses.length > 0) {
                /** set address data to inputs */
                searchmapInput.val(responses[0].formatted_address);
            } else {
                /** clear inputs */
                searchmapInput.val('');
            }
        });
    }

    function setCircle(position) {
        if (typeof circle != 'undefined') {
            circle.setCenter(position);
            circle.setRadius(radius * 1609.344);
        } else {
            /** set circle */
            var circleOptions = {
                strokeColor: '#0f27a1',
                strokeOpacity: 0.6,
                strokeWeight: 1,
                fillColor: '#0f27a1',
                fillOpacity: 0.1,
                map: map,
                center: position,
                radius: radius * 1609.344
            };
            // Add the circle for the radius to the map.
            circle = new google.maps.Circle(circleOptions);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
});

/**
 * Datagrid
 */

var Extension;

;
(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'projects'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', Extension.Index.titleClick);
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };


    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            cache_response: true,
            events: {
                fetching: function (dg) {
                    dg.source = dg.source.split('?')[0];
                    if (latInput.val() && lngInput.val() && radius) {
                        dg.source += '?' + $.param({
                                lat: latInput.val(),
                                lng: lngInput.val(),
                                radius: radius,
                                status: 'Active'
                            });
                    } else {
                        dg.source += '?hide=1';
                    }
                },
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                }
            },
        };

        Extension.Index.Grid = $.datagrid('projects', '#data-grid', '#data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);