<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    {{ Asset::queue('normalize', 'bct/projects::custom/css/normalize.css') }}
    {{ Asset::queue('custom-pdf-css', 'bct/projects::custom/css/bootstrap-grid-export.css') }}

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach

    <style>
        .col-md-6 {
            width: 49%;
        }
        table
        {
            border-collapse:unset!important;
        }
        .pre-spaces {
            padding-left: 1em;
        }
    </style>

</head>
<body>

{{--Headers with logo--}}
@include('bct/projects::pdf/partials/header')

<div class="hr10"></div>

<div class="ext-profiles">

    <div class="panel-body">
        <div class="row">

            <div class="col-md-12">

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td class="td-value">
                            <div>

                                <div class="col-md-6 col-sm-12 border-right col-panel">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::contacts/model.general.name') }}:</b>
                                            <span class="pre-spaces">{{ $contact->full_name }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::general/common.updated') }}:</b>
                                            <span class="pre-spaces">{{ date_correct_details_projects($contact->updated_at) }}</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::contacts/model.general.address') }}:</b>
                                            <span class="pre-spaces">{{ $contact->location }}</span>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::contacts/model.general.phone') }}:</b>
                                            <span class="pre-spaces">{{ $contact->phone }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::contacts/model.general.phone2') }}:</b>
                                            <span class="pre-spaces">{{ $contact->phone2 }}</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-12 col-panel">

                                    <div class="row">

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::contacts/model.general.company') }}:</b>
                                            <span class="pre-spaces">{{ $contact->company }}</span>
                                        </div>

                                        <div class="col-md-6">
                                            <b>{{ trans('bct/projects::contacts/model.general.title') }}:</b>
                                            <span class="pre-spaces">{{ $contact->title }}</span>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::contacts/model.general.email') }}:</b>
                                            <span class="pre-spaces">{{ $contact->email }}</span>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <b>{{ trans('bct/projects::contacts/model.general.notes') }}:</b>
                                            <span class="pre-spaces">{{ $contact->notes_short }}</span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

        </div>

        @if (!$contact->projects->isEmpty())

            <div><h1>{{ trans('bct/projects::projects/common.title') }}</h1></div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ trans('bct/projects::projects/model.general.name') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.city') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.state') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.created_at') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.status_id') }}</th>
                            <th>{{ trans('bct/projects::projects/model.general.contact_type') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($contact->projects as $project)
                                <tr>
                                    <td>{{ $project->name }}</td>
                                    <td>{{ $project->city }}</td>
                                    <td>{{ $project->state }}</td>
                                    <td>{{ date_correct_details_projects($project->created_at) }}</td>
                                    <td>{{ ($project->status) ? $project->status->name : '' }}</td>
                                    <td>{{ ($project->pivot->contact_type) ? $project->pivot->contact_type->name : '' }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @endif

</body>
</html>
