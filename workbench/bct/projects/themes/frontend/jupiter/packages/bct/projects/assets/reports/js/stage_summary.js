var Extension;
var checked_rows = [];

;(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'stage_summary'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.datePicker(Extension);
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };


    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid-calendar-preset]', Extension.Index.calendarPresets)

        ;
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    // Handle Data Grid calendar
    Extension.Index.calendarPresets = function (event) {
        event.preventDefault();

        var start, end;

        switch ($(this).data('grid-calendar-preset')) {
            case 'day':
                start = end = moment().subtract(1, 'day').startOf('day').format('MM/DD/YYYY');
                break;

            case 'week':
                start = moment().startOf('week').format('MM/DD/YYYY');
                end = moment().endOf('week').format('MM/DD/YYYY');
                break;

            case 'month':
                start = moment().startOf('month').format('MM/DD/YYYY');
                end = moment().endOf('month').format('MM/DD/YYYY');
                break;

            case 'last30days':
                start = moment().subtract('29','days').format('MM/DD/YYYY');
                end = moment().format('MM/DD/YYYY');
                break;

            case 'last90days':
                start = moment().subtract('89','days').format('MM/DD/YYYY');
                end = moment().format('MM/DD/YYYY');
                break;

            default:
        }

        var $daterange_start_date = $('[data-grid="' + Extension.Index.grid + '"] [data-range-start]'),
            $daterange_end_date = $('[data-grid="' + Extension.Index.grid + '"] [data-range-end]');


        $daterange_start_date.val(start);
        $daterange_end_date.val(end);
        $daterange_start_date.trigger('change');
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            //hash: false,
            callback_local: function() {
                $('[data-grid-calendar-preset="last30days"]').click();
            },
            events: {
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
            callback: function () {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

            }
        };

        Extension.Index.Grid = $.datagrid(Extension.Index.grid, '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
