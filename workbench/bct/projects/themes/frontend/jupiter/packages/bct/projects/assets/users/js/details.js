var $modal = $('#modal-form');

jQuery('document').ready(function ($) {
    /** TABS */
    var hash = window.location.hash;

    $('.nav-tabs a').click(function (e) {
        $(this).tab('show');
        window.location.hash = this.hash;
        var scrollmem = $('body').scrollTop();
        $('html,body').scrollTop(scrollmem);

    });

    if (!hash) {
        $('.nav-tabs a[href="#details"]').click();
    } else {
        $('.nav-tabs a[href="' + hash + '"]').click();
    }

    var config = {
        preventHashChange: true,
        hash: false
    };


    var gridGroupsSummary, gridVisitsSummary, gridGroupsAdd, gridGroupsDel;


    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        var $id = $(e.target).attr('href');
        if ($id == '#details') {
            if (!gridGroupsSummary) {
                gridGroupsSummary = $.datagrid('groups-summary', '#data-grid_groups', '.data-grid_pagination', '#data-grid_applied', config);

            } else {
                gridGroupsSummary.refresh();
                var url = window.location.origin + window.location.pathname + "/refresh/groups";
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {},
                    success: function (response) {
                        $('#groups-counter').text(response);
                    }
                });
            }

            if (!gridVisitsSummary) {
                gridVisitsSummary = $.datagrid('visits-summary', '#data-grid_visits', '.data-grid_pagination', '#data-grid_applied', config);

            } else {
                gridVisitsSummary.refresh();
                var url = window.location.origin + window.location.pathname + "/refresh/visits";
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {},
                    success: function (response) {
                        $('#visits-counter').text(response);
                    }
                });
            }
        } else if ($id == '#groups') {
            if (!gridGroupsDel) {
                gridGroupsDel = $.datagrid('groups-added', '#data-grid', '.data-grid_pagination', '#data-grid_applied', $.extend({}, config, {
                    events: {
                        fetched: function () {
                            if (!gridGroupsAdd) {
                                gridGroupsAdd = $.datagrid('groups', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
                            }
                        }
                    }
                }));
            }
        } else {
            $('input[name="phone"]').mask('(999) 999-9999? x999');
        }
    });

    $('.go-to-details').on('click', function (e) {
        e.preventDefault();
        $('.nav-tabs a[href="#details"]').click();
    });

    $('.go-to-tab').on('click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $('.nav-tabs a[href="' + id + '"]').click();
    });

    $('#show-details-edit').on('click', function () {
        $('.nav-tabs a#dropdown2-tab').click();
    });

    $('body').on('click', '#submit-btn', function (e) {
        e.preventDefault();
        var form_modal = $('#content-form');

        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post(
                        form_modal.attr('action'),
                        form_modal.serialize(),
                        function (data) {
                            resolve();
                        }
                    );
                });
            }
        }).then(function () {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }

            window.location.href = window.location.origin + window.location.pathname;
        });
    });


    /*
     Typeahead for "local" field
     */
    var local_bloodhound = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: window.location.origin + '/pprofiles/locals/get/withID/json'
    });
    var local_typeahead = $('#local'),
        local_submit = $('#local_submit');

    local_bloodhound.clear();
    local_bloodhound.clearPrefetchCache();
    local_bloodhound.initialize(true);

    local_typeahead.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'local',
            display: 'name',
            source: local_bloodhound
        }).bind('typeahead:selected', function (event, data) {
        local_submit.val(data.id);
    }).bind('typeahead:autocompleted', function (event, data) {
        local_submit.val(data.id);
    }).on('input', function () {
        if ($(this).val() === '') {
            local_submit.val('');
        }
    });
    /*
     end; Typeahead for "local" field
     */

});


var Extension;

;(function (window, document, $, undefined) {
    'use strict';

    var gridVisits;
    var config_visits = {
        preventHashChange: true,
        hash: false,
        events: {
            sorted: function (response) {
                if (response.pagination.total == 0) {
                    $('[data-grid="rep-visits"].download-container').addClass('hide');
                }
            },
            fetched: function (response) {
                if (response.pagination.filtered == 0) {
                    $('[data-grid="rep-visits"].download-container').addClass('hide');
                } else {
                    $('[data-grid="rep-visits"].download-container').removeClass('hide');
                }
            },
        },
    };


    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'rep-visits'
    });

    // Initialize functions
    Extension.Index.init = function () {
        if (!gridVisits) {
            gridVisits = $.datagrid('rep-visits', '#rep-visits-data-grid', '.rep-visits-data-grid_pagination', '#rep-visits-data-grid_applied', config_visits);
        }

        Extension.Index.datePicker(Extension);
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function () {
    };

    Extension.Index.setGetParameter = function (grid, paramName, paramValue) {
        var url = grid.source;
        if (url.indexOf(paramName + "=") >= 0) {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
        }
        else {
            if (url.indexOf("?") < 0)
                url += "?" + paramName + "=" + paramValue;
            else
                url += "&" + paramName + "=" + paramValue;
        }

        grid.source = url;
    };

    Extension.Index.removeParam = function (grid, key) {
        var sourceURL = grid.source,
            rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        grid.source = rtn;
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
