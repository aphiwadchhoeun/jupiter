/**
 * Created by sky on 06.06.16.
 */
/**
 * Created by roman on 28.09.15.
 */
jQuery('document').ready(function ($) {

    var regions = [], scopes = [], include_non_union_gc = true;
    var cartodb_api = null, cartodb_table = null;


    function fetchCartodbConfigs(callback) {
        if (cartodb_api == null || cartodb_table == null) {
            $.get('/ui/api/cartodb/configs/get')
                .done(function (data) {
                    cartodb_api = data.api_url;
                    cartodb_table = data.tablename;
                    callback();
                });
        } else {
            callback();
        }
    }


    fetchCartodbConfigs(function () {
        var regionService = cartodb_api + '/sql?q=select areaabrv, areaname from ' + cartodb_table;

        // make a call to cartodb service and retrieve regions data, then populate them into select boxes
        $.get(regionService, function (data) {
            $('#regions-report1').prop('disabled', true);
            if (data.rows.length > 0) {

                $.each(data.rows, function (i, row) {

                    $('#regions-report1').append($('<option>', {
                        value: row.areaabrv,
                        text: row.areaabrv
                    }));

                });
                $('#regions-report1').prop('disabled', false);

            }
        }).done(function () {
            var $regions = $('#regions-report1').selectize({
                onChange: function (value) {
                    regions = value;

                    if (value !== null && value.length > 0) {
                        ajaxResults(regions, scopes, include_non_union_gc);
                        $(".btn-download-pdf").removeClass('hide');
                        $('.regions-report1-deselect-all').removeClass('hide');
                    } else {
                        $(".btn-download-pdf").addClass('hide');
                        $('.report-results').html('');
                    }
                }
            });

            var selectize_report3 = $regions[0].selectize;

            $('.regions-report1-select-all').on('click', function (e) {
                e.preventDefault();

                $(this).addClass('hide');
                $('.regions-report1-deselect-all').removeClass('hide');

                selectize_report3.setValue(_.keys(selectize_report3.options));
            });

            $('.regions-report1-deselect-all').on('click', function (e) {
                e.preventDefault();

                $(this).addClass('hide');
                $('.regions-report1-select-all').removeClass('hide');
                selectize_report3.clear();
            });
        });
    });

    var $scopes = $('#scopes-report1').selectize({
        onChange: function (value) {
            scopes = value;

            if (value !== null && value.length > 0) {
                ajaxResults(regions, scopes, include_non_union_gc);
                $('.scopes-report1-deselect-all').removeClass('hide');
            }
        }
    });

    var selectize_scopes = $scopes[0].selectize;

    $('.scopes-report1-select-all').on('click', function (e) {
        e.preventDefault();

        $(this).addClass('hide');
        $('.scopes-report1-deselect-all').removeClass('hide');

        selectize_scopes.setValue(_.keys(selectize_scopes.options));
    });

    $('.scopes-report1-deselect-all').on('click', function (e) {
        e.preventDefault();

        $(this).addClass('hide');
        $('.scopes-report1-select-all').removeClass('hide');
        selectize_scopes.clear();
    });

    $('#include_non_union_gc').bootstrapToggle({
        onstyle: 'success',
        offstyle: 'danger',
        on: 'Yes',
        off: 'No'
    });

    $('#include_non_union_gc').on('onChange', function (event, state) {
        if (state) {
            include_non_union_gc = true;
        } else {
            include_non_union_gc = false;
        }

        ajaxResults(regions, scopes, include_non_union_gc);
    });

    $('.btn-download-pdf').click(function (e) {
        e.preventDefault();

        askFileName(regions, scopes, include_non_union_gc);
    });

});

function askFileName(regions, scopes, include) {
    var defaultFilename = 'Projects Report';
    var title = 'Enter filename';
    var text = '';

    swal({
        title: title,
        text: text,
        input: 'text',
        showCancelButton: true,
        inputValue: defaultFilename,
        showLoaderOnConfirm: true,
        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    if (value.length > 80) {
                        reject("Filename length should not be longer than 80 characters!");
                    }

                    resolve();
                } else {
                    reject("You need to write filename!");
                }
            });
        },
        preConfirm: function (input) {
            return new Promise(function (resolve, reject) {
                var url = '/pprofiles/reports/report1/projects/export';

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        regions: regions,
                        scopes: scopes,
                        inc: include,
                        filename: input,
                        download: 'pdf'
                    }
                })
                    .done(function () {
                        resolve();
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        var error = (jqXHR.status === 422) ? JSON.parse(jqXHR.responseText) : '';
                        var title = errorThrown;
                        var text = (typeof error.message !== 'undefined') ? error.message : '';
                        swal(title, text, "error")
                    });
            });
        }
    }).then(function () {
        swal('PDF document has been requested!', 'Your document will be ready very soon.', 'success');

    });
}

function ajaxResults(regions, scopes, include) {
    var url = '/pprofiles/reports/report1/projects/export';

    showLoader();

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            regions: regions,
            scopes: scopes,
            inc: include
        }
    })
        .done(function (response) {
            var results = JSON.parse(response.results);
            var report_results_template = _.template($('#report_results_template').html())({results: results});
            $('.report-results').html(report_results_template);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            var error = (jqXHR.status === 422) ? JSON.parse(jqXHR.responseText) : '';
            var title = errorThrown;
            var text = (typeof error.message !== 'undefined') ? error.message : '';
            swal(title, text, "error");
            $('.report-results').html('');
        })
        .always(function () {
            hideLoader()
        });
}

function showLoader() {
    $('.loader').addClass('loader-active');
}

function hideLoader() {
    $('.loader').removeClass('loader-active');
}