<div class="modal-dialog">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">{{{ trans('bct/projects::projects/common.action_' . $mode) }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="
                @if($action->exists)
                    {{ route('bct.projects.projects.editAction', [$project->id, $action->id]) }}
                @else
                    {{ route('bct.projects.projects.createAction', $project->id) }}
                @endif
                " data-refresh="actions">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>
                            <div class="row">

                                {{--add more form-group for other fields, just keep the same uniform--}}
                                <div class="form-group{{ Alert::onForm('z_action_type_id', ' has-error') }}">

                                    <label for="action_type" class="control-label">
                                        {{{ trans('bct/projects::actions/model.general.action_type') }}}
                                    </label>
                                    <select class="form-control" name="z_action_type_id" id="action_type" placeholder="{{{ trans('bct/projects::actions/model.general.action_type_help') }}}" >
                                        @foreach($action_types as $action_type)
                                            <option @if($action->z_action_type_id==$action_type->id) selected @endif value="{{$action_type->id}}">{{$action_type->name}}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('z_action_type_id') }}}</span>

                                </div>

                                <div class="form-group">

                                    <label for="details" class="control-label">
                                        {{{ trans('bct/projects::actions/model.general.details') }}}
                                    </label>

                                    <textarea class="form-control" name="details"
                                              placeholder="{{{ trans('bct/projects::actions/model.general.details_help') }}}"
                                              required rows="5">{{ $action->details }}</textarea>

                                    <span class="help-block">{{{ Alert::onForm('details') }}}</span>

                                </div>

                                {{--add more form-group for other fields, just keep the same uniform--}}
                                <div class="form-group">

                                    <label for="contractor_id" class="control-label">
                                        {{{ trans('bct/projects::actions/model.general.contractor_id') }}}
                                    </label>

                                    <select class="form-control" name="z_contractors_id" id="contractor_id">
                                        <option value="">{{{ trans('bct/projects::actions/model.general.contractor_id_help') }}}</option>
                                        @foreach($contractors as $contractor)
                                            <option value="{{ $contractor->id }}" @if($action->z_contractors_id == $contractor->id) selected @endif
                                                    >{{ $contractor->name }}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group">

                                    <label for="action_date" class="control-label">
                                        {{ trans('bct/projects::actions/model.general.date') }}
                                    </label>

                                    <div class='input-group date'>
                                        <input type="text" class="form-control" name="action_date" id="action_date"
                                               placeholder="{{ trans('bct/projects::actions/model.general.date_help') }}"
                                               required autofocus data-parsley-date value="{{ $action->action_date_custom }}">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>

                                </div>

                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="submit-btn-modal" name="submit-btn-modal" class="btn btn-primary">
                {{{ trans('action.save') }}}</button>

            @if($action->exists)
                <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-action="delete"
                        data-url="{{ route('bct.projects.projects.deleteAction', [$project->id, $action->id]) }}">
                    {{{ trans('action.delete') }}}</button>
            @endif

            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
