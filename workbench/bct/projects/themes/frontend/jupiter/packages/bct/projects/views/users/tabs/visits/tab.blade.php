<header class="panel-heading">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip go-to-details" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $user->first_name }} {{ $user->last_name }} {{ trans("bct/projects::users/common.tabs.visits") }}</span>
            </div>

        </div>

    </nav>

</header>

<div class="panel-body">

    <section class="panel panel-default panel-grid">

        {{-- Grid: Header --}}
        <div class="panel-heading hidden-print">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#project-contractors-actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <span class="navbar-brand">{{ trans("bct/projects::visits/common.title") }}</span>
                    </div>

                    {{-- Grid: Actions --}}
                    <div class="collapse navbar-collapse">

                        <ul class="nav navbar-nav navbar-left download-container" data-grid="rep-visits">

                            <li class="dropdown">
                                <a href="#"
                                   class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                                   aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                    <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                    <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                </ul>
                            </li>

                        </ul>

                        {{-- Grid: Filters --}}
                        <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                              data-grid="rep-visits" role="form">

                            <div class="input-group">

                                <span class="input-group-btn">
                                    <select name="column" class="form-control" id="rep-visit-filter-select">
                                        <option value="z_projects.name">{{ trans('bct/projects::visits/model.general.project') }}
                                        </option>
                                        <option value="details">{{ trans('bct/projects::visits/model.general.notes') }}
                                        </option>
                                    </select>

                                    <button class="btn btn-default" type="button" data-grid-calendar id="rep-visit-filter-calendar"
                                            data-range-filter="z_project_visit.created_at">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>

                                <input class="form-control" name="filter" type="text" id="rep-visit-filter-input-search"
                                       placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="rep-visit-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="rep-visits" data-reset id="rep-visit-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>
                                </span>
                            </div>

                            <input type="hidden" data-range-filter="z_project_visit.created_at" data-format="YYYY-MM-DD" data-range-start></button>
                            <input type="hidden" data-range-filter="z_project_visit.created_at" data-format="YYYY-MM-DD" data-range-end></button>

                        </form>

                    </div>

                </div>

            </nav>

        </div>


        <div class="panel-body">

            {{-- Grid: Applied Filters --}}
            <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

                <div id="rep-visits-data-grid_applied" class="btn-group" data-grid="rep-visits"></div>

            </div>


            {{-- Grid: Table --}}
            <div class="table-responsive">

                <table id="rep-visits-data-grid" class="table table-hover"
                       data-source="{{ route('bct.projects.users.visits', $user->id) }}"
                       data-filename="{{ $user->first_name }} {{ $user->last_name }} {{ trans("bct/projects::users/common.tabs.visits") }}"
                       data-grid="rep-visits">
                    <thead>
                        <tr>
                            <th class="col-md-3">{{ trans('bct/projects::visits/model.general.project') }}</th>
                            <th class="col-md-3 sortable" data-sort="details">{{ trans('bct/projects::visits/model.general.notes') }}</th>
                            <th class="col-md-3">{{ trans('bct/projects::visits/model.general.stage') }}</th>
                            <th class="col-md-2 sortable" data-sort="created_at">{{ trans('bct/projects::visits/model.general.date') }}</th>
                            <th class="col-md-1 hidden-print">{{ trans('bct/projects::general/common.details') }}</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>


        <footer class="panel-footer clearfix">

            {{-- Grid: Pagination --}}
            <div class="rep-visits-data-grid_pagination" data-grid="rep-visits"></div>

        </footer>

        {{-- Grid: templates --}}

        @include('bct/projects::users.grid.rep-visits.results')
        @include('bct/projects::users.grid.rep-visits.pagination')
        @include('bct/projects::users.grid.rep-visits.filters')
        @include('bct/projects::users.grid.rep-visits.no_results')

    </section>

</div>