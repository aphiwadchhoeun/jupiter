<script type="text/template" data-grid="reviews" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
            @if (!$currentUser->inRole('repuser') && !$currentUser->inRole('pp-guest'))
                <td>
                    <input content="id" input data-grid-checkbox=""  name="entries[]" type="checkbox" value="<%- r.id %>"
                    <% if (r.approved == false) { %>
                        data-not_approved
                    <% } %>>
                </td>
            @endif
            <td><%- r.project_name %></td>
            <td><%- r.users_names %></td>

            @if (config_projects('bct.projects.settings.enable_project_visit_weather'))
                <td><%- r.weather %></td>
            @endif

            @if (config_projects('bct.projects.settings.enable_project_visit_workers'))
                <td class="text-center"><%- r.workers_count %></td>
            @endif

            <td><%- r.region %></td>
                <td>
                    <div><b>{{trans('bct/projects::projects/model.general.follow_up')}}:</b> <%- r.reason %></div>
                    <div><b>{{ trans('bct/projects::visits/model.general.notes') }}:</b> <%- r.notes_short %></div>
                </td>
            <td><%- r.created_custom %></td>
            <td>
                <% if (r.approved == true) { %>
                    <span class="label label-success">{{ trans('bct/projects::reviews/model.general.approved') }}</span>
                <% } else { %>
                    <span class="label label-danger">{{ trans('bct/projects::reviews/model.general.not_approved') }}</span>
                <% } %>
            </td>
            <td><a href="<%- r.view_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                            class="fa fa-book fa-fw"></i></a></td>
		</tr>

	<% }); %>

</script>
