
$(function() {

    getFiles();

    var form_create_modal = $('#member_file_upload_modal #fileupload');

    var $create_modal = $('#member_file_upload_modal');


    $(document.body).on('click', '.add-file', function (e) {
        $('.filename-edit').addClass('hide');
        $('.filename-edit #filename').val('');
        $('.filename-edit #fileDescription').val('');
        form_create_modal.data('file_id','');
        $('.fileupload-buttonbar').show();
        $create_modal.find('.modal-title').text('Add File');

        $create_modal.modal('show');

        form_create_modal.fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            //url: 'member/document/upload',
            autoUpload: true,
            uploadTemplateId: null,
            downloadTemplateId: null
        }).bind('fileuploaddone', function (e, data) {
            var file = data.result.files[0];
            form_create_modal.data('file_id',file.id)
            $('.fileupload-buttonbar').hide();
            $('.filename-edit').removeClass('hide');
            $(document).on('keydown', '#filename', function(event) {
                if(event.which === 13) return false;
            });

        }).bind('fileuploadcompleted', function (e, data) {

        }).bind('fileuploaddestroy', function (e, data) {
            e.preventDefault;

            swal({
                title: "Are you sure you want to delete this file?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#019964",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                showLoaderOnConfirm: true,
            }).then(function () {
                form_create_modal.fileupload('option', 'destroy').call($('#fileupload'), $.Event('destroy'), data);
            }, function(dismiss) {
                return false;
            });

            return false;
        });

    });
    
    $(document.body).on('click', '#project-file-submit-btn-modal', function(e) {
        e.preventDefault();
        form_create_modal.parsley().validate();
        form_create_modal.removeClass('update');

    });

    $(document.body).on('click', '.btn-file-update', function(e) {
        e.preventDefault();

        $('.fileupload-buttonbar').hide();
        $('.filename-edit').removeClass('hide');

        var file_id = $(this).data('id');
        var file_name = $(this).data('name');
        var file_description = $(this).data('description');
        form_create_modal.data('file_id',file_id);
        console.log($(this).data());
        form_create_modal.addClass('update');
        $create_modal.find('.modal-title').text('Edit File');



        $create_modal.modal('show');
    });

    $(document.body).on('click', '.btn-file-delete', function(e) {
        e.preventDefault;
        var $this = $(this);

        swal({
            title: "Are you sure you want to delete this file?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            var url = $this.data('url');

            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: 'json',
                success: function (response) {
                    swal({
                        title: "Success!",
                        text: "File was deleted",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });
                    getFiles();
                },
                error: function (response) {
                    var errors = JSON.parse(response.responseText).errors;

                    swal({
                        title: "Error!",
                        text: "=(",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    });
                }
            });

        }, function(dismiss) {
            return false;
        });

        return false;

    });

    form_create_modal.parsley().on('form:error', function(formInstance) {
        resetLoadingBtn($('#member_file_upload_modal #project-file-submit-btn-modal'));
    });
    form_create_modal.parsley().on('form:validated', function(formInstance) {
        if (formInstance.isValid() === true) {
            var data = [];

            form_create_modal.find('#filename').val();
            form_create_modal.find('#fileDescription').val();

            data.push({
                id: form_create_modal.data('file_id'),
                document_name: form_create_modal.find('#filename').val(),
                document_description: form_create_modal.find('#fileDescription').val(),
            });

            var url = window.location.origin + '/pprofiles/projects/files/update';
            var action = 'created';
            if(form_create_modal.hasClass('update')){
                action='updated';
            }

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    files_data: data
                },
                success: function (response) {
                    swal({
                        title: "Success!",
                        text: "File was "+action,
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });
                    $create_modal.modal('hide');
                    resetLoadingBtn($('#member_file_upload_modal #project-file-submit-btn-modal'));
                    getFiles();
                },
                error: function (response) {
                    var errors = JSON.parse(response.responseText).errors;
                    $create_modal.modal('hide');
                    resetLoadingBtn($('#member_file_upload_modal #project-file-submit-btn-modal'));

                    swal({
                        title: "Error!",
                        text: "=(",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    });
                }
            });
        }
    });

    function getFiles(){
        // Load existing files:
        var $files = $('tbody.files>tr:not(:first)');
        var $loader = $('#files-block .loader');
        $files.hide()
        $loader.show();
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').data('action-get'),
            dataType: 'json',
        }).done(function (result) {
            $files.remove();
            var files_tmpl = _.template($('#template-files').html())({files: result});
            $('tbody.files').append(files_tmpl);
        }).always(function(){
            $loader.hide();
            $files.show();
        });
    }
});