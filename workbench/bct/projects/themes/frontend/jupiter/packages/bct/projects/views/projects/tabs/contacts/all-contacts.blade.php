{{ Asset::queue('all-contacts-grid', 'bct/projects::projects/js/contacts/contacts.js', ['extension-base', 'bootstrap-typeahead', 'select2-jquery']) }}

<section class="panel panel-default panel-grid">
    {{-- Grid --}}

    {{-- Grid: Header --}}
    <header class="panel-heading">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#project-contacts-all-action">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{{ trans('bct/projects::contacts/common.available') }}}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="project-contacts-all-action">

                    <ul class="nav navbar-nav navbar-left">

                        <li class="dropdown">
                            <a href="#" id="project-contacts-all-exporter"
                               class="dropdown-toggle tip" data-toggle="dropdown" role="button"
                               aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
                                <i class="fa fa-download"></i> <span class="visible-xs-inline">{{ trans('action.export') }}</span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
                                <li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                            </ul>
                        </li>

                    </ul>

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="contacts" role="form">

                        <div class="input-group">

                            <input class="form-control" name="filter" type="text" id="project-available-contacts-filter-input-search"
                                   placeholder="{{{ trans('common.search') }}}">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" id="project-available-contacts-filter-btn-search">
                                        <span class="fa fa-search"></span>
                                    </button>

                                    <button class="btn btn-default" data-grid="contacts" data-reset id="project-available-contacts-filter-btn-reset">
                                        <i class="fa fa-refresh fa-sm"></i>
                                    </button>

                                </span>

                        </div>

                    </form>

                    <div class="nav navbar-nav navbar-right">

                        <a href="{{ route('bct.projects.contacts.create') }}"
                           id="contacts-add-btn"
                           name="contacts-add-btn"
                           data-modal-ajax
                           class="btn btn-success pull-right margin-right5" data-toggle="tooltip"
                           id="project-available-contacts-filter-btn-create"
                           data-original-title="{{{ trans('action.create') }}}">
                            {{ trans('bct/projects::contacts/common.create') }}
                        </a>

                    </div>

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="contacts-data-grid-applied-filters">

            <div id="contacts-data-grid_applied" class="btn-group" data-grid="contacts"></div>

        </div>


        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="contacts-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridAllContacts', $project->id) }}"
                   data-filename="{{ trans('bct/projects::contacts/common.title') }}"
                   data-grid="contacts">
                <thead>
                <tr>
                    <th class="sortable col-md-1"
                        data-sort="id">{{ trans('bct/projects::contacts/model.general.id') }}</th>
                    <th class="sortable col-md-3"
                        data-sort="name">{{ trans('bct/projects::contacts/model.general.name') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="company">{{ trans('bct/projects::contacts/model.general.company') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="phone">{{ trans('bct/projects::contacts/model.general.phone') }}</th>
                    <th class="sortable col-md-2"
                        data-sort="email">{{ trans('bct/projects::contacts/model.general.email') }}</th>
                    <th class="sortable col-md-2"
                    data-sort="email">{{ trans('bct/projects::contacts/model.general.city') }}</th>
                    <th class="col-md-1">{{{ trans('bct/projects::general/common.actions') }}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>


    <footer class="panel-footer clearfix">

        {{-- Grid: Pagination --}}
        <div class="contacts-data-grid_pagination" data-grid="contacts"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.contacts.grid.contacts.results')
    @include('bct/projects::projects.tabs.contacts.grid.contacts.pagination')
    @include('bct/projects::projects.tabs.contacts.grid.contacts.filters')
    @include('bct/projects::projects.tabs.contacts.grid.contacts.no_results')

</section>