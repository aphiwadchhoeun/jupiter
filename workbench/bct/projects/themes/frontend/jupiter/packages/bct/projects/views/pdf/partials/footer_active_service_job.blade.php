<html>
    <head>
        <style>
            .pdf-footer {
            }

            .pdf-footer > * {
                font-size: 10px !important;
            }

            .pdf-footer .footer-left {
                position: absolute;
                left: 0px;
                top: 0px;
                font-weight: bold;
                width: 40%;
            }

            .pdf-footer .footer-center {
                position: absolute;
                left: 450px;
                top: 0px;
                font-weight: bold;
                width: 40%;
            }

            .pdf-footer .footer-right {
                position: absolute;
                right: 25px;
                top: 0px;
                text-align: right;
                font-weight: bold;
                width: 20%;
            }
        </style>
        <script>
            function subst() {
                var vars = {};
                var x = window.location.search.substring(1).split('&');
                for (var i in x) {
                    var z = x[i].split('=', 2);
                    vars[z[0]] = unescape(z[1]);
                }
                var x = ['frompage', 'topage', 'page', 'webpage', 'section', 'subsection', 'subsubsection'];
                for (var i in x) {
                    var y = document.getElementsByClassName(x[i]);
                    for (var j = 0; j < y.length; ++j) y[j].textContent = vars[x[i]];
                }
            }
        </script>
    </head>
    <body style="border:0; margin:0;" onload="subst()">

        <div class="pdf-footer">
            <div class="footer-left">
                <span>Council Active Service Job List Report</span>
                <span style="margin-left: 25px">OPEIU #8, afl-clo</span>
            </div>
            <div class="footer-center">
                Includes Union Jobs, Jobs with Union GC's, and GC's with PA's
            </div>
            <div class="footer-right">
                Page <span class="page"></span> of <span class="toPage"></span>
            </div>
        </div>

    </body>
</html>