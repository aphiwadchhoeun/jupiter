<script type="text/template" data-grid="project-action" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.action_type %></td>
        <td><%- r.details %></td>
        <td><%- r.username %></td>
        <td><%- r.contractor %></td>
        <td><%- r.action_date %></td>
        @if (!$currentUser->inRole('pp-guest'))
        <td class="hidden-print">
            <% if (r.view_uri) { %>
                <a href="<%- r.view_uri %>" id="action_edit" name="action_edit" data-modal data-toggle="tooltip" class="btn btn-primary btn-circle"
                       data-placement="right" data-original-title="{{{ trans('bct/projects::general/common.edit') }}}">
                        <i class="fa fa-book fa-fw"></i></a>
            <% } %>
        </td>
        @endif
    </tr>

    <% }); %>

</script>
