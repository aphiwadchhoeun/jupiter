@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::dashboard/common.title') }}
@stop

{{-- Queue assets --}}

{{ Asset::queue('dashboard-c3-css', 'bower_components/c3/c3.min.css') }}
{{ Asset::queue('dashboard-style', 'bct/projects::dashboard/css/dashboard.css') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('dashboard-d3', 'bower_components/d3/d3.min.js', 'jquery') }}
{{ Asset::queue('dashboard-c3', 'bower_components/c3/c3.min.js', 'jquery') }}
{{ Asset::queue('dashboard-script', 'bct/projects::dashboard/js/dashboard.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    {{-- Form: CSRF Token --}}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="col-md-12">
            <h3>{{ trans('bct/projects::dashboard/common.title') }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="well no-scroll">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('bct/projects::dashboard/common.share_by_area') }}</h4>
                    </div>
                    <div class="dashboard-filter col-md-6 col-xs-6">
                        <div class="col-md-6 col-xs-6">
                            <select class="form-control" name="select_scope" id="select_scope">
                                <option value="">{{ trans('bct/projects::dashboard/common.select_scope') }}</option>
                                @foreach($scopes as $scope)
                                    <option value="{{ $scope->name }}">{{ $scope->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <select class="form-control" name="select_area" id="select_area">
                                <option value="">{{ trans('bct/projects::dashboard/common.select_area') }}</option>
                                <option value="OUT_OF_REGION">{{ trans('bct/projects::dashboard/common.out_of_region') }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="market-share-by-area" class="dashboard-box pie-chart"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="well no-scroll">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('bct/projects::dashboard/common.totals') }}</h4>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <select class="form-control" name="select_year" id="select_year">
                                <option value="">{{ trans('bct/projects::dashboard/common.year') }}</option>
                                <?php $d = \Carbon\Carbon::now(); ?>
                                @foreach(range(($d->year-1), ($d->year - 10)) as $year)
                                    <option value="{{ $year }}">{{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div id="totals"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('bct/projects::dashboard/common.top_non_union') }}</h4>
                    </div>
                    <div class="dashboard-filter col-md-6 col-xs-6">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <select class="form-control" name="select_scope_non_union" id="select_scope_non_union">
                                    <option value="">{{ trans('bct/projects::dashboard/common.select_scope') }}</option>
                                    @foreach($scopes as $scope)
                                        <option value="{{ $scope->name }}">{{ $scope->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <select class="form-control" name="select_area_non_union" id="select_area_non_union">
                                    <option value="">{{ trans('bct/projects::dashboard/common.select_area') }}</option>
                                    <option value="OUT_OF_REGION">{{ trans('bct/projects::dashboard/common.out_of_region') }}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <table class="table table-condensed table-bordered dashboard-table">
                        <thead>
                        <tr>
                            <th>{{ trans('bct/projects::dashboard/common.contractor_name') }}</th>
                            <th>{{ trans('bct/projects::dashboard/common.contractor_value') }}</th>
                        </tr>
                        </thead>
                        <tbody id="top-non-union-contractors"></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('bct/projects::dashboard/common.top_union') }}</h4>
                    </div>
                    <div class="dashboard-filter col-md-6 col-xs-6">
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <select class="form-control" name="select_scope_union" id="select_scope_union">
                                    <option value="">{{ trans('bct/projects::dashboard/common.select_scope') }}</option>
                                    @foreach($scopes as $scope)
                                        <option value="{{ $scope->name }}">{{ $scope->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="form-group">
                                <select class="form-control" name="select_area_union" id="select_area_union">
                                    <option value="">{{ trans('bct/projects::dashboard/common.select_area') }}</option>
                                    <option value="OUT_OF_REGION">{{ trans('bct/projects::dashboard/common.out_of_region') }}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <table class="table table-condensed table-bordered dashboard-table">
                        <thead>
                        <tr>
                            <th>{{ trans('bct/projects::dashboard/common.contractor_name') }}</th>
                            <th>{{ trans('bct/projects::dashboard/common.contractor_value') }}</th>
                        </tr>
                        </thead>
                        <tbody id="top-union-contractors"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('bct/projects::dashboard/common.follow_up') }}</h4>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <select class="form-control" name="select_area_follow_up" id="select_area_follow_up">
                                <option value="">{{ trans('bct/projects::dashboard/common.select_area') }}</option>
                                <option value="OUT_OF_REGION">{{ trans('bct/projects::dashboard/common.out_of_region') }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div>
                    <table class="table table-condensed table-bordered dashboard-table">
                        <thead>
                        <tr>
                            <th>{{ trans('bct/projects::dashboard/common.project_name') }}</th>
                            <th>{{ trans('bct/projects::dashboard/common.project_follow_up') }}</th>
                        </tr>
                        </thead>
                        <tbody id="follow_up"></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="well">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ trans('bct/projects::dashboard/common.rep_visits') }}</h4>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <select class="form-control" name="select_area_rep_visits" id="select_area_rep_visits">
                                <option value="">{{ trans('bct/projects::dashboard/common.select_area') }}</option>
                                <option value="OUT_OF_REGION">{{ trans('bct/projects::dashboard/common.out_of_region') }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div>
                    <table class="table table-condensed table-bordered dashboard-table">
                        <thead>
                        <tr>
                            <th>{{ trans('bct/projects::dashboard/common.user_name') }}</th>
                            <th>{{ trans('bct/projects::dashboard/common.project_name') }}</th>
                            <th>{{ trans('bct/projects::dashboard/common.visit_date') }}</th>
                        </tr>
                        </thead>
                        <tbody id="rep-visits"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop
