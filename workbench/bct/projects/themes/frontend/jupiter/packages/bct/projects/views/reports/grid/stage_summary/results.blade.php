<script type="text/template" data-grid="stage_summary" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>

            <td><%- r.stage_name %></td>
			<% if(r.projects_total>0){ %>
			<td><a href="<%- r.all_projects_url %>"><%- r.projects_total %><a/></td>
			<% } else { %>
			<td><%- r.projects_total %></td>
			<% } %>

		</tr>

	<% }); %>

</script>
