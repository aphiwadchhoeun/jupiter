<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{{ trans("bct/projects::contact_types/common.title") }}} {{{ trans("action.{$mode}") }}}</h4>
</div>
<div class="modal-body">
    {{-- Form --}}
    <form id="content-form-modal" action="
        @if ($type->exists)
            {{ route('bct.projects.contact_types.edit', $type->id) }}
        @else
    {{ route('bct.projects.contact_types.create') }}
        @endif
            " method="post" data-refresh="contact_types">

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="panel-body">

            <div class="col-md-12">

                {{-- Form: General --}}
                <fieldset>
                    <div class="row">

                        {{-- Form:name --}}
                        <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                            <label for="name" class="control-label">
                                {{{ trans('bct/projects::contact_types/model.general.name') }}}
                            </label>

                            <input type="text" class="form-control" name="name" id="name" required autofocus
                                   placeholder="{{{ trans('bct/projects::contact_types/model.general.name_help') }}}"
                                   value="{{{ input()->old('name', $type->name) }}}">

                            <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                        </div>

                    </div>

                </fieldset>

            </div>

        </div>
        
    </form>
</div>

<div class="modal-footer">
    @if ($type->exists)
        <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-toggle="tooltip"
           data-grid-bulk-action="delete" data-target="modal-confirm"
           data-url="{{ route('bct.projects.contact_types.delete', $type->id) }}"
           data-original-title="{{{ trans('action.delete') }}}">
            <i class="fa fa-trash fa-fw"></i>{{{ trans('action.delete') }}}
        </button>

    @endif
    <button id="submit-btn-modal" name="submit-btn-modal" class="btn btn-primary" data-toggle="tooltip"
            data-original-title="{{{ trans('action.save') }}}">
        <i class="fa fa-save fa-fw"></i></button>

    <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>

</div>