var AllContractorsExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    AllContractorsExtension = Object.create(ExtensionBase);
    AllContractorsExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'contractors'
    });

    // Initialize functions
    AllContractorsExtension.Index.init = function () {
        AllContractorsExtension.Index.dataGrid();
        AllContractorsExtension.Index.listeners();
    };

    // Add Listeners
    AllContractorsExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', AllContractorsExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    AllContractorsExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };


    // Data Grid initialization
    AllContractorsExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            callback: function (data) {
            }
        };

        AllContractorsExtension.Index.Grid = $.datagrid('contractors', '#contractors-data-grid', '.contractors-data-grid_pagination', '#contractors-data-grid_applied', config);
    };

    // Job done, lets run.
    AllContractorsExtension.Index.init();

})(window, document, jQuery);


$('#contractors-add-btn').on('click', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modalAjax = $('#modal-form-ajax');

    $modalAjax.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (response) {
            $modalAjax.modal('show');

            initAjaxModal();

            $('#modal-form-ajax').on('click', '#submit-btn', function (e) {
                e.preventDefault();

                var form_modal = $modalAjax.find('#content-form');

                swal({
                    title: "Are you sure you want to save?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#019964",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    showLoaderOnConfirm: true,
                }).then(function () {
                    $.post(
                        form_modal.attr('action'),
                        form_modal.serialize(),
                        function (data) {
                            if (typeof data.success == 'undefined') {
                                $modalAjax.find('.modal-content').html(data);
                                initAjaxModal();
                            } else {
                                $modalAjax.modal('hide');
                                AllContractorsExtension.Index.Grid.refresh();
                            }
                        }
                    );
                });
            });
        } else {
            location.reload();
        }
    });

    function initAjaxModal() {
        $modalAjax.find('[data-mask]').mask('(999) 999-9999');
        $modalAjax.find('[data-mask-ein]').mask('99-9999999');
        $modalAjax.find('[data-mask-zip]').mask('99999?-9999');
        $modalAjax.find('input[name="is_union"]').bootstrapToggle({
            width: 77,
            height: 43
        });
    }
});


$('#contractors-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            $('#scope_id').select2();

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                            if (data == 'success') {
                                $modal.modal('hide');
                                ProjectContractorExtension.Index.Grid.refresh();
                                contractorsSummaryUpdate();
                            } else {
                                swal(data);
                            }
                        })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });
            }

        } else {
            location.reload();
        }
    });
});

$('[data-modal-create-na-ajax]').on('click', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            $('#scope_id').select2();

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                                if (data == 'success') {
                                    $modal.modal('hide');
                                    ProjectContractorExtension.Index.Grid.refresh();
                                    contractorsSummaryUpdate();
                                } else {
                                    swal(data);
                                }
                            })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });
            }

        } else {
            location.reload();
        }
    });
});

