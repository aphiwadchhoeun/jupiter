jQuery('document').ready(function ($) {
    var hash = window.location.hash;

    $('.nav-tabs a').on('click', function (e) {
        e.preventDefault();
        switchContent(this, this.hash);
    });

    $('.go-to-details').on('click', function (e) {
        e.preventDefault();
        $('.nav-tabs a[href="#details"]').click();
    });

    $(document.body).on('click', '.go-to-tab', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        $('.nav-tabs a[href="' + id + '"]').click();
    });

    $(document.body).on('click', '.btn-swal-error', function (e) {
        e.preventDefault();

        swal({
            title: "In order to add contractors please specify project classification",
            type: "error",
            showConfirmButton: true
        });
    });

    if (!hash) {
        $('.nav-tabs a[href="#details"]').click();
    } else {
        $('.nav-tabs a[href="' + hash + '"]').click();
    }

});

function ajaxBtnStatus(target, className) {
    if (!target.hasClass('disabled')) {
        target.addClass('disabled')
            .find('i')
            .removeClass(className)
            .addClass('fa-spinner fa-spin');
    } else {
        target.removeClass('disabled')
            .find('i')
            .removeClass('fa-spinner fa-spin')
            .addClass(className);
    }
}

function contractorsSummaryUpdate(){
    var contractorsDiv = $('#contractors-div');
    var contractorsUrl = contractorsDiv.data('update-url');
    var unionValue = $('#union-value');
    unionValue.html('<i class="fa fa-spinner fa-spin"></i>');
    $('#contractors-counter').html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: contractorsUrl
    }).done(function(result) {
        contractorsDiv.html(result.html);
        unionValue.text(result.union);
    });
}

function contactsSummaryUpdate(){
    var contactsDiv = $('#contacts-div');
    var contactsUrl = contactsDiv.data('update-url');
    $('#contacts-counter').html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: contactsUrl
    }).done(function(result) {
        contactsDiv.html(result.html);
    });
}

function membersSummaryUpdate(){
    var membersDiv = $('#members-div');
    var membersUrl = membersDiv.data('update-url');
    $('#members-counter').html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: membersUrl
    }).done(function(result) {
        membersDiv.html(result.html);
    });
}


function visitsSummaryUpdate(){
    var visitsDiv = $('#visits-div');
    var visitsUrl = visitsDiv.data('update-url');
    $('#visits-counter').html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: visitsUrl
    }).done(function(result) {
        visitsDiv.html(result.html);
    });
}

function projectFollowUpUpdate(){
    var follow_up_div = $('#follow_up_date_details');
    var follow_up_url = follow_up_div.data('update-url');
    follow_up_div.html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: follow_up_url
    }).done(function(result) {
        follow_up_div.html(result.html);
    });
}

function projectStatusUpdate(){
    var status_div = $('#status_details');
    var status_url = status_div.data('update-url');
    status_div.html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: status_url
    }).done(function(result) {
        status_div.html(result.html);
    });
}


function actionsSummaryUpdate(){
    var actionsDiv = $('#actions-div');
    var actionsUrl = actionsDiv.data('update-url');
    $('#actions-counter').html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: actionsUrl
    }).done(function(result) {
        actionsDiv.html(result.html);
    });
}

function listsSummaryUpdate(){
    var listsDiv = $('#lists-div');
    var listsUrl = listsDiv.data('update-url');
    $('#lists-counter').html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: listsUrl
    }).done(function(result) {
        listsDiv.html(result.html);
    });
}

function filesSummaryUpdate(){
    var filesDiv = $('#files-div');
    var filesUrl = filesDiv.data('update-url');
    $('#files-counter').html('<i class="fa fa-refresh fa-spin"></i>');
    $.ajax({
        url: filesUrl
    }).done(function(result) {
        filesDiv.html(result.html);
    });
}

