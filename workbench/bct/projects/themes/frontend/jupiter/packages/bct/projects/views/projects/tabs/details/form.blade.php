@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans("action.{$mode}") }}} {{ trans('bct/projects::projects/common.title') }}
@stop


{{-- Inline scripts --}}
@section('scripts')
    @parent
    <script type="text/javascript"
            src="{{ URL::asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyC3Q6hXQv25HIm1ONXYqF9ffi7MlCgxl0s&libraries=places&language=en') }}"></script>


    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
    {{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['jquery', 'moment']) }}
    {{ Asset::queue('selectize', 'bower_components/selectize/dist/js/standalone/selectize.min.js', 'jquery') }}

    {{ Asset::queue('project-form', 'bct/projects::projects/js/form.js', 'jquery') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}
    {{ Asset::queue('selectize', 'bower_components/selectize/dist/css/selectize.css') }}
    {{ Asset::queue('selectize-bootstrap3', 'bower_components/selectize/dist/css/selectize.bootstrap3.css', 'bootstrap') }}
    {{ Asset::queue('form-css', 'bct/projects::projects/css/form.css') }}

@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default">
        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post">

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip" href="@if(!$project->exists)
                                    {{ route('bct.projects.projects.search') }}
                                    @else
                                    {{ route('bct.projects.projects.details', $project->id) }}
                                    @endif" data-toggle="tooltip"
                                       data-original-title="{{{ trans('bct/projects::general/common.back')}}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{ trans('bct/projects::general/common.back') }}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">
                                @if($project->exists)
                                    {{{ $project->name }}}
                                @else
                                    {{{ trans("bct/projects::projects/common.title") }}}
                                @endif
                                {{{ trans("action.{$mode}") }}}
                            </span>
                        </div>


                        <button id="submit-btn" class="btn btn-primary pull-right" data-toggle="tooltip"
                                data-original-title="{{{ trans('action.save') }}}">
                            <i class="fa fa-save fa-fw"></i>
                        </button>

                    </div>

                </nav>

            </header>

            <div class="panel-body">

                <div class="row">

                    <div class="col-md-6">
                        {{-- Form:name --}}
                        <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                            <label for="name" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.name') }}}
                            </label>

                            <input type="text" class="form-control" name="name" id="name"
                                   placeholder="{{{ trans('bct/projects::projects/model.general.name_help') }}}"
                                   value="{{{ input()->old('name', $project->name) }}}">

                            <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                        </div>
                    </div>

                    <div class="col-md-3">
                        {{-- Form:z_project_status_id --}}
                        <div class="form-group{{ Alert::onForm('z_project_status_id', ' has-error') }}">

                            <label for="z_project_status_id" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.status_id') }}}
                            </label>

                            <select class="form-control" name="z_project_status_id" id="z_project_status_id">
                                @foreach($statuses as $status)
                                    <option value="{{ $status->id }}"
                                            @if(input()->old('z_project_status_id', $project->z_project_status_id) == $status->id) selected @endif
                                    >{{ $status->name }}</option>
                                @endforeach
                            </select>

                            <span class="help-block">{{{ Alert::onForm('z_project_status_id') }}}</span>

                        </div>
                    </div>

                    <div class="col-md-3">
                        {{-- Form:follow_up_date --}}
                        <div class="form-group{{ Alert::onForm('follow_up_date', ' has-error') }}">

                            <label for="follow_up_date" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.follow_up_date') }}}
                            </label>

                            <div class='input-group date'>
                                <input type="text" class="form-control" name="follow_up_date" id="follow_up_date"
                                       placeholder="{{{ trans('bct/projects::projects/model.general.follow_up_date_help') }}}"
                                       value="{{{ input()->old('follow_up_date', data_get($project, 'follow_up_date_custom')) }}}">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <span class="help-block">{{{ Alert::onForm('follow_up_date') }}}</span>

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-3">
                        {{-- Form:z_project_classification_id --}}
                        <div class="form-group{{ Alert::onForm('z_project_classification_id', ' has-error') }}">

                            <label for="z_project_classification_id" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.classification_id') }}}
                            </label>

                            <select class="form-control" name="z_project_classification_id"
                                    id="z_project_classification_id" data-contractors-count="{{$contractors_count}}">
                                <option value="">{{ trans('bct/projects::projects/model.general.classification_id_help') }}
                                </option>
                                @foreach($classifications as $classification)
                                    <option value="{{ $classification->id }}"
                                            @if(input()->old('z_project_classification_id', $project->z_project_classification_id) == $classification->id) selected @endif
                                    >{{ $classification->name }}</option>
                                @endforeach
                            </select>

                            <span class="help-block">{{{ Alert::onForm('z_project_classification_id') }}}</span>

                        </div>
                    </div>

                    <div class="col-md-3">
                        {{-- Form:z_prevailing_wage_id --}}
                        <div class="form-group{{ Alert::onForm('z_prevailing_wage_id', ' has-error') }}">

                            <label for="z_prevailing_wage_id" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.prevailing_wage_id') }}}
                            </label>

                            <select class="form-control" name="z_prevailing_wage_id" id="z_prevailing_wage_id">
                                <option value="">{{ trans('bct/projects::projects/model.general.prevailing_wage_id_help') }}
                                </option>
                                @foreach($prevailingWages as $prevailingWage)
                                    <option value="{{ $prevailingWage->id }}"
                                            @if(input()->old('z_prevailing_wage_id', $project->z_prevailing_wage_id) == $prevailingWage->id) selected @endif
                                    >{{ $prevailingWage->name }}</option>
                                @endforeach
                            </select>

                            <span class="help-block">{{{ Alert::onForm('z_prevailing_wage_id') }}}</span>

                        </div>
                    </div>

                    <div class="col-md-3">
                        {{-- Form:z_project_visibility_id --}}
                        <div class="form-group{{ Alert::onForm('z_project_visibility_id', ' has-error') }}">

                            <label for="z_project_visibility_id" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.visibility_id') }}}
                            </label>

                            <select class="form-control" name="z_project_visibility_id" id="z_project_visibility_id">
                                <option value="">{{ trans('bct/projects::projects/model.general.visibility_id_help') }}
                                </option>
                                @foreach($visibilities as $visibility)
                                    <option value="{{ $visibility->id }}"
                                            @if(input()->old('z_project_visibility_id', $project->z_project_visibility_id) == $visibility->id) selected @endif
                                    >{{ $visibility->name }}</option>
                                @endforeach
                            </select>

                            <span class="help-block">{{{ Alert::onForm('z_project_visibility_id') }}}</span>

                        </div>
                    </div>

                    <div class="col-md-3">
                        {{-- Form:is_class_a --}}
                        <div class="form-group{{ Alert::onForm('is_class_a', ' has-error') }}">

                            <label for="is_class_a" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.is_class_a') }}}
                            </label>

                            <select class="form-control" name="is_class_a" id="is_class_a">
                                <option value="">{{ trans('bct/projects::projects/model.general.class_a_help') }}</option>
                                <option value="0" @if(input()->old('is_class_a', $project->is_class_a) === 0) selected @endif>No</option>
                                <option value="1" @if(input()->old('is_class_a', $project->is_class_a) === 1) selected @endif>Yes</option>
                            </select>

                            <span class="help-block">{{{ Alert::onForm('is_class_a') }}}</span>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        {{-- Form:description --}}
                        <div class="form-group{{ Alert::onForm('description', ' has-error') }}">

                            <label for="description" class="control-label">
                                {{{ trans('bct/projects::projects/model.general.description') }}}
                            </label>

                            <input type="text" class="form-control" name="description"
                              placeholder="{{{ trans('bct/projects::projects/model.general.description_help') }}}"
                                   value="{{ input()->old('description', $project->description) }}"
                                >

                            <span class="help-block">{{{ Alert::onForm('description') }}}</span>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-6">
                                {{-- Form:z_project_types_id --}}
                                <div class="form-group{{ Alert::onForm('z_project_types_id', ' has-error') }}">

                                    <label for="z_project_types_id" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.type_id') }}}
                                    </label>

                                    <select class="form-control" name="z_project_types_id" id="z_project_types_id">
                                        <option value="">{{{ trans('bct/projects::projects/model.general.type_id_help') }}}
                                        </option>
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}"
                                                    @if(input()->old('z_project_types_id', $project->z_project_types_id) == $type->id) selected @endif
                                            >{{ $type->type }}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('z_project_types_id') }}}</span>

                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- Form:z_project_values_id --}}
                                <div class="form-group{{ Alert::onForm('z_project_values_id', ' has-error') }}">

                                    <label for="z_project_values_id" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.value_id') }}}
                                    </label>

                                    <select class="form-control" name="z_project_values_id" id="z_project_values_id">
                                        <option value="">{{{ trans('bct/projects::projects/model.general.value_id_help') }}}
                                        </option>
                                        @foreach($values as $value)
                                            <option value="{{ $value->id }}"
                                                    @if(input()->old('z_project_values_id', $project->z_project_values_id) == $value->id) selected @endif
                                            >{{ $value->value }}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('z_project_values_id') }}}</span>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6">
                        @if($mode == 'create')

                            {{-- Form:follow up reason--}}
                            <div class="form-group{{ Alert::onForm('followup_reason', ' has-error') }}">

                                <label for="followup_reason" class="control-label">
                                    {{{ trans('bct/projects::projects/model.general.follow_up_reason') }}}
                                </label>

                                <input type="text" class="form-control" name="followup_reason"
                                       placeholder="{{{ trans('bct/projects::projects/model.general.follow_up_reason_help') }}}"
                                       value="{{ input()->old('followup_reason', '') }}">

                                <span class="help-block">{{{ Alert::onForm('followup_reason') }}}</span>

                            </div>

                        @else

                            <div class="form-group">
                                <label for="tags" class="control-label">
                                    {{ trans('bct/projects::general/common.tags') }}
                                </label>
                                <select name="tags[]" id="project-tags-list" class="form-control" multiple>
                                    @if (isset($all_tags))
                                        @foreach ($all_tags as $tag)
                                            <option @if(in_array($tag,$selected_tags)) selected @endif value="{{ $tag }}">{{ $tag }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        @endif

                    </div>

                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-6">
                                {{--Form:z_project_stage_id --}}
                                <div class="form-group{{ Alert::onForm('z_project_stage_id', ' has-error') }}">

                                    <label for="stages" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.stage') }}}
                                    </label>

                                    <select class="form-control" name="stages[]"
                                            id="stages" multiple>
                                        @foreach($stages as $stage)
                                            <option value="{{ $stage->id }}"@if($selected_stages->contains($stage)) selected @endif>{{ $stage->name }}</option>
                                        @endforeach
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('stages') }}}</span>

                                </div>
                            </div>

                            <div class="col-md-6">
                                {{--Form:reference_details --}}
                                <div class="form-group{{ Alert::onForm('reference_details', ' has-error') }}">

                                    <label for="reference_details" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.reference_details') }}}
                                    </label>

                                    <input type="text" class="form-control" name="reference_details" id="reference_details"
                                           placeholder="{{{ trans('bct/projects::projects/model.general.reference_details_help') }}}"
                                           value="{{{ input()->old('reference_details', $project->reference_details) }}}">

                                    <span class="help-block">{{{ Alert::onForm('reference_details') }}}</span>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                @if($mode == 'create')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tags" class="control-label">
                                    {{ trans('bct/projects::general/common.tags') }}
                                </label>
                                <select name="tags[]" id="project-tags-list" class="form-control" multiple>
                                    @if (isset($all_tags))
                                        @foreach ($all_tags as $tag)
                                            <option @if(in_array($tag,$selected_tags)) selected @endif value="{{ $tag }}">{{ $tag }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-6">

                        <label for="reference_details" class="control-label">
                            {{ trans('bct/projects::projects/common.project_map') }}
                        </label>

                        <div class="panel panel-primary">

                            <div class="panel-heading">{{ trans('bct/projects::projects/common.move_pin_to_update') }}</div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <div id="map-canvas"></div>
                                </div>

                                <div class="form-group">

                                    <label class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.suggested_address') }}}
                                    </label>

                                    <div class="row">
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" id="searchmap" value="">
                                        </div>
                                        <div class="col-md-2">
                                            <button id="search-suggest" class="btn btn-primary pull-right" data-toggle="tooltip"
                                                    data-address=""
                                                    data-state=""
                                                    data-county=""
                                                    data-city=""
                                                    data-zipcode=""
                                                    data-original-title="{{{ trans('bct/projects::projects/common.use_suggested_addres') }}}">
                                                <i class="fa fa-edit fa-fw"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <div>
                                        <p class="text14">{{ trans('bct/projects::projects/common.user_address_to_search') }}</p>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-12">
                                {{-- Form:address --}}
                                <div class="form-group{{ Alert::onForm('address', ' has-error') }}">

                                    <label for="address" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.address') }}}
                                    </label>

                                    <input type="text" class="form-control" name="address" id="address"
                                           placeholder="{{{ trans('bct/projects::projects/model.general.address_help') }}}"
                                           value="{{{ input()->old('address', $project->address) }}}">

                                    <span class="help-block">{{{ Alert::onForm('address') }}}</span>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {{-- Form:city --}}
                                <div class="form-group{{ Alert::onForm('city', ' has-error') }}">

                                    <label for="city" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.city') }}}
                                    </label>

                                    <input type="text" class="form-control" name="city" id="city"
                                           placeholder="{{{ trans('bct/projects::projects/model.general.city_help') }}}"
                                           value="{{{ input()->old('city', $project->city) }}}">

                                    <span class="help-block">{{{ Alert::onForm('city') }}}</span>

                                </div>
                            </div>
                            <div class="col-md-6">
                                {{-- Form:state --}}
                                <div class="form-group{{ Alert::onForm('state', ' has-error') }}">

                                    <label for="state" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.state') }}}
                                    </label>

                                    <input type="text" class="form-control" name="state" id="state"
                                           placeholder="{{{ trans('bct/projects::projects/model.general.state_help') }}}"
                                           value="{{{ input()->old('state', $project->state) }}}">

                                    <span class="help-block">{{{ Alert::onForm('state') }}}</span>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {{-- Form:county --}}
                                <div class="form-group{{ Alert::onForm('county', ' has-error') }}">

                                    <label for="county" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.county') }}}
                                    </label>

                                    <input type="text" class="form-control" name="county" id="county"
                                           placeholder="{{{ trans('bct/projects::projects/model.general.county_help') }}}"
                                           value="{{{ input()->old('county', $project->county) }}}">

                                    <span class="help-block">{{{ Alert::onForm('county') }}}</span>

                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- Form:zipcode --}}
                                <div class="form-group{{ Alert::onForm('zipcode', ' has-error') }}">

                                    <label for="zipcode" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.zip') }}}
                                    </label>

                                    <input type="text" class="form-control" name="zipcode" id="zipcode"
                                           placeholder="{{{ trans('bct/projects::projects/model.general.zip_help') }}}"
                                           value="{{{ input()->old('zipcode', $project->zipcode) }}}">

                                    <span class="help-block">{{{ Alert::onForm('zipcode') }}}</span>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ Alert::onForm('region', ' has-error') }}">

                                    <label for="region" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.region') }}}
                                    </label>

                                    <input type="text" class="form-control" name="region" id="region" readonly
                                           placeholder="{{{ trans('bct/projects::projects/model.general.region_help') }}}"
                                           value="{{{ input()->old('region', $project->region) }}}">

                                    <span class="help-block">{{{ Alert::onForm('region') }}}</span>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group{{ Alert::onForm('region_abrv', ' has-error') }}">

                                    <label for="region_abrv" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.region_abrv') }}}
                                    </label>

                                    <input type="text" class="form-control" name="region_abrv" id="region_abrv" readonly
                                           placeholder="{{{ trans('bct/projects::projects/model.general.region_abrv') }}}"
                                           value="{{{ input()->old('region_abrv', $project->region_abrv) }}}">

                                    <span class="help-block">{{{ Alert::onForm('region_abrv') }}}</span>

                                </div>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" name="lat" id="lat" readonly
                               value="{{{ input()->old('lat', $project->lat) }}}">

                        <input type="hidden" class="form-control" name="lng" id="lng" readonly
                               value="{{{ input()->old('lng', $project->lng) }}}">

                        <input type="hidden" class="form-control" name="budgetgroup" id="budgetgroup" readonly
                               value="{{{ input()->old('budgetgroup', $project->budgetgroup) }}}">

                    </div>
                </div>


            </div>
        </form>
    </section>
@stop
