@extends('bct/projects::default')

{{-- Page title --}}
@section('title'){{--
    --}}@parent{{--
    --}} Projects Report{{--
--}}@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('selectize', 'bower_components/selectize/dist/js/standalone/selectize.min.js', 'jquery') }}
    {{ Asset::queue('encodeurl', 'bct/projects::custom/js/encodeurl.js', 'jquery') }}
    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}

    {{ Asset::queue('report1', 'bct/projects::reports/js/report1.js', ['jquery', 'swal']) }}

@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('selectize-css', 'bower_components/selectize/dist/css/selectize.css') }}
    {{ Asset::queue('selectize-bootstrap3', 'bower_components/selectize/dist/css/selectize.bootstrap3.css', 'bootstrap') }}
@stop

{{-- Page --}}
@section('page-extension')
    @parent

    {{-- Grid --}}
    <section class="panel panel-default">

        {{-- Grid: Header --}}
        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <ul class="nav navbar-nav navbar-cancel">
                            <li>
                                <a class="tip" href="{{route('bct.projects.reports.index')}}" data-toggle="tooltip" data-original-title="Cancel">
                                    <i class="fa fa-reply"></i> <span class="visible-xs-inline">Cancel</span>
                                </a>
                            </li>
                        </ul>
                        <span class="navbar-brand">{{ trans('bct/projects::reports/common.tabs.report1_projects') }}</span>
                    </div>

                    <div class="pull-right">
                        <a href="#"
                           class="btn btn-link pull-right btn-download-pdf with-select hide"
                           data-toggle="tooltip"
                           data-original-title="{{{ trans('bct/projects::general/common.print') }}}">
                            <i class="fa fa-download fa-fw"></i>
                        </a>
                    </div>

                </div>

            </nav>

        </header>


        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">

                    <nav class="navbar navbar-default navbar-actions">

                        <div class="row">

                            <div class="col-md-2">
                                <label>{{ trans('bct/projects::reports/common.text.report1.select_regions') }}:</label>
                            </div>

                            <div class="col-md-6">
                                <select name="regions[]" id="regions-report1" class="form-control" multiple>
                                    <option value="">{{ trans('bct/projects::reports/common.text.report1.select_regions') }}
                                        ...
                                    </option>

                                </select>
                            </div>

                            <div class="col-md-4">
                                <button class="btn btn-default navbar-left regions-report1-select-all margin-hor-sides">{{ trans('bct/projects::general/common.select_all') }}</button>
                                <button class="btn btn-default navbar-left regions-report1-deselect-all hide margin-hor-sides">{{ trans('bct/projects::general/common.deselect_all') }}</button>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-2">
                                <label>{{ trans('bct/projects::reports/common.text.report1.select_scopes') }}:</label>
                            </div>

                            <div class="col-md-6">
                                <select name="scopes[]" id="scopes-report1" class="form-control" multiple>
                                    <option value="">{{ trans('bct/projects::reports/common.text.report1.select_scopes') }}
                                        ...
                                    </option>
                                    @foreach($scopes as $scope)
                                        <option value="{{ $scope->name }}">{{ $scope->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <button class="btn btn-default navbar-left scopes-report1-select-all margin-hor-sides">{{ trans('bct/projects::general/common.select_all') }}</button>
                                <button class="btn btn-default navbar-left scopes-report1-deselect-all hide margin-hor-sides">{{ trans('bct/projects::general/common.deselect_all') }}</button>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-2"><label>{{ trans('bct/projects::reports/common.text.report1.include_non_union_gc') }}:</label></div>

                            <div class="col-md-6"><input type="checkbox" id="include_non_union_gc" checked></div>

                        </div>

                    </nav>

                </div>

            </div>


            <div class="row">
                <div class="loader" style="position: static">
                    <div class="sk-circle">
                        <div class="sk-circle1 sk-child"></div>
                        <div class="sk-circle2 sk-child"></div>
                        <div class="sk-circle3 sk-child"></div>
                        <div class="sk-circle4 sk-child"></div>
                        <div class="sk-circle5 sk-child"></div>
                        <div class="sk-circle6 sk-child"></div>
                        <div class="sk-circle7 sk-child"></div>
                        <div class="sk-circle8 sk-child"></div>
                        <div class="sk-circle9 sk-child"></div>
                        <div class="sk-circle10 sk-child"></div>
                        <div class="sk-circle11 sk-child"></div>
                        <div class="sk-circle12 sk-child"></div>
                    </div>
                </div>
            </div>


            <div class="row report-results"></div>

        </div>

        <script type="text/template" id="report_results_template">
            <% if (Object.keys(results).length > 0) { %>

            <%  _.each(results, function(projects, region) { %>

            <% if (projects.length > 0) { %>

            <div>
                <div>
                    <div class="navbar-brand text-center w100">Job List - <%- region %></div>
                </div>

                <div style="clear:both; height: 10px;"></div>

                <div>
                    <div class="panel-body">
                        <div style="width:100%;">

                            <% _.each(projects, function(project, key) { %>

                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                {{ strtoupper(trans('bct/projects::projects/common.project')) }}: <u><%-
                                                    project.project_name.substring(0,130) %></u>
                                            </div>
                                            <div class="col-md-6">
                                                {{ strtoupper(trans('bct/projects::general/common.address')) }}: <u><%-
                                                    project.project_full_address %></u>
                                            </div>

                                            <div class="col-md-2 text-right">
                                                {{ strtoupper(trans('bct/projects::projects/model.general.value_id')) }}
                                                : <u><%- project.value %></u>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-9">
                                                {{ strtoupper(trans('bct/projects::projects/common.project_stages')) }}:
                                                <u><%- project.stages %></u>
                                            </div>
                                            <div class="col-md-3 text-right">
                                                {{ strtoupper(trans('bct/projects::projects/common.last_jobsite_visit')) }}
                                                : <u><%- project.last_visit_date %></u>
                                            </div>
                                        </div>

                                        <div style="clear:both; height: 10px;"></div>

                                        <% if (project.general_contractor !== null) {
                                        var general_contractors = project.general_contractor.split('!!');
                                        %>

                                        <div class="row">
                                            <% _.each(general_contractors, function(gc, key) { %>
                                            <div class="col-md-4">
                                                {{ strtoupper(trans('bct/projects::projects/common.general_contractor')) }}
                                                :
                                                <u>
                                                    <%- gc %></u>
                                                </u>
                                            </div>
                                            <% }) %>
                                        </div>

                                        <% } %>

                                        <% if (project.others_contractors !== null) {
                                        var others_contractors = project.others_contractors.split('!!');
                                        %>

                                        <div class="row">
                                            <% _.each(others_contractors, function(contractor, key) {
                                            var scope_contractors = contractor.split(':');
                                            %>
                                            <div class="col-md-4">
                                                <%- scope_contractors[0].toUpperCase() %>: <u><%- scope_contractors[1]
                                                    %></u>
                                            </div>
                                            <% }) %>
                                        </div>

                                        <% } %>

                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <% }) %>

                        </div>
                    </div>
                </div>
            </div>

            <% } %>

            <% }) %>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        Date: {{ Carbon\Carbon::now()->format('M, d Y') }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        Contains jobs being performed by PNWRCC Signatory Contractors or Non-Signatory Contractors with
                        signed Project Agreements. Additional information about jobs listed within this report can be
                        obtained by contacting your Council Representative at 1-877-745-9555 or visiting the jobsite
                        directly. This report is the property of the PNWRCC and is not to be duplicated or distributed
                        without prior written consent from the PNWRCC. No exceptions.
                    </div>
                </div>
            </div>

            <% } %>

        </script>

    </section>

@stop
