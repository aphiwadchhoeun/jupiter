/**
 * Created by Aphiwad on 10/28/2015.
 */
var projectStewardSummary;
var refreshStewardSummary = $('#refresh-stewards-summary');

if (!projectStewardSummary) {
    var options = {
        events: {
            'fetching': function () {
                refreshStewardSummary.addClass('disabled');
                $('#stewards-counter').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            'fetched': function (result) {
                refreshStewardSummary.removeClass('disabled');
                var num = (result.pagination.filtered > 5) ? 5 : result.pagination.filtered;
                $('#stewards-counter').text('(' + num + ' of ' + result.pagination.total + ')');
            }
        }
    };

    projectStewardSummary = $.datagrid(
        'project-stewards-summary',
        '#project-stewards-summary',
        'null',
        'null',
        options);
}

refreshStewardSummary.on('click', function (e) {
    e.preventDefault();
    projectStewardSummary.refresh();
});