var Extension;

;(function (window, document, $, undefined) {

    'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'groups'
    });

    // Initialize functions
    Extension.Index.init = function () {
        Extension.Index.dataGrid();
        Extension.Index.listeners();
    };

    // Add Listeners
    Extension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function () {
        var config = {
            events: {
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
        };

        Extension.Index.Grid = $.datagrid('groups', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);

$("document").ready(function ($) {
    var groups = $('#groups');
    var submit_btn = $('#submit-btn');
    var main_form = $('#content-form');
    var create_modal = $('#modal-form');
    var delete_modal = $('#modal-confirm');

    groups.on('change', function () {
        document.location.href = $(this).val();
    });

    submit_btn.click(function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            main_form.submit();
        });
    });

    create_modal.on('loaded.bs.modal', function () {

        var submit_btn_modal = $('#submit-btn-modal');
        var form_modal = $('#content-form-modal');

        if (form_modal) {
            form_modal.on('submit', function (e) {
                e.preventDefault();
            });

            form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
                if (formInstance.isValid() == true) { // check if form valid or not
                    $.post(
                        form_modal.attr('action'),
                        form_modal.serialize(),
                        function (data) {
                            document.location.href = data;
                        }
                    );
                }
            });

            submit_btn_modal.on('click', function () {
                form_modal.parsley().validate();
            });
        }
    });

    delete_modal.on('loaded.bs.modal', function () {
        var delete_link = $('#delete-button');

        if (delete_link) {

            delete_link.on('click', function (e) {
                e.preventDefault();
                $.ajax({
                    url: delete_link.attr('href'),
                    type: 'DELETE',
                    success: function (data) {
                        document.location.href = data;
                    }
                });
            });
        }
    });
});


jQuery('document').ready(function ($) {


    $('body').on('click', '[data-modal]', function () {
        var $modal = $('#modal-form');

        $modal.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
            if (response) {
                $modal.modal('show');

                var submit_btn_modal = $('#submit-btn-modal');
                var delete_btn_modal = $('#delete-btn-modal');
                var form_modal = $('#content-form-modal');

                if (form_modal) {
                    form_modal.on('submit', function (e) {
                        e.preventDefault();
                    });

                    form_modal.on('keypress', function (e) {

                        var keycode = e.which || e.keyCode;

                        if (keycode == 13) {
                            e.preventDefault();
                        }
                    });

                    if (form_modal.parsley()) {
                        form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
                            if (formInstance.isValid() === true) {
                                $.post(
                                    form_modal.attr('action'),
                                    form_modal.serialize(),
                                    function (data) {
                                        $modal.modal('hide');
                                        window.location.reload();
                                    }
                                );
                            }
                        });

                        submit_btn_modal.on('click', function () {
                            form_modal.parsley().validate();
                        });
                    }

                    if (delete_btn_modal) {
                        delete_btn_modal.on('click', function () {
                            var url = $(this).data('url');
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {},
                                success: function (response) {
                                    $modal.modal('hide');
                                    Extension.Index.Grid.refresh();
                                }
                            });
                        });
                    }
                }
            } else {
                location.reload();
            }

        });
    });
});
