{{ Asset::queue('all-stewards-grid', 'bct/projects::projects/js/stewards/stewards.js', 'extension-base') }}

<section class="panel panel-default panel-grid">
    {{-- Grid --}}

    {{-- Grid: Header --}}
    <header class="panel-heading">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#stewards-actions">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <span class="navbar-brand">{{{ trans('bct/projects::stewards/common.title') }}}</span>

                </div>

                {{-- Grid: Actions --}}
                <div class="collapse navbar-collapse" id="stewards-actions">

                    {{-- Grid: Filters --}}
                    <form class="navbar-form navbar-left" method="post" accept-charset="utf-8" data-search
                          data-grid="stewards" role="form">

                        <div class="input-group">

                        <span class="input-group">
                            <select name="column" class="form-control">
                                <option value="name">{{ trans('bct/projects::users/model.general.name') }}
                                </option>
                                <option value="phone">{{ trans('bct/projects::users/model.general.phone') }}
                                </option>
                                <option value="email">{{ trans('bct/projects::users/model.general.email') }}
                                </option>
                                <option value="local">{{ trans('bct/projects::users/model.general.local') }}
                                </option>
                            </select>
                        </span>

                        <span class="input-group-btn">
                            <input class="form-control" name="filter" type="text"
                                   placeholder="{{{ trans('common.search') }}}">

                            <button class="btn btn-default" type="submit">
                                <span class="fa fa-search"></span>
                            </button>

                            <button class="btn btn-default" data-grid="stewards" data-reset>
                                <i class="fa fa-refresh fa-sm"></i>
                            </button>

                        </span>

                        </div>

                    </form>

                    <div class="nav navbar-nav navbar-right">
                        @if(!$is_repuser)
                            <a href="{{ route('bct.projects.stewards.create') }}"
                               id="stewards-add-btn"
                               name="stewards-add-btn"
                                data-modal-ajax
                               class="btn btn-success pull-right margin-right5" data-toggle="tooltip"
                               data-original-title="{{{ trans('action.create') }}}">
                                <i class="fa fa-plus fa-fw"></i>
                            </a>
                        @endif
                    </div>

                </div>

            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="stewards-data-grid-applied-filters">

            <div id="stewards-data-grid_applied" class="btn-group" data-grid="stewards"></div>

        </div>


        {{-- Grid: Table --}}
        <div class="table-responsive">

            <table id="stewards-data-grid" class="table table-hover"
                   data-source="{{ route('bct.projects.projects.gridAllStewards', $project->id) }}" data-grid="stewards">
                <thead>
                <tr>
                    <th class="sortable"
                        data-sort="name">{{ trans('bct/projects::users/model.general.name') }}</th>
                    <th class="sortable"
                        data-sort="phone">{{ trans('bct/projects::users/model.general.phone') }}</th>
                    <th class="sortable"
                        data-sort="email">{{ trans('bct/projects::users/model.general.email') }}</th>
                    <th class="sortable"
                        data-sort="local">{{ trans('bct/projects::users/model.general.local') }}</th>
                    <th>{{{ trans('bct/projects::general/common.actions') }}}</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>

        </div>

    </div>


    <footer class="panel-footer clearfix">

        {{-- Grid: Pagination --}}
        <div class="stewards-data-grid_pagination" data-grid="stewards"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('bct/projects::projects.tabs.stewards.grid.stewards.results')
    @include('bct/projects::projects.tabs.stewards.grid.stewards.pagination')
    @include('bct/projects::projects.tabs.stewards.grid.stewards.filters')
    @include('bct/projects::projects.tabs.stewards.grid.stewards.no_results')

</section>