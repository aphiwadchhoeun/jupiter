<header class="panel-heading">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#actions">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a class="tip" id="show-details" href="#details"
                           data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{{ $group->name }}} {{{ trans("bct/projects::general/common.details") }}}</span>
            </div>

            <div class="pull-right">
                @if ($group->exists)
                    <a href="{{ route('bct.projects.groups.delete', $group->id) }}"
                       data-method="delete" data-token="{{ csrf_token() }}" data-swal="true"
                       class="btn btn-danger"
                       data-toggle="tooltip" data-original-title="Delete">
                        <i class="fa fa-trash-o fa-fw"></i></a>
                @endif

                <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                        data-original-title="{{{ trans('action.save') }}}">
                    <i class="fa fa-save fa-fw"></i>{{{ trans('action.save') }}}
                </button>
            </div>

        </div>

    </nav>

</header>
<div class="panel-body">

    <section class="panel panel-default panel-tabs">

        <form id="content-form" action="
              @if ($group->exists)
              {{ route('bct.projects.groups.edit', $group->id) }}
              @else
              {{ route('bct.projects.groups.create') }}
              @endif
              " method="post" data-refresh="lists" data-parsley-validate>

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="panel-body">

                <div class="col-md-12">

                    {{-- Form: General --}}
                    <fieldset>
                        <div class="row">
                            <input type="hidden" id="group_id" value="{{{ $group->id }}}">
                            {{-- Form:name --}}
                            <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                                <label for="name" class="control-label">
                                    {{{ trans('bct/projects::groups/model.general.name') }}}
                                </label>

                                <input type="text" class="form-control" name="name" id="name" required
                                       placeholder="{{{ trans('bct/projects::groups/model.general.name_help') }}}"
                                       value="{{{ input()->old('name', $group->name) }}}">

                                <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                            </div>
                            
                            
                             {{-- Form:status --}}
                                <div class="form-group{{ Alert::onForm('status', ' has-error') }}">

                                    <label for="status" class="control-label">
                                        {{{ trans('bct/projects::groups/model.general.status') }}}
                                    </label>

                                    <select class="form-control" name="status" id="status">
                                        <option value="1" @if(input()->old('status', $group->status) == 1) selected @endif>{{{ trans('bct/projects::groups/model.general.active') }}}</option>
                                        <option value="0" @if(input()->old('status', $group->status) == 0) selected @endif>{{{ trans('bct/projects::groups/model.general.inactive') }}}</option>
                                    </select>

                                    <span class="help-block">{{{ Alert::onForm('z_project_status_id') }}}</span>

                                </div>


                            {{-- Form:type --}}
                            <div class="form-group{{ Alert::onForm('individual', ' has-error') }}">

                                <label for="individual" class="control-label">
                                    {{{ trans('bct/projects::groups/model.general.type') }}}
                                </label>

                                <select class="form-control" name="individual" id="individual" >
                                    <option @if($group->individual==0) selected @endif value="0">{{trans('bct/projects::groups/model.general.group')}}</option>
                                    <option @if($group->individual==1) selected @endif value="1">{{trans('bct/projects::groups/model.general.individual')}}</option>
                                </select>

                                <span class="help-block">{{{ Alert::onForm('individual') }}}</span>

                            </div>


                            {{-- Form:description --}}
                            <div class="form-group{{ Alert::onForm('description', ' has-error') }}">

                                <label for="description" class="control-label">
                                    {{{ trans('bct/projects::groups/model.general.description') }}}
                                </label>

                                <textarea class="form-control" name="description" rows="4"
                                          placeholder="{{{ trans('bct/projects::groups/model.general.description_help') }}}"
                                          >{{{ input()->old('description', $group->description) }}}</textarea>

                                <span class="help-block">{{{ Alert::onForm('description') }}}</span>

                            </div>


                        </div>

                    </fieldset>

                </div>

            </div>

        </form>

    </section>

</div>