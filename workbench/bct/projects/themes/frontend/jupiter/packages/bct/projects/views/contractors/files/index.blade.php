@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::contractors/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('fileupload', 'bower_components/blueimp-file-upload/css/jquery.fileupload.css') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}

{{ Asset::queue('jqueryui-widget', 'bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js', 'jquery') }}

{{-- WE USE CUSTOM tmpl.min.js INSTEAD OF BOWER HERE BECAUSE CONFLILCT strict mode THAT CAUSE PROBLEM IN IE AND SAFARI --}}
{{ Asset::queue('tmpl', 'blueimp-tmpl/js/tmpl.min.js') }}

{{ Asset::queue('fileupload', 'bower_components/blueimp-file-upload/js/jquery.fileupload.js', 'jquery') }}
{{ Asset::queue('fileupload-process', 'bower_components/blueimp-file-upload/js/jquery.fileupload-process.js', 'jquery') }}
{{ Asset::queue('fileupload-ui', 'bower_components/blueimp-file-upload/js/jquery.fileupload-ui.js', 'jquery') }}

{{ Asset::queue('contractors-files', 'bct/projects::contractors/js/files/files_new.js', 'jquery') }}
{{-- Queue assets --}}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')
    <section class="panel panel-default panel-grid">
<header class="panel-heading hidden-print">

    <nav class="navbar navbar-default navbar-actions">

        <div class="container-fluid">

            <div class="navbar-header">
                <ul class="nav navbar-nav navbar-cancel">
                    <li>
                        <a href="{{ route('bct.projects.contractors.details',['id'=>$contractor->id]) }}"
                           class="tip go-to-details" data-toggle="tooltip"
                           data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span
                                    class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>
                    </li>
                </ul>

                <span class="navbar-brand">{{ $contractor->name }} {{ trans("bct/projects::projects/common.tabs.files") }}</span>

            </div>

        </div>

    </nav>

</header>

<div class="panel-body">
    <div class="row visible-print">
        <h4 class="overview-heading text-center visible-print">
            {{ data_get($contractor, 'name', '') }} -
            {{ trans("bct/projects::projects/common.title") }} {{ trans("bct/projects::projects/common.tabs.files") }}
        </h4>
    </div>

    <div class="row">

        <div class="col-md-12">
            @include('bct/projects::contractors.files.contractor-file')
        </div>

    </div>

</div>

@include('bct/projects::contractors/files/templates/template_upload')
@include('bct/projects::contractors/files/templates/template_download')
    </section>


@stop
