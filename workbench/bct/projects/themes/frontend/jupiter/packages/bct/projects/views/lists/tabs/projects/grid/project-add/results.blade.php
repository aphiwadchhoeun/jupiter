<script type="text/template" data-grid="project-add" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td>
            <input input="" type="hidden" value="<%- r.id %>">
            <%- r.name %>
        </td>
        <td><%- r.city %></td>
        <td><%- r.zipcode %></td>
        <td><%- r.follow_up_date_custom %></td>
        <td><%- r.tags %></td>
        <td>
            <% if (r.status == 'Active') { %>
            <span class="label label-success"><%- r.status %></span>
            <% } else if (r.status == 'Disabled') {%>
            <span class="label label-danger"><%- r.status %></span>
            <% } else {%>
            <span class="label label-default"><%- r.status %></span>
            <% } %>
        </td>
        <td>
            <button class="btn btn-success btn-circle add-to-list" data-toggle="tooltip" data-placement="right"
                    data-original-title="{{{ trans('bct/projects::lists/common.add_to_list') }}}"><i
                        class="fa fa-plus fa-fw"></i></button>
        </td>
    </tr>

    <% }); %>

</script>
