@extends('bct/projects::default')


{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Queue assets --}}{{-- Queue assets --}}
{{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('validate', 'platform/js/parsley.min.js', 'jquery') }}
{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}

{{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['jquery', 'moment']) }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
{{ Asset::queue('list-details', 'bct/projects::lists/js/details.js', 'jquery') }}
{{ Asset::queue('add-delete', 'bct/projects::lists/js/add-delete.js', 'jquery') }}
{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-grid">
        <div class="panel-body">

            <div class="row hidden-print">

                <div class="col-md-12">

                    <ul class="nav nav-tabs">
                        <li class="dropdown">
                            <a href="#details" data-toggle="tab">
                                {{{ trans("bct/projects::lists/common.tabs.details") }}}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                <li class=""><a href="#details" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">@fat</a></li>
                                <li class=""><a href="#details-edit" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">@mdo</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#projects" data-toggle="tab">
                                {{{ trans("bct/projects::lists/common.tabs.projects") }}}
                            </a>
                        </li>
                        <li>
                            <a href="#reps" data-toggle="tab">
                                {{{ trans("bct/projects::lists/common.tabs.reps") }}}
                            </a>
                        </li>

                    </ul>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    <div class="tab-content">
                         
                        <div role="tabpanel" class="tab-pane fade" id="details">
                            @include('bct/projects::lists.tabs.details.tab')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="details-edit">
                            @include('bct/projects::lists.tabs.details.tab-edit')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="projects">
                            @include('bct/projects::lists.tabs.projects.tab')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="reps">
                            @include('bct/projects::lists.tabs.reps.tab')
                        </div>
                        
                    </div>

                </div>

            </div>
        </div>

    </section>

@stop
