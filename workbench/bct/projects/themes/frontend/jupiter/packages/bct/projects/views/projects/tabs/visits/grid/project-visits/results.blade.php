<script type="text/template" data-grid="project-visit" data-template="results">

    <% _.each(results, function(r) { %>
    <tr data-grid-row>
        <td><%- r.users_names %></td>
        <td>
            <div><b>{{trans('bct/projects::projects/model.general.follow_up')}}:</b> <%- r.reason %></div>
            <div><b>{{ trans('bct/projects::visits/model.general.notes') }}:</b> <%- r.details %></div>
        </td>
        <td><%- r.stages_names %></td>

        @if (config_projects('bct.projects.settings.enable_project_visit_weather'))
            <td><%- r.weather %></td>
        @endif

        @if (config_projects('bct.projects.settings.enable_project_visit_workers'))
            <td class="text-center"><%- r.workers_count %></td>
        @endif

        <td><%- r.created %></td>
        <td>
            <% if (r.approved == true) { %>
            <span class="label label-success">{{ trans('bct/projects::reviews/model.general.approved') }}</span>
            <% } else { %>
            <span class="label label-danger">{{ trans('bct/projects::reviews/model.general.not_approved') }}</span>
            <% } %>
        </td>
        @if (!$currentUser->inRole('pp-guest'))
        <td class="hidden-print">
            <% if (r.view_uri) { %>
                <a href="<%- r.view_uri %>" id="visit_edit" name="visit_edit" data-modal data-toggle="tooltip" class="btn btn-primary btn-circle"
                       data-placement="right" data-original-title="{{{ trans('bct/projects::general/common.edit') }}}">
                        <i class="fa fa-book fa-fw"></i></a>
            <% } %>
        </td>
        @endif
    </tr>

    <% }); %>

</script>
