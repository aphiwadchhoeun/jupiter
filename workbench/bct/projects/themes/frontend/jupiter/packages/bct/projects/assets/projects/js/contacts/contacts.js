/**
 * Created by user on 30/06/15.
 */
var AllContactsExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    AllContactsExtension = Object.create(ExtensionBase);
    AllContactsExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'contacts'
    });

    // Initialize functions
    AllContactsExtension.Index.init = function () {
        AllContactsExtension.Index.dataGrid();
        AllContactsExtension.Index.listeners();
    };

    // Add Listeners
    AllContactsExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', AllContactsExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    AllContactsExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    AllContactsExtension.Index.exporterStatus = function (grid) {
        $('#project-contacts-all-exporter').toggleClass('disabled', grid.pagination.filtered == 0);
        $('#project-contacts-all-exporter').parent().toggleClass('disabled', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    AllContactsExtension.Index.dataGrid = function () {
        var config = {
            //scroll: '#data-grid',
            events: {
                sorted: function (response) {
                    if (response.pagination.total == 0) {
                        $('.download-container').addClass('hide');
                    }
                },
                fetched: function (response) {
                    if (response.pagination.filtered == 0) {
                        $('.download-container').addClass('hide');
                    } else {
                        $('.download-container').removeClass('hide');
                    }
                },
            },
            callback: function () {
            }
        };

        AllContactsExtension.Index.Grid = $.datagrid('contacts', '#contacts-data-grid', '.contacts-data-grid_pagination', '#contacts-data-grid_applied', config);
    };

    // Job done, lets run.
    AllContactsExtension.Index.init();

})(window, document, jQuery);

$('#contacts-add-btn').on('click', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modalAjax = $('#modal-form-ajax');

    $modalAjax.find('.modal-content').load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (response) {
            $modalAjax.modal('show');

            initAjaxModal();

            $('#modal-form-ajax').on('click', '#submit-btn', function (e) {
                e.preventDefault();

                var form_modal = $modalAjax.find('#content-form');

                swal({
                    title: "Are you sure you want to save?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#019964",
                    confirmButtonText: "Yes!",
                    cancelButtonText: "No",
                    showLoaderOnConfirm: true,
                }).then(function () {
                    $.post(
                        form_modal.attr('action'),
                        form_modal.serialize(),
                        function (data) {
                            if (typeof data.success == 'undefined') {
                                $modalAjax.find('.modal-content').html(data);
                                initAjaxModal();
                            } else {
                                $modalAjax.modal('hide');
                                AllContactsExtension.Index.Grid.refresh();
                            }
                        }
                    );
                });
            });
        } else {
            location.reload();
        }
    });

    function initAjaxModal() {
        $modalAjax.find('[data-mask]').mask('(999) 999-9999');
        $modalAjax.find('[data-mask-zip]').mask('99999?-9999');
    }
});

$('#contacts-data-grid').on('click', '[data-modal]', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().on('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                            if (data == 'success') {
                                $modal.modal('hide');
                                ProjectContactExtension.Index.Grid.refresh();
                                contactsSummaryUpdate();
                            } else {
                                swal(data);
                            }
                        })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    } else {
                        submit_btn_modal.removeAttr('disabled');
                    }
                });

                submit_btn_modal.on('click', function () {
                    submit_btn_modal.prop('disabled', true);
                    form_modal.parsley().validate();
                });
            }

        } else {
            location.reload();
        }
    });
});
