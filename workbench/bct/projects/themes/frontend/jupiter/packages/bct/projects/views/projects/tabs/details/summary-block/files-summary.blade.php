<span class="navbar-brand">{{ trans("bct/projects::files/common.title") }} <span id="files-counter" class="text14">( {{ $files->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $files->count() }} )</span></span>
@if (!$currentUser->inRole('pp-guest'))

<a data-id="#files" id="files_tab" name="files_tab" class="btn btn-primary go-to-tab pull-right"
   data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
            class="fa fa-edit fa-fw"></i></a>
@endif
{{--<a href="{{ route('bct.projects.projects.createFile', $project->id) }}"
   data-modal
   class="btn btn-success pull-right margin-right5 files-add-btn" data-toggle="tooltip"
   data-original-title="{{{ trans('action.create') }}}">
    <i class="fa fa-plus fa-fw"></i>
</a>--}}


<table class="table table-striped table-bordered">
    <tbody>
    <tr>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::files/model.general.name') }}</td>
        <td class="td-label col-md-5 text-center">{{ trans('bct/projects::files/model.general.description') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.type') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.date') }}</td>

    </tr>
    @foreach($files->take(5) as $file)
        <tr>
            <td class="td-value"><a href="{{ $file->download_url }}" download="{{ $file->name_full }}">{{ $file->name }}</a></td>
            <td class="td-value text-center">{{ $file->description }}</td>
            <td class="td-value text-center">{{ $file->extension }}</td>
            <td class="td-value text-center">{{ date_correct_details_projects( $file->created_at ) }}</td>
        </tr>
    @endforeach
</table>