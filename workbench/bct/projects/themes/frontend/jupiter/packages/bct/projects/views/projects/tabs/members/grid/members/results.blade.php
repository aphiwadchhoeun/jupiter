<script type="text/template" data-grid="members" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.member_number %></td>
        <td><%- r.member_name %></td>
        <td><%- r.phone %></td>
        <td><%- r.member_since_correct %></td>
        <td><a href="<%- r.view_uri %>" data-modal
               data-toggle="tooltip" class="btn btn-success btn-circle" data-placement="right"
               data-original-title="{{{ trans('bct/projects::projects/common.connect') }}}"><i
                        class="fa fa-plus fa-fw"></i></a></td>
    </tr>

    <% }); %>

</script>
