@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ trans("action.{$mode}") }}} {{ trans('bct/projects::contractors/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
{{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}
{{ Asset::queue('contractor-form', 'bct/projects::contractors/js/form.js', ['jquery','bootstrap-toggle']) }}


{{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default">

        {{-- Form --}}
        <form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post"
              data-refresh="contractors">

            {{-- Form: CSRF Token --}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <header class="panel-heading">

                <nav class="navbar navbar-default navbar-actions">

                    <div class="container-fluid">

                        <div class="navbar-header">
                            <ul class="nav navbar-nav navbar-cancel">
                                <li>
                                    <a class="tip" href="@if($mode=='create')
                                        {{ route('bct.projects.contractors.all') }}
                                    @else
                                        {{ route('bct.projects.contractors.details',$contractor->id ) }}
                                    @endif"
                                       data-toggle="tooltip"
                                       data-original-title="{{{ trans('action.cancel') }}}">
                                        <i class="fa fa-reply"></i> <span
                                                class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                                    </a>
                                </li>
                            </ul>

                            <span class="navbar-brand">{{ trans("bct/projects::contractors/common.title") }} {{ trans("action.{$mode}") }}</span>
                        </div>

                        <div class="pull-right">
                            @if ($contractor->exists)
                            <div class="pull-left form-group" id="status-switch">

                                <label for="status" class="control-label">
                                    {{{ trans('bct/projects::general/common.status') }}}
                                </label>

                                <input type="checkbox" name="is_union" id="is_union"
                                       @if ( $contractor->is_union)
                                       checked
                                        @endif
                                />
                            </div>
                            <div class="pull-left form-group" id="active-switch">

                                <label for="active" class="control-label">
                                    {{{ trans('bct/projects::general/common.active') }}}
                                </label>

                                <input type="checkbox" name="active" id="active"
                                       @if ( $contractor->active)
                                       checked
                                        @endif
                                />
                            </div>
                            @endif

                            <button id="submit-btn" class="btn btn-primary" data-toggle="tooltip"
                                    data-original-title="{{{ trans('action.save') }}}">
                                <i class="fa fa-save fa-fw"></i></button>

                        </div>

                    </div>

                </nav>

            </header>

            @if($mode=='create')
                @include('bct/projects::contractors._form')
            @endif


        </form>

    </section>
    @if($contractor->exists)
        @include('bct/projects::contractors.form_contacts')
    @endif
@stop
