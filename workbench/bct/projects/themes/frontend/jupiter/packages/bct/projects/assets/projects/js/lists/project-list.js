var ProjectListExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectListExtension = Object.create(ExtensionBase);
    ProjectListExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-list'
    });

    // Initialize functions
    ProjectListExtension.Index.init = function () {
        ProjectListExtension.Index.datePicker(ProjectListExtension);
        ProjectListExtension.Index.dataGrid();
        ProjectListExtension.Index.listeners();
        ProjectListExtension.Index.datePicker.on('show.daterangepicker', function () {
            showDirectionPP('Added');
        });
    };

    // Add Listeners
    ProjectListExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', ProjectListExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    ProjectListExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    ProjectListExtension.Index.exporterStatus = function (grid) {
        $('#project-lists-exporter').toggleClass('hide', grid.pagination.filtered == 0);
        $('#project-lists-exporter').parent().toggleClass('hide', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    ProjectListExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            callback: function (data) {
                ProjectListExtension.Index
                    .exporterStatus(data);
            }
        };

        ProjectListExtension.Index.Grid = $.datagrid('project-list', '#project-list-data-grid', '.project-list-data-grid_pagination', '#project-list-data-grid_applied', config);
    };

    // Job done, lets run.
    ProjectListExtension.Index.init();

})(window, document, jQuery);

$(document.body).on('click', '.lists-add-btn', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-plus');

    var $modal = $('#modal-form');

    $modal.load($(this).attr('href'), '', function (response, status, xhr) {
        ajaxBtnStatus(sourceBtn, 'fa-plus');

        if (status == 'error') {
            swal('You are not authorized to access this form.', null, 'error');

            return;
        }
        if (response) {
            $modal.modal('show');

            var submit_btn_modal = $('#submit-btn-modal');
            var form_modal = $('#content-form-modal');

            if (form_modal) {
                form_modal.on('submit', function (e) {
                    e.preventDefault();
                });

                form_modal.parsley().subscribe('form:validated', function (formInstance) {
                    if (formInstance.isValid() == true) { // check if form valid or not
                        submit_btn_modal.prop('disabled', true);
                        $.post(
                            form_modal.attr('action'),
                            form_modal.serialize()
                        ).done(function (data) {
                            if (data == 'success') {
                                $modal.modal('hide');
                                ProjectListExtension.Index.Grid.refresh();
                                listsSummaryUpdate();
                            } else {
                                swal(data);
                            }
                        })
                            .always(function () {
                                submit_btn_modal.removeAttr('disabled');
                            });
                    }
                });

                submit_btn_modal.on('click', function () {
                    form_modal.parsley().validate();
                });
            }

        }
        else {
            location.reload();
        }
    });
});

// Project List Data Grid
$('#project-list-data-grid').on('click', '.delete-confirm', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-trash');

    var $this = $(this);

    swal({
        title: "Are you sure you want to remove?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc002e",
        confirmButtonText: "Yes!",
        cancelButtonText: "No",
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post($this.attr('href'))
                    .success(function (data) {
                        ajaxBtnStatus(sourceBtn, 'fa-trash');

                        if (data == 'success') {
                            resolve();
                        } else {
                            reject('Deleting From List Failed!');
                        }
                    })
            });
        }
    }).then(function () {
        swal('Project removed from the list!', '', 'success');
        ProjectListExtension.Index.Grid.refresh();
        listsSummaryUpdate();

    }, function () {
        ajaxBtnStatus(sourceBtn, 'fa-trash');

    });
});