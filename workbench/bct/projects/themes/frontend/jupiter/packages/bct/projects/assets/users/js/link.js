$("document").ready(function ($) {
    // find mrp user status checkbox
    var statusControl = $('#activated');
    var submit_btn = $('#submit-btn');
    var main_form = $('#content-form');

    // init status switch control
    statusControl.bootstrapToggle({
        onstyle: 'success',
        offstyle: 'danger'
    });

    submit_btn.click(function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
        }).then(function () {
            main_form.submit();
        });
    });

});