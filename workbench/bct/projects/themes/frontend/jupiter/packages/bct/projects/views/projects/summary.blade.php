@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    {{ trans('bct/projects::projects/common.title') }}
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent

    {{ Asset::queue('parsley-validate', 'platform/js/parsley.min.js', 'jquery') }}
    {{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
    {{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
    {{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}
    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js', 'jquery') }}
    {{ Asset::queue('jquery-inputmask', 'bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js', 'jquery') }}
    {{ Asset::queue('numeric', 'bower_components/jquery-numeric/jquery.numeric.js', 'jquery') }}

    {{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['jquery', 'moment']) }}
    {{ Asset::queue('bootstrap-timepicker', 'bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js', ['jquery', 'moment']) }}
    {{ Asset::queue('input-mask', 'bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js', 'jquery') }}
    {{ Asset::queue('select2-jquery', 'bower_components/select2/dist/js/select2.full.min.js', 'jquery') }}

    {{ Asset::queue('selectize', 'bower_components/selectize/dist/js/standalone/selectize.min.js', 'jquery') }}

    {{ Asset::queue('jqueryui-widget', 'bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js', 'jquery') }}

    {{-- WE USE CUSTOM tmpl.min.js INSTEAD OF BOWER HERE BECAUSE CONFLILCT strict mode THAT CAUSE PROBLEM IN IE AND SAFARI --}}
    {{ Asset::queue('tmpl', 'blueimp-tmpl/js/tmpl.min.js') }}

    {{ Asset::queue('load-image', 'bower_components/blueimp-load-image/js/load-image.all.min.js') }}
    {{ Asset::queue('canvas-to-blob', 'bower_components/blueimp-canvas-to-blob/js/canvas-to-blob.min.js') }}
    {{ Asset::queue('iframe-transport', 'bower_components/blueimp-file-upload/js/jquery.iframe-transport.js', 'jquery') }}
    {{ Asset::queue('fileupload', 'bower_components/blueimp-file-upload/js/jquery.fileupload.js', 'jquery') }}
    {{ Asset::queue('fileupload-process', 'bower_components/blueimp-file-upload/js/jquery.fileupload-process.js', 'jquery') }}
    {{ Asset::queue('fileupload-image', 'bower_components/blueimp-file-upload/js/jquery.fileupload-image.js', 'jquery') }}
    {{ Asset::queue('fileupload-audio', 'bower_components/blueimp-file-upload/js/jquery.fileupload-audio.js', 'jquery') }}
    {{ Asset::queue('fileupload-video', 'bower_components/blueimp-file-upload/js/jquery.fileupload-video.js', 'jquery') }}
    {{ Asset::queue('fileupload-validate', 'bower_components/blueimp-file-upload/js/jquery.fileupload-validate.js', 'jquery') }}
    {{ Asset::queue('fileupload-ui', 'bower_components/blueimp-file-upload/js/jquery.fileupload-ui.js', 'jquery') }}

    {{ Asset::queue('project-details-helpers', 'bct/projects::projects/js/helpers.js', 'jquery') }}
    {{ Asset::queue('project-details', 'bct/projects::projects/js/details.js', 'project-details-helpers') }}
    {{ Asset::queue('project-files', 'bct/projects::projects/js/files/files_new.js', 'jquery') }}
@stop

{{-- Inline styles --}}
@section('styles')
    @parent

    {{ Asset::queue('bootstrap-toggle', 'bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Asset::queue('bootstrap-datepicker', 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}
    {{ Asset::queue('bootstrap-timepicker', 'bower_components/bootstrap-timepicker/css/timepicker.less') }}
    {{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}
    {{ Asset::queue('select2', 'bower_components/select2/dist/css/select2.min.css') }}

    {{ Asset::queue('selectize-css', 'bower_components/selectize/dist/css/selectize.css') }}
    {{ Asset::queue('selectize-bootstrap3', 'bower_components/selectize/dist/css/selectize.bootstrap3.css', 'bootstrap') }}

    {{ Asset::queue('fileupload', 'bower_components/blueimp-file-upload/css/jquery.fileupload.css') }}
    {{ Asset::queue('fileupload-ui', 'bower_components/blueimp-file-upload/css/jquery.fileupload-ui.css') }}

    {{ Asset::queue('summary', 'bct/projects::projects/css/details/summary.css') }}
@stop

{{-- Page --}}
@section('page-extension')

    <section class="panel panel-default panel-grid">

        <div class="panel-body">

            @include('bct/projects::projects.sub-nav.sub-nav')

            <div class="tab-content">

                <div class="tab-pane" id="details">
                    @include('bct/projects::projects.tabs.details.tab')
                </div>
                <div class="tab-pane" id="contractors">
                    @include('bct/projects::projects.tabs.contractors.tab')
                </div>
                <div class="tab-pane" id="contacts">
                    @include('bct/projects::projects.tabs.contacts.tab')
                </div>
                <div class="tab-pane" id="members">
                    @include('bct/projects::projects.tabs.members.tab')
                </div>
                @if (!$currentUser->inRole('pp-guest'))
                <div class="tab-pane" id="visits">
                    @include('bct/projects::projects.tabs.visits.tab')
                </div>
                @endif
                <div class="tab-pane" id="actions-history">
                    @include('bct/projects::projects.tabs.actions.tab')
                </div>
                <div class="tab-pane" id="files">
                    @include('bct/projects::projects.tabs.files.tab')
                </div>
                @if (!$currentUser->inRole('pp-guest'))
                <div class="tab-pane" id="lists">
                    @include('bct/projects::projects.tabs.lists.tab')
                </div>
                @endif

            </div>

        </div>

    </section>

@stop
