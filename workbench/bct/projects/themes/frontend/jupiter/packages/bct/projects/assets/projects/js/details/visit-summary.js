/**
 * Created by Aphiwad on 10/28/2015.
 */
var projectVisitSummary;

if (!projectVisitSummary) {
    var options = {
        events: {
            'fetching': function () {
                $('#visits-counter').html('<i class="fa fa-refresh fa-spin"></i>');
            },
            'fetched': function (result) {
                var num = (result.pagination.filtered > 5) ? 5 : result.pagination.filtered;
                $('#visits-counter').text('(' + num + ' of ' + result.pagination.total + ')');
            }
        }
    };

    projectVisitSummary = $.datagrid(
        'project-visits-summary',
        '#project-visits-summary',
        'null',
        'null',
        options);
}