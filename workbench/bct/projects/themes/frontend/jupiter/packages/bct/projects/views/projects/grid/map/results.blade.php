<script type="text/template" data-grid="projects" data-template="results">

	<% _.each(results, function(r) { %>

        <tr data-grid-row>
            @if (!$currentUser->inRole('pp-guest'))
            <td><input content="id" data-checkbox-id="<%- r.id %>" data-grid-checkbox="" name="entries[]" type="checkbox" value="<%- r.id %>"></td>
            @endif
            <td><%- r.id %></td>
            <td class="project-name-column"><%- r.name %></td>
            <td><%- r.city %></td>
            <td><%- r.region_abrv %></td>
            <td class="follow-date-column"><%- r.follow_up_date_custom %></td>
            <td><%= r.tags %></td>
            <td>
                <% if (r.status) { %>
                    <% if (r.status == 'Active') { %>
                        <span class="label label-success"><%- r.status %></span>
                    <% } else if (r.status_id == 'Disabled') {%>
                        <span class="label label-danger"><%- r.status %></span>
                    <% } else {%>
                        <span class="label label-default"><%- r.status %></span>
                    <% } %>
                <% } %>
            </td>
            <td><a target="_blank" href="<%- r.view_uri %>#details" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
                            class="fa fa-book fa-fw"></i></a></td>
		</tr>

	<% }); %>

</script>
