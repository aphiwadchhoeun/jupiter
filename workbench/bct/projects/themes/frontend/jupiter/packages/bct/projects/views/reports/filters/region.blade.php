<div class="input-group select-width">
    <span class="input-group-btn">
        <button class="btn btn-default" type="button" disabled>
            {{ trans('bct/projects::reports/common.placeholder.region') }}
        </button>
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul name="column" class="dropdown-menu" role="menu" data-filter-reset>
            @foreach($regions as $region)
                <li><a href="#" data-filter="region:{{$region}}" value="{{$region}}">{{$region}}</a></li>
            @endforeach
        </ul>
     </span>
</div>