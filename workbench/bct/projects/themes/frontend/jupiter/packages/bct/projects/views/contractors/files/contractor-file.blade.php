<div >

    <form id="fileupload" @if($isRepUser)class="repuser-file"@endif action="{{ route('bct.projects.contractors.files.upload', $contractor->id) }}" data-action-get="{{ route('bct.projects.contractors.files.get', $contractor->id) }}" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7 col-xs-12 pull-right text-right">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="icon-plus icon-white"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                </span>
                
                <span>
                    <button
                            class="btn btn-primary btn-files-update">
                        <i class="fa fa-edit fa-fw"></i> {{ trans('action.save') }}
                    </button>
                </span>
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 col-xs-12 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped">
            <thead>
            <tr>
                <th class="text-center">File URL</th>
                <th class="text-center">File Name</th>
                <th class="text-center">Description</th>
                <th class="text-center">Size</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody class="files"></tbody>
        </table>
    </form>

</div>