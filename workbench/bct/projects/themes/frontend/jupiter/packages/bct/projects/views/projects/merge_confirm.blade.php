@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
	@parent
	{{ trans('bct/projects::projects/common.title') }}
@stop
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'bower_components/moment/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/data-grid::js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'bower_components/underscore/underscore.js', 'jquery') }}
{{ Asset::queue('bootstrap-daterange', 'bower_components/bootstrap-daterangepicker/daterangepicker.js', 'jquery') }}

{{ Asset::queue('index', 'bct/projects::projects/js/merge.js', 'extension-base') }}

{{-- Inline scripts --}}
@section('scripts')
	@parent
@stop

{{-- Inline styles --}}
@section('styles')
	@parent
@stop

{{-- Page --}}
@section('page-extension')

	<section id="information" class="panel panel-default panel-tabs">

		<header class="panel-heading hidden-print">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
						        data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<ul class="nav navbar-nav navbar-cancel">
							<li>
								<a class="tip" href="{{ route('bct.projects.projects.all') }}"
								   data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
									<i class="fa fa-reply"></i> <span
										class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
								</a>
							</li>
						</ul>

						<span class="navbar-brand">{{ $stay_project->name }}: {{ trans("bct/projects::general/common.merge_confirmation") }}</span>
					</div>

					<div class="pull-right hidden-print">

						<a href="{{route('bct.projects.projects.all')}}"
						   class="btn btn-danger">
							{{{ trans('bct/projects::general/common.cancel') }}}
						</a>

						<a href="{{route('bct.projects.projects.merge')}}"
						   class="btn btn-primary"
						   id="merge"
						   name="merge"
						   data-redirect="{{route('bct.projects.projects.all')}}"
						   data-stay="{{$stay_project->id}}"
						   data-merge="{{$merge_project->id}}">
							{{{ trans('bct/projects::general/common.confirm') }}}
						</a>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">
			<div>
				<div class="navbar-brand col-md-6 center"><b>{{ trans('bct/projects::projects/common.merged_project') }}</b></div>
				<div class="navbar-brand col-md-6 center"><b>{{ trans('bct/projects::contractors/common.merge_result') }}</b></div>
			</div>
			<div class="project-info">
				<div class="col-md-6">
					<div class="col-md-12 row">

						<table class="table table-bordered">
							<tbody>
							<tr>
								<td class="td-value">
									<div>
										<div class="col-md-6 border-right col-panel">

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.id') }}:</b>
													<span class="pre-spaces">
                                                        {{ $merge_project->id }}
                                                    </span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.name') }}:</b>
													<span class="pre-spaces">
                                                        {{ $merge_project->name }}
														@if($merge_project->status) -
														{{ $merge_project->status->name }}
														@endif
                                                    </span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.address') }}:</b>
													<span class="pre-spaces">{{ $merge_project->full_address }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.percent_union') }}:</b>
													<span class="pre-spaces"><span id="union-value">{{ number_format($merge_project->union_value * 100, 2) }}%</span></span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}:</b>
													<span class="pre-spaces">{{ date_correct_details_projects($merge_project->follow_up_date) }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.description') }}:</b>
													<span class="pre-spaces">{{ $merge_project->description }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::tags/common.tags') }}:</b>
													<span class="pre-spaces">{{ implode(',', $merge_project->tags->pluck('name')->all()) }}</span>
												</div>
											</div>


										</div>
										<div class="col-md-6 col-panel">

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.classification_id') }}:</b>
													<span class="pre-spaces">{{ ($merge_project->classification) ? $merge_project->classification->name : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.wage') }}:</b>
													<span class="pre-spaces">{{ ($merge_project->prevailingWage) ? $merge_project->prevailingWage->name : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.region') }}:</b>
													<span class="pre-spaces">{{ $merge_project->region_abrv }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.visibility_id') }}:</b>
													<span class="pre-spaces">
                                                        @if ($merge_project->visibility)
															{{ $merge_project->visibility->name }}
														@endif
                                                    </span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.type_id') }}:</b>
													<span class="pre-spaces">{{ ($merge_project->type) ? $merge_project->type->type : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.value_id') }}:</b>
													<span class="pre-spaces">{{ ($merge_project->value) ? $merge_project->value->value : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.reference_details') }}:</b>
													<span class="pre-spaces">{{ $merge_project->reference_details }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::tags/common.tag_groups') }}:</b>
													<span class="pre-spaces">
                                                        {{ $merge_project->tags()->groupby('group')->get(['group'])->implode('group', ', ') }}
                                                    </span>
												</div>
											</div>

										</div>
									</div>
								</td>
							</tr>
							</tbody>
						</table>

					</div>
				</div>
				<div class="col-md-6 row">
					<div class="col-md-12">

						<table class="table table-bordered">
							<tbody>
							<tr>
								<td class="td-value">
									<div>
										<div class="col-md-6 border-right col-panel">

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.id') }}:</b>
													<span class="pre-spaces">
                                                        {{ $stay_project->id }}
                                                    </span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.name') }}:</b>
													<span class="pre-spaces">
                                                        {{ $stay_project->name }}
														@if($stay_project->status) -
														{{ $stay_project->status->name }}
														@endif
                                                    </span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.address') }}:</b>
													<span class="pre-spaces">{{ $stay_project->full_address }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.percent_union') }}:</b>
													<span class="pre-spaces"><span id="union-value">{{ number_format($stay_project->union_value * 100, 2) }}%</span></span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.follow_up_date_grid') }}:</b>
													<span class="pre-spaces">{{ date_correct_details_projects($stay_project->follow_up_date) }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.description') }}:</b>
													<span class="pre-spaces">{{ $stay_project->description }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::tags/common.tags') }}:</b>
													<span class="pre-spaces">{{ implode(',', $stay_project->tags->pluck('name')->all()) }}</span>
												</div>
											</div>


										</div>
										<div class="col-md-6 col-panel">

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.classification_id') }}:</b>
													<span class="pre-spaces">{{ ($stay_project->classification) ? $stay_project->classification->name : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.wage') }}:</b>
													<span class="pre-spaces">{{ ($stay_project->prevailingWage) ? $stay_project->prevailingWage->name : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.region') }}:</b>
													<span class="pre-spaces">{{ $stay_project->region_abrv }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.visibility_id') }}:</b>
													<span class="pre-spaces">
                                                        @if ($stay_project->visibility)
															{{ $stay_project->visibility->name }}
														@endif
                                                    </span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.type_id') }}:</b>
													<span class="pre-spaces">{{ ($stay_project->type) ? $stay_project->type->type : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.value_id') }}:</b>
													<span class="pre-spaces">{{ ($stay_project->value) ? $stay_project->value->value : '' }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::projects/model.general.reference_details') }}:</b>
													<span class="pre-spaces">{{ $stay_project->reference_details }}</span>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<b>{{ trans('bct/projects::tags/common.tag_groups') }}:</b>
													<span class="pre-spaces">
                                                        {{ $stay_project->tags()->groupby('group')->get(['group'])->implode('group', ', ') }}
                                                    </span>
												</div>
											</div>

										</div>
									</div>
								</td>
							</tr>
							</tbody>
						</table>


					</div>
				</div>
			</div>
			<div class="page-break">
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::visits/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::visits/model.general.reps') }}</th>
									<th class="td-label col-md-5 text-center">{{trans('bct/projects::projects/model.general.follow_up_reason')}} & {{ trans('bct/projects::visits/model.general.notes') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.stage') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.date') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($merge_visits as $visit)
									<tr>
										<td class="td-value">{{ $visit->reps_names }}</td>
										<td class="td-value">
											<div><strong>{{ trans('bct/projects::projects/model.general.follow_up') }}:</strong> {{ $visit->reason }}</div>
											<div><strong>{{ trans('bct/projects::visits/model.general.notes') }}:</strong> {{ $visit->details }}</div>
										</td>
										<td class="td-value text-center">{{ $visit->stages_names }}</td>
										<td class="td-value text-center">{{ date_correct_details_projects( $visit->created_at ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::visits/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::visits/model.general.reps') }}</th>
									<th class="td-label col-md-5 text-center">{{trans('bct/projects::projects/model.general.follow_up_reason')}} & {{ trans('bct/projects::visits/model.general.notes') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.stage') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::visits/model.general.date') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($stay_visits as $visit)
									<tr>
										<td class="td-value">{{ $visit->reps_names }}</td>
										<td class="td-value">
											<div><strong>{{ trans('bct/projects::projects/model.general.follow_up') }}:</strong> {{ $visit->reason }}</div>
											<div><strong>{{ trans('bct/projects::visits/model.general.notes') }}:</strong> {{ $visit->details }}</div>
										</td>
										<td class="td-value text-center">{{ $visit->stages_names }}</td>
										<td class="td-value text-center">{{ date_correct_details_projects( $visit->created_at ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
			<div class="page-break">
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::contractors/common.title') }} </b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::projects/model.general.scope_id') }}</th>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::contractors/model.general.contractor') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.union') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.pa') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($merge_contractors as $contractor)
									<tr>
										<td class="td-value">@if(is_object($contractor->pivot->scope)){{ $contractor->pivot->scope->name }} ({{ $contractor->pivot->scope->value }})@endif</td>
										<td class="td-value text-center">{{ $contractor->name }}</td>
										<td class="td-value text-center">
											{{ $contractor->is_union_text }}
										</td>
										<td class="td-value text-center">
											@if ($contractor->pivot->is_agreement)
												Yes
											@else
												No
											@endif
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::contractors/common.title') }}</b></div>

						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::projects/model.general.scope_id') }}</th>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::contractors/model.general.contractor') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.union') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.pa') }}</th>
								</tr>
								</thead>
								<tbody>

								@foreach($stay_contractors as $contractor)
									<tr>
										<td class="td-value">@if(is_object($contractor->pivot->scope)){{ $contractor->pivot->scope->name }} ({{ $contractor->pivot->scope->value }})@endif</td>
										<td class="td-value text-center">{{ $contractor->name }}</td>
										<td class="td-value text-center">
											{{ $contractor->is_union_text }}
										</td>
										<td class="td-value text-center">
											@if ($contractor->pivot->is_agreement)
												Yes
											@else
												No
											@endif
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
			<div class="page-break">
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::members/common.title') }} </b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.id') }}</th>
									<th class="td-label col-md-4 text-center">{{ trans('bct/projects::members/model.general.name') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.scope') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.number_hours') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.wage') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($merge_members as $member)
									<tr>
										<td class="td-value text-center">{{ $member->member_number }}</td>
										<td class="td-value text-center">{{ $member->full_name }}</td>
										<td class="td-value text-center">@if(is_object($member->pivot->scope)){{ $member->pivot->scope->name }} ({{ $member->pivot->scope->value }})@endif</td>
										<td class="td-value text-center">{{ $member->pivot->number_of_hours }}</td>
										<td class="td-value text-center">{{ show_money_projects($member->pivot->wage) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::members/common.title') }} </b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.id') }}</th>
									<th class="td-label col-md-4 text-center">{{ trans('bct/projects::members/model.general.name') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.scope') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.number_hours') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::members/model.general.wage') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($stay_members as $member)
									<tr>
										<td class="td-value text-center">{{ $member->member_number }}</td>
										<td class="td-value text-center">{{ $member->full_name }}</td>
										<td class="td-value text-center">@if(is_object($member->pivot->scope)){{ $member->pivot->scope->name }} ({{ $member->pivot->scope->value }})@endif</td>
										<td class="td-value text-center">{{ $member->pivot->number_of_hours }}</td>
										<td class="td-value text-center">{{ show_money_projects($member->pivot->wage) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>

			<div class="page-break">
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::contacts/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::contacts/model.general.name') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::projects/model.general.type') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::contacts/model.general.phone') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($merge_contacts as $contact)
									<tr>
										<td class="td-value text-center">{{ $contact->full_name }}</td>
										<td class="td-value text-center">@if(is_object($contact->pivot->contact_type)){{ $contact->pivot->contact_type->name }}@endif</td>
										<td class="td-value text-center">{{ $contact->phone }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::contacts/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::contacts/model.general.name') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::projects/model.general.type') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::contacts/model.general.phone') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($stay_contacts as $contact)
									<tr>
										<td class="td-value text-center">{{ $contact->full_name }}</td>
										<td class="td-value text-center">@if(is_object($contact->pivot->contact_type)){{ $contact->pivot->contact_type->name }}@endif</td>
										<td class="td-value text-center">{{ $contact->phone }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
			<div class="page-break">
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::actions/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::actions/model.general.contractor_id') }}</th>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::actions/model.general.details') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.action_type') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.date') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($actions as $action)
									<tr>
										<td class="td-value">{{ data_get($action, 'contractor_name') }}</td>
										<td class="td-value text-center">{{ $action->details }}</td>
										<td class="td-value text-center">{{ $action->action_type }}</td>
										<td class="td-value text-center">{{ date_correct_details_projects( $action->created_at ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::actions/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::actions/model.general.contractor_id') }}</th>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::actions/model.general.details') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.action_type') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::actions/model.general.date') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($actions as $action)
									<tr>
										<td class="td-value">{{ data_get($action, 'contractor_name') }}</td>
										<td class="td-value text-center">{{ $action->details }}</td>
										<td class="td-value text-center">{{ $action->action_type }}</td>
										<td class="td-value text-center">{{ date_correct_details_projects( $action->created_at ) }}</td>
									</tr>
								@endforeach
								@foreach($added_actions as $action)
									<tr>
										<td class="td-value">{{ data_get($action, 'contractor_name') }}</td>
										<td class="td-value text-center">{{ $action->details }}</td>
										<td class="td-value text-center">{{ $action->action_type }}</td>
										<td class="td-value text-center">{{ date_correct_details_projects( $action->created_at ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
			<div class="page-break">
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::files/common.title') }}</b></div>

						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::files/model.general.name') }}</th>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::files/model.general.description') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.type') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.date') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($merge_files as $file)
									<tr>
										<td class="td-value">{{ $file->name }}</td>
										<td class="td-value text-center">{{ $file->description }}</td>
										<td class="td-value text-center">{{ $file->extension }}</td>
										<td class="td-value text-center">{{ show_date( $file->created_at ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::files/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::files/model.general.name') }}</th>
									<th class="td-label col-md-5 text-center">{{ trans('bct/projects::files/model.general.description') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.type') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::files/model.general.date') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($stay_files as $file)
									<tr>
										<td class="td-value">{{ $file->name }}</td>
										<td class="td-value text-center">{{ $file->description }}</td>
										<td class="td-value text-center">{{ $file->extension }}</td>
										<td class="td-value text-center">{{ show_date( $file->created_at ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="page-break">
				<div class="col-md-6">
					<div class="row">

						<div class="navbar-brand"><b>{{ trans('bct/projects::lists/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.name') }}</th>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.category') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.group') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.status') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.date_added') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($lists as $list)
									<tr>
										<td class="td-value">{{ $list->name }}</td>
										<td class="td-value text-center">{{ $list->category }}</td>
										<td class="td-value text-center"> {{ $list->group_name }}</td>
										<td class="td-value text-center"><span class="label @if($list->is_active==1) label-success @else label-danger @endif">@if($list->is_active==1) Active @else Inactive @endif</span></td>
										<td class="td-value text-center">{{ show_date( $list->due_date ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="navbar-brand"><b>{{ trans('bct/projects::lists/common.title') }}</b></div>
						<div class="col-md-12">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.name') }}</th>
									<th class="td-label col-md-3 text-center">{{ trans('bct/projects::lists/model.general.category') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.group') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.status') }}</th>
									<th class="td-label col-md-2 text-center">{{ trans('bct/projects::lists/model.general.date_added') }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($lists as $list)
									<tr>
										<td class="td-value">{{ $list->name }}</td>
										<td class="td-value text-center">{{ $list->category }}</td>
										<td class="td-value text-center"> {{ $list->group_name }}</td>
										<td class="td-value text-center"><span class="label @if($list->is_active==1) label-success @else label-danger @endif">@if($list->is_active==1) Active @else Inactive @endif</span></td>
										<td class="td-value text-center">{{ show_date( $list->due_date ) }}</td>
									</tr>
								@endforeach
								@foreach($added_lists as $list)
									<tr>
										<td class="td-value">{{ $list->name }}</td>
										<td class="td-value text-center">{{ $list->category }}</td>
										<td class="td-value text-center"> {{ $list->group_name }}</td>
										<td class="td-value text-center"><span class="label @if($list->is_active==1) label-success @else label-danger @endif">@if($list->is_active==1) Active @else Inactive @endif</span></td>
										<td class="td-value text-center">{{ show_date( $list->due_date ) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			@if(count($jobs)>0 || count($added_jobs)>0)
				<div class="page-break">
					<div class="col-md-6">
						<div class="row">
							<div class="navbar-brand"><b>{{ trans("bct/projects::general/common.jobs") }}</b></div>
							<div class="col-md-12">
								<table class="table table-bordered">
									<tbody>
									<tr>
										<th class="td-label col-md-3 text-center">{{ trans('bct/projects::general/form.id') }}</th>
										<th class="td-label col-md-5 text-center">{{ trans('bct/projects::general/form.name') }}</th>
									</tr>
									@foreach($jobs as $job)
										<tr>
											<td class="td-value">{{ $job->id }}</td>
											<td class="td-value text-center">{{ $job->name }}</td>
										</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="navbar-brand"><b>{{ trans("bct/projects::general/common.jobs") }}</b></div>
							<div class="col-md-12">
								<table class="table table-bordered">
									<tbody>
									<tr>
										<th class="td-label col-md-3 text-center">{{ trans('bct/projects::general/form.id') }}</th>
										<th class="td-label col-md-5 text-center">{{ trans('bct/projects::general/form.name') }}</th>
									</tr>
									@foreach($jobs as $job)
										<tr>
											<td class="td-value">{{ $job->id }}</td>
											<td class="td-value text-center">{{ $job->name }}</td>
										</tr>
									@endforeach
									@foreach($added_jobs as $added_job)
										<tr>
											<td class="td-value">{{ $added_job->id }}</td>
											<td class="td-value text-center">{{ $added_job->name }}</td>
										</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
			@endif

		</div>

	</section>
@stop
