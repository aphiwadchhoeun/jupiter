<script type="text/template" data-grid="project-summary" data-template="no_results">

    <tr>
        <td class="no-results" colspan="3">{{{ trans('common.no_results') }}}</td>
    </tr>

</script>
