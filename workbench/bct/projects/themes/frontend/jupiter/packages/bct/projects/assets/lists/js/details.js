var $modal = $('#modal-form');
var submit_btn = $('#submit-btn');

jQuery('document').ready(function ($) {
    var dueDate = $('#due_date');
    /** datepicker */
    dueDate.parents('.date').datepicker({
        format: 'M-dd-yyyy',
        assumeNearbyYear: true,
        showOnFocus: false
    }).on('changeDate', function() {
        if (dueDate.val()) {
            submit_btn.prop('disabled',false);
            $(this).parent().removeClass('has-error');
            $(this).parent().find('.help-block').text('');
        }
    }).on('focusout', function () {
        var date = dueDate.val();
        if (date !== '') {
            var valid_date = moment(date, 'MMM-DD-YYYY', true).isValid();
            if(valid_date===false){
                submit_btn.prop('disabled',true);
                $(this).parent().addClass('has-error');
                $(this).parent().find('.help-block').text('Please use format like Dec-01-2001.')
            }
            else{
                submit_btn.prop('disabled',false);
                $(this).parent().removeClass('has-error');
                $(this).parent().find('.help-block').text('');
            }
        }
    });

    datePickerProjectDel();
    datePickerProjectAdd();


    /** TABS */
    var hash = window.location.hash;

    $('.nav-tabs a').click(function (e) {
        $(this).tab('show');
        window.location.hash = this.hash;
        var scrollmem = $('body').scrollTop();
        $('html,body').scrollTop(scrollmem);

    });

    if (!hash) {
        $('.nav-tabs a[href="#details"]').click();
    }

    var config = {
        preventHashChange: true,
        hash: false
    };

    var gridProjectSummary, gridProjectAdd, gridProjectDel, gridRepsSummary, gridRepsInfo;


    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        var $id = $(e.target).attr('href');
        var $tabContent = $($id);
        if ($id == '#details') {

            if (!gridProjectSummary) {
                gridProjectSummary = $.datagrid(
                    'project-summary',
                    '#data-grid-project-summary',
                    '.data-grid_pagination',
                    '#data-grid_applied',
                    $.extend({}, config, {
                        events: {
                            fetched: function () {
                                if (!gridRepsSummary) {
                                    gridRepsSummary = $.datagrid('reps-summary', '#data-grid-reps-summary', '.data-grid_pagination', '#data-grid_applied', config);
                                }
                                else {
                                    gridRepsSummary.refresh();
                                }
                            }
                        }
                    }));
            }
            else {
                gridProjectSummary.refresh();
                if (!window.location.origin) {
                    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
                }
                var url = window.location.origin + window.location.pathname + "/refresh";
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {},
                    success: function (response) {
                        $('#projects-counter').text(response);
                    }
                });

            }
        } else if ($id == '#reps') {
            if (!gridRepsInfo) {
                gridRepsInfo = $.datagrid('reps-info', '#data-grid-reps-info', '.data-grid-reps-info_pagination', '#data-grid-reps-info_applied', config);
            }

        }
    });

    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.go-to-details').on('click', function (e) {
        e.preventDefault();
        $('.nav-tabs a[href="#details"]').click();
    });

    $('.go-to-tab').on('click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');

        $('.nav-tabs a[href="' + id + '"]').click();
    });

    $('#show-details-edit').on('click', function () {
        $('.nav-tabs a#dropdown2-tab').click();
    });

    $('body').on('click', '.redirect-to', function () {
        var url = $(this).data('url');
        if (url) {
            window.location.href = url;
        }
    });

    function setGetParameter(grid, paramName, paramValue) {
        var url = grid.source;
        if (url.indexOf(paramName + "=") >= 0) {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
        }
        else {
            if (url.indexOf("?") < 0)
                url += "?" + paramName + "=" + paramValue;
            else
                url += "&" + paramName + "=" + paramValue;
        }

        grid.source = url;
    };

    function removeParam(grid, key) {
        var sourceURL = grid.source,
            rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        grid.source = rtn;
    };

    function refreshGrid(refreshItem) {
        if (refreshItem == 'contractors') {
            if (gridProjectContractors) gridProjectContractors.refresh();
            if (gridProjectContractorsSummary) gridProjectContractorsSummary.refresh();
            if (gridContractors) gridContractors.refresh();
        } else if (refreshItem == 'contacts') {
            if (gridProjectContacts) gridProjectContacts.refresh();
            if (gridProjectContactsSummary) gridProjectContactsSummary.refresh();
            if (gridContacts) gridContacts.refresh();
        } else if (refreshItem == 'visits') {
            if (gridProjectVisits) gridProjectVisits.refresh();
            if (gridProjectVisitsSummary) gridProjectVisitsSummary.refresh();
            if (gridProjectLists) gridProjectLists.refresh();
            if (gridProjectListsSummary) gridProjectListsSummary.refresh();
        } else if (refreshItem == 'actions') {
            if (gridProjectActions) gridProjectActions.refresh();
            if (gridProjectActionsSummary) gridProjectActionsSummary.refresh();
        } else if (refreshItem == 'lists') {
            if (gridProjectLists) gridProjectLists.refresh();
            if (gridProjectListsSummary) gridProjectListsSummary.refresh();
        } else if (refreshItem == 'files') {
            Extension.Index.Grid.refresh();
            if (gridProjectFilesSummary) gridProjectFilesSummary.refresh();
        } else if (refreshItem == 'stewards') {
            if (gridProjectStewards) gridProjectStewards.refresh();
            if (gridProjectStewardsSummary) gridProjectStewardsSummary.refresh();
            if (gridStewards) gridStewards.refresh();
        }
    }

    $('body').on('click', '[data-modal]', function () {

        $modal.load($(this).attr('href'), '', function (response, status, xhr) {
            if (response) {
                $modal.modal('show');

                var submit_btn_modal = $('#submit-btn-modal');
                var delete_btn_modal = $('#delete-btn-modal');
                var form_modal = $('#content-form-modal');

                if (form_modal) {
                    var refreshItem = form_modal.data('refresh');

                    /** init select2 */
                    var input = $("#contact_type");
                    var stages = $("#stages");
                    var users = $("#users");
                    var lists = $("#project_lists");


                    if (input) {
                        input.select2({
                            createSearchChoice: function (term, data) {
                                if ($(data).filter(function () {
                                        return this.text.localeCompare(term) === 0;
                                    }).length === 0) {
                                    return {
                                        id: term,
                                        text: term
                                    };
                                }
                            },
                            multiple: false,
                            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                                url: input.data('href'),
                                dataType: 'json',
                                quietMillis: 250,
                                data: function (term, page) {
                                    return {
                                        q: term // search term
                                    };
                                },
                                results: function (data, page) { // parse the results into the format expected by Select2.
                                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                                    var arr = [];
                                    _.each(data, function (item) {
                                        arr.push({
                                            id: item.name,
                                            text: item.name
                                        });
                                    });
                                    return {results: arr};
                                },
                                cache: true
                            },
                            initSelection: function (element, callback) {
                                // the input tag has a value attribute preloaded that points to a preselected repository's id
                                // this function resolves that id attribute to an object that select2 can render
                                // using its formatResult renderer - that way the repository name is shown preselected
                                var id = $(element).val();
                                if (id !== "") {
                                    $.ajax(input.data('href') + '?id=' + id, {
                                        dataType: "json"
                                    }).done(function (data) {
                                        callback({
                                            id: data.name,
                                            text: data.name
                                        });
                                    });
                                }
                            }
                        });
                    }

                    if (stages) stages.select2();
                    if (users) users.select2();
                    if (lists) lists.select2();

                    /** init datetimepicker */
                    var inputDate = form_modal.find("#follow_up_date");

                    if (inputDate) {
                        inputDate.parents('.date').datepicker({
                            format: 'yyyy-mm-dd',
                            assumeNearbyYear: true,
                            showOnFocus: false
                        });
                    }

                    form_modal.on('submit', function (e) {
                        e.preventDefault();
                    });

                    form_modal.on('keypress', function (e) {

                        var keycode = e.which || e.keyCode;

                        if (keycode == 13) {
                            e.preventDefault();
                        }
                    });

                    if (form_modal.parsley()) {
                        form_modal.parsley().subscribe('parsley:form:validate', function (formInstance) {
                            if (formInstance.isValid() === true) {
                                formInstance.asyncIsValid().done(function () {
                                    $.post(
                                        form_modal.attr('action'),
                                        form_modal.serialize(),
                                        function (data) {
                                            $modal.modal('hide');
                                            refreshGrid(refreshItem);

                                            if (typeof data.refreshUnion != 'undefined') {
                                                $('#union-value').text(data.refreshUnion);
                                            }
                                        }
                                    );
                                });
                            }
                        });

                        submit_btn_modal.on('click', function () {
                            form_modal.parsley().asyncValidate();
                        });
                    }

                    if (delete_btn_modal) {
                        delete_btn_modal.on('click', function () {
                            var url = $(this).data('url');
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {},
                                success: function (response) {
                                    $modal.modal('hide');
                                    refreshGrid(refreshItem);

                                    if (typeof response.refreshUnion != 'undefined') {
                                        $('#union-value').text(response.refreshUnion);
                                    }
                                }
                            });
                        });
                    }
                }
            } else {
                location.reload();
            }
        });
    });
    $('body').on('click', '#submit-btn', function (e) {
        e.preventDefault();
        var form_modal = $('#content-form');

        var refreshItem = form_modal.data('refresh');
        swal({
            title: "Are you sure you want to save?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#019964",
            confirmButtonText: "Yes!",
            cancelButtonText: "No",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post(
                        form_modal.attr('action'),
                        form_modal.serialize(),
                        function (data) {
                            if (data.error) {
                                window.location.reload();
                            } else {
                                resolve();
                            }
                        }
                    );
                });
            }
        }).then(function () {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }

            window.location.href = window.location.origin + window.location.pathname;
        });
    });

    $('body').on('click', '#delete-btn', function (e) {
        e.preventDefault();
        var form_modal = $('#content-form');

        var refreshItem = form_modal.data('refresh');
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this list!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    if (!window.location.origin) {
                        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
                    }
                    var url = (window.location.origin + window.location.pathname).replace("summary", "delete");

                    $.post(
                        url,
                        form_modal.serialize(),
                        function (data) {
                            resolve();
                        }
                    );
                });
            }
        }).then(function () {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }

            window.location.href = window.location.origin + "/pprofiles/lists";
        });

    });


    function datePickerProjectDel() {
        var startDate, endDate, config, filter;

        var filters = _.compact(
            String(window.location.hash.slice(3)).split('/').splice(2)
        );

        config = {
            opens: 'right'
        };

        _.each(filters, function (route) {
            filter = route.split(':');

            if (filter[0] === 'created_at' && filter[1] !== undefined && filter[2] !== undefined) {
                startDate = moment(filter[1]);

                endDate = moment(filter[2]);
            }
        });

        if (startDate && endDate) {
            config = {
                startDate: startDate,
                endDate: endDate,
                opens: 'right'
            };
        }

        var datepicker_project_del = $('[data-grid="project-del"] [data-grid-calendar]').daterangepicker(config, function (start, end, label) {
            $('[data-grid="project-del"] [data-range-start]').trigger('change');
        });

        datepicker_project_del.on('apply.daterangepicker', function (ev, picker) {
            $('[data-grid="project-del"] [data-range-start]').val(picker.startDate.format('YYYY-MM-DD 00:00:00'));
            $('[data-grid="project-del"] [data-range-end]').val(picker.endDate.format('YYYY-MM-DD  00:00:00'));
            $('[data-grid="project-del"] [data-range-start]').trigger('change');
        });

        datepicker_project_del.on('show.daterangepicker', function () {
            showDirectionPP('Follow Up');
        });
    };

    function datePickerProjectAdd() {
        var startDate, endDate, config, filter;

        var filters = _.compact(
            String(window.location.hash.slice(3)).split('/').splice(2)
        );

        config = {
            opens: 'right'
        };

        _.each(filters, function (route) {
            filter = route.split(':');

            if (filter[0] === 'created_at' && filter[1] !== undefined && filter[2] !== undefined) {
                startDate = moment(filter[1]);

                endDate = moment(filter[2]);
            }
        });

        if (startDate && endDate) {
            config = {
                startDate: startDate,
                endDate: endDate,
                opens: 'right'
            };
        }

        var datepicker_project_add = $('[data-grid="project-add"] [data-grid-calendar]').daterangepicker(config, function (start, end, label) {
            $('[data-grid="project-add"] [data-range-start]').trigger('change');
        });

        datepicker_project_add.on('apply.daterangepicker', function (ev, picker) {
            $('[data-grid="project-add"] [data-range-start]').val(picker.startDate.format('YYYY-MM-DD 00:00:00'));
            $('[data-grid="project-add"] [data-range-end]').val(picker.endDate.format('YYYY-MM-DD  00:00:00'));
            $('[data-grid="project-add"] [data-range-start]').trigger('change');
        });

        datepicker_project_add.on('show.daterangepicker', function () {
            showDirectionPP('Follow Up');
        });
    };

});
