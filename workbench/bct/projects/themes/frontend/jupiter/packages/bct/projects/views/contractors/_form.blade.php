<div class="panel-body">

    <div class="col-md-12">

        {{-- Form: General --}}
        <fieldset>
            <div class="row">

                <div class="col-md-4">

                    {{--add more form-group for other fields, just keep the same uniform--}}

                    {{-- Form:name --}}
                    <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                        <label for="name" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.name') }}}
                        </label>

                        <input type="text" class="form-control" name="name" id="name"
                               maxlength="64"
                               placeholder="{{{ trans('bct/projects::contractors/model.general.name_help') }}}"
                               value="{{{ input()->old('name', $contractor->name) }}}">

                        <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                    </div>

                </div>

                <div class="col-md-4">

                    {{-- Form:address --}}
                    <div class="form-group{{ Alert::onForm('address', ' has-error') }}">

                        <label for="address" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.address') }}}
                        </label>

                        <input type="text" class="form-control" name="address" id="address"
                               maxlength="64"
                               placeholder="{{{ trans('bct/projects::contractors/model.general.address_help') }}}"
                               value="{{{ input()->old('address', $contractor->address) }}}">

                        <span class="help-block">{{{ Alert::onForm('address') }}}</span>

                    </div>

                </div>
                <div class="col-md-4">

                    {{-- Form:city --}}
                    <div class="form-group{{ Alert::onForm('city', ' has-error') }}">

                        <label for="city" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.city') }}}
                        </label>

                        <input type="text" class="form-control" name="city" id="city"
                               maxlength="25"
                               placeholder="{{{ trans('bct/projects::contractors/model.general.city_help') }}}"
                               value="{{{ input()->old('city', $contractor->city) }}}">

                        <span class="help-block">{{{ Alert::onForm('city') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-4">

                    {{-- Form:state --}}
                    <div class="form-group{{ Alert::onForm('state', ' has-error') }}">

                        <label for="state" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.state') }}}
                        </label>

                        <input type="text" class="form-control" name="state" id="state" maxlength="2"
                               placeholder="{{{ trans('bct/projects::contractors/model.general.state_help') }}}"
                               value="{{{ input()->old('state', $contractor->state) }}}">

                        <span class="help-block">{{{ Alert::onForm('state') }}}</span>

                    </div>

                </div>
                <div class="col-md-4">

                    {{-- Form:zipcode --}}
                    <div class="form-group{{ Alert::onForm('zipcode', ' has-error') }}">

                        <label for="zipcode" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.zip') }}}
                        </label>

                        <input type="text" class="form-control" name="zipcode" id="zipcode" data-mask-zip
                               placeholder="{{{ trans('bct/projects::contractors/model.general.zip_help') }}}"
                               value="{{{ input()->old('zipcode', $contractor->zipcode) }}}">

                        <span class="help-block">{{{ Alert::onForm('zipcode') }}}</span>

                    </div>

                </div>
                <div class="col-md-4">

                    {{-- Form:phone --}}
                    <div class="form-group{{ Alert::onForm('phone', ' has-error') }}">

                        <label for="phone" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.phone') }}}
                        </label>

                        <input type="text" class="form-control" name="phone" id="phone" data-mask
                               placeholder="{{{ trans('bct/projects::contractors/model.general.phone_help') }}}"
                               value="{{{ input()->old('phone', $contractor->phone) }}}">

                        <span class="help-block">{{{ Alert::onForm('phone') }}}</span>

                    </div>

                </div>



            </div>


            <div class="row">

                <div class="col-md-6">

                    {{-- Form:email --}}
                    <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                        <label for="email" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.email') }}}
                        </label>

                        <input type="text" class="form-control" name="email" id="email"
                               placeholder="{{{ trans('bct/projects::contractors/model.general.email_help') }}}"
                               value="{{{ input()->old('email', $contractor->email) }}}">

                        <span class="help-block">{{{ Alert::onForm('email') }}}</span>

                    </div>

                </div>
                <div class="col-md-6">

                    {{-- Form:license --}}
                    <div class="form-group{{ Alert::onForm('license', ' has-error') }}">

                        <label for="license" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.license') }}}
                        </label>

                        <input type="text" class="form-control" name="license" id="license" maxlength="15"
                               placeholder="{{{ trans('bct/projects::contractors/model.general.license_help') }}}"
                               value="{{{ input()->old('license', $contractor->license) }}}">

                        <span class="help-block">{{{ Alert::onForm('license') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-md-6">

                    {{-- Form:ein --}}
                    <div class="form-group{{ Alert::onForm('ein', ' has-error') }}">

                        <label for="ein" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.ein') }}}
                        </label>

                        <input type="text" class="form-control" name="ein" id="ein" data-mask-ein
                               placeholder="{{{ trans('bct/projects::contractors/model.general.ein_help') }}}"
                               value="{{{ input()->old('ein', $contractor->ein) }}}">

                        <span class="help-block">{{{ Alert::onForm('ein') }}}</span>

                    </div>

                </div>

                <div class="col-md-6">

                    {{-- Form:notes --}}
                    <div class="form-group{{ Alert::onForm('notes', ' has-error') }}">

                        <label for="notes" class="control-label">
                            {{{ trans('bct/projects::contractors/model.general.notes') }}}
                        </label>

                            <textarea class="form-control" name="notes" rows="3"
                                      placeholder="{{{ trans('bct/projects::contractors/model.general.notes_help') }}}"
                                    >{{{ input()->old('notes', $contractor->notes) }}}</textarea>

                        <span class="help-block">{{{ Alert::onForm('notes') }}}</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-3">

                    {{-- Form:is_union --}}
                    <div class="form-group{{ Alert::onForm('is_union', ' has-error') }}">

                        <label for="is_union" class="row col-md-12 control-label">
                            {{{ trans('bct/projects::contractors/model.general.is_union') }}}
                        </label>

                        <input type="checkbox" name="is_union" id="is_union"
                               @if (input()->old('is_union', $contractor->is_union))
                               checked
                                @endif
                        />

                        <span class="help-block">{{{ Alert::onForm('is_union') }}}</span>

                    </div>


                </div>

                <div class="col-md-3">
                    <div class="form-group{{ Alert::onForm('active', ' has-error') }}">

                        <label for="active" class="row col-md-12 control-label">
                            {{{ trans('bct/projects::general/common.active') }}}
                        </label>

                        <input type="checkbox" name="active" id="active"
                               @if (input()->old('active', $contractor->active))
                               checked
                                @endif
                        />

                        <span class="help-block">{{{ Alert::onForm('active') }}}</span>

                    </div>
                </div>


            </div>

        </fieldset>

    </div>

</div>

<div class="panel-footer">
    @if (request()->ajax())
        <button id="submit-btn" class="btn btn-primary">
            <i class="fa fa-save fa-fw"></i>{{{ trans('action.save') }}}
        </button>

        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ trans('action.cancel') }}
        </button>
    @endif
</div>