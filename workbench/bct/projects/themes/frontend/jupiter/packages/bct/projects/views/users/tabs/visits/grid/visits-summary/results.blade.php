<script type="text/template" data-grid="visits-summary" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.project.name %></td>
        <td class="text-center"><%- r.created %></td>
    </tr>

    <% }); %>

</script>
