<script type="text/template" data-grid="project-summary" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
            <td class="text-center"><%- r.name %></td>
            <td class="text-center"><%- r.follow_up_date %></td>
            <td class="text-center">
                <% if (r.status == 'Visited') { %>
                <span class="label label-success"><%- r.status %></span>
                <% } else if (r.status == 'Not visited') {%>
                <span class="label label-danger"><%- r.status %></span>
                <% } else {%>
                <span class="label label-default"><%- r.status %></span>
                <% } %>
            </td>
		</tr>

	<% }); %>

</script>
