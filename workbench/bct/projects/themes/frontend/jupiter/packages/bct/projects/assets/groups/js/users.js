var Extension;

;(function(window, document, $, undefined)
{

	'use strict';

    Extension = Object.create(ExtensionBase);
    Extension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'user'
    });

	// Initialize functions
	Extension.Index.init = function()
	{
		Extension.Index.dataGrid();
		Extension.Index.listeners();
	};

	// Add Listeners
	Extension.Index.listeners = function()
	{
		Platform.Cache.$body
			//.on('click', '[data-grid-row]', Extension.Index.checkRow)
			.on('click', '#data-grid [data-grid-row] a', Extension.Index.titleClick)
			.on('click', '#data-grid [data-grid-checkbox]', Extension.Index.checkboxes)
			.on('click', '#btn-add-groups', Extension.Index.bulkActions)
			.on('click', '.add-to-group', Extension.Index.bulkRow)
		;
	};

	// Handle Data Grid checkboxes
	Extension.Index.checkboxes = function(event)
	{
		event.stopPropagation();

		var type = $(this).attr('data-grid-checkbox');

		if (type === 'all')
		{
			$('#data-grid [data-grid-checkbox]').not(this).prop('checked', this.checked);

			$('#data-grid [data-grid-row]').not(this).toggleClass('active', this.checked);
		}

		$(this).parents('[data-grid-row]').toggleClass('active');

		Extension.Index.bulkStatus();
	};

	// Handle Data Grid row checking
	Extension.Index.checkRow = function()
	{
		$(this).toggleClass('active');

		var checkbox = $(this).find('[data-grid-checkbox]');

		checkbox.prop('checked', ! checkbox.prop('checked'));

		Extension.Index.bulkStatus();
	};

	Extension.Index.bulkStatus = function()
	{
		var rows = $('#data-grid [data-grid-checkbox]').not('[data-grid-checkbox="all"]').length;

		var checked = $('#data-grid [data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').length;

		$('[data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);

		if (checked > 0)
		{
			$('[data-grid-bulk-action="delete"]').attr('data-modal', true);
			$('#btn-add-groups').removeClass('disabled');
		}
		else
		{
			$('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
			$('#btn-add-groups').addClass('disabled');
		}

		$('#data-grid [data-grid-checkbox="all"]')
			.prop('disabled', rows < 1)
			.prop('checked', rows < 1 ? false : rows === checked)
		;
	};

	// Handle Data Grid bulk actions
	Extension.Index.bulkActions = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

		var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
		{
			return +event.value;
		});

		if (rows.length > 0)
		{
			$.ajax({
				type: 'POST',
				url: url,
				data: {
					action : 'attach',
					rows   : rows
				},
				success: function(response)
				{
					ExtensionAdded.Index.Grid.refresh();
					Extension.Index.Grid.refresh();
				}
			});
		}
	};

	Extension.Index.bulkRow = function(event)
	{
		event.preventDefault();
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
		var url = window.location.origin + window.location.pathname;

		var id = $(this).parents('tr').find('input').val();

		if (id)
		{
			$.ajax({
				type: 'POST',
				url: url,
				data: {
					action : 'attach',
					rows   : [id]
				},
				success: function(response)
				{
					ExtensionAdded.Index.Grid.refresh();
					Extension.Index.Grid.refresh();
				}
			});
		}
	};

	// Ignore row selection on title click
	Extension.Index.titleClick = function(event)
	{
		event.stopPropagation();
	};

	// Data Grid initialization
	Extension.Index.dataGrid = function()
	{
		var config = {
			scroll: '#data-grid',
			events: {
				sorted: function(response) {
					if (response.pagination.total== 0) {
						$('.download-container').addClass('hide');
					}
				},
				fetched: function(response) {
					if (response.pagination.filtered == 0) {
						$('.download-container').addClass('hide');
					} else {
						$('.download-container' ).removeClass('hide');
					}
				},
			},
			callback: function()
			{
				$('[data-grid-checkbox-all]').prop('checked', false);

				$('[data-action]').prop('disabled', true);

				Extension.Index.bulkStatus();
			}
		};

		Extension.Index.Grid = $.datagrid('user', '#data-grid', '.data-grid_pagination', '#data-grid_applied', config);
	};

	// Job done, lets run.
	Extension.Index.init();

})(window, document, jQuery);
