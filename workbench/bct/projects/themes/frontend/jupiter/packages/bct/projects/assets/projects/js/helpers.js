/**
 * Created by Aphiwad on 10/28/2015.
 */
function switchContent(link, hash) {
    var tempScrollTop = $(window).scrollTop();
    $(link).tab('show');
    window.location.hash = hash;
    $(window).scrollTop(tempScrollTop);
}
