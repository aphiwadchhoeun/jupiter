<script type="text/template" data-grid="summary_projects" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>

            <td><%- r.project_name %></td>
            <td><%- r.last_visit %></td>
			<td><a target="_blank" href="<%- r.details_url %>" class="btn btn-primary btn-circle btn-details" data-toggle="tooltip" data-placement="right"
				   data-original-title="{{{ trans('bct/projects::general/common.details') }}}"><i
							class="fa fa-book fa-fw"></i></a></td>
		</tr>

	<% }); %>

</script>
