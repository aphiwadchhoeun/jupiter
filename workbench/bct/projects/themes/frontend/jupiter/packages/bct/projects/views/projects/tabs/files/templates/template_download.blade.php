<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download" data-id="{%=file.id%}">
    <td>

            <a href="{%= file.url%}" download="{%=file.name%}">{%=file.name%}</a>

            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <input type="text" name="name" value="{%=file.name%}"
                class="form-control w100"
                data-url="{%= file.url%}"
                data-name="{%= file.name%}"
                data-extension="{%= file.extension%}"
            >
        </td>
        <td>

            <input type="text" name="description" value="{%=file.description%}"
                class="form-control w100"
        </td>
        <td class="text-center">
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        @if (!$currentUser->inRole('pp-guest'))
        <td class="text-center">
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" id="file_delete" name="file_delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
        @endif
    </tr>
{% } %}
</script>