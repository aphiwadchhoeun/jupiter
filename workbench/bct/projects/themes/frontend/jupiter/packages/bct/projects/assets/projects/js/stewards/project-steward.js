var ProjectStewardExtension;

;
(function (window, document, $, undefined) {

    'use strict';

    ProjectStewardExtension = Object.create(ExtensionBase);
    ProjectStewardExtension.Index = $.extend({}, ExtensionBase.Index, {
        grid: 'project-steward'
    });

    // Initialize functions
    ProjectStewardExtension.Index.init = function () {
        ProjectStewardExtension.Index.dataGrid();
        ProjectStewardExtension.Index.listeners();
    };

    // Add Listeners
    ProjectStewardExtension.Index.listeners = function () {
        Platform.Cache.$body
            .on('click', '[data-grid-row] a', ProjectStewardExtension.Index.titleClick)
        ;
    };

    // Ignore row selection on title click
    ProjectStewardExtension.Index.titleClick = function (event) {
        event.stopPropagation();
    };

    ProjectStewardExtension.Index.exporterStatus = function (grid) {
        $('#project-steward-exporter').toggleClass('disabled', grid.pagination.filtered == 0);
        $('#project-steward-exporter').parent().toggleClass('disabled', grid.pagination.filtered == 0);

        return this;
    };

    // Data Grid initialization
    ProjectStewardExtension.Index.dataGrid = function () {
        var config = {
            hash: false,
            callback: function (data) {
                ProjectStewardExtension.Index
                    .exporterStatus(data);
            }
        };

        ProjectStewardExtension.Index.Grid = $.datagrid('project-steward', '#project-steward-data-grid', '.project-steward-data-grid_pagination', '#project-steward-data-grid_applied', config);
    };

    // Job done, lets run.
    ProjectStewardExtension.Index.init();

})(window, document, jQuery);

// Project Steward Data Grid
$('#project-steward-data-grid').on('click', '.delete-confirm', function (e) {
    e.preventDefault();
    var sourceBtn = $(this);
    ajaxBtnStatus(sourceBtn, 'fa-trash');

    var $this = $(this);

    swal({
        title: "Are you sure you want to delete?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc002e",
        confirmButtonText: "Yes!",
        cancelButtonText: "No",
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post($this.attr('href'), {},
                    function (data) {
                        ajaxBtnStatus(sourceBtn, 'fa-trash');

                        if (data == 'success') {
                            resolve();
                        } else {
                            reject('Deleting Steward Failed!');
                        }
                    }
                );
            })
        }
    }).then(function () {
        swal('Steward Deleted!', '', 'success');
        ProjectStewardExtension.Index.Grid.refresh();

    }, function () {
        ajaxBtnStatus(sourceBtn, 'fa-trash');

    });
});