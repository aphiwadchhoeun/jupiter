@extends('bct/projects::default')

{{-- Page title --}}
@section('title')
    @parent
    Youtube Videos
@stop

{{-- Inline scripts --}}
@section('scripts')
    @parent
@stop

{{-- Inline styles --}}
@section('styles')
    @parent
@stop

{{-- Page --}}
@section('page-extension')

    <div class="row">

        <div class="col-md-12">

            <h1>Playlist for Reps: </h1>

            <div class="col-md-6">
                <h3>01 Using datagrids to filter, search, and export</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/s9BDyEiDMfM?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>02 Creating a new project profile</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/bUnJPNQhqZw?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>03 Creating & linking contractors to project</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/FbPsYXy7xtg?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>04 Creating & linking contacts to project</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/8y9LyUmJHyA?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>05 Creating actions to project</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/_4EoJeX1FF0?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>06 Creating rep visit to project</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/OgB9O3UxpyM?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>07 Attaching files to project</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/T6T7voKhIek?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>08 Map search for projects</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/L49D50BjyCA?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>09 List management & project assignment</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Bfjq9_2bp64?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>10 Routes to visit projects</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/xfC2T5Ml0dI?list=PL058yknUqTAQe7H3tNqW21PLaWmuIlOtm" frameborder="0" allowfullscreen></iframe>
            </div>

        </div>

        <div class="col-md-12">

            <h1>Playlist for RepAdmins: </h1>

            <div class="col-md-6">
                <h3>01 Admin - Approving project visits</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/_3i02ISJCWg?list=PL058yknUqTATWaSvjV6Xtyvb5vYhJVIST" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>02 Admin - Creating and managing users</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/JrNhM47ts6I?list=PL058yknUqTATWaSvjV6Xtyvb5vYhJVIST" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h3>03 Admin - Creating Lists</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/DloE08CeZMs?list=PL058yknUqTATWaSvjV6Xtyvb5vYhJVIST" frameborder="0" allowfullscreen></iframe>
            </div>

        </div>

    </div>

@stop