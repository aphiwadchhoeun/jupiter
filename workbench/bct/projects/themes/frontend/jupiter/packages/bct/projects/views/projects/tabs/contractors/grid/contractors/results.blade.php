<script type="text/template" data-grid="contractors" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.id %></td>
        <td><a href="<%- r.edit_uri %>"><%- r.name %></a></td>
        <td><%- r.city %></td>
        <td><%- r.state %></td>
        <td><a class="under-none" href="tel:<%- r.phone %>"><%- r.phone %></a></td>
        <td><%- r.license %></td>
        <td>
                <a href="<%- r.view_uri %>"

                    class="btn btn-success btn-circle
                        @if (!$project->has_access_add_contractor)
                                btn-swal-error
                        @endif
                    "
                   @if ($project->has_access_add_contractor)
                    data-modal
                   @endif
                   data-placement="right"  data-toggle="tooltip"
                    data-original-title="{{{ trans('bct/projects::projects/common.connect') }}}">
                    <i class="fa fa-plus fa-fw"></i>
                </a>
        </td>
    </tr>

    <% }); %>

</script>
