<script type="text/template" data-grid="contact-types" data-template="results">

	<% _.each(results, function(r) { %>
		<tr data-grid-row>
			<td><%- r.name %></td>
            <td>
                <a href="<%- r.view_uri %>" class="btn btn-primary btn-circle" data-toggle="tooltip" data-placement="right"
                   data-original-title="{{{ trans('bct/projects::general/common.details') }}}" data-modal><i
                            class="fa fa-book fa-fw"></i></a>
            </td>
		</tr>

	<% }); %>

</script>
