<html><head>
    <style>
        .navbar-brand {
            font-weight: bold;
            display: block;
            margin-bottom: 5px;
            font-size: 16px;
        }

        .table-striped > tbody > tr:first-child > td {
            font-weight: bold;
        }
        .pdf-footer {
        }

        .pdf-footer > * {
            font-size: 10px !important;
        }

        .pdf-footer .footer-timestamp {
            position: absolute;
            left: 0px;
            top: 0px;
            width: 200px;
        }

        .pdf-footer .footer-pages {
            position: absolute;
            left: 450px;
            top: 0px;
            width: 100px;
        }

        .pdf-footer .footer-info {
            position: absolute;
            right: 0px;
            top: 0px;
            width: 400px;
            text-align: right;
        }

        .pdf-footer .footer-text {
            position: absolute;
            left: 0px;
            top: 20px;
        }

        .text-right {
            text-align: right;
        }

        .text-underline {
            text-decoration: underline;
        }

        .not-break {
            page-break-inside: avoid;
        }
    </style>
    <script>
        function subst() {
            var vars={};
            var x=window.location.search.substring(1).split('&');
            for (var i in x) {var z=x[i].split('=',2);vars[z[0]] = unescape(z[1]);}
            var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
            for (var i in x) {
                var y = document.getElementsByClassName(x[i]);
                for (var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
            }
        }
    </script></head><body style="border:0; margin:0;" onload="subst()">

    <div class="pdf-footer">
        <div class="footer-timestamp">
            {{ Carbon\Carbon::now()->format('d M Y H:i:s') }}
        </div>
        <div class="footer-pages">Page <span class="page"></span></div>
        <div class="footer-info">
            @setting('platform.app.tagline')
        </div>

        <div class="footer-text">
            Contains jobs being performed by PNWRCC Signatory Contractors or Non-Signatory Contractors with signed Project Agreements. Additional information about jobs listed within this report can be obtained by contacting your Council Representative at 1-877-745-9555 or visiting the jobsite directly. This report is the property of the PNWRCC and is not to be duplicated or distributed without prior written consent from the PNWRCC. No exceptions.
        </div>
    </div>

</body></html>


