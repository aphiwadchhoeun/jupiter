<div class="modal-dialog ext-profiles" id="project-contractors-modal">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="gridSystemModalLabel">{{{ trans('bct/projects::projects/common.contractor_' . $mode) }}}</h4>
        </div>

        <div class="modal-body">

            <form id="content-form-modal" action="
                    @if(isset($pivot))
                        {{ route('bct.projects.projects.contractor.edit', [$project->id, $contractor->id, $pivot->id]) }}
                    @else
                        {{ route('bct.projects.projects.contractor.add', [$project->id, $contractor->id]) }}
                    @endif" data-refresh="contractors">

                {{-- Form: CSRF Token --}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="contractor_id" value="{{ $contractor->id }}">
                @if(isset($pivot))
                    <input type="hidden" name="pivot_id" value="{{ $pivot->id }}">
                @endif

                <div class="panel-body">

                    <div class="col-md-12">

                        {{-- Form: General --}}
                        <fieldset>
                            <div class="row">
                                <table class="table table-bordered table-details">
                                    <tbody>
                                        <tr>
                                            <td class="td-value">
                                                <div >
                                                    <div class="col-md-6">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.name') }}}:</b>
                                                                <span class="pre-spaces">{{ $contractor->name }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.address') }}}:</b>
                                                                <span class="pre-spaces">{{ $contractor->location }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.phone') }}}:</b>
                                                                <span class="pre-spaces"><a class="under-none" href="tel:{{ $contractor->phone }}">{{ $contractor->phone }}</a></span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.ubi') }}}:</b>
                                                                <span class="pre-spaces">{{ $contractor->ubi }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.union') }}}:</b>
                                                                <span class="pre-spaces">
                                                                    @if($contractor->is_union)
                                                                        <span class="label label-success">Yes</span>
                                                                    @else
                                                                        <span class="label label-danger">No</span>
                                                                    @endif
                                                                </span>
                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.website') }}}:</b>
                                                                <span class="pre-spaces"><a class="under-none" target="_blank" href="http://{{ $contractor->website }}">{{ $contractor->website }}</a></span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.email') }}}:</b>
                                                                <span class="pre-spaces"><a class="under-none" href="mailto:{{ $contractor->email }}">{{ $contractor->email }}</a></span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.notes') }}}:</b>
                                                                <span class="pre-spaces">{{ $contractor->notes }}</span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.license') }}}:</b>
                                                                <span class="pre-spaces">{{ $contractor->license }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.fax') }}}:</b>
                                                                <span class="pre-spaces"><a class="under-none" href="tel:{{ $contractor->fax }}">{{ $contractor->fax }}</a></span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.ein') }}}:</b>
                                                                <span class="pre-spaces">{{ $contractor->ein }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <b>{{{ trans('bct/projects::contractors/model.general.updated_at') }}}:</b>
                                                                <span class="pre-spaces">{{ date_correct_details_projects($contractor->updated_at) }}</span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                          </td>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                            <div class="row">

                                {{--add more form-group for other fields, just keep the same uniform--}}
                                <div class="form-group">

                                    <label for="scope_id" class="control-label">
                                        {{{ trans('bct/projects::projects/model.general.scope_id') }}}
                                    </label>
                                    @if($mode==='update')
                                        <select class="form-control" id="scope_id" name="scope_id" style="width: 100%" required>
                                            @foreach($scopes as $scope)
                                                <option value="{{ $scope->id }}" @if(isset($pivot) && $pivot->z_scopes_id == $scope->id) selected @endif
                                                >{{ $scope->name }} ({{ $scope->value }})</option>
                                            @endforeach
                                        </select>
                                    @else
                                    <select class="form-control" name="scope_id[]" multiple data-parsley-multiple="scope_id[]" id="scope_id" style="width: 100%" required>
                                        @foreach($scopes as $scope)
                                            <option value="{{ $scope->id }}" @if(isset($pivot) && $pivot->z_scopes_id == $scope->id) selected @endif
                                                    >{{ $scope->name }} ({{ $scope->value }})</option>
                                        @endforeach
                                    </select>
                                    @endif

                                    <span class="help-block">{{{ Alert::onForm('scope_id') }}}</span>

                                </div>

                                @if(!$contractor->is_union)
                                    {{--add more form-group for other fields, just keep the same uniform--}}
                                    <div class="form-group">

                                        <label class="control-label">
                                            {{{ trans('bct/projects::projects/model.general.is_agreement') }}}
                                        </label>

                                        <div class="radio">
                                            <input type="radio" name="is_agreement" id="is_agreement1" value="1"
                                                   @if(isset($pivot) && $pivot->is_agreement) checked="checked" @endif />
                                            <label for="is_agreement1">Yes</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="is_agreement" id="is_agreement2" value="0"
                                                   @if(!isset($pivot) || !$pivot->is_agreement) checked="checked" @endif />
                                            <label for="is_agreement2">No</label>
                                        </div>

                                    </div>
                                @else
                                    <div class="form-group">

                                        <label class="control-label">
                                            {{{ trans('bct/projects::projects/model.general.is_non_union') }}}
                                        </label>

                                        <div class="radio">
                                            <input type="radio" name="is_non_union" id="is_non_union_yes" value="1"
                                                   @if(isset($pivot) && !$pivot->is_union) checked="checked" @endif />
                                            <label for="is_non_union_yes">Yes</label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="is_non_union" id="is_non_union_no" value="0"
                                                   @if(!isset($pivot) || $pivot->is_union) checked="checked" @endif />
                                            <label for="is_non_union_no">No</label>
                                        </div>

                                    </div>
                                @endif

                            </div>

                        </fieldset>

                    </div>
                </div>
            </form>
        </div>

        <div class="modal-footer">
            <button id="submit-btn-modal" name="submit-btn-modal" class="btn btn-primary">
                {{{ trans('action.save') }}}</button>

                @if(isset($pivot))
                    <button id="delete-btn-modal" name="delete-btn-modal" class="btn btn-danger" data-action="delete"
                            data-url="{{ route('bct.projects.projects.deleteContractor', [
                                'id' => $project->id,
                                'pivot_id' => $pivot->id,
                            ]) }}">
                        {{{ trans('action.delete') }}}</button>
                @endif

            <button type="button" class="btn btn-default" data-dismiss="modal">{{{ trans('action.cancel') }}}</button>
        </div>

    </div>

</div>
