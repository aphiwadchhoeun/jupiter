<script type="text/template" data-grid="reps-summary" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row>
            <td class="text-center"><%- r.name %></td>
            <td class="text-center"><%- r.group %></td>
		</tr>

	<% }); %>

</script>
