<div class="row hidden-print">

    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li>
                <a href="#details" id="tab_project_details" name="tab_project_details">
                    {{{ trans("bct/projects::projects/common.tabs.details") }}}
                </a>
            </li>
            @if (!$currentUser->inRole('pp-guest'))
            <li>
                <a href="#visits" id="tab_project_visits" name="tab_project_visits">
                    {{{ trans("bct/projects::projects/common.tabs.visits") }}}
                </a>
            </li>
            @endif
            <li>
                <a href="#contractors" id="tab_project_contractors" name="tab_project_contractors">
                    {{{ trans("bct/projects::projects/common.tabs.contractors") }}}
                </a>
            </li>
            <li>
                <a href="#contacts" id="tab_project_contacts" name="tab_project_contacts">
                    {{{ trans("bct/projects::projects/common.tabs.contacts") }}}
                </a>
            </li>
            <li>
                <a href="#members" id="tab_project_members" name="tab_project_members">
                    {{{ trans("bct/projects::projects/common.tabs.members") }}}
                </a>
            </li>
            <li>
                <a href="#actions-history" id="tab_project_actions_history" name="tab_project_actions_history">
                    {{{ trans("bct/projects::projects/common.tabs.actions") }}}
                </a>
            </li>
            <li>
                <a href="#files" id="tab_project_files" name="tab_project_files">
                    {{{ trans("bct/projects::projects/common.tabs.files") }}}
                </a>
            </li>
            @if (!$currentUser->inRole('pp-guest'))
            <li>
                <a href="#lists" id="tab_project_lists" name="tab_project_lists">
                    {{{ trans("bct/projects::projects/common.tabs.lists") }}}
                </a>
            </li>
            @endif
        </ul>
    </div>

</div>