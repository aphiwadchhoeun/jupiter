<span class="navbar-brand">{{ trans("bct/projects::contractors/common.title") }} <span id="contractors-counter" class="text14">( {{ $contractors->take(5)->count() }} {{ trans('bct/projects::general/common.of') }} {{ $contractors->count() }} )</span></span>
@if (!$currentUser->inRole('pp-guest'))

<a data-id="#contractors" id="contractors_tab" name="contractors_tab" class="btn btn-primary go-to-tab pull-right"
   data-toggle="tooltip" data-original-title="{{{ trans('action.edit') }}}"><i
            class="fa fa-edit fa-fw"></i></a>
@endif
<table class="table table-striped table-bordered" id="contractor-summary"
data-union-value-url="{{ route('bct.projects.projects.details.unionvalue', $project->id) }}">
    <tbody>
    <tr>
        <td class="td-label col-md-3 text-center">{{ trans('bct/projects::projects/model.general.scope_id') }}</td>
        <td class="td-label col-md-5 text-center">{{ trans('bct/projects::contractors/model.general.contractor') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.union') }}</td>
        <td class="td-label col-md-2 text-center">{{ trans('bct/projects::contractors/model.general.pa') }}</td>
    </tr>
    @foreach($contractors->take(5) as $contractor)
        <tr>
            <td class="td-value">{{ $contractor->scope }} ({{ $contractor->value }})</td>
            <td class="td-value text-center">
                @if (!is_null($contractor->name))
                    <a href="{{ route('bct.projects.contractors.details', $contractor->id) }}">{{ $contractor->name }}</a>
                @else
                    {{ trans('bct/projects::general/common.na') }}
                @endif
            </td>
            <td class="td-value text-center">
                @if (!is_null($contractor->name))
                    @if($contractor->is_union)
                        <span class="label label-success">Yes</span>
                    @else
                        <span class="label label-danger">No</span>
                    @endif
                @endif
            </td>
            <td class="td-value text-center">
                @if (!is_null($contractor->name))
                    @if ($contractor->is_agreement)
                        <span class="label label-success">Yes</span>
                    @else
                        <span class="label label-danger">No</span>
                    @endif
                @endif
            </td>
        </tr>
    @endforeach
</table>