<script type="text/template" data-grid="project-list" data-template="results">

    <% _.each(results, function(r) { %>

    <tr data-grid-row>
        <td><%- r.name %></td>
        <td><%- r.username %></td>
        <td><%- r.date_added_custom %></td>
        @if (!$currentUser->inRole('pp-guest'))
        <td class="hidden-print">
            <% if (r.delete_uri) { %>
                <a href="<%- r.delete_uri %>" data-toggle="tooltip" class="btn btn-danger btn-circle delete-confirm"
                   data-placement="right" data-original-title="{{{ trans('bct/projects::general/common.remove') }}}">
                        <i class="fa fa-minus fa-fw"></i></a>
            <% } %>
        </td>
        @endif
    </tr>

    <% }); %>

</script>
