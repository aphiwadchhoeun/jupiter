<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateScopesClassificationsJoinTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_scopes_classifications_join');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'  => 'z_scopes_id',
                'type'  => 'integer',
                'index' => true,
            ],
            [
                'name'    => 'z_classifications_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => null,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'value',
                'type'    => 'integer',
                'options' => [
                    'unsigned' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
