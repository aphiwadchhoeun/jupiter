<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreatePpUsersView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_users` AS
            SELECT 
                `users`.`id` AS `id`,
                `users`.`email` AS `email`,
                `users`.`created_at` AS `created_at`,
                IF(`activations`.`completed` IS NULL,0,`activations`.`completed`) AS `status`,
                CONCAT_WS(" ",
                        `users`.`first_name`,
                        `users`.`last_name`) AS `name`,
                GROUP_CONCAT(DISTINCT `roles`.`name`
                    SEPARATOR ",") AS `role`
            FROM
                (((`users`
                LEFT JOIN `role_users` ON ((`role_users`.`user_id` = `users`.`id`)))
                LEFT JOIN `roles` ON ((`role_users`.`role_id` = `roles`.`id`)))
                LEFT JOIN `activations` ON ((`activations`.`user_id` = `users`.`id`)))
            WHERE
                (`roles`.`slug` IN ("repuser", "repadmin", "pp-guest"))
            GROUP BY `users`.`id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
