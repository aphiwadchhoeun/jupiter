<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectsGroupTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_projects_group');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'name',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'description',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'individual',
                'type'    => 'boolean',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'status',
                'type'    => 'boolean',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
