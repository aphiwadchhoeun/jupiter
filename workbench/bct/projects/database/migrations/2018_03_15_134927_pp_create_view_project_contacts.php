<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewProjectContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_project_contacts` AS
            SELECT
                `z_contact`.`first_name` AS `first_name`,
                `z_contact`.`last_name` AS `last_name`,
                concat_ws(\' \',`z_contact`.`first_name`,`z_contact`.`last_name`) AS `full_name`,
                `z_contact`.`id` AS `id`,
                `z_contact`.`email` AS `email`,
                `z_contact`.`phone` AS `phone`,
                `z_contact`.`active` AS `active`,
                `z_project_contact_join`.`company` AS `company`,
                `type`.`name` AS `type`,
                `type`.`id` AS `type_id`,
                `z_project_contact_join`.`z_projects_id` AS `z_projects_id`,
                `z_project_contact_join`.`id` AS `pivot_id`
            FROM ((`z_contact`
                JOIN `z_project_contact_join` on((`z_contact`.`id` = `z_project_contact_join`.`z_contact_id`)))
                JOIN `z_contact_type` `type` on((`z_project_contact_join`.`z_contact_type_id` = `type`.`id`)))
            WHERE isnull (`z_contact`.`deleted_at`)
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
