<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreatePpViewMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_members` AS
            SELECT
                `z_members`.`id`,
                `z_members`.`address`,
                `z_members`.`city`,
                `z_members`.`phone`,
                `z_members`.`email`,
                `z_members`.`member_since`,
                `z_members`.`date_of_birth`,
                `z_members`.`social_number`,
                `z_members`.`dues_paid_though`,
                `z_members`.`member_number`,
                `z_members`.`active`,
                `z_members`.`status_created`,
                `z_members`.`created_at`,
                `z_members`.`monthly_dues`,
                CONCAT(`z_members`.`first_name`, " ", `z_members`.`last_name`) AS member_name,
                CONCAT(`z_members`.`address`, " ", `z_members`.`city`) AS address_city
            FROM
                `z_members`

            WHERE
                `z_members`.`deleted_at` IS null
                    and
                `z_members`.`active` = 1
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
