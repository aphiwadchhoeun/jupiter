<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$util = app()->make('bct.unionimpactbase.migrationutil');

		$util->setTableName('users');

		$util->setOptions([
			[
				'name' 		=> 'z_user_profiles_id',
				'type' 		=> 'integer',
			],
		]);

		$util->run();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
