<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectVisitStageJoinTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_project_visit_stage_join');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'z_project_visit_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_stage_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();

        $sm = Schema::getConnection()->getDoctrineSchemaManager();
        if (!array_has($sm->listTableIndexes('z_project_visit_stage_join'), 'visit_stage_index')) {
            Schema::table('z_project_visit_stage_join', function ($table) {
                $table->index(['z_project_visit_id', 'z_project_stage_id'], 'visit_stage_index');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
