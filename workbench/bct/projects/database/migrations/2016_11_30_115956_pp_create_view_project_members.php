<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewProjectMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_project_members` AS
            SELECT
                CONCAT(`z_members`.`first_name`, " ", `z_members`.`last_name`) AS member_name,
                `z_members`.`id` AS `id`,
                `z_members`.`member_number` AS `member_number`,
                `z_members`.`email` AS `email`,
                `z_members`.`phone` AS `phone`,
                `z_project_member_join`.`z_projects_id` AS `z_projects_id`,
                `z_project_member_join`.`id` AS `pivot_id`,
                z_project_member_join.number_of_hours,
                z_project_member_join.wage,
                z_scopes.name as scope_name
            FROM `z_members`
                JOIN `z_project_member_join` on `z_members`.`id` = `z_project_member_join`.`z_members_id`
                LEFT JOIN z_scopes on z_scopes.id = z_project_member_join.z_scopes_id
            WHERE isnull(`z_members`.`deleted_at`)
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
