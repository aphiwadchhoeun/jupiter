<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewProjectActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_project_actions` AS
            SELECT
                `z_project_action`.`id`,
                `z_action_type`.`name` as action_type,
                `z_project_action`.`action_date`,
                `z_project_action`.`z_projects_id`,
                `z_project_action`.`details` as details,
                `z_contractors`.`name` as contractor,
                `users`.`id` AS `user_id`,
                SUBSTRING( CONCAT_WS(\' \', users.first_name, users.last_name), 1, 35) as username
            FROM `z_project_action`
            LEFT JOIN `z_action_type` ON `z_project_action`.`z_action_type_id`=`z_action_type`.`id`
            LEFT JOIN `z_contractors` ON `z_project_action`.`z_contractors_id` = `z_contractors`.`id`
            LEFT JOIN `users` ON `users`.`id` = `z_project_action`.`user_id`
            WHERE
                `z_project_action`.`deleted_at` is null
                    AND
                `z_project_action`.`z_projects_id` is not null
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
