<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateViewReportActiveProjectsWithStewards extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('CREATE OR REPLACE VIEW `pp_report_active_projects_with_stewards` AS
           SELECT 
                `parent`.`id` AS `project_id`,
                `parent`.`name` AS `project_name`,
                `parent`.`address` AS `project_address`,
                `parent`.`city` AS `project_city`,
                `parent`.`region_abrv` AS `region_abrv`,
                (SELECT 
                        GROUP_CONCAT(IF((CONCAT(`z_contact`.`first_name`,
                                            \' \',
                                            `z_contact`.`last_name`) = \'\'),
                                    `z_contact`.`company`,
                                    CONCAT(`z_contact`.`first_name`,
                                            \' \',
                                            `z_contact`.`last_name`))
                                SEPARATOR \',\')
                    FROM
                        ((`z_project_contact_join`
                        JOIN `z_contact` ON ((`z_project_contact_join`.`z_contact_id` = `z_contact`.`id`)))
                        JOIN `z_projects` ON ((`z_project_contact_join`.`z_projects_id` = `z_projects`.`id`)))
                    WHERE
                        ((`z_projects`.`z_project_status_id` = 1)
                            AND (`z_project_contact_join`.`z_contact_type_id` = 7)
                            AND (`z_project_contact_join`.`z_projects_id` = `parent`.`id`))
                    GROUP BY `z_project_contact_join`.`z_projects_id`) AS `stewards`
            FROM
                (`z_projects` `parent`
                JOIN `z_project_contact_join` ON ((`parent`.`id` = `z_project_contact_join`.`z_projects_id`)))
            WHERE
                (ISNULL(`parent`.`deleted_at`)
                    AND (`parent`.`z_project_status_id` = 1)
                    AND (`z_project_contact_join`.`z_contact_type_id` = 7))
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
