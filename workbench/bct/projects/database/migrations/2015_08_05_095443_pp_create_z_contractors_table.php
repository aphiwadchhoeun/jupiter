<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZContractorsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_contractors');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'name',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'address',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'city',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'state',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'zipcode',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'phone',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'fax',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'email',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'website',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'license',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'ein',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'is_union',
                'type'    => 'boolean',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'active',
                'type'    => 'boolean',
                'options' => [
                    'default'  => 1,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'created_by',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'ubi',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'notes',
                'type'    => 'text',
                'options' => [
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
