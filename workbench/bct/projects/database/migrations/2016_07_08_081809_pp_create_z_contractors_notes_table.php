<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateZContractorsNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_contractor_notes');
        $util->setOptions([
            [
                'name'    => 'id',
                'type'    => 'increments',
                'options' => [
                    'unique' => true
                ]
            ],
            [
                'name' => 'z_contractors_id',
                'type' => 'integer',
                'options' => [
                    'unsigned' => true
                ]
            ],
            [
                'name' => 'created_by',
                'type' => 'integer'
            ],
            [
                'name' => 'notes',
                'type' => 'text',
            ],
            [
                'name' => 'type',
                'type' => 'string',
            ]
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
