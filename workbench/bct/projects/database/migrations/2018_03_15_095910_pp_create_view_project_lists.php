<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewProjectLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_project_lists` AS
            SELECT
                CONCAT(user.first_name, " ", user.last_name) as username,
                `z_project_list`.`id`,
                `z_project_list`.`category`,
                `z_project_list`.`z_projects_group_id`,
                `z_project_list`.`name`,
                `z_project_list`.`id` as `pid`,
                `z_project_list`.`due_date`,
                `z_project_list`.`is_active`,
                `z_project_list`.`created_at`,
                `z_project_list_project_join`.`id` as join_id, 
                `z_project_list_project_join`.`created_by` as join_created_by, 
                `z_project_list_project_join`.`z_projects_id`,
                `z_project_list_project_join`.`created_at` as `date_added`
            FROM `z_project_list`
            INNER JOIN `z_project_list_project_join` ON `z_project_list`.`id` = `z_project_list_project_join`.`z_project_list_id`
            INNER JOIN `users` as `user` ON `z_project_list_project_join`.`created_by` = `user`.`id`
            WHERE
                `z_project_list`.`deleted_at` is null
                    AND
                `z_project_list_project_join`.`is_active` = 1
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
