<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewMapProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW pp_map_project AS
                SELECT
                    `z_projects`.`id` AS `id`,
                    `z_projects`.`name` AS `name`,
                    `z_projects`.`description` AS `description`,
                    `z_projects`.`address` AS `address`,
                    `z_projects`.`county` AS `county`,
                    `z_projects`.`city` AS `city`,
                    `z_projects`.`state` AS `state`,
                    `z_projects`.`zipcode` AS `zipcode`,
                    `z_projects`.`region` AS `region`,
                    `z_projects`.`region_abrv` AS `region_abrv`,
                    `z_projects`.`union_value` AS `union_value`,
                    `z_projects`.`lat` AS `lat`,
                    `z_projects`.`lng` AS `lng`,
                    `z_projects`.`follow_up_date` AS `follow_up_date`,
                    `z_projects`.`follow_up_date` AS `follow_up_date_custom`,
                    `z_project_status`.`id` AS `status_id`,
                    `z_project_status`.`name` AS `status`,
                    `z_projects`.`created_at` AS `created_at`,
                    `z_projects`.`updated_at` AS `updated_at`,
                    `z_projects`.`deleted_at` AS `deleted_at`,
                    `z_project_list_project_join`.`is_active`,
                    GROUP_CONCAT(DISTINCT `tags`.`name`) as `tags`,
                    GROUP_CONCAT( `z_contractors`.`name`) as `contractors`,
                    `visit1`.`id` as `visit_id`,
                    GROUP_CONCAT(DISTINCT `z_project_stage`.`name`) as `stages`
                FROM
                    `z_projects`
                    LEFT JOIN `z_project_status` ON `z_projects`.`z_project_status_id` = `z_project_status`.`id`
                    LEFT JOIN `z_project_list_project_join` on `z_project_list_project_join`.`z_projects_id` = `z_projects`.`id`
                    LEFT JOIN `tagged` ON `z_projects`.`id` = `tagged`.`taggable_id` AND `tagged`.`taggable_type` = "Bct\\\Projects\\\Models\\\Project"
                    LEFT JOIN `tags` ON `tagged`.`tag_id` = `tags`.`id`
                    LEFT JOIN `z_project_contractor_join` ON `z_projects`.`id` = `z_project_contractor_join`.`z_projects_id`
                    LEFT JOIN `z_contractors` ON `z_project_contractor_join`.`z_contractors_id` = `z_contractors`.`id`
                    JOIN `z_project_visit` `visit1` ON `visit1`.`z_projects_id` = `z_projects`.`id`
                    LEFT OUTER JOIN `z_project_visit` `visit2` ON (`visit2`.`z_projects_id` = `z_projects`.`id` AND
                    (`visit1`.`id` < `visit2`.`id`))
                    LEFT JOIN `z_project_visit_stage_join` ON `z_project_visit_stage_join`.`z_project_visit_id` = `visit1`.`id`
                    LEFT JOIN `z_project_stage` ON `z_project_visit_stage_join`.`z_project_stage_id` = `z_project_stage`.`id`
                    WHERE `visit2`.`id` IS NULL
                GROUP BY `z_projects`.`id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
