<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZScopesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_scopes');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'name',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'display_name',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                    'default'  => null,
                ]
            ],
            [
                'name'    => 'craft',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                    'default'  => null,
                ]
            ],
            [
                'name'    => 'value',
                'type'    => 'integer',
                'options' => [
                    'unsigned' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
