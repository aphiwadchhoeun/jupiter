<?php
use Illuminate\Database\Migrations\Migration;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:48 PM
 */


class PpCreateViewContact extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('CREATE OR REPLACE VIEW `pp_contacts` AS
            SELECT
                `z_contact`.`id` AS `id`,
                `z_contact`.`first_name` AS `first_name`,
                `z_contact`.`last_name` AS `last_name`,
                concat(`z_contact`.`first_name`,\' \',`z_contact`.`last_name`) AS `name`,
                `z_contact`.`email` AS `email`,
                `z_contact`.`title` AS `title`,
                `z_contact`.`phone` AS `phone`,
                `z_contact`.`phone2` AS `phone2`,
                `z_contact`.`fax` AS `fax`,
                `z_contact`.`company` AS `company`,
                `z_contact`.`address` AS `address`,
                `z_contact`.`city` AS `city`,
                `z_contact`.`state` AS `state`,
                `z_contact`.`zip` AS `zip`,
                `z_contact`.`notes` AS `notes`,
                `z_contact`.`active` AS `active`,
                `z_contact`.`created_at` AS `created_at`,
                `z_contact`.`deleted_at` AS `deleted_at`
            FROM `z_contact`
            WHERE isnull(`z_contact`.`deleted_at`)
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}