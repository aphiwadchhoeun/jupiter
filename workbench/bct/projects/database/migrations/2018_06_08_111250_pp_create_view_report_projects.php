f<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewReportProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_report_projects` AS
            select 
                `z_projects`.`id` as `project_id`, 
                `z_projects`.`name` as `project_name`, 
                CONCAT(`z_projects`.`address`, \', \', `z_projects`.`city`, \', \', `z_projects`.`state`, \' \', `z_projects`.`zipcode`) as project_full_address,
                z_projects.region_abrv,
                (
                    select DATE_FORMAT(z_project_visit.created_at, \'%b %d, %Y\')
                    from z_project_visit
                    where z_project_visit.`z_projects_id` = `z_projects`.`id`
                    order by z_project_visit.created_at desc, z_project_visit.id desc
                    limit 1
                ) as last_visit_date,
                
                z_project_values.value,
                
                (
                    select group_concat(distinct z_project_stage.name SEPARATOR " / ") 
                    from z_project_stage
                    left join z_project_visit_stage_join on z_project_visit_stage_join.z_project_stage_id = z_project_stage.id
                    where 
                        z_project_visit_stage_join.z_project_visit_id = (
                            select z_project_visit.id
                            from z_project_visit
                            where z_project_visit.`z_projects_id` = `z_projects`.`id`
                            order by z_project_visit.created_at desc, z_project_visit.id desc
                            limit 1
                        )
                ) as stages,
                
                ( 
                    select CONCAT( z_contractors.name, \' - UNION\' )
                    from `z_contractors` 
                    inner join `z_project_contractor_join` on `z_contractors`.`id` = `z_project_contractor_join`.`z_contractors_id` 
                    inner join `z_scopes` as `scope` on `z_project_contractor_join`.`z_scopes_id` = `scope`.`id` 
                    where 
                        `z_project_contractor_join`.`z_projects_id` = `z_projects`.`id` 
                            and 
                        (
                            `z_project_contractor_join`.`is_union` = 1 
                                or 
                            `z_project_contractor_join`.`is_agreement` = 1
                        )
                            and 
                        `scope`.`name` = \'GC\' 
                            and 
                        `z_contractors`.`deleted_at` is null
                    limit 1
                        
                ) AS general_contractor,
            
                ( 
                    select group_concat(distinct concat(scope.name, \': \',  z_contractors.name) SEPARATOR "!!") 
                    from `z_contractors` 
                    inner join `z_project_contractor_join` on `z_contractors`.`id` = `z_project_contractor_join`.`z_contractors_id` 
                    inner join `z_scopes` as `scope` on `z_project_contractor_join`.`z_scopes_id` = `scope`.`id`
            
                    where 
                        `z_project_contractor_join`.`z_projects_id` = `z_projects`.`id` 
                            and 
                        (
                            `z_project_contractor_join`.`is_union` = 1 
                                or 
                            `z_project_contractor_join`.`is_agreement` = 1
                        )
                            and 
                        `scope`.`name` != \'GC\' 
                            and 
                        `z_contractors`.`deleted_at` is null
                            
                ) AS others_contractors
            
            from `z_projects` 
            left join z_project_contractor_join on z_project_contractor_join.`z_projects_id` = z_projects.id
            left join z_contractors on z_project_contractor_join.`z_contractors_id` = z_contractors.id
            left join z_project_values on z_project_values.`id` = z_projects.z_project_values_id
            
            
            where 
                `z_projects`.`deleted_at` is null 
                    and 
                (
                    select count(*) 
                    from `z_contractors` 
                    inner join `z_project_contractor_join` on `z_contractors`.`id` = `z_project_contractor_join`.`z_contractors_id` 
                    inner join `z_scopes` as `scope` on `z_project_contractor_join`.`z_scopes_id` = `scope`.`id` 
                    where 
                        `z_project_contractor_join`.`z_projects_id` = `z_projects`.`id` 
                            and 
                        (
                            `z_project_contractor_join`.`is_union` = 1 
                                or 
                            `z_project_contractor_join`.`is_agreement` = 1
                        )
                            and 
                        `scope`.`name` = \'GC\' 
                            and 
                        `z_contractors`.`deleted_at` is null
                ) >= 1 
                    and 
                (
                    select count(*) 
                    from `z_project_status` 
                    where 
                        `z_projects`.`z_project_status_id` = `z_project_status`.`id` 
                            and 
                        `z_project_status`.`name` = \'Active\'
                ) >= 1 
                    and 
                `union_value` >= 0.75
                
                GROUP BY `z_projects`.`id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
