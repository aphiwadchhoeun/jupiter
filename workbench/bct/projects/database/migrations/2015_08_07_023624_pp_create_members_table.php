<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateMembersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_members');

        $options = [
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name' => 'hash',
                'type' => 'string'
            ],
            [
                'name'  => 'member_number',
                'type'  => 'string',
                'index' => true
            ],
            [
                'name' => 'drivers_license',
                'type' => 'string'
            ],
            [
                'name' => 'social_number',
                'type' => 'string'
            ],
            [
                'name' => 'first_name',
                'type' => 'string'
            ],
            [
                'name' => 'last_name',
                'type' => 'string'
            ],
            [
                'name' => 'address',
                'type' => 'string'
            ],
            [
                'name' => 'city',
                'type' => 'string'
            ],
            [
                'name' => 'state',
                'type' => 'string'
            ],
            [
                'name' => 'zipcode',
                'type' => 'string'
            ],
            [
                'name' => 'phone',
                'type' => 'string'
            ],
            [
                'name' => 'email',
                'type' => 'string'
            ],
            [
                'name' => 'date_of_birth',
                'type' => 'date'
            ],
            [
                'name' => 'gender',
                'type' => 'string'
            ],
            [
                'name' => 'ethnicity',
                'type' => 'string'
            ],
            [
                'name' => 'member_type',
                'type' => 'string'
            ],
            [
                'name' => 'profile_image_name',
                'type' => 'string'
            ],
            [
                'name' => 'profile_image_id',
                'type' => 'integer'
            ],
            [
                'name'    => 'active',
                'type'    => 'boolean',
                'options' => [
                    'default' => false
                ]
            ],
            [
                'name'    => 'status_created',
                'type'    => 'boolean',
                'options' => [
                    'default' => false
                ]
            ],
            [
                'name' => 'member_since',
                'type' => 'date'
            ],
            [
                'name' => 'membership_end_date',
                'type' => 'date'
            ],
            [
                'name' => 'monthly_dues',
                'type' => 'decimal'
            ],
            [
                'name' => 'dues_paid_though',
                'type' => 'date'
            ],
            [
                'name' => 'z_locals_id',
                'type' => 'integer'
            ],
            [
                'name' => 'memo',
                'type' => 'text'
            ],
            [
                'name' => 'created_by',
                'type' => 'integer'
            ]
        ];

        $util->setOptions($options);

        $util->run();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
