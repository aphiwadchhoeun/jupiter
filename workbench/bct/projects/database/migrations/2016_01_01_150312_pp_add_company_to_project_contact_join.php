<?php

use Illuminate\Database\Migrations\Migration;

class PpAddCompanyToProjectContactJoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_project_contact_join');

        $options = [
            [
                'name' => 'company',
                'type' => 'string',
                'options' => [
                    'nullable' => true,
                ]
            ]

        ];
        $util->setOptions($options);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
