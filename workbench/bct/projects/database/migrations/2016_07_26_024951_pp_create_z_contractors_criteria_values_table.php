<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateZContractorsCriteriaValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_contractors_criteria_values');

        $options = [
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name' => 'z_members_id',
                'type' => 'integer',
                'options' => [
                    'unsigned' => true
                ]
            ],
            [
                'name' => 'z_criteria_values_id',
                'type' => 'integer',
                'options' => [
                    'unsigned' => true
                ]
            ],
            [
                'name' => 'end_date',
                'type' => 'date',
            ],
            [
                'name'    => 'status',
                'type'    => 'boolean',
                'options' => [
                    'unsigned' => true,
                    'default' => 1
                ]
            ],
        ];

        $util->setOptions($options);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
