<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewPpProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_projects` AS
            select 
                `z_projects`.`id`, 
                `z_projects`.`name`, 
                IF(char_length(`z_projects`.`name`) > 30, CONCAT(SUBSTR(`z_projects`.`name`, 1, 30), "..."), `z_projects`.`name`) as name_short,
                `z_projects`.`city`, 
                `z_projects`.`region_abrv`, 
                `z_projects`.`follow_up_date`, 
                DATE_FORMAT(z_projects.follow_up_date, \'%b-%d-%Y\') as follow_up_date_custom,
                GROUP_CONCAT(DISTINCT tags.name) as tags, 
                z_project_status.name as status, 
                GROUP_CONCAT(DISTINCT `z_project_stage`.`name`) as stages
                
            from `z_projects` 
                left join `z_project_status` on `z_project_status`.`id` = `z_projects`.`z_project_status_id` 
                left join `tagged` on `z_projects`.`id` = `tagged`.`taggable_id` and `tagged`.`taggable_type` = "Bct\\\\Projects\\\\Models\\\\Project" 
                left join `tags` on `tagged`.`tag_id` = `tags`.`id` 
                left join `z_project_visit` as `visit1` on `visit1`.`z_projects_id` = `z_projects`.`id` 
                left join `z_project_visit` as `visit2` on `visit2`.`z_projects_id` = `z_projects`.`id` and `visit1`.`id` < `visit2`.`id` 
                left join `z_project_visit_stage_join` on `z_project_visit_stage_join`.`z_project_visit_id` = `visit1`.`id` 
                left join `z_project_stage` on `z_project_visit_stage_join`.`z_project_stage_id` = `z_project_stage`.`id` 
            
            where 
                `z_projects`.`deleted_at` is null 
                    and 
                `visit2`.`id` is null 
            group by `z_projects`.`id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
