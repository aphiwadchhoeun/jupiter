<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Aphiwad updated join clause:
        // If associated project was deleted, it makes sense to not show it for review
        // but it's ok if lost reference to some of the users (in case of users got deleted)
        DB::statement('CREATE OR REPLACE VIEW `pp_reviews` AS
            SELECT
                `z_project_visit`.`id` as `id`,
                `z_project_visit`.`details` as `details`,
                `z_project_visit`.`reason` as `reason`,
                `z_project_visit`.`weather` as `weather`,
                `z_project_visit`.`workers_count` as `workers_count`,
                `z_project_visit`.`approved` as `approved`,
                `z_project_visit`.`created_at` as `created_at`,
                `z_projects`.`id` as `project_id`,
                `z_projects`.`region_abrv` as `region`,
                `z_projects`.`name` as `project_name`,
                GROUP_CONCAT(CONCAT_WS(\' \', users.first_name, users.last_name)) as users_names
            FROM
                `z_project_visit`

                JOIN `z_projects` on `z_projects`.`id` = `z_project_visit`.`z_projects_id`
                JOIN `z_project_visit_user_id_join` on `z_project_visit_user_id_join`.`z_project_visit_id` = `z_project_visit`.`id`
                LEFT JOIN `users` on `users`.`id` = `z_project_visit_user_id_join`.`user_id`

            WHERE
                `z_project_visit`.`deleted_at` is null
            GROUP BY `z_project_visit`.`id`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
