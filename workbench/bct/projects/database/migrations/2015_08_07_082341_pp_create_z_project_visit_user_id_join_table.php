<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectVisitUserIdJoinTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_project_visit_user_id_join');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'z_project_visit_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'user_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();

        // check if the key name exists before create
        $sm = Schema::getConnection()->getDoctrineSchemaManager();
        if (!array_has($sm->listTableIndexes('z_project_visit_user_id_join'), 'visit_user_index')) {
            Schema::table('z_project_visit_user_id_join', function ($table) {
                $table->index(['z_project_visit_id', 'user_id'], 'visit_user_index');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
