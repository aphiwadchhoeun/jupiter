<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectContactJoinTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_project_contact_join');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'z_projects_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_contact_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_contact_type_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_projects_temp_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
