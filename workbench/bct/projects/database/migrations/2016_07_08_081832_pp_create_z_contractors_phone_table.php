<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateZContractorsPhoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_contractor_phone');
        $util->setOptions([
            [
                'name'    => 'id',
                'type'    => 'increments',
                'options' => [
                    'unique' => true
                ]
            ],
            [
                'name' => 'z_contractors_id',
                'type' => 'integer',
                'options' => [
                    'unsigned' => true
                ]
            ],
            [
                'name' => 'phone',
                'type' => 'string',
            ],
            [
                'name' => 'type',
                'type' => 'string',
            ]
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
