<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateViewReportActiveServiceJobs extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('CREATE OR REPLACE VIEW `pp_report_active_service_jobs` AS
           SELECT 
                `p`.`id` AS `id`,
                `p`.`name` AS `name`,
                `p`.`region_abrv` AS `region`,
                `p`.`address` AS `address`,
                `p`.`city` AS `city`,
                (SELECT 
                        GROUP_CONCAT(`z_contractors`.`name`
                                SEPARATOR \' | \')
                    FROM
                        (((`z_project_contractor_join`
                        JOIN `z_contractors` ON ((`z_project_contractor_join`.`z_contractors_id` = `z_contractors`.`id`)))
                        JOIN `z_scopes` ON ((`z_project_contractor_join`.`z_scopes_id` = `z_scopes`.`id`)))
                        JOIN `z_projects` ON ((`z_project_contractor_join`.`z_projects_id` = `z_projects`.`id`)))
                    WHERE
                        ((`z_projects`.`z_project_status_id` = 1)
                            AND (`z_scopes`.`name` = \'GC\')
                            AND ((`z_project_contractor_join`.`is_union` = 1)
                            OR (`z_project_contractor_join`.`is_agreement` = 1))
                            AND (`z_project_contractor_join`.`z_projects_id` = `p`.`id`)
                            AND ISNULL(`z_projects`.`deleted_at`))
                    GROUP BY `z_project_contractor_join`.`z_projects_id`) AS `contractors`,
                (SELECT 
                        GROUP_CONCAT(IF((CONCAT(`z_contact`.`first_name`,
                                            \' \',
                                            `z_contact`.`last_name`) = \'\'),
                                    `z_contact`.`company`,
                                    CONCAT(`z_contact`.`first_name`,
                                            \' \',
                                            `z_contact`.`last_name`))
                                SEPARATOR \' | \')
                    FROM
                        ((`z_project_contact_join`
                        JOIN `z_contact` ON ((`z_project_contact_join`.`z_contact_id` = `z_contact`.`id`)))
                        JOIN `z_projects` ON ((`z_project_contact_join`.`z_projects_id` = `z_projects`.`id`)))
                    WHERE
                        ((`z_projects`.`z_project_status_id` = 1)
                            AND (`z_project_contact_join`.`z_contact_type_id` = 7)
                            AND (`z_project_contact_join`.`z_projects_id` = `p`.`id`)
                            AND ISNULL(`z_projects`.`deleted_at`))
                    GROUP BY `z_project_contact_join`.`z_projects_id`) AS `stewards`
            FROM
                ((`z_projects` `p`
                    JOIN `z_project_contractor_join` `pc` ON ((`p`.`id` = `pc`.`z_projects_id`)))
                    JOIN `z_scopes` `s` ON ((`pc`.`z_scopes_id` = `s`.`id`)))
            WHERE
                (ISNULL(`p`.`deleted_at`)
                    AND (`p`.`z_project_status_id` = 1)
                    AND (`s`.`name` = \'GC\')
                    AND ((`pc`.`is_union` = 1)
                    OR (`pc`.`is_agreement` = 1)))
            GROUP BY `p`.`id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
