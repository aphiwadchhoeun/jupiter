<?php
use Illuminate\Database\Migrations\Migration;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:48 PM
 */


class PpCreateViewSummaryProject extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('CREATE OR REPLACE VIEW pp_summary_profile AS
                SELECT
                    `z_projects`.`id` AS `id`,
                    `z_projects`.`name` AS `name`,
                    `z_projects`.`description` AS `description`,
                    `z_projects`.`address` AS `address`,
                    `z_projects`.`county` AS `county`,
                    `z_projects`.`city` AS `city`,
                    `z_projects`.`state` AS `state`,
                    `z_projects`.`zipcode` AS `zipcode`,
                    `z_projects`.`region` AS `region`,
                    `z_projects`.`lat` AS `lat`,
                    `z_projects`.`lng` AS `lng`,
                    `z_projects`.`follow_up_date` AS `follow_up_date`,
                    `z_project_status`.`name` AS `status`,
                    `z_projects`.`created_at` AS `created_at`,
                    `z_projects`.`updated_at` AS `updated_at`,
                    `z_projects`.`deleted_at` AS `deleted_at`,
                    `z_project_list_project_join`.`is_active`

                FROM
                    (`z_projects`
                    LEFT JOIN `z_project_status` ON ((`z_projects`.`z_project_status_id` = `z_project_status`.`id`)))
                    LEFT JOIN `z_project_list_project_join` on `z_project_list_project_join`.`z_projects_id` = `z_projects`.`id`
                GROUP BY `z_projects`.`id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}