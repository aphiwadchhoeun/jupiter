<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateZLocalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_locals');

        $options = [
            [
                'name' => 'id',
                'type' => 'increments'
            ],
            [
                'name' => 'name',
                'type' => 'string',
            ],
            [
                'name' => 'email',
                'type' => 'string',
            ],
            [
                'name' => 'address',
                'type' => 'string',
            ],
            [
                'name' => 'city',
                'type' => 'string',
            ],
            [
                'name' => 'state',
                'type' => 'string',
            ],
            [
                'name' => 'zipcode',
                'type' => 'string',
            ],
            [
                'name' => 'phone',
                'type' => 'string',
            ],
            [
                'name' => 'alt_phone',
                'type' => 'string',
            ],
            [
                'name' => 'fax',
                'type' => 'string',
            ],
            [
                'name' => 'website',
                'type' => 'string',
            ],
            [
                'name' => 'z_dp_regions_id',
                'type' => 'integer',
                'options' => [
                    'unsigned' => true
                ]
            ]
        ];

        $util->setOptions($options);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
