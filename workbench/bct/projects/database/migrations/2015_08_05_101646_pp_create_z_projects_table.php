<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_projects');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'name',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'address',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'city',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'state',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'zipcode',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'region',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'region_abrv',
                'type'    => 'string',
                'options' => [
                    'nullable' => true,
                    'length'   => 8,
                ]
            ],
            [
                'name'    => 'area',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'county',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_types_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'description',
                'type'    => 'text',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_prevailing_wage_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_reference_type_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'reference_details',
                'type'    => 'text',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_values_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'num_floor',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'lat',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'lng',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'budgetgroup',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'is_class_a',
                'type'    => 'boolean',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_visibility_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_status_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_classification_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_project_stage_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'follow_up_date',
                'type'    => 'date',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'union_value',
                'type'    => 'decimal',
                'options' => [
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

}
