<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectListProjectJoinTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_project_list_project_join');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'z_project_list_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_projects_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'created_by',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'is_active',
                'type'    => 'boolean',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();

        $sm = Schema::getConnection()->getDoctrineSchemaManager();
        if (!array_has($sm->listTableIndexes('z_project_list_project_join'), 'project_list_index')) {
            Schema::table('z_project_list_project_join', function ($table) {
                $table->index(['z_projects_id'], 'project_list_index');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
