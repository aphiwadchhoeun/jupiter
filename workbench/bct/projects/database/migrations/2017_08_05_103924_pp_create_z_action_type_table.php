<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZActionTypeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_action_type');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'name',
                'type'    => 'string',
                'options' => [
                    'default'  => 'n/a',
                    'nullable' => true,
                ]
            ]
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
