<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectVisitTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_project_visit');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'details',
                'type'    => 'text',
                'options' => [
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_projects_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'approved',
                'type'    => 'boolean',
                'options' => [
                    'default'  => 0,
                ]
            ],
            [
                'name'    => 'approved_user_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
