<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateViewProjectVisits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `pp_project_visits` AS
            SELECT
                `z_project_visit`.*,
                GROUP_CONCAT( DISTINCT CONCAT_WS(\' \', users.first_name, users.last_name) SEPARATOR \', \' ) as users_names,
                GROUP_CONCAT( DISTINCT z_project_stage.name SEPARATOR \', \') as stages_names,
                `z_project_visit_user_id_join`.`user_id` as created_by
            FROM `z_project_visit`
            LEFT JOIN `z_project_visit_user_id_join` on `z_project_visit_user_id_join`.`z_project_visit_id` = `z_project_visit`.`id`
            LEFT JOIN `users` on `users`.`id` = `z_project_visit_user_id_join`.`user_id`
            LEFT JOIN `z_project_visit_stage_join` on `z_project_visit_stage_join`.`z_project_visit_id` = `z_project_visit`.`id`
            LEFT JOIN `z_project_stage` on `z_project_stage`.`id` = `z_project_visit_stage_join`.`z_project_stage_id`
            WHERE
                `z_project_visit`.`deleted_at` is null
            GROUP BY `z_project_visit`.`id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
