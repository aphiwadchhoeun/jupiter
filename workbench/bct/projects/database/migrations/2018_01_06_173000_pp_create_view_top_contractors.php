<?php
use Illuminate\Database\Migrations\Migration;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:48 PM
 */


class PpCreateViewTopContractors extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('CREATE OR REPLACE VIEW `pp_top_contractors` AS
            SELECT 
                `z_contractors`.`id` AS `id`,
                `z_contractors`.`name` AS `contractor_name`,
                `z_projects`.`region_abrv` AS `region_abrv`,
                `z_projects`.`z_project_status_id` AS `project_status_id`,
                `z_scopes`.`name` AS `scope`,
                `z_project_contractor_join`.`is_union` AS `is_union`,
                SUM(`z_scopes_classifications_join`.`value`) AS `value`
            FROM
                ((((`z_contractors`
                JOIN `z_projects`)
                JOIN `z_project_contractor_join`)
                JOIN `z_scopes_classifications_join`)
                JOIN `z_scopes`)
            WHERE
                ((`z_contractors`.`id` = `z_project_contractor_join`.`z_contractors_id`)
                    AND (`z_projects`.`id` = `z_project_contractor_join`.`z_projects_id`)
                    AND (`z_scopes_classifications_join`.`z_scopes_id` = `z_project_contractor_join`.`z_scopes_id`)
                    AND (`z_projects`.`z_project_classification_id` = `z_scopes_classifications_join`.`z_classifications_id`)
                    AND (`z_scopes`.`id` = `z_scopes_classifications_join`.`z_scopes_id`)
                    AND ISNULL(`z_projects`.`deleted_at`)
                    AND ISNULL(`z_contractors`.`deleted_at`))
            GROUP BY `z_project_contractor_join`.`z_contractors_id` , `z_project_contractor_join`.`is_union` , `z_projects`.`region_abrv`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}