<?php

use Illuminate\Database\Migrations\Migration;

class PpCreateZUserGroupJoinTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_user_group_join');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'user_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_projects_groups_id',
                'type'    => 'integer',
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}
