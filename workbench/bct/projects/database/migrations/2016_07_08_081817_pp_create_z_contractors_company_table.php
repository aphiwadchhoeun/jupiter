<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateZContractorsCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_contractor_company');
        $util->setOptions([
            [
                'name'    => 'id',
                'type'    => 'increments',
                'options' => [
                    'unique' => true
                ]
            ],
            [
                'name' => 'z_contractors_id',
                'type' => 'integer',
                'options' => [
                    'unsigned' => true
                ]
            ],
            [
                'name' => 'company',
                'type' => 'string',
            ],
            [
                'name' => 'type',
                'type' => 'string',
            ]
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
