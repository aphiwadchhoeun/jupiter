<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateZProjectMemberJoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_project_member_join');

        $util->setOptions([
            [
                'name'   => 'id',
                'type'   => 'increments',
                'unique' => true,
            ],
            [
                'name'    => 'z_projects_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_members_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'z_scopes_id',
                'type'    => 'integer',
                'index'   => true,
                'options' => [
                    'default'  => 0,
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'number_of_hours',
                'type'    => 'decimal',
                'options' => [
                    'default'  => '0.00',
                    'nullable' => true,
                ]
            ],
            [
                'name'    => 'wage',
                'type'    => 'decimal',
                'options' => [
                    'nullable' => true,
                ]
            ],
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
