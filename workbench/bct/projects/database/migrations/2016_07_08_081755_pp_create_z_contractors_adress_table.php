<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PpCreateZContractorsAdressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $util = app()->make('bct.unionimpactbase.migrationutil');

        $util->setTableName('z_contractor_address');
        $util->setOptions([
            [
                'name'    => 'id',
                'type'    => 'increments',
                'options' => [
                    'unique' => true
                ]
            ],
            [
                'name' => 'z_contractors_id',
                'type' => 'integer',
                'options' => [
                    'unsigned' => true
                ]
            ],
            [
                'name' => 'address',
                'type' => 'string',
            ],
            [
                'name' => 'city',
                'type' => 'string',
            ],
            [
                'name' => 'state',
                'type' => 'string',
            ],
            [
                'name' => 'zipcode',
                'type' => 'string',
            ],
            [   'name' => 'type',
                'type' => 'string'
            ]
        ]);

        $util->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
