<?php
use Illuminate\Database\Migrations\Migration;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:48 PM
 */


class PpCreateViewMarketShare extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('CREATE OR REPLACE VIEW `pp_market_share` AS
            SELECT 
                `p`.`id` AS `id`,
                `p`.`name` AS `name`,
                `p`.`region_abrv` AS `region`,
                `s`.`name` AS `scope`,
                `sc`.`value` AS `value`,
                `pc`.`is_union` AS `is_union`,
                `pc`.`is_agreement` AS `is_agreement`
            FROM
                (((`z_projects` `p`
                JOIN `z_project_contractor_join` `pc`)
                JOIN `z_scopes` `s`)
                JOIN `z_scopes_classifications_join` `sc`)
            WHERE
                ((`p`.`id` = `pc`.`z_projects_id`)
                    AND (`pc`.`z_scopes_id` = `s`.`id`)
                    AND (`p`.`z_project_status_id` = 1)
                    AND (`sc`.`z_scopes_id` = `s`.`id`)
                    AND (`sc`.`z_classifications_id` = `p`.`z_project_classification_id`)
                    AND ISNULL(`p`.`deleted_at`))');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}