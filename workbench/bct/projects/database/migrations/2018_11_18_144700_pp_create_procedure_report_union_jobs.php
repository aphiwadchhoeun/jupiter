<?php
use Illuminate\Database\Migrations\Migration;

/**
 * Created by PhpStorm.
 * User: Aphiwad
 * Date: 11/18/2015
 * Time: 2:48 PM
 */


class PpCreateProcedureReportUnionJobs extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $sql = <<<SQL
        DROP PROCEDURE  IF EXISTS pp_report_union_jobs;

		CREATE PROCEDURE `pp_report_union_jobs`(IN inlcude_non_union BOOLEAN, IN job_scopes TEXT, IN region VARCHAR(255))
        BEGIN
         
            SET @sql = CONCAT('SELECT 
                `z_projects`.`id` AS `project_id`,
                `z_projects`.`name` AS `project_name`,
                CONCAT(`z_projects`.`address`,
                        \', \',
                        `z_projects`.`city`,
                        \', \',
                        `z_projects`.`state`,
                        \' \',
                        `z_projects`.`zipcode`) AS `project_full_address`,
                `z_projects`.`region_abrv` AS `region_abrv`,
                (SELECT 
                        DATE_FORMAT(`z_project_visit`.`created_at`,
                                    \'%b %d, %Y\')
                    FROM
                        `z_project_visit`
                    WHERE
                        (`z_project_visit`.`z_projects_id` = `z_projects`.`id`)
                    ORDER BY `z_project_visit`.`created_at` DESC , `z_project_visit`.`id` DESC
                    LIMIT 1) AS `last_visit_date`,
                `z_project_values`.`value` AS `value`,
                (SELECT 
                        GROUP_CONCAT(DISTINCT `z_project_stage`.`name`
                                SEPARATOR \' / \')
                    FROM
                        (`z_project_stage`
                        LEFT JOIN `z_project_visit_stage_join` ON ((`z_project_visit_stage_join`.`z_project_stage_id` = `z_project_stage`.`id`)))
                    WHERE
                        (`z_project_visit_stage_join`.`z_project_visit_id` = (SELECT 
                                `z_project_visit`.`id`
                            FROM
                                `z_project_visit`
                            WHERE
                                (`z_project_visit`.`z_projects_id` = `z_projects`.`id`)
                            ORDER BY `z_project_visit`.`created_at` DESC , `z_project_visit`.`id` DESC
                            LIMIT 1))) AS `stages`,
                (SELECT 
                    GROUP_CONCAT(
                        CONCAT(`z_contractors`.`name`, \' - \', if(`z_project_contractor_join`.`is_union` or `z_project_contractor_join`.`is_agreement`, \'UNION\', \'NON-UNION\')) 
                        SEPARATOR \'!!\')
                    FROM
                        ((`z_contractors`
                        INNER JOIN `z_project_contractor_join` ON ((`z_contractors`.`id` = `z_project_contractor_join`.`z_contractors_id`)))
                        INNER JOIN `z_scopes` `scope` ON ((`z_project_contractor_join`.`z_scopes_id` = `scope`.`id`)))
                    WHERE
                        ((`z_project_contractor_join`.`z_projects_id` = `z_projects`.`id`)');
                        
            IF inlcude_non_union is FALSE then
                SET @sql = CONCAT(@sql, ' AND ((`z_project_contractor_join`.`is_union` = 1) OR (`z_project_contractor_join`.`is_agreement` = 1))');
            END IF;
                            
            SET @sql = CONCAT(@sql, ' AND (`scope`.`name` = \'GC\')
                            AND ISNULL(`z_contractors`.`deleted_at`))) AS `general_contractor`,
                (SELECT 
                        GROUP_CONCAT(DISTINCT CONCAT(`scope`.`name`,
                                        \': \',
                                        `z_contractors`.`name`)
                                SEPARATOR \'!!\')
                    FROM
                        ((`z_contractors`
                        INNER JOIN `z_project_contractor_join` ON ((`z_contractors`.`id` = `z_project_contractor_join`.`z_contractors_id`)))
                        INNER JOIN `z_scopes` `scope` ON ((`z_project_contractor_join`.`z_scopes_id` = `scope`.`id`)))
                    WHERE
                        ((`z_project_contractor_join`.`z_projects_id` = `z_projects`.`id`)
                            AND ((`z_project_contractor_join`.`is_union` = 1) OR (`z_project_contractor_join`.`is_agreement` = 1))');
                            
            IF job_scopes is not null then 
                SET @sql = CONCAT(@sql, ' AND (`scope`.`name` in (', job_scopes, ')) ');
            ELSE
                SET @sql = CONCAT(@sql, ' AND (`scope`.`name` <> \'GC\') ');
            END IF;				
                            
            SET @sql = CONCAT(@sql, 'AND ISNULL(`z_contractors`.`deleted_at`))) AS `others_contractors`
            FROM
                `z_projects`
                INNER JOIN `z_project_contractor_join` ON `z_project_contractor_join`.`z_projects_id` = `z_projects`.`id`
                LEFT JOIN `z_contractors` ON `z_project_contractor_join`.`z_contractors_id` = `z_contractors`.`id`
                LEFT JOIN `z_project_values` ON `z_project_values`.`id` = `z_projects`.`z_project_values_id`
                INNER JOIN `z_scopes` ON `z_project_contractor_join`.`z_scopes_id` = `z_scopes`.`id`
            WHERE
                (ISNULL(`z_projects`.`deleted_at`)
                    AND ((SELECT COUNT(0)
                        FROM
                            ((`z_contractors`
                            INNER JOIN `z_project_contractor_join` ON ((`z_contractors`.`id` = `z_project_contractor_join`.`z_contractors_id`)))
                            INNER JOIN `z_scopes` `scope` ON ((`z_project_contractor_join`.`z_scopes_id` = `scope`.`id`)))
                        WHERE
                            ((`z_project_contractor_join`.`z_projects_id` = `z_projects`.`id`)');
                            
            IF job_scopes is not null then 
                SET @sql = CONCAT(@sql, ' AND ((`z_project_contractor_join`.`is_union` = 1) OR (`z_project_contractor_join`.`is_agreement` = 1))
                            AND `scope`.`name` in (', job_scopes, ')');
            END IF;
                            
            SET @sql = CONCAT(@sql, ' AND ISNULL(`z_contractors`.`deleted_at`))) >= 1)
                    AND ((SELECT COUNT(0)
                        FROM
                            `z_project_status`
                        WHERE
                            ((`z_projects`.`z_project_status_id` = `z_project_status`.`id`)
                                AND (`z_project_status`.`name` = \'Active\'))) >= 1))
                    AND region_abrv = \'', region, '\'');
                    
            IF inlcude_non_union is FALSE then
                SET @sql = CONCAT(@sql, ' AND (`z_scopes`.`name` = \'GC\' AND (`z_project_contractor_join`.`is_union` = 1) OR (`z_project_contractor_join`.`is_agreement` = 1))');
            END IF; 
            
            SET @sql = CONCAT(@sql, 'GROUP BY `z_projects`.`id`
            ORDER BY `z_projects`.`city` ASC;');
            
            PREPARE stmt FROM @sql;
            
            EXECUTE stmt;
            
            DEALLOCATE PREPARE stmt;
         
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }

}