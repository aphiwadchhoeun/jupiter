<?php namespace Bct\Projects\Database\Seeds;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 25.01.16
 * Time: 12:07
 */

use Bct\Projects\Models\Config;
use DB;
use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder {

    public function run() {
        $dp_config = [
            [
                'item'  => 'bct.projects.settings.map_radius',
                'value' => '0.25'
            ],
            [
                'item'  => 'bct.projects.settings.map_week_start',
                'value' => '4'
            ],
            [
                'item'  => 'bct.projects.settings.map_week_end',
                'value' => '8'
            ],
            [
                'item'  => 'bct.projects.settings.map_union_start',
                'value' => '50'
            ],
            [
                'item'  => 'bct.projects.settings.map_union_end',
                'value' => '75'
            ],
            [
                'item'  => 'bct.projects.settings.map_count_projects',
                'value' => '250'
            ],
            [
                'item'  => 'bct.projects.settings.map_less_4',
                'value' => 'green'
            ],
            [
                'item'  => 'bct.projects.settings.map_4_8',
                'value' => 'yellow'
            ],
            [
                'item'  => 'bct.projects.settings.map_more_8',
                'value' => 'red'
            ],
            [
                'item'  => 'bct.projects.settings.map_union_1',
                'value' => 'sad'
            ],
            [
                'item'  => 'bct.projects.settings.map_union_2',
                'value' => 'neutral'
            ],
            [
                'item'  => 'bct.projects.settings.map_union_3',
                'value' => 'smile'
            ],
            [
                'item'  => 'bct.projects.settings.enable_project_visit_workers',
                'value' => 0
            ],
            [
                'item'  => 'bct.projects.settings.enable_project_visit_weather',
                'value' => 0
            ],
            [
                'item'  => 'bct.projects.settings.enable_project_visit_date',
                'value' => 0
            ]
        ];

        if ($dp_config && is_array($dp_config)) {
            foreach ($dp_config as $config) {

                /*
                 * Loop through configs array from above to create config item in db
                 * First check if the item with the same key exists. If yes, update its value.
                 * If no, create a new entry item.
                 */
                if ($db_config = Config::findByItem($config['item'])->first()) {
                    $db_config->update([
                        'value' => $config['value']
                    ]);
                } else {
                    $db_config = Config::firstOrNew([
                        'item'  => $config['item'],
                        'value' => $config['value'],
                    ]);
                    $db_config->save();
                }
            }
        }
    }
}