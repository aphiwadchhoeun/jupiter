<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsValueTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_project_values');

        $util->setKeyColumns(['value']);

        $util->setData([
            ['value' => '$0 - $100K'],
            ['value' => '$101K - $200K'],
            ['value' => '$201K - $300K'],
            ['value' => '$301K - $400K'],
            ['value' => '$401K - $500K'],
            ['value' => '$501K - $750K'],
            ['value' => '$751K - $1M'],
            ['value' => '$1M - $2M'],
            ['value' => '$2M - $5M'],
            ['value' => '$5M - $10M'],
            ['value' => '$10M - $15M'],
            ['value' => '$15M - $20M'],
            ['value' => 'Greater Than $20M'],
        ]);

        $util->seed();
	}

}
