<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsScopeTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_scopes');

        $util->setKeyColumns(['name', 'classification']);

        $util->setData([

            ['name' => 'GC', 'display_name' => 'GC', 'craft' => null, 'value' => 1],
            ['name' => 'Abatement', 'display_name' => 'Abatement', 'craft' => null, 'value' => 1],
            ['name' => 'Acoustical', 'display_name' => 'Acoustical', 'craft' => null, 'value' => 1],
            ['name' => 'Bridges', 'display_name' => 'Bridges', 'craft' => null, 'value' => 1],
            ['name' => 'Cabinet Installation', 'display_name' => 'Cabinet Installation', 'craft' => null, 'value' => 1],
            ['name' => 'CleanRoom', 'display_name' => 'CleanRoom', 'craft' => null, 'value' => 1],
            ['name' => 'Commercial Fixtures', 'display_name' => 'Commercial Fixtures', 'craft' => null, 'value' => 1],
            ['name' => 'Concrete - Barriers/Sound Walls', 'display_name' => 'Concrete - Barriers/Sound Walls', 'craft' => null, 'value' => 1],
            ['name' => 'Concrete - Flatwork', 'display_name' => 'Concrete - Flatwork', 'craft' => null, 'value' => 1],
            ['name' => 'Concrete - Horizontal Decks', 'display_name' => 'Concrete - Horizontal Decks', 'craft' => null, 'value' => 1],
            ['name' => 'Concrete - Tiltups', 'display_name' => 'Concrete - Tiltups', 'craft' => null, 'value' => 1],
            ['name' => 'Concrete - VerticalConcrete', 'display_name' => 'Concrete - VerticalConcrete', 'craft' => null, 'value' => 1],
            ['name' => 'Demolition', 'display_name' => 'Demolition', 'craft' => null, 'value' => 1],
            ['name' => 'Demountable Partitions & Furniture', 'display_name' => 'Demountable Partitions & Furniture', 'craft' => null, 'value' => 1],
            ['name' => 'Diving/Hyberbaric', 'display_name' => 'Diving/Hyberbaric', 'craft' => null, 'value' => 1],
            ['name' => 'Doors & Hardware', 'display_name' => 'Doors & Hardware', 'craft' => null, 'value' => 1],
            ['name' => 'Drywall', 'display_name' => 'Drywall', 'craft' => null, 'value' => 1],
            ['name' => 'Firestopping & Expansion Joints', 'display_name' => 'Firestopping & Expansion Joints', 'craft' => null, 'value' => 1],
            ['name' => 'Flooring - Carpet', 'display_name' => 'Flooring - Carpet', 'craft' => null, 'value' => 1],
            ['name' => 'Flooring - Hard Surface', 'display_name' => 'Flooring - Hard Surface', 'craft' => null, 'value' => 1],
            ['name' => 'Flooring - Hardwood', 'display_name' => 'Flooring - Hardwood', 'craft' => null, 'value' => 1],
            ['name' => 'Flooring - Pedestal', 'display_name' => 'Flooring - Pedestal', 'craft' => null, 'value' => 1],
            ['name' => 'Flooring - Resilient', 'display_name' => 'Flooring - Resilient', 'craft' => null, 'value' => 1],
            ['name' => 'Flooring - Tile', 'display_name' => 'Flooring - Tile', 'craft' => null, 'value' => 1],
            ['name' => 'Insulation', 'display_name' => 'Insulation', 'craft' => null, 'value' => 1],
            ['name' => 'Lath/Plaster/EIFS', 'display_name' => 'Lath/Plaster/EIFS', 'craft' => null, 'value' => 1],
            ['name' => 'Marine Construction', 'display_name' => 'Marine Construction', 'craft' => null, 'value' => 1],
            ['name' => 'Medical Best Practices', 'display_name' => 'Medical Best Practices', 'craft' => null, 'value' => 1],
            ['name' => 'Metal Framing', 'display_name' => 'Metal Framing', 'craft' => null, 'value' => 1],
            ['name' => 'Metal Stud/Framing', 'display_name' => 'Metal Stud/Framing', 'craft' => null, 'value' => 1],
            ['name' => 'Millwork Installation', 'display_name' => 'Millwork Installation', 'craft' => null, 'value' => 1],
            ['name' => 'Millwright', 'display_name' => 'Millwright', 'craft' => null, 'value' => 1],
            ['name' => 'PileDriving', 'display_name' => 'PileDriving', 'craft' => null, 'value' => 1],
            ['name' => 'Roof Structure', 'display_name' => 'Roof Structure', 'craft' => null, 'value' => 1],
            ['name' => 'Roofing', 'display_name' => 'Roofing', 'craft' => null, 'value' => 1],
            ['name' => 'Scaffold', 'display_name' => 'Scaffold', 'craft' => null, 'value' => 1],
            ['name' => 'ShipWright', 'display_name' => 'ShipWright', 'craft' => null, 'value' => 1],
            ['name' => 'Shoring', 'display_name' => 'Shoring', 'craft' => null, 'value' => 1],
            ['name' => 'Siding', 'display_name' => 'Siding', 'craft' => null, 'value' => 1],
            ['name' => 'Structural Insulated Panels', 'display_name' => 'Structural Insulated Panels', 'craft' => null, 'value' => 1],
            ['name' => 'Structural Steel', 'display_name' => 'Structural Steel', 'craft' => null, 'value' => 1],
            ['name' => 'Supervision', 'display_name' => 'Supervision', 'craft' => null, 'value' => 1],
            ['name' => 'Taping & Painting', 'display_name' => 'Taping & Painting', 'craft' => null, 'value' => 1],
            ['name' => 'TradeShow', 'display_name' => 'TradeShow', 'craft' => null, 'value' => 1],
            ['name' => 'Wood Framing', 'display_name' => 'Wood Framing', 'craft' => null, 'value' => 1],

        ]);

        $util->seed();

    }

}
