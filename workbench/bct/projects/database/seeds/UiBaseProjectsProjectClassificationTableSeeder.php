<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsProjectClassificationTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_project_classification');

        $util->setKeyColumns(['name']);

        $util->setData([
            ['name' => 'Commercial'],
            ['name' => 'Industrial'],
            ['name' => 'Heavy & HWY'],
            ['name' => 'Mixed Use'],
            ['name' => 'TI'],
            ['name' => 'Bridge'],
            ['name' => 'Remodel'],
            ['name' => 'Residential'],
            ['name' => 'Tradeshow']
        ]);

        $util->seed();

    }

}
