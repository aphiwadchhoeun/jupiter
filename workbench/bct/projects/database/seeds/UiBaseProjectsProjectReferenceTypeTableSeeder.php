<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsProjectReferenceTypeTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_project_reference_type');

        $util->setKeyColumns(['name']);

        $util->setData([
            ['name' => 'Permit'],
            ['name' => 'Drive-By'],
            ['name' => 'MRP'],
            ['name' => 'Other'],
        ]);

        $util->seed();

    }

}
