<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UiBaseProjectsContactTypeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_contact_type');

        $util->setKeyColumns(['name']);

        $util->setData([
            ['name' => 'Architect'],
            ['name' => 'Developer'],
            ['name' => 'Leasing Agent'],
            ['name' => 'Owner'],
            ['name' => 'Property Mgr'],
            ['name' => 'Tenant'],
            ['name' => 'Steward'],
            ['name' => 'Superintendent'],
            ['name' => 'Foreman'],
            ['name' => 'Other'],
        ]);

        $util->seed();

	}

}
