<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class ProjectsTypeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_project_types');

        $util->setKeyColumns(['type']);

        $util->setData([
            ['type' => 'Bridge'],
            ['type' => 'High Rise'],
            ['type' => 'Hospital'],
            ['type' => 'Mall'],
            ['type' => 'Marine'],
            ['type' => 'Millwright'],
            ['type' => 'Modular'],
            ['type' => 'New Construction'],
            ['type' => 'Parking Structure'],
            ['type' => 'Renewable Energy'],
            ['type' => 'School'],
            ['type' => 'TILT'],
            ['type' => 'Tribal Work'],
            ['type' => 'Water Treatment'],
            ['type' => 'Acoustical Ceiling Install'],
            ['type' => 'Arizona Heavy Highway'],
            ['type' => 'Breakwater'],
            ['type' => 'Pedestrian Bridge'],
            ['type' => 'Cabinet Shop'],
            ['type' => 'Cabinet Installation'],
            ['type' => 'Caltrans'],
            ['type' => 'Concrete Box Culverts'],
            ['type' => 'Concrete Bridge'],
            ['type' => 'Concrete Form Work'],
            ['type' => 'Comm Frame on Steel Structure'],
            ['type' => 'Commercial Framing Wood'],
            ['type' => 'Concrete High Rise'],
            ['type' => 'Installation of Cooler Boxes'],
            ['type' => 'Computer Flooring'],
            ['type' => 'Concrete Parking Structure'],
            ['type' => 'CRA (Los Angeles)'],
            ['type' => 'Concrete Column Retrofit - Bridges'],
            ['type' => 'Concrete on Steel Structure'],
            ['type' => 'Concrete Stm Drains/Catch Basins'],
            ['type' => 'Concrete Site Work'],
            ['type' => 'Concrete Tanks'],
            ['type' => 'Comm Wood Flooring'],
            ['type' => 'Concrete Dock'],
            ['type' => 'Demolition'],
            ['type' => 'Drywall - Metal Stud Framing'],
            ['type' => 'Engineering Work'],
            ['type' => 'Earthquake Retrofit of Building'],
            ['type' => 'Commercial Finish Work'],
            ['type' => 'Fixture Work'],
            ['type' => 'General Commercial Const'],
            ['type' => 'Grading'],
            ['type' => 'General Remodels of Bldgs'],
            ['type' => 'Hazardous Materials Job'],
            ['type' => 'Housing Authority'],
            ['type' => 'Heavy Timber Bridge'],
            ['type' => 'Housing & Urban Dev.t Agy'],
            ['type' => 'Housing'],
            ['type' => 'LA County/Non-Housing'],
            ['type' => 'Paper and Wire Lathing'],
            ['type' => 'Lumberyard'],
            ['type' => 'LADC Bas & Fin Secretaries'],
            ['type' => 'Modular Office Systems'],
            ['type' => 'MTA'],
            ['type' => 'Metal Framing'],
            ['type' => 'Off-Shore Rig'],
            ['type' => 'Public Works - Class-A'],
            ['type' => 'Pre-Cast Erection'],
            ['type' => 'Pour in Place Concrete'],
            ['type' => 'Piles'],
            ['type' => 'Panelized Roof Structures'],
            ['type' => 'Pier'],
            ['type' => 'Housing Finish Work'],
            ['type' => 'Comm Roof Structure'],
            ['type' => 'Scaffolding'],
            ['type' => 'Shoring and Lagging'],
            ['type' => 'Shoring and Scaffolding'],
            ['type' => 'Siesmic Work'],
            ['type' => 'Store Remodels - Grocery'],
            ['type' => 'Tile Setting and Finishing'],
            ['type' => 'Tilt Up'],
            ['type' => 'Tenant Improvement'],
            ['type' => 'Trolley Extension'],
            ['type' => 'Terrazzo Install (12 So Counties)'],
            ['type' => 'Wood Framing'],
            ['type' => 'Welding'],
            ['type' => 'Water Reservoir Structure'],
            ['type' => 'Wharf'],
            ['type' => 'Water Treatment Plant'],
            ['type' => 'Lathers'],
            ['type' => 'Plastering'],
            ['type' => 'Cooling Tower'],
            ['type' => 'Insulation: Commercial'],
            ['type' => 'Drywall Finishing'],
            ['type' => 'Commercial'],
            ['type' => 'Industrial'],
            ['type' => 'Heavy & Highway'],
            ['type' => 'Mixed Use'],
            ['type' => 'Remodel'],
            ['type' => 'Residential'],
            ['type' => 'Envelope repair'],
        ]);

        $util->seed();
	}

}
