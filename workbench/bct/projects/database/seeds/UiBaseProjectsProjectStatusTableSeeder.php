<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsProjectStatusTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_project_status');

        $util->setKeyColumns(['name']);

        $util->setData([
            ['name' => 'Active'],
            ['name' => 'Disabled'],
            ['name' => 'Completed'],
        ]);

        $util->seed();
	}

}
