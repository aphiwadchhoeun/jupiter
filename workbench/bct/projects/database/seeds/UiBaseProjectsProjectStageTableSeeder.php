<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsProjectStageTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_project_stage');

        $util->setKeyColumns(['name']);

        $util->setData([
            ['name' => 'Planning', 'order' => 1],
            ['name' => 'Bidding', 'order' => 2],
            ['name' => 'Awarded', 'order' => 3],
            ['name' => 'Abatement', 'order' => 4],
            ['name' => 'Demolition', 'order' => 5],
            ['name' => 'Site Work', 'order' => 6],
            ['name' => 'Piling', 'order' => 8],
            ['name' => 'Footing', 'order' => 9],
            ['name' => 'Slab', 'order' => 10],
            ['name' => 'Concrete Decks', 'order' => 11],
            ['name' => 'Walls', 'order' => 12],
            ['name' => 'Roof', 'order' => 13],
            ['name' => 'Frame-Wood', 'order' => 14],
            ['name' => 'Frame-Metal', 'order' => 15],
            ['name' => 'Drywall', 'order' => 16],
            ['name' => 'Taping', 'order' => 17],
            ['name' => 'Acoustical', 'order' => 18],
            ['name' => 'Flooring-Carpet', 'order' => 19],
            ['name' => 'Scaffolding', 'order' => 20],
            ['name' => 'Doors/Hardware', 'order' => 21],
            ['name' => 'Siding/Paint', 'order' => 23],
            ['name' => 'Cabinets', 'order' => 24],
            ['name' => 'Punch List', 'order' => 25],
            ['name' => 'General Notes', 'order' => 26],
            ['name' => 'Completed', 'order' => 27],
            ['name' => 'Insulation', 'order' => 7],
            ['name' => 'Millwork', 'order' => 22],
            ['name' => 'Disabled', 'order' => 28],
        ]);

        $util->seed();

    }

}
