<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsProjectVisabilityTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_project_visibility');

        $util->setKeyColumns(['name']);

        $util->setData([
            ['name' => 'High'],
            ['name' => 'Medium'],
            ['name' => 'Low'],
        ]);

        $util->seed();
	}

}
