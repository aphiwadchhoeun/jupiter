<?php

namespace Bct\Projects\Database\Seeds;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * {@inheritDoc}
     */
    public function run()
    {
        $this->call('Bct\Projects\Database\Seeds\ConfigTableSeeder');
        $this->call('Bct\Projects\Database\Seeds\StatesTableSeeder');
        $this->call('Bct\Projects\Database\Seeds\UiBaseProjectsContactTypeTableSeeder');
        $this->call('Bct\Projects\Database\Seeds\UiBaseProjectsPrevailingWageTableSeeder');
        $this->call('Bct\Projects\Database\Seeds\UiBaseProjectsProjectReferenceTypeTableSeeder');
        $this->call('Bct\Projects\Database\Seeds\UiBaseProjectsProjectStatusTableSeeder');
        $this->call('Bct\Projects\Database\Seeds\UiBaseProjectsProjectVisabilityTableSeeder');
        $this->call('Bct\Projects\Database\Seeds\UiBaseProjectsValueTableSeeder');
    }
}
