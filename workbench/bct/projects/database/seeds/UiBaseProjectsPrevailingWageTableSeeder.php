<?php namespace Bct\Projects\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

class UiBaseProjectsPrevailingWageTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $util = app()->make('bct.unionimpactbase.seedutil');

        $util->setTable('z_prevailing_wage');

        $util->setKeyColumns(['name']);

        $util->setData([
            ['name' => 'State'],
            ['name' => 'Federal'],
            ['name' => 'Private'],
        ]);

        $util->seed();

    }

}
