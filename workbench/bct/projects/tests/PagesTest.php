<?php namespace Bct\Projects\Tests;
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 04.03.16
 * Time: 16:08
 */

use Bct\Projects\Models\User;
use Bct\Projects\Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PagesTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */

    protected $users = [
        'superadmin' => '1@1.com',
        'rep_user' => 'rep_user@gmail.com',
        'rep_admin' => 'rep_admin@gmail.com',
    ];

    public function testDashboardPageFromSuperAdmin()
    {
        $user = User::email($this->users['superadmin'])->first();

        $this->actingAs($user)
            ->visit('/pprofiles/dashboard')
            ->assertResponseOk();
    }
}
