<?php

use Illuminate\Foundation\Application;
use Cartalyst\Extensions\ExtensionInterface;
use Cartalyst\Settings\Repository as Settings;
use Cartalyst\Permissions\Container as Permissions;

return [

    /*
    |--------------------------------------------------------------------------
    | Name
    |--------------------------------------------------------------------------
    |
    | This is your extension name and it is only required for
    | presentational purposes.
    |
    */

    'name' => 'Projects',

    /*
    |--------------------------------------------------------------------------
    | Slug
    |--------------------------------------------------------------------------
    |
    | This is your extension unique identifier and should not be changed as
    | it will be recognized as a new extension.
    |
    | Ideally, this should match the folder structure within the extensions
    | folder, but this is completely optional.
    |
    */

    'slug' => 'bct/projects',

    /*
    |--------------------------------------------------------------------------
    | Author
    |--------------------------------------------------------------------------
    |
    | Because everybody deserves credit for their work, right?
    |
    */

    'author' => 'Aphiwad Chhoeun',

    /*
    |--------------------------------------------------------------------------
    | Description
    |--------------------------------------------------------------------------
    |
    | One or two sentences describing the extension for users to view when
    | they are installing the extension.
    |
    */

    'description' => 'Projects Component',

    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | Version should be a string that can be used with version_compare().
    | This is how the extensions versions are compared.
    |
    */

    'version' => '0.2.0',

    /*
    |--------------------------------------------------------------------------
    | Requirements
    |--------------------------------------------------------------------------
    |
    | List here all the extensions that this extension requires to work.
    | This is used in conjunction with composer, so you should put the
    | same extension dependencies on your main composer.json require
    | key, so that they get resolved using composer, however you
    | can use without composer, at which point you'll have to
    | ensure that the required extensions are available.
    |
    */

    'require' => [
        'platform/users',
        'bct/unionimpactbase',
    ],

    /*
    |--------------------------------------------------------------------------
    | Autoload Logic
    |--------------------------------------------------------------------------
    |
    | You can define here your extension autoloading logic, it may either
    | be 'composer', 'platform' or a 'Closure'.
    |
    | If composer is defined, your composer.json file specifies the autoloading
    | logic.
    |
    | If platform is defined, your extension receives convetion autoloading
    | based on the Platform standards.
    |
    | If a Closure is defined, it should take two parameters as defined
    | bellow:
    |
    |	object \Composer\Autoload\ClassLoader      $loader
    |	object \Illuminate\Foundation\Application  $app
    |
    | Supported: "composer", "platform", "Closure"
    |
    */

    'autoload' => 'composer',

    /*
    |--------------------------------------------------------------------------
    | Service Providers
    |--------------------------------------------------------------------------
    |
    | Define your extension service providers here. They will be dynamically
    | registered without having to include them in app/config/app.php.
    |
    */

    'providers' => [
        'Bct\Projects\Providers\UserServiceProvider',
        'Bct\Projects\Providers\LogServiceProvider',
        'Bct\Projects\Providers\GroupServiceProvider',
        'Bct\Projects\Providers\ContactServiceProvider',
        'Bct\Projects\Providers\ContractorServiceProvider',
        'Bct\Projects\Providers\ProjectServiceProvider',
        'Bct\Projects\Providers\VisitServiceProvider',
        'Bct\Projects\Providers\ActionServiceProvider',
        'Bct\Projects\Providers\FileServiceProvider',
        'Bct\Projects\Providers\ListsServiceProvider',
        'Bct\Projects\Providers\ContactTypeServiceProvider',
        'Bct\Projects\Providers\ActionTypeServiceProvider',
        'Bct\Projects\Providers\ScopeServiceProvider',
        'Bct\Projects\Providers\MediaServiceProvider',
        'Bct\Projects\Providers\SettingsServiceProvider',
        'Bct\Projects\Providers\JavaScriptServiceProvider',
        'Bct\Projects\Providers\MemberServiceProvider',
        'Bct\Projects\Providers\ConsoleServiceProvider'

    ],

    /*
    |--------------------------------------------------------------------------
    | Routes
    |--------------------------------------------------------------------------
    |
    | Closure that is called when the extension is started. You can register
    | any custom routing logic here.
    |
    | The closure parameters are:
    |
    |	object \Cartalyst\Extensions\ExtensionInterface  $extension
    |	object \Illuminate\Foundation\Application        $app
    |
    */

    'routes' => function (ExtensionInterface $extension, Application $app) {

        /*
         * Youtube help page
         */
        Route::get('pprofiles/help',
            ['as' => 'bct.projects.help', 'uses' => 'Bct\Projects\Controllers\Frontend\BaseController@showHelpPage']);

        /*
         * Media
         */
        Route::group([
            'prefix'    => 'pprofiles/media',
            'as'        => 'bct.projects.media.',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('view/{id}', ['as' => 'view', 'uses' => 'MediaController@view'])->where('id', '.*?');
            Route::get('download/{id}', ['as' => 'download', 'uses' => 'MediaController@download'])->where('id', '.*?');
        });

        /** MEMBERS */
        Route::group([
            'prefix'    => 'pprofiles/members',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('create', ['as' => 'bct.projects.members.create', 'uses' => 'MembersController@create']);
        });

        /** CONTACTS */
        Route::group([
            'prefix'    => 'pprofiles/contacts',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::post('merge', ['as' => 'bct.projects.contacts.merge', 'uses' => 'ContactsController@merge']);

            Route::get('/', ['as' => 'bct.projects.contacts.all', 'uses' => 'ContactsController@index']);
            Route::post('/', ['as' => 'bct.projects.contacts.all', 'uses' => 'ContactsController@executeAction']);

            Route::get('grid', ['as' => 'bct.projects.contacts.grid', 'uses' => 'ContactsController@grid']);
            Route::get('gridProjects/{id}',
                ['as' => 'bct.projects.contacts.gridProjects', 'uses' => 'ContactsController@gridProjects']);

            Route::get('create', ['as' => 'bct.projects.contacts.create', 'uses' => 'ContactsController@create']);
            Route::post('create', ['as' => 'bct.projects.contacts.create', 'uses' => 'ContactsController@store']);

            Route::get('{id}', ['as' => 'bct.projects.contacts.edit', 'uses' => 'ContactsController@edit']);
            Route::post('{id}', ['as' => 'bct.projects.contacts.edit', 'uses' => 'ContactsController@update']);

            Route::get('details/{id}',
                ['as' => 'bct.projects.contacts.details', 'uses' => 'ContactsController@details']);
            Route::get('pdf/details/{id}',
                ['as' => 'bct.projects.contacts.pdf.details', 'uses' => 'ContactsController@downloadPdfDetails']);

            Route::delete('{id}', ['as' => 'bct.projects.contacts.delete', 'uses' => 'ContactsController@delete']);
        });

        /** CONTRACTORS */
        Route::group([
            'prefix'    => 'pprofiles/contractors',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('/', ['as' => 'bct.projects.contractors.all', 'uses' => 'ContractorsController@index']);
            Route::post('/', ['as' => 'bct.projects.contractors.all', 'uses' => 'ContractorsController@executeAction']);

            Route::get('grid', ['as' => 'bct.projects.contractors.grid', 'uses' => 'ContractorsController@grid']);
            Route::get('gridProjects/{id}',
                ['as' => 'bct.projects.contractors.gridProjects', 'uses' => 'ContractorsController@gridProjects']);

            Route::get('create', ['as' => 'bct.projects.contractors.create', 'uses' => 'ContractorsController@create']);
            Route::post('create', ['as' => 'bct.projects.contractors.create', 'uses' => 'ContractorsController@store']);

            Route::post('merge',
                ['as' => 'bct.projects.contractors.merge', 'uses' => 'ContractorsController@mergeAction']);
            Route::get('merge/confirm/{stay_contractor_id}/{merge_contractor_id}',
                ['as' => 'bct.projects.contractors.merge.confirm', 'uses' => 'ContractorsController@mergeConfirm']);

            Route::get('{id}', ['as' => 'bct.projects.contractors.edit', 'uses' => 'ContractorsController@edit']);
            Route::post('{id}', ['as' => 'bct.projects.contractors.edit', 'uses' => 'ContractorsController@update']);

            Route::get('details/{id}',
                ['as' => 'bct.projects.contractors.details', 'uses' => 'ContractorsController@details']);
            Route::get('pdf/details/{id}',
                ['as' => 'bct.projects.contractors.pdf.details', 'uses' => 'ContractorsController@downloadPdfDetails']);

            Route::delete('{id}',
                ['as' => 'bct.projects.contractors.delete', 'uses' => 'ContractorsController@delete']);

            Route::get('{id}/files/create',
                ['as' => 'bct.projects.contractors.createFile', 'uses' => 'ContractorFileController@createFile']);
            Route::post('{id}/files/create',
                ['as' => 'bct.projects.contractors.createFile', 'uses' => 'ContractorFileController@storeFile']);

            Route::get('{id}/files/edit/{file_id}', [
                'as'   => 'bct.projects.contractors.editFile',
                'uses' => 'ProjectContractorFileControllerFileController@editFile'
            ]);
            Route::post('{id}/files/edit/{file_id}',
                ['as' => 'bct.projects.contractors.editFile', 'uses' => 'ContractorFileController@updateFile']);

            Route::get('{id}/files/view/{path}', [
                'as'   => 'bct.projects.contractors.viewFile',
                'uses' => 'ContractorFileController@viewFile'
            ])->where('path', '.*?');

            Route::post('{id}/files/delete',
                ['as' => 'bct.projects.contractors.deleteFiles', 'uses' => 'ContractorFileController@deleteFiles']);
            Route::delete('{contractor_id}/{file_id}/file/delete',
                ['as' => 'bct.projects.contractors.deleteFile', 'uses' => 'ContractorFileController@deleteFile']);

            Route::get('files/get/{contractor_id?}',
                ['as' => 'bct.projects.contractors.files.get', 'uses' => 'ContractorFileController@filesGet']);
            Route::post('files/upload/{contractor_id?}',
                ['as' => 'bct.projects.contractors.files.upload', 'uses' => 'ContractorFileController@filesUpload']);
            Route::delete('files/delete/{file_id}',
                ['as' => 'bct.projects.contractors.files.delete', 'uses' => 'ContractorFileController@filesDelete']);
            Route::post('files/update',
                ['as' => 'bct.projects.contractors.files.update', 'uses' => 'ContractorFileController@filesUpdate']);
            Route::get('{contractor_id}/files',
                ['as' => 'bct.projects.contractors.files', 'uses' => 'ContractorFileController@index']);
            Route::get('list/json',
                ['as' => 'bct.projects.contractors.list', 'uses' => 'ContractorsController@getContractorsJsonList']);

            Route::get('address/get/{address_id}',
                ['as' => 'address.get', 'uses' => 'ContractorAddressController@getAddress']);
            Route::post('address/create', ['as' => 'address.create', 'uses' => 'ContractorAddressController@store']);
            Route::post('address/update/{address_id}',
                ['as' => 'address.update', 'uses' => 'ContractorAddressController@update']);
            Route::delete('address/delete/{address_id}',
                ['as' => 'address.delete', 'uses' => 'ContractorAddressController@delete']);

            Route::get('phone/get/{phone_id}', ['as' => 'phone.get', 'uses' => 'ContractorPhoneController@getPhone']);
            Route::post('phone/create', ['as' => 'phone.create', 'uses' => 'ContractorPhoneController@store']);
            Route::post('phone/update/{phone_id}',
                ['as' => 'phone.update', 'uses' => 'ContractorPhoneController@update']);
            Route::delete('phone/delete/{phone_id}',
                ['as' => 'phone.delete', 'uses' => 'ContractorPhoneController@delete']);

            Route::get('email/get/{email_id}', ['as' => 'email.get', 'uses' => 'ContractorEmailController@getEmail']);
            Route::post('email/create', ['as' => 'email.create', 'uses' => 'ContractorEmailController@store']);
            Route::post('email/update/{email_id}',
                ['as' => 'email.update', 'uses' => 'ContractorEmailController@update']);
            Route::delete('email/delete/{email_id}',
                ['as' => 'email.delete', 'uses' => 'ContractorEmailController@delete']);

            Route::get('company/get/{company_id}',
                ['as' => 'email.get', 'uses' => 'ContractorCompanyController@getCompany']);
            Route::post('company/create', ['as' => 'company.create', 'uses' => 'ContractorCompanyController@store']);
            Route::post('company/update/{company_id}',
                ['as' => 'company.update', 'uses' => 'ContractorCompanyController@update']);
            Route::delete('company/delete/{company_id}',
                ['as' => 'company.delete', 'uses' => 'ContractorCompanyController@delete']);

            Route::get('license/get/{license_id}',
                ['as' => 'license.get', 'uses' => 'ContractorLicenseController@getLicense']);
            Route::post('license/create', ['as' => 'license.create', 'uses' => 'ContractorLicenseController@store']);
            Route::post('license/update/{license_id}',
                ['as' => 'license.update', 'uses' => 'ContractorLicenseController@update']);
            Route::delete('license/delete/{license_id}',
                ['as' => 'license.delete', 'uses' => 'ContractorLicenseController@delete']);

            Route::get('notes/get/{notes_id}', ['as' => 'notes.get', 'uses' => 'ContractorNotesController@getNotes']);
            Route::post('notes/create', ['as' => 'notes.create', 'uses' => 'ContractorNotesController@store']);
            Route::post('notes/update/{notes_id}',
                ['as' => 'notes.update', 'uses' => 'ContractorNotesController@update']);
            Route::delete('notes/delete/{notes_id}',
                ['as' => 'notes.delete', 'uses' => 'ContractorNotesController@delete']);

        });

        /** MAP */
        Route::group([
            'prefix'    => 'pprofiles/map',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('/', ['as' => 'bct.projects.map', 'uses' => 'MapController@index']);

            Route::get('get/projects',
                ['as' => 'bct.projects.map.get.projects', 'uses' => 'MapController@getProjectsByMap']);
        });

        /** REVIEWS */
        Route::group([
            'prefix'    => 'pprofiles/visits',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('/', ['as' => 'bct.projects.visits', 'uses' => 'ReviewsController@index']);
            Route::get('grid', ['as' => 'bct.projects.visits.grid', 'uses' => 'ReviewsController@grid']);

            Route::post('/approve', ['as' => 'bct.projects.visits.approve', 'uses' => 'ReviewsController@approve']);
            Route::post('/unapprove',
                ['as' => 'bct.projects.visits.unapprove', 'uses' => 'ReviewsController@unapprove']);
        });

        /** PROJECTS */
        Route::group([
            'prefix'    => 'pprofiles/projects',
            'namespace' => 'Bct\Projects\Controllers\Frontend\Projects',
        ], function () {
            Route::get('/', ['as' => 'bct.projects.projects.all', 'uses' => 'ProjectsController@index']);
            Route::post('/', ['as' => 'bct.projects.projects.all', 'uses' => 'ProjectsController@executeAction']);

            Route::post('merge', ['as' => 'bct.projects.projects.merge', 'uses' => 'ProjectsController@mergeAction']);
            Route::get('merge/confirm/{stay_project_id}/{merge_project_id}',
                ['as' => 'bct.projects.projects.merge.confirm', 'uses' => 'ProjectsController@mergeConfirm']);

            Route::post('check',
                ['as' => 'bct.projects.projects.check', 'uses' => 'ProjectListController@checkProjectsContainInList']);
            Route::get('grid', ['as' => 'bct.projects.projects.grid', 'uses' => 'ProjectsController@grid']);
            Route::post('grid/pdf/details', [
                'as'   => 'bct.projects.projects.grid.pdf.details',
                'uses' => 'ProjectsController@downloadGridPdfDetails'
            ]);
            Route::post('grid/pdf/field-reports', [
                'as'   => 'bct.projects.projects.grid.pdf.fieldreports',
                'uses' => 'ProjectsController@downloadGridPdfFieldReports'
            ]);

            Route::get('map/search/grid',
                ['as' => 'bct.projects.projects.newproject', 'uses' => 'ProjectsController@gridProjectCreate']);
            Route::post('map/search',
                ['as' => 'bct.projects.routes.search', 'uses' => 'ProjectsController@searchOnMap']);

            Route::get('search', ['as' => 'bct.projects.projects.search', 'uses' => 'ProjectsController@search']);

            Route::get('create', ['as' => 'bct.projects.projects.create', 'uses' => 'ProjectsController@create']);
            Route::post('create', ['as' => 'bct.projects.projects.create', 'uses' => 'ProjectsController@store']);

            Route::get('{id}', ['as' => 'bct.projects.projects.edit', 'uses' => 'ProjectsController@edit']);
            Route::post('{id}', ['as' => 'bct.projects.projects.update', 'uses' => 'ProjectsController@update']);

            Route::get('summary/{id}',
                ['as' => 'bct.projects.projects.details', 'uses' => 'ProjectsController@details']);
            Route::get('summary/{id}/unionvalue',
                ['as' => 'bct.projects.projects.details.unionvalue', 'uses' => 'ProjectsController@getUnionValue']);
            Route::get('summary/{id}/contacts',
                ['as' => 'bct.projects.projects.details.contacts', 'uses' => 'ProjectsController@getContacts']);
            Route::get('summary/{id}/contractors',
                ['as' => 'bct.projects.projects.details.contractors', 'uses' => 'ProjectsController@getContractors']);
            Route::get('summary/{id}/visits',
                ['as' => 'bct.projects.projects.details.visits', 'uses' => 'ProjectsController@getVisits']);
            Route::get('summary/{id}/actions',
                ['as' => 'bct.projects.projects.details.actions', 'uses' => 'ProjectsController@getActions']);
            Route::get('summary/{id}/files',
                ['as' => 'bct.projects.projects.details.files', 'uses' => 'ProjectsController@getFiles']);
            Route::get('summary/{id}/lists',
                ['as' => 'bct.projects.projects.details.lists', 'uses' => 'ProjectsController@getLists']);
            Route::get('summary/{id}/members',
                ['as' => 'bct.projects.projects.details.members', 'uses' => 'ProjectsController@getMembers']);
            Route::post('summary/{id}/check', [
                'as'   => 'bct.projects.projects.checkCompletedProject',
                'uses' => 'ProjectsController@checkCompletedProjectExistsInList'
            ]);
            Route::get('pdf/details/{id}',
                ['as' => 'bct.projects.projects.pdf.details', 'uses' => 'ProjectsController@downloadPdfDetails']);
            Route::get('pdf/field-report/{id}', [
                'as'   => 'bct.projects.projects.pdf.fieldreport',
                'uses' => 'ProjectsController@downloadPdfFieldReport'
            ]);

            Route::post('get-lists/{id}',
                ['as' => 'bct.projects.projects.getLists', 'uses' => 'ProjectListController@getGroupLists']);
            Route::post('get-groups/{group_type}',
                ['as' => 'bct.projects.projects.getGroups', 'uses' => 'ProjectListController@getGroups']);

            Route::delete('{id}', ['as' => 'bct.projects.projects.delete', 'uses' => 'ProjectsController@delete']);

            /** CONTRACTORS TAB */
            Route::get('{id}/contractors/grid', [
                'as'   => 'bct.projects.projects.gridContractors',
                'uses' => 'ProjectContractorController@gridContractors'
            ]);
            Route::get('{id}/contractors/gridAll', [
                'as'   => 'bct.projects.projects.gridAllContractors',
                'uses' => 'ProjectContractorController@gridAllContractors'
            ]);

            Route::get('{id}/contractors/add/na', [
                'as'   => 'bct.projects.projects.contractor.add.na',
                'uses' => 'ProjectContractorController@addContractorNa'
            ]);
            Route::post('{id}/contractors/add/na', [
                'as'   => 'bct.projects.projects.contractor.add.na',
                'uses' => 'ProjectContractorController@storeContractorNa'
            ]);

            Route::get('{id}/contractors/add/{contractor_id}', [
                'as'   => 'bct.projects.projects.contractor.add',
                'uses' => 'ProjectContractorController@addContractor'
            ]);
            Route::post('{id}/contractors/add/{contractor_id}', [
                'as'   => 'bct.projects.projects.contractor.add',
                'uses' => 'ProjectContractorController@storeContractor'
            ]);

            Route::get('{id}/contractors/edit/na/pivot/{pivot_id}', [
                'as'   => 'bct.projects.projects.contractor.edit.na',
                'uses' => 'ProjectContractorController@editContractorNa'
            ]);
            Route::get('{id}/contractors/edit/{contractor_id}/pivot/{pivot_id}', [
                'as'   => 'bct.projects.projects.contractor.edit',
                'uses' => 'ProjectContractorController@editContractor'
            ]);
            Route::post('{id}/contractors/edit/na/pivot/{pivot_id}', [
                'as'   => 'bct.projects.projects.contractor.edit.na',
                'uses' => 'ProjectContractorController@updateContractorNa'
            ]);
            Route::post('{id}/contractors/edit/{contractor_id}/pivot/{pivot_id}', [
                'as'   => 'bct.projects.projects.contractor.edit',
                'uses' => 'ProjectContractorController@updateContractor'
            ]);

            Route::post('{id}/contractors/delete/{pivot_id}', [
                'as'   => 'bct.projects.projects.deleteContractor',
                'uses' => 'ProjectContractorController@deleteContractor'
            ]);

            /** CONTACTS TAB */
            Route::get('{id}/contacts/grid',
                ['as' => 'bct.projects.projects.gridContacts', 'uses' => 'ProjectContactController@gridContacts']);
            Route::get('{id}/contacts/gridAll', [
                'as'   => 'bct.projects.projects.gridAllContacts',
                'uses' => 'ProjectContactController@gridAllContacts'
            ]);

            Route::get('{id}/contacts/add/{contact_id}',
                ['as' => 'bct.projects.projects.contact.add', 'uses' => 'ProjectContactController@addContact']);
            Route::post('{id}/contacts/add/{contact_id}',
                ['as' => 'bct.projects.projects.contact.add', 'uses' => 'ProjectContactController@storeContact']);

            Route::get('{id}/contacts/edit/{contact_id}/pivot/{pivot_id}',
                ['as' => 'bct.projects.projects.contact.edit', 'uses' => 'ProjectContactController@editContact']);
            Route::post('{id}/contacts/edit/{contact_id}/pivot/{pivot_id}',
                ['as' => 'bct.projects.projects.contact.edit', 'uses' => 'ProjectContactController@updateContact']);

            Route::post('{id}/contacts/delete/{pivot_id}',
                ['as' => 'bct.projects.projects.deleteContact', 'uses' => 'ProjectContactController@deleteContact']);

            Route::get('get/contactTypesData', [
                'as'   => 'bct.projects.projects.getContactTypesData',
                'uses' => 'ProjectContactController@getContactTypesData'
            ]);

            /** MEMBERS TAB */
            Route::get('{id}/members/grid',
                ['as' => 'bct.projects.projects.gridMembers', 'uses' => 'ProjectMemberController@gridMembers']);
            Route::get('{id}/members/gridAll',
                ['as' => 'bct.projects.projects.gridAllMembers', 'uses' => 'ProjectMemberController@gridAllMembers']);

            Route::get('{id}/members/add/{contact_id}',
                ['as' => 'bct.projects.projects.member.add', 'uses' => 'ProjectMemberController@addMember']);
            Route::post('{id}/members/add/{contact_id}',
                ['as' => 'bct.projects.projects.member.add', 'uses' => 'ProjectMemberController@storeMember']);

            Route::get('{id}/members/edit/{contact_id}/pivot/{pivot_id}',
                ['as' => 'bct.projects.projects.member.edit', 'uses' => 'ProjectMemberController@editMember']);
            Route::post('{id}/members/edit/{contact_id}/pivot/{pivot_id}',
                ['as' => 'bct.projects.projects.member.edit', 'uses' => 'ProjectMemberController@updateMember']);

            Route::post('{id}/members/delete/{pivot_id}',
                ['as' => 'bct.projects.projects.deleteMember', 'uses' => 'ProjectMemberController@deleteMember']);

            /** VISITS TAB */
            Route::get('{id}/visits/grid',
                ['as' => 'bct.projects.projects.gridVisits', 'uses' => 'ProjectVisitController@gridVisits']);

            Route::get('{id}/visits/create',
                ['as' => 'bct.projects.projects.createVisit', 'uses' => 'ProjectVisitController@createVisit']);
            Route::post('{id}/visits/create',
                ['as' => 'bct.projects.projects.createVisit', 'uses' => 'ProjectVisitController@storeVisit']);

            Route::get('{id}/visits/edit/{visit_id}',
                ['as' => 'bct.projects.projects.editVisit', 'uses' => 'ProjectVisitController@editVisit']);
            Route::post('{id}/visits/edit/{visit_id}',
                ['as' => 'bct.projects.projects.editVisit', 'uses' => 'ProjectVisitController@updateVisit']);

            /** ACTIONS TAB */
            Route::get('{id}/actions/grid',
                ['as' => 'bct.projects.projects.gridActions', 'uses' => 'ProjectActionController@gridActions']);

            Route::get('{id}/actions/create',
                ['as' => 'bct.projects.projects.createAction', 'uses' => 'ProjectActionController@createAction']);
            Route::post('{id}/actions/create',
                ['as' => 'bct.projects.projects.createAction', 'uses' => 'ProjectActionController@storeAction']);

            Route::get('{id}/actions/edit/{action_id}',
                ['as' => 'bct.projects.projects.editAction', 'uses' => 'ProjectActionController@editAction']);
            Route::post('{id}/actions/edit/{action_id}',
                ['as' => 'bct.projects.projects.editAction', 'uses' => 'ProjectActionController@updateAction']);

            Route::post('{id}/actions/delete/{action_id}',
                ['as' => 'bct.projects.projects.deleteAction', 'uses' => 'ProjectActionController@deleteAction']);

            /** FILES TAB */

            // @deprecated
            //Route::get('{id}/files/grid', ['as' => 'bct.projects.projects.gridFiles', 'uses' => 'ProjectFileController@gridFiles']);

            Route::get('{id}/files/create',
                ['as' => 'bct.projects.projects.createFile', 'uses' => 'ProjectFileController@createFile']);
            Route::post('{id}/files/create',
                ['as' => 'bct.projects.projects.createFile', 'uses' => 'ProjectFileController@storeFile']);

            Route::get('{id}/files/edit/{file_id}',
                ['as' => 'bct.projects.projects.editFile', 'uses' => 'ProjectFileController@editFile']);
            Route::post('{id}/files/edit/{file_id}',
                ['as' => 'bct.projects.projects.editFile', 'uses' => 'ProjectFileController@updateFile']);

            Route::get('{id}/files/view/{path}',
                ['as' => 'bct.projects.projects.viewFile', 'uses' => 'ProjectFileController@viewFile'])->where('path',
                '.*?');

            Route::post('{id}/files/delete',
                ['as' => 'bct.projects.projects.deleteFiles', 'uses' => 'ProjectFileController@deleteFiles']);
            Route::delete('{project_id}/{file_id}/file/delete',
                ['as' => 'bct.projects.projects.deleteFile', 'uses' => 'ProjectFileController@deleteFile']);

            Route::get('files/get/{project_id?}',
                ['as' => 'bct.projects.projects.files.get', 'uses' => 'ProjectFileController@filesGet']);
            Route::post('files/upload/{project_id?}',
                ['as' => 'bct.projects.projects.files.upload', 'uses' => 'ProjectFileController@filesUpload']);
            Route::delete('files/delete/{file_id}',
                ['as' => 'bct.projects.projects.files.delete', 'uses' => 'ProjectFileController@filesDelete']);
            Route::post('files/update',
                ['as' => 'bct.projects.projects.files.update', 'uses' => 'ProjectFileController@filesUpdate']);

            /** LISTS TAB */
            Route::get('{id}/lists/grid',
                ['as' => 'bct.projects.projects.gridLists', 'uses' => 'ProjectListController@gridLists']);

            Route::get('{id}/lists/create',
                ['as' => 'bct.projects.projects.createLists', 'uses' => 'ProjectListController@createLists']);
            Route::post('{id}/lists/create',
                ['as' => 'bct.projects.projects.createLists', 'uses' => 'ProjectListController@storeLists']);

            Route::post('{id}/lists/delete/{pivot_id}',
                ['as' => 'bct.projects.projects.deleteLists', 'uses' => 'ProjectListController@deleteLists']);

            /** STEWARDS TAB */
            Route::get('{id}/stewards/grid',
                ['as' => 'bct.projects.projects.gridStewards', 'uses' => 'ProjectStewardController@gridStewards']);
            Route::get('{id}/stewards/gridAll', [
                'as'   => 'bct.projects.projects.gridAllStewards',
                'uses' => 'ProjectStewardController@gridAllStewards'
            ]);

            Route::post('{id}/stewards/add/{steward_id}',
                ['as' => 'bct.projects.projects.steward.add', 'uses' => 'ProjectStewardController@storeSteward']);

            Route::post('{id}/stewards/delete/{pivot_id}',
                ['as' => 'bct.projects.projects.deleteSteward', 'uses' => 'ProjectStewardController@deleteSteward']);

            Route::get('{id}/status',
                ['as' => 'bct.projects.projects.status', 'uses' => 'ProjectsController@getStatus']);
            Route::get('{id}/follow-up-date',
                ['as' => 'bct.projects.projects.follow_up_date', 'uses' => 'ProjectsController@getFollowUpDate']);


        });

        /** SETTINGS */
        Route::group([
            'prefix'    => 'pprofiles/settings',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            /** USERS */
            Route::group([
                'prefix' => 'users',
            ], function () {
                Route::get('/', ['as' => 'bct.projects.users.all', 'uses' => 'UsersController@index']);
                Route::post('/', ['as' => 'bct.projects.users.all', 'uses' => 'UsersController@executeAction']);

                Route::get('grid', ['as' => 'bct.projects.users.grid', 'uses' => 'UsersController@grid']);
                Route::get('gridGroups/{id}',
                    ['as' => 'bct.projects.users.gridGroups', 'uses' => 'UsersController@gridGroups']);

                Route::get('create', ['as' => 'bct.projects.users.create', 'uses' => 'UsersController@create']);
                Route::post('create', ['as' => 'bct.projects.users.create', 'uses' => 'UsersController@store']);

                Route::post('check', ['as' => 'bct.projects.users.check', 'uses' => 'UsersController@checkUser']);
                Route::post('addrole', ['as' => 'bct.projects.users.addrole', 'uses' => 'UsersController@addRole']);

                Route::get('{id}', ['as' => 'bct.projects.users.edit', 'uses' => 'UsersController@edit']);
                Route::post('{id}', ['as' => 'bct.projects.users.edit', 'uses' => 'UsersController@update']);

                Route::delete('{id}', ['as' => 'bct.projects.users.delete', 'uses' => 'UsersController@delete']);

                Route::get('link/{id}', ['as' => 'bct.projects.users.link', 'uses' => 'UsersController@editLink']);
                Route::post('link/{id}', ['as' => 'bct.projects.users.link', 'uses' => 'UsersController@updateLink']);
                Route::put('link/{id}', ['as' => 'bct.projects.users.view', 'uses' => 'UsersController@executeAction']);
                Route::post('link/{id}/refresh/groups',
                    ['as' => 'bct.projects.users.refresh.groups', 'uses' => 'UsersController@getSummaryGroupsCounter']);
                Route::post('link/{id}/refresh/visits',
                    ['as' => 'bct.projects.users.refresh.visits', 'uses' => 'UsersController@getSummaryVisitsCounter']);

                Route::get('{id}/visits',
                    ['as' => 'bct.projects.users.visits', 'uses' => 'UsersController@gridVisits']);
            });

            /** GROUPS */
            Route::group([
                'prefix' => 'groups',
            ], function () {
                Route::get('/', ['as' => 'bct.projects.groups.all', 'uses' => 'GroupsController@index']);
                Route::post('/', ['as' => 'bct.projects.groups.all', 'uses' => 'GroupsController@executeAction']);

                Route::get('grid', ['as' => 'bct.projects.groups.grid', 'uses' => 'GroupsController@grid']);
                Route::get('gridUsers/{id}',
                    ['as' => 'bct.projects.groups.gridUsers', 'uses' => 'GroupsController@gridUsers']);

                Route::get('create', ['as' => 'bct.projects.groups.create', 'uses' => 'GroupsController@create']);
                Route::post('create', ['as' => 'bct.projects.groups.create', 'uses' => 'GroupsController@store']);

                Route::post('check',
                    ['as' => 'bct.projects.groups.check', 'uses' => 'GroupsController@checkGroupToListAttachment']);

                Route::get('{id}/edit', ['as' => 'bct.projects.groups.edit', 'uses' => 'GroupsController@edit']);
                Route::post('{id}/edit', ['as' => 'bct.projects.groups.edit', 'uses' => 'GroupsController@update']);

                Route::get('{id}', ['as' => 'bct.projects.groups.view', 'uses' => 'GroupsController@view']);
                Route::post('{id}', ['as' => 'bct.projects.groups.view', 'uses' => 'GroupsController@updateView']);
                Route::post('{id}/refresh',
                    ['as' => 'bct.projects.groups.refresh', 'uses' => 'GroupsController@getSummaryRepsCounter']);

                Route::get('{id}/delete',
                    ['as' => 'bct.projects.groups.deleteView', 'uses' => 'GroupsController@deleteView']);
                Route::delete('{id}', ['as' => 'bct.projects.groups.delete', 'uses' => 'GroupsController@delete']);
            });

            /** SETTINGS CONTACT TYPES */
            Route::group([
                'prefix' => 'contact-types',
            ], function () {
                Route::get('/', ['as' => 'bct.projects.contact_types.all', 'uses' => 'ContactTypesController@index']);
                Route::post('/',
                    ['as' => 'bct.projects.contact_types.all', 'uses' => 'ContactTypesController@executeAction']);

                Route::get('grid',
                    ['as' => 'bct.projects.contact_types.grid', 'uses' => 'ContactTypesController@grid']);

                Route::get('create',
                    ['as' => 'bct.projects.contact_types.create', 'uses' => 'ContactTypesController@create']);
                Route::post('create',
                    ['as' => 'bct.projects.contact_types.create', 'uses' => 'ContactTypesController@store']);

                Route::get('edit/{id}',
                    ['as' => 'bct.projects.contact_types.edit', 'uses' => 'ContactTypesController@edit']);
                Route::post('edit/{id}',
                    ['as' => 'bct.projects.contact_types.edit', 'uses' => 'ContactTypesController@update']);

                Route::post('delete/{id}',
                    ['as' => 'bct.projects.contact_types.delete', 'uses' => 'ContactTypesController@delete']);
            });

            /** SETTINGS ACTION TYPES */
            Route::group([
                'prefix' => 'action-types',
            ], function () {
                Route::get('/', ['as' => 'bct.projects.action_types.all', 'uses' => 'Projects\ProjectActionTypesController@index']);
                Route::post('/',
                    ['as' => 'bct.projects.action_types.all', 'uses' => 'Projects\ProjectActionTypesController@executeAction']);

                Route::get('grid',
                    ['as' => 'bct.projects.action_types.grid', 'uses' => 'Projects\ProjectActionTypesController@grid']);

                Route::get('create',
                    ['as' => 'bct.projects.action_types.create', 'uses' => 'Projects\ProjectActionTypesController@create']);
                Route::post('create',
                    ['as' => 'bct.projects.action_types.create', 'uses' => 'Projects\ProjectActionTypesController@store']);

                Route::get('edit/{id}',
                    ['as' => 'bct.projects.action_types.edit', 'uses' => 'Projects\ProjectActionTypesController@edit']);
                Route::post('edit/{id}',
                    ['as' => 'bct.projects.action_types.edit', 'uses' => 'Projects\ProjectActionTypesController@update']);

                Route::post('delete/{id}',
                    ['as' => 'bct.projects.action_types.delete', 'uses' => 'Projects\ProjectActionTypesController@delete']);
            });

            /** SETTINGS SCOPES */
            Route::group([
                'prefix' => 'scopes',
            ], function () {
                Route::get('/', ['as' => 'bct.projects.scopes.all', 'uses' => 'ScopesController@index']);
                Route::post('/', ['as' => 'bct.projects.scopes.all', 'uses' => 'ScopesController@executeAction']);

                Route::get('grid', ['as' => 'bct.projects.scopes.grid', 'uses' => 'ScopesController@grid']);

                Route::get('create', ['as' => 'bct.projects.scopes.create', 'uses' => 'ScopesController@create']);
                Route::post('create', ['as' => 'bct.projects.scopes.create', 'uses' => 'ScopesController@store']);

                Route::get('edit/{id}', ['as' => 'bct.projects.scopes.edit', 'uses' => 'ScopesController@edit']);
                Route::post('edit/{id}', ['as' => 'bct.projects.scopes.edit', 'uses' => 'ScopesController@update']);

                Route::post('delete/{id}', ['as' => 'bct.projects.scopes.delete', 'uses' => 'ScopesController@delete']);
            });

            /** PROJECT TAGS**/
            Route::group([
                'prefix' => 'tags',
            ], function () {
                Route::get('/', ['as' => 'bct.projects.tags.all', 'uses' => 'TagsController@index']);
                Route::post('/', ['as' => 'bct.projects.tags.all', 'uses' => 'TagsController@executeAction']);

                Route::get('grid', ['as' => 'bct.projects.tags.grid', 'uses' => 'TagsController@grid']);

                Route::get('create', ['as' => 'bct.projects.tags.create', 'uses' => 'TagsController@create']);
                Route::post('create', ['as' => 'bct.projects.tags.create', 'uses' => 'TagsController@store']);

                Route::get('edit/{id}', ['as' => 'bct.projects.tags.edit', 'uses' => 'TagsController@edit']);
                Route::post('edit/{id}', ['as' => 'bct.projects.tags.edit', 'uses' => 'TagsController@update']);

                Route::get('delete/{id}', ['as' => 'bct.projects.tags.delete', 'uses' => 'TagsController@delete']);


            });

            /** SETTINGS STEWARDS */
            Route::group([
                'prefix' => 'stewards',
            ], function () {
                Route::get('create', ['as' => 'bct.projects.stewards.create', 'uses' => 'StewardsController@create']);
                Route::post('create', ['as' => 'bct.projects.stewards.create', 'uses' => 'StewardsController@store']);

                Route::get('{id}', ['as' => 'bct.projects.stewards.edit', 'uses' => 'StewardsController@edit']);
                Route::post('{id}', ['as' => 'bct.projects.stewards.edit', 'uses' => 'StewardsController@update']);

                Route::delete('{id}', ['as' => 'bct.projects.stewards.delete', 'uses' => 'StewardsController@delete']);

                Route::get('details/{id}',
                    ['as' => 'bct.projects.stewards.details', 'uses' => 'StewardsController@details']);
            });

            /** SETTINGS CONFIGURATION */
            Route::group([
                'prefix' => 'configuration',
            ], function () {
                Route::get('/',
                    ['as' => 'bct.projects.configuration.change', 'uses' => 'SettingsController@configuration']);
                Route::post('/',
                    ['as' => 'bct.projects.configuration.change', 'uses' => 'SettingsController@changeConfiguration']);
            });

        });

        /** LISTS */
        Route::group([
            'prefix'    => 'pprofiles/lists',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('/', ['as' => 'bct.projects.lists.search', 'uses' => 'ListsController@search']);
            Route::get('grid', ['as' => 'bct.projects.lists.gridForUsers', 'uses' => 'ListsController@gridForUsers']);
            Route::get('{id}/projects',
                ['as' => 'bct.projects.lists.gridProjects', 'uses' => 'ListsController@gridProjects']);

            Route::get('{id}/projects/grid',
                ['as' => 'bct.projects.lists.gridForProjects', 'uses' => 'ListsController@gridForProjects']);
            Route::get('{id}/reps/grid',
                ['as' => 'bct.projects.lists.gridForReps', 'uses' => 'ListsController@gridForReps']);
            Route::get('{id}/projects-tab/grid',
                ['as' => 'bct.projects.lists.gridForProjectsTab', 'uses' => 'ListsController@gridForProjectsTab']);

            Route::get('create', ['as' => 'bct.projects.lists.create', 'uses' => 'ListsController@create']);
            Route::post('create', ['as' => 'bct.projects.lists.create', 'uses' => 'ListsController@store']);

            Route::get('edit/{id}', ['as' => 'bct.projects.lists.edit', 'uses' => 'ListsController@edit']);
            Route::post('edit/{id}', ['as' => 'bct.projects.lists.edit', 'uses' => 'ListsController@update']);

            Route::get('summary/{id}', ['as' => 'bct.projects.lists.details', 'uses' => 'ListsController@details']);
            Route::post('summary/{id}', ['as' => 'bct.projects.lists.details', 'uses' => 'ListsController@updateView']);
            Route::post('summary/{id}/refresh',
                ['as' => 'bct.projects.lists.refresh', 'uses' => 'ListsController@getSummaryProjectsCounter']);
            Route::post('delete/{id}', ['as' => 'bct.projects.lists.delete', 'uses' => 'ListsController@delete']);
        });

        /** ROUTES */
        Route::group([
            'prefix'    => 'pprofiles/routes',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('/', ['as' => 'bct.projects.routes.index', 'uses' => 'RoutesController@index']);
            Route::post('/', ['as' => 'bct.projects.routes.submit.route', 'uses' => 'RoutesController@submitRoute']);

            Route::post('create', ['as' => 'bct.projects.routes.create', 'uses' => 'RoutesController@create']);
            Route::post('sort', ['as' => 'bct.projects.routes.sort', 'uses' => 'RoutesController@sort']);

            Route::post('delete/{id}', ['as' => 'bct.projects.routes.delete', 'uses' => 'RoutesController@delete']);
        });

        /** DASHBOARD */
        Route::group([
            'prefix'    => 'pprofiles/dashboard',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('/', ['as' => 'bct.projects.dashboard.index', 'uses' => 'DashboardController@index']);

            Route::get('/share-by-area',
                ['as' => 'bct.projects.dashboard.share_by_area', 'uses' => 'DashboardController@shareByArea']);
            Route::get('/totals', ['as' => 'bct.projects.dashboard.totals', 'uses' => 'DashboardController@totals']);
            Route::get('/top-contractors',
                ['as' => 'bct.projects.dashboard.top_contractors', 'uses' => 'DashboardController@topContractors']);
            Route::get('/follow-up',
                ['as' => 'bct.projects.dashboard.follow_up', 'uses' => 'DashboardController@followUp']);
            Route::get('/rep-visits',
                ['as' => 'bct.projects.dashboard.rep_visits', 'uses' => 'DashboardController@repVisits']);
        });

        /** LOCALS */
        Route::group([
            'prefix'    => 'pprofiles/locals',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('/get/withID/json',
                ['as' => 'bct.projects.locals.get.id.json', 'uses' => 'LocalsController@getLocals']);
        });

        // --- REPORTS ---
        Route::group([
            'prefix'    => 'pprofiles/reports/',
            'namespace' => 'Bct\Projects\Controllers\Frontend',
        ], function () {
            Route::get('', ['as' => 'bct.projects.reports.index', 'uses' => 'ReportController@index']);
            Route::get('report1/projects',
                ['as' => 'bct.projects.reports.report1.projects', 'uses' => 'ReportController@report1Projects']);
            Route::post('report1/projects/export', [
                'as'   => 'bct.projects.reports.report1.projects.export',
                'uses' => 'ReportController@report1ProjectsExport'
            ]);

            Route::get('active-projects-with-stewards/', [
                'as'   => 'bct.projects.reports.active_projects_with_stewards',
                'uses' => 'ReportController@reportActiveProjectsWithStewards'
            ]);
            Route::post('active-projects-with-stewards/export', [
                'as'   => 'bct.projects.reports.active_projects_with_stewards.export',
                'uses' => 'ReportController@reportActiveProjectsWithStewardsExport'
            ]);

            Route::get('active-service-jobs/', [
                'as'   => 'bct.projects.reports.active_service_jobs',
                'uses' => 'ReportController@reportActiveServiceJobs'
            ]);

            Route::post('active-service-jobs/export', [
                'as'   => 'bct.projects.reports.active_service_jobs.export',
                'uses' => 'ReportController@reportActiveServiceJobsExport'
            ]);

            Route::get('rep-summary/', [
                'as'   => 'bct.projects.reports.rep_summary',
                'uses' => 'ReportController@reportRepSummary'
            ]);

            Route::get('rep-summary/grid', [
                'as'   => 'bct.projects.reports.rep_summary.grid',
                'uses' => 'ReportController@reportRepSummaryGrid'
            ]);

            Route::get('contractor-summary/', [
                'as'   => 'bct.projects.reports.contractor_summary',
                'uses' => 'ReportController@reportContractorSummary'
            ]);

            Route::get('contractor-summary/grid', [
                'as'   => 'bct.projects.reports.contractor_summary.grid',
                'uses' => 'ReportController@reportContractorSummaryGrid'
            ]);

            Route::get('contractor-summary/projects/{contractor_id}/grid', [
                'as'   => 'bct.projects.reports.contractor_summary.projects.grid',
                'uses' => 'ReportController@contractorSummaryProjectsGrid'
            ]);
            Route::get('contractor-summary/projects/{contractor_id}', [
                'as'   => 'bct.projects.reports.contractor_summary.projects',
                'uses' => 'ReportController@сontractorSummaryProjects'
            ]);

            Route::get('stage-summary/', [
                'as'   => 'bct.projects.reports.stage_summary',
                'uses' => 'ReportController@reportStageSummary'
            ]);

            Route::get('stage-summary/grid', [
                'as'   => 'bct.projects.reports.stage_summary.grid',
                'uses' => 'ReportController@reportStageSummaryGrid'
            ]);

            Route::get('stage-summary/projects/{stage_id}/grid', [
                'as'   => 'bct.projects.reports.stage_summary.projects.grid',
                'uses' => 'ReportController@stageSummaryProjectsGrid'
            ]);
            Route::get('stage-summary/projects/{stage_id}', [
                'as'   => 'bct.projects.reports.stage_summary.projects',
                'uses' => 'ReportController@stageSummaryProjects'
            ]);

            Route::get('wage-summary/', [
                'as'   => 'bct.projects.reports.wage_summary',
                'uses' => 'ReportController@reportWageSummary'
            ]);


            Route::get('wage-summary/grid', [
                'as'   => 'bct.projects.reports.wage_summary.grid',
                'uses' => 'ReportController@reportWageSummaryGrid'
            ]);

            Route::get('wage-summary/projects/{wage_id}/grid', [
                'as'   => 'bct.projects.reports.wage_summary.projects.grid',
                'uses' => 'ReportController@wageSummaryProjectsGrid'
            ]);
            Route::get('wage-summary/projects/{wage_id}', [
                'as'   => 'bct.projects.reports.wage_summary.projects',
                'uses' => 'ReportController@wageSummaryProjects'
            ]);

            Route::get('contacts-summary/', [
                'as'   => 'bct.projects.reports.contacts_summary',
                'uses' => 'ReportController@reportContactsSummary'
            ]);

            Route::get('contacts-summary/grid', [
                'as'   => 'bct.projects.reports.contacts_summary.grid',
                'uses' => 'ReportController@reportContactsSummaryGrid'
            ]);

            Route::get('contacts-summary/projects/{contacts_id}/grid', [
                'as'   => 'bct.projects.reports.contacts_summary.projects.grid',
                'uses' => 'ReportController@contactsSummaryProjectsGrid'
            ]);
            Route::get('contacts-summary/projects/{contacts_id}', [
                'as'   => 'bct.projects.reports.contacts_summary.projects',
                'uses' => 'ReportController@contactsSummaryProjects'
            ]);

            Route::get('project-classification-summary/', [
                'as'   => 'bct.projects.reports.project_classification_summary',
                'uses' => 'ReportController@reportProjectClassificationSummary'
            ]);

            Route::get('project-classification-summary/grid', [
                'as'   => 'bct.projects.reports.project_classification_summary.grid',
                'uses' => 'ReportController@reportProjectClassificationSummaryGrid'
            ]);

            Route::get('project-classification-summary/projects/{classification_id}/grid', [
                'as'   => 'bct.projects.reports.project_classification_summary.projects.grid',
                'uses' => 'ReportController@projectClassificationSummaryProjectsGrid'
            ]);
            Route::get('project-classification-summary/projects/{classification_id}', [
                'as'   => 'bct.projects.reports.project_classification_summary.projects',
                'uses' => 'ReportController@projectClassificationSummaryProjects'
            ]);

            Route::get('project-region-summary/', [
                'as'   => 'bct.projects.reports.project_region_summary',
                'uses' => 'ReportController@reportProjectRegionSummary'
            ]);

            Route::get('project-region-summary/grid', [
                'as'   => 'bct.projects.reports.project_region_summary.grid',
                'uses' => 'ReportController@reportProjectRegionSummaryGrid'
            ]);

            Route::get('project-region-summary/projects/{region}/grid', [
                'as'   => 'bct.projects.reports.project_region_summary.projects.grid',
                'uses' => 'ReportController@projectRegionSummaryProjectsGrid'
            ]);
            Route::get('project-region-summary/projects/{region}', [
                'as'   => 'bct.projects.reports.project_region_summary.projects',
                'uses' => 'ReportController@projectRegionSummaryProjects'
            ]);

            Route::get('project-type-summary/', [
                'as'   => 'bct.projects.reports.project_type_summary',
                'uses' => 'ReportController@reportProjectTypeSummary'
            ]);

            Route::get('project-type-summary/grid', [
                'as'   => 'bct.projects.reports.project_type_summary.grid',
                'uses' => 'ReportController@reportProjectTypeSummaryGrid'
            ]);

            Route::get('project-type-summary/projects/{project_type_id}/grid', [
                'as'   => 'bct.projects.reports.project_type_summary.projects.grid',
                'uses' => 'ReportController@projectTypeSummaryProjectsGrid'
            ]);
            Route::get('project-type-summary/projects/{project_type_id}', [
                'as'   => 'bct.projects.reports.project_type_summary.projects',
                'uses' => 'ReportController@projectTypeSummaryProjects'
            ]);

            Route::get('actions-summary/', [
                'as'   => 'bct.projects.reports.actions_summary',
                'uses' => 'ReportController@reportActionsSummary'
            ]);

            Route::get('actions-summary/grid', [
                'as'   => 'bct.projects.reports.actions_summary.grid',
                'uses' => 'ReportController@reportActionsSummaryGrid'
            ]);

            Route::get('actions-summary/projects/{action_type_id}/grid', [
                'as'   => 'bct.projects.reports.actions_summary.projects.grid',
                'uses' => 'ReportController@actionsSummaryProjectsGrid'
            ]);
            Route::get('actions-summary/projects/{action_type_id}', [
                'as'   => 'bct.projects.reports.actions_summary.projects',
                'uses' => 'ReportController@actionsSummaryProjects'
            ]);
        });

    },

    /*
    |--------------------------------------------------------------------------
    | Database Seeds
    |--------------------------------------------------------------------------
    |
    | Platform provides a very simple way to seed your database with test
    | data using seed classes. All seed classes should be stored on the
    | `database/seeds` directory within your extension folder.
    |
    | The order you register your seed classes on the array below
    | matters, as they will be ran in the exact same order.
    |
    | The seeds array should follow the following structure:
    |
    |	Vendor\Namespace\Database\Seeds\FooSeeder
    |	Vendor\Namespace\Database\Seeds\BarSeeder
    |
    */

    'seeds' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | Permissions
    |--------------------------------------------------------------------------
    |
    | Register here all the permissions that this extension has. These will
    | be shown in the user management area to build a graphical interface
    | where permissions can be selected to allow or deny user access.
    |
    | For detailed instructions on how to register the permissions, please
    | refer to the following url https://cartalyst.com/manual/permissions
    |
    */

    'permissions' => function (Permissions $permissions) {
        $permissions->group('Projects', function ($g) {
            $g->name = 'Projects Profiles';

            /** PROJECTS USERS  */
            // LIST USERS
            $g->permission('projects_users.index', function ($p) {
                $p->label = trans('bct/projects::users/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\UsersController', 'index, grid');
            });

            // CREATE NEW USER
            $g->permission('projects_users.create', function ($p) {
                $p->label = trans('bct/projects::users/permissions.create');

                $p->controller('Bct\Projects\Controllers\Frontend\UsersController',
                    'create, store, checkUser, addRole');
            });

            // EDIT USER
            $g->permission('projects_users.edit', function ($p) {
                $p->label = trans('bct/projects::users/permissions.edit');

                $p->controller('Bct\Projects\Controllers\Frontend\UsersController', 'edit, update, editLink, updateLink,
				gridGroups, getSummaryGroupsCounter');
            });

            // DELETE USER
            $g->permission('projects_users.delete', function ($p) {
                $p->label = trans('bct/projects::users/permissions.delete');

                $p->controller('Bct\Projects\Controllers\Frontend\UsersController', 'delete, executeAction');
            });

            // USER VISITS
            $g->permission('projects_users.visits', function ($p) {
                $p->label = trans('bct/projects::users/permissions.visits');

                $p->controller('Bct\Projects\Controllers\Frontend\UsersController', 'gridVisits');
            });

            /** PROJECTS GROUPS */
            // LIST GROUPS
            $g->permission('projects_group.index', function ($p) {
                $p->label = trans('bct/projects::groups/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\GroupsController', 'index, grid');
            });

            // CREATE NEW GROUP
            $g->permission('projects_groups.create', function ($p) {
                $p->label = trans('bct/projects::groups/permissions.create');

                $p->controller('Bct\Projects\Controllers\Frontend\GroupsController', 'create, store');
            });

            // EDIT GROUP
            $g->permission('projects_groups.edit', function ($p) {
                $p->label = trans('bct/projects::groups/permissions.edit');

                $p->controller('Bct\Projects\Controllers\Frontend\GroupsController', 'edit, update, view, updateView,
				gridUsers, checkGroupToListAttachment, getSummaryRepsCounter');
            });

            // DELETE GROUP
            $g->permission('projects_groups.delete', function ($p) {
                $p->label = trans('bct/projects::groups/permissions.delete');

                $p->controller('Bct\Projects\Controllers\Frontend\GroupsController',
                    'delete, deleteView, executeAction');
            });

            /** PROJECTS CONTACTS */
            // LIST CONTACTS
            $g->permission('projects_contact.index', function ($p) {
                $p->label = trans('bct/projects::contacts/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\ContactsController', 'index, grid, gridProjects, merge, 
                details, downloadPdfDetails');
            });

            // CREATE NEW CONTACT
            $g->permission('projects_contact.create', function ($p) {
                $p->label = trans('bct/projects::contacts/permissions.create');

                $p->controller('Bct\Projects\Controllers\Frontend\ContactsController', 'create, store');
            });

            // EDIT CONTACT
            $g->permission('projects_contact.edit', function ($p) {
                $p->label = trans('bct/projects::contacts/permissions.edit');

                $p->controller('Bct\Projects\Controllers\Frontend\ContactsController', 'edit, update');
            });

            // DELETE CONTACT
            $g->permission('projects_contact.delete', function ($p) {
                $p->label = trans('bct/projects::contacts/permissions.delete');

                $p->controller('Bct\Projects\Controllers\Frontend\ContactsController', 'delete, executeAction');
            });

            /** PROJECTS CONTRACTORS */
            // LIST CONTRACTORS
            $g->permission('projects_contractors.index', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorsController', 'index, grid, gridProjects, 
                getContractorsJsonList, details, downloadPdfDetails');
            });

            // CREATE NEW CONTRACTOR
            $g->permission('projects_contractors.create', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.create');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorsController', 'create, store');
            });

            // EDIT CONTRACTOR
            $g->permission('projects_contractors.edit', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.edit');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorsController', 'edit, update, 
                mergeAction, mergeConfirm');
            });

            // DELETE CONTRACTOR
            $g->permission('projects_contractors.delete', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.delete');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorsController', 'delete, executeAction');
            });

            // CONTRACTOR FILES
            $g->permission('projects_contractor_files.all', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.files');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorFileController', '
                    index, gridFiles, createFile, storeFile, editFile, updateFile, viewFile, deleteFiles,
                    deleteFile, filesGet, filesUpload, filesDelete, filesUpdate
                ');
            });

            /** PROJECTS PROJECTS */
            // LIST PROJECTS
            $g->permission('projects_projects.index', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectsController', 'index, grid, getGroupLists, getUnionValue, downloadGridPdfDetails,
                    downloadGridPdfFieldReports, details, downloadPdfDetails, getFiles');
            });

            // CREATE NEW PROJECT
            $g->permission('projects_projects.create', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.create');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectsController',
                    'create, store, search, gridProjectCreate, searchOnMap');
            });

            // EDIT PROJECT
            $g->permission('projects_projects.edit', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.edit');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectsController', 'edit, update, delete, checkProjectsContainInList, getSummaryCounters,
                    checkCompletedProjectExistsInList, getGroupLists,
                    getContacts, getContractors, getVisits, getActions, getStatus, getFollowUpDate,
                    downloadPdfFieldReport, mergeAction, mergeConfirm, getLists
                    ');
            });

            // DELETE PROJECT
            $g->permission('projects_projects.delete', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.delete');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectsController',
                    'delete, executeAction');
            });

            // PROJECT ACTIONS
            $g->permission('projects_project_actions.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.actions');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectActionController', '
                    gridActions, createAction, storeAction, editAction, updateAction, deleteAction
                ');
            });

            // PROJECT CONTRACTORS
            $g->permission('projects_project_contractors.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.contractors');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectContractorController', '
                    gridContractors, gridAllContractors, addContractor, addContractorNa, storeContractor, storeContractorNa, 
                    editContractor, editContractorNa, updateContractor, updateContractorNa, deleteContractor
                ');
            });

            // PROJECT CONTACTS
            $g->permission('projects_project_contacts.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.contacts');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectContactController', '
                    gridContacts, gridAllContacts, addContact, storeContact, editContact, updateContact,
                    deleteContact, getContactTypesData
                ');
            });

            // PROJECT VISITS
            $g->permission('projects_project_visits.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.visits');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectVisitController', '
                    gridVisits, createVisit, storeVisit, editVisit, updateVisit, deleteVisit
                ');
            });

            // PROJECT FILES
            $g->permission('projects_project_files.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.files');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectFileController', '
                    gridFiles, createFile, storeFile, editFile, updateFile, viewFile, deleteFiles,
                    deleteFile, filesGet, filesUpload, filesDelete, filesUpdate
                ');
            });

            // PROJECT LISTS
            $g->permission('projects_project_lists.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.lists');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectListController', '
                    gridLists, createLists, storeLists, deleteLists, getGroups, getGroupLists, checkProjectsContainInList
                ');
            });

            // PROJECT MEMBERS
            $g->permission('projects_project_members.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.members');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectMemberController', '
                    gridMembers, gridAllMembers, addMember, storeMember, editMember, updateMember, deleteMember, getMemberTypesData
                ');
            });

            // PROJECT STEWARDS
            $g->permission('projects_project_stewards.all', function ($p) {
                $p->label = trans('bct/projects::projects/permissions.stewards');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectStewardController', '
                    gridStewards, gridAllStewards, storeSteward, deleteSteward
                ');
            });

            /** PROJECTS AREAS */
            // LIST AREAS
            $g->permission('projects_lists.index', function ($p) {
                $p->label = trans('bct/projects::lists/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\ListsController', 'index, grid');
            });

            /** SETTINGS CONTACT TYPES */
            // LIST AREAS
            $g->permission('contact_types.index', function ($p) {
                $p->label = trans('bct/projects::contact_types/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\ContactTypesController', 'index, grid, create, store,
					edit, update, delete, executeAction');
            });


            $g->permission('action_types.index', function ($p) {
                $p->label = trans('bct/projects::action_types/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectActionTypesController', 'index, grid, create, store,
					edit, update, delete, executeAction');
            });

            /** SETTINGS SCOPES */
            $g->permission('scopes.index', function ($p) {
                $p->label = trans('bct/projects::scopes/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\ScopesController', 'index, grid, create, store,
					edit, update, delete, executeAction');
            });

            /** SETTINGS STEWARDS */
            $g->permission('stewards.index', function ($p) {
                $p->label = trans('bct/projects::stewards/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\StewardsController', 'index, grid, create, store,
					edit, update, delete, executeAction, details');
            });

            /** SETTINGS TAGS */
            $g->permission('tags.index', function ($p) {
                $p->label = trans('bct/projects::tags/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\TagsController', 'index, grid, create, store,
					edit, update, delete, executeAction, details');
            });

            /*
             * Settings \ Configuration
             */
            $g->permission('bct.projects.settings.configuration', function ($p) {
                $p->label = trans('bct/projects::settings/permissions.configuration');

                $p->controller('Bct\Projects\Controllers\Frontend\SettingsController',
                    'configuration, changeConfiguration');
            });

            /** LISTS */
            $g->permission('projects_lists.search', function ($p) {
                $p->label = trans('bct/projects::lists/permissions.search_l');

                $p->controller('Bct\Projects\Controllers\Frontend\ListsController',
                    'search, gridForUsers, gridProjects, details');
            });

            // CREATE NEW LIST
            $g->permission('projects_lists.create', function ($p) {
                $p->label = trans('bct/projects::lists/permissions.create');

                $p->controller('Bct\Projects\Controllers\Frontend\ListsController', 'create, store');
            });

            // EDIT LIST
            $g->permission('projects_lists.edit', function ($p) {
                $p->label = trans('bct/projects::lists/permissions.edit');

                $p->controller('Bct\Projects\Controllers\Frontend\ListsController',
                    'edit, update, gridForUsers, gridProjects, gridForProjects, gridForProjectsTab, gridForReps, updateView, getSummaryProjectsCounter');
            });

            // DELETE LIST
            $g->permission('projects_lists.delete', function ($p) {
                $p->label = trans('bct/projects::lists/permissions.delete');

                $p->controller('Bct\Projects\Controllers\Frontend\ListsController', 'delete, executeAction');
            });

            /** ROUTES */
            $g->permission('projects_routes.index', function ($p) {
                $p->label = trans('bct/projects::routes/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\RoutesController',
                    'index, create, sort, delete, submitRoute');
            });

            /** MAP */
            $g->permission('projects_map.index', function ($p) {
                $p->label = trans('bct/projects::map/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\MapController', 'index, getProjectsByMap');
            });

            /** DASHBOARD */
            $g->permission('projects_dashboard.index', function ($p) {
                $p->label = trans('bct/projects::dashboard/permissions.index');

                $p->controller('Bct\Projects\Controllers\Frontend\DashboardController',
                    'index, shareByArea, totals, topContractors, followUp, repVisits');
            });

            /** LOCALS */
            $g->permission('projects_locals.index', function ($p) {
                $p->label = trans('bct/projects::general/permissions.locals');

                $p->controller('Bct\Projects\Controllers\Frontend\LocalsController', 'getLocals');
            });

            /** DOWNLOAD / VIEW FILES */
            $g->permission('projects_media.index', function ($p) {
                $p->label = trans('bct/projects::general/permissions.media');

                $p->controller('Bct\Projects\Controllers\Frontend\MediaController', 'view, download');
            });

            /** REVIEWS */
            $g->permission('projects_visits.index', function ($p) {
                $p->label = trans('bct/projects::general/permissions.visits');

                $p->controller('Bct\Projects\Controllers\Frontend\ReviewsController',
                    'index, grid, approve, unapprove');
            });

            /*
             * Reports
             */
            $g->permission('bct.pp.reports', function ($p) {
                $p->label = trans('bct/projects::general/permissions.reports');

                $p->controller('Bct\Projects\Controllers\Frontend\ReportController', 'index, report1Projects, report1ProjectsExport, reportActiveProjectsWithStewards, reportActiveProjectsWithStewardsExport, 
                    reportActiveServiceJobs, reportActiveServiceJobsExport, reportRepSummary, reportRepSummaryGrid, reportContractorSummary, reportContractorSummaryGrid, contractorSummaryProjectsGrid, сontractorSummaryProjects, reportStageSummary, reportStageSummaryGrid,
                    stageSummaryProjectsGrid, stageSummaryProjects, reportWageSummary, reportWageSummaryGrid, wageSummaryProjectsGrid, wageSummaryProjects, reportContactsSummary, reportContactsSummaryGrid, contactsSummaryProjectsGrid,
                     contactsSummaryProjects, reportProjectClassificationSummary, reportProjectClassificationSummaryGrid, projectClassificationSummaryProjectsGrid, projectClassificationSummaryProjects, reportProjectRegionSummary, 
                     reportProjectRegionSummaryGrid, projectRegionSummaryProjectsGrid, projectRegionSummaryProjects, reportProjectTypeSummary, reportProjectTypeSummaryGrid, projectTypeSummaryProjectsGrid, projectTypeSummaryProjects, 
                     reportActionsSummary, reportActionsSummaryGrid, actionsSummaryProjectsGrid, actionsSummaryProjects');
            });

            /*
            * Contractors Address \ All
            */
            $g->permission('projects_contractors.address', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.address');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorAddressController',
                    'getAddress, store, update, delete');
            });

            /*
             * Contractors Phone \ All
             */
            $g->permission('projects_contractors.phone', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.phone');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorPhoneController',
                    'getPhone, store, update, delete');
            });

            /*
             * Contractors Email \ All
             */
            $g->permission('projects_contractors.email', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.email');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorEmailController',
                    'getEmail, store, update, delete');
            });

            /*
             * Contractors Company \ All
             */
            $g->permission('projects_contractors.company', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.company');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorCompanyController',
                    'getCompany, store, update, delete');
            });

            /*
             * Contractors License \ All
             */
            $g->permission('projects_contractors.license', function ($p) {
                $p->label = trans('bct/projects::contractors/permissions.license');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorLicenseController',
                    'getLicense, store, update, delete');
            });

            /*
             * Contractors Notes \ All
             */
            $g->permission('projects_contractors.notes', function ($p) {
                $p->label = trans('bct/projects::general/permissions.contractors.notes');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorNotesController',
                    'getNotes, store, update, delete');
            });

            /**
             * Guest Permissions
             */

            // PP-Guest Dashboard Section
            $g->permission('pp_guest.dashboard', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.dashboard');

                $p->controller('Bct\Projects\Controllers\Frontend\DashboardController', 'index, shareByArea, totals, 
                    topContractors, followUp, repVisits');
            });

            // PP-Guest Users Section
            $g->permission('pp_guest.users', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.users');

                $p->controller('Bct\Projects\Controllers\Frontend\UsersController', 'index, grid');
            });

            // PP-Guest Users / Visits Grid
            $g->permission('pp_guest.users.details', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.users.details');

                $p->controller('Bct\Projects\Controllers\Frontend\UsersController', 'gridVisits');
            });

            // PP-Guest Groups Section
            $g->permission('pp_guest.groups', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.groups');

                $p->controller('Bct\Projects\Controllers\Frontend\GroupsController', 'index, grid');
            });

            // PP-Guest Groups / Details Section
            $g->permission('pp_guest.groups.details', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.groups.details');

                $p->controller('Bct\Projects\Controllers\Frontend\GroupsController',
                    'view, gridUsers, getSummaryRepsCounter');
            });

            // PP-Guest Contacts Section
            $g->permission('pp_guest.contacts', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contacts');

                $p->controller('Bct\Projects\Controllers\Frontend\ContactsController', 'index, grid, gridProjects,  
                details, downloadPdfDetails');
            });

            // PP-Guest Contractors Section
            $g->permission('pp_guest.contractors', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorsController', 'index, grid, gridProjects, 
                getContractorsJsonList, details, downloadPdfDetails');
            });

            // PP-Guest Contractors / Details Section
            $g->permission('pp_guest.contractors.details', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors.details');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorFileController', 'index, gridFiles, 
                    viewFile, filesGet');
            });

            // PP-Guest Contractors / Address Section
            $g->permission('pp_guest.contractors.address', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors.address');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorAddressController', 'getAddress');
            });

            // PP-Guest Contractors / Phone Section
            $g->permission('pp_guest.contractors.phone', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors.phone');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorPhoneController', 'getPhone');
            });

            // PP-Guest Contractors / Email Section
            $g->permission('pp_guest.contractors.email', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors.email');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorEmailController', 'getEmail');
            });

            // PP-Guest Contractors / Company Section
            $g->permission('pp_guest.contractors.company', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors.company');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorCompanyController', 'getCompany');
            });

            // PP-Guest Contractors / License Section
            $g->permission('pp_guest.contractors.license', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors.license');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorLicenseController', 'getLicense');
            });

            // PP-Guest Contractors / Notes Section
            $g->permission('pp_guest.contractors.notes', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contractors.notes');

                $p->controller('Bct\Projects\Controllers\Frontend\ContractorNotesController', 'getNotes');
            });

            // PP-Guest Projects Section
            $g->permission('pp_guest.projects', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectsController', 'index, grid, getGroupLists, 
                    getUnionValue, downloadGridPdfDetails, downloadGridPdfFieldReports, details, downloadPdfDetails, getFiles,
                    search, searchOnMap');
            });

            // PP-Guest Projects / Details Section
            $g->permission('pp_guest.projects.details', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectsController', 'checkProjectsContainInList, getSummaryCounters,
                    checkCompletedProjectExistsInList, getGroupLists, getContacts, getContractors, getVisits, getActions, getStatus, getFollowUpDate,
                    downloadPdfFieldReport, getLists');
            });

            // PP-Guest Projects / Details / Actions Section
            $g->permission('pp_guest.projects.details.actions', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details.actions');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectActionController', 'gridActions');
            });

            // PP-Guest Projects / Details / Contractors Section
            $g->permission('pp_guest.projects.details.contractors', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details.contractors');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectContractorController',
                    'gridContractors, gridAllContractors');
            });

            // PP-Guest Projects / Details / Contacts Section
            $g->permission('pp_guest.projects.details.contacts', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details.contacts');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectContactController', '
                    gridContacts, gridAllContacts
                ');
            });

            // PP-Guest Projects / Details / Visits Section
            $g->permission('pp_guest.projects.details.visits', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details.visits');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectVisitController', 'gridVisits');
            });

            // PP-Guest Projects / Details / Files Section
            $g->permission('pp_guest.projects.details.files', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details.files');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectFileController',
                    'gridFiles, viewFile, filesGet');
            });

            // PP-Guest Projects / Details / Lists Section
            $g->permission('pp_guest.projects.details.lists', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details.lists');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectListController', 'gridLists');
            });

            // PP-Guest Projects / Details / Members Section
            $g->permission('pp_guest.projects.details.members', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.projects.details.members');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectMemberController', '
                    gridMembers, gridAllMembers
                ');
            });

            // PP-Guest Lists Section
            $g->permission('pp_guest.lists', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.lists');

                $p->controller('Bct\Projects\Controllers\Frontend\ListsController', 'index, grid');
            });

            // PP-Guest Lists / Details Section
            $g->permission('pp_guest.lists.details', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.lists.details');

                $p->controller('Bct\Projects\Controllers\Frontend\ListsController', 'search, gridForUsers, gridProjects, details,
                    gridForProjects, gridForProjectsTab, gridForReps, getSummaryProjectsCounter');
            });

            // PP-Guest Contact Types Section
            $g->permission('pp_guest.contact-types', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.contact-types');

                $p->controller('Bct\Projects\Controllers\Frontend\ContactTypesController', 'index, grid');
            });

            // PP-Guest Scopes Section
            $g->permission('pp_guest.scopes', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.scopes');

                $p->controller('Bct\Projects\Controllers\Frontend\ScopesController', 'index, grid');
            });

            // PP-Guest Stewards Section
            $g->permission('pp_guest.stewards', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.stewards');

                $p->controller('Bct\Projects\Controllers\Frontend\StewardsController', 'index, grid, details');
            });

            // PP-Guest Tags Section
            $g->permission('pp_guest.tags', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.tags');

                $p->controller('Bct\Projects\Controllers\Frontend\TagsController', 'index, grid, details');
            });

            // PP-Guest Routes Section
            $g->permission('pp_guest.routes', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.routes');

                $p->controller('Bct\Projects\Controllers\Frontend\RoutesController', 'index');
            });

            // PP-Guest Map Section
            $g->permission('pp_guest.map', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.map');

                $p->controller('Bct\Projects\Controllers\Frontend\MapController', 'index, getProjectsByMap');
            });


            // CREATE NEW PROJECT
            $g->permission('pp_guest.map.search', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.map.search');

                $p->controller('Bct\Projects\Controllers\Frontend\Projects\ProjectsController',
                    'search, gridProjectCreate, searchOnMap');
            });

            // PP-Guest Locals Section
            $g->permission('pp_guest.locals', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.locals');

                $p->controller('Bct\Projects\Controllers\Frontend\LocalsController', 'getLocals');
            });

            // PP-Guest Media
            $g->permission('pp_guest.media', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.media');

                $p->controller('Bct\Projects\Controllers\Frontend\MediaController', 'view, download');
            });

            // PP-Guest Reviews Section
            $g->permission('pp_guest.reviews', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.reviews');

                $p->controller('Bct\Projects\Controllers\Frontend\ReviewsController', 'index, grid');
            });

            // PP-Guest Reports Section
            $g->permission('pp_guest.reports', function ($p) {
                $p->label = trans('bct/projects::ppguest/permissions.reports');

                $p->controller('Bct\Projects\Controllers\Frontend\ReportController', 'index, report1Projects, report1ProjectsExport, 
                    reportActiveProjectsWithStewards, reportActiveProjectsWithStewardsExport, reportActiveServiceJobs, reportActiveServiceJobsExport,reportRepSummary, reportRepSummaryGrid, reportContractorSummary, reportContractorSummaryGrid, contractorSummaryProjectsGrid, сontractorSummaryProjects, reportStageSummary, reportStageSummaryGrid,
                    stageSummaryProjectsGrid, stageSummaryProjects, reportWageSummary, reportWageSummaryGrid, wageSummaryProjectsGrid, wageSummaryProjects, reportContactsSummary, reportContactsSummaryGrid, contactsSummaryProjectsGrid,
                     contactsSummaryProjects, reportProjectClassificationSummary, reportProjectClassificationSummaryGrid, projectClassificationSummaryProjectsGrid, projectClassificationSummaryProjects, reportProjectRegionSummary, 
                     reportProjectRegionSummaryGrid, projectRegionSummaryProjectsGrid, projectRegionSummaryProjects, reportProjectTypeSummary, reportProjectTypeSummaryGrid, projectTypeSummaryProjectsGrid, projectTypeSummaryProjects, 
                     reportActionsSummary, reportActionsSummaryGrid, actionsSummaryProjectsGrid, actionsSummaryProjects');
            });

        });
    },

    /*
    |-------------------------------------------------------s-------------------
    | Widgets
    |--------------------------------------------------------------------------
    |
    | Closure that is called when the extension is started. You can register
    | all your custom widgets here. Of course, Platform will guess the
    | widget class for you, this is just for custom widgets or if you
    | do not wish to make a new class for a very small widget.
    |
    */

    'widgets' => function () {

    },

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    |
    | Register any settings for your extension. You can also configure
    | the namespace and group that a setting belongs to.
    |
    */

    'settings' => function (Settings $settings, Application $app) {

    },

    /*
    |--------------------------------------------------------------------------
    | Menus
    |--------------------------------------------------------------------------
    |
    | You may specify the default various menu hierarchy for your extension.
    | You can provide a recursive array of menu children and their children.
    | These will be created upon installation, synchronized upon upgrading
    | and removed upon uninstallation.
    |
    | Menu children are automatically put at the end of the menu for extensions
    | installed through the Operations extension.
    |
    | The default order (for extensions installed initially) can be
    | found by editing app/config/platform.php.
    |
    */

    'menus' => [

        'admin' => [],

        'projects-admin-menu' => [
            [
                'slug'       => 'projects-admin-projects-main',
                'name'       => 'Projects',
                'class'      => 'fa fa-home fa-fw',
                'uri'        => 'pprofiles/dashboard',
                'regex'      => '/pprofiles/i',
                'visibility' => 'logged_in',
                'children'   => [
                    [
                        'slug'       => 'projects-dashboard-usr',
                        'name'       => 'Dashboard',
                        'class'      => 'fa fa-dashboard fa-fw',
                        'uri'        => 'pprofiles/dashboard',
                        'regex'      => '/pprofiles\/dashboard/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-contacts-usr',
                        'name'       => 'Contacts',
                        'class'      => 'fa fa-phone fa-fw',
                        'uri'        => 'pprofiles/contacts',
                        'regex'      => '/pprofiles\/contacts/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-contractors-usr',
                        'name'       => 'Contractors',
                        'class'      => 'fa fa-wrench fa-fw',
                        'uri'        => 'pprofiles/contractors',
                        'regex'      => '/pprofiles\/contractors/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-usr',
                        'name'       => 'Projects',
                        'class'      => 'fa fa-home fa-fw',
                        'uri'        => 'pprofiles/projects',
                        'regex'      => '/pprofiles\/projects/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-lists-usr',
                        'name'       => 'Lists',
                        'class'      => 'fa fa-list fa-fw',
                        'uri'        => 'pprofiles/lists',
                        'regex'      => '/pprofiles\/lists/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-routes-usr',
                        'name'       => 'Routes',
                        'class'      => 'fa fa-car fa-fw',
                        'uri'        => 'pprofiles/routes',
                        'regex'      => '/pprofiles\/routes/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-map-usr',
                        'name'       => 'Map',
                        'class'      => 'fa fa-map-marker fa-fw',
                        'uri'        => 'pprofiles/map',
                        'regex'      => '/pprofiles\/map/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-visits-usr',
                        'name'       => 'Visits',
                        'class'      => 'fa fa-eye fa-fw',
                        'uri'        => 'pprofiles/visits',
                        'regex'      => '/pprofiles\/visits/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-reports-usr',
                        'name'       => 'Reports',
                        'class'      => 'fa fa-bar-chart fa-fw',
                        'uri'        => 'pprofiles/reports',
                        'regex'      => '/pprofiles\/reports.*/i',
                        'visibility' => 'logged_in',
                    ],
                    [
                        'slug'       => 'projects-settings-admn',
                        'name'       => 'Settings',
                        'class'      => 'fa fa-puzzle-piece fa-fw',
                        'uri'        => 'pprofiles/settings/users',
                        'regex'      => '/pprofiles\/settings.*/i',
                        'visibility' => 'logged_in',
                        'children'   => [
                            [
                                'slug'       => 'projects-settings-users-super',
                                'name'       => 'Reps',
                                'class'      => 'fa fa-user fa-fw',
                                'uri'        => 'pprofiles/settings/users',
                                'regex'      => '/pprofiles\/settings\/users/i',
                                'visibility' => 'logged_in',
                            ],
                            [
                                'slug'       => 'projects-settings-tags-super',
                                'name'       => 'Projects Tags',
                                'class'      => 'fa fa-tasks fa-fw',
                                'uri'        => 'pprofiles/settings/tags',
                                'regex'      => '/pprofiles\/settings\/tags/i',
                                'visibility' => 'logged_in',
                            ],
                            [
                                'slug'       => 'projects-settings-action-types-admn',
                                'name'       => 'Action Types',
                                'class'      => 'fa fa-tasks fa-fw',
                                'uri'        => 'pprofiles/settings/action-types',
                                'regex'      => '/pprofiles\/settings\/action-types/i',
                                'visibility' => 'logged_in',
                            ],
                            [
                                'slug'       => 'projects-settings-groups-admn',
                                'name'       => 'Groups',
                                'class'      => 'fa fa-group fa-fw',
                                'uri'        => 'pprofiles/settings/groups',
                                'regex'      => '/pprofiles\/settings\/groups/i',
                                'visibility' => 'logged_in',
                            ],
                            [
                                'slug'       => 'projects-settings-contact-types-admn',
                                'name'       => 'Contact Types',
                                'class'      => 'fa fa-tasks fa-fw',
                                'uri'        => 'pprofiles/settings/contact-types',
                                'regex'      => '/pprofiles\/settings\/contact-types/i',
                                'visibility' => 'logged_in',
                            ],
                            [
                                'slug'       => 'projects-settings-scopes-super',
                                'name'       => 'Scopes',
                                'class'      => 'fa fa-tasks fa-fw',
                                'uri'        => 'pprofiles/settings/scopes',
                                'regex'      => '/pprofiles\/settings\/scopes/i',
                                'visibility' => 'logged_in',
                            ],
                            [
                                'slug'       => 'projects-settings-configuration-super',
                                'name'       => 'Configuration',
                                'class'      => 'fa fa-cog fa-fw',
                                'uri'        => 'pprofiles/settings/configuration',
                                'regex'      => '/pprofiles\/settings\/configuration/i',
                                'visibility' => 'logged_in',
                            ],
                        ]
                    ],
                ]
            ],
        ],

    ],

];
