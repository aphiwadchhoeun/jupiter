<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>
        @section('title')
            @setting('platform.app.title')
        @show
    </title>
    <meta name="description" content="@yield('meta-description')">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="base_url" content="{{ URL::to('/') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (session('login_success'))
        <script>
            var login_success = '{{ csrf_token() }}';
        </script>
    @endif

    {{-- Queue assets --}}
    {{ Asset::queue('bootstrap', 'bootstrap/css/bootstrap.min.css') }}
    {{ Asset::queue('bootstrap-theme', 'bootstrap/css/bootstrap.theme.css') }}
    {{ Asset::queue('font-awesome', 'font-awesome/fonts/font-awesome.min.css') }}
    {{ Asset::queue('swal', 'bower_components/sweetalert2/dist/sweetalert2.min.css') }}
    {{ Asset::queue('datagrid-loader', 'cartalyst/data-grid::css/datagrid-loader.css') }}
    {{ Asset::queue('app-style', 'app/css/app.css') }}

    {{ Asset::queue('es6-promise', 'bower_components/es6-promise/es6-promise.auto.min.js') }}
    {{ Asset::queue('jquery', 'jquery/js/jquery.min.js') }}

    @if (isExtensionEnabled('bct/reports'))
        {{ Asset::queue('jquery-ui', 'jquery-ui/js/jquery-ui.min.js', 'jquery') }}
    @endif

    {{ Asset::queue('bootstrap', 'bootstrap/js/bootstrap.min.js', 'jquery') }}
    {{ Asset::queue('platform', 'platform/js/platform.js', 'jquery') }}
    {{ Asset::queue('localforage', 'cartalyst/data-grid::bower/localforage/dist/localforage.nopromises.min.js') }}
    {{ Asset::queue('swal', 'bower_components/sweetalert2/dist/sweetalert2.min.js') }}
    {{ Asset::queue('bind-first', 'bower_components/jquery.bind-first/release/jquery.bind-first-0.2.3.min.js', 'jquery') }}
    {{ Asset::queue('app-script', 'app/js/app.js', 'jquery') }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{{ Asset::getUrl('app/img/favicon.png') }}">

    {{-- Compiled styles --}}
    @foreach (Asset::getCompiledStyles() as $style)
        <link href="{{ $style }}" rel="stylesheet">
    @endforeach

    {{-- Call custom inline styles --}}
    @section('styles')
    @show
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better
    experience this site.</p>
<![endif]-->

<div id="wrapper">
    <!-- Navigation -->
    @if (env('GETCODE_MENU'))
        @include('partials/navigation_getcode')
    @else
        @include('partials/navigation')
    @endif

    <div class="base">

        <!-- Page -->
        <div class="page">
            <div class="row visible-print">
                <div class="col-xs-3">
                    <img src="{{ getLogo() }}" alt="@setting('platform.app.title')" class="img-responsive"/>
                </div>
            </div>

            <!-- alerts -->
            <div class="alerts">
                @include('partials/alerts')
            </div>

            <!-- Page Content-->
            <div class="page__content container">
                @yield('page')
            </div>

        </div>

    </div>

    <!-- Footer -->
    @include('partials/footer')

    {{-- Modals --}}
    @include('partials/modals')

    {{-- Compiled scripts --}}
    @foreach (Asset::getCompiledScripts() as $script)
        <script type="text/javascript" src="{{ $script }}"></script>
    @endforeach

    {{-- Call custom inline scripts --}}
    @section('scripts')
    @show
</div>
</body>
</html>
