<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand brand" href="{{ URL::to('/') }}">
                <img src="{{ getLogo() }}" alt="@setting('platform.app.title')"/></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <?php
        $getcode_menu = config('getcode-menu.extensions');
        $current_domain = request()->server->get('SERVER_NAME');
        if(isset($currentUser)){
            $user_auth = $currentUser;
            if (isExtensionEnabled('bct/member') && $user_auth) {
                $mp_user = \Bct\Member\Models\User::find($user_auth->id);
            }
        }

        ?>

        <!-- Account Menu -->
        @nav('account', 0, 'nav navbar-nav navbar-right')

        @if ($getcode_menu && !empty($getcode_menu))

            @foreach ($getcode_menu as $extension => $menu)
                @if ($menu['domain'] === $current_domain)

                    @if (in_array('bct/projects', $menu['enable']))

                        @if (isExtensionEnabled('bct/projects'))
                            <!-- Projects Menu -->
                            @nav('projects-admin-menu', 1, 'nav navbar-nav')
                        @endif
                    @endif

                    @if (in_array('bct/dispatch', $menu['enable']))

                        @if (isExtensionEnabled('bct/dispatch'))
                            <!-- Dispatch Office Menu -->
                            @nav('dispatch-office', 1, 'nav navbar-nav navbar')
                        @endif
                    @endif


                    @if (in_array('bct/dispatch_janitors', $menu['enable']))

                        @if (isExtensionEnabled('bct/dispatch_janitors') && isset($user_auth) && !is_null($user_auth) && !$user_auth->inRole('dpj_kiosk') && !$user_auth->inRole('dpj_contractor'))
                            <!-- Dispatch Janitors Menu -->
                            @nav('dispatch-janitors', 1, 'nav navbar-nav navbar')
                        @endif

                        @if (isExtensionEnabled('bct/dispatch_janitors') && isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('dpj_kiosk'))
                            <!-- Dispatch Janitors Kiosk Menu -->
                            @nav('dispatch-janitors-kiosk', 1, 'nav navbar-nav navbar')
                        @endif

                        @if (isExtensionEnabled('bct/dispatch_janitors') && isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('dpj_contractor'))
                            <!-- Dispatch Janitors Contractor Menu -->
                            @nav('dispatch-janitors-contractor', 1, 'nav navbar-nav navbar')
                        @endif

                    @endif

                    @if (in_array('bct/dispatch_theater', $menu['enable']))

                        @if (isExtensionEnabled('bct/dispatch_theater') && isset($user_auth) && !is_null($user_auth) && !$user_auth->inRole('dpth_kiosk') && !$user_auth->inRole('dpth_contractor'))
                            <!-- Dispatch Theater Menu -->
                            @nav('dispatch-theater', 1, 'nav navbar-nav navbar')
                        @endif

                        @if (isExtensionEnabled('bct/dispatch_theater') && isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('dpth_kiosk'))
                            <!-- Dispatch Theater Kiosk Menu -->
                            @nav('dispatch-theater-kiosk', 1, 'nav navbar-nav navbar')
                        @endif

                        @if (isExtensionEnabled('bct/dispatch_theater') && isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('dpth_contractor'))
                            <!-- Dispatch Theater Contractor Menu -->
                            @nav('dispatch-theater-contractor', 1, 'nav navbar-nav navbar')
                        @endif

                    @endif


                    @if (in_array('bct/member', $menu['enable']))

                        <!-- Member Menu -->
                        @if (
                            isExtensionEnabled('bct/member') &&
                            (
                                (isset($mp_user) && !is_null($mp_user) && ($mp_user->isMPOfficeStaffOrMPAdmin() || $mp_user->isAdmin()))
                                    ||
                                (isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('py_officeadmin'))
                            )
                        )
                            @nav('member-office-menu', 1, 'nav navbar-nav navbar-left')
                        @endif

                        <!-- Member Request Menu -->
                        @if (isExtensionEnabled('bct/member') && isset($mp_user) && !is_null($mp_user) && $mp_user->isMPMember())
                            @nav('member-office-menu-request', 1, 'nav navbar-nav navbar-left')
                        @endif

                        <!-- Member Kiosk User Menu -->
                        @if (isExtensionEnabled('bct/member') && isset($mp_user) && !is_null($mp_user) && $mp_user->isMPKioskUser() )
                            @nav('member-kiosk-user-menu', 1, 'nav navbar-nav navbar-left')
                        @endif

                        @if (
                            isExtensionEnabled('bct/payment') || isExtensionEnabled('bct/payment_seiu87') ||
                            isExtensionEnabled('bct/payment_uaw2865') || isExtensionEnabled('bct/payment_proa')
                        )
                            <!-- Payment Menu -->
                            @nav('payment-office-admin-menu', 1, 'nav navbar-nav')
                        @endif

                    @endif


                    @if (in_array('bct/mrp', $menu['enable']))

                        @if (isExtensionEnabled('bct/mrp'))
                            <!-- MRP Admin Menu -->
                            @nav('mrp-admin-menu', 1, 'nav navbar-nav')

                            <!-- MRP User Menu -->
                            @nav('mrp-menu', 1, 'nav navbar-nav')
                        @endif
                    @endif

                    @if (in_array('bct/mrp_pnw', $menu['enable']))
                        @if (isExtensionEnabled('bct/mrp_pnw'))
                            <!-- MRP Admin Menu -->
                            @nav('mrp-pnw-admin-menu', 1, 'nav navbar-nav')

                            <!-- MRP User Menu -->
                            @nav('mrp-pnw-menu', 1, 'nav navbar-nav')
                        @endif
                    @endif


                    @if (in_array('bct/payment', $menu['enable']))

                        <!-- Member Menu -->
                        @if (
                            isExtensionEnabled('bct/member') &&
                            (
                                (isset($mp_user) && !is_null($mp_user) && ($mp_user->isMPOfficeStaffOrMPAdmin() || $mp_user->isAdmin()))
                                    ||
                                (isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('py_officeadmin'))
                            )
                        )
                            @nav('member-office-menu', 1, 'nav navbar-nav navbar-left')
                        @endif

                        @if(isExtensionEnabled('bct/payment'))
                            <!-- Payment Menu -->
                            @nav('payment-office-admin-menu', 1, 'nav navbar-nav')
                        @endif
                    @endif


                    @if (in_array('bct/payment_seiu87', $menu['enable']))

                        <!-- Member Menu -->
                        @if (
                            isExtensionEnabled('bct/member') &&
                            (
                                (isset($mp_user) && !is_null($mp_user) && ($mp_user->isMPOfficeStaffOrMPAdmin() || $mp_user->isAdmin()))
                                    ||
                                (isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('py_officeadmin'))
                            )
                        )
                            @nav('member-office-menu', 1, 'nav navbar-nav navbar-left')
                        @endif

                        @if(isExtensionEnabled('bct/payment_seiu87'))
                            <!-- Payment Menu -->
                            @nav('payment-office-admin-menu', 1, 'nav navbar-nav')
                        @endif
                    @endif

                    @if (in_array('bct/rci', $menu['enable']))

                        <!-- Member Menu -->
                        @if (
                            isExtensionEnabled('bct/member') &&
                            (
                                (isset($mp_user) && !is_null($mp_user) && ($mp_user->isMPOfficeStaffOrMPAdmin() || $mp_user->isAdmin()))
                                    ||
                                (isset($user_auth) && !is_null($user_auth) && $user_auth->inRole('rci_officeadmin'))
                            )
                        )
                            @nav('member-office-menu', 1, 'nav navbar-nav navbar-left')
                        @endif

                        @if(isExtensionEnabled('bct/rci'))
                            <!-- RCI Menu -->
                                @nav('rci-office-admin-menu', 1, 'nav navbar-nav')
                            @endif
                        @endif

                    @endif

                @endforeach

            @endif


            @if(isExtensionEnabled('bct/reports')  && isset($user_auth) && !$user_auth->inRole('mp_kioskuser'))
                @nav('reports-menu', 1, 'nav navbar-nav navbar')
            @endif

            @if(isExtensionEnabled('bct/unionimpactbase'))
                @nav('uibase-menu', 1, 'nav navbar-nav navbar')
            @endif

            @if(
                isExtensionEnabled('bct/projects') ||
                isExtensionEnabled('bct/member') ||
                isExtensionEnabled('bct/dispatch') ||
                isExtensionEnabled('bct/mrp') ||
                isExtensionEnabled('bct/dispatch_janitors') ||
                isExtensionEnabled('bct/dispatch_theater')
            )
                @nav('open-routes', 1, 'nav navbar-nav navbar')
            @endif

        </div>

    </div>
</nav>

@if(isset($currentUser))

    <nav class="navbar subnav">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#subnav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="subnav">
                <!-- Projects -->
                @if (@preg_match('/pprofiles/i', url()->current()))
                    @nav('projects-admin-projects-main', 0, 'nav navbar-nav')
                @endif

                @if ((bool) preg_match('/\/mp\/?(?!users).*/i', url()->current()) !== false)
                    @nav('bct-memberoffice', 0, 'nav navbar-nav')
                    @nav('bct-memberkioskuser', 0, 'nav navbar-nav')
                @endif

                @if (isExtensionEnabled('bct/payment') &&
                    isset($user_auth) &&
                    !is_null($user_auth))

                    @if (stripos(url()->current(), url('py')) !== false)
                        @nav('bct-payment-office', 0, 'nav navbar-nav')
                    @endif
                @endif

                @if (isExtensionEnabled('bct/payment_seiu87') &&
                    isset($user_auth) &&
                    !is_null($user_auth))

                    @if (stripos(url()->current(), url('py')) !== false)
                        @nav('bct-payment-office', 0, 'nav navbar-nav')
                    @endif
                @endif

                @if (isExtensionEnabled('bct/paymentlite') &&
                    isset($user_auth) &&
                    !is_null($user_auth))

                    @if (stripos(url()->current(), url('pl')) !== false)
                        @nav('bct-paymentlite-member', 0, 'nav navbar-nav')
                        @nav('bct-paymentlite-office', 0, 'nav navbar-nav')
                    @endif
                @endif

                @if (
                    isExtensionEnabled('bct/rci') &&
                    isset($user_auth) &&
                    !is_null($user_auth)
                )

                    @if (stripos(url()->current(), url('rci')) !== false)
                        @nav('bct-rci-office', 0, 'nav navbar-nav')
                    @endif
                @endif

                @if ((bool) preg_match('/\/mp\/?users.*/i', url()->current()) !== false)
                    @nav('bct-user-request', 0, 'nav navbar-nav')
                @endif

                @if (stripos(url()->current(), '/dp/') !== false)
                    @nav('bct-dispatch-office-menu', 0, 'nav navbar-nav')
                @endif

                @if (@preg_match('/mrp\/useradmin\//i', url()->current()))
                    @nav('bct-mrpadmin', 0, 'nav navbar-nav')
                @endif

                @if (@preg_match('/mrp\/user\//i', url()->current()))
                    @nav('bct-mrp', 0, 'nav navbar-nav')
                @endif

                @if (@preg_match('/mrp_pnw\/useradmin\//i', url()->current()))
                    @nav('bct-mrp-pnw-admin', 0, 'nav navbar-nav')
                @endif

                @if (@preg_match('/mrp_pnw\/user\//i', url()->current()))
                    @nav('bct-mrp-pnw', 0, 'nav navbar-nav')
                @endif

                @if (
                    isExtensionEnabled('bct/dispatch_janitors') &&
                    isset($user_auth) &&
                    !is_null($user_auth) &&
                    !$user_auth->inRole('dpj_kiosk') &&
                    !$user_auth->inRole('dpj_contractor') &&
                    stripos(url()->current(), '/dpj/') !== false
                    )
                    @nav('bct-dispatch-janitors-menu', 0, 'nav navbar-nav')
                @endif

                @if (
                    isExtensionEnabled('bct/dispatch_janitors') &&
                    isset($user_auth) &&
                    !is_null($user_auth) &&
                    $user_auth->inRole('dpj_kiosk') &&
                    stripos(url()->current(), '/dpj/') !== false
                    )
                    @nav('bct-dispatch-janitors-kiosk-menu', 0, 'nav navbar-nav')
                @endif

                @if (
                    isExtensionEnabled('bct/dispatch_janitors') &&
                    isset($user_auth) &&
                    !is_null($user_auth) &&
                    $user_auth->inRole('dpj_contractor') &&
                    stripos(url()->current(), '/dpj/') !== false
                    )
                    @nav('bct-dispatch-janitors-contractor-menu', 0, 'nav navbar-nav')
                @endif


                @if (
                    isExtensionEnabled('bct/dispatch_theater') &&
                    isset($user_auth) &&
                    !is_null($user_auth) &&
                    !$user_auth->inRole('dpth_kiosk') &&
                    !$user_auth->inRole('dpth_contractor') &&
                    stripos(url()->current(), '/dpth/') !== false
                    )
                    @nav('bct-dispatch-theater-menu', 0, 'nav navbar-nav')
                @endif

                @if (
                    isExtensionEnabled('bct/dispatch_theater') &&
                    isset($user_auth) &&
                    !is_null($user_auth) &&
                    $user_auth->inRole('dpth_kiosk') &&
                    stripos(url()->current(), '/dpth/') !== false
                    )
                    @nav('bct-dispatch-theater-kiosk-menu', 0, 'nav navbar-nav')
                @endif

                @if (
                    isExtensionEnabled('bct/dispatch_theater') &&
                    isset($user_auth) &&
                    !is_null($user_auth) &&
                    $user_auth->inRole('dpth_contractor') &&
                    stripos(url()->current(), '/dpth/') !== false
                    )
                    @nav('bct-dispatch-theater-contractor-menu', 0, 'nav navbar-nav')
                @endif


                @if (isExtensionEnabled('bct/reports'))
                    @if (stripos(url()->current(), '/ui-reports') !== false)
                        @nav('reports-menu-main', 0, 'nav navbar-nav')
                    @endif
                @endif

            </div>

        </div>
    </nav>

@endif