<div class="footer">
    <div class="footer-box col-lg-12">
        <div class="footer-content">
            <div class="row">
                <div class="col-md-12 text-right">
                    @if(@config('bct.unionimpactbase.show_policy', 1))
                        <a href="/policy"><small><i>UNION IMPACT PRIVACY POLICY</i></small></a>&nbsp;|&nbsp;
                    @endif
                    @if(@config('bct.unionimpactbase.show_help', 1))
                        @section('footer-youtube-link')
                        <a href="/help">
                            <img src="{{ Asset::getUrl('app/img/youtube_icon.png') }}" alt="Help" height="15px">
                            <small><i>Youtube Tutorial Videos</i></small>
                        </a>
                        @show
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@include('cartalyst/data-grid::templates.loader')
