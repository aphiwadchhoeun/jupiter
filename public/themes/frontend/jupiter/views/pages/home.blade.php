@extends('layouts/default')

{{-- Page title --}}
@section('title')
    @parent
    {{{ $page->meta_title or $page->name }}}
@stop


{{-- Queue Assets --}}
{{ Asset::queue('home-style', 'app/css/home.css', 'bootstrap') }}
{{ Asset::queue('sweet-alert-css', 'bower_components/sweetalert2/dist/sweetalert2.css', 'bootstrap') }}
{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('sweet-alert-js', 'bower_components/sweetalert2/dist/sweetalert2.min.js', 'jquery') }}
{{ Asset::queue('home-js', 'app/js/home.js', ['jquery', 'sweet-alert-js']) }}

{{-- Meta description --}}
@section('meta-description')
    {{{ $page->meta_description }}}
@stop

{{-- Page content --}}
@section('page')

    <div class="row">

        @if(!$currentUser)

            <div class="col-md-6 col-md-offset-3">

                <form action="{{{ URL::to('/login') }}}" id="login-form" role="form" method="post" accept-char="UTF-8"
                      autocomplete="off" data-parsley-validate>

                    {{-- CSRF Token --}}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="panel panel-default">

                        <div class="panel-heading">{{{ trans('platform/users::auth/form.login.legend') }}}</div>

                        <div class="panel-body">

                            {{-- Email Address --}}
                            <div class="form-group{{ Alert::onForm('email', ' has-error') }}">

                                <label class="control-label"
                                       for="email">{{{ trans('platform/users::auth/form.email') }}}</label>

                                <input class="form-control" type="email" name="email" id="email"
                                       value="{{{ Input::old('email') }}}"
                                       placeholder="{{{ trans('platform/users::auth/form.email_placeholder') }}}"
                                       required
                                       autofocus
                                       data-parsley-trigger="change"
                                       data-parsley-error-message="{{{ trans('platform/users::auth/form.email_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('email') ?: trans('platform/users::auth/form.email_help') }}}
						</span>

                            </div>

                            {{-- Password --}}
                            <div class="form-group{{ Alert::onForm('password', ' has-error') }}">

                                <label class="control-label"
                                       for="password">{{{ trans('platform/users::auth/form.password') }}}</label>

                                <input class="form-control" type="password" name="password" id="password"
                                       placeholder="{{{ trans('platform/users::auth/form.password_placeholder') }}}"
                                       required
                                       data-parsley-trigger="change"
                                       data-parsley-error-message="{{{ trans('platform/users::auth/form.password_error') }}}">

						<span class="help-block">
							{{{ Alert::onForm('password') ?: trans('platform/users::auth/form.password_help') }}}
						</span>

                            </div>

                            {{-- Remember me --}}
                            <div class="form-group">

                                <label for="remember">

                                    <input type="checkbox" name="remember" id="remember"
                                           value="1"{{ Input::old('remember') ? ' checked="checked"' : null }} />

                                    {{{ trans('platform/users::auth/form.login.remember-me') }}}

                                </label>

                            </div>

                            <hr>

                            {{-- Form actions --}}
                            <div class="form-group">

                                <button class="btn btn-primary btn-block"
                                        type="submit">{{{ trans('platform/users::auth/form.login.submit') }}}
                                </button>

                                <p class="help-block text-center">Don't have an Account? <a
                                            href="{{ url('register') }}">Register</a> or recover your <a
                                            href="{{ url('reminder') }}">{{ trans('platform/users::auth/form.login.forgot-password') }}</a>
                                </p>

                            </div>

                        </div>

                    </div>

                </form>

            </div>

        @else

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        <h1>{{{ @config('platform.app.title') }}}</h1>

                        <p>{{{ @config('platform.app.tagline') }}}</p>

                    </div>
                </div>
            </div>

            <div class="row home-dashboard">
                <div class="homebox">
                    @nav('projects-admin-menu', 1)
                </div>

                <div class="homebox">
                    @nav('office-user-menu', 1)
                    @nav('office-user-menu-request', 1)
                </div>

                <div class="homebox">
                    @nav('mrp-admin-menu', 1)
                    @nav('mrp-menu', 1)
                </div>

                <div class="homebox">
                    @nav('dispatch-office-menu', 1)
                </div>
            </div>

            @if($currentUser->inRole('registered') || $currentUser->inRole('mruser') || $currentUser->inRole('mp_member') || $currentUser->inRole('repuser'))

                <!-- Signup Modal -->
                <?php
                $mrpProfileUri = '#';
                if (app('extensions')->has('bct/mrp') && app('extensions')->get('bct/mrp')->isEnabled()) {
                    $mrpProfileUri = route('bct.mrp.users.profile');
                }

                $memberProfileUri = '#';
                if (app('extensions')->has('bct/member') && app('extensions')->get('bct/member')->isEnabled()) {
                    $memberProfileUri = '/mp/register/member'; // uri link to member profile registration
                }

                $showPopup = empty($currentUser->z_user_profiles_id) ? 1 : 0;
                ?>

                <div class="row">
                    <a href="#" data-toggle="modal" data-target="#signup-modal">Signup for roles</a>
                    <input type="hidden" id="show-popup" value="{{ $showPopup }}"/>
                </div>

                <div class="modal" id="signup-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                            class="fa fa-times"></i></button>
                                <h4 class="modal-title">Signup for roles</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row signup-modal-body">
                                    <div class="col-md-4">
                                        @if($mrpProfileUri!='#' && !$currentUser->inRole('mruser'))
                                            <a href="{{ $mrpProfileUri }}" class="btn btn-primary btn-lg"><i
                                                        class="fa fa-user fa-fw"></i>Contractor</a>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        @if($memberProfileUri!='#' && !$currentUser->inRole('mp_member'))
                                            <a href="{{ $memberProfileUri }}" id="register_member" class="btn btn-primary btn-lg"><i
                                                        class="fa fa-users fa-fw"></i>Member</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <p>
                                    <small>You can always find this popup at the bottom of dashboard.</small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            @endif

        @endif

    </div>

@stop
