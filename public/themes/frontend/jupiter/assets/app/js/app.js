//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size

$(function () {
    $(window).bind("load resize", function () {
        topOffset = $('.navbar-static-top').height() + 34;
        footer_height = $('.footer').height();
        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset - footer_height;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $(".base").css("min-height", (height) + "px");
        }
    })
})


$(function () {
    $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
});

$(function() {
    if ($('.alert').is(':visible')) {
        setTimeout(
            function()
            {
                $('.alert').find('button').click();
            }, 5000);

    }
});

if (typeof login_success !== 'undefined' && login_success === $('meta[name="csrf-token"]').attr('content')) {

    if (typeof localforage !== 'undefined') {

        try {
            localforage.config({
                name: 'Bct DataGrid settings'
            });
            localforage.clear();
        } catch (e) {
            console.error('LocalForage clear error -' + e);
        }

        $.ajax({
            async: false,
            type: 'POST',
            url: window.location.origin + '/ui/filter/get',
            success: function(response) {
                var filters = $.parseJSON(response.filters);

                if (typeof filters !== 'undefined' && filters !== null && filters !== '') {
                    localforage.setItem('datagrid_filters', filters, function(err,value) {
                        if (err !== null) {
                            console.error('LocalForage setItem error - ' + err);
                        }
                    });
                }
            },
            error: function(response) {
                console.error('LocalForage not init');
            }
        });
    } else {
        console.error('LocalForage not init');
    }
}

/*
 --------------------------------------------
 UrlHashMonitor (crossbrowser hashchange)
 --------------------------------------------
 */
(function(w, $){
    var UrlHashMonitor = {};
    UrlHashMonitor.oldHash = '';
    UrlHashMonitor.newHash = '';
    UrlHashMonitor.oldHref = '';
    UrlHashMonitor.newHref = '';
    UrlHashMonitor.onHashChange = function(f){
        $(window).on('hashchange', function(e){
            UrlHashMonitor.oldHash = UrlHashMonitor.newHash;
            UrlHashMonitor.newHash = w.location.hash;
            UrlHashMonitor.oldHref = UrlHashMonitor.newHref;
            UrlHashMonitor.newHref = w.location.href;
            f(e);
        });
    };
    UrlHashMonitor.init = function(){
        UrlHashMonitor.oldHash = UrlHashMonitor.newHash =   w.location.hash;
        UrlHashMonitor.oldHref = UrlHashMonitor.newHref = w.location.href;
    };
    w.UrlHashMonitor = UrlHashMonitor;
    return UrlHashMonitor;

})(window, window.jQuery);

UrlHashMonitor.init();
/*
 --------------------------------------------
 */

/*
 --------------------------------------------
 Jquery Plugin for prevent Double Click
 - only for elements with class "btn-prevent-double-click"
 --------------------------------------------
 */
jQuery.preventDoubleClick = function() {

    $('.btn-prevent-double-click').bindFirst('click', function(e) {
        var $btn = $(this);

        $btn.data('loading-text', "<i class='fa fa-spinner fa-spin'></i> " + $btn.text().trim());

        if ($btn.data('clicked') === true) {
            // Previously submitted - don't submit again
            e.preventDefault();
        } else {
            // Mark it so that the next submit can be ignored
            $btn.data('clicked', true);
            $btn.button('loading');
        }
    });

    return this;
};

function resetLoadingBtn(btn)
{
    var $btn = (btn instanceof jQuery) ? btn : $(btn);
    $btn.data('clicked', false);
    $btn.button('reset');
}

$.preventDoubleClick();
/*
 --------------------------------------------
 */

if (!String.prototype.trim) {
    (function() {
        // Вырезаем BOM и неразрывный пробел
        String.prototype.trim = function() {
            return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
        };
    })();
}