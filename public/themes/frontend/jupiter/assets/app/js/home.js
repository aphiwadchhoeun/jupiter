/**
 * Created by Aphiwad on 8/24/2015.
 */
jQuery('document').ready(function ($) {
    var registerMember = $('#register_member');

    if ($('#show-popup').val() == 1) {
        $('#signup-modal').modal("show");
    }

    registerMember.on('click', function (e) {
        e.preventDefault();

        var dest = $(this).attr('href');

        $.get(dest, function (data) {
            if(data.success) {
                swal(data.message, '', 'success');
            } else {
                swal(data.errors, '', 'error');
            }
        }).fail(function (data) {
            swal('Sorry, the request you\'re trying to make has failed!', '', 'error');
        });
    });

});